<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'saedconnect_wp_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g:!nH;GdTmLf{cyC&1Y$6{a<wwZp+=nAz,$/*9pG[bsQ3Cv3|*+zN{|:?6iXTlb}');
define('SECURE_AUTH_KEY',  'PaLy7hXT(-r{(Vpl;mxc;Xc;D;49>$&N,^!6H&YQ [,-[yv^&hnBVvL2(&4V_C?Q');
define('LOGGED_IN_KEY',    '1bZ<lnc)w3q%|WhX}&52Ep-cR8t>l+c{QbA^9>fvYP%7;xNG3qv/N:0FL$,o]|$6');
define('NONCE_KEY',        'e^,P@5XKI-{)cLx;^WxFBYr/6Hg5Z_dcUqGD@TO:%E<0Iw&,m%2gyJlS!f^=V4WL');
define('AUTH_SALT',        'nhNE+5Uz8R@V>L< -J]eYE@)Qf]uTN8+n2m6<WyGaM+,1>9tF`J3K[~i#C#c[I~J');
define('SECURE_AUTH_SALT', '.U7Xpp`KRb<pZU(b6a6I|T4zf`L*(%Fr]TQ-;J4R>}&/a,q:!+$:IG_a>%?|hrz^');
define('LOGGED_IN_SALT',   '@hY`;TZ(u:.x->N,B4rXy7S!+UKyw%nP(tl-dGT4e@_HXw`Y3~cNO9SbTLQ6L+P0');
define('NONCE_SALT',       'r!h#L1c2p4UuswGcieJ]u;-wX{tQp]#- ea!N.q7wMA&=.f,}(;:.7L8a+{vF0<T');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Multisite Activation */
define('WP_ALLOW_MULTISITE', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
