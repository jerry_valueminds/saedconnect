<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
/* Default comment here */ 


jQuery(document).ready(function( $ ){
    
  $('.ajax-submit').click(function(e){
  	e.preventDefault();
    
    var postName = $("#post-name").val();
    var postSlug = $("#post-slug").val();
    var childPostType = $("#child-post-type").val();
    var parentId = $("#parent-id").val();
    var relationshipType = $("#relationship-type").val();
    
  	$.ajax({ 
         data: {action: 'contact_form', postName:postName, childPostType:childPostType, parentId:parentId, relationshipType:relationshipType, },
         type: 'post',
         url: ajaxurl,
         success: function(data) {
              //alert(data); //should print out the name since you sent it along
        	$(".child-list").append(data);
          	$("#post-name").val("");
        }
    });

});
  
  
});

</script>
<!-- end Simple Custom CSS and JS -->
