<?php /*Template Name: General*/ ?>

<?php get_header() ?>
    
    <main class="main-content">
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="container-wrapper padding-o-80 txt-xlg">
            <?php the_content() ?>
        </div>
        <?php endwhile; // end of the loop. ?>
    </main>

<?php get_footer() ?>