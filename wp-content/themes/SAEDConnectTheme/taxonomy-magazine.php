    <?php get_header() ?>
   
    <main class="main-content">
        <header class="magazine-header row">
            <div class="cool-md-10">
                <h1 class="title">ACCELERATE BLOG</h1>
            </div>
            <div class="col-md-2">
                <div class="dropdown">
                    <button class="btn btn-trans-green dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        SECTIONS
                    </button>
                    <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                        <a class="dropdown-item" href="#">
                            Select...
                        </a>
                        <!-- Get All Top Magazine Terms -->
                        <?php

                            //Get Terms
                            $terms = get_terms( 'magazine', array('hide_empty' => false,)); //Get all the terms

                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                // Check and see if the term is a top-level parent. If so, display it.
                                $parent = $term->parent;

                                if ( $parent=='0' ) {

                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                                    $term_url = get_term_link($term);
                        ?>

                            <a class="dropdown-item" href="<?php echo $term_url ?>">
                                <?php echo $term_name; ?>  
                            </a>
                        <?php
                                } 
                            }

                            //Revert to Previous Multisite
                            //restore_current_blog();
                        ?>
                    </div>
                </div>
            </div>
        </header>
        <section>
            <header class="container-wrapper padding-t-40 padding-b-20">
                <h2 class="article-header-lg">
                    <?php $category_name = single_term_title("", false); echo $category_name; ?>
                </h2>
            </header>
            <div class="container-wrapper">
                <article class="row row-40">
                <?php
                    /* Get Term Slug */
                    $queried_object = get_queried_object();
                    $term_slug = $queried_object->slug;
                    
                    $articleQuery = new WP_Query(
                        array(
                            'post_type' => 'story',
                            'showposts' => -1,
                            'tax_query' => array(
                                array(
                                    'taxonomy'  => 'magazine',
                                    'terms'     => $term_slug,
                                    'field'     => 'slug'
                                )
                            )
                        )
                    );
                    
                        while ($articleQuery->have_posts()) : $articleQuery->the_post();
                ?>

                    <div class="col-md-4 padding-lr-40 margin-b-30">
                        <a class="featured-article" href="<?php the_permalink() ?>">
                            <figure class="image-box">
                                <?php if (has_post_thumbnail()) { ?>

                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>" alt="">

                                <?php } else { ?>

                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.jpg" alt="">

                                <?php } ?>
                                
                                <?php if ( has_term( 'profile', 'content-type' ) ) {?>
                                    <figcaption class="caption-icon caption-video">
                                        PROFILE
                                    </figcaption>
                                <?php } elseif ( has_term( 'book-review', 'content-type' ) ) {?>
                                    <figcaption class="caption-icon caption-video">
                                        BOOK REVIEW
                                    </figcaption>
                                <?php } elseif ( has_term( 'feauture', 'content-type' ) ) {?>
                                    <figcaption class="caption-icon caption-video">
                                        FEATURE
                                    </figcaption>
                                <?php } elseif ( has_term( 'interview', 'content-type' ) ) {?>
                                    <figcaption class="caption-icon caption-video">
                                        INTERVIEW
                                    </figcaption>
                                <?php } ?>
                                                                
                            </figure>
                            <h4 class="title">
                                <?php echo the_title() ?>
                            </h4>
                            <p class="date">
                                <?php echo get_the_date( 'F j, Y' ); ?>
                            </p>
                        </a>
                    </div>
                    
                <?php endwhile;
                    // Reset things, for good measure
                    $services = null;
                    wp_reset_postdata();
                ?>
                </article>
            </div>
        </section>
        <!--<section class="container-wrapper padding-tb-40 border-tb-1 border-color-darkgrey">
            <div class="btn-wrapper text-center">
                <a class="btn btn-trans-green margin-r-40" href="">
                    Show More
                </a>
            </div>
        </section>-->
        <section class="container-wrapper bg-grey padding-tb-60">
            <h4 class="txt-xxlg txt-bold margin-b-20">
                ACCELERATE BLOG
            </h4>
            <ul class="big-list uppercase">
                <!-- Get All Magazine Terms -->
                <?php 

                    //Get Terms
                    $terms = get_terms( 'magazine', array('hide_empty' => false,)); //Get all the terms

                    foreach ($terms as $term) { //Cycle through terms, one at a time

                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        if ( $parent=='0' ) {

                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                            $term_url = get_term_link($term);
                ?>

                    <li>
                        <a href="<?php echo $term_url  ?>">
                            <?php echo $term_name; ?>   
                        </a>
                    </li>
                <?php
                        } 
                    }

                ?>
            </ul>
        </section>
    </main>
    
    <?php get_footer() ?>