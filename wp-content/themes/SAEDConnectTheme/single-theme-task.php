<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php $task_id = get_the_ID(); ?>

<main class="main-content">

    <figure 
        class="image-box"
        style="height:400px; background-image:url('https://www.saedconnect.org/young-leaders/wp-content/uploads/sites/104/2019/10/vawg.jpg')"
    ></figure>
    
    <div class="container-wrapper padding-tb-80">
        <div class="row">
            <article class="col-lg-8 mx-auto text-center">
                <h2 class="uppercase margin-b-10">
                    <?php 
                        $parent = get_post( get_post_meta( $task_id, 'parent_post', true ) );
                    ?>
                    <a class="txt-color-green txt-medium" href="<?php echo get_the_permalink($parent); ?>">
                        <i class="fa fa-angle-left"></i>
                        <span class="padding-l-10"><?php echo get_the_title($parent); ?></span>
                    </a>
                </h2>
                <h1 class="txt-2em txt-height-1-2 margin-b-20"><?php the_title() ?></h1>
                <div class="">
                    <p class="margin-b-20"><?php echo get_post_meta( $task_id, 'type', true ) ?></p>
                    <p class="margin-b-20">
                        <span class="txt-medium">Runs until</span>
                        <?php 
                            $date = strtotime(get_post_meta( $task_id, 'expiry_date', true ));
                            echo date('j F Y', $date);
                        ?>
                    </p>
                </div>
                <p><?php echo get_post_meta( $task_id, 'description', true ) ?></p>
                <div class="padding-t-40">
                    <a 
                        class="btn btn-green txt-sm no-m-b" 
                        data-toggle="modal" 
                        href="#addIdeaModal"
                    >Participate</a>
                </div>
            </article>
        </div>
    </div>

</main>

<!-- Add Idea Foem -->
<div class="modal fade font-main filter-modal" id="addIdeaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-addIdea'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        if( $_POST && $_GET['view'] == 'form-addIdea' ){

                            /* Get Post Name */
                            $post_name = sanitize_text_field( $_POST['post_name'] ); 

                            /* Meta */
                            $telephone = sanitize_text_field( $_POST['telephone'] );
                            $email = sanitize_text_field( $_POST['email'] );
                            $description = wp_kses_post( $_POST['description'] ); 
                            $location = sanitize_text_field( $_POST['location'] );

                            /* Save Post to DB */
                            $idea_id = wp_insert_post(array (
                                'post_type' => 'theme-task-response',
                                'post_title' => $post_name,
                                'post_content' => "",
                                'post_status' => 'publish',
                            ));

                            update_post_meta( $idea_id, 'parent_post', $task_id );

                            update_post_meta( $idea_id, 'telephone', $telephone );
                            update_post_meta( $idea_id, 'email', $email );
                            update_post_meta( $idea_id, 'description', $description );
                            update_post_meta( $idea_id, 'location', $location );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                        }
                    ?>

                    <h3 class="txt-xxlg txt-light padding-b-30">
                        Register for <?php echo $post_title ?>
                    </h3>

                    <div class="form margin-b-40">
                        <!-- Name -->
                        <div class="form-item">
                            <label for="post_name">Your Name</label>
                            <input 
                                type="text" 
                                name="post_name" 
                                required
                            >
                        </div>
                        <!-- Telephone -->
                        <div class="form-item">
                            <label for="telephone">Your Phone</label>
                            <input 
                                type="telephone" 
                                name="telephone" 
                                required
                            >
                        </div>
                        <!-- Email -->
                        <div class="form-item">
                            <label for="email">Your Contact Email</label>
                            <input 
                                type="email" 
                                name="email" 
                                required
                            >
                        </div>
                        <!-- Location -->
                        <div class="form-item">
                            <label for="location">What state are you based?</label>
                            <select name="location" id="location">
                            <?php 
                                switch_to_blog(110);
                                $terms = get_terms( 'state', array('hide_empty' => false));
                                restore_current_blog();

                                foreach( $terms as $term ){
                            ?>
                                <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                            <?php } ?>
                            </select>
                        </div>
                        <!-- Make a comment -->
                        <div class="form-item">
                            <label for="description">
                                Make a comment
                            </label>
                            <textarea name="description" rows="8" required><?php echo $description ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-green txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>