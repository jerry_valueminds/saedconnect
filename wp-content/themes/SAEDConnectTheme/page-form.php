    <?php /*Template Name: General Form Template*/ ?>
    
    <?php get_header() ?>
    <style type="text/css">
        .um-button, .um-button.um-alt{
            position: relative !important;
            outline: 0 !important;
            font-weight: 500 !important;
            border-radius: 20px !important;
            transition: all 0.4s ease !important;
        }

        
    </style>

        <main class="main-content font-main">
            <header class="container-wrapper bg-grey padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        eSAED Registration.
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        eSAED leverages technology to enable skills Acquisition & Entrepreneurship Development solutions for youths across Nigeria. Get connected to a wide range of skills development, personal growth, business growth and earning opportunities when you subscribe.
                    </h2>
                </div>
            </div>
        </header>
        <?php
            // TO SHOW THE PAGE CONTENTS
            while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
            
           <section class="padding-tb-40">
               <div class="container">
                   <div class="col-md-10 mx-auto border-o-1 border-color-darkgrey padding-tb-20 padding-lr-30 esaed-form">
                       <?php the_content(); ?>
                   </div>
                   <?php
                                        
            //echo do_shortcode( "[stickylist id='45' ]" );


            $form_id = 45;  // Get eSAED Registration Form ID

            // Prepare Search Parameters
            $search_criteria = array(
                'status'        => 'active',
                'field_filters' => array(
                    'created_by' => 1
                )
            );

            // Search for entries for this User
            $entries = GFAPI::get_entries( $form_id, $search_criteria );
            //var_dump($search_criteria);
            var_dump($entries);

        ?>
               </div>
           </section>
        <?php
            endwhile; //resetting the page loop
            wp_reset_query(); //resetting the page query
        ?>
       

        
    </main>
        
    <?php get_footer() ?>