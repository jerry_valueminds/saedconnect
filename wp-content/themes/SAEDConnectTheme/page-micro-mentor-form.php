<?php /*Template Name: Micro Mentor Form*/ ?>
   

<?php get_header() ?>

<?php
    // TO SHOW THE PAGE CONTENTS
    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
?>
    
    <main class="main-content">
       
        <section class="container-wrapper padding-t-80 padding-b-40 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        SAEDConnect MicroMentor Information Form
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        <p>
                            The SAEDConnect Micro-Mentor programme provides an opportunity for upcoming business practitioners in a particular field of business to morderate an online community where people who are interested in starting or growing that business can come to ask questions and get answers. The Micro-mentor will be responsible for connecting with established entrepreneurs in that business to find answers to questions and extract useful information, verify genuineness of materials and content sent directly to you or posted on the forum. You are here because you have been identified as potentially capable of being a micro-mentor. Kindly complete the form below comprehensively as it will help us objectively appraise your capacity as a micro mentor.
                        </p>
                    </h2>
                </div>
            </div>
        </section>
        
        <style>
            
            .gform_body{
                border: 0 !important;
            }
            
            .gform_wrapper .top_label .gfield_label {
                font-size: 0.8rem !important;
                margin-bottom: 0 !important;
            }
            
            .ginput_container input, .ginput_container select, .ginput_container textarea{
                width: 100% !important;
                font-size: 0.9rem !important;
                padding: 0.4rem 0.8rem !important;
            }
            
            .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
                margin-top: 0px !important;
            }
            
            .gform_button{
                text-align: center;
                margin: 0 !important;
                margin-bottom: 20px !important;
            }
            
            .gform_footer{
                display: flex !important;
                flex-direction: column;
                justify-content: center !important;
                align-items: center !important;
            }
            
    
        </style>
        
        <section class="container-wrapper bg-grey padding-t-60 padding-b-40" id="get-started">
            <div class="row">
                <div class="col-md-8 mx-auto esaed-form">
                    <?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
                </div>
            </div>
        </section>

    </main>
    
<?php
    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
?>

<?php get_footer() ?>
