<?php /*Template Name: Homepage*/ ?>

<?php
                    
    /* On form submission, show confirmation */
    if($_GET['page'] == 'gf_activation') {
        // If User is Logged in, redirect to User Dashbord
        $activtion_page = get_site_url().'/account-activated';

        if ( wp_redirect( $activtion_page ) ) {
            exit;
        }
    }

?>    
    

<!DOCTYPE html>
<html lang="en-US">

<?php get_header('home-page') ?>

<body>
    <div class="homepage-hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image%20(1).jpg'); height:100vh;">
        <div class="content">
            <header class="main-navigation home-page font-main" id="myHeader">
                <div class="navigation-wrapper justify-content-center padding-b-15">
                    <a class="brand" href="https://www.saedconnect.org/">
                        <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                        <span class="name">SAEDConnect</span>
                        <span class="txt-sm txt-color-dark padding-l-10 margin-l-10 border-l-1">
                            In Partnership
                            <br>
                            with the NYSC
                        </span>
                    </a>
                </div>
            </header>

            <main class="main-content-home">
                <header class="container-wrapper txt-color-white padding-b-40 text-center">
                    <div class="row">
                        <div class="col-md-6 mx-auto">
                            <h1 class="txt-3em txt-medium txt-height-1-2 margin-b-20">
                                <span class="d-block">
                                    Make Progress
                                </span>
                                <span class="d-block">
                                    Towards Your Dreams    
                                </span>
                            </h1>
                            <h2 class="txt-height-1-4">
                                SAEDConnect is a youth empowerment accelerator providing solutions that enable you to discover your potential, activate your employability & entrepreneurial capacity and connect you to opportunities.
                            </h2>
                        </div>
                    </div>
                </header>
                <section class="container-wrapper padding-b-40 text-center">
                    <div class="row">
                        <div class="col-md-3 mx-auto">
                            <div class="padding-lr-20 margin-b-20">
                                <a href="https://www.saedconnect.org/about/synopsis/find-your-path/" class="btn btn-trans-wb full-width no-m-b">
                                    Find your Path
                                </a>
                            </div>
                            <div class=" padding-lr-20 margin-b-10">
                                <a href="https://www.saedconnect.org/start-your-business" class="btn btn-trans-wb full-width no-m-b">
                                    Start / Grow your Business
                                </a>
                            </div>
                            <div class="padding-lr-20 margin-b-10">
                                <a href="https://www.saedconnect.org/get-your-job" class="btn btn-trans-wb full-width no-m-b">
                                    Get your Job
                                </a>
                            </div>
                            <div class="padding-lr-20 margin-b-20">
                                <a href="https://www.saedconnect.org/opportunity-center/" class="btn btn-trans-wb full-width no-m-b">
                                    Opportunity Center
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <?php
                    
                        if ( !is_user_logged_in() ) {
                            $current_user = wp_get_current_user();
                    ?>
                       
                        <div class="col-md-3 padding-lr-20 mx-auto">
                            <div class="row row-5">
                                <div class="col-6 padding-lr-5">
                                    <a href="https://www.saedconnect.org/register" class="btn btn-blue full-width">
                                        Sign Up
                                    </a>
                                </div>
                                <div class="col-6 padding-lr-5">
                                    <a href="https://www.saedconnect.org/login" class="btn btn-green full-width">
                                        Login
                                    </a>
                                </div>
                            </div>
                        </div> 
                        
                    <?php } else { ?> 
                    
                        <div class="col-md-3 padding-lr-20 mx-auto padding-lr-20 margin-b-20">
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard" class="btn btn-blue full-width no-m-b">
                                Go to Dashboard
                            </a>
                        </div>
                    
                    <?php } ?>
                    </div>
                </section>
            </main>
        </div>  
    </div>     
    <!--Load Scripts-->
	<?php wp_footer(); ?>
</body>
</html>