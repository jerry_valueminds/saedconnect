<?php /*Template Name: Track Confirmation*/ ?> 
  

<?php get_header() ?>
   
<?php while ( have_posts() ) : the_post(); ?>
    <main class="main-content">
        <section class="container-wrapper bg-white padding-tb-80">
            <div class="row">
                <div class="col-md-7 mx-auto">
                    <h1 class="txt-2-2em txt-medium margin-b-40">
                        Your registration was successful
                    </h1>
                    <article class="text-box">
                        <?php the_content() ?>
                    </article>
                    <div class="text-center margin-t-80">
                        <a href="#" class="btn btn-trans-blue" onclick="goBack()">
                            <i class="fa fa-arrow-left"></i>
                            <span class="padding-l-10">
                                Go Back
                            </span>
                        </a>
                        <a href="https://www.saedconnect.org/tei-mentor-hub/" class="btn btn-blue">
                            <i class="fa fa-home"></i>
                            <span class="padding-l-10">
                                Go to Homepage
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    
<?php endwhile; ?>
    
<?php get_footer() ?>