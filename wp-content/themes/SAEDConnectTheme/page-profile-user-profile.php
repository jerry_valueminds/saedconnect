<?php /*Template Name: Profile - User*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* Publication */
        $publication_key   = 'publication_status';
        $accreditation_key   = 'nysc_accreditation_status';
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Get Avatar */
        $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
        $meta_key = 'user_avatar_url';
        $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

        if($get_avatar_url){
            $avatar_url = $get_avatar_url;
        }

        /* Get Trainer profile */
        $user_id = $_GET['user-id'];
    ?>
    
    
    <?php get_header() ?>
    
    <main class="main-content txt-color-light bg-white">
        <?php get_template_part( 'template-parts/user-dashboard/_dashboard_strip' ); ?>
        <section class="row">
        <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav-user-profile' ); ?>
            <div class="dashboard-multi-main-content">
                <div class="page-header">
                    <h1 class="page-title">
                        My CV
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Your Personal Profile is the Key information about you which would be required by potential employers
                    </p>
                </article>
                <!-- Basic Information -->
                <div class="section-wrapper">
                <?php
                    switch_to_blog(1);

                    $gf_id = 4; //Form ID
                    $gv_id = 1445; //Gravity View ID
                    $title = 'Basic Information';

                    $entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $user_id, //Current logged in user
                            )
                        )
                    );

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria ); 
                ?>

                    <?php if(!$entry_count){ //If no entry ?>

                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>
                            <div class="entry">
                                <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                    Input basic information about yourself like your name, gender, year of birth, etc.
                                </h3>
                            </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>
                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Name          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Username          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Email         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 2 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Phone         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 7 ); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                    <?php } ?>

                    <?php restore_current_blog(); ?>
                </div>

                <!-- Social Details -->
                <div class="section-wrapper">
                <?php
                    switch_to_blog(109);

                    $gf_id = 28; //Form ID
                    $gv_id = 1123; //Gravity View ID
                    $title = 'Social Details';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $user_id, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>


                    <?php if(!$entry_count){ //If no entry ?>
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                No information available.
                            </h3>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Email          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 11 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Phone          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 12 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Address          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 13 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Current State of Residence
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 14 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Personal Blog/Website          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 15 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Facebook         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 16 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Twitter         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 17 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Instagram
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 18 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Linkedin          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 19 ); ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                    <?php } ?>

                    <?php restore_current_blog(); ?>
                </div>
            </div>
            <div class="dashboard-multi-main-sidebar">
               
                <?php if( is_user_logged_in() ){ ?>
                    <div class="margin-t-20">
                        <a data-toggle="modal" href="#messageModal" class="btn btn-blue w-100 txt-normal-s">
                           <span class="padding-r-5">Contact</span>
                           <i class="fa fa-envelope-o"></i>
                        </a>
                    </div>
                <?php } ?>
                
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>

<!-- iCheck -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/_all.css" integrity="sha256-808LC4rdK/cu4RspCXPGrLKH7mgCcuNspF46UfBSbNQ=" crossorigin="anonymous" />

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>