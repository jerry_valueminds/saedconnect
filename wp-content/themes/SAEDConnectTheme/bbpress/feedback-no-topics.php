<?php

/**
 * No Topics Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="container-wrapper padding-t-40">
    <!--<div class="bbp-template-notice">-->
    <div class="">
        <p class="txt-color-dark">
            <?php _e( 'No topics found!', 'bbpress' ); ?>
        </p>
    </div>
</div>