<?php

/**
 * Forums Loop - Single Forum
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="row row-15 forum-list" id="bbp-topic-<?php bbp_topic_id(); ?>">
    <div class="col-md-6 padding-lr-15">
        <h3 class="txt-height-1-2">
            <?php if ( bbp_is_user_home() && bbp_is_subscriptions() ) : ?>

                <span class="bbp-row-actions">

                    <?php do_action( 'bbp_theme_before_forum_subscription_action' ); ?>

                    <?php bbp_forum_subscription_link( array( 'before' => '', 'subscribe' => '+', 'unsubscribe' => '&times;' ) ); ?>

                    <?php do_action( 'bbp_theme_after_forum_subscription_action' ); ?>

                </span>

            <?php endif; ?>

            <?php do_action( 'bbp_theme_before_forum_title' ); ?>

            <a class="bbp-forum-title txt-color-white" href="<?php bbp_forum_permalink(); ?>">
                <?php bbp_forum_title(); ?>
            </a>

            <?php do_action( 'bbp_theme_after_forum_title' ); ?>

            <?php do_action( 'bbp_theme_before_forum_description' ); ?>

            <!--<div class="bbp-forum-content txt-sm margin-t-20 txt-height-1-5 txt-color-white">
                <?php // bbp_forum_content(); ?>
            </div>-->

            <?php do_action( 'bbp_theme_after_forum_description' ); ?>

            <?php do_action( 'bbp_theme_before_forum_sub_forums' ); ?>

            <?php //bbp_list_forums(); ?>

            <?php do_action( 'bbp_theme_after_forum_sub_forums' ); ?>

            <?php bbp_forum_row_actions(); ?>
        </h3>
        
        <p class="txt-sm d-none">
            <?php do_action( 'bbp_theme_before_topic_started_by' ); ?>

            <span class="">
                <?php printf( __( '%1$s', 'bbpress' ), bbp_get_topic_author_link( array( 'size' => '14' ) ) ); ?>
            </span>


            <?php do_action( 'bbp_theme_after_topic_started_by' ); ?>

            <?php if ( !bbp_is_single_forum() || ( bbp_get_topic_forum_id() !== bbp_get_forum_id() ) ) : ?>

                <?php do_action( 'bbp_theme_before_topic_started_in' ); ?>

                <span class="bbp-topic-started-in"><?php printf( __( 'in: <a href="%1$s">%2$s</a>', 'bbpress' ), bbp_get_forum_permalink( bbp_get_topic_forum_id() ), bbp_get_forum_title( bbp_get_topic_forum_id() ) ); ?></span>

                <?php do_action( 'bbp_theme_after_topic_started_in' ); ?>

            <?php endif; ?>

        </p>
        
       <!-- <p>
            
            
            <span class="author padding-r-10">
                Jerry Adaji
            </span>
            <span class="time txt-color-light">
                18 Feb 2018, 17:40:53
            </span>
        </p>-->
    </div>
    <div class="col-md-2 d-none d-md-block"></div>
    <div class="col-md-2 d-none d-md-block padding-lr-15 txt-normal-s">
        <?php do_action( 'bbp_theme_before_forum_freshness_link' ); ?>

        <div class="freshness-link">
		    <?php bbp_forum_freshness_link(); ?>
        </div>

		<?php do_action( 'bbp_theme_after_forum_freshness_link' ); ?>

		<!--<p class="bbp-topic-meta margin-t-5">
			<?php do_action( 'bbp_theme_before_topic_author' ); ?>

            <span class="bbp-topic-freshness-author">
			    <?php bbp_author_link( array( 'post_id' => bbp_get_forum_last_active_id(), 'size' => 14 ) ); ?>
            </span>

            
            <?php do_action( 'bbp_theme_after_topic_author' ); ?>
		</p>-->
    </div>
    <div class="col-md-1 d-none d-md-block padding-lr-15 text-center txt-sm">
        <?php bbp_forum_topic_count(); ?>
    </div>
    <div class="col-md-1 d-none d-md-block padding-lr-15 text-center txt-sm">
        <?php bbp_show_lead_topic() ? bbp_forum_reply_count() : bbp_forum_post_count(); ?>
    </div>
</div>
