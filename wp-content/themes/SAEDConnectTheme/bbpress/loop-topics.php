<?php

/**
 * Topics Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_topics_loop' ); ?>

<!--<ul id="bbp-forum-<?php bbp_forum_id(); ?>" class="bbp-topics">-->
<div>
    <!-- Header -->
    
                
                
    

<div class="container-wrapper padding-t-20">

	<div class="overflow-hidden">

		<div class="row row-15 txt-normal-s txt-bold padding-b-20 margin-b-20 border-b-1 border-color-grey">
			<div class="col-9 col-md-8 padding-lr-15">
                <?php _e( 'Topics', 'bbpress' ); ?>
            </div>
            <div class="d-none d-md-block col-3 col-md-2 padding-lr-15">
                <?php _e( 'Activity', 'bbpress' ); ?>
            </div>
            <div class="col-md-1 d-none d-md-block text-center padding-lr-15 text-center">
                <?php _e( 'Voices', 'bbpress' ); ?>
            </div>
            <div class="col-2 col-md-1 text-center padding-lr-15 text-center">
                <?php _e( 'Replies', 'bbpress' ); ?>
            </div>
        </div>

	</div>
</div>
	
<div class="container-wrapper">
    <?php while ( bbp_topics() ) : bbp_the_topic(); ?>

        <?php bbp_get_template_part( 'loop', 'single-topic' ); ?>

    <?php endwhile; ?>
</div>

<div class="container-wrapper">
    <!--<div class="row">
        <p>
            <span class="td colspan<?php echo ( bbp_is_user_home() && ( bbp_is_favorites() || bbp_is_subscriptions() ) ) ? '5' : '4'; ?>">
                &nbsp;
            </span>
        </p>
    </div>-->
</div>


<?php do_action( 'bbp_template_after_topics_loop' ); ?>
