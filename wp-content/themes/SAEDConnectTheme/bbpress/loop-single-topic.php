<?php

/**
 * Topics Loop - Single
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="row row-15 forum-list" id="bbp-topic-<?php bbp_topic_id(); ?>">
    <div class="col-9 col-md-8 padding-lr-15">
        <h3 class="txt-height-1-2 txt-color-dark">

                    <?php if ( bbp_is_user_home() ) : ?>

                        <?php if ( bbp_is_favorites() ) : ?>

                            <span class="bbp-row-actions">

                                <?php do_action( 'bbp_theme_before_topic_favorites_action' ); ?>

                                <?php bbp_topic_favorite_link( array( 'before' => '', 'favorite' => '+', 'favorited' => '&times;' ) ); ?>

                                <?php do_action( 'bbp_theme_after_topic_favorites_action' ); ?>

                            </span>

                        <?php elseif ( bbp_is_subscriptions() ) : ?>

                            <span class="bbp-row-actions">

                                <?php do_action( 'bbp_theme_before_topic_subscription_action' ); ?>

                                <?php bbp_topic_subscription_link( array( 'before' => '', 'subscribe' => '+', 'unsubscribe' => '&times;' ) ); ?>

                                <?php do_action( 'bbp_theme_after_topic_subscription_action' ); ?>

                            </span>

                        <?php endif; ?>

                    <?php endif; ?>

                    <?php do_action( 'bbp_theme_before_topic_title' ); ?>

                    <a class="bbp-topic-permalink" href="<?php bbp_topic_permalink(); ?>"><?php bbp_topic_title(); ?></a>

                    <?php do_action( 'bbp_theme_after_topic_title' ); ?>

                    <?php bbp_topic_pagination(); ?>

                    <?php do_action( 'bbp_theme_before_topic_meta' ); ?>



                    <?php // do_action( 'bbp_theme_after_topic_meta' ); ?>

                    <?php bbp_topic_row_actions(); ?>

                </h3>
            
        
       <!-- <p>
            
            
            <span class="author padding-r-10">
                Jerry Adaji
            </span>
            <span class="time txt-color-light">
                18 Feb 2018, 17:40:53
            </span>
        </p>-->
    </div>
    <div class="d-none d-md-block col-3 col-md-2 padding-lr-15">
        <div class="freshness-link">
		    <?php bbp_reply_post_date(0, true); ?>
        </div>
    </div>
    <div class="col-md-1 d-none d-md-block padding-lr-15 text-center">
        <?php bbp_topic_voice_count(); ?>
    </div>
    <div class="col-2 col-md-1 padding-lr-15 replies-section text-center">
            <?php bbp_show_lead_topic() ? bbp_topic_reply_count() : bbp_topic_post_count(); ?>
        <!--<p class="txt-sm txt-color-light">
            3 days ago
        </p>-->
    </div>
</div>

<!--
<ul <?php bbp_topic_class(); ?>>


	<li class="bbp-topic-voice-count"><?php bbp_topic_voice_count(); ?></li>

	<li class="bbp-topic-reply-count"></li>

	<li class="bbp-topic-freshness">

		<?php do_action( 'bbp_theme_before_topic_freshness_link' ); ?>

		<?php bbp_topic_freshness_link(); ?>

		<?php do_action( 'bbp_theme_after_topic_freshness_link' ); ?>

		<p class="bbp-topic-meta">

			<?php do_action( 'bbp_theme_before_topic_freshness_author' ); ?>

			<span class="bbp-topic-freshness-author"><?php bbp_author_link( array( 'post_id' => bbp_get_topic_last_active_id(), 'size' => 14 ) ); ?></span>

			<?php do_action( 'bbp_theme_after_topic_freshness_author' ); ?>

		</p>
	</li>

</ul> #bbp-topic-<?php bbp_topic_id(); ?> -->
