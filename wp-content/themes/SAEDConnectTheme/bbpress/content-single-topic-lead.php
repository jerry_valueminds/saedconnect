<?php

/**
 * Single Topic Lead Content Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php
    /* Viewable Forums Without Login */
    $viewable_forums = array(
        33 //Help Center
    );

    $blog_id = get_current_blog_id();

    /* Check if User is logged in */              
    if ( is_user_logged_in() || in_array($blog_id, $viewable_forums) ) {
?>

    <?php do_action( 'bbp_template_before_lead_topic' ); ?>
    <header class="overview-header container-wrapper bg-white">
        <div class="row padding-t-20 margin-b-40">
            <div class="col-md-3 padding-b-15">
            <?php
                /* Get Forum Category */
                $forum_category = rwmb_meta( 'forum_category', array( 'object_type' => 'setting' ), 'my_options' );

                /* Disable Back Button */
                $disable_back_btn_list = array('_tei_modal', '_job_advisor_modal');
                                                                        
                if( !in_array($forum_category, $disable_back_btn_list) ){
            ?>
                <a href="<?php echo get_permalink( $post->post_parent ); ?>" class="txt-lg" onclick="goBack()">
                    <i class="fa fa-chevron-left padding-r-10"></i>
                    Back
                </a>
            <?php } ?>
            </div>
            
            <div class="col-md-6">
                <h1 class="txt-xlg txt-medium txt-height-1-2 padding-b-20">
                    <?php the_title() ?>
                </h1>
                <article class="padding-b-20">
                    <p class="txt-normal-s d-flex txt-color-lighter">
                        <span class="author padding-r-30">
                            <i class="fa fa-eye padding-r-5"></i>
                            <?php bbp_topic_voice_count(); ?>
                        </span>
                        <span class="author padding-r-10">
                            <i class="fa fa-comment-o padding-r-5"></i>
                            <?php bbp_topic_reply_count() ?>
                        </span>
                    </p>
                </article>
                <article class="text-box sm txt-color-light">
                    <?php do_action( 'bbp_theme_before_topic_content' ); ?>

                    <div class="topic-content">
                        <?php bbp_topic_content(); ?>
                    </div>

                    <?php do_action( 'bbp_theme_after_topic_content' ); ?>
                </article>
            </div>
        </div>
        <div class="row padding-b-20">
            <!--<div class="col-md-3">
                <?php bbp_topic_subscription_link(); ?>

                 <?php bbp_topic_favorite_link(); ?>

                <a class="txt-light" href="forum_single.html">
                    <i class="fa fa-arrow-left"></i>
                    <span class="padding-l-10">
                        Back to Topic Name
                    </span>
                </a>
            </div>-->

            <style>
                .bbp-topic-author{
                    display: flex;

                }

                .bbp-topic-author img{
                    width: 18px;
                    height: 18px;
                    border-radius: 50%;
                    display: none;
                }

                .bbp-topic-author .bbp-author-role{
                    font-weight: 500;
                }

                .bbp-admin-links{
                    float: none !important;
                }
            </style>

            <div class="col-md-6 mx-auto">
                <article class="margin-b-20">
                    <div class="txt-normal-s d-flex">
                        <span class="started-by txt-color-light padding-r-10">
                            Started by
                        </span>
                        <span class="bbp-topic-author padding-r-10 txt-color-dark">

                            <?php bbp_topic_author_link( array( 'sep' => '<span class="padding-r-5"></span>', 'show_role' => false ) ); ?>
                        </span>
                        <span class="padding-r-10">
                            on
                        </span>
                        <span class="time txt-color-light">
                            <?php bbp_topic_post_date(); ?>
                        </span>
                    </div>
                </article>
                <article class="">
                    <div class="bbp-meta">

                        <!--<a href="<?php bbp_topic_permalink(); ?>" class="bbp-topic-permalink">#<?php bbp_topic_id(); ?></a>-->

                        <?php do_action( 'bbp_theme_before_topic_admin_links' ); ?>

                        <?php bbp_topic_admin_links(); ?>

                        <?php do_action( 'bbp_theme_after_topic_admin_links' ); ?>

                    </div>
                </article>
            </div>
        </div>
    </header>
    
    
    
    
    

    <?php if ( !bbp_has_replies() ) : ?>
    <section class="bg-grey padding-tb-40 topic">
        <div class="container-wrapper">
            <div class="row">

                <div class="col-md-6 mx-auto">
                    <a class="btn btn-blue txt-sm no-m-b" data-toggle="collapse" href="#topic-form-2" role="button" aria-expanded="false" aria-controls="topic-form-2">
                        Reply to Topic
                    </a>

                    <div class="collapse" id="topic-form-2">
                        <div class="margin-t-20 margin-b-15">
                        <?php 
                            if( is_user_logged_in() ){

                                bbp_get_template_part( 'form', 'reply' );

                            } else { 
                        ?>
                            <p class="txt-normal-s margin-b-10">
                                You must be logged in, in order to post a reply.
                                <a href="https://www.saedconnect.org/login" class="txt-medium txt-color-blue">
                                    Login
                                </a>
                            </p>
                        <?php
                            } 
                        ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php endif; ?>

    <?php do_action( 'bbp_template_after_lead_topic' ); ?>

    <script>
    $(document).ready(function(){
        $('.topic-content').collapser({
            mode: 'words',
            truncate: 30,
            showText: 'Show More',
            hideText: 'Show Less',
            effect: 'slide',
            changeText: 1,
            lockHide: false,
            controlBtn: 'txt-color-blue txt-medium',
        });

    });
    </script>

<?php } else { ?>
   
    <style>
        .gfield_checkbox label, .login-form .gfield_label{
            display: block !important;
        }
        
        .gfield_checkbox li{
            display: flex;
            align-items: center;
        }
        
        .gfield_checkbox input{
            margin: 0 !important;
            margin-right: 10px !important;
        }
    </style>
    
    <section class="container-wrapper text-center topic">
        <div class="row">
            <div class="col-md-4 mx-auto">
                <h1 class="txt-2em txt-medium margin-b-20">
                    <?php the_title() ?>
                </h1>
                
                <?php
                    switch_to_blog(1);
                ?>
                
                <div class="form-toggle">
                    <!-- Login Tab -->
                    <div class="login">
                        <p class="margin-b-40">
                            You need to login to view this topic. Don't have an account?                                
                            <a class="toggle-btn txt-color-blue" href="#">
                                Click Here to get started.
                            </a>
                        </p>
                        <div class="login-form">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                            <?php endif;?>
                        </div>
                    </div>
                    
                    <!-- Signup Tab -->
                    <div class="register">
                        <p class="margin-b-40">
                            You need to login to view this topic. Already have an account?
                            <a class="toggle-btn txt-color-blue" href="#">
                                Click here Login.
                            </a>
                        </p>
                        <div class="login-form">
                            <?php
                                echo do_shortcode( "[gravityform id='4' title='false' description='false' ajax='false']"); 
                            ?>
                        </div>
                    </div>
                </div>
                
                <?php
                    //Revert to Previous Multisite
                    restore_current_blog();
                ?>
                
            </div>
        </div>
    </section>
    <style>
        .register{
            display: none;
        }
    </style>
    
    <script>
        $(document).ready(function() {
            function ButtonText() {
                if ($(".form-toggle .login").is(":visible")) {
                    $(".form-toggle .toggle-btn").text("Click Here to get started.");
                } else {
                    $(".form-toggle .toggle-btn").text("Click here Login.");
                }
            }

            ButtonText();

            $(".form-toggle .toggle-btn").click(function () {
                $(".form-toggle .login").toggle();
                $(".form-toggle .register").toggle();
                ButtonText();
            });
        });
    </script>

<?php } ?>
