<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

?>


<div class="margin-b-30 padding-b-10 border-b-1 border-color-darkgrey">
    <div class="">
        <h3 class="txt-medium margin-b-15 d-flex justify-content-between">
            <div class="">
                <?php do_action( 'bbp_theme_before_reply_author_details' ); ?>

                <span class="txt-normal-s">
                    <?php bbp_reply_author_link( array( 'sep' => '<br />', 'show_role' => false, 'type' => 'name' ) ); ?>
                </span>

                <span class="txt-sm txt-color-lighter">
                   on <?php bbp_reply_post_date(); ?>
                </span>
            </div>
            <!--<div class="dropdown">
                <button class="reply-action txt-sm txt-color-lighter" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-h"></i>
                    <i class="fa fa-chevron-down"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <div class="edit-links">
                        <?php bbp_reply_admin_links(); ?>
                    </div>
                </div>
            </div>-->
        </h3>

        <article class="text-box sm txt-color-light">
            <?php do_action( 'bbp_theme_before_reply_content' ); ?>

            <?php bbp_reply_content(); ?>

            <?php do_action( 'bbp_theme_after_reply_content' ); ?>
        </article>
        <article class="reply-wrapper txt-sm txt-medium margin-t-20 row row-5">
            <div class="col-2">
                <?php echo bbp_get_reply_to_link() ?>
            </div>
            <div class="col-2">
                <?php echo bbp_reply_edit_link() ?>
            </div>
            <!--<div class="col-2">
                <?php echo bbp_reply_trash_link() ?>
            </div>
            <div class="col-2">
                <?php echo bbp_reply_spam_link() ?>
            </div>-->
        </article>
    </div>
</div>

<!--<div class=" margin-b-40">
    <div id="post-<?php bbp_reply_id(); ?>" class="bbp-reply-header">

        <div class="bbp-meta">

            <?php if ( bbp_is_single_user_replies() ) : ?>

                <span class="bbp-header">
                    <?php _e( 'in reply to: ', 'bbpress' ); ?>
                    <a class="bbp-topic-permalink" href="<?php bbp_topic_permalink( bbp_get_reply_topic_id() ); ?>"><?php bbp_topic_title( bbp_get_reply_topic_id() ); ?></a>
                </span>

            <?php endif; ?>

            <?php do_action( 'bbp_theme_before_reply_admin_links' ); ?>

            <?php bbp_reply_admin_links(); ?>

            <?php do_action( 'bbp_theme_after_reply_admin_links' ); ?>

        </div>

    </div>
</div>-->

