<?php
    /* Check if User is logged in */              
    if ( is_user_logged_in() ) {
?>


<?php

/**
 * Replies Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_replies_loop' ); ?>


<!-- Loop Comment -->
<?php if ( bbp_thread_replies() ) : ?>

    <?php bbp_list_replies(); ?>

<?php else : ?>

    <?php while ( bbp_replies() ) : bbp_the_reply(); ?>

        <?php bbp_get_template_part( 'loop', 'single-reply' ); ?>

    <?php endwhile; ?>

<?php endif; ?>



<!--<div id="topic-<?php bbp_topic_id(); ?>-replies" class="forums bbp-replies">

	<li class="bbp-header">

		<div class="bbp-reply-author"><?php  _e( 'Author',  'bbpress' ); ?></div>

		<div class="bbp-reply-content">

			<?php if ( !bbp_show_lead_topic() ) : ?>

				<?php _e( 'Posts', 'bbpress' ); ?>

				<?php bbp_topic_subscription_link(); ?>

				<?php bbp_user_favorites_link(); ?>

			<?php else : ?>

				<?php _e( 'Replies', 'bbpress' ); ?>

			<?php endif; ?>

		</div>

	</li>

</div>-->

<?php do_action( 'bbp_template_after_replies_loop' ); ?>

<?php } ?>
