<?php

/**
 * Forums Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_forums_loop' ); ?>

<div class="container-wrapper padding-t-20 bg-ash txt-color-white">
	<div class="overflow-hidden">
		<div class="row row-15 txt-normal-s txt-bold padding-b-20 margin-b-20 border-b-1 border-color-grey">
			<div class="col-md-6 padding-lr-15">
                <?php _e( 'Sub Forums', 'bbpress' ); ?>
            </div>
            <div class="col-md-2 d-none d-md-block"></div>
            <div class="col-md-2 d-none d-md-block padding-lr-15">
                <?php _e( 'Last Activity', 'bbpress' ); ?>
            </div>
            <div class="col-md-1 d-none d-md-block padding-lr-15 text-center">
                <?php _e( 'Topics', 'bbpress' ); ?>
            </div>
            <div class="col-md-1 d-none d-md-block padding-lr-15 text-center">
                <?php bbp_show_lead_topic() ? _e( 'Replies', 'bbpress' ) : _e( 'Posts', 'bbpress' ); ?>
            </div>
            
        </div>
	</div>
</div>

<div class="container-wrapper bg-ash txt-color-white padding-b-20">
	<div class="overflow-hidden">
       <?php while ( bbp_forums() ) : bbp_the_forum(); ?>

			<?php bbp_get_template_part( 'loop', 'single-forum' ); ?>

		<?php endwhile; ?>
    </div>
</div>

<!--<ul id="forums-list-<?php bbp_forum_id(); ?>" class="bbp-forums">



	<li class="bbp-footer">

		<div class="tr">
			<p class="td colspan4">&nbsp;</p>
		</div>

	</li> 

</ul>--><!-- .forums-directory -->

<?php do_action( 'bbp_template_after_forums_loop' ); ?>
