<?php /*Template Name: Profile - Trainer*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <header class="dashboard-main-nav font-main ">
        <div class="container-wrapper">
            <div class="wrapper">
                <a class="brand" href="https://www.saedconnect.org/">
                    <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                    <span class="name">My SAEDConnect</span>
                </a>                
                <nav class="nav-box">
                    <nav class="navigation-list-wrapper left">
                        <ul class="navigation-list">
                        <?php
                            $current_user = wp_get_current_user();
                            $user_roles = $current_user->roles;

                            if($user_roles){
                                if ( !in_array('bbp_moderator', $user_roles) ) {
                        ?>
                            <li>
                                <a href="<?php echo $base_url.'/?action=entrepreneurship-dashboard' ?>">
                                    Entrepreneurship
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="sub-menu txt-color-white">
                                    <div class="sub-menu-content">
                                        <div class="row row-40">
                                            <div class="col-md-4 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        My Side Hustle Communuties
                                                    </div>
                                                </h4>
                                                
                                                <?php
                                                    $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'value' => $current_user->id );

                                                    $entries = GFAPI::get_entries( 21, $search_criteria ); 
                                                    //$entries = GFAPI::get_entries( 211, $search_criteria ); 

                                                    /*  */
                                                    $exclude_list = array();
                                                ?>

                                                <!--Using Get entries-->
                                                <?php                               
                                                    /* Loop Through Entries */
                                                    if($entries){
                                                ?>
                                                <div class="row row-15">
                                                <?php
                                                    foreach($entries as $entry){
                                                            /* Get Entry ID */
                                                            $entry_id = $entry['id'];

                                                            /* Get Post attached to entry */
                                                            //$post = get_post( $entry['post_id'] );

                                                            /* Generate Edit Entry Link */
                                                            $edit_entry_link = do_shortcode('[gv_entry_link action="edit" entry_id="'.$entry_id.'" view_id="'.$gv_id.'" return="url" /]');

                                                            $post_title = rgar( $entry, '8' );

                                                            /* If has an entry, Add to Exclude list */
                                                            array_push($exclude_list, $post_title);

                                                            $post = get_page_by_title($post_title, OBJECT, 'information-session');

                                                            $images = rwmb_meta( 'info-session-feautured-image', array( 'limit' => 1 ), $post->ID );

                                                            $image = reset( $images );    
                                                ?>
                                                    
                                                    <div class="col-6 padding-lr-10 padding-b-10">
                                                        <div class="d-flex">
                                                            <i class="fa fa-arrow-right padding-r-5 txt-sm"></i>
                                                            <div class="flex_1">
                                                                <h4 class="sub-menu-title">
                                                                    <a class="" href="<?php echo get_the_permalink() ?>">
                                                                        <?php echo $post->post_title ?>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php } ?>
                                                </div>
                                                
                                            <?php } else { ?>
                                                
                                                <div class="txt-sm">
                                                    <p class="">
                                                        You have not joined any community.
                                                    </p>
                                                    <p class="margin-t-20">
                                                        <a href="" class="btn btn-blue txt-xs">
                                                            Join a Community
                                                        </a>
                                                    </p>
                                                </div>
                                                
                                            <?php } ?>
                                            </div>
                                            <div class="col-md-3 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        The Entrepreneurship Incubator
                                                    </div>
                                                </h4>
                                                <?php // Display posts
                                                    $wp_query = new WP_Query( array(
                                                        'relationship' => array(
                                                            'id'   => 'program_to_track',
                                                            'from' => 632, // You can pass object ID or full object
                                                        ),
                                                        'nopaging' => true,
                                                    ) );
                                                    while ( $wp_query->have_posts() ) : $wp_query->the_post();

                                                            $program_id = $post->ID;    //Get Program 

                                                            //  Get Program Featured Image
                                                            $images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ), $post->ID );

                                                            $image = reset( $images );

                                                ?>
                                                
                                                <div class="padding-b-15 d-flex">
                                                    <i class="fa fa-arrow-right padding-r-5 txt-sm"></i>
                                                    <div class="flex_1">
                                                        <h4 class="sub-menu-title">
                                                            <a class="" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                                                <?php echo the_title(); ?>
                                                            </a>
                                                        </h4>
                                                        <p class="txt-sm txt-height-1-1">
                                                            <?php echo rwmb_meta( 'program-name' ); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <?php
                                                    endwhile;
                                                    wp_reset_postdata();
                                                ?>
                                            </div>
                                            <div class="col-md-5 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        Business Clinic
                                                    </div>
                                                </h4>
                                                <div class="row row-15">
                                                <?php // Display posts
                                                    $program_query = new WP_Query( array(
                                                        'relationship' => array(
                                                            'id'   => 'program_to_track',
                                                            //'from' => 1041, // You can pass object ID or full object
                                                            'from' => 650, // You can pass object ID or full object
                                                        ),
                                                        'nopaging' => true,
                                                    ) );
                                                    while ( $program_query->have_posts() ) : $program_query->the_post();

                                                            $program_id = $post->ID;    //Get Program ID

                                                            //  Get Program Featured Image
                                                            $images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ), $post->ID );

                                                            $image = reset( $images );

                                                ?>
                                                   
                                                    <div class="col-6 padding-lr-15 padding-b-20">
                                                        <div class="d-flex">
                                                            <i class="fa fa-arrow-right padding-r-5 txt-sm padding-t-5"></i>
                                                            <div class="flex_1">
                                                                <h4 class="sub-menu-title">
                                                                    <a class="" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                                                        <?php the_title() ?>
                                                                    </a>
                                                                </h4>
                                                                <p class="txt-sm txt-height-1-1">
                                                                    <?php echo rwmb_meta( 'program-name' ); ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php
                                                    endwhile;
                                                    wp_reset_postdata();
                                                ?>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a class="" href="<?php echo $base_url.'/?action=career-dashboard' ?>">
                                    Career
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="sub-menu txt-color-white">
                                    <div class="sub-menu-content">
                                        <div class="row row-40">
                                            <div class="col-md-4 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        Job Roles I am preparing for
                                                    </div>
                                                </h4>
                                            <?php 
                                                $user_id = 1;
                                                $key = 'role_id';
                                                $single = false;

                                                $results = get_user_meta( get_current_user_id(), $key, $single );

                                                $roles_array = array_unique($results);

                                                if($roles_array){

                                            ?>
                                               
                                                <div class="row row-15">
                                                <?php
                                                    foreach($roles_array as $role){

                                                        $post = get_post( $role );    
                                                ?>
                                                   
                                                    <div class="col-6 padding-lr-10 padding-b-10">
                                                        <div class="d-flex">
                                                            <i class="fa fa-arrow-right padding-r-5 txt-sm"></i>
                                                            <div class="flex_1">
                                                                <h4 class="sub-menu-title">
                                                                    <a class="" href="<?php get_permalink($role); ?>">
                                                                        <?php echo $post->post_title ?>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php } ?>
                                                </div>
                                                
                                            <?php } else { ?>
                                                <div class="txt-sm">
                                                    <p class="">
                                                        You have not added any roles.
                                                    </p>
                                                    <p class="margin-t-20">
                                                        <a href="" class="btn btn-blue txt-xs">
                                                            Add a Role
                                                        </a>
                                                    </p>
                                                </div>
                                            <?php } ?>
                                            </div>
                                            <div class="col-md-3 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        JobAdvisor
                                                    </div>
                                                </h4>
                                            <?php // Display posts
                                                $wp_query = new WP_Query( array(
                                                    'relationship' => array(
                                                        'id'   => 'program_to_track',
                                                        'from' => 649, // You can pass object ID or full object
                                                    ),
                                                    'nopaging' => true,
                                                ) );
                                                while ( $wp_query->have_posts() ) : $wp_query->the_post();

                                                        $program_id = $post->ID;    //Get Program 

                                                        //  Get Program Featured Image
                                                        $images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ), $post->ID );

                                                        $image = reset( $images );

                                            ?>
                                                <div class="padding-b-15 d-flex">
                                                    <i class="fa fa-arrow-right padding-r-5 txt-sm padding-t-5"></i>
                                                    <div class="flex_1">
                                                        <h4 class="sub-menu-title">
                                                            <a class="" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                                                <?php the_title() ?>
                                                            </a>
                                                        </h4>
                                                        <p class="txt-sm txt-height-1-1">
                                                            <?php echo rwmb_meta( 'program-name' ); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                            <?php
                                                endwhile;
                                                wp_reset_postdata();
                                            ?>
                                            </div>
                                            <div class="col-md-4 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        My CV
                                                    </div>
                                                </h4>
                                                <div class="txt-sm">
                                                    <p class="">
                                                        Create & Manage your CV.
                                                    </p>
                                                    <p class="margin-t-20">
                                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=entrepreneurship-dashboard" class="btn btn-blue txt-xs">
                                                            Manage
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php
                                }
                            }
                        ?>
                           
                        <?php
                            $current_user = wp_get_current_user();
                            $user_roles = $current_user->roles;

                            if($user_roles){
                                if ( in_array('administrator', $user_roles) || in_array('bbp_moderator', $user_roles) ) {
                        ?>
                            <!--<li>
                                <a href="<?php echo $base_url.'/?action=partner-dashboard' ?>">
                                    Mentor
                                </a>
                            </li>-->
                        <?php
                                }
                            }
                        ?>
                            <li>
                                <a href="<?php echo $base_url.'/?action=partner-dashboard' ?>">
                                    Partner
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $base_url.'/?action=nysc-saed-dashboard' ?>">
                                    NYSC SAED
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $base_url.'/?action=make-money-dashboard' ?>">
                                    MarketPlace
                                </a>
                            </li>
                        </ul>
                    </nav>
                </nav>
                
                <div class="user-account">
                <?php
                    
                    if ( is_user_logged_in() ) {
                        $current_user = wp_get_current_user();
                ?>
                        
                    <div class="dropdown">
                        <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="account-info">
                                <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="">
                                <span class="profile-name">
                                    <?php echo $current_user->display_name ?>
                                </span>
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                            <!--<a class="dropdown-item" href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=career-dashboard">
                                My Career Dashboard
                            </a>
                            <a class="dropdown-item" href="https://www.saedconnect.org/growth-programs/my-dashboard/">
                                My Entrepreneurship Dashboard
                            </a>-->
                            <a class="dropdown-item" href="<?php echo $base_url.'/?action=account-dashboard' ?>">
                                Account
                            </a>
                            <a class="dropdown-item" href="<?php echo wp_logout_url( 'https://www.saedconnect.org' ); ?> ">
                                Logout
                            </a>
                        </div>
                    </div>
                        
                <?php } else { ?>        

                    <a class="login" href="https://www.saedconnect.org/login">Login</a>
                    <a class="signup" href="https://www.saedconnect.org/register">Sign up</a>
                    
                <?php } ?>
                </div>
                <div class="home-button bg-white-trans">
                    <button class="hamburger hamburger--spring menu-btn" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
	</header>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <header class="container-wrapper dashboard-multi-header bg-yellow padding-tb-20">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="txt-xlg txt-medium txt-color-white">
                        My Profile
                    </h1>
                </div>
            </div>
        </header>

        <section class="container-wrapper" style="padding-top: 62px">
            <div class="row">
                <div class="col-md-2 dashboard-multi-main-menu">
                    <div class="cv-menu-toggle">
                        Profile Menu
                    </div>
                    <figure class="avatar">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="">
                    </figure>
                    <div class="user-info">
                        <div class="name">
                            John Doe
                        </div>
                    </div>
                    <ul class="cv-menu-list">
                        <li>
                            <a href="#collapseExample">
                                Welcome                 
                            </a>
                        </li>
                        <li>
                            <a 
                                class="active has-child" 
                                href="#collapseExample"
                                data-toggle="collapse"
                                aria-expanded="false"
                                aria-controls="collapseExample"
                                role="button"
                            >
                                My Profile                    
                            </a>
                            <div class="collapse" id="collapseExample">
                                <ul class="sub-list">
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information">
                                            Personal Profile                  
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&cv-view=overview"
                                    >
                                            Education & Experience          
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Skills Profile          
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/service-marketplace/my-capabilities/">
                                            Capability Profile                 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/service-provider-directory/trainer-profile/"
                                    >
                                            Trainer Profile        
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Uploads     
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#businesses">
                                My Businesses                        
                            </a>
                        </li>
                        <li>
                            <a href="#projects">
                                My Projects                   
                            </a>
                        </li>
                        <li>
                            <a href="#programs">
                                My Programs                       
                            </a>
                        </li>
                        <li>
                            <a 
                                class="has-child"
                                href="#offers"
                                data-toggle="collapse"
                                aria-expanded="false"
                                aria-controls="offers"
                                role="button"
                            >
                                My Offers                    
                            </a>
                            <div class="collapse" id="offers">
                                <ul class="sub-list">
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Service Offers                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Training Offers                       
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a 
                                class="has-child"
                                href="#talentRequests"
                                data-toggle="collapse"
                                aria-expanded="false"
                                aria-controls="talentRequests"
                                role="button"
                            >
                                My Talent Requests                       
                            </a>
                            <div class="collapse" id="talentRequests">
                                <ul class="sub-list">
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            My Job Posts                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            My Task/gig requests                       
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a 
                                class="has-child"
                                href="#helpRequests"
                                data-toggle="collapse"
                                aria-expanded="false"
                                aria-controls="helpRequests"
                                role="button"
                            >
                                My Help Requests                    
                            </a>
                            <div class="collapse" id="helpRequests">
                                <ul class="sub-list">
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Business Partner Requests                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Mentorship Requests                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Marketing/Publicity  Support Requests                      
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Tool/Equipment Request                      
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Officespace/workspace request                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Land & Landed Property Request                      
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Funding Request                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Volunteer Requests                      
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a 
                                class="has-child"
                                href="#businesses"
                                data-toggle="collapse"
                                aria-expanded="false"
                                aria-controls="businesses"
                                role="button"
                            >
                                My Interactions                        
                            </a>
                            <div class="collapse" id="businesses">
                                <ul class="sub-list">
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Businesses I have engaged                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Projects I have engaged                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Opportunity Applications                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Service Offers I have engaged                  
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Trainings Offers I have engaged                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Internship & Job Applications                  
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Training Requests I have engaged         
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a 
                                class="has-child"
                                href="#enquiries"
                                data-toggle="collapse"
                                aria-expanded="false"
                                aria-controls="enquiries"
                                role="button"
                            >
                                Enquiries & Applications                   
                            </a>
                            <div class="collapse" id="enquiries">
                                <ul class="sub-list">
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Business Enquiries                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Project Enquiries                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Program Applications                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Service offer enquiries                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Job Offer Applications                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Training Offer Enquiries                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Training request responses                       
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/data-collector-profile/">
                                Data Collector Dashboard                 
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/influencer-profile/">
                                Influencer Dashboard                    
                            </a>
                        </li>
                        <li>
                            <a href="#businesses">
                                My Community Interactions                       
                            </a>
                        </li>
                        <li>
                            <a 
                                class="has-child"
                                href="#nysc"
                                data-toggle="collapse"
                                aria-expanded="false"
                                aria-controls="nysc"
                                role="button"
                            >
                                NYSC SAED                      
                            </a>
                            <div class="collapse" id="nysc">
                                <ul class="sub-list">
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            Verify that you have served/are Serving                       
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            My SAED Experience                 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                                    >
                                            SAED Certificate                    
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-7 dashboard-multi-main-content">
                <?php if($_GET['view'] == 'form'){ //Display form  ?>
                   
                    <div class="section-wrapper">
                    <?php if( $_REQUEST['gf-id'] ){ ?>
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                Add <?php echo $_REQUEST['form-title'] ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <?php 
                                echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                            ?>
                        </div>

                    <?php } else { ?>
                            
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                Add <?php echo $_REQUEST['form-title'] ?>
                            </h2>
                            <div class="text-right">
                                <a 
                                    href="https://www.saedconnect.org/service-provider-directory/trainer-profile/" 
                                    class="edit-btn"
                                >
                                    Cancel
                                </a>
                            </div>
                        </div>
                        <div class="entry">   
                            <form action="https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form" method="post">
                                <?php
                                    /* Meta Key */
                                    $meta_key = 'training_area';
                                    $tax_type = 'training-category';
                                    $redirect_link = 'https://www.saedconnect.org/service-provider-directory/trainer-profile/';
                                    /*
                                    *
                                    * Save / Retrieve Form Data
                                    *
                                    */
                                    if($_POST['options']){
                                        /* Serialize Form Submission */
                                        $serialized_data = maybe_serialize($_POST['options']);

                                        /* Save usermeta */
                                        update_user_meta( $current_user->ID, $meta_key, $serialized_data);
                                        
                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }

                                    /* Get User Meta to populate Form */
                                    $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, $meta_key, true) );

                                    /*
                                    *
                                    * Populate Form Data from Terms
                                    *
                                    */
                                    //Get Terms
                                    $terms = get_terms( $tax_type, array('hide_empty' => false));

                                    foreach ($terms as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $parent = $term->parent;
                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                ?>


                                    <div class="margin-b-20">
                                    <?php if( $parent == 0 ){ ?>

                                        <div class="txt-normal-s txt-medium txt-color-dark margin-b-15">
                                            <?php echo $term_name; ?>
                                        </div>
                                        <?php
                                            foreach ($terms as $child_term) {
                                                // Check and see if the term is a top-level parent. If so, display it.
                                                $child_parent = $child_term->parent;
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name

                                                if( $child_parent == $term_id ){
                                        ?>
                                                <label class="txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                    <input
                                                        class="margin-r-5"
                                                        type="checkbox" 
                                                        value="<?php echo $child_term_id ?>" 
                                                        name="options[]" 
                                                        <?php echo in_array($child_term_id, $unserialized_data) ? "checked" : "" ?>
                                                    >
                                                    <?php echo $child_term_name; ?>
                                                </label>
                                        <?php
                                                }
                                            }
                                        ?>
                                    <?php } ?>
                                    </div>

                                <?php 
                                    } 
                                ?>
                                <div class="text-right">
                                    <input type="submit" class="btn btn-yellow-dark txt-xs">
                                </div>
                            </form>
                        </div>

                    <?php } ?>                    
                </div>      
                                                                    
                <?php } else { //Empty Profile Message ?>

                    <!-- Trainer Information -->
                    <div class="section-wrapper">
                    <?php
                        $gf_id = 1; //Form ID
                        $gv_id = 637; //Gravity View ID
                        $title = 'Trainer Profile / Tutorlink';

                        $entry_count = 0;

                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                            )
                        );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                    ?>

                        <?php if(!$entry_count){ //If no entry ?>
                            
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>
                            <div class="entry">
                                <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                    Empty! Start by creating your <?php echo $title ?>.
                                </h3>

                                <div class="padding-b-20">
                                    <a 
                                        class="btn btn-career-purple txt-xxs no-m-b"
                                        href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                    >
                                        Create Profile
                                    </a>
                                </div>
                            </div>                            
                            
                        <?php } else { ?>
                           
                            <?php foreach( $entries as $entry ){ ?>

                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        <?php echo $title ?>
                                    </h2>
                                    <div class="text-right">
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=trainer-profile' ?>" 
                                            class="edit-btn"
                                        >
                                            Edit
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="entry">
                                    <div class="row row-10">
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Name of Training Outfit        
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 1 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Brief Trainer Profile          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 5 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Have you ever Trained before?          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 17 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                What training models are you open to providing?         
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 3; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Skill Areas where you provide training          
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 2; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Name of Contact person for any enquiries         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 8 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                States where you can offer private tutoring & classroom trainings          
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 7; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Primary Contact Address         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 9 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact Email address (Where anyone interested in this training should email for enquiry)
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 10 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact Phone Number (Where anyone interested in training with you should call for enquiry)         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 11 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact WhatsApp Number (Where anyone interested in training with you should WhatsApp for enquiry)          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 12 ); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                        <?php } ?>
                    </div>
                    
                    <!-- Training Area Competency -->
                    <div class="section-wrapper padding-b-10">
                    <?php
                        $gf_id = 3; //Form ID
                        $gv_id = 652; //Gravity View ID
                        $title = 'Competency Areas';
    
                        $competency_gf_id = 11; //Form ID
                        $competency_gv_id = 681; //Gravity View ID

                        $entry_count = 0;

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                            )
                        );
    
                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
    
    
                        /* Get Competency Entries */
                        $competency_entries = GFAPI::get_entries( $competency_gf_id, $search_criteria );
    
                        /* Get Competency Entry Count */
                        $competency_entry_count = GFAPI::count_entries( $competency_gf_id, $search_criteria );
                    ?>

                        <?php if(!$entry_count){ //If no entry ?>
                            
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>
                            <div class="entry">
                                <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                    Empty! Start by Adding your <?php echo $title ?>.
                                </h3>

                                <div class="padding-b-20">
                                    <a 
                                        class="btn btn-career-purple txt-xxs no-m-b"
                                        href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&form-title=%s", $title); ?>"
                                    >
                                        Add Competencies
                                    </a>
                                </div>
                            </div>                            
                            
                        <?php } else { ?>
                           
                            <?php foreach( $entries as $entry ){ ?>

                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        <?php echo $title ?>
                                    </h2>
                                    <div class="text-right">
                                        <a 
                                            href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&form-title=%s", $title); ?>" 
                                            class="edit-btn"
                                        >
                                            Add Competencies
                                        </a>
                                    </div>
                                </div>
                                <?php
                                    $competencies = array(
                                        array(
                                            'field_id' => 5,
                                            'title' => 'Exams'
                                        ),
                                        array(
                                            'field_id' => 6,
                                            'title' => 'Languages'
                                        ),
                                        array(
                                            'field_id' => 7,
                                            'title' => 'Music / Dance'
                                        ),
                                        array(
                                            'field_id' => 8,
                                            'title' => 'Games / Sport'
                                        ),
                                        array(
                                            'field_id' => 9,
                                            'title' => 'Fashion & Beauty'
                                        ),
                                        array(
                                            'field_id' => 10,
                                            'title' => 'Art / Craft'
                                        ),
                                        array(
                                            'field_id' => 11,
                                            'title' => 'Video, Graphics, Design & Printing'
                                        ),
                                        array(
                                            'field_id' => 12,
                                            'title' => 'Agriculture'
                                        ),
                                        array(
                                            'field_id' => 13,
                                            'title' => 'Productivity Software Tools'
                                        ),
                                        array(
                                            'field_id' => 14,
                                            'title' => 'Programming & Server Management'
                                        ),
                                        array(
                                            'field_id' => 15,
                                            'title' => 'Computer Hardware'
                                        ),
                                        array(
                                            'field_id' => 16,
                                            'title' => 'Business & Finance'
                                        ),
                                        array(
                                            'field_id' => 17,
                                            'title' => 'Digital Marketing'
                                        ),
                                        array(
                                            'field_id' => 18,
                                            'title' => 'Writing'
                                        ),
                                        array(
                                            'field_id' => 19,
                                            'title' => 'Building & Construction'
                                        ),
                                    );
                        
                                    foreach($competencies as $competency){
                                ?>
                                
                                
                                    <?php
                                        $field_id = $competency['field_id']; // Update this number to your field id number
                                        $field = RGFormsModel::get_field( $gf_id, $field_id );
                                        $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                        $values = explode(",", $value);

                                        if($value){
                                    ?>
                                <div class="entry">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-15">
                                        <?php echo $competency['title'] ?>      
                                    </p>
                                        <?php foreach($values as $value){ ?>
                                    
                                    <div class="row bg-ghostwhite padding-o-10 margin-b-10">
                                        <div class="col-5">
                                            <p class="txt-sm txt-color-dark txt-medium">
                                                <?php echo $value ?>
                                            </p>                                 
                                        </div>
                                        <p class="col-7 txt-xs text-right">
                                        <?php 
                                            $itExist = false;
                                                                         
                                            foreach( $competency_entries as $competency_entry ){ 
                                                if(rgar( $competency_entry, 1 ) == trim($value)){
                                                    $itExist = true;
                                                }
                                            }
                                                                         
                                            if(!$itExist){
                                        ?>
                                           <a href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=11&form-title=%s Description&competency-title=%s", trim($value), trim($value)); ?>"class="">
                                               <i class="fa fa-plus"></i>
                                               Add Description
                                           </a>
                                        <?php } ?>
                                        </p>
                                        
                                            <?php 
                                                foreach( $competency_entries as $competency_entry ){ 
                                                    if(rgar( $competency_entry, 1 ) == trim($value)){
                                            ?>
                                        <div class="col-12 padding-t-10 margin-t-10 border-t-1">
                                            <div class="row row-10">
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What is your experience in this subject?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 2 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        Number of Years of Experience     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 3 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What training models are you open to providing for this subject area?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php
                                                            $field_id = 4; // Update this number to your field id number
                                                            $field = RGFormsModel::get_field( $competency_gf_id, $field_id );
                                                            $value = is_object( $field ) ? $field->get_value_export( $competency_entry ) : '';
                                                            echo $value;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10 padding-t-20 padding-b-10">
                                                    <a href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$competency_entry['id'].'" view_id="'.$competency_gv_id.'" action="edit" return="url" /]').'&view=trainer-profile' ?>" class="edit-btn">
                                                       <i class="fa fa-pencil"></i>
                                                       Edit Description
                                                   </a>
                                                </div>
                                            </div>
                                        </div>
                                            <?php 
                                                    }
                                                }
                                            ?>
                                            
                                    </div>
                                       
                                        <?php } ?>
                                </div>
                                    <?php } ?>
                                
                                <?php } ?>

                            <?php } ?>

                        <?php } ?>
                    </div>
                    
                    <!-- Training Area Competency -->
                    <!--<div class="section-wrapper">
                    <?php
                        $gf_id = 3; //Form ID
                        $gv_id = 652; //Gravity View ID
                        $title = 'Competency Areas';

                        $entry_count = 0;

                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                            )
                        );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                    ?>
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>

                        <?php if(!$entry_count){ //If no entry ?>

                            <div class="entry">
                                <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                    Empty! Start by adding your <?php echo $title ?>.
                                </h3>

                                <div class="padding-b-20">
                                    <a 
                                        class="btn btn-career-purple txt-xxs no-m-b" 
                                        href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                    >
                                        Add <?php echo $title ?>
                                    </a>
                                </div>
                            </div>

                        <?php } else { ?>
                            <?php foreach( $entries as $entry ){ ?>

                            <div class="entry">
                                <div class="row row-10">
                                <?php
                                    $field_id = 5; // Update this number to your field id number
                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                        
                                    if($value){
                                ?>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Exams      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo $value; ?>
                                        </p>
                                    </div>
                                <?php } ?>
                                   
                                <?php
                                    $field_id = 6; // Update this number to your field id number
                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                        
                                    if($value){
                                ?>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Languages      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo $value; ?>
                                        </p>
                                    </div>
                                <?php } ?>
                                   
                                    <?php
                                        $value = is_object( $field ) ? $field->get_value_export( $entry, $field_id, true ) : '';
                                        $values = explode(",", $value);
                                        print_r($values);
                                    
                                    ?>
                                    
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            What is your experience in this subject?     
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Number of Years of Experience     
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 3 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            What training models are you open to providing for this subject area?     
                                        </p>
                                        <p class="txt-sm">
                                            <?php
                                                $field_id = 20; // Update this number to your field id number
                                                $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                echo $value;
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a 
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=trainer-profile&cv-form-title=Referee' ?>" 
                                       class="edit-btn"
                                    >
                                        Edit
                                    </a>
                                    <a 
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=trainer-profile';  ?>" 
                                       class="delete-btn margin-l-5 confirm-delete"
                                    >
                                        Delete
                                    </a>
                                </div>
                            </div>

                            <?php } ?>

                            <div class="padding-o-20">
                                <a 
                                    class="btn btn-career-purple txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>

                        <?php } ?>
                    </div>-->
                    
                    
                    
                <?php } ?>   
                </div>
                <div class="col-md-3 dashboard-multi-main-sidebar">
                    <div class="side-bar-card">
                        <h4 class="txt-medium txt-normal-s txt-color-dark margin-b-20">
                            Print CV
                        </h4>
                        <p class="txt-xs">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ut, commodi, ea nisi voluptatibus error.
                        </p>
                        <div class="margin-t-20">
                            <a href="" class="btn btn-yellow-dark txt-xxs no-m-b">
                                <i class="fa fa-print"></i>
                                <span class="padding-l-5">
                                    Print
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="side-bar-card">
                        <h4 class="txt-medium txt-color-dark margin-b-20">
                            Download CV
                        </h4>
                        <p class="txt-sm">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ut, commodi, ea nisi voluptatibus error.
                        </p>
                        <div class="margin-t-20">
                            <a href="" class="btn btn-yellow-dark txt-xxs no-m-b">
                                <i class="fa fa-download"></i>
                                <span class="padding-l-5">
                                    Download
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>