<?php /*Template Name: Profile - Enquiry: Business*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <header class="dashboard-main-nav font-main ">
        <div class="container-wrapper">
            <div class="wrapper">
                <a class="brand" href="https://www.saedconnect.org/">
                    <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                    <span class="name">My SAEDConnect</span>
                </a>                
                <nav class="nav-box">
                    <nav class="navigation-list-wrapper left">
                        <ul class="navigation-list">
                        <?php
                            $current_user = wp_get_current_user();
                            $user_roles = $current_user->roles;

                            if($user_roles){
                                if ( !in_array('bbp_moderator', $user_roles) ) {
                        ?>
                            <li>
                                <a href="<?php echo $base_url.'/?action=entrepreneurship-dashboard' ?>">
                                    Entrepreneurship
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="sub-menu txt-color-white">
                                    <div class="sub-menu-content">
                                        <div class="row row-40">
                                            <div class="col-md-4 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        My Side Hustle Communuties
                                                    </div>
                                                </h4>
                                                
                                                <?php
                                                    $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'value' => $current_user->id );

                                                    $entries = GFAPI::get_entries( 21, $search_criteria ); 
                                                    //$entries = GFAPI::get_entries( 211, $search_criteria ); 

                                                    /*  */
                                                    $exclude_list = array();
                                                ?>

                                                <!--Using Get entries-->
                                                <?php                               
                                                    /* Loop Through Entries */
                                                    if($entries){
                                                ?>
                                                <div class="row row-15">
                                                <?php
                                                    foreach($entries as $entry){
                                                            /* Get Entry ID */
                                                            $entry_id = $entry['id'];

                                                            /* Get Post attached to entry */
                                                            //$post = get_post( $entry['post_id'] );

                                                            /* Generate Edit Entry Link */
                                                            $edit_entry_link = do_shortcode('[gv_entry_link action="edit" entry_id="'.$entry_id.'" view_id="'.$gv_id.'" return="url" /]');

                                                            $post_title = rgar( $entry, '8' );

                                                            /* If has an entry, Add to Exclude list */
                                                            array_push($exclude_list, $post_title);

                                                            $post = get_page_by_title($post_title, OBJECT, 'information-session');

                                                            $images = rwmb_meta( 'info-session-feautured-image', array( 'limit' => 1 ), $post->ID );

                                                            $image = reset( $images );    
                                                ?>
                                                    
                                                    <div class="col-6 padding-lr-10 padding-b-10">
                                                        <div class="d-flex">
                                                            <i class="fa fa-arrow-right padding-r-5 txt-sm"></i>
                                                            <div class="flex_1">
                                                                <h4 class="sub-menu-title">
                                                                    <a class="" href="<?php echo get_the_permalink() ?>">
                                                                        <?php echo $post->post_title ?>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php } ?>
                                                </div>
                                                
                                            <?php } else { ?>
                                                
                                                <div class="txt-sm">
                                                    <p class="">
                                                        You have not joined any community.
                                                    </p>
                                                    <p class="margin-t-20">
                                                        <a href="" class="btn btn-blue txt-xs">
                                                            Join a Community
                                                        </a>
                                                    </p>
                                                </div>
                                                
                                            <?php } ?>
                                            </div>
                                            <div class="col-md-3 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        The Entrepreneurship Incubator
                                                    </div>
                                                </h4>
                                                <?php // Display posts
                                                    $wp_query = new WP_Query( array(
                                                        'relationship' => array(
                                                            'id'   => 'program_to_track',
                                                            'from' => 632, // You can pass object ID or full object
                                                        ),
                                                        'nopaging' => true,
                                                    ) );
                                                    while ( $wp_query->have_posts() ) : $wp_query->the_post();

                                                            $program_id = $post->ID;    //Get Program 

                                                            //  Get Program Featured Image
                                                            $images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ), $post->ID );

                                                            $image = reset( $images );

                                                ?>
                                                
                                                <div class="padding-b-15 d-flex">
                                                    <i class="fa fa-arrow-right padding-r-5 txt-sm"></i>
                                                    <div class="flex_1">
                                                        <h4 class="sub-menu-title">
                                                            <a class="" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                                                <?php echo the_title(); ?>
                                                            </a>
                                                        </h4>
                                                        <p class="txt-sm txt-height-1-1">
                                                            <?php echo rwmb_meta( 'program-name' ); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <?php
                                                    endwhile;
                                                    wp_reset_postdata();
                                                ?>
                                            </div>
                                            <div class="col-md-5 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        Business Clinic
                                                    </div>
                                                </h4>
                                                <div class="row row-15">
                                                <?php // Display posts
                                                    $program_query = new WP_Query( array(
                                                        'relationship' => array(
                                                            'id'   => 'program_to_track',
                                                            //'from' => 1041, // You can pass object ID or full object
                                                            'from' => 650, // You can pass object ID or full object
                                                        ),
                                                        'nopaging' => true,
                                                    ) );
                                                    while ( $program_query->have_posts() ) : $program_query->the_post();

                                                            $program_id = $post->ID;    //Get Program ID

                                                            //  Get Program Featured Image
                                                            $images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ), $post->ID );

                                                            $image = reset( $images );

                                                ?>
                                                   
                                                    <div class="col-6 padding-lr-15 padding-b-20">
                                                        <div class="d-flex">
                                                            <i class="fa fa-arrow-right padding-r-5 txt-sm padding-t-5"></i>
                                                            <div class="flex_1">
                                                                <h4 class="sub-menu-title">
                                                                    <a class="" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                                                        <?php the_title() ?>
                                                                    </a>
                                                                </h4>
                                                                <p class="txt-sm txt-height-1-1">
                                                                    <?php echo rwmb_meta( 'program-name' ); ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php
                                                    endwhile;
                                                    wp_reset_postdata();
                                                ?>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a class="" href="<?php echo $base_url.'/?action=career-dashboard' ?>">
                                    Career
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="sub-menu txt-color-white">
                                    <div class="sub-menu-content">
                                        <div class="row row-40">
                                            <div class="col-md-4 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        Job Roles I am preparing for
                                                    </div>
                                                </h4>
                                            <?php 
                                                $user_id = 1;
                                                $key = 'role_id';
                                                $single = false;

                                                $results = get_user_meta( get_current_user_id(), $key, $single );

                                                $roles_array = array_unique($results);

                                                if($roles_array){

                                            ?>
                                               
                                                <div class="row row-15">
                                                <?php
                                                    foreach($roles_array as $role){

                                                        $post = get_post( $role );    
                                                ?>
                                                   
                                                    <div class="col-6 padding-lr-10 padding-b-10">
                                                        <div class="d-flex">
                                                            <i class="fa fa-arrow-right padding-r-5 txt-sm"></i>
                                                            <div class="flex_1">
                                                                <h4 class="sub-menu-title">
                                                                    <a class="" href="<?php get_permalink($role); ?>">
                                                                        <?php echo $post->post_title ?>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php } ?>
                                                </div>
                                                
                                            <?php } else { ?>
                                                <div class="txt-sm">
                                                    <p class="">
                                                        You have not added any roles.
                                                    </p>
                                                    <p class="margin-t-20">
                                                        <a href="" class="btn btn-blue txt-xs">
                                                            Add a Role
                                                        </a>
                                                    </p>
                                                </div>
                                            <?php } ?>
                                            </div>
                                            <div class="col-md-3 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        JobAdvisor
                                                    </div>
                                                </h4>
                                            <?php // Display posts
                                                $wp_query = new WP_Query( array(
                                                    'relationship' => array(
                                                        'id'   => 'program_to_track',
                                                        'from' => 649, // You can pass object ID or full object
                                                    ),
                                                    'nopaging' => true,
                                                ) );
                                                while ( $wp_query->have_posts() ) : $wp_query->the_post();

                                                        $program_id = $post->ID;    //Get Program 

                                                        //  Get Program Featured Image
                                                        $images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ), $post->ID );

                                                        $image = reset( $images );

                                            ?>
                                                <div class="padding-b-15 d-flex">
                                                    <i class="fa fa-arrow-right padding-r-5 txt-sm padding-t-5"></i>
                                                    <div class="flex_1">
                                                        <h4 class="sub-menu-title">
                                                            <a class="" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                                                <?php the_title() ?>
                                                            </a>
                                                        </h4>
                                                        <p class="txt-sm txt-height-1-1">
                                                            <?php echo rwmb_meta( 'program-name' ); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                            <?php
                                                endwhile;
                                                wp_reset_postdata();
                                            ?>
                                            </div>
                                            <div class="col-md-4 padding-lr-40">
                                                <h4 class="sub-menu-header">
                                                    <div class="txt-color-yellow">
                                                        My CV
                                                    </div>
                                                </h4>
                                                <div class="txt-sm">
                                                    <p class="">
                                                        Create & Manage your CV.
                                                    </p>
                                                    <p class="margin-t-20">
                                                        <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=entrepreneurship-dashboard" class="btn btn-blue txt-xs">
                                                            Manage
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php
                                }
                            }
                        ?>
                           
                        <?php
                            $current_user = wp_get_current_user();
                            $user_roles = $current_user->roles;

                            if($user_roles){
                                if ( in_array('administrator', $user_roles) || in_array('bbp_moderator', $user_roles) ) {
                        ?>
                            <!--<li>
                                <a href="<?php echo $base_url.'/?action=partner-dashboard' ?>">
                                    Mentor
                                </a>
                            </li>-->
                        <?php
                                }
                            }
                        ?>
                            <li>
                                <a href="<?php echo $base_url.'/?action=partner-dashboard' ?>">
                                    Partner
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $base_url.'/?action=nysc-saed-dashboard' ?>">
                                    NYSC SAED
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $base_url.'/?action=make-money-dashboard' ?>">
                                    MarketPlace
                                </a>
                            </li>
                        </ul>
                    </nav>
                </nav>
                
                <div class="user-account">
                <?php
                    
                    if ( is_user_logged_in() ) {
                        $current_user = wp_get_current_user();
                ?>
                        
                    <div class="dropdown">
                        <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="account-info">
                                <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="">
                                <span class="profile-name">
                                    <?php echo $current_user->display_name ?>
                                </span>
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                            <!--<a class="dropdown-item" href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=career-dashboard">
                                My Career Dashboard
                            </a>
                            <a class="dropdown-item" href="https://www.saedconnect.org/growth-programs/my-dashboard/">
                                My Entrepreneurship Dashboard
                            </a>-->
                            <a class="dropdown-item" href="<?php echo $base_url.'/?action=account-dashboard' ?>">
                                Account
                            </a>
                            <a class="dropdown-item" href="<?php echo wp_logout_url( 'https://www.saedconnect.org' ); ?> ">
                                Logout
                            </a>
                        </div>
                    </div>
                        
                <?php } else { ?>        

                    <a class="login" href="https://www.saedconnect.org/login">Login</a>
                    <a class="signup" href="https://www.saedconnect.org/register">Sign up</a>
                    
                <?php } ?>
                </div>
                <div class="home-button bg-white-trans">
                    <button class="hamburger hamburger--spring menu-btn" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
	</header>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <header class="container-wrapper dashboard-multi-header bg-yellow padding-tb-20">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="txt-xlg txt-medium txt-color-white">
                        My Profile
                    </h1>
                </div>
            </div>
        </header>

        <section class="container-wrapper" style="padding-top: 62px">
            <div class="row">
                <div class="col-md-2 dashboard-multi-main-menu">
                    <?php 
                        get_template_part( 'template-parts/user-dashboard/_dashboard-nav' );
                    ?>
                </div>
                <div class="col-md-10 dashboard-multi-main-content">
                    <section class="section-wrapper">
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                Enquiries Business
                            </h2>
                        </div>
                        <div class="entry overflow-hidden">
                           <?php
                            
                                $get_search_fields = array( //which fields to search
                                    'mode' => 'any',   
                                );
                                                        
                                /* GF Search Criteria */
                                $gf_id = 12;
                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    )
                                  )
                                );

                                /* Get GF Entry Count */
                                $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                                $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                                

                                foreach( $entries as $entry ){

                                    $get_search_field = array(
                                        array(
                                            'key' => '2',
                                            'value' => $entry['post_id'], //Current logged in user
                                        )
                                    );
                                    
                                    $get_search_fields = array_merge($get_search_fields, $get_search_field);
                                
                                }
                            ?>
                           
                           
                            <div class="row row-15">
                            <?php
                                /* GF Search Criteria */
                                $gf_id = 30;
                                $parent_post_id = 2;
                                /* GF Search Criteria */
                                $search_criteria = array(

                                    'field_filters' => $get_search_fields
                                );

                                /* Get GF Entry Count */
                                $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                                $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                                

                                foreach( $entries as $entry ){

                                    $parent_post_id = rgar( $entry, $parent_post_id );
                                    $parent_post = get_post($parent_post_id);
                                    
                                    $author_id = $entry['created_by'];
                                    
                                    
                                     /* Get User Display Name */
                                    switch_to_blog(1);

                                    $user_gf_id = 4; //Form ID

                                    /* GF Search Criteria */
                                    $user_search_criteria = array(

                                    'field_filters' => array( //which fields to search

                                        array(

                                            'key' => 'created_by', 'value' => $author_id, //Current logged in user
                                            )
                                        )
                                    );

                                    /* Get Entries */
                                    $user_entries = GFAPI::get_entries( $user_gf_id, $user_search_criteria );

                                    /* Get GF Entry Count */
                                    $user_entry_count = GFAPI::count_entries( $user_gf_id, $user_search_criteria );

                                    if($user_entry_count){ //If no entry
                                        foreach( $user_entries as $user_entry ){          
                                            $displayname = rgar( $user_entry, '4.3' ).' '.rgar( $user_entry, '4.6' );
                                        }                
                                    }   

                                    restore_current_blog();
                            ?>
                               
                                <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                    <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                        <h3 class="margin-b-10 txt-lg txt margin-b-10">
                                             <a class="txt-color-blue" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                        </h3>
                                        <div class="txt-normal-s margin-b-20">
                                            <p>
                                                <?php echo rgar( $entry, '4' ) ?>
                                            </p>
                                        </div>
                                        <div class="txt-xs txt-medium">
                                            <div class="txt-color-dark" style="text-transform:capitalize">
                                                by
                                                <?php echo ($post->post_author == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                            </div>
                        </div>
                    </section>   
                </div>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>