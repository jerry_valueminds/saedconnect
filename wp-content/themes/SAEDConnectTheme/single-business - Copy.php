<?php get_header() ?>
   
<?php
    $site_url = site_url();
    $current_user = wp_get_current_user();

    $gf_id = 12;
    $gv_id = 89;

?>
    
<?php while ( have_posts() ) : the_post(); ?>

<?php
    $post_id = get_the_ID();
    $form_title = get_the_title();
    $author_id = get_the_author_meta( 'ID' );

    /* GF Search Criteria */
    $search_criteria = array(

    'field_filters' => array( //which fields to search

        array(

            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
            'key' => '24', 'value' => $post_id, //Current logged in user
        )
      )
    );

    /* Get GF Entry Count */
    $entries = GFAPI::get_entries( $gf_id, $search_criteria );
    $entry = $entries[0];

    /*Get Theme Color*/
    $theme_color = rwmb_meta( 'opportunity-theme' );

    /*Get Theme Alt Color*/
    $theme_alt_color = rwmb_meta( 'opportunity-theme-alt' );

    /* Use User data or admin data */
    $user_data = rwmb_get_value( 'user_data' );
    
?>
    <style>
        .venture-hero{
            background-position: center !important;
            background-size: cover !important;
            background-repeat: no-repeat !important;
        }
    </style>
    <main class="main-content">
        <?php
            $meta = rgar( $entry, '3' );
            $meta = str_ireplace( 'http:', 'https:', $meta );
        ?>
        <div 
            class="venture-hero bg-ash container-wrapper padding-tb-40"
            style="background-image: url('<?php echo $meta; ?>')"
        >
            <div class="logo">
                <?php
                    $meta = rgar( $entry, '2' );
                    $meta = str_ireplace( 'http:', 'https:', $meta );
                ?>
                <img src="<?php echo $meta; ?>" alt="">
            </div>
        </div>
        <?php 
            //print_r($form_entry);
            /*print_r(rgar( $entry, '22' ));
        
            $meta = rgar( $entry, '22' );
        
            foreach($meta as $key=>$value) {
                echo $value;
                if($key < count($meta) - 1 ){
                    echo ', ';
                }
            }*/
        ?>
        <section class="container-wrapper padding-t-40 padding-b-20">
            <div class="row">
                <div class="col-md-6 mx-auto text-center">
                    <h1 class="txt-2em txt-medium margin-b-20">
                        <?php the_title() ?>
                    </h1>
                    <div class="text-box txt-normal-s txt-height-1-5">
                        <?php echo rgar( $entry, '4' ); ?>
                    </div>
                    <div class="margin-t-20">
                        <?php if( $current_user->ID == $author_id ){ ?>
                           
                            <a href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=business&form-title='.$form_title; ?>" 
                               class="btn btn-blue txt-normal-s n-m-b"
                            >
                                Manage this Business
                            </a>
                            
                        <?php } else { ?>
                           
                            <a 
                                href="<?php printf('https://www.saedconnect.org/ventures-directory/marketplace-form/?gf-id=30&form-title=%s&parent_id=%s', 'Contact '.get_the_title(), $post_id) ?>" 
                               class="btn btn-blue txt-normal-s n-m-b"
                            >
                                Contact this Business
                            </a>
                            
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        
        <div class="padding-b-40">
            <section class=" border-tb-1 border-color-darkgrey">
                <div class="row">
                    <div class="col-md-10 mx-auto text-center txt-sm txt-height-1-4">
                        <div class="row">
                            <div class="col-md-2 padding-o-20 border-r-1 border-color-darkgrey">
                                <div class="txt-color-lighter margin-b-15">
                                    Founding Date
                                </div>
                                <div class=" txt-medium txt-color-dark">
                                    <?php
                                        $meta = rgar( $entry, '11' );
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo date('j F Y',$date);
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-2 padding-o-20 border-r-1 border-color-darkgrey">
                                <div class="txt-color-lighter margin-b-15">
                                    Role in Business
                                </div>
                                <div class=" txt-medium txt-color-dark">
                                    <?php echo rgar( $entry, '16' ); ?>
                                </div>
                            </div>
                            <div class="col-md-2 padding-o-20 border-r-1 border-color-darkgrey">
                                <div class="txt-color-lighter margin-b-15">
                                    Registration Status
                                </div>
                                <div class=" txt-medium txt-color-dark">
                                    <?php echo rgar( $entry, '8' ); ?>
                                </div>
                            </div>
                            <div class="col-md-2 padding-o-20 border-r-1 border-color-darkgrey">
                                <div class="txt-color-lighter margin-b-15">
                                    Nature of Business
                                </div>
                                <div class="txt-medium txt-color-dark txt-height-1-2">
                                    <?php echo rgar( $entry, '5' ); ?>
                                </div>
                            </div>
                            <div class="col-md-2 padding-o-20 border-r-1 border-color-darkgrey">
                                <div class="txt-color-lighter margin-b-15">
                                    Stage of Business
                                </div>
                                <div class="txt-medium txt-color-dark txt-height-1-2">
                                    <?php echo rgar( $entry, '7' ); ?>
                                </div>
                            </div>
                            <div class="col-md-2 padding-o-20">
                                <div class="txt-color-lighter margin-b-15">
                                    Industry
                                </div>
                                <div class=" txt-medium txt-color-dark txt-height-1-4" style="text-transform:capitalize;">
                                    <?php
                                        $meta = wp_get_post_terms($post_id, 'industry', array("fields" => "names"));
                                    
                                        if($meta){
                                            foreach($meta as $key=>$value) {
                                                echo $value;
                                                if($key < count($meta) - 1 ){
                                                    echo ', ';
                                                }
                                            }
                                        }
                                    
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        
        <div class="section margin-b-40">
            <div class="venture-filter">
                <div class="container-wrapper">
                    <div class="row">
                        <div class="col-md-12 mx-auto">
                            <ul>
                                <li>
                                    <a href="<?php echo get_the_permalink() ?>" 
                                       class="<?php echo ($_GET['view'] == 'pitch') ? '' : 'active'; ?>"
                                    >
                                        About Business
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo get_the_permalink().'/?view=pitch' ?>"
                                       class="<?php echo ($_GET['view'] == 'pitch') ? 'active' : ''; ?>"
                                    >
                                        Pitch
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
            if( !$_GET['view'] == 'pitch' ){
        ?>
        
        <section class="container-wrapper padding-b-80">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-xlg margin-b-20">
                        About this Business
                    </h1>
                    <div class="row row-40">
                        <div class="col-md-7 padding-lr-40">
                            <article class="text-box txt-normal-s txt-height-1-7">
                                <?php echo rgar( $entry, '6' ); ?>
                            </article>
                        </div>
                        <div class="col-md-5 padding-lr-40 txt-normal-s txt-height-1-5">
                            <div class="row margin-b-20">
                                <div class="col-md-4 txt-color-lighter">
                                    Contact
                                </div>
                                <div class="col-md-8 txt-medium txt-color-dark">
                                    <?php echo rgar( $entry, '21' ); ?>
                                </div>
                            </div>
                            <div class="row margin-b-20">
                                <div class="col-md-4 txt-color-lighter">
                                    Website
                                </div>
                                <div class="col-md-8 txt-medium txt-color-dark">
                                    <a href="<?php echo rgar( $entry, '17' ); ?>" target="_blank">
                                        <?php echo rgar( $entry, '17' ); ?>
                                    </a>
                                </div>
                            </div>
                            <div class="row margin-b-20">
                                <div class="col-md-4 txt-color-lighter">
                                    Instagram
                                </div>
                                <div class="col-md-8 txt-medium txt-color-dark">
                                    <?php echo rgar( $entry, '19' ); ?>
                                </div>
                            </div>
                            <div class="row margin-b-20">
                                <div class="col-md-4 txt-color-lighter">
                                    Address
                                </div>
                                <div class="col-md-8 txt-medium txt-color-dark">
                                    <?php echo rgar( $entry, '14' ); ?>
                                </div>
                            </div>
                            <div class="row margin-b-20">
                                <div class="col-md-4 txt-color-lighter">
                                    Location
                                </div>
                                <div class="col-md-8 txt-medium txt-color-dark" style="text-transform:capitalize;">
                                    <?php
                                        $field_id = 15; // Update this number to your field id number
                                        $field = RGFormsModel::get_field( $gf_id, $field_id );
                                        $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                        echo $value;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php }else{ ?>
                                       
        <section class="container-wrapper padding-b-80">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-xlg margin-b-20">
                        Business Pitch
                    </h1>
                    <div class="row row-40">
                        <div class="col-md-7 padding-lr-40">
                            <article class="text-box sm txt-height-1-4">
                                <?php echo rgar( $entry, '6' ); ?>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
                                        
        <?php } ?>
        
        <?php if( $current_user->ID == $author_id ){ ?>
        
        <section class="container-wrapper padding-t-80">
            <div class="row">
                <div class="col-md-10 mx-auto padding-b-80">
                    <h1 class="txt-xxlg margin-b-80 text-center">
                        Requests
                    </h1>
                    <?php 
                        $request_type_array = array(
                            array(
                                'gf_id' => 19,
                                'parent_post_field' => 13,
                                'gv_id' => 99,
                                'title' => 'Business Partner Request',
                            ),
                            array(
                                'gf_id' => 20,
                                'parent_post_field' => 15,
                                'gv_id' => 99,
                                'title' => 'Tool Request',
                            ),
                            array(
                                'gf_id' => 21,
                                'parent_post_field' => 12,
                                'gv_id' => 99,
                                'title' => 'Land Request',
                            ),
                            array(
                                'gf_id' => 22,
                                'parent_post_field' => 13,
                                'gv_id' => 20,
                                'title' => 'Request for Workspace/Office Space/Building/Factory Space',
                            ),
                            array(
                                'gf_id' => 23,
                                'parent_post_field' => 13,
                                'gv_id' => 20,
                                'title' => 'Request for Collaborator/Volunteer',
                            ),
                            array(
                                'gf_id' => 24,
                                'parent_post_field' => 9,
                                'gv_id' => 20,
                                'title' => 'Request for Funding',
                            ),
                            array(
                                'gf_id' => 25,
                                'parent_post_field' => 11,
                                'gv_id' => 20,
                                'title' => 'Request for Marketing/Publicity',
                            ),
                            array(
                                'gf_id' => 27,
                                'parent_post_field' => 2,
                                'gv_id' => 20,
                                'title' => 'Request for a Mentor',
                            ),
                        );
                    ?>
                    <?php foreach( $request_type_array as $request_item ){ ?>
                    
                    <div class="overflow-hidden padding-b-40 margin-b-40 border-b-1 border-color-darkgrey">
                        <div class="row row-40">
                            <div class="col-md-3 padding-lr-40">
                                <h2 class="txt-color-light txt-height-1-2">
                                    <?php echo $request_item['title']; ?>
                                </h2>
                                <div class="margin-t-20">
                                    <a 
                                       href="<?php printf('%s/request-for-help/?form-title=%s&gf-id=%s&parent_id=%s', $site_url, $request_item['title'], $request_item['gf_id'], $post_id); ?>" 
                                       class="txt-sm txt-underline txt-color-dark"
                                       target="_blank"
                                    >
                                        Add Request
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-9 padding-lr-40 txt-normal-s txt-height-1-5">
                            <?php
                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => $request_item['parent_post_field'], 'value' => $post_id, //Parent Post
                                    )
                                  )
                                );

                                /* Get GF Entry Count */
                                $order_count = GFAPI::count_entries( $request_item['gv_id'], $search_criteria );
                                $orders = GFAPI::get_entries( $request_item['gf_id'], $search_criteria );
                                                                          
                                foreach( $orders as $order ){
                                    $child_post_id = $order['post_id'];
                            ?>

                                <div class="row margin-b-15">
                                    <div class="col-md-8 txt-color-lighter">
                                        <h4 class="txt-lg txt-color-blue">
                                            <?php echo get_the_title($child_post_id); ?>
                                        </h4>
                                        <p class="txt-sm margin-t-5">
                                            <a
                                                href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$order['id'].'" view_id="'.$request_item['gv_id'].'" action="edit" return="url" /]').'&view=marketplace&form-title='.$request_item['title'].'&post_id='.$post_id; ?>" 
                                                class="txt-color-green padding-r-15"
                                                target="_blank"
                                            >
                                                <i class="fa fa-pencil"></i>
                                                Edit
                                            </a>
                                            <a href="" class="txt-color-red confirmation">
                                                <i class="fa fa-trash"></i>
                                                Delete
                                            </a>
                                        </p>
                                    </div>
                                    <div class="col-md-4 txt-medium txt-color-dark text-right">
                                        <a href="<?php the_permalink($child_post_id); ?>" class="txt-xs btn btn-blue no-m-b">
                                            View Request
                                        </a>
                                    </div>
                                </div>

                            <?php } ?>
                            </div>
                        </div>
                    </div>
                    
                    <?php } ?>
                </div>
            </div>
        </section>
        
        <?php } ?>
    </main>

<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>