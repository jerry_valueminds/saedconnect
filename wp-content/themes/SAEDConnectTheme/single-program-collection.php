    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
   
    <main class="main-content">
        <header class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        Re-invent your career. Join the Tech Revolution.
                        <br>
                        Start your career in technology.
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        Technology roles are becoming one of the highest-paying jobs globally, and you don’t require a college degree to be a tech star. The Tech Career Mentorship Scheme is designed to help people across all backgrounds to start up a brilliant career in tech related areas.
                    </h2>
                </div>
            </div>
        </header>
        
        <section class="bg-ash padding-t-80 padding-b-60 txt-light txt-color-white">
            <div class="container-wrapper">
                <header class="txt-light margin-b-60">
                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Why Pursue a Career in Tech?
                    </h2>
                    <!--<p class="txt-medium">
                        Find all you need to plan your business and get it off the ground.
                        <br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                    </p>-->
                </header>
                <div class="row row-10">
                    <div class="col-md-3 padding-lr-10 padding-b-20">
                        <figure class="margin-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/make-more-money-white.png" alt="" width="50">
                        </figure>
                        <h3 class="txt-bold margin-b-15">
                            Make more money
                        </h3>
                        <p class="txt-normal-s txt-height-1-7">
                            Technology careers are one of the highest-paying careers – from entry level. Even better, they don’t require a college degree to start.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-10 padding-b-20">
                        <figure class="margin-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/high-demand-white.png" alt="" width="50">
                        </figure>
                        <h3 class="txt-bold margin-b-15">
                            High Demand
                        </h3>
                        <p class="txt-normal-s txt-height-1-7">
                            It is Projected that there will be1 million job shortfall for IT roles within the next 10 years. Tech roles are also projected to grow two-thirds faster than the average for all jobs. Secure your place in this future.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-10 padding-b-20">
                        <figure class="margin-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/career-flexibility-white.png" alt="" width="50">
                        </figure>
                        <h3 class="txt-bold margin-b-15">
                            Career Flexibility
                        </h3>
                        <p class="txt-normal-s txt-height-1-7">
                            Tech Careers give you the freedom and flexibility to work from anywhere in the world. You can be in a remote town in Nigeria and be working with a firm in China.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-10 padding-b-20">
                        <figure class="margin-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/love-your-work-white.png" alt="" width="50">
                        </figure>
                        <h3 class="txt-bold margin-b-15">
                            Love Your Work
                        </h3>
                        <p class="txt-normal-s txt-height-1-7">
                            Tech work can be fun and stimulating. No 2 days are the same.  Wake up everyday and feel valuable, stimulated, and excited to open your laptop.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="padding-tb-40 bg-yellow">
            <div class="container-wrapper">
                <header class="txt-light margin-b-30">
                    
                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Different Entry Points to a great career
                    </h2>
                    <p class="txt-height-1-">
                        You can start your tech journey either as a Wordpress developer, a frontend web developer,
                        <br>
                        a full stack developer, or a digital marketer. Which ever your choice, there is a track for you.
                    </p>
                </header>
                <div class="row row-10">
                <?php 

                    $nav = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'program_collection_to_program',
                            'from' => get_the_ID(), // You can pass object ID or full object
                        ),
                        'posts_per_page' => 1,
                    ) );
                    while ( $nav->have_posts() ) : $nav->the_post();
                ?>   

                    <div class="col-md-4 padding-lr-10 padding-b-20">
                        <?php
                            $images = rwmb_meta( 'skilli-program-feautured-image', array( 'limit' => 1 ) );
                            $image = reset( $images );
                        ?>
                        <article
                            class="feature-image-block d-flex card-shadow-2"
                            style="background-image:url('<?php echo $image['full_url'];?>')"
                        >
                            <div class="content txt-color-white align-self-end">
                                <h3 class="txt-lg txt-medium txt-height-1-2 margin-b-20">
                                    <?php the_title() ?>
                                </h3>
                                <div class="btn-wrapper">
                                    <a class="btn btn-trans-wb" href="<?php the_permalink() ?>">
                                        Get Started
                                    </a>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
                </div>
            </div>
        </section>
        
        <section class="padding-t-80 padding-b-40">
            <div class="container-wrapper">
                <header class="padding-b-80">
                    <h1 class="txt-xxlg txt-bold txt-height-1-1 margin-b-20">
                        What am I getting?
                    </h1>
                    <h2 class="txt-height-1-5">
                        We are going all out to ensure you get all the support & resources you require to start and excel in your tech career.
                    </h2>
                </header>
                <div class="row row-40">
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/expert-instruction-and-guidance.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Expert Instruction & Guidance
                                </p>
                                <div class="collapse" id="collapse-1">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Learn with industry-vetted curriculum created by Subject-Matter Experts.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access to experienced Mentors available 12-14 hours/day during the week to help you in real time.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access useful Templates, Reference Guides & tailored advice from experts.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p>
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/immersive-learning.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10 txt-height-1-4">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Immersive Learning
                                </p>
                                <div class="collapse" id="collapse-2">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Instructor & Mentor Support.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access real-live entrepreneur interviews that help you appreciate the practical application of lessons shared.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Collaborative tasks.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p>
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/get-real-world-experience.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Get Real-World Experience
                                </p>
                                <div class="collapse" id="collapse-3">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    You will work on challenging projects to solve real problems during the sessions, 
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    You are challenged to define and develop a personal project to apply the skills being learnt.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/amazing-system-support.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Amazing Support System.
                                </p>
                                <div class="collapse" id="collapse-4">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Advisor Support beyond the end of the session
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Peer-to-Peer Motivation in the alumni community
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Tools, resources and programs that would make your tech career journey easier
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/career-support.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Career Support
                                </p>
                                <div class="collapse" id="collapse-5">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Get support to prepare you for the technical recruiting process
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Resume and portfolio critique and a review of your LinkedIn and GitHub profiles
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Mock Interviews
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Troubleshooting and work on weak areas
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/access-to-Growth-opportunities.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Access to Growth Opportunities
                                </p>
                                <div class="collapse" id="collapse-6">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Events and network meetings where you can meet other great tech gurus like you
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Discounts on online services like hosting, plugins, etc
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Be the first t know about internship and job openings for tech talent shared by partners and alumni
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="padding-t-80 padding-b-40 bg-grey">
            <div class="container-wrapper">
                <header class="padding-b-40">
                    <h1 class="txt-xxlg txt-bold txt-height-1-1 margin-b-20">
                        How the Tech Career Mentorship scheme Works
                    </h1>
                    <h2 class="txt-height-1-5">
                        The Tech Career Mentorship Scheme utilizes an innovative model to deliver
                        <br>
                        game changing support to you anywhere you are.
                    </h2>
                </header>
                <div class="how-it-works-tab">
                    <ul class="nav nav-tabs" id="skillTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="location-1-skill-tab-1" data-toggle="tab" href="#location-1-skill-1" role="tab" aria-controls="location-1-skill-1" aria-selected="true">
                                Blended Session
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="location-1-skill-tab-2" data-toggle="tab" href="#location-1-skill-2" role="tab" aria-controls="location-1-skill-2" aria-selected="false">
                                Offline Session
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="skillTabContent text-center">
                        <div class="tab-pane fade show active" id="location-1-skill-1" role="tabpanel" aria-labelledby="location-tab-1">
                            <div class="row row-10">
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/join-a-track.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Join a track
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Join a track of your choice before the deadline date of a new session. There are limited spaces available per session. You will be assigned a personal mentor for the session (optional)
                                    </p>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/participate-in-virtual-learning.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Participate
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Participate in virtual learning sessions delivered through the Telegram Messaging App (How to download the telegram App), WhatsApp & other third party Apps.
                                    </p>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/engage-in-all-the-session.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Engage
                                    </h3>
                                    <p class="txt-sm txt-height-1-7 margin-b-15">
                                        Engage in all the session activities and unlock your capacity 
                                    </p>
                                    <ul class="icon-list txt-sm">
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Complete all the assigned tasks and get feedback
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Participate in group collaborative sessions
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Get real-time advise and feedback from your assigned mentor
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/apply-the-lessons-learnt.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Apply the lessons learnt
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Apply the lessons learnt & resources provided to complete your personal project.  
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="location-1-skill-2" role="tabpanel" aria-labelledby="location-tab-2">
                            <div class="row row-10">
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="images/analyses.png" alt="" height="150">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Join a track
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Join a track of your choice before the deadline date of a new session. There are limited spaces available per session. You will be assigned a personal mentor for the session (optional)
                                    </p>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="images/squares.jpg" alt="" height="150">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Participate
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Participate in virtual learning sessions delivered through the Telegram Messaging App (How to download the telegram App), WhatsApp & other third party Apps.
                                    </p>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="images/analyses.png" alt="" height="150">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Engage
                                    </h3>
                                    <p class="txt-sm txt-height-1-7 margin-b-15">
                                        Engage in all the session activities and unlock your capacity 
                                    </p>
                                    <ul class="icon-list txt-sm">
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Complete all the assigned tasks and get feedback
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Participate in group collaborative sessions
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Get real-time advise and feedback from your assigned mentor
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="images/squares.jpg" alt="" height="150">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Apply the lessons learnt
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Apply the lessons learnt & resources provided to complete your personal project.  
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="bg-grey padding-t-80">
            <div class="container-wrapper">
                <header class="margin-b-100 txt-light">
                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Meet your Mentors
                    </h2>
                    <p class="txt-height-1-7">
                        The Tech Mentorship Scheme is led by actively-practicing,
                        <br>
                        world class programmers who actually make a living from coding.
                    </p>
                </header>
                <div class="row row-10 text-center">
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/onewoman.png" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Hussein Alayo
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/m_uwede.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Oguntade Samson
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/onewoman.png" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Tunde Omiwole
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Jerry Adaji
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Tayo Erubu
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Odugbemi
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="container-wrapper bg-ghostwhite padding-tb-60">
            <header class="margin-b-40">
                <h2 class="txt-xxlg txt-medium">Frequently Asked Questions</h2>    
            </header>
            <div class="row row-10">
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-1" aria-expanded="false" aria-controls="collapseExample">
                            How much does it cost to join a session?
                        </button>
                        <div class="collapse" id="faq-1">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-2" aria-expanded="false" aria-controls="collapseExample">
                            How much does it cost to join a session?
                        </button>
                        <div class="collapse" id="faq-2">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-3" aria-expanded="false" aria-controls="collapseExample">
                            How much does it cost to join a session?
                        </button>
                        <div class="collapse" id="faq-3">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-5" aria-expanded="false" aria-controls="collapseExample">
                            How much does it cost to join a session?
                        </button>
                        <div class="collapse" id="faq-5">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <?php endwhile; // end of the loop. ?>
    
    <?php get_footer() ?>