    
    
    <?php while ( have_posts() ) : the_post(); ?>
    <!-- Get Topic Area Type: This is used to determine the redirect link -->
    <?php $topic_area_type = rwmb_get_value( 'topic-area-type' ); ?>
               
    <?php if($topic_area_type == 'guide'){ ?>
                
        <!-- Loop: Find First Topic Area related to Topic -->
        <?php 

            $nav = new WP_Query( array(
                'relationship' => array(
                    'id'   => 'topic_area_to_article',
                    'from' => get_the_ID(), // You can pass object ID or full object
                ),
                'posts_per_page' => 1,
            ) );
            while ( $nav->have_posts() ) : $nav->the_post(); 

        ?>   

                <?php
                    if ( wp_redirect( get_permalink()) ) {
                        exit;
                    }
                ?>

        <?php
            endwhile;
            wp_reset_postdata();
        ?>
        <!-- Loop: END -->
        
    <?php }elseif($topic_area_type == 'faq'){ ?>
        
        <!-- Loop: Find First Topic Area related to Topic -->
        <?php 

            $nav = new WP_Query( array(
                'relationship' => array(
                    'id'   => 'topic_area_to_faq_article',
                    'from' => get_the_ID(), // You can pass object ID or full object
                ),
                'posts_per_page' => 1,
            ) );
            while ( $nav->have_posts() ) : $nav->the_post(); 

        ?>   

                <?php
                    if ( wp_redirect( get_permalink()) ) {
                        exit;
                    }
                ?>

        <?php
            endwhile;
            wp_reset_postdata();
        ?>
        <!-- Loop: END -->
        
    <?php }else{ ?>
        <?php get_template_part( 'template-parts/related-advice-template' ); ?>
    <?php } ?>


<?php endwhile; // end of the loop. ?>