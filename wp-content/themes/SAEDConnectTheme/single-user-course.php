    
<?php get_header() ?>

<?php
    // Get Base URL
    $base_url = get_site_url().'/my-courses-form';

    // Get Currently logged in User
    $current_user = wp_get_current_user();
?> 

<main class="main-content font-main">
   
    <section class="container text-center">
        <h1 class="txt-2em txt-medium margin-b-20">
            Course - <?php the_title(); ?>
        </h1>
    </section>
    
<?php
    
    // TO SHOW THE POST CONTENT
    while ( have_posts() ) : the_post();

    $mentor_post_id = get_the_ID();
    $mentor_entry_id = wp_strip_all_tags( get_the_content() );
    
?>
    <!-- Program Information -->
    <section class="padding-b-20">
       <div class="container">

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Course Information
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $mentor_entry_id );
                $entries = GFAPI::get_entries( 96, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-20">
                    <!-- Name of Individual / Organization -->
                    <?php
                        $meta = get_post_meta($post->ID, 'mentor-information-name', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Name of Individual/Organization
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <form action="<?php echo $base_url.'/?action=course-information-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Uploads -->
    <section class="padding-b-20">
       <div class="container">

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Uploads
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $mentor_entry_id );
                $entries = GFAPI::get_entries( 97, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-20">
                    <!-- Name of Individual / Organization -->
                    <?php
                        $meta = get_post_meta($post->ID, 'mentor-trainer-experience', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Number of Years of Training Experience
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <form action="<?php echo $base_url.'/?action=course-uploads-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Frequently Asked Questions -->
    <section class="padding-b-20">
       <div class="container">

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Frequently Asked Questions
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $mentor_entry_id );
                $entries = GFAPI::get_entries( 100, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-20">
                    <!-- Name of Individual / Organization -->
                    <?php
                        $meta = get_post_meta($post->ID, 'mentor-youth-service', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            How do you offer this service?
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <form action="<?php echo $base_url.'/?action=course-faqs-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Program Packages -->
    <section class="padding-b-20">
       <div class="container">

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Course Packages
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $mentor_entry_id );
                $entries = GFAPI::get_entries( 98, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-20">
                    <!-- Name of Individual / Organization -->
                    <?php
                        $meta = get_post_meta($post->ID, 'mentor-information-name', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Name of Individual/Organization
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <form action="<?php echo $base_url.'/?action=course-package-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Program Schedule -->
    <section class="padding-b-20">
       <div class="container">

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Course Schedule
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $mentor_entry_id );
                $entries = GFAPI::get_entries( 99, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-20">
                    <!-- Name of Individual / Organization -->
                    <?php
                        $meta = get_post_meta($post->ID, 'mentor-information-name', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Name of Individual/Organization
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <form action="<?php echo $base_url.'/?action=course-schedule-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>

<?php
    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
?>

</main>    
<?php get_footer() ?>


