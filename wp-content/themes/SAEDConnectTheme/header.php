<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 

    
    <meta name="description" content="">
    <meta name="author" content="saedconnect.org">
    
    <!-- Graph Meta Tags -->
    <?php 
        /* Open Graph */
        global $ogLink;
        global $ogTitle;
        global $ogDescription;
        global $ogImage;
    ?>
    <meta property="og:url" content="<?php echo $ogLink ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?php echo $ogTitle ?>" />
    <meta property="og:description" content="<?php echo $ogDescription ?>" />
    <meta property="og:image" content="<?php echo $ogImage ?>" />
    
    <meta name="twitter:title" content="<?php echo $ogTitle ?>">
    <meta name="twitter:description" content="<?php echo $ogDescription ?>">
    <meta name="twitter:image" content="<?php echo $ogImage ?>">
    <meta name="twitter:site" content="@SAEDConnect">
    <meta name="twitter:creator" content="@SAEDConnect">
    
    <?php 
        $queried = get_queried_object();
        $pageName = $queried->post_title;
    ?>
	<title>SAEDConnect - <?php echo ( $pageName ) ? $pageName : "Activating Africa's Youth Human Capital"; ?></title>
	
	<!--Site icon-->
	<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" type="image/x-icon">
	
    <!--Load Styles-->
	<?php wp_head(); ?>
	
	<!-- JSCookie CDN -->
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    
    <!-- JQuery CDN -->
	<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    
    <!-- include summernote css/js -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.39/jodit.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.39/jodit.min.js"></script>
    <script>
        $(document).ready(function() {

            /*$('.editor').each(function () {
                var editor = new Jodit(this);
            });*/
        })
    </script>
    
    <script>
        $(document).ready(function() {

            $(".remember-page, #gform_submit_button_0").click(function(){
                /* Set Previoys Page Cookie */
                Cookies.set('previous-page', window.location.href);
            });
        })
    </script>
    
    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/_all.css" integrity="sha256-808LC4rdK/cu4RspCXPGrLKH7mgCcuNspF46UfBSbNQ=" crossorigin="anonymous" />
	
	<!--FontAwesome CDN-->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
	<style>
        .bbp-breadcrumb{
            display: none !important;
        }
        
        .freshness-link a{
            color: white !important;
        }
        
        .bbp-forum-content{
            font-size: 0.8rem !important;
            margin-top: 10px;
        }
        
        /* Reply */
        .reply-wrapper a {
            font-weight: 500;
        }

        .reply-icon a{
            display: block;
            position: relative;
            height: 1em;
            width: 1em;
            padding-left: 1.5em;
            font-size: 0.7rem;
            font-weight: 500;
            margin-bottom: 10px;
        }

        .reply-icon a:after{
            font-family: FontAwesome;
            content: '\f112';
            position: absolute;
            top: 0;
            left: 0;

            color: inherit;
            font-size: 0.9em;
        }
        
        .reply-icon.edit a:after{
            content: '\f040';
        }
        
        .reply-icon.trash a:after{
            content: '\f014';
        }
        
        .reply-icon.spam a:after{
            content: '\f071';
        }
        
        /* Disable Template Notice */
        .bbp-template-notice{
            display: none !important;
        }
        
        /* BBP Form */
        .bbp-form{}
        
        .bbp-form label{
            font-size: 0.9rem;
            font-weight: 500;
            margin-bottom: 0.5rem;
        }
        
        #bbp_topic_title{
            display: block !important;
            width: 100% !important;
            border: 1px solid gainsboro !important;
            border-radius: 0.3rem;
            outline: 0;
            padding: 0.4rem 0.6rem;
            font-size: 0.9rem;
        }
        
        .bbp-form div.bbp-submit-wrapper{
            float: left;
        }
        
        .bbp-form .button{
            font-size: 0.9em;
            font-weight: 500;
            padding: 0.8em 2em;
            border: 0;
            outline: 0;
            background-color: darkgray;
            float: left;
            border-radius: 20px;
            cursor: pointer;
            transform: 0.3s all ease;
        }
        
        .bbp-form .button:hover{
            background-color: gainsboro;
            transform: 0.3s all ease;
        }
    </style>
    
    <style>
        .con-list li{
            display: flex;
            line-height: 1.4;
        }

        .con-list li span{
            flex: 1;
            padding-left: 5px;
        }

        .con-list li i{
            padding-top: 0.3em;
        }
    </style>
    
    <style>
        .main-content, .main-footer{
            display: none;
        }
        
        .pre-loader{
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        
        .modal-header{
            color: white;
            background-color: #4a4a4a;
        }        
        
        .modal-header button{
            color: white;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('.main-content, .main-footer').fadeIn();
            $('.pre-loader').fadeOut();
        });
    </script>
	
</head>
<body>
    <div class="pre-loader"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/spinner.svg" alt="pre-loader"></div>
	<header class="main-navigation font-main" id="myHeader">
        <div class="navigation-wrapper">
            <a class="brand" href="https://www.saedconnect.org/">
                <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                <span class="name">
                    SAEDConnect
                </span>
                <span class="txt-sm txt-color-dark padding-l-10 margin-l-10 border-l-1">
                    In Partnership
                    <br>
                    with the NYSC
                </span>
            </a>
            <div class="action-btn-wrapper">
                
                <div class="search">
                    <div class="search-btn">
                        <i class="fa fa-search"></i>
                    </div>
                    <form action="" class="search-form">
                        <div class="wrapper">
                            <input class="search-box" type="search" placeholder="Type to Search">
                            <div class="close-btn">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-times fa-stack-1x"></i>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- User Account -->
                <div class="user-account">
                <?php
                    
                    if ( is_user_logged_in() ) {
                        $current_user = wp_get_current_user();
                ?>
                        
                    <div class="dropdown">
                        <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="account-info">
                                <?php
                                    /* Get Avatar */
                                    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                    $meta_key = 'user_avatar_url';
                                    $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

                                    if($get_avatar_url){
                                        $avatar_url = $get_avatar_url;
                                    }

                                    /* Get User Display Name */
                                    switch_to_blog(1);

                                    $gf_id = 4; //Form ID
                                    $entry_count = 0;

                                    /* GF Search Criteria */
                                    $search_criteria = array(

                                    'field_filters' => array( //which fields to search

                                        array(

                                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                            )
                                        )
                                    );

                                    /* Get Entries */
                                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                    /* Get GF Entry Count */
                                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                                    if($entry_count){ //If no entry
                                        foreach( $entries as $entry ){          
                                            $displayname = rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' );
                                        }                
                                    }   
                                
                                    restore_current_blog();
                                ?>
                                <figure class="profile-image" style="background-image: url('<?php echo $avatar_url; ?>')"></figure>
                                <span class="profile-name" style="text-transform:capitalize">
                                    <?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </span>
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                            <a class="dropdown-item" href="https://www.saedconnect.org/welcome">
                                Dashboard
                            </a>
                            <a class="dropdown-item" href="https://www.saedconnect.org/competency-profile/profile/">
                                My Profile
                            </a>
                            <?php if($current_user->ID == 1){ ?>
                                <a class="dropdown-item" href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information">
                                    Old Dashboard
                                </a>
                            <?php } ?>
                            <a class="dropdown-item" href="<?php echo wp_logout_url( 'https://www.saedconnect.org' ); ?> ">
                                Logout
                            </a>
                        </div>
                    </div>
                        
                <?php } else { ?>        

                    <a class="login" href="https://www.saedconnect.org/login">Login</a>
                    <a class="signup" href="https://www.saedconnect.org/register">Sign up</a>
                    
                <?php } ?>
                </div>
                <button class="menu-btn new hamburger hamburger--spring d-block d-sm-none">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>

        <nav class="navigation-list-wrapper left">
           
            <!-- Left -->
            <ul class="navigation-list">
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'find-your-path')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(573); ?>">
                        Find Your Path
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Discover Yourself
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Use our career guidance online tool to gain clarity about your strengths & weaknesses, and discover viable careers that match your talents.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" data-toggle="modal" href="#comingSoonModal">Start Here</a>  
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Get Counseling
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Let our counselors work with you to uncover your strengths, interpret your passions and provide objective and professional advice about optimal career paths to choose.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/about/reference/purpose-discovery-services/">Start Here</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Self Discovery Guide
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Dive into concepts & explanations that help you appreciate your uniqueness and guide you in making informed career decisions.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/self-discovery-guide/">Start Here</a>  
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
                <li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/start-your-business/">
                        Learning & Mentorship
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header padding-b-15">
                                        Programs
                                    </h4>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/information/information-page/learning-for-earning/">
                                                Learning for Earning
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Hot courses designed to help you start earning.
                                        </div>
                                    </div>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/find-your-path/">
                                                Find your Path
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            The journey to a successful life starts with a step in the right direction. Find your right direction.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Learn / Train
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/service-provider-directory/trainers/">
                                                Find Trainers
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Available Training Courses
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/post-a-training-request/">
                                                Request for a Training
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/become-a-trainer/">
                                                Become a Trainer
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                View Training Requests
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6 padding-lr-20">
                                    <h4 class="sub-menu-header padding-b-15">
                                        Mentorships
                                    </h4>
                                    <div class="row row-15">
                                        <div class="col-lg-6 padding-lr-15 margin-b-20">
                                            <div class="txt-sm txt-bold margin-b-10">
                                                <!--<a href="https://www.saedconnect.org/growth-programs/programs/the-entrepreneurship-incubator/">
                                                    The Entrepreneurship Incubator
                                                </a>-->
                                                <a href="https://www.saedconnect.org/tei-mentor-hub/">
                                                    The Entrepreneurship Incubator
                                                </a>
                                            </div>
                                            <div class="txt-sm txt-height-1-4">
                                                Generate, build & launch your business ideas with help from expert mentors.
                                            </div>
                                        </div>
                                        <div class="col-lg-6 padding-lr-15 margin-b-20">
                                            <div class="txt-sm txt-bold margin-b-10">
                                                <a href="https://www.saedconnect.org/growth-programs/programs/side-hustle-skills/">
                                                    Side Hustle Mentor Hub
                                                </a>
                                            </div>
                                            <div class="txt-sm txt-height-1-4">
                                                Get expert support to start & grow any of 40+ different businesses & get it right the first time.
                                            </div>
                                        </div>
                                        <div class="col-lg-6 padding-lr-15 margin-b-20">
                                            <div class="txt-sm txt-bold margin-b-10">
                                                <a href="https://www.saedconnect.org/growth-programs/programs/business-clinic/">
                                                    Business Clinic
                                                </a>
                                            </div>
                                            <div class="txt-sm txt-height-1-4">
                                                Whatever your business challenge, get expert help to solve them & take your business to the next level.
                                            </div>
                                        </div>
                                        <div class="col-lg-6 padding-lr-15 margin-b-20">
                                            <div class="txt-sm txt-bold margin-b-10">
                                                <a href="https://www.saedconnect.org/growth-programs/programs/career-forum/">
                                                    Career Advisor
                                                </a>
                                            </div>
                                            <div class="txt-sm txt-height-1-4">
                                                Get guidance on everything you need to build a super CV, find those amazing jobs & blast your interviews.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/start-your-business/">
                        HelpSquare
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light  margin-b-20">
                                        Request help for your business / project
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        What do you need to take your business/idea to the next level? There is someone willing to help. Make your request.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="	https://www.saedconnect.org/information/information-page/request-help-to-actualize-your-business-or-project/">Get Started</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light  margin-b-20">
                                        Give Help
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Give Funding, Offer Mentorship, Offer Workspace/Office Space / Factory Space, Offer a tool/equipment, Offer Land & Landed Property, Offer Marketing & Publicity Support, Volunteer on a project. 
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="https://www.saedconnect.org/information/information-page/give_help/">Get Started</a>
                                </div>
                                <div class="col-md-6 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Explore Help Requests
                                    </h4>
                                    <ul class="sub-menu-list row">
                                        <li class="col-lg-6">
                                            <a href="https://www.saedconnect.org/ventures-directory/funding-requests/">
                                                Funding Requests
                                            </a>
                                        </li>
                                        <li class="col-lg-6">
                                            <a href="https://www.saedconnect.org/ventures-directory/mentor-requests/">
                                                Mentorship Requests
                                            </a>
                                        </li>
                                        <li class="col-lg-6">
                                            <a href="https://www.saedconnect.org/ventures-directory/workspace-requests/">
                                                Workspace/Office Space/Factory Space Requests
                                            </a>
                                        </li>
                                        <li class="col-lg-6">
                                            <a href="https://www.saedconnect.org/ventures-directory/tool-requests/">
                                                Tool/equipment Requests
                                            </a>
                                        </li>
                                        <li class="col-lg-6">
                                            <a href="https://www.saedconnect.org/ventures-directory/land-requests/">
                                                Land & Landed Property Requests
                                            </a>
                                        </li>
                                        <li class="col-lg-6">
                                            <a href="https://www.saedconnect.org/ventures-directory/marketing-requests/">
                                                Marketing/Publicity Support Requests
                                            </a>
                                        </li>
                                        <li class="col-lg-6">
                                            <a href="https://www.saedconnect.org/ventures-directory/volunteer-requests/">
                                                Request for Volunteers
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/start-your-business/">
                        Skills/Job Market
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header padding-b-15">
                                        Skills Market
                                    </h4>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/information/information-page/convert-your-skillhobby-into-money/">
                                                Sell your Skills
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Somebody needs your skills/ hobby no matter how small. Convert your skills/hobby into cash
                                        </div>
                                    </div>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="#">
                                                Find a skilled person 
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Explore our talent database for people who meet your criteria
                                        </div>
                                    </div>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/service-marketplace/">
                                                Freelancer Marketplace 
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Explore affordable service offers from freelancers near you.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Explore Jobs
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="#">
                                                View Full-time/Project Jobs
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                View Freelancer Jobs
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                View Training Jobs
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Business Partner Openings
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/join-the-saedconnect-mobile-workforce/">
                                                Join the Mobile workforce
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header padding-b-15">
                                        Job Programs 
                                    </h4>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/information/information-page/jobtrac/">
                                                JobTrac
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            The JobTrac program placement screens for skilled young people and connects them to work opportunities.
                                        </div>
                                    </div>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/information/information-page/sales-consultant-program/">
                                                Sales Consultant Program
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Earn extra income while boosting your selling skills and even land yourself a job
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Find Talent
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/post-a-job/">
                                                Post a Job
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Explore our talent database
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/service-marketplace/">
                                                Explore the Freelancer marketplace
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/lets-help-you-power-your-youth-program/">
                                                Host a talent contest
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/talent-services/">
                                                SAEDConnect Talent Services
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/start-your-business/">
                        Ventures
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light margin-b-20">
                                        List your business/project
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Get connected to investors, collaborators, and customers who can support & patronize your business.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="https://www.saedconnect.org/information/information-page/list-your-business-on-the-ventures-directory/">Get Started</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light margin-b-20">
                                        Explore the Business Directory 
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="https://www.saedconnect.org/ventures-directory/business-ventures/">Get Started</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light margin-b-20">
                                        Explore the Project Directory 
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="https://www.saedconnect.org/ventures-directory/personal-projects-directory/">Get Started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="hasSubMenu">
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/opportunity-center/">
                        Opportunity Center
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <!--<div class="">
                                <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/">
                                    All
                                </a>
                            <?php 
                                //Switch to Multi-site
                                switch_to_blog(12);

                                //Get Terms
                                $terms = get_terms( 'program-type', array('hide_empty' => false, 'parent' => 0)); //Get all the terms

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;

                                    $term_id = $term->term_id; //Get the term ID
                                    $term_slug = 'program-type';
                                    $term_name = $term->name; //Get the term name
                                    $term_url = get_term_link($term);
                            ?>
                                    
                                <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/?s-program-type=<?php echo $term_id ?>">
                                    <?php echo $term_name; ?>
                                </a>
                            <?php 
                                } 
                                
                                //Revert to Previous Multisite
                                restore_current_blog();
                            ?>
                            </div>-->
                            <div class="row row-20">
                                <div class="col-md-12 padding-lr-20">
                                    <div class="">
                                        <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/">
                                            All
                                        </a>

                                        <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/?s-program-type=2612">
                                            Access to Market Programs 
                                        </a>

                                        <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/?s-program-type=2634">
                                            Ask the Expert 
                                        </a>

                                        <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/?s-program-type=2606">
                                            Business Opportunities
                                        </a>

                                        <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/?s-program-type=2610">
                                            Competitions
                                        </a>

                                        <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/?s-program-type=2604">
                                            Events
                                        </a>

                                        <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/?s-program-type=2611">
                                            Financial Support
                                        </a>

                                        <a class="btn btn-trans-green txt-sm" href="https://www.saedconnect.org/opportunity-center/?s-program-type=2605">
                                            Growth Programs
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                                
                <!--<li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/start-your-business/">
                        Communities
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="https://www.saedconnect.org/ventures-directory/">
                                            Entrepreneurship Communities
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/tei-forum/forums/forum/entrepreneurship-101/">
                                                Entrepreneurship 101
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/tei-forum/forums/forum/finding-your-business-idea//">
                                                Finding a great business Idea
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/tei-forum/forums/forum/validate-build/">
                                                Planning, Developing &amp; Activating your Business Idea
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/tei-forum/forums/forum/going-live/">
                                                Going Live
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="https://www.saedconnect.org/ventures-directory/">
                                            Career Communities
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/job-advisor-forum/forums/forum/ace-the-interviews/">
                                                Preparing for, &amp; Acing your interviews
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/job-advisor-forum/forums/forum/job-search/">
                                                Searching &amp; Applying for Jobs
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/job-advisor-forum/forums/forum/cv-and-cover-letters/">
                                                Crafting your CV, Social Profiles &amp; Cover Letters
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light margin-b-20">
                                        Side Hustle Communities
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        The journey ro a successful life starts with a step in the right direction. Find your right direction
                                    </p>
                                    <a 
                                        class="btn btn-trans btn-trans-blue txt-sm" 
                                        data-toggle="modal"
                                        href="#communityModal"
                                    >Get Started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
                
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="https://www.saedconnect.org/about/synopsis/find-your-path/">
                        Find Your Path
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </li>-->
                
                <!--<li class="hasSubMenu">
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'get-a-job')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/get-your-job">
                        Get a Job
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-6 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Career Development Programs
                                    </h4>
                                    <div class="row row-10">
                                        <div class="col-12 col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/JobTrac_program.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/jobtrac/">
                                                            JobTrac
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block  txt-sm txt-height-1-1">
                                                        Preparing young people for work & life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/successKeys_program.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/successkeys/">
                                                            SuccessKeys
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block  txt-sm txt-height-1-1">
                                                        Preparing young people for work & life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/job_interview_preparation.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/program/career-forum/">
                                                            JobAdvisor
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block  txt-sm txt-height-1-1">
                                                        Create an amazing CV for yourself.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="row row-5 sub-menu-image-box padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/staff_source.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="https://www.saedconnect.org/esaed/mini-site/talentsource/">
                                                    The Job Challenge
                                                </a>
                                            </h4>
                                            <p class="txt-sm txt-height-1-1">
                                                Get hired for specific job roles.
                                            </p>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Tools
                                    </h4>
                                    
                                    <div class="row row-5 sub-menu-image-box padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/cv_cover_letter_service.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="http://www.saedconnect.org/esaed/mini-site/tutorlink/">
                                                    Online CV Creator
                                                </a>
                                            </h4>
                                            <p class="txt-sm txt-height-1-1">
                                                Create an amazing CV for yourself.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row row-5 sub-menu-image-box padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/service_provider_directory.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="https://www.saedconnect.org/esaed/mini-site/talentsource/">
                                                    Database of Stars
                                                </a>
                                            </h4>
                                            <p class="txt-sm txt-height-1-1">
                                                Get exposed to local & international employers.
                                            </p>
                                        </div>
                                    </div>
                                </div>   
                            
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/career-guide">
                                            Job Search & Career Development Guide
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 3)
                                        switch_to_blog(3);
                                                                                                        
                                        //WP Query to get Guides
                                        
                                        $temp = $wp_query; $wp_query= null;
                                        $wp_query = new WP_Query();
                                        $wp_query->query(array('post_type' => 'guide-section'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php echo the_permalink() ?>">
                                                <?php echo the_title() ?>
                                            </a>
                                        </li>
                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                        endwhile;
                                        wp_reset_postdata();
                                        wp_reset_query();
                                        $wp_query = $temp;
                                                                                                        
                                    ?>
                                    </ul>
                                </div>
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Get Expert Help
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/cv-cover-letter-prep//">
                                                CV & Cover letter Creation & Review
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/linkedin-page-creation-review-networking-coaching/">
                                                Linkedin Profile Creation & Review
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/job-search-coaching/">
                                                Job Search Coaching
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/interview-prep/">
                                                Interview Coaching
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
                
                <!--<li class="hasSubMenu">
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/start-your-business/">
                        Do Business
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                               
                                <div class="col-md-6 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Entrepreneurship Development Programs
                                    </h4>
                                    <div class="row row-10">
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/side_hustle_communities.jpeg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/small-business-communities/">
                                                            Side Hustle Communities
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Meet with fellow Entrepreneurs accross over 40 businesses nationwide.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/join_a_professional_association.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://saedconnect.org/growth-programs/small-business-mentor/">
                                                            Small Business Mentor
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Get all the help you need to start and grow your business.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/the_entrepreneurship_incubator.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/the-entrepreneurship-incubator/">
                                                            The Entrepreneurship Incubator
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Generate your brilliant idea & build it into a thriving business.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/get_business_counseling.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/business-clinic/">
                                                            The Business Clinic
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Start earning from a small business of your choice.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/join_a_professional_association.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/program/side-hustle-skills/">
                                                            Side Hustle Mentor Hubs
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Connect with expert mentors to help you start and grow your side hustle.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Venture ToolPack
                                    </h4>
                                    <div class="row sub-menu-image-box padding-b-10">
                                        <figure class="image-item">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/cv_cover_letter_service.jpg" alt="">
                                        </figure>

                                        <p class="txt-sm margin-t-5">
                                            A suit of tools to structure and showcase your business to investors and potential partners
                                        </p>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/entrepreneurship-guide/">
                                            Doing Business Guide
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 2)
                                        switch_to_blog(2);
                                                                                                        
                                        //WP Query to get Guides
                                        $temp = $wp_query; $wp_query= null;
                                        $wp_query = new WP_Query();
                                        $wp_query->query(array('post_type' => 'guide-section'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php echo the_permalink() ?>">
                                                <?php echo the_title() ?>
                                            </a>
                                        </li>
                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                        endwhile;
                                        wp_reset_postdata();
                                        wp_reset_query();
                                        $wp_query = $temp;
                                    ?>
                                    </ul>
                                </div>
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Get Expert Help
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-development-consulting/">
                                                Business Counseling
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-plan-creation/">
                                                Business Plan Creation
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/raise-finance-for-your-business/">
                                                Raise Finance for your Business
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-registrations/">
                                                Business Registration & Legal Services
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-branding/">
                                                Business Branding
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
                
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="https://www.saedconnect.org/service-provider-directory/trainers/">
                        Learn a Skill
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </li>-->
                <!--
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'make-money')
                                echo 'active';
                            }
                        ?>" href="https://www.saedconnect.org/programs/make-money/">
                        Make Money
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                -->
                <!--<li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'learn-a-skill')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/learn-a-skill">
                        Growth Programs
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <h4 class="txt-xlg txt-bold txt-height-1-4">
                                        Innovative programs to help you develop & activate your employability & entrepreneurship capacity.
                                    </h4>
                                </div>
                                <div class="col-md-9 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Available Programs
                                    </h4>
                                    <div class="row row-10">
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/school_oversea.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/side-hustle-skills/">
                                                            Side Hustle Communities
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Start earning from a small business of your choice.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/successKeys_program.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/successkeys/">
                                                            SuccessKeys
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Preparing young people for work & life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/staff_source.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/esaed/mini-site/talentsource/">
                                                            The Job Challenge
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Get hired for specific job roles.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/the_entrepreneurship_incubator.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/the-entrepreneurship-incubator/">
                                                            The Entrepreneurship Incubator
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Generate your brilliant idea & build it into a thriving business.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/get_business_counseling.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/business-clinic/">
                                                            The Business Clinic
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Start earning from a small business of your choice.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/JobTrac_program.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/jobtrac/">
                                                            JobTrac
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Preparing young people for work & life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/job_interview_preparation.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/program/career-forum/">
                                                            JobAdvisor
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Create an amazing CV for yourself.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/school_oversea.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/skill-splash/program/tech-career-mentorship/">
                                                            Tech Mentorship Scheme
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Join the revolution. Start a career in tech.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
            </ul>
            
            <!-- Right -->
            <ul class="navigation-list">
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="http://www.saedconnect.org/esaed">
                        eSAED
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                -->
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="https://www.saedconnect.org/young-leaders-network/">
                        Young Leaders Network
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </li>-->
                <!--<li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'get-a-job')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/get-your-job">
                        Engage
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light  margin-b-20">
                                        Discover your Career Direction
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        The journey to a successful life starts with a step in the right direction. Find your right direction
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="https://www.saedconnect.org/find-your-path/">Get Started</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/growth-programs/programs/the-entrepreneurship-incubator/">
                                                The Entrepreneurship Incubator
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Generate, build & launch your business ideas with help from expert mentors.
                                        </div>
                                    </div>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/growth-programs/programs/side-hustle-skills/">
                                                Side Hustle Mentor Hub
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Get expert support to start & grow any of 40+ different businesses & get it right the first time.
                                        </div>
                                    </div>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/growth-programs/programs/business-clinic/">
                                                Business Clinic
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Whatever your business challenge, get expert help to solve them & take your business to the next level.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-bold margin-b-10">
                                            <a href="https://www.saedconnect.org/growth-programs/programs/career-forum/">
                                                Career Advisor
                                            </a>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            Get guidance on everything you need to build a super CV, find those amazing jobs & blast your interviews.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
                <li>
                    <a
                        class="blue"
                        href="https://www.saedconnect.org/young-leaders/">
                        Young Leaders Network
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                    >
                        Engage
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Partner with Us
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/corporates/">
                                                Corporates & Donor Organizations
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/schools/">
                                                Schools & Youth Institutions
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/foundations/">
                                                Foundations & Donor Organizations
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/high-net-worth-individuals/">
                                                High Networth Individuals
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/support-youth-development-overview/">
                                            Leverage our Network
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/a_list-on-the-opportunity-directory/">
                                                Promote your youth program
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/find-talent/">
                                                Find Talent
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/offer-your-youth-development-services/">
                                                Offer your youth development services
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/gather-authentic-data-nationwide/">
                                                Collect Data Nationwide
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/run-a-campaign/">
                                                Run a campaign
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/crowd-source-inputs/">
                                                Crowdsource inputs
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/extend-your-product-distribution-network/">
                                                Extend your Distribution
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/csr/">
                                                Execute & Scale your CSR Projects
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/give_help/">
                                                Support a Young Person
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/support-youth-development-overview/">
                                            Power Your Youth Program
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/consulting/">
                                                Program Development Consulting Support
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/a_list-on-the-opportunity-directory/">
                                                Promote your youth program
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/receive-and-manage-your-applications/">
                                                Application Management
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/information/information-page/get-execution-support/">
                                                Program Execution Support
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                
                                <!--<div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/trainings-overview/">
                                            Offer Trainings & Youth Services
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/join-the-trainer-directory/">
                                                Join the Trainer Directory
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/list-your-offers-events-and-special-programs-on-our-opportunity-directory/">
                                                List your offers, events, and special programs on our opportunity Directory
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/join-the-service-provider-directory/">
                                                Join the Service Provider Directory
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="sub-menu-list">               
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 8)
                                        switch_to_blog(11);

                                        $connected = new WP_Query( array(
                                            'relationship' => array(
                                                'id'   => 'mini_site_to_two_c_m_page',
                                                'from' => 608, // You can pass object ID or full object
                                            ),
                                            'nopaging' => true,
                                        ) );
                                        while ( $connected->have_posts() ) : $connected->the_post(); ?>
                                            <li>
                                               <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                            </li>
                                    <?php
                                        endwhile;
                                        wp_reset_postdata();
                                                                                                        
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Ways to Support
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/execute-or-co-create-a-youth-development-program/">
                                                Execute or Co-create a Youth Development Program
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/support-with-sensitization/">
                                                Inspire & Promote Excellence
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/share-your-story/">
                                                Become a Mentor/Coach
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/lend-your-expertise/">
                                                Lend Your Expertise
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/support-a-project/">
                                                Support a Project
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/volunteer/">
                                                Volunteer
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="sub-menu-list">               
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 8)
                                        switch_to_blog(11);

                                        $connected = new WP_Query( array(
                                            'relationship' => array(
                                                'id'   => 'mini_site_to_two_c_m_page',
                                                'from' => 625, // You can pass object ID or full object
                                            ),
                                            'nopaging' => true,
                                        ) );
                                        while ( $connected->have_posts() ) : $connected->the_post(); ?>
                                            <li>
                                               <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                            </li>
                                    <?php
                                        endwhile;
                                        wp_reset_postdata();
                                                                                                        
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/donate-overview/">
                                            Donate
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="http://www.saedconnect.org/contribute/synopsis/donate-ways-to-give/">
                                                Ways to Give
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/contribute/synopsis/donate-online/">
                                                Donate Online
                                            </a>
                                        </li>
                                    </ul>
                                </div>-->
                                
                            </div>
                        </div>
                    </div>
                </li>
                <li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/start-your-business/">
                        About
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light margin-b-20">
                                        Who we are
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        We leverage technology & partnerships to develop & scale youth empowerment interventions.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="https://www.saedconnect.org/information/information-page/about-saedconnect/">Get Started</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light margin-b-20">
                                        Partnerships
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="https://www.saedconnect.org/information/information-page/saedconnect-partnerships/">Get Started</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold txt-color-light margin-b-20">
                                        Support our impact programs
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue txt-sm" href="https://www.saedconnect.org/information/information-page/support-our-impact-programs/">Get Started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/about/synopsis/about-saedconnect-overview/">
                        About
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </li>-->
            </ul>
            
            <!-- User Account -->
            <div class="user-account padding-t-30">
            <?php

                if ( is_user_logged_in() ) {
                    $current_user = wp_get_current_user();
            ?>

                <div class="dropdown">
                    <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="account-info">
                            <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="">
                            <span class="profile-name">
                                <?php echo $current_user->display_name ?>
                            </span>
                        </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                        <a class="dropdown-item" href="https://www.saedconnect.org/my-dashboard/growth-programs/?action=career-dashboard">
                            My Career Dashboard
                        </a>
                        <a class="dropdown-item" href="https://www.saedconnect.org/growth-programs/my-dashboard/">
                            My Entrepreneurship Dashboard
                        </a>
                        <a class="dropdown-item" href="<?php echo wp_logout_url( 'https://www.saedconnect.org' ); ?> ">
                            Logout
                        </a>
                    </div>
                </div>

            <?php } else { ?>        

                <a class="login" href="https://www.saedconnect.org/login">Login</a>
                <a class="signup" href="https://www.saedconnect.org/register">Sign up</a>

            <?php } ?>
            </div>
        </nav>
	</header>
	<header class="mobile-navigation">
        <div class="top">
            <a class="brand" href="https://www.saedconnect.org/">
                <img class="icon" src="https://www.saedconnect.org/wp-content/themes/SAEDConnectTheme/images/icons/logo.png" alt="">
                <span class="name">
                    SAEDConnect
                </span>
                <span class="txt-sm txt-color-white padding-l-10 margin-l-10 border-l-1">
                    In Partnership
                    <br>
                    with the NYSC
                </span>
            </a>
            <button class="menu-btn hamburger hamburger--spring d-block d-sm-none">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/close.png" alt="Close icon">
            </button>
        </div>
        <ul class="navigation-list">
            <li>
                <a class="" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Young People
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="sub-list">
                        <li>
                            <a href="#">Anim pariatur cliche</a>
                        </li>
                        <li>
                            <a href="#">reprehenderit, enim eiusmod high</a>
                        </li>
                        <li>
                            <a href="#">life accusamus terry</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a class="" data-toggle="collapse" href="#collapseExample-2" role="button" aria-expanded="false" aria-controls="collapseExample-2">
                    Corporates & Service providers
                </a>
                <div class="collapse" id="collapseExample-2">
                    <ul class="sub-list">
                        <li>
                            <a href="#">Anim pariatur cliche</a>
                        </li>
                        <li>
                            <a href="#">reprehenderit, enim eiusmod high</a>
                        </li>
                        <li>
                            <a href="#">life accusamus terry</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a class="" data-toggle="collapse" href="#collapseExample-3" role="button" aria-expanded="false" aria-controls="collapseExample-3">
                    Young Leaders Network
                </a>
                <div class="collapse" id="collapseExample-3">
                    <ul class="sub-list">
                        <li>
                            <a href="#">Anim pariatur cliche</a>
                        </li>
                        <li>
                            <a href="#">reprehenderit, enim eiusmod high</a>
                        </li>
                        <li>
                            <a href="#">life accusamus terry</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a class="" data-toggle="collapse" href="#collapseExample-4" role="button" aria-expanded="false" aria-controls="collapseExample-4">
                    About
                </a>
                <div class="collapse" id="collapseExample-4">
                    <ul class="sub-list">
                        <li>
                            <a href="#">Anim pariatur cliche</a>
                        </li>
                        <li>
                            <a href="#">reprehenderit, enim eiusmod high</a>
                        </li>
                        <li>
                            <a href="#">life accusamus terry</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <div class="user-account padding-t-30">
            <a class="login" href="https://www.saedconnect.org/login">Login</a>
            <a class="signup" href="https://www.saedconnect.org/register">Sign up</a>
        </div>
    </header>
	
	<?php switch_to_blog(20); ?>

    <!-- SHC Modal -->
    <div class="modal fade font-main shc-modal" id="communityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="container-wrapper">
                    <div class="col-md-10 mx-auto">
                        <div class="padding-tb-80">
                            <div class="margin-b-40">
                                <h2 class="txt-2em txt-bold txt-height-1-2">
                                    What Side Hustle Community
                                    <br>
                                    are you interested in?
                                </h2>
                            </div>
                            <ul class="row row-20 shc-list txt-normal-s txt-medium">
                            <?php


                                $shc_query = new WP_Query();
                                $shc_query->query(array('post_type' => 'information-session'));
                                    while ($shc_query->have_posts()) : $shc_query->the_post();                
                            ?>

                                <div class="col-md-4 padding-lr-20">
                                    <li>
                                        <a href="<?php echo rwmb_meta( 'info-session-community-forum' ); ?>">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                <?php the_title() ?>
                                            </span>
                                        </a>
                                    </li>
                                </div>

                            <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php restore_current_blog(); ?>
    
    <?php 
        /* Get current URL */
        function currentUrl( $trim_query_string = false ) {
            $pageURL = (isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on') ? "https://" : "http://";
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            if( ! $trim_query_string ) {
                return $pageURL;
            } else {
                $url = explode( '?', $pageURL );
                return $url[0];
            }
        }
    
        /* Truncate */
        function truncate($string, $length){
            if (strlen($string) > $length) {
                $string = substr($string, 0, $length) . '...';
            }

            return $string;
        }
    ?>
    
    
	