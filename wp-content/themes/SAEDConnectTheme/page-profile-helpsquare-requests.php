<?php /*Template Name: Profile - HealpSquare Requests*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                </div>
               
            <?php } elseif($_GET['view'] == 'interactions'){ ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        HelpSquare Requests I have engaged
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium culpa, ullam placeat quisquam aliquid nesciunt incidunt doloribus nobis animi.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My HelpSquare Requests
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                HelpSquare Requests I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <!-- Tool Requests -->
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Tool Requests
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 20;
                            $parent_post_id = 15;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_id );
                                $parent_post = get_post($parent_post_id);
                        ?>
                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                        <?php echo rgar( $entry, '1' ) ?>
                                    </h3>
                                    <h3 class="txt-sm txt-color-dark txt-medium">
                                        For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </section>
                <!-- Land Requests -->
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Land Requests
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 21;
                            $parent_post_id = 12;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_id );
                                $parent_post = get_post($parent_post_id);
                        ?>
                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                        <?php echo rgar( $entry, '1' ) ?>
                                    </h3>
                                    <h3 class="txt-sm txt-color-dark txt-medium">
                                        For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </section>
                <!-- Workspace Requests -->
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Workspace Requests
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 22;
                            $parent_post_id = 13;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_id );
                                $parent_post = get_post($parent_post_id);
                        ?>
                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                        <?php echo rgar( $entry, '1' ) ?>
                                    </h3>
                                    <h3 class="txt-sm txt-color-dark txt-medium">
                                        For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </section>
                <!-- Volunteer Requests -->
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Volunteer Requests
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 23;
                            $parent_post_id = 13;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_id );
                                $parent_post = get_post($parent_post_id);
                        ?>
                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                        <?php echo rgar( $entry, '1' ) ?>
                                    </h3>
                                    <h3 class="txt-sm txt-color-dark txt-medium">
                                        For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </section>
                <!-- Funding Requests -->
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Funding Requests
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 24;
                            $parent_post_id = 9;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_id );
                                $parent_post = get_post($parent_post_id);
                        ?>
                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                        <?php echo rgar( $entry, '1' ) ?>
                                    </h3>
                                    <h3 class="txt-sm txt-color-dark txt-medium">
                                        For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </section>
                <!-- Marketing Requests -->
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Marketing Requests
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 25;
                            $parent_post_id = 11;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_id );
                                $parent_post = get_post($parent_post_id);
                        ?>
                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                        <?php echo rgar( $entry, '2' ) ?>
                                    </h3>
                                    <h3 class="txt-sm txt-color-dark txt-medium">
                                        For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </section>
                <!-- Business Partner Requests -->
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Business Partner Requests
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 19;
                            $parent_post_id = 2;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_id );
                                $parent_post = get_post($parent_post_id);
                        ?>
                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                        <?php echo rgar( $entry, '12' ) ?>
                                    </h3>
                                    <h3 class="txt-sm txt-color-dark txt-medium">
                                        For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </section>
                <!-- Mentor Requests -->
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Mentor Requests
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 27;
                            $parent_post_id = 2;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_id );
                                $parent_post = get_post($parent_post_id);
                        ?>
                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                        <?php echo rgar( $entry, '1' ) ?>
                                    </h3>
                                    <h3 class="txt-sm txt-color-dark txt-medium">
                                        For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </section>   
                
            <?php } else { ?>
            
                <div class="page-header">
                    <h1 class="page-title">
                        My HelpSquare Requests
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium culpa, ullam placeat quisquam aliquid nesciunt incidunt doloribus nobis animi.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My HelpSquare Requests
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                HelpSquare Requests I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <!-- Tool Requests -->
                <section class="faq-accordion border-b-1 border-color-darkgrey">
                    <div class="padding-tb-30">
                        <button class="btn-link collapsed txt-medium" data-toggle="collapse" data-target="#main-1" aria-expanded="false" aria-controls="main-1">
                            <span class="row">
                                <div class="col-8">
                                    Tool Requests
                                </div>
                                <div class="col-4 text-right">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </span>
                        </button>
                    </div>
                    <div id="main-1" class="collapse" aria-labelledby="heading-1" data-parent="#accordion">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 20;
                            $parent_post_field = 15;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $parent_post_field;

                            if( $entries ){
                                foreach( $entries as $entry ){
                                /* Get Parent Post ID */
                                $parent_post_id = rgar( $entry, $parent_post_field );
                                $entry_post_id = $entry['post_id'];

                                $parent_post = get_post($parent_post_id);
                                $entry_post = get_post($entry_post_id);
                        ?>
                           
                            <div class="col-md-6 padding-lr-15 padding-b-30">
                                <div 
                                    class="venture-request-card"
                                    req-title="<?php echo $entry_post->post_title; ?>"
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo 'tool_type="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                        if($meta){
                                            echo 'tool_type_order="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                        if($meta){
                                            echo 'tool_description="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                        if($meta){
                                            echo 'tool_impact="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo 'tool_quantity="'.$meta.'"';
                                        }

                                        /* Locations */
                                        $field = 'tool_location_needed';
                                        $output_string;

                                        $meta = get_post_meta($entry_post_id, $field, false);

                                        if($meta){

                                            foreach($meta as $key=>$value) {
                                                $output_string .= $value;
                                                if($key < count($meta) - 1 ){
                                                    $output_string .= ', ';
                                                }
                                            }

                                            echo 'tool_location_needed="'.$output_string.'"';
                                        }


                                        $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo 'tool_need_duration="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                        if($meta){
                                            echo 'tool_affordability="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                        if($meta){
                                            echo 'tool_offererd_amount="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                        if($meta){
                                            echo 'tool_returns="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                        }
                                    ?>
                                >
                                    <div class="row row-10 margin-b-20">
                                        <div class="col-8 padding-lr-10">
                                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                                <?php echo $entry_post->post_title; ?>
                                            </h3>
                                            <h3 class="txt-xs txt-color-light">
                                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                            </h3>
                                        </div>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-clock-o txt-color-red"></i>
                                            <span class="txt-color-lighter padding-l-5">
                                                Posted 9 minutes ago
                                            </span>
                                        </div>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Type:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Quantity:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Duration:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                                if($meta){
                                                    echo $meta; 
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="row row-10 margin-t-30">
                                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                            8 People responded
                                        </h3>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-map-marker txt-color-red"></i>
                                            <span class="padding-l-5">
                                                Lagos
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        <?php 
                                } 
                            } else { 
                        ?>
                           
                            <div class="col-md-12 padding-lr-15 padding-b-30">
                                <p>
                                    You have not made any requests.
                                </p>
                            </div>
                            
                        <?php } ?>
                        </div>
                    </div>
                </section>
                
                <!-- Land Requests -->
                <section class="faq-accordion border-b-1 border-color-darkgrey">
                    <div class="padding-tb-30">
                        <button class="btn-link collapsed txt-medium" data-toggle="collapse" data-target="#main-2" aria-expanded="false" aria-controls="main-2">
                            <span class="row">
                                <div class="col-8">
                                    Land Requests
                                </div>
                                <div class="col-4 text-right">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </span>
                        </button>
                    </div>
                    <div id="main-2" class="collapse" aria-labelledby="heading-2" data-parent="#accordion">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 21;
                            $parent_post_field = 12;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $parent_post_field;

                            if( $entries ){
                                foreach( $entries as $entry ){
                                    /* Get Parent Post ID */
                                    $parent_post_id = rgar( $entry, $parent_post_field );
                                    $entry_post_id = $entry['post_id'];

                                    $parent_post = get_post($parent_post_id);
                                    $entry_post = get_post($entry_post_id);
                        ?>
                           
                            <div class="col-md-6 padding-lr-15 padding-b-30">
                                <div 
                                    class="venture-request-card"
                                    req-title="<?php echo $entry_post->post_title; ?>"
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo 'tool_type="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                        if($meta){
                                            echo 'tool_type_order="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                        if($meta){
                                            echo 'tool_description="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                        if($meta){
                                            echo 'tool_impact="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo 'tool_quantity="'.$meta.'"';
                                        }

                                        /* Locations */
                                        $field = 'tool_location_needed';
                                        $output_string;

                                        $meta = get_post_meta($entry_post_id, $field, false);

                                        if($meta){

                                            foreach($meta as $key=>$value) {
                                                $output_string .= $value;
                                                if($key < count($meta) - 1 ){
                                                    $output_string .= ', ';
                                                }
                                            }

                                            echo 'tool_location_needed="'.$output_string.'"';
                                        }


                                        $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo 'tool_need_duration="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                        if($meta){
                                            echo 'tool_affordability="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                        if($meta){
                                            echo 'tool_offererd_amount="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                        if($meta){
                                            echo 'tool_returns="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                        }
                                    ?>
                                >
                                    <div class="row row-10 margin-b-20">
                                        <div class="col-8 padding-lr-10">
                                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                                <?php echo $entry_post->post_title; ?>
                                            </h3>
                                            <h3 class="txt-xs txt-color-light">
                                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                            </h3>
                                        </div>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-clock-o txt-color-red"></i>
                                            <span class="txt-color-lighter padding-l-5">
                                                Posted 9 minutes ago
                                            </span>
                                        </div>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Type:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Quantity:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Duration:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                                if($meta){
                                                    echo $meta; 
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="row row-10 margin-t-30">
                                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                            8 People responded
                                        </h3>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-map-marker txt-color-red"></i>
                                            <span class="padding-l-5">
                                                Lagos
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        <?php 
                                } 
                            } else { 
                        ?>
                           
                            <div class="col-md-12 padding-lr-15 padding-b-30">
                                <p>
                                    You have not made any requests.
                                </p>
                            </div>
                            
                        <?php } ?>
                        </div>
                    </div>
                </section>
                
                <!-- Workspace Requests -->
                <section class="faq-accordion border-b-1 border-color-darkgrey">
                    <div class="padding-tb-30">
                        <button class="btn-link collapsed txt-medium" data-toggle="collapse" data-target="#main-3" aria-expanded="false" aria-controls="main-3">
                            <span class="row">
                                <div class="col-8">
                                    Workspace Requests
                                </div>
                                <div class="col-4 text-right">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </span>
                        </button>
                    </div>
                    <div id="main-3" class="collapse" aria-labelledby="heading-3" data-parent="#accordion">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 22;
                            $parent_post_field = 13;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $parent_post_field;

                            if( $entries ){
                                foreach( $entries as $entry ){
                                    /* Get Parent Post ID */
                                    $parent_post_id = rgar( $entry, $parent_post_field );
                                    $entry_post_id = $entry['post_id'];

                                    $parent_post = get_post($parent_post_id);
                                    $entry_post = get_post($entry_post_id);
                        ?>
                           
                            <div class="col-md-6 padding-lr-15 padding-b-30">
                                <div 
                                    class="venture-request-card"
                                    req-title="<?php echo $entry_post->post_title; ?>"
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo 'tool_type="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                        if($meta){
                                            echo 'tool_type_order="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                        if($meta){
                                            echo 'tool_description="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                        if($meta){
                                            echo 'tool_impact="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo 'tool_quantity="'.$meta.'"';
                                        }

                                        /* Locations */
                                        $field = 'tool_location_needed';
                                        $output_string;

                                        $meta = get_post_meta($entry_post_id, $field, false);

                                        if($meta){

                                            foreach($meta as $key=>$value) {
                                                $output_string .= $value;
                                                if($key < count($meta) - 1 ){
                                                    $output_string .= ', ';
                                                }
                                            }

                                            echo 'tool_location_needed="'.$output_string.'"';
                                        }


                                        $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo 'tool_need_duration="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                        if($meta){
                                            echo 'tool_affordability="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                        if($meta){
                                            echo 'tool_offererd_amount="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                        if($meta){
                                            echo 'tool_returns="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                        }
                                    ?>
                                >
                                    <div class="row row-10 margin-b-20">
                                        <div class="col-8 padding-lr-10">
                                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                                <?php echo $entry_post->post_title; ?>
                                            </h3>
                                            <h3 class="txt-xs txt-color-light">
                                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                            </h3>
                                        </div>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-clock-o txt-color-red"></i>
                                            <span class="txt-color-lighter padding-l-5">
                                                Posted 9 minutes ago
                                            </span>
                                        </div>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Type:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Quantity:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Duration:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                                if($meta){
                                                    echo $meta; 
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="row row-10 margin-t-30">
                                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                            8 People responded
                                        </h3>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-map-marker txt-color-red"></i>
                                            <span class="padding-l-5">
                                                Lagos
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        <?php 
                                } 
                            } else { 
                        ?>
                           
                            <div class="col-md-12 padding-lr-15 padding-b-30">
                                <p>
                                    You have not made any requests.
                                </p>
                            </div>
                            
                        <?php } ?>
                        </div>
                    </div>
                </section>
                
                <!-- Volunteer Requests -->
                <section class="faq-accordion border-b-1 border-color-darkgrey">
                    <div class="padding-tb-30">
                        <button class="btn-link collapsed txt-medium" data-toggle="collapse" data-target="#main-4" aria-expanded="false" aria-controls="main-4">
                            <span class="row">
                                <div class="col-8">
                                    Volunteer Requests
                                </div>
                                <div class="col-4 text-right">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </span>
                        </button>
                    </div>
                    <div id="main-4" class="collapse" aria-labelledby="heading-4" data-parent="#accordion">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 23;
                            $parent_post_field = 13;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $parent_post_field;

                            if( $entries ){
                                foreach( $entries as $entry ){
                                    /* Get Parent Post ID */
                                    $parent_post_id = rgar( $entry, $parent_post_field );
                                    $entry_post_id = $entry['post_id'];

                                    $parent_post = get_post($parent_post_id);
                                    $entry_post = get_post($entry_post_id);
                        ?>
                           
                            <div class="col-md-6 padding-lr-15 padding-b-30">
                                <div 
                                    class="venture-request-card"
                                    req-title="<?php echo $entry_post->post_title; ?>"
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo 'tool_type="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                        if($meta){
                                            echo 'tool_type_order="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                        if($meta){
                                            echo 'tool_description="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                        if($meta){
                                            echo 'tool_impact="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo 'tool_quantity="'.$meta.'"';
                                        }

                                        /* Locations */
                                        $field = 'tool_location_needed';
                                        $output_string;

                                        $meta = get_post_meta($entry_post_id, $field, false);

                                        if($meta){

                                            foreach($meta as $key=>$value) {
                                                $output_string .= $value;
                                                if($key < count($meta) - 1 ){
                                                    $output_string .= ', ';
                                                }
                                            }

                                            echo 'tool_location_needed="'.$output_string.'"';
                                        }


                                        $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo 'tool_need_duration="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                        if($meta){
                                            echo 'tool_affordability="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                        if($meta){
                                            echo 'tool_offererd_amount="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                        if($meta){
                                            echo 'tool_returns="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                        }
                                    ?>
                                >
                                    <div class="row row-10 margin-b-20">
                                        <div class="col-8 padding-lr-10">
                                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                                <?php echo $entry_post->post_title; ?>
                                            </h3>
                                            <h3 class="txt-xs txt-color-light">
                                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                            </h3>
                                        </div>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-clock-o txt-color-red"></i>
                                            <span class="txt-color-lighter padding-l-5">
                                                Posted 9 minutes ago
                                            </span>
                                        </div>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Type:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Quantity:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Duration:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                                if($meta){
                                                    echo $meta; 
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="row row-10 margin-t-30">
                                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                            8 People responded
                                        </h3>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-map-marker txt-color-red"></i>
                                            <span class="padding-l-5">
                                                Lagos
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        <?php 
                                } 
                            } else { 
                        ?>
                           
                            <div class="col-md-12 padding-lr-15 padding-b-30">
                                <p>
                                    You have not made any requests.
                                </p>
                            </div>
                            
                        <?php } ?>
                        </div>
                    </div>
                </section>
                
                <!-- Funding Requests -->
                <section class="faq-accordion border-b-1 border-color-darkgrey">
                    <div class="padding-tb-30">
                        <button class="btn-link collapsed txt-medium" data-toggle="collapse" data-target="#main-5" aria-expanded="false" aria-controls="main-5">
                            <span class="row">
                                <div class="col-8">
                                    Funding Requests
                                </div>
                                <div class="col-4 text-right">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </span>
                        </button>
                    </div>
                    <div id="main-5" class="collapse" aria-labelledby="heading-5" data-parent="#accordion">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 24;
                            $parent_post_field = 19;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $parent_post_field;

                            if( $entries ){
                                foreach( $entries as $entry ){
                                    /* Get Parent Post ID */
                                    $parent_post_id = rgar( $entry, $parent_post_field );
                                    $entry_post_id = $entry['post_id'];

                                    $parent_post = get_post($parent_post_id);
                                    $entry_post = get_post($entry_post_id);
                        ?>
                           
                            <div class="col-md-6 padding-lr-15 padding-b-30">
                                <div 
                                    class="venture-request-card"
                                    req-title="<?php echo $entry_post->post_title; ?>"
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo 'tool_type="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                        if($meta){
                                            echo 'tool_type_order="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                        if($meta){
                                            echo 'tool_description="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                        if($meta){
                                            echo 'tool_impact="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo 'tool_quantity="'.$meta.'"';
                                        }

                                        /* Locations */
                                        $field = 'tool_location_needed';
                                        $output_string;

                                        $meta = get_post_meta($entry_post_id, $field, false);

                                        if($meta){

                                            foreach($meta as $key=>$value) {
                                                $output_string .= $value;
                                                if($key < count($meta) - 1 ){
                                                    $output_string .= ', ';
                                                }
                                            }

                                            echo 'tool_location_needed="'.$output_string.'"';
                                        }


                                        $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo 'tool_need_duration="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                        if($meta){
                                            echo 'tool_affordability="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                        if($meta){
                                            echo 'tool_offererd_amount="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                        if($meta){
                                            echo 'tool_returns="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                        }
                                    ?>
                                >
                                    <div class="row row-10 margin-b-20">
                                        <div class="col-8 padding-lr-10">
                                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                                <?php echo $entry_post->post_title; ?>
                                            </h3>
                                            <h3 class="txt-xs txt-color-light">
                                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                            </h3>
                                        </div>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-clock-o txt-color-red"></i>
                                            <span class="txt-color-lighter padding-l-5">
                                                Posted 9 minutes ago
                                            </span>
                                        </div>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Type:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Quantity:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Duration:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                                if($meta){
                                                    echo $meta; 
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="row row-10 margin-t-30">
                                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                            8 People responded
                                        </h3>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-map-marker txt-color-red"></i>
                                            <span class="padding-l-5">
                                                Lagos
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        <?php 
                                } 
                            } else { 
                        ?>
                           
                            <div class="col-md-12 padding-lr-15 padding-b-30">
                                <p>
                                    You have not made any requests.
                                </p>
                            </div>
                            
                        <?php } ?>
                        </div>
                    </div>
                </section>
                
                <!-- Marketing Requests -->
                <section class="faq-accordion border-b-1 border-color-darkgrey">
                    <div class="padding-tb-30">
                        <button class="btn-link collapsed txt-medium" data-toggle="collapse" data-target="#main-6" aria-expanded="false" aria-controls="main-6">
                            <span class="row">
                                <div class="col-8">
                                    Marketing Requests
                                </div>
                                <div class="col-4 text-right">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </span>
                        </button>
                    </div>
                    <div id="main-6" class="collapse" aria-labelledby="heading-6" data-parent="#accordion">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 25;
                            $parent_post_field = 11;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $parent_post_field;

                            if( $entries ){
                                foreach( $entries as $entry ){
                                    /* Get Parent Post ID */
                                    $parent_post_id = rgar( $entry, $parent_post_field );
                                    $entry_post_id = $entry['post_id'];

                                    $parent_post = get_post($parent_post_id);
                                    $entry_post = get_post($entry_post_id);
                        ?>
                           
                            <div class="col-md-6 padding-lr-15 padding-b-30">
                                <div 
                                    class="venture-request-card"
                                    req-title="<?php echo $entry_post->post_title; ?>"
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo 'tool_type="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                        if($meta){
                                            echo 'tool_type_order="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                        if($meta){
                                            echo 'tool_description="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                        if($meta){
                                            echo 'tool_impact="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo 'tool_quantity="'.$meta.'"';
                                        }

                                        /* Locations */
                                        $field = 'tool_location_needed';
                                        $output_string;

                                        $meta = get_post_meta($entry_post_id, $field, false);

                                        if($meta){

                                            foreach($meta as $key=>$value) {
                                                $output_string .= $value;
                                                if($key < count($meta) - 1 ){
                                                    $output_string .= ', ';
                                                }
                                            }

                                            echo 'tool_location_needed="'.$output_string.'"';
                                        }


                                        $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo 'tool_need_duration="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                        if($meta){
                                            echo 'tool_affordability="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                        if($meta){
                                            echo 'tool_offererd_amount="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                        if($meta){
                                            echo 'tool_returns="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                        }
                                    ?>
                                >
                                    <div class="row row-10 margin-b-20">
                                        <div class="col-8 padding-lr-10">
                                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                                <?php echo $entry_post->post_title; ?>
                                            </h3>
                                            <h3 class="txt-xs txt-color-light">
                                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                            </h3>
                                        </div>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-clock-o txt-color-red"></i>
                                            <span class="txt-color-lighter padding-l-5">
                                                Posted 9 minutes ago
                                            </span>
                                        </div>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Type:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Quantity:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Duration:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                                if($meta){
                                                    echo $meta; 
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="row row-10 margin-t-30">
                                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                            8 People responded
                                        </h3>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-map-marker txt-color-red"></i>
                                            <span class="padding-l-5">
                                                Lagos
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        <?php 
                                } 
                            } else { 
                        ?>
                           
                            <div class="col-md-12 padding-lr-15 padding-b-30">
                                <p>
                                    You have not made any requests.
                                </p>
                            </div>
                            
                        <?php } ?>
                        </div>
                    </div>
                </section>
                
                <!-- Business Partner Requests -->
                <section class="faq-accordion border-b-1 border-color-darkgrey">
                    <div class="padding-tb-30">
                        <button class="btn-link collapsed txt-medium" data-toggle="collapse" data-target="#main-7" aria-expanded="false" aria-controls="main-7">
                            <span class="row">
                                <div class="col-8">
                                    Business Partner Requests
                                </div>
                                <div class="col-4 text-right">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </span>
                        </button>
                    </div>
                    <div id="main-7" class="collapse" aria-labelledby="heading-7" data-parent="#accordion">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 19;
                            $parent_post_field = 2;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $parent_post_field;

                            if( $entries ){
                                foreach( $entries as $entry ){
                                    /* Get Parent Post ID */
                                    $parent_post_id = rgar( $entry, $parent_post_field );
                                    $entry_post_id = $entry['post_id'];

                                    $parent_post = get_post($parent_post_id);
                                    $entry_post = get_post($entry_post_id);
                        ?>
                           
                            <div class="col-md-6 padding-lr-15 padding-b-30">
                                <div 
                                    class="venture-request-card"
                                    req-title="<?php echo $entry_post->post_title; ?>"
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo 'tool_type="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                        if($meta){
                                            echo 'tool_type_order="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                        if($meta){
                                            echo 'tool_description="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                        if($meta){
                                            echo 'tool_impact="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo 'tool_quantity="'.$meta.'"';
                                        }

                                        /* Locations */
                                        $field = 'tool_location_needed';
                                        $output_string;

                                        $meta = get_post_meta($entry_post_id, $field, false);

                                        if($meta){

                                            foreach($meta as $key=>$value) {
                                                $output_string .= $value;
                                                if($key < count($meta) - 1 ){
                                                    $output_string .= ', ';
                                                }
                                            }

                                            echo 'tool_location_needed="'.$output_string.'"';
                                        }


                                        $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo 'tool_need_duration="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                        if($meta){
                                            echo 'tool_affordability="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                        if($meta){
                                            echo 'tool_offererd_amount="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                        if($meta){
                                            echo 'tool_returns="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                        }
                                    ?>
                                >
                                    <div class="row row-10 margin-b-20">
                                        <div class="col-8 padding-lr-10">
                                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                                <?php echo $entry_post->post_title; ?>
                                            </h3>
                                            <h3 class="txt-xs txt-color-light">
                                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                            </h3>
                                        </div>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-clock-o txt-color-red"></i>
                                            <span class="txt-color-lighter padding-l-5">
                                                Posted 9 minutes ago
                                            </span>
                                        </div>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Type:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Quantity:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Duration:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                                if($meta){
                                                    echo $meta; 
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="row row-10 margin-t-30">
                                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                            8 People responded
                                        </h3>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-map-marker txt-color-red"></i>
                                            <span class="padding-l-5">
                                                Lagos
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        <?php 
                                } 
                            } else { 
                        ?>
                           
                            <div class="col-md-12 padding-lr-15 padding-b-30">
                                <p>
                                    You have not made any requests.
                                </p>
                            </div>
                            
                        <?php } ?>
                        </div>
                    </div>
                </section>
                
                <!-- Mentor Requests -->
                <section class="faq-accordion border-b-1 border-color-darkgrey">
                    <div class="padding-tb-30">
                        <button class="btn-link collapsed txt-medium" data-toggle="collapse" data-target="#main-8" aria-expanded="false" aria-controls="main-8">
                            <span class="row">
                                <div class="col-8">
                                    Mentor Requests
                                </div>
                                <div class="col-4 text-right">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </span>
                        </button>
                    </div>
                    <div id="main-8" class="collapse" aria-labelledby="heading-8" data-parent="#accordion">
                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 27;
                            $parent_post_field = 2;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $parent_post_field;

                            if( $entries ){
                                foreach( $entries as $entry ){
                                    /* Get Parent Post ID */
                                    $parent_post_id = rgar( $entry, $parent_post_field );
                                    $entry_post_id = $entry['post_id'];

                                    $parent_post = get_post($parent_post_id);
                                    $entry_post = get_post($entry_post_id);
                        ?>
                           
                            <div class="col-md-6 padding-lr-15 padding-b-30">
                                <div 
                                    class="venture-request-card"
                                    req-title="<?php echo $entry_post->post_title; ?>"
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo 'tool_type="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                        if($meta){
                                            echo 'tool_type_order="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                        if($meta){
                                            echo 'tool_description="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                        if($meta){
                                            echo 'tool_impact="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo 'tool_quantity="'.$meta.'"';
                                        }

                                        /* Locations */
                                        $field = 'tool_location_needed';
                                        $output_string;

                                        $meta = get_post_meta($entry_post_id, $field, false);

                                        if($meta){

                                            foreach($meta as $key=>$value) {
                                                $output_string .= $value;
                                                if($key < count($meta) - 1 ){
                                                    $output_string .= ', ';
                                                }
                                            }

                                            echo 'tool_location_needed="'.$output_string.'"';
                                        }


                                        $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo 'tool_need_duration="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                        if($meta){
                                            echo 'tool_affordability="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                        if($meta){
                                            echo 'tool_offererd_amount="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                        if($meta){
                                            echo 'tool_returns="'.$meta.'"';
                                        }

                                        $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                        if($meta){
                                            $date = strtotime($meta);
                                            echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                        }
                                    ?>
                                >
                                    <div class="row row-10 margin-b-20">
                                        <div class="col-8 padding-lr-10">
                                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                                <?php echo $entry_post->post_title; ?>
                                            </h3>
                                            <h3 class="txt-xs txt-color-light">
                                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                            </h3>
                                        </div>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-clock-o txt-color-red"></i>
                                            <span class="txt-color-lighter padding-l-5">
                                                Posted 9 minutes ago
                                            </span>
                                        </div>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Type:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Quantity:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="txt-sm margin-b-10">
                                        <span class="padding-r-10 txt-color-lighter">
                                            Duration:
                                        </span>
                                        <span class="txt-medium">
                                            <?php
                                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                                if($meta){
                                                    echo $meta; 
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="row row-10 margin-t-30">
                                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                            8 People responded
                                        </h3>
                                        <div class="col-4 txt-sm text-right padding-lr-10">
                                            <i class="fa fa-map-marker txt-color-red"></i>
                                            <span class="padding-l-5">
                                                Lagos
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        <?php 
                                } 
                            } else { 
                        ?>
                           
                            <div class="col-md-12 padding-lr-15 padding-b-30">
                                <p>
                                    You have not made any requests.
                                </p>
                            </div>
                            
                        <?php } ?>
                        </div>
                    </div>
                </section>
            
            <?php } ?>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>