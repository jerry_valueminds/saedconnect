    
<?php get_header() ?>

<?php
    // Get Base URL
    $base_url = get_site_url().'/cv-forms';

    // Get Currently logged in User
    $current_user = wp_get_current_user();
?> 

<main class="main-content font-main">
   
    <section class="container text-center">
        <h1 class="txt-2em txt-medium margin-b-20">
            <?php the_title(); ?>
        </h1>
    </section>
    
<?php
    // TO SHOW THE PAGE CONTENTS
    while ( have_posts() ) : the_post();

    $cv_id = get_the_ID();
    $cv_entry_id = wp_strip_all_tags( get_the_content() );
    $relationship_type = 'cv_to_school';
?>
    <!-- Personal Information -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Personal Information
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 6, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-20">
                    <!-- DOB -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-personal-info-dob', true);
                        if($meta){
                    ?>
                        <p>
                            <span class="txt-bold">DOB: </span>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                    <!-- Nationality -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-personal-info-nationality', true);
                        if($meta){
                    ?>
                        <p>
                            <span class="txt-bold">Nationality: </span>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                    <!-- Gender -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-personal-info-gender', true);
                        if($meta){
                    ?>
                        <p>
                            <span class="txt-bold">Gender: </span>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                    <!-- Marital Status -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-personal-info-marital', true);
                        if($meta){
                    ?>
                        <p>
                            <span class="txt-bold">Marital Status: </span>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <form action="<?php echo $base_url.'/?action=cv-personal-information-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>

    <!-- Summary -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Summary
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 7, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-headline', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="margin-b-20">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-about', true);
                        if($meta){
                    ?>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-summary-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>

    <!-- Contact Details -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Contact Details
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 8, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-email', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Email
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-phone', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Phone
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-address', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Address
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-state', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            State of Residence
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-website', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Personal Blog/Website
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-facebook', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Facebook
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-twitter', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Twitter
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-instagram', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Instagram
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                   
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-linkedin', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Linkedin
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>


                <form action="<?php echo $base_url.'/?action=cv-contact-details-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Experience -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Experience
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 9, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-experience-Position', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Position/Job Title
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-phone', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Phone
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-contact-address', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Address
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="row">
                    <form action="<?php echo $base_url.'/?action=cv-experience-form' ?>" method="post" class="margin-r-10">
                        <button class="sticky-list-edit submit btn">Edit</button>
                        <input type="hidden" name="mode" value="edit">
                        <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                    </form>
                    
                    <button class="btn btn-green" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Add
                    </button>
                </div>
                
                <div class="collapse" id="collapseExample">
                    <div class="padding-tb-40">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat aperiam, nobis nostrum, tempora hic ab, optio natus corrupti temporibus sequi soluta! Velit fugiat, qui vero, cupiditate eos deserunt ab quidem.
                    </div>
                </div>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Personal Projects -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium padding-b-20 margin-b-20 uppercase border-b-2">
                    Personal Projects
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 10, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>
                <div class="padding-b-10 margin-b-20 border-b-1">
                    <div class="margin-b-20">
                        <!-- Time -->
                        <?php
                            $meta = get_post_meta($post->ID, 'cv-personal-project-name', true);
                            if($meta){
                        ?>
                            <p class="txt-bold">
                                <?php echo $meta; ?>
                            </p>
                        <?php
                            }
                        ?>

                        <!-- Description -->
                        <?php
                            $meta = get_post_meta($post->ID, 'cv-personal-project-description', true);
                            if($meta){
                        ?>
                            <p>
                                <?php echo $meta; ?>
                            </p>
                        <?php
                            }
                        ?>

                    </div>

                    <form action="<?php echo $base_url.'/?action=cv-personal-projects-form' ?>" method="post" class="margin-r-10">
                        <button class="sticky-list-edit submit btn">Edit</button>
                        <input type="hidden" name="mode" value="edit">
                        <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                    </form>
                </div>
            <?php

                }

            ?>
            
                <div class="">
                    <div class="text-right">
                        <button class="btn btn-green" type="button" data-toggle="collapse" data-target="#collapse-5" aria-expanded="false" aria-controls="collapseExample">
                            Add
                        </button>
                    </div>
                    <div class="collapse" id="collapse-5">
                        <div class="padding-tb-40">
                            <?php echo do_shortcode('[gravityform id="10" title="false" description="false" field_values="post-title=Some Value&cv-id='.$cv_entry_id.'"]'); ?>
                        </div>
                    </div>
                </div>
           </div>
       </div>
    </section>

    <!-- Education & Training -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Education & Training
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 11, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-education-degree', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Degree Attained
                        </p>
                        <p class="txt-bold">
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="margin-b-20">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-about', true);
                        if($meta){
                    ?>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-education-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>

    <!-- Awards -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Awards
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 12, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-award-title', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Award Title
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <form action="<?php echo $base_url.'/?action=cv-awards-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Accomplishments -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Accomplishments
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 13, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-accomplishments-description', true);
                        if($meta){
                    ?>
                        <p class="">
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="margin-b-20">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-about', true);
                        if($meta){
                    ?>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-accomplishments-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Skills -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Skills
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 14, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-20">
                    <!-- DOB -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-skill-name', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Skill Name
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-skills-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>

    <!-- Certifications -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Certifications
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 15, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-headline', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="margin-b-20">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-about', true);
                        if($meta){
                    ?>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-certifications-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>

    <!-- Affiliations -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Affiliations
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 16, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-affiliation-title', true);
                        if($meta){
                    ?>
                        <p>
                            Title
                        </p>
                        <p class="txt-bold">
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="margin-b-20">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-about', true);
                        if($meta){
                    ?>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-affiliations-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Patents -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Patents
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 17, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-patent-title', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Title
                        </p>
                        <p class="txt-bold">
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="margin-b-20">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-about', true);
                        if($meta){
                    ?>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-patents-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- Publications -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Publications
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 18, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-20">
                    <!-- DOB -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-publication-title', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Title
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                    <!-- Nationality -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-personal-info-nationality', true);
                        if($meta){
                    ?>
                        <p>
                            <span class="txt-bold">Nationality: </span>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                    <!-- Gender -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-personal-info-gender', true);
                        if($meta){
                    ?>
                        <p>
                            <span class="txt-bold">Gender: </span>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                    <!-- Marital Status -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-personal-info-marital', true);
                        if($meta){
                    ?>
                        <p>
                            <span class="txt-bold">Marital Status: </span>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <form action="<?php echo $base_url.'/?action=cv-publications-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>

    <!-- Interests -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Interests
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 19, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-interest-title', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Title
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="margin-b-20">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-summary-about', true);
                        if($meta){
                    ?>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-interests-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>

    <!-- Languages -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    Languages
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 20, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-interest-title', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Title
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>

                <div class="margin-b-20">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-language-title', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Language Name
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>

                </div>

                <form action="<?php echo $base_url.'/?action=cv-languages-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    <!-- References -->
    <section class="padding-b-20">
       <div class="container">
          <?php //echo do_shortcode( "[stickylist id='6']"); ?>

           <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                <h1 class="txt-xlg txt-medium margin-b-20">
                    References
                </h1>

            <!--Using Get entries-->
            <?php

                $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                $entries = GFAPI::get_entries( 21, $search_criteria );

                foreach($entries as $entry){
                    $post = get_post( $entry['post_id'] ); 
           ?>

                <div class="margin-b-10">
                    <!-- About -->
                    <?php
                        $meta = get_post_meta($post->ID, 'cv-reference-name', true);
                        if($meta){
                    ?>
                        <p class="txt-bold">
                            Name of Referee
                        </p>
                        <p>
                            <?php echo $meta; ?>
                        </p>
                    <?php
                        }
                    ?>
                </div>


                <form action="<?php echo $base_url.'/?action=cv-references-form' ?>" method="post">
                    <button class="sticky-list-edit submit btn">Edit</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                </form>
            <?php

                }

            ?>
           </div>
       </div>
    </section>
    
    
    
    
    
    
    
    
    
    
<?php
    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
?>

</main>
        
        
        
        
        
        
<?php get_footer() ?>