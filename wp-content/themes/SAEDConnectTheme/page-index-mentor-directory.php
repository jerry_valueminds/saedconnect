    <?php /*Template Name: Homepage-Mentor Directory*/ ?>
    
    <?php get_header() ?>

    <main class="main-content">
        <header class="container-wrapper padding-t-80 padding-b-80 bg-blue txt-color-white text-center">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <h1 class="txt-3em txt-light margin-b-20">
                        Meet our Mentors
                    </h1>
                    <article>
                        <p class="">
                            We have the best of the best. Browse our growing list of mentors to find your personal design coach!
                        </p>
                    </article>
                    <div class="margin-t-20">
                        <a href="" class="btn btn-green txt-normal-s no-m-b" >
                            Become a mentor
                        </a>
                    </div>
                </div>
            </div>
        </header>
        
        <!-- Filter -->
        <section class="padding-t-20">
            <form id="filter-accordion" class="filter-accordion">                
                <?php
                    /* Filter Options Array */
                    $filter_items_array = array(
                        array(
                            'term_slug' =>'coverage-nigeria',
                            'term_name' => 'Location',
                            'meta_key' => 'mentor-location-nigeria'
                        ),

                    );
                ?>
                
                <!-- Accordion Header -->
                <div class="container-wrapper">
                    <div class="row row-5">
                    <?php
                        if($_GET){
                    ?>
                        <div class="padding-lr-5 padding-b-10">
                            <a href="https://www.saedconnect.org/growth-programs/mentor-directory/" class="btn btn-dark txt-normal-s">
                                <i class="fa fa-times"></i>
                                Clear Filter
                            </a>
                        </div>
                    <?php } ?>
                    
                    <div class="padding-lr-5 padding-b-10">
                        <button
                            id="heading-mentorship-area"
                            class="btn btn-trans-green dropdown-toggle txt-normal-s
                                <?php
                                    if($_GET['s-mentorship-area'] != ''){
                                        echo 'bg-darkgrey';
                                    }
                                ?>
                            "
                            type="button" data-toggle="collapse"
                            data-target="#filter-content-mentorship-area"
                            aria-expanded="false"
                            aria-controls="filter-content-mentorship-area"
                        >
                            Mentorship Area
                        </button>
                    </div>
                    
                    <?php foreach($filter_items_array as $filter_item){ ?>
                        <div class="padding-lr-5 padding-b-10">
                            <button
                                id="heading-<?php echo $filter_item['term_slug']; ?>"
                                class="btn btn-trans-green dropdown-toggle txt-normal-s
                                    <?php
                                        if($_GET[$filter_item['meta_key']] != ''){
                                            echo 'bg-darkgrey';
                                        }
                                    ?>
                                "
                                type="button" data-toggle="collapse"
                                data-target="#filter-content-<?php echo $filter_item['term_slug']; ?>"
                                aria-expanded="false"
                                aria-controls="filter-content-<?php echo $filter_item['term_slug']; ?>"
                            >
                                <?php echo $filter_item['term_name']; ?>
                            </button>
                        </div>
                    <?php } ?>
                        <div class="padding-lr-5 padding-b-10">
                            <input class="btn btn-green txt-normal-s" type="submit" value="GO">
                        </div>
                    </div>
                </div>
                
                <!-- Accordion Content -->
                <div class="container-wrapper bg-grey">
                <?php foreach($filter_items_array as $filter_item){ ?>
                    <div
                        id="filter-content-<?php echo $filter_item['term_slug']; ?>"
                        class="collapse"
                        aria-labelledby="heading-<?php echo $filter_item['term_slug']; ?>"
                        data-parent="#filter-accordion"
                    >
                        <div class="custom-radio txt-sm padding-t-40 padding-b-30">
                            <label class="radio-item parent=term">
                                <input
                                    type="radio"
                                    name="<?php echo $filter_item['meta_key']; ?>"
                                    value=""
                                    <?php 
                                        if('' == esc_html($_REQUEST['meta_key'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    All
                                </span>
                            </label>
                        <?php 

                            //Get Terms
                            $terms = get_terms( $filter_item['term_slug'], array('hide_empty' => false, 'parent' => 0)); //Get all the terms

                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                // Check and see if the term is a top-level parent. If so, display it.
                                $parent = $term->parent;

                                $term_id = $term->term_id; //Get the term ID
                                //$term_slug = $filter_item['term_slug'];
                                $term_slug = $term->slug;
                                $term_name = $term->name; //Get the term name
                                $term_url = get_term_link($term);
                                
                        ?>
                            
                                <label class="radio-item <?php echo ($parent == '0'  ? 'parent-term' : '') ?>">
                                    <input
                                        type="radio"
                                        name="<?php echo $filter_item['meta_key']; ?>"
                                        value="<?php echo $term_slug ?>"
                                        <?php 
                                            if($term_slug == esc_html($_REQUEST[$filter_item['meta_key']])){
                                                echo 'checked';
                                            }
                                        ?>
                                    >
                                    <span class="text">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>
                        <?php

                            }

                        ?>
                        </div>
                    </div>
                <?php } ?>
                    <div
                        id="filter-content-mentorship-area"
                        class="collapse"
                        aria-labelledby="heading-mentorship-area"
                        data-parent="#filter-accordion"
                    >
                        <div class="custom-radio txt-sm padding-t-40 padding-b-30">
                            <label class="radio-item parent=term">
                                <input
                                    type="radio"
                                    name="mentor-mentorship-areas"
                                    value=""
                                    <?php 
                                        if('' == esc_html($_REQUEST['mentor-mentorship-areas'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    All
                                </span>
                            </label>
                        
                            <!-- TEI -->
                            <label class="radio-item">
                                <input
                                    type="radio"
                                    name="mentor-mentorship-areas"
                                    value="The Entrepreneurship Incubator"
                                    <?php 
                                        if('The Entrepreneurship Incubator' == esc_html($_REQUEST['mentor-mentorship-areas'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    The Entrepreneurship Incubator
                                </span>
                            </label>
                            
                            <!-- Career Development -->
                            <label class="radio-item">
                                <input
                                    type="radio"
                                    name="mentor-mentorship-areas"
                                    value="Job Advisor"
                                    <?php 
                                        if('Job Advisor' == esc_html($_REQUEST['mentor-mentorship-areas'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    Career Development
                                </span>
                            </label>
                            
                            <!-- Business Growth -->
                            <label class="radio-item">
                                <input
                                    type="radio"
                                    name="mentor-mentorship-areas"
                                    value="Business Clinic"
                                    <?php 
                                        if('Business Clinic' == esc_html($_REQUEST['mentor-mentorship-areas'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    Business Growth
                                </span>
                            </label>
                            
                            <!-- Side Hustle Mentor Hubs -->
                            <label class="radio-item">
                                <input
                                    type="radio"
                                    name="mentor-mentorship-areas"
                                    value="Side Hustle Mentor Hubs"
                                    <?php 
                                        if('Side Hustle Mentor Hubs' == esc_html($_REQUEST['mentor-mentorship-areas'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    Specific Small Business Mentor
                                </span>
                            </label>
   
                        </div>
                    </div>
                </div>
                
            </form>
        </section>
        
        <!-- Mentors -->
        <section class="bg-grey padding-t-40 padding-b-80">
            <div class="container-wrapper">
                
                <div class="row text-center">
                    <div class="col-md-11 mx-auto">    
                        <div class="row row-30">
                        <?php
                            /*
                            *==================================================================================
                            *==================================================================================
                            *   WP Query
                            *==================================================================================
                            *==================================================================================
                            */
                            // Define Tax Query
                            $meta_array = array('relation' => 'AND');

                            // Add Arguements from _REQUEST
                            foreach($_GET as $key => $value){

                                /* Check & Exclude:
                                        - S ( WP Search field )
                                        - Search-type ( For selecting Search Template View to use )
                                        - Empty values
                                */
                                if($key !== 's' && $key !== 'search-type' && !empty($value) && $value != 'all'){
                                    
                                    // Create Tax Array entry
                                    $category_array = array(

                                        // Add Term Term Values
                                        array (
                                            'key' => trim($key), //Texanomy Type
                                            'value' => $value, //Search field
                                        ),

                                    );

                                    // Add just created Array to Tax Array. 
                                    $meta_array = array_merge($meta_array, $category_array); 
                                }
                            }
                            
                             // Create Query Argument
                            $args = array(
                                'post_type' => 'mentor',
                                'showposts' => -1,
                                'meta_query' => $meta_array,
                            );


                            $wp_query = new WP_Query($args);

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Get Post ID */
                            $post_id = $post->ID 
                        ?>
                           
                            <!-- Mentor -->
                            <div class="col-md-2 padding-lr-30 padding-b-40 d-flex">
                                <div class="mentor-card">
                                    <div class="image-box">
                                        <?php
                                            $field = 'mentor-profile-image';

                                            $meta = get_post_meta($post->ID, $field, true);
                                            
                                        ?>
                                        <img src="<?php if($meta){  echo $meta; } ?>" alt="">
                                    </div>
                                    <div class="name">
                                        <a data-toggle="modal" href="#mentor-<?php echo $post_id ?>">
                                            <?php echo the_title(); ?>
                                        </a>
                                    </div>
                                    <div class="position">
                                        <?php
                                            $field_position = 'mentor-position';
                                            $field_company = 'mentor-company';

                                            $meta_position = get_post_meta($post->ID, $field_position, true);
                                            $meta_company = get_post_meta($post->ID, $field_company, true);

                                            echo $meta_position.' @ '.$meta_company;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Mentor Modal -->
                            <div class="modal fade font-main coming-soon-modal" id="mentor-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content padding-o-40">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <div class="text-center">
                                            <div class="margin-b-10">
                                                <?php
                                                    $field = 'mentor-profile-image';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <img src="<?php if($meta){  echo $meta; } ?>" width="120" class="profile-image">
                                            </div>
                                            <div class="txt-normal-s margin-b-10">
                                                Mentor
                                            </div>
                                            <h4 class="txt-xlg txt-medium margin-b-10">
                                                <?php echo the_title(); ?>
                                            </h4>
                                            <h5 class="txt-medium uppercase txt-sm margin-b-20">
                                                <?php
                                                    $field_position = 'mentor-position';
                                                    $field_company = 'mentor-company';

                                                    $meta_position = get_post_meta($post->ID, $field_position, true);
                                                    $meta_company = get_post_meta($post->ID, $field_company, true);

                                                    echo $meta_position.' @ '.$meta_company;
                                                ?>
                                            </h5>
                                            <p class="txt-sm txt-height-1-7 margin-b-20">
                                                <?php
                                                    $field = 'mentor-short-profile';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                    if($meta){
                                                        echo $meta;
                                                    }
                                                ?>
                                            </p>
                                            <div class="txt-xlg">
                                                <?php
                                                    $field = 'mentor-facebook';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-facebook fa-fw"></i>
                                                </a>
                                                
                                                <?php
                                                    $field = 'mentor-twitter';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a 
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-twitter fa-fw"></i>
                                                </a>
                                                
                                                <?php
                                                    $field = 'mentor-linkedin';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a 
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-linkedin fa-fw"></i>
                                                </a>
                                            </div>
                                            <div class="padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Mentors on
                                                </div>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field = 'mentor-mentorship-areas';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="border-t-1 border-color-darkgrey padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Small Business Mentorship area
                                                </div>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field = 'mentor-small-business-mentorship-interest';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          
                        <?php
                            endwhile;
                        ?>  
                        </div>
                    </div>
                </div>
                
                <div class="row margin-b-80 text-center">
                    <div class="col-md-8 mx-auto">
                        <header>
                            <h1 class="txt-xlg txt-height-1-1 margin-b-20">
                                Interested in joining the cause?
                            </h1>
                            <div class="margin-b-20">
                                <a href="https://www.saedconnect.org/growth-programs/become-a-mentor/" class="btn btn-green txt-normal-s no-m-b">
                                    Apply to become a mentor
                                </a>
                            </div>
                            <h2 class="txt-normal-s txt-height-1-7">
                                Have questions? Check out the
                                <a href="" class="">
                                    Help Center
                                </a>
                                .
                            </h2>
                        </header>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <style>
        .coming-soon-modal .close{
            color: black;
        }
    </style>
    
    
    <?php get_footer() ?>
