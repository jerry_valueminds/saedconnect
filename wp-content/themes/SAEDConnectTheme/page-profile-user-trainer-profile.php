<?php /*Template Name: Profile - User Trainer Profile*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* Publication */
        $publication_key   = 'publication_status';
        $accreditation_key   = 'nysc_accreditation_status';
        $accreditation_skill_key   = 'nysc_accreditation_skill';
        $accreditation_skill_status_key   = 'nysc_accreditation_skill_status';
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Get Avatar */
        $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
        $meta_key = 'user_avatar_url';
        $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

        if($get_avatar_url){
            $avatar_url = $get_avatar_url;
        }


        /* Get Trainer profile */
        $gf_id = 1; //Form ID
        $gv_id = 637; //Gravity View ID
        $title = 'Trainer Information';
        $trainer_id = $_GET['user-id'];
        $user_info = get_userdata( $trainer_id );
        $assigned_state_id = $_GET['state-id'];

        $entry_count = 0;

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => 'created_by', 'value' => $trainer_id, //Current logged in user
                )
            )
        );

        /* Get GF Entry Count */
        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

        /* Get Entries */
        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

        $post_id = $entries[0]['post_id'];

        switch_to_blog(110);
        $terms = get_terms( 'state', array('hide_empty' => false));
        restore_current_blog();
                
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <?php get_header() ?>
    
    <main class="main-content txt-color-light bg-white">
        <?php if($_GET['user-id']){ ?>
        
        <?php get_template_part( 'template-parts/user-dashboard/_dashboard_strip' ); ?>
        
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav-user-profile' ); ?>
            <div class="dashboard-multi-main-content">
                <div class="page-header">
                    <h1 class="page-title">
                        Trainer Profile
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Find information about my training outfit as this would help you understand what I do.
                    </p>
                </article>     

            <?php if( $_GET['view'] == 'publication' ){ ?>
            
            <?php
                /* Publish / Unpublish & Return */
                /* Meta value to save */
                $value = "admin_published";

                /* Get saved meta */
                $saved_meta = get_post_meta( $post_id, $publication_key, true );

                if ( $saved_meta ){ //If published, Unpublish
                    delete_post_meta( $post_id, $publication_key );
                    update_post_meta( $post_id, $publication_key, $value );
                    
                    /* Send Activation mail */
                    //$profile_link = currentUrl(true).'?user-id='.$trainer_id.'&state-id='.$assigned_state_id;
                    $profile_link = 'https://www.saedconnect.org/service-provider-directory/trainer-profile/';

                    $to = $user_info->user_email;
                    $subject = 'SAEDConnect: Trainer Profile Approval';

                    $message = '<div style="font-size:16px;text-align:center;font-family:Arial, Helvetica, sans-serif;margin-top: 100px;"><div style="text-align: left; color:#48545d;font-size:14px;line-height: 24px;"> Hi Juliet</div></div><div style="font-size:16px;text-align:center;font-family:Arial, Helvetica, sans-serif;margin-top: 50px;"><div style="color:#48545d;font-size:14px;line-height:24px;"> Congratulation! Your trainers profile has been successfully accredited by the NYSC.</div><div style="color:#48545d;font-size:14px;line-height:24px;"> Click the button below to view your profile</div></div><div align="center" style="margin-top: 50px;"> <span> <a href="'.$profile_link.'" style="padding:10px 50px; font-family: -apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Oxygen","Ubuntu","Cantarell","Fira Sans","Droid Sans","Helvetica Neue",sans-serif; background-color:#f4c026; color:#ffffff; border-radius:3px; text-decoration:none;"> View your profile </a> </span></div><div style="font-size:16px;text-align:center;font-family:Arial, Helvetica, sans-serif; margin-top: 50px;"><div style="color:#48545d;font-size:14px;line-height: 24px;"> Or copy the link below and paste in your browser:</div></div><div style="font-size:16px;text-align:center;font-family:Arial, Helvetica, sans-serif;margin-top: 20px;"><div style="color:#48545d;font-size:14px;line-height: 24px;"> <a><u>'.$profile_link.'</u></a></div></div>';

                    wp_mail( $to, $subject, $message );
                }

                /* Redirect */
                printf('<script>window.location.replace("%s")</script>', currentUrl(true).'?user-id='.$trainer_id.'&state-id='.$assigned_state_id);
            ?>
            
            <?php } elseif( $_GET['view'] == 'form-nysc-accreditation' ){ ?>
            
            <?php 
                if($_POST){
                    /* Publish / Unpublish & Return */
                    /* Meta value to save */
                    $term = $_POST['state-id'];
                    
                    $toSave = array(
                        'status' => 'admin_published',
                        'skills' => $_POST['skills']
                    );
                    
                    $value = maybe_serialize($toSave);

                    /* Get saved meta */
                    $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term, true );

                    if ( $saved_meta ) //If published, Unpublish
                        delete_post_meta( $post_id, $accreditation_key.'_'.$term );
                    else //If Unpublished, Publish
                        update_post_meta( $post_id, $accreditation_key.'_'.$term, $value );

                    /* Redirect */
                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                } elseif( $_GET['id'] && $_GET['skill-id'] ) {
                    /* Publish / Unpublish & Return */
                    $term = $_GET['id'];
                    $skill = $_GET['skill-id'];
                    switch_to_blog(110);
                    $state = get_term( $term, 'state');
                    restore_current_blog();
                    
                    $saved_skill = get_term($_GET['skill-id']);
                    
                    $saved_meta = get_post_meta( $post_id, $accreditation_skill_status_key.'_'.$term.'_'.$skill, true );
                    
                    if($saved_meta == 'user_published'){
                        $value = 'admin_published';
                        
                        /* Send Activation mail */
                        $profile_link = 'https://www.saedconnect.org/service-provider-directory/trainer-profile/';
                        $message = '<div style="font-size:16px;text-align:center;font-family:Arial, Helvetica, sans-serif;margin-top: 100px;"><div style="text-align: left; color:#48545d;font-size:14px;line-height: 24px;"> Hi</div></div><div style="font-size:16px;font-family:Arial, Helvetica, sans-serif;margin-top: 20px;"><div style="color:#48545d;font-size:14px;line-height:24px;"> Congratulation! You have been accredited by The NYSC SAED ('.$state->name.' state) as a(n) <strong style="font-weight:600;">'.$saved_skill->name.'</strong> trainer.</div><div style="color:#48545d;font-size:14px;line-height:24px;"> Click the button below to view your profile</div></div><div align="center" style="margin-top: 50px;"> <span> <a href="'.$profile_link.'" style="padding:10px 50px; font-family:Arial, Helvetica, sans-serif; background-color:#f4c026; color:#ffffff; border-radius:3px; text-decoration:none;"> View your profile </a> </span></div><div style="font-size:16px;text-align:center;font-family:Arial, Helvetica, sans-serif; margin-top: 50px;"><div style="color:#48545d;font-size:14px;line-height: 24px;"> Or copy the link below and paste in your browser:</div></div><div style="font-size:16px;text-align:center;font-family:Arial, Helvetica, sans-serif;margin-top: 20px;"><div style="color:#48545d;font-size:14px;line-height: 24px;"> <a href="'.$profile_link.'"><u>'.$profile_link.'</u></a></div></div>';

                        $to = $user_info->user_email;
                        $subject = 'SAEDConnect: Trainer Profile Accreditation ('.$state->name.' state)';

                        wp_mail( $to, $subject, $message );
                    }elseif($saved_meta == 'admin_published'){
                        $value = 'user_published';
                        $message = 'Approval Canceled';
                    }
                        
                    update_post_meta( $post_id, $accreditation_skill_status_key.'_'.$term.'_'.$skill, $value );
                    
                    

                    /* Redirect */
                    printf('<script>window.location.replace("%s")</script>', currentUrl(true).'?user-id='.$trainer_id.'&state-id='.$assigned_state_id);
                }
            ?>
            
            <?php
                
            ?>

            <?php } else { //Empty Profile Message ?>

                <!-- Trainer Information -->
                <div class="section-wrapper">

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Empty! Start by creating your <?php echo $title ?>.
                            </h3>
                        </div>    

                    <?php } else { ?>

                        <?php foreach( $entries as $entry ){ ?>

                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>

                            
                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Name of Training Outfit        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Brief Trainer Profile          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 5 ); ?>
                                        </p>
                                    </div>
                                </div>
                                <div id="trainerProfileContent" class="collapse" aria-labelledby="headingOne">
                                    <div class="row row-10">
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                What training models are you open to providing?         
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 3; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Skill Areas where you provide training          
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 2; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Name of Contact person for any enquiries         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 8 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                States where you can offer private tutoring & classroom trainings          
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 7; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Primary Contact Address         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 9 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact Email address (Where anyone interested in this training should email for enquiry)
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 10 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact Phone Number (Where anyone interested in training with you should call for enquiry)         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 11 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact WhatsApp Number (Where anyone interested in training with you should WhatsApp for enquiry)          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 12 ); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center padding-b-15">
                                    <a data-toggle="collapse" href="#trainerProfileContent" aria-expanded="false" class="d-inline-block padding-o-10 txt-normal-s bg-blue txt-color-white">
                                        <span class="padding-r-10 txt-color-white">show more</span>
                                        <i class="fa fa-chevron-down txt-color-white"></i>
                                    </a>
                                </div>
                            </div>

                        <?php } ?>

                    <?php } ?>
                </div>

                <!-- Training Area -->
                <div class="section-wrapper padding-b-10">
                <?php
                    $title = 'Skills Trainer Train On';

                    $competency_gf_id = 11; //Form ID
                    $competency_gv_id = 681; //Gravity View ID

                    $entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $trainer_id, //Current logged in user
                            )
                        )
                    );

                    /* Get Competency Entries */
                    $competency_entries = GFAPI::get_entries( $competency_gf_id, $search_criteria );

                    /* Get Competency Entry Count */
                    $competency_entry_count = GFAPI::count_entries( $competency_gf_id, $search_criteria );

                    /* Get User Meta to populate Form */
                    $unserialized_data = maybe_unserialize( get_user_meta($trainer_id, 'training_area', true) );

                ?>

                    <?php if(!$unserialized_data){ //If no entry ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Empty! Start by Adding your <?php echo $title ?>.
                            </h3>
                        </div>                            

                    <?php } else { ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                            <?php
                                foreach($unserialized_data as $key => $subterms){
                                    $term = get_term($key);
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <div class="entry">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-15">
                                        <?php echo $term_name ?> skills    
                                    </p>
                                        <?php 
                                            foreach($subterms as $subterm){
                                                $child_term = get_term($subterm);
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name
                                        ?>

                                    <div class="row bg-ghostwhite padding-o-10 margin-b-10">
                                        <div class="col-5">
                                            <p class="txt-sm txt-color-dark txt-medium">
                                                <?php echo $child_term_name ?>
                                            </p>                                 
                                        </div>
                                        <p class="col-7 txt-xs text-right">
                                        <?php 
                                            $itExist = false;

                                            foreach( $competency_entries as $competency_entry ){ 
                                                if(rgar( $competency_entry, 1 ) == trim($child_term_id)){
                                                    $itExist = true;
                                                }
                                            }

                                        ?>
                                           
                                        </p>

                                            <?php 
                                                foreach( $competency_entries as $competency_entry ){ 
                                                    if(rgar( $competency_entry, 1 ) == $child_term_id){
                                            ?>
                                        <div class="col-12 padding-t-10 margin-t-10 border-t-1">
                                            <div class="row row-10">
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What is your experience in this subject?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 2 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        Number of Years of Experience     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 3 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What training models are you open to providing for this subject area?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php
                                                            $field_id = 4; // Update this number to your field id number
                                                            $field = RGFormsModel::get_field( $competency_gf_id, $field_id );
                                                            $value = is_object( $field ) ? $field->get_value_export( $competency_entry ) : '';
                                                            echo $value;
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                            <?php 
                                                    }
                                                }
                                            ?>

                                    </div>

                                        <?php } ?>
                                </div>

                            <?php } ?>


                    <?php } ?>
                </div>  
                
                <!-- Certifications / Affiliations -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 13; //Form ID
                    $gv_id = 912; //Gravity View ID
                    $title = 'Trainer Certifications / Affiliations';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $trainer_id, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $title ?>
                        </h2>
                    </div>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-sm txt-height-1-4 margin-t-10 margin-b-20">
                                What certifications do you possess? What year were they acquired? Please provide all the information about your certifications here.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Certifications / Affiliations Name
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Type       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 15 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        From       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 11 ); ?>
                                    </p>
                                </div>

                                <?php if( rgar( $entry, 12 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        To         
                                    </p>
                                    <p class="txt-sm">
                                    <?php 
                                        if( rgar( $entry, 12 ) ){
                                            echo rgar( $entry, 12 );
                                        } else {
                                            echo 'Present';
                                        }

                                    ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 19 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        To       
                                    </p>
                                    <p class="txt-sm">
                                    <?php 
                                        if( rgar( $entry, 19 ) ){
                                            echo rgar( $entry, 19 );
                                        } else {
                                            echo 'Present';
                                        }

                                    ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 13 ) ){ ?>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Your Achievements in the group       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 13 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Description      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 18 ); ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                    <?php } ?>
                </div>  

            <?php } ?>   
            </div>
            <div class="dashboard-multi-main-sidebar">
                <?php if( is_user_logged_in() ){ ?>
                    <div class="margin-t-20 padding-b-10">
                        <a data-toggle="modal" href="#messageModal" class="btn btn-blue w-100 txt-normal-s">
                           <span class="padding-r-5">Contact Me</span>
                           <i class="fa fa-envelope-o"></i>
                        </a>
                    </div>
                <?php } ?>
                
                <?php  
                    /* Get Validation data */
                    switch_to_blog(110);
                                   
                    /* Get Saved Users */
                    $saved_users = get_post_meta(1409, 'assigned_users');
                                   
                    restore_current_blog();
                ?>
                
                <?php if( in_array($current_user->ID, $saved_users) ){ ?>
                <div class="bg-grey padding-tb-30 padding-lr-20 margin-b-15">
                    <div class="txt-normal-s txt-medium text-center margin-b-15">
                        Trainer Profile Status
                    </div>
                    <div class="bg-yellow padding-o-10 rounded-o-12 text-center margin-b-15">
                        <?php
                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                            if($publication_meta == 'user_published')
                                echo 'Requested Approval';
                            elseif($publication_meta == 'admin_published') 
                                echo 'Published';
                            else
                                echo 'Draft';
                           
                        ?>
                    </div>
                    <p class="txt-xs text-center">
                       Publishing your profile allows you to be listed in the SAEDConnect trainer directory.
                       You must complete the Trainer Information section & select at least 1 skill that you train on, before you can publish your Trainer profile.
                    </p>
                    <div class="text-center padding-t-30">
                        <a href="<?php echo currentUrl(false).'?&view=publication' ?>" class="txt-normal-s txt-medium no-m-b">
                           <u class="txt-color-dark">
                           <?php
                                $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                if($publication_meta == 'admin_published')
                                    echo 'Unpublish';
                                else
                                    echo 'Publish Profile';
                            ?>
                            </u>
                        </a>
                    </div>
                </div>
                <?php } ?>
                
                <?php 
                    /*
                    *
                    * Populate Form Data from Terms
                    *
                    */
                    //Get Terms
                    $user_priviledge;
                    //$terms = get_terms( 'state', array('hide_empty' => false));
                    foreach ($terms as $term) { //Cycle through terms, one at a time
                        // Check and see if the term is a top-level parent. If so, display it.
                        $term_id = $term->term_id; //Get the term ID
                        $user_priviledge = get_user_meta( $current_user->ID, 'nysc_assigned_state', false );
                        foreach($user_priviledge as $priviledge){
                            if(in_array($term_id, $user_priviledge)){
                                $show_accreditation = true;
                            } 
                        }
                    }
                ?>
                <?php if( $show_accreditation ){ ?>   
                    <div class="padding-t-20 padding-lr-20 border-o-1 border-color-blue">
                        <div class="txt-normal-s txt-medium text-center margin-b-20">
                           NYSC Accreditation
                        </div>
                        <p class="txt-xs text-center margin-b-30">
                           Become recognized as a NYSC approved trainer under the SAED Program. Corp members who attend a training offered by an accredited SAED trainer are eligible to receive a special SAED certificate at the end of their service year.
                        </p> 
                        <div class="">
                            <?php 
                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                
                                $user_priviledge = get_user_meta( $current_user->ID, 'nysc_assigned_state', false );
                    
                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                                    $saved_meta = get_post_meta( $post_id, $accreditation_skill_key.'_'.$term_id, false );
                                    
                            ?>
                                <?php if( $saved_meta && in_array($term_id, $user_priviledge) ){ ?>
                                    <div class="margin-b-20">
                                        <div class="txt-sm txt-medium ">
                                            <a data-toggle="collapse" href="#accrediationRequestsCollapse-<?php echo $term_id ?>" aria-expanded="false" class="d-flex justify-content-between align-items-center">
                                                <span class="txt-color-txt-dark">
                                                    <?php echo $term_name; ?>
                                                </span>
                                                <i class="fa fa-angle-down padding-r-5"></i>
                                            </a>
                                        </div>
                                        <div id="accrediationRequestsCollapse-<?php echo $term_id ?>" class="collapse" aria-labelledby="headingOne">
                                            <div class="padding-o-10 margin-t-10 border-o-1 border-color-darkgrey">
                                                <p class="txt-xs">
                                                   Skills you requested accreditation for:
                                                </p>
                                                <div class="txt-xs txt-bold padding-t-15">
                                                    <?php 
                                                        $saved_skill_ids = $saved_meta;
                                                        foreach($saved_skill_ids as $key => $saved_skill_id){
                                                            $saved_skill = get_term($saved_skill_id);
                                                    ?>
                                                    <p 
                                                       class="<?php echo (  $key < ( count( $saved_skill_ids ) - 1 ) ? 'padding-b-10' : '' ); ?> d-flex justify-content-between align-items-center">
                                                        <span>
                                                            <?php echo $saved_skill->name; ?>
                                                            <span class="txt-color-yellow-dark">
                                                                <?php 
                                                                    $status = get_post_meta( $post_id, $accreditation_skill_status_key.'_'.$term_id.'_'.$saved_skill_id, true );

                                                                    if($status == 'user_published'){
                                                                        echo '(In Review)';
                                                                    } elseif($status == 'admin_published') {
                                                                        echo '(Published)';
                                                                    }
                                                                ?>
                                                            </span>
                                                        </span>
                                                        <a href="<?php echo currenturl().'&view=form-nysc-accreditation&id='.$term_id.'&skill-id='.$saved_skill_id ?>" class="txt-color-blue confirmation">
                                                            <?php
                                                                $status = get_post_meta( $post_id, $accreditation_skill_status_key.'_'.$term_id.'_'.$saved_skill_id, true );
                                                                if($status == 'admin_published')
                                                                    echo 'Cancel Approval';
                                                                else
                                                                    echo 'Approve';
                                                            ?>
                                                        </a>
                                                    </p> 
                                                    <?php 
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                           

                        </div>
                   </div>
                <?php } ?>
                
            </div>
        </section>
        
        <?php }else{ ?>
           
        <section class="container-wrapper bg-white text-center padding-tb-80">
            <h1 class="txt-7em txt-bold margin-b-40">
                Oops!
            </h1>
            <h2 class="uppercase txt-bold margin-b-20">
                Something went wrong
            </h2>
            <div class="row">
                <div class="col-md-4 mx-auto">
                    <p class="txt-sm">
                        The page you are looking for might have been removed, had it's name changed or is temporarily unavailable.
                    </p>
                </div>
            </div>
            <div class="margin-t-20">
                <a href="#" class="btn btn-trans-blue" onclick="goBack()">
                    <i class="fa fa-arrow-left"></i>
                    <span class="padding-l-10">
                        Go Back
                    </span>
                </a>
                <a href="https://www.saedconnect.org/" class="btn btn-blue">
                    <i class="fa fa-home"></i>
                    <span class="padding-l-10">
                        Go to Homepage
                    </span>
                </a>
            </div>
    </section>
        
        <?php } ?>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>

<!-- iCheck -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/_all.css" integrity="sha256-808LC4rdK/cu4RspCXPGrLKH7mgCcuNspF46UfBSbNQ=" crossorigin="anonymous" />

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>