<?php /*Template Name: Problems - Career*/ ?>
    

<?php get_header() ?>
   
    <main class="main-content">
        <div class="container-wrapper padding-t-40"> 
            <div class="color-hero bg-purple txt-color-white">
                <div class="content">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="txt-4em txt-bold margin-b-20">
                                I need a Job
                            </h1>
                            <h2 class="txt-height-1-7 txt-lg">
                                Build, showcase & verify your competency - and get connected to a large pool of employers looking to hire you.
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 d-flex">
                    <article class="feature-image-block sm" style="background-image:url('https://www.saedconnect.org/wp-content/themes/SAEDConnectTheme/images/grid/image (21).jpg');">
                        <div class="content txt-color-white">
                            <h3 class="txt-xxlg txt-medium margin-b-10">
                                Sell your Skills
                            </h3>
                            <p class="txt-sm margin-b-30">
                                JobTrac helps you build, compile and verify your competency in specific job roles and then connects you to employers looking to hire verified, job-ready candidates in those roles.
                            </p>
                            <a class="btn btn-trans-wb" href="https://www.saedconnect.org/jobtrac/">
                                Explore
                            </a>
                        </div>   
                    </article>
                </div>
                <div class="col-md-4 d-flex">
                    <article class="feature-image-block sm" style="background-image:url('https://www.saedconnect.org/wp-content/themes/SAEDConnectTheme/images/grid/image (12).jpg');">
                        <div class="content txt-color-white">
                            <h3 class="txt-xxlg txt-medium margin-b-10">
                                Explore Jobs
                            </h3>
                            <p class="txt-sm margin-b-30">
                                Get seasoned mentors to show you how to craft a great CV or cover letter, search for the best jobs and get interviews, ace job interviews and more
                            </p>
                            <div class="dropdown">
                            <button class="btn btn-trans-wb w-100 d-flex justify-content-between align-items-center dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Explore
                                </button>
                                <div class="dropdown-menu txt-normal-s padding-tb-20" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item margin-b-10" href="#">View Full-time/Project Jobs</a>
                                    <a class="dropdown-item margin-b-10" href="#">View Freelancer Jobs</a>
                                    <a class="dropdown-item margin-b-10" href="#">View Training Jobs</a>
                                    <a class="dropdown-item margin-b-10" href="#">View Business Partner Openings</a>
                                </div>
                            </div>
                        </div>   
                    </article>
                </div>
                <div class="col-md-4 d-flex">
                    <article class="feature-image-block sm" style="background-image:url('https://www.saedconnect.org/wp-content/themes/SAEDConnectTheme/images/grid/image (12).jpg');">
                        <div class="content txt-color-white">
                            <h3 class="txt-xxlg txt-medium margin-b-10">
                                Job Programs
                            </h3>
                            <p class="txt-sm margin-b-30">
                                Get seasoned mentors to show you how to craft a great CV or cover letter, search for the best jobs and get interviews, ace job interviews and more
                            </p>
                            <a class="btn btn-trans-wb" href="https://www.saedconnect.org/growth-programs/program/career-forum/">
                                Connect
                            </a>
                        </div>   
                    </article>
                </div>
            </div>
        </div>
        <div class="container-wrapper">
            <div class="padding-tb-60">
                <!--<h4 class="txt-xxlg txt-bold txt-height-1-2 margin-b-60">
                    Hire a Business Support Expert
                </h4>-->
                <div class="support-list">
                    <?php

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'problem',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'problem-type',
                                        'field'    => 'ID',
                                        'terms'    => 131,
                                    ),
                                )
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID

                            /* Get Opportunity Banner */
                            $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                            $image = reset( $images );
                    ?>
                        
                        <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                            <div class="row">
                                <div class="col-md-5">
                                    <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                        <a class="txt-color-green" data-toggle="modal" href="#problemModal-<?php echo $post_id; ?>" aria-expanded="false">
                                            <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                            <?php the_title() ?>
                                        </a>
                                    </h4>
                                </div>
                                <div class="col-md-4"> 
                                    <article class="text-box sm">
                                        <p><?php echo rwmb_meta( 'problem-summary' ) ?></p>
                                    </article>
                                </div>
                                <div class="col-md-3 text-right txt-xlg">
                                    <a class="txt-color-green d-none d-md-inline" data-toggle="modal" href="#problemModal-<?php echo $post_id; ?>" aria-expanded="false">
                                        <span class="txt-xs txt-bold">Learn How</span>
                                        <i class="fa fa-long-arrow-right padding-lr-5"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Details Modal -->
                        <div class="modal fade font-main filter-modal" id="problemModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentfaqModal" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header padding-lr-30">
                                        <h5 class="modal-title txt-medium" id="exampleModalLabel"><?php the_title() ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body padding-o-30">
                                        <p class="margin-b-30"><?php echo rwmb_meta( 'problem-summary' ) ?></p>
                                            <?php $group_values = rwmb_meta( 'problem-answer-group' ); ?>

                                            <?php $answer_group_counter = 1 ?>
                                            <?php foreach($group_values as $value){ ?>

                                            <div class="margin-b-5">
                                                <h2>
                                                    <a class=" d-flex align-items-center justify-content-between bg-ash txt-normal-s  padding-o-15" data-toggle="collapse" href="#collapseAnswerGroup-<?php echo $post_id."-".$answer_group_counter ?>" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                        <span class="txt-medium txt-color-white">
                                                            <?php echo $value['problem-answer-group-title'] ?>
                                                        </span>
                                                        <span class="dropdown-toggle txt-color-white"></span>
                                                    </a>
                                                </h2>
                                                <div class="collapse txt-normal-s" id="collapseAnswerGroup-<?php echo $post_id."-".$answer_group_counter ?>">
                                                    <div class="border-o-1 border-color-darkgrey padding-lr-15 padding-t-15">
                                                        <?php $answer_counter = 1 ?>
                                                        <?php foreach( $value['problem-answer-group-answers'] as $answer ){ ?>
                                                            <p class="d-flex padding-b-15">
                                                                <span style="width:10px;" class="txt-medium">
                                                                    <?php echo $answer_counter ?>.
                                                                </span>
                                                                <span class="padding-l-10">
                                                                    <?php echo $answer['problem-answer-group-answer'] ?>
                                                                </span>
                                                            </p>
                                                            <?php $answer_counter++ ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $answer_group_counter++ ?>
                                            <?php } ?>
                                    </div>
                                    <div class="modal-footer padding-lr-30">
                                        <button type="button" class="btn btn-blue txt-xs" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <h2 class="txt-lg txt-medium">
                            No Problems added.
                        </h2>

                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="container-wrapper text-center">
            <div class="row row-10">
                <div class="col-md-6 padding-lr-10 padding-b-20">
                    <div class="bg-grey padding-tb-40 padding-lr-20">
                        <div class="row">
                            <div class="col-md-6 mx-auto">
                                <h3 class="txt-medium txt-xlg margin-b-15">
                                    Interested in starting a business instead?
                                </h3>
                                <p class="txt-normal-s">
                                    Make money on your own terms. Get help to overcome your fears, start any business of your choice, and grow your small business into an empire.
                                </p>
                                <div class="margin-t-30">
                                    <a class="btn btn-blue no-m-b" href="https://www.saedconnect.org/information/entrepreneurial-and-business-challenges/">
                                        Do Business
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 padding-lr-10 padding-b-20">
                    <div class="bg-grey padding-tb-40 padding-lr-20">
                        <div class="row">
                            <div class="col-md-6 mx-auto">
                                <h3 class="txt-medium txt-xlg margin-b-15">
                                    Interested in making Extra income?
                                </h3>
                                <p class="txt-normal-s">
                                    Build, showcase & verify your competency - and get connected to a large pool of employers looking to hire you.
                                </p>
                                <div class="margin-t-30">
                                    <a class="btn btn-blue no-m-b" href="https://www.saedconnect.org/information/make-extra-income/">
                                        Make Extra Income
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
<!-- SHC Modal -->
<?php get_template_part( 'template-parts/_shc_modal' ); ?>    

<!-- SHC Free Communuties Modal -->
<?php get_template_part( 'template-parts/_shc_free_modal' ); ?>
    
<?php get_footer() ?>