    <?php /*Template Name: Corp Member Verification Page*/ ?>
    
    <?php
                    
        /*if ( is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            //$dashboard_link = get_site_url().'/my-dashboard'; //Get Daasboard Page Link by ID
            $dashboard_link = network_home_url().'/growth-programs/my-dashboard'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }*/
    
    ?>
    
    <?php get_header() ?>
    
    <!-- // If User is not Logged in, Load Login Page -->
    <main class="main-content">
        <section class="container-wrapper padding-t-40 padding-b-80">
            <div class="row">
                <div class="col-md-6 mx-auto text-center txt-height-1-5">                   
                    <?php 
                        if (!$_POST['call_up_number']){
                            
                    ?>
                        <header class="margin-b-40">
                            <h1 class="txt-2em txt-bold margin-b-20">
                                Corp Member Verification
                            </h1>
                            <h2>
                                Enter your call up number
                            </h2>
                        </header> 
                          
                        <form method="post">
                            <input type="text" name="call_up_number">
                            <input type="submit">
                        </form>
                           
                    <?php } else { ?>
                           
                        <header class="margin-b-40">
                            <h1 class="txt-2em txt-bold">
                                Corp Member Verification
                            </h1>
                        </header> 
                           
                    <?php
                        function generateVerificationCode(){
                            /* Validate Verification Code Uniqueness */
                            $generated_token = md5(time());
                            
                            /* Check if token exists */
                            global $wpdb;
                            $nysc_list_db = new wpdb('root','root','saedconnect_wp_db','localhost');
                            $token = $nysc_list_db->get_row("SELECT verification_token FROM corp_member_profile WHERE verification_token = '".$generated_token."' LIMIT 0,1");
                            
                            if( $token ){
                                generateVerificationCode();
                            } else {
                                return $generated_token;
                            }
                        }
                            
                        /* Get Call-Up Number User entered */  
                        $query_cu_number = $_POST['call_up_number'];
                                                        
                        /* Check Call-Up number in from DB */
                        //$nysc_list_db = new wpdb('root','root','nysc_db','localhost'); //Local
                        $nysc_list_db = new wpdb('root','umMv65ekyMRxfNfm','nysc_db','localhost'); //Server
                        $corp_member = $nysc_list_db->get_row("SELECT call_up_number, verification_status, first_name, last_name, middle_name FROM corp_member_profile WHERE call_up_number = '".$query_cu_number."' LIMIT 0,1");

                            if ( $corp_member ){
                                                                
                                /* If User is trying to verify Call-Up Number for the first time */
                                if ( !$corp_member->verification_status ){
                                    
                                    /* Generate Verification Token */
                                    $verification_token = generateVerificationCode();

                                    /* Update Verification status to PENDING */
                                    $nysc_list_db->update( 
                                        'corp_member_profile', //Table
                                        array(
                                            "verification_status" => "pending", 
                                            "verification_token" => $verification_token
                                        ), //Data
                                        array("call_up_number" => $corp_member->call_up_number), //Where?
                                        array("%s", "%s"), //Data format
                                        array("%s") //Where format
                                    );
                                    
                                    /* Send Activation mail */
                                    $activation_link = 'https://www.saedconnect.org/corp-member-profile-activation/?activation-code='.$verification_token;
                                    
                                    $to = $corp_member->email;
                                    $subject = 'Activate your SAEDConnect Profile';
                                    
                                    $message = '<p>Hello, '.$corp_member->last_name.' '.$corp_member->first_name.' '.$corp_member->middle_name.'</p>';
                                    
                                    $message .= '<p>Your Call-Up Number: <strong>'.$corp_member->call_up_number.'</strong> has been submitted for the SAEDConnect Corp Member Profile activation. Click the link below to activate your account.</p>';
                                    
                                    $message .= '<p><strong><a href="'.$activation_link.'">Activate profile</a></strong></p>';

                                    wp_mail( $to, $subject, $message );
                                    
                                    /* Confirmation Message: Success */
                                    printf('<div class="margin-tb-20">Congatulations %s, %s %s, <br><br> Your Call Up number: <span class="txt-bold">%s</span> has been verified.<br><br> An activation mail has been sent to your email address with instructions on how to activate your SAEDConnect Corp Member Profile.</div>', $corp_member->last_name, $corp_member->first_name, $corp_member->middle_name, $corp_member->call_up_number);
                                    
                                } elseif ( $corp_member->verification_status == 'verified' ) {
                                    
                                    /* Confirmation Message: Verified */
                                    printf('<div class="margin-tb-20">Congratulations %s, %s %s, <br><br> Your Call Up number: <span class="txt-bold">%s</span> has been verified.<br><br> And Your SAEDConnect Corp Member Profile is activated.</div>', $corp_member->last_name, $corp_member->first_name, $corp_member->middle_name, $corp_member->call_up_number);
                                    
                                    echo '<p class="txt-normal-s txt-medium">If this is your Call-up Number and you would like to claim it, kindly contact the SAEDConnect <a class="txt-color-blue" href="">Help Center</a></p>';
                                    
                                } elseif ( $corp_member->verification_status == 'pending' ) {
                                
                                    /* Confirmation Message: Pending Verification */
                                    printf('<div class="margin-tb-20">Hello %s, %s %s, <br><br> Your Call Up number: <span class="txt-bold">%s</span> has already been verified.<br><br> An activation mail was sent to your email address with instructions on how to activate your SAEDConnect Corp Member Profile.</div>', $corp_member->last_name, $corp_member->first_name, $corp_member->middle_name, $corp_member->call_up_number);
                                    
                                    echo '<p class="txt-normal-s txt-medium">If this is your Call-up Number and you are not the one who requested the pending verification request, kindly contact the SAEDConnect <a class="txt-color-blue" href="">Help Center</a></p>';
                                    
                                }
                            
                            } else {
                                /* Confirmation Message: Record not found */
                                printf('<div class="margin-tb-20">The call-up number you entered: <span class="txt-bold">%s</span><br> Does not exist in our Database</div>', $query_cu_number);
                                
                            }
                            

                        }
                    ?>
                    <div class="margin-t-40">
                        <a href="#" class="btn btn-trans-blue" onclick="goBack()">
                            <i class="fa fa-arrow-left"></i>
                            <span class="padding-l-10">
                                Go Back
                            </span>
                        </a>
                        <a href="https://www.saedconnect.org/" class="btn btn-blue">
                            <i class="fa fa-home"></i>
                            <span class="padding-l-10">
                                Go to Your Dashboard
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    
    <?php get_footer() ?>