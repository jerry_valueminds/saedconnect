<?php get_header() ?>
   
<?php
    $current_user = wp_get_current_user();
?>
    
<?php while ( have_posts() ) : the_post(); ?>

<?php
    $post_id = get_the_ID();
    
    /*Get Theme Color*/
    $theme_color = rwmb_meta( 'information-page-theme' );

    /*Get Theme Alt Color*/
    $theme_alt_color = rwmb_meta( 'information-page-theme-alt' );

    /* Get Image data */
    $program_images = rwmb_meta( 'information-page-image', array( 'limit' => 1 ) );
    $program_images = reset( $program_images );
?>

<main class="main-content-home">
    <!--<section class="container-wrapper padding-tb-80">
        <div class="row text-center padding-b-80">
            <div class="col-md-9 mx-auto">
                <h2 class="txt-3em margin-b-20">
                    SKILL-UP CLIMB-UP<br>LEARNING TRACKS
                </h2>
                <p class="txt-color-light txt-height-1-7">
                    This projects involves empowering Nigerians with skills in the following tracks
                </p>
            </div>
        </div>
        <div class="row row-20">
            <a class="col-md-3 padding-lr-20 d-flex" href="">
                <div class="change-round-card">
                    <div class="image">
                        <figure style="background-image: url(images/nav/career_coaching_service.jpg)"></figure>
                    </div>
                    <div class="title">
                        Digital Skills
                    </div>
                </div>
            </a>
            <div class="col-md-3 padding-lr-20 d-flex">
                <div class="change-round-card">
                    <div class="image">
                        <figure style="background-image: url(images/nav/career_coaching_service.jpg)"></figure>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding-lr-20 d-flex">
                <div class="change-round-card">
                    <div class="image">
                        <figure style="background-image: url(images/nav/career_coaching_service.jpg)"></figure>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding-lr-20 d-flex">
                <div class="change-round-card">
                    <div class="image">
                        <figure style="background-image: url(images/nav/career_coaching_service.jpg)"></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <?php get_template_part( 'template-parts/content-section-template' ); ?>   
</main>

<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>