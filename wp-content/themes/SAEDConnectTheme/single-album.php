<?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>

    <main class="font-main">
        <div class="album-gallery relative">
            <header class="album-header">
                <a class="brand" href="index.html">
                    <img class="icon" src="images/icons/logo.png" alt="">
                    <span class="name">SAEDConnect</span>
                </a>
                <div class="album-navigation">
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                    <!-- Add Navigation -->
                    <div class="navigation-wrapper">
                        <div class="swiper-button-prev"></div> 
                        <div class="swiper-button-next"></div>
                    </div>
                    <a class="close-album" href="<?php wp_get_referer() ?>">
                        <i class="fa fa-times"></i>
                    </a>
                </div>  
            </header>
            <section class="swiper-wrapper">
                <?php 
                    global $relashionship_id; 
                    global $relationship_parent_id;

                    $relashionship_id = 'album_to_album_content';
                    $relationship_parent_id = get_the_ID();
                ?>
                <?php get_template_part( 'template-parts/album-slides' ); ?>
            </section>
            
        </div>
    </main>
    
<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>
    