    
    <?php get_header() ?>
    <style type="text/css">
        .um-button, .um-button.um-alt{
            position: relative !important;
            outline: 0 !important;
            font-weight: 500 !important;
            border-radius: 20px !important;
            transition: all 0.4s ease !important;
        }
    </style>

        <main class="main-content font-main">
            <section class="container text-center">
                <h1 class="txt-2em txt-medium margin-b-20">
                    <?php the_title(); ?>
                </h1>
            </section>
        <?php
            // TO SHOW THE PAGE CONTENTS
            while ( have_posts() ) : the_post();
            
            $cv_id = get_the_ID();
            $cv_entry_id = wp_strip_all_tags( get_the_content() );
            $relationship_type = 'cv_to_school';
        ?>
            
           <section class="padding-b-20">
               <div class="container">
                  <?php //echo do_shortcode( "[stickylist id='6']"); ?>
                  
                   <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-t-20 padding-lr-30">
                        <h1 class="txt-xlg txt-medium margin-b-20">
                            Personal Information
                        </h1>
                        
                      <!-- <?php echo do_shortcode('[gravityform id="5" title="false" description="false" field_values="parent_cv_id='.$cv_id.'&relationship_type='.$relationship_type.'"]'); ?>-->
                      
                    
                    <!--Using Get entries-->
                    <?php
                       
                        $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $cv_entry_id );
                        $entries = GFAPI::get_entries( 6, $search_criteria );

                        foreach($entries as $entry){
                            $post = get_post( $entry['post_id'] ); 
                   ?>
                                
                        <div class="margin-b-20">
                            <!-- DOB -->
                            <?php
                                $meta = get_post_meta($post->ID, 'cv-personal-info-dob', true);
                                if($meta){
                            ?>
                                <p>
                                    <span class="txt-bold">DOB: </span>
                                    <?php echo $meta; ?>
                                </p>
                            <?php
                                }
                            ?>
                            
                            <!-- Nationality -->
                            <?php
                                $meta = get_post_meta($post->ID, 'cv-personal-info-nationality', true);
                                if($meta){
                            ?>
                                <p>
                                    <span class="txt-bold">Nationality: </span>
                                    <?php echo $meta; ?>
                                </p>
                            <?php
                                }
                            ?>
                            
                            <!-- Gender -->
                            <?php
                                $meta = get_post_meta($post->ID, 'cv-personal-info-gender', true);
                                if($meta){
                            ?>
                                <p>
                                    <span class="txt-bold">Gender: </span>
                                    <?php echo $meta; ?>
                                </p>
                            <?php
                                }
                            ?>
                            
                            <!-- Marital Status -->
                            <?php
                                $meta = get_post_meta($post->ID, 'cv-personal-info-marital', true);
                                if($meta){
                            ?>
                                <p>
                                    <span class="txt-bold">Marital Status: </span>
                                    <?php echo $meta; ?>
                                </p>
                            <?php
                                }
                            ?>
                        </div>
                            
                        <form action="http://127.0.0.1/saedconnect_wp/edit-personal-information/" method="post">
                            <button class="sticky-list-edit submit btn">Edit</button>
                            <input type="hidden" name="mode" value="edit">
                            <input type="hidden" name="edit_id" value="<?php echo $entry['id']; ?>">
                        </form>
                        
                                
                    <?php

                        }

                    ?>
                    

                   </div>
               </div>
           </section>
           
           <section class="padding-b-40">
               <div class="container">
                   <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-tb-20 padding-lr-30">
                        <h1 class="txt-xlg txt-medium margin-b-20">
                            Schools
                        </h1>
                        <ul class="margin-b-20 padding-b-20 border-b-1 border-color-darkgrey">
                        <?php 

                            $connected = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => $relationship_type,
                                    'from' => $cv_id, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $connected->have_posts() ) : $connected->the_post(); ?>

                                <li class="row padding-b-15 margin-b-15 border-b-1 border-color-darkgrey">
                                    <div class="col-md-4">
                                        <a href=""><?php echo the_title() ?></a>
                                    </div>
                                    <div class="col-md-4">
                                        <!--<a href="http://127.0.0.1/saedconnect_wp/edit-school/?entry=<?php echo wp_strip_all_tags( get_the_content() );?>">Edit</a>-->
                                        <form action="http://127.0.0.1/saedconnect_wp/edit-school/" method="post">
                                            <button class="sticky-list-edit submit">Edit</button>
                                            <input type="hidden" name="mode" value="edit">
                                            <input type="hidden" name="edit_id" value="<?php echo wp_strip_all_tags( get_the_content() ); ?>">
                                        </form>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="">Delete</a>
                                    </div>
                                </li>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>
                        </ul>
                        
                        <!--<h2 class="txt-bold txt-lg margin-b-15">Using Get Entries</h2>
                        <?php 
                            $entries = GFAPI::get_entries( 4 );
                       
                            foreach($entries as $entry){
                                $post = get_post( $entry['post_id'] ); 
                                $meta = get_post_meta($post->ID, 'School test field', true);
                       ?>
                                
                        <div class="margin-b-10">
                            <p>
                                <span class="txt-bold">Title: </span>
                                <?php echo $post->post_title; echo ' - '; ?>
                            </p>
                            <p>
                                <span class="txt-bold">Meta Value: </span>
                                <?php echo $meta; ?>
                            </p>
                            <form action="http://127.0.0.1/saedconnect_wp/edit-school/" method="post">
                                <button class="sticky-list-edit submit">Edit</button>
                                <input type="hidden" name="mode" value="edit">
                                <input type="hidden" name="edit_id" value="<?php echo $entry['id'] ?>">
                            </form>
                            <form action="http://127.0.0.1/saedconnect_wp/edit-school/" method="post">
                                <button class="sticky-list-duplicate submit">Duplicate</button>
                                <input type="hidden" name="mode" value="duplicate">
                                <input type="hidden" name="duplicate_id" value="<?php echo $entry['id'] ?>">
                            </form>
                        </div>
                                
                        <?php
                                
                            }
                            
                        ?>
                        -->
                        
                        
                        
                        <?php echo do_shortcode( "[stickylist id='4' field='2' value='".$cv_id."' ]");?>
                        <h1 class="txt-lg txt-medium margin-b-20 margin-t-40">
                            Add New School
                        </h1>
                        
                       <?php echo do_shortcode('[gravityform id="4" title="false" description="false" field_values="parent_cv_id='.$cv_id.'&relationship_type='.$relationship_type.'"]'); ?>
                       
                   </div>
               </div>
           </section>
        <?php
            endwhile; //resetting the page loop
            wp_reset_query(); //resetting the page query
        ?>

    </main>
        
    <?php get_footer() ?>