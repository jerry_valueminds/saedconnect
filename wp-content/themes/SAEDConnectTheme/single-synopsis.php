    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
        <main class="main-content">
            <?php
                /* Set Up Variables */
                $mini_site_id;
                $minisite_name;
            
                $multi_item_name = get_the_title();
                $multi_item_id = get_the_ID();
            

                /* Get data from minisite */
                $connected = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'mini_site_to_m_item_page',
                        'to' => $multi_item_id, // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $connected->have_posts() ) : $connected->the_post();

                    $mini_site_id = get_the_ID();
                    $minisite_name = get_the_title();

                endwhile;
                wp_reset_postdata();
            ?>
            
            <!-- Page title begin -->
            <?php if($minisite_name){ ?>
                <header class="overview-header container-wrapper">
                    <h1 class="title">
                        <a href="<?php the_permalink(); ?>"><?php echo $minisite_name; ?></a>
                    </h1>
                    <nav class="header-nav">
                        <div class="header-nav-title">
                            <span class="name">
                                Submenu
                            </span>
                            <span class="icon"></span>
                        </div>
                        <ul>

                        <!-- Second Loop - I: Get All Multi-Items related to this Mini-Site --> 
                        <?php 

                            $nav_1 = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_m_item_page',
                                    'from' => $mini_site_id, // You can pass object ID or full object
                                    'sibling' => true,
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $nav_1->have_posts() ) : $nav_1->the_post();
                        ?>   

                            <li class="<?php echo ($multi_item_name == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>

                        <!-- Second Loop - II: Get All Multi-Pages related to this Mini-Site --> 
                        <?php 

                            $nav_2 = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_three_c_m_page',
                                    'from' => $mini_site_id, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $nav_2->have_posts() ) : $nav_2->the_post();
                        ?>   

                            <li class="<?php echo ($multi_item_name == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>

                        <!-- Thirs Loop - II: Get All 2olumn Multi-Pages related to this Mini-Site --> 
                        <?php 

                            $nav_3 = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_two_c_m_page',
                                    'from' => $mini_site_id, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $nav_3->have_posts() ) : $nav_3->the_post();
                        ?>   

                            <li class="<?php echo ($multi_item_name == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>
                        </ul>
                    </nav>
                </header>
            <?php }else{ ?>
            
                <header class="overview-header container-wrapper">
                    <h1 class="title">
                        <?php echo $multi_item_name ?>
                    </h1>
                </header>
            
            <?php } ?>
            <!-- Page title End -->
           
            <?php
                /* Check For For main content */
                $check_cb = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'm_item_page_to_cb',
                        'from' => get_the_ID()
                    ),
                    'nopaging' => true,
                ) );
            
                /* Show section if there is Content */
                if($check_cb->post_count > 0){
            ?>
                <!--Show Main & Side Content Blocks-->
                <section>
                    <div class="row">
                        <!-- Main Content -->
                        <div class="col-md-8 left-content padding-t-40">
                            <?php 
                                global $main_content_relashionship_id;
                                global $main_content_relationship_parent_id;

                                $main_content_relashionship_id = 'm_item_page_to_cb';
                                $main_content_relationship_parent_id = get_the_ID();
                            ?>
                            <?php get_template_part( 'template-parts/multi-item-main-content' ); ?>
                        </div>

                        <!-- Right Side Content -->
                        <div class="col-md-4">
                            <?php
                                global $side_content_relashionship_id;
                                global $side_content_relationship_parent_id;

                                $side_content_relashionship_id = 'm_item_page_to_rcb';
                                $side_content_relationship_parent_id = get_the_ID();

                                get_template_part( 'template-parts/multi-item-side-content' );
                            ?>   
                        </div>
                    </div>
                </section>
            <?php }else{ ?>
                <!--Prepare for only Bottom Content Blocks-->
                
            <?php } ?>

            <!-- General Section -->
            <section class="row">
                <?php 
                    global $relashionship_id;
                    global $relationship_parent_id;
                        
                    $relashionship_id = 'm_item_page_to_bcb';
                    $relationship_parent_id = get_the_ID();
                ?>
                <?php get_template_part( 'template-parts/multi-item-general-content' ); ?>
            </section>
            
        </main>
    <?php endwhile; // end of the loop. ?>

    <?php get_footer() ?>