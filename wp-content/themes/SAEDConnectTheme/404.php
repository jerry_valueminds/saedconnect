<?php get_header() ?>
   
    <main class="main-content">
        <section class="container-wrapper bg-white text-center padding-tb-80">
            <h1 class="txt-7em txt-bold margin-b-40">
                Oops!
            </h1>
            <h2 class="uppercase txt-bold margin-b-20">
                404 - Page not found
            </h2>
            <div class="row">
                <div class="col-md-4 mx-auto">
                    <p class="txt-sm">
                        The page you are looking for might have been removed, had it's name changed or is temporarily unavailable.
                    </p>
                </div>
            </div>
            <div class="margin-t-20">
                <a href="#" class="btn btn-trans-blue" onclick="goBack()">
                    <i class="fa fa-arrow-left"></i>
                    <span class="padding-l-10">
                        Go Back
                    </span>
                </a>
                <a href="https://www.saedconnect.org/" class="btn btn-blue">
                    <i class="fa fa-home"></i>
                    <span class="padding-l-10">
                        Go to Homepage
                    </span>
                </a>
            </div>
        </section>
    </main>
    
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    
<?php get_footer() ?>