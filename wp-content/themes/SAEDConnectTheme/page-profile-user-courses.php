<?php /*Template Name: Profile - User Courses*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* Publication */
        $publication_key   = 'publication_status';
        $accreditation_key   = 'nysc_accreditation_status';
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Get Avatar */
        $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
        $meta_key = 'user_avatar_url';
        $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

        if($get_avatar_url){
            $avatar_url = $get_avatar_url;
        }

        /* Get Trainer profile */
        $user_id = $_GET['user-id'];
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <?php get_header() ?>
    
    <main class="main-content txt-color-light bg-white">
        <?php get_template_part( 'template-parts/user-dashboard/_dashboard_strip' ); ?>
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav-user-profile' ); ?>
            <div class="dashboard-multi-main-content">
                <div class="page-header">
                    <h1 class="page-title">
                        My Courses
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Here you can request for a courses in any subject area here.
                    </p>
                </article>
                <div class="row row-5">
                   <style>
                        .publication-status{
                            position: absolute;
                            display: flex;
                            align-items: center;
                            font-weight: 500;
                            top: -0.1em;
                            right: 0.4em;
                            padding: 15px;
                            color: black;
                            background-color: gainsboro;
                        }

                        .user_published{
                           background-color: #f4c026; 
                        }

                        .admin_published{
                           background-color: #00bfe7; 
                        }

                        .home-community-card{
                            overflow: visible;
                        }
                    </style>
                    <?php
                        

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'training-offer',
                                'post_status' => 'publish',
                                'author' => $user_id,
                                'posts_per_page' => -1,
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID

                    ?>
                        <div class="col-md-4 padding-lr-5 padding-b-20 d-flex">
                            <div class="home-community-card">
                                <article class="content">
                                    <figure>
                                        <?php $images = get_attached_media( 'image', $post_id ); ?>

                                        <?php if($images){ ?>

                                            <?php  
                                                foreach($images as $image) { //print_r( $image ); 
                                                    $previousImg_id = $image->ID;
                                            ?>
                                                <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">

                                            <?php } ?>

                                        <?php } else { ?>
                                            <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png" width="150" class="margin-b-60">
                                            </figure>

                                        <?php } ?>
                                    </figure>
                                    <div class="info bg-white">
                                        
                                        <h3 class=" txt-medium margin-b-10">
                                            <a href="<?php the_permalink() ?>" class="txt-bold">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <p class="txt-sm">
                                            <?php
                                                $meta = get_post_meta($post_id, 'summary', true);
                                                if($meta){
                                                    echo truncate($meta, 70);
                                                }
                                            ?>
                                        </p>
                                    </div>
                                </article>  
                            </div>   
                        </div>
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                            <h2 class="txt-lg txt-medium">
                                No Courses found.
                            </h2>
                        </div>   

                    <?php } ?>
                </div>
            </div>
            <div class="dashboard-multi-main-sidebar">
               
                <?php if( is_user_logged_in() ){ ?>
                    <div class="margin-t-20">
                        <a data-toggle="modal" href="#messageModal" class="btn btn-blue w-100 txt-normal-s">
                           <span class="padding-r-5">Contact Me</span>
                           <i class="fa fa-envelope-o"></i>
                        </a>
                    </div>
                <?php } ?>
                
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>

<!-- iCheck -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/_all.css" integrity="sha256-808LC4rdK/cu4RspCXPGrLKH7mgCcuNspF46UfBSbNQ=" crossorigin="anonymous" />

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>