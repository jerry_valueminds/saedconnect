    <?php /*Template Name: Homepage-YNL*/ ?>
    
    <?php get_header() ?>
    
    <style>
        .ynl-home-banner {
            display: flex;
            align-items: flex-start;
            padding-top: 60px;
            position: relative;
            background-position: bottom;
            background-repeat: no-repeat;
            background-size: contain;
            min-height: 480px !important;
        }
        
        .ynl-home-banner.image:after {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255, 255, 255, 0);
            z-index: 0;
            transition: all 0.4s ease;
        }
        
        .ynl-home-small-bg:before {
            height: 180px;
        }
    </style>
    
    <main class="main-content">
        <header class="ynl-home-banner lg image" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/hero_ynl.png');">
            <div class="content container-wrapper">
                <div class="row">
                    <div class="col-lg-6 mx-auto text-center">
                        <div class="text-center padding-b-20">
                            <span class="bg-grey txt-normal-s padding-tb-10 padding-lr-20">
                                JOIN THE YOUNG LEADERS NETWORK                                      
                            </span>
                        </div>
                        <h1 class="txt-3em txt-bold padding-b-20">
                            Make Tomorrow Better
                        </h1>
                        <h2 class="txt-color-light txt-height-1-7">
                            Join a nationwide network of passionate young people to identify, develop and deploy creative solutions to development challenges
                        </h2>
                        <div class="padding-t-40">
                            <a class="btn btn-green txt-normal-s" data-toggle="modal" href="#championProgramFormModal">Volunteer</a>
                            <a class="btn btn-white txt-normal-s" data-toggle="modal" href="#aboutIdeasModal">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="bg-green padding-t-80 padding-b-80">
            <div class="container-wrapper text-center">
                <h2 class="txt-2em uppercase txt-color-white padding-b-20">
                    What are you passionate about?
                </h2>
                <p class=" txt-color-white">Explore available Programs</p>
            </div>
        </section>
        <section class="ynl-home-small-bg padding-b-40">
            <div class="container-wrapper">
                <nav class="padding-b-40">
                    <ul class="ynl-home-category-filter">
                        <?php 
                            $filter_term = $_GET['category'];
                            
                            /* Tax Query Array */
                            if($filter_term){
                                $tax_query = array(
                                    array(
                                        'taxonomy' => 'thematic-category',
                                        'terms' => $filter_term,
                                    )
                                );
                            }
                        
                            /* Get terms */
                            $terms = get_terms( 'thematic-category', array('hide_empty' => false, 'parent' => 0) ); 
                            $counter = 1;
                        ?>
                        <li>
                            <a class="<?php echo ( empty( $filter_term ) ) ? 'active' : ''; ?>" href="<?php echo currenturl(true); ?>">All</a>
                        </li>
                        <?php foreach( $terms as $term ){ ?>
                            <?php if( $counter <= 5 ){ ?>
                            <li>
                                <a 
                                   href="<?php echo currenturl(true).'?category='.$term->term_id; ?>" 
                                   class="<?php echo ( $filter_term == $term->term_id ) ? 'active' : ''; ?>"
                                >
                                    <?php echo $term->name ?>
                                </a>
                            </li>
                            <?php } ?>
                            <?php $counter++; ?>
                        <?php } ?>
                        <li class="dropdown show">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMoreCategoriesLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                More
                            </a>
                            <div class="dropdown-menu padding-tb-15" aria-labelledby="dropdownMoreCategoriesLink">
                            <?php foreach( $terms as $term ){ ?>
                                <a class="dropdown-item padding-tb-10" href="<?php echo currenturl(true).'?category='.$term->term_id; ?>">
                                    <span class="txt-color-green txt-light"><?php echo $term->name ?></span>
                                </a>
                            <?php } ?>
                            </div>
                        </li>
                    </ul>
                </nav>
                
                <!--<div>   
                    <div class="txt-normal-s txt-medium txt-color-white padding-o-15 border-0-1 border-color-white-trans">
                       Showing
                        <?php 
                            $current_term = get_term($filter_term);
                            echo ( $filter_term ) ? $current_term->name : 'All';
                        ?>
                    </div>
                </div>-->
                
                <?php  ?>
                <div class="row row-15">
                <?php
                    $custom_query = new WP_Query();
                    $custom_query->query( 
                        array(
                            'post_type' => 'thematic-area',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'tax_query' => $tax_query
                        ) 
                    );

                    if ( $custom_query->have_posts() ) {

                        while ($custom_query->have_posts()) : $custom_query->the_post();

                        /* Variables */
                        $post_id = $post->ID;    //Get Program ID

                        $images = rwmb_meta( 'featured_image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                ?>

                    <div class="col-md-6 col-lg-4 col-xl-3 padding-lr-15 padding-b-30 d-flex">
                        <div class="ynl-home-card col p-0" style="overflow:visible">
                            <article class="content d-flex flex-column">
                                <figure class="image-box" style="background-image: url('<?php echo $image['full_url']; ?>')">
                                    <div class="category txt-xxs dropdown show">
                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMoreCategoriesLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php 
                                            $term_list = wp_get_post_terms( $post->ID, 'thematic-category', array( 'fields' => 'names' ) );
                                            $counter = 1;
                                            foreach ( $term_list as $term_item ){
                                                if( $counter < 2 ){
                                                    echo $term_item;
                                                }

                                                $counter++;
                                            } 
                                            echo '  ';
                                            echo '  +';
                                            echo count($term_list) - 1; 
                                        ?>
                                        </a>
                                        <div class="dropdown-menu padding-tb-15 txt-normal-s" aria-labelledby="dropdownMoreCategoriesLink">
                                        <?php $counter = 1; ?>
                                        <?php foreach( $term_list as $term_item ){ ?>
                                            <?php if( $counter > 1 ){ ?>
                                                <a class="dropdown-item padding-tb-10" href="">
                                                    <span class="txt-color-green"><?php echo $term_item; ?></span>
                                                </a>
                                            <?php } ?>
                                            <?php $counter++ ?>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </figure>
                                <div class="course-info flex_1 d-flex flex-column justify-content-between">
                                    <h3 class="title txt-height-1-2 padding-b-40 <?php echo ( rwmb_meta( 'status' ) == 'Published' ) ? '' : 'txt-color-lighter' ?>">
                                        <a 
                                           class="" 
                                           <?php if( rwmb_meta( 'status' ) == 'Published' ){ ?>
                                           href="<?php the_permalink() ?>"
                                           <?php } ?>
                                        >
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                    <div class="row row-10">
                                    <?php if( rwmb_meta( 'status' ) == 'Published' ){ ?>
                                        <div class="col-4 padding-lr-10">
                                            <p class="txt-sm txt-color-lighter padding-b-5">Challenges</p>
                                            <p class="txt-lg txt-color-green">
                                            <?php 
                                                $challenges_query = new WP_Query( array(
                                                    'post_type' => 'problem-area',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                        array(
                                                            'key' => 'parent_post',
                                                            'value' => $post_id,
                                                        ),
                                                    ),
                                                ) );    

                                                echo $challenges_query->found_posts;
                                            ?>
                                            </p>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <p class="txt-sm txt-color-lighter padding-b-5">Ideas</p>
                                            <p class="txt-lg txt-color-green">
                                            <?php 
                                                $idea_query = new WP_Query( array(
                                                    'post_type' => 'idea',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                        array(
                                                            'key' => 'thematic_area',
                                                            'value' => $post_id,
                                                        ),
                                                    ),
                                                ) );   

                                                echo $idea_query->found_posts;
                                            ?>
                                            </p>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <p class="txt-sm txt-color-lighter padding-b-5">Live Projects</p>
                                            <p class="txt-lg txt-color-green">0</p>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-6 padding-lr-10 padding-b-5 txt-normal-s txt-medium txt-color-lighter">
                                            Coming soon
                                        </div>
                                        <!--<div class="col-6 text-right padding-lr-10 padding-b-5 txt-normal-s txt-medium n">
                                            <a class="txt-color-gree" data-toggle="modal" href="#iAmInterestedModal-<?php echo $post_id ?>">
                                                <u>I am interested</u>
                                            </a>
                                        </div>-->
                                    <?php } ?>
                                    </div>
                                </div>
                            </article>  
                        </div>   
                    </div>

                <?php endwhile; ?>

                <?php }else{ ?>
                    <div class="col-12 padding-lr-15 padding-t-20 padding-b-40">
                        <h2 class="txt-lg txt-medium txt-color-white text-center">
                            No Projects available.
                        </h2>
                    </div>   
                <?php } ?>
                </div>
            </div>
        </section>
        
        
        <?php


            if ( $custom_query->have_posts() ) {

                while ($custom_query->have_posts()) : $custom_query->the_post();

                /* Variables */
                $post_id = $post->ID;    //Get Program ID

                $images = rwmb_meta( 'featured_image', array( 'limit' => 1 ) );
                $image = reset( $images );
        ?>
        
        <!-- Interest Modal -->
        <div class="modal fade font-main filter-modal" id="iAmInterestedModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-interested-in-idea-'.$idea_id; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel">Interested in <?php echo the_title() ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            <?php
                                if( $_POST && $_GET['view'] == 'form-interested-in-idea-'.$idea_id ){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                    $email = sanitize_text_field( $_POST['email'] ); 
                                    $location = sanitize_text_field( $_POST['location'] ); 

                                    /* Update Mailing List */
                                    /* Data */
                                    switch_to_blog(110);
                                    $state = get_term( $location );
                                    restore_current_blog();

                                    $api_key = '0fkxtL9crlJFX7cqCXUV';
                                    $master_list = 'CoLqq7CTYUZYKBnJx5u7633g'; //Master

                                    /* Endpoints */
                                    $subscribe_url = 'http://maylbox.com/subscribe';

                                    /* Subscribe Body */
                                    $body = array(
                                        'name' => $post_name,
                                        'email' => $email,
                                        'Phone' => $telephone,
                                        'Location' => $state->name,
                                        'ThematicArea' => get_the_title(),
                                        'list' => $master_list,
                                        'boolean' => 'true'
                                    );

                                    $args = array(
                                        'body' => $body,
                                        'timeout' => '5',
                                        'redirection' => '5',
                                        'httpversion' => '1.0',
                                        'blocking' => true,
                                        'headers' => array(),
                                        'cookies' => array()
                                    );

                                    $response = wp_remote_post( $subscribe_url, $args );
                                    $http_code = wp_remote_retrieve_response_code( $response );
                                    $body = wp_remote_retrieve_body( $response );
                                    /* Update Mailing List End */

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                }
                            ?>


                            <div class="form margin-b-40">
                                <div class="row row-10">
                                    <!-- Name -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="post_name">Name</label>
                                        <input 
                                            type="text" 
                                            name="post_name" 
                                        >
                                    </div>
                                    <!-- Telephone -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="telephone">Telephone</label>
                                        <input 
                                            type="telephone" 
                                            name="telephone" 
                                        >
                                    </div>
                                    <!-- Email -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="email">Email</label>
                                        <input 
                                            type="email" 
                                            name="email" 
                                        >
                                    </div>
                                    <!-- Location -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="location">Location</label>
                                        <select name="location" id="location">
                                        <?php 
                                            switch_to_blog(110);
                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                            restore_current_blog();

                                            foreach( $terms as $term ){
                                        ?>
                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-green txt-sm padding-lr-15">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <?php endwhile; ?>
        
         <?php } ?>
        
        
        
        <section class="container-wrapper padding-t-40 padding-t-30">
            <header class="txt-center margin-b-20">
                <h2 class="txt-2em uppercase text-center">Different Ways to support</h2>    
            </header>
        </section>
        <section class="bg-ash txt-color-white padding-tb-40">
            <div class="container-wrapper">
                <div class="row row-20">
                    <div class="col-lg-3 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/submit idea.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Submit or Refine an Idea</h2> 
                        <p class="txt-sm">Explore highlighted challenges and recommend solution ideas that can be implemented nationwide, or review submitted ideas and share your thoughts on how to make them more impactful.</p>  
                    </div>
                    <div class="col-lg-3 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/submit an idea.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Champion an Idea</h2> 
                        <p class="txt-sm">Idea Champions believe in the effectiveness of a listed solution idea and are willing to actively participate in making it a reality. Enrol as a champion for any idea that connects to you, and we will reach out to you first, if we decide to convert that idea into a project.</p>  
                    </div>
                    <div class="col-lg-3 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/participate in a project.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Participate in a project</h2>   
                        <p class="txt-sm">Volunteer to work with our expert partners to develop an idea into a project or get involved with executing fully developed projects in your immediate locality.</p> 
                    </div>
                    <div class="col-lg-3 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Support a project.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Support a Project</h2>  
                        <p class="txt-sm">You can support the implementation of a project an an individual, Government, Private sector company, NGO or development agency by providing expertise, donating resources or funding to make change happen at the grass roots, nationwide.</p>  
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Learn more -->
        <div class="modal fade font-main filter-modal" id="aboutIdeasModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-tasks'; ?>" method="post">
                        <div class="modal-header padding-lr-40">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-40">
                            <div class="row">
                                <div class="col-lg-10 mx-auto">                                
                                   <h3 class="txt-xxlg txt-bold txt-height-1-1 txt-color-green uppercase padding-b-60">
                                       YOUNG PEOPLE COMING TOGETHER TO CREATE IMPACT AT SCALE
                                   </h3>
                                   <figure class="d-flex align-items-center justify-content-between txt-2em padding-b-60">
                                        <div class="">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Idea.png" alt="" width="160">
                                        </div>
                                        <i class="fa fa-long-arrow-right txt-color-orange padding-lr-10"></i>
                                        <div class="">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Project.png" alt="" width="160">
                                        </div>
                                        <i class="fa fa-long-arrow-right txt-color-orange padding-lr-10"></i>
                                        <div class="">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Impact.png" alt="" width="160">
                                        </div>
                                    </figure>
                                    <h5 class="txt-xlg txt-height-1-4 padding-b-60">
                                        The young leaders network is a nationwide group of passionate young people working together to identify, develop and deploy creative solutions to development challenges.
                                    </h5>

                                    <article class="padding-b-40">
                                        <h5 class="txt-lg txt-bold txt-height-1-4 padding-b-20">
                                            How we create impact
                                        </h5>
                                        <p class="txt-normal-s txt-height-1-7">For each program focus area, we crowdsource ideas from passionate young leaders, bring them together in teams -  to convert these ideas into high impact projects, and then enlist a nationwide network of young grassroots leaders to implement these projects in their localities.</p>
                                    </article>

                                    <div class="padding-b-40">
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-1" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#00752F"
                                                >
                                                    <span class="txt-bold txt-color-dark">01: Explore areas for action</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-1" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>Our program officers work with development agencies to prioritise areas for nationwide intervention. (e.g violence against women & girls.)  You will find a wide variety of program focus areas on the Young Leaders Network  home page.</p>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-2" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#029E41"
                                                >
                                                    <span class="txt-bold txt-color-dark">02: Discover challenges you are passionate about solving</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-2" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>From the list of programs, select any one you are passionate about and learn about the key issues we are looking to tackle under that program area.</p>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-3" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#00BC4C"
                                                >
                                                    <span class="txt-bold txt-color-dark">03: Participate in crafting a solution.</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-3" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>As a young leader, you can participate in different ways </p>
                                                        <ul>
                                                            <li>Submit an Idea - any idea - which you think has the potential to solve the challenges highlighted.</li>
                                                            <li>Explore existing ideas and add your voice: Have a suggestion? Have a modified version of this idea? Or do you have experience doing something like this that you would love to share?  Your views are important in helping us determine whether or not we should convert this idea into a project. Follow the link to add your voice.</li>
                                                            <li>Become a champion for ideas you connect with: Idea Champions believe in the effectiveness of an idea and are willing to actively participate in making it a reality. If you find an idea that you connect with, all you have to do is submit your name as an idea champion and we will reach out to you first, if/when we decide to convert this idea into a project. </li>
                                                        </ul>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-4" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#00E65D"
                                                >
                                                    <span class="txt-bold txt-color-dark">04: Support & Implement projects in your community</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-4" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>We will select ideas out of the pool of submitted ideas and convert into projects which can be implemented in your local community. Here are 3 ways you can get involved in making impact at the grass root.</p>
                                                        <ul>
                                                            <li><strong>Implement the project in your community.</strong> As much as possible, we will do our best to provided you with all the tools and support to deploy the solution where you are and provide feedback to the project coordinators.</li>
                                                            <li><strong>Donate a resource:</strong> Sometimes, we might need resources like laptops, etc to implement a project in a community. If you are in such a community and have such a resource, you can support implementation by connecting with local champions and supporting them with these resources</li>
                                                            <li>You can also donate funds to the implementing team, if that is what you have.</li>
                                                        </ul>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <article class="padding-b-40">
                                        <h5 class="txt-lg txt-bold txt-height-1-4 padding-b-20">
                                            Whats in it for you?
                                        </h5>
                                        <article class="text-box txt-normal-s">
                                            <p>Participating in developing and implementing a project comes with many perks. Some example are:</p>
                                            <ul>
                                                <li>Use your experience on these projects as work experience on your CV</li>
                                                <li>Learn a lot of new skills from peers and coordinating partners</li>
                                                <li>Build long lasting valuable network of friends and influencers</li>
                                                <li>Be recommended for Jobs with our partners and other corporates based on your performance on a project.</li>
                                                <li>Enjoy the feeling of fulfilment that comes with contributing to making the world a better place.</li>
                                            </ul>
                                        </article>
                                    </article>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-green txt-xs" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Champion form -->
        <div class="modal fade font-main filter-modal" id="championProgramFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-champion-program'; ?>" method="post">
                        <?php
                            if( $_POST && $_GET['view'] == 'form-champion-program' ){

                                /* Get Post Name */
                                $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                /* Meta */
                                $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                $email = sanitize_text_field( $_POST['email'] ); 
                                $location = sanitize_text_field( $_POST['location'] ); 
                                $implementation = sanitize_text_field( $_POST['implementation'] ); 
                                $educational_status = sanitize_text_field( $_POST['educational_status'] ); 
                                $contribution = wp_kses_post( $_POST['contribution'] );

                                /* Save Post to DB */
                                $champion_id = wp_insert_post(array (
                                    'post_type' => 'idea-champion',
                                    'post_title' => $post_name,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                update_post_meta( $champion_id, 'thematic_area', $post_id );
                                //update_post_meta( $champion_id, 'problem_area', $post_id );

                                update_post_meta( $champion_id, 'telephone', $telephone );
                                update_post_meta( $champion_id, 'email', $email );
                                update_post_meta( $champion_id, 'location', $location );
                                update_post_meta( $champion_id, 'implementation', $implementation );
                                update_post_meta( $champion_id, 'educational_status', $educational_status );
                                update_post_meta( $champion_id, 'contribution', $contribution );

                                /* Update Mailing List */
                                /* Data */
                                switch_to_blog(110);
                                $state = get_term( $location );
                                restore_current_blog();

                                $api_key = '0fkxtL9crlJFX7cqCXUV';
                                $master_list = 'YdUPrXEfWxjULk9AsCRbPQ'; //Master

                                /* Endpoints */
                                $subscribe_url = 'http://maylbox.com/subscribe';

                                /* Subscribe Body */
                                $body = array(
                                    'name' => $post_name,
                                    'email' => $email,
                                    'Phone' => $telephone,
                                    'Location' => $state->name,
                                    'PreviousImplementation' => $implementation,
                                    'EducationalStatus' => $educational_status,
                                    'contribution' => $contribution,
                                    'ThematicArea' => $post_title,
                                    'list' => $master_list,
                                    'boolean' => 'true'
                                );

                                $args = array(
                                    'body' => $body,
                                    'timeout' => '5',
                                    'redirection' => '5',
                                    'httpversion' => '1.0',
                                    'blocking' => true,
                                    'headers' => array(),
                                    'cookies' => array()
                                );

                                $response = wp_remote_post( $subscribe_url, $args );
                                $http_code = wp_remote_retrieve_response_code( $response );
                                $body = wp_remote_retrieve_body( $response );
                                /* Update Mailing List End */

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                            }
                        ?>
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            
                            <h3 class="txt-xxlg txt-light padding-b-30">
                                Join the Young Leaders Network as a Volunteer
                            </h3>
                            
                            <div class="margin-b-30 padding-tb-15">
                                <p class="txt-sm">Program volunteers recognizes the urgency of a program area and are willing to actively participate to make things better in that area. If you want to work with other young leaders to <span class="text-lowercase"><?php echo $post_title ?></span> , please submit your name as a program volunteer and we will reach out to you whenever there are new activities and events under this program.</p>
                            </div>

                            <div class="form margin-b-40">
                                <div class="row row-10">
                                    <!-- Name -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="post_name">Your Name</label>
                                        <input 
                                            type="text" 
                                            name="post_name" 
                                        >
                                    </div>
                                    <!-- Telephone -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="telephone">Your Telephone</label>
                                        <input 
                                            type="telephone" 
                                            name="telephone" 
                                        >
                                    </div>
                                    <!-- Email -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="email">Your Email Address</label>
                                        <input 
                                            type="email" 
                                            name="email" 
                                        >
                                    </div>
                                    <!-- Location -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="location">Where do you live?</label>
                                        <select name="location" id="location">
                                        <?php 
                                            switch_to_blog(110);
                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                            restore_current_blog();

                                            foreach( $terms as $term ){
                                        ?>
                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                    <!-- Age Bracket -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="age_bracket">What is your age bracket?</label>
                                        <select name="age_bracket" id="age_bracket">
                                            <option value="less than 18">less than 18</option>
                                            <option value="19-25">19-25</option>
                                            <option value="26-34">26-34</option>
                                        </select>
                                    </div>
                                    <!-- Gender -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="gender">Gender</label>
                                        <select name="gender" id="gender">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                    <!-- Disabilty -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="disability_status">Are you disabled in anyway?</label>
                                        <select name="disability_status" id="disability_status">
                                            <option value="No">No</option>
                                            <option value="Yes">Yes</option>
                                        </select>
                                    </div>
                                    <!-- Challenge Areas -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="challenge_areas">Select challenge areas you are interested in (for volunteerism)</label>
                                        <select name="challenge_areas" id="challenge_areas">
                                            <option value="Education">Education</option>
                                            <option value="Employment & Entrepreneurship">Employment & Entrepreneurship</option>
                                            <option value="Social Justice">Social Justice</option>
                                            <option value="Health">Health</option>
                                            <option value="Security">Security</option>
                                        </select>
                                    </div>
                                    <!-- Implementation -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="implementation">Have you implemented any program anywhere before?</label>
                                        <select name="implementation" id="implementation">
                                            <option value="No, But i believe it will work">No</option>
                                            <option value="I have started working on it, but haven't launched">Yes, I have led the implementation of this program in the past </option>
                                            <option value="I have implemented the idea but at early stages">Yes, I have been part of a team that Implemented this program</option>
                                        </select>
                                    </div>
                                    <!-- Educational Status -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="educational_status">Educational status</label>
                                        <select name="educational_status" id="educational_status">
                                            <option value="Secondary School Student">Secondary School Student</option>
                                            <option value="Undergraduate">Undergraduate</option>
                                            <option value="Graduate (Pre-NYSC)">Graduate (Pre-NYSC)</option>
                                            <option value="NYSC Corps Member">NYSC Corps Member</option>
                                            <option value="Graduate (Post NYSC)">Graduate (Post NYSC)</option>
                                            <option value="Professional">Professional</option>
                                        </select>
                                    </div>
                                    <!-- Contribution -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="contribution">How can you contribute to this program</label>
                                        <textarea name="contribution" rows="8"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" id="" class="btn btn-green txt-sm padding-lr-15">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
   
    <?php get_footer() ?>
