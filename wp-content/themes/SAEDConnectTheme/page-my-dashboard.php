<?php /*Template Name: User Dashboard*/ ?>
  
    <?php
        /* Get current User ID */
        $current_user = wp_get_current_user();   

        if ( $current_user->ID == 7 ) {
            echo "NYSC";
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = 'https://www.saedconnect.org/nysc-dashboard/'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>

   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    ?>
    
    <?php
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';
   
        /* Get User */
        $current_user = wp_get_current_user();

        /* Check if User has only mode role */
        $onlymod = true;

        /* Check if User has only Moderator role */
        /*foreach( $current_user->roles as $role ){
            if( $role == 'administrator' || $role == 'subscriber'  || $role == 'bbp_participant' ){
                 User is not only mode 
                $onlymod = false;
                //echo '<h1 class="txt-2em padding-t-80">Role: '.$role.'</h1>';
            }
        }*/

        /*if($onlymod){
            
            get_template_part( 'template-parts/user-dashboard/moderator-dashboard' );
            
        } else {*/

            /* If User has */
            /*if($page_type){

                get_template_part( 'template-parts/user-dashboard/cv' );
                get_template_part( 'template-parts/user-dashboard/'.$page_type );

            } else {

                //get_template_part( 'template-parts/user-dashboard/entrepreneurship-dashboard' );

            }*/

            get_template_part( 'template-parts/user-dashboard/dashboard_header' );
            get_template_part( 'template-parts/user-dashboard/cv' );

    ?>

    
<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>