<?php /*Template Name: Password Reset Form*/ ?>

<?php get_header() ?>

<?php
    // TO SHOW THE PAGE CONTENTS
    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
?>
    <style>
        label{
            display: block;
            margin-bottom: 10px;
        }
        
        input {
            display: inline-block !important;
            margin-bottom: 20px;
        }
    </style>
    <main class="main-content">
        
        <section class="container-wrapper padding-t-80 padding-b-40 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        <?php the_title() ?>
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        <p>
                            
                        </p>
                    </h2>
                </div>
            </div>
        </section>
        
        <section class="container-wrapper padding-b-40" id="get-started">
            <div class="row">
                <div class="col-md-8 mx-auto text-center">
                    <?php echo the_content() ?>
                </div>
            </div>
        </section>

    </main>
    
<?php
    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
?>

<?php get_footer() ?>
