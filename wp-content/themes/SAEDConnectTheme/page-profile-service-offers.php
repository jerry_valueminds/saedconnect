<?php /*Template Name: Profile - Service Offers*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Publication */
        $publication_key   = 'publication_status';

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                <?php if( $_REQUEST['gf-id'] ){ //GF View ?>
                   
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                    
                <?php } else { //Offer Configuration ?>
                
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post">
                            <?php
                                /* Meta Key */
                                $postType = 'offer-a-service';
                                $tax_types = array(
                                    array(
                                        'name' => 'Offer Category',
                                        'slug' => 'service-category',
                                        'hierachical' => true,
                                    ),
                                    array(
                                        'name' => 'Locations where this offer is available',
                                        'slug' => 'nigerian-state',
                                        'hierachical' => false,
                                    ),
                                );
                                $redirect_link = currentUrl(true);
                              
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    $post = get_post($post_id);
                                    $postName = $post->post_title;
                                    
                                    $fee = get_post_meta( $post->ID, 'fee', true );
                                    
                                    /* Delete & Return */
                                    if($_GET['action'] == 'delete'){
                                        /* Delete Post */
                                        wp_delete_post($post_id);

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                    
                                    /* Publish / Unpublish & Return */
                                    if($_GET['action'] == 'publication'){
                                        /* Meta value to save */
                                        $value = "user_published";
                                        
                                        /* Get saved meta */
                                        $saved_meta = get_post_meta( $post_id, $publication_key, true );

                                        if ( $saved_meta ) //If published, Unpublish
                                            delete_post_meta( $post_id, $publication_key );
                                        else //If Unpublished, Publish
                                            update_post_meta( $post_id, $publication_key, $value );

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                }
                              
                                /* Returns All Term Items to populate Form */
                                $term_list = wp_get_post_terms($post->ID, $tax_type, array("fields" => "ids"));                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if($_POST){
                                    
                                    /* Get Post Name */
                                    $postName = sanitize_text_field( $_POST['post-name'] );
                                    $fee = sanitize_text_field( $_POST['fee'] );
                                    
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $post_id, 'fee', $fee );

                                    /* Save terms to post */
                                    foreach($tax_types as $tax_type){
                                        wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug']);
                                    }
                                    
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }
                            ?>
                            
                            <div class="margin-b-40">
                                <label for="post-name" class="d-block txt-normal-s txt-color-dark txt-medium margin-b-10">
                                    Offer Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post-name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $postName ?>"
                                >
                            </div>

                            <!-- Terms -->
                            <div class="txt-normal-s">
                            <?php foreach($tax_types as $tax_type){ ?>
                            
                                <?php if( $tax_type['hierachical']){ ?>
                            
                                    <!-- Tax Title -->
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        <?php echo $tax_type['name']; ?>
                                    </h2>

                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <?php if( $parent == 0 ){ ?>

                                            <div class="padding-b-20">
                                                <div class="txt-medium txt-color-dark margin-b-15">
                                                    <?php echo $term_name; ?>
                                                </div>
                                                <?php
                                                    foreach ($terms as $child_term) {
                                                        // Check and see if the term is a top-level parent. If so, display it.
                                                        $child_parent = $child_term->parent;
                                                        $child_term_id = $child_term->term_id; //Get the term ID
                                                        $child_term_name = $child_term->name; //Get the term name

                                                        if( $child_parent == $term_id ){
                                                ?>
                                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                            <input
                                                                class="margin-r-5"
                                                                type="checkbox" 
                                                                value="<?php echo $child_term_id ?>" 
                                                                name="<?php echo $tax_type['slug'] ?>[]" 
                                                                <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                            >
                                                            <span class="bg-label">
                                                                <?php echo $child_term_name; ?>
                                                            </span>
                                                        </label>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </div>

                                        <?php } ?>

                                    <?php } ?>
                                
                                <?php } else { ?>
                                
                                    <!-- Tax Title -->
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        <?php echo $tax_type['name']; ?>
                                    </h2>
                                    
                                    <div class="padding-b-20">
                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                            <input
                                                class="margin-r-5"
                                                type="checkbox" 
                                                value="<?php echo $term_id ?>" 
                                                name="<?php echo $tax_type['slug'] ?>[]" 
                                                <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                            >
                                            <span class="bg-label">
                                                <?php echo $term_name; ?>
                                            </span>
                                        </label>

                                    <?php } ?>
                                    </div>
                                <?php } ?>
                                
                            <?php } ?>
                            </div>
                            
                            <div class="margin-b-40">
                                <label for="post-name" class="d-block txt-normal-s txt-color-dark txt-medium margin-b-10">
                                    Minimum Fee
                                </label>
                                <input 
                                    type="number" 
                                    name="fee" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $fee ?>"
                                >
                            </div>
                            
                            <div class="text-right">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>
                
                <?php } ?>  
                </div>
            
            <?php } elseif($_GET['view'] == 'interactions'){ ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        Service Offers I have engaged
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Service Offers I have engaged.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Service Offers
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Service Offers I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="row row-15">
                <?php
                    /* GF Search Criteria */
                    $gf_id = 9;
                    $parent_post_id = 2;
                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                    foreach( $entries as $entry ){

                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $parent_post = get_post($parent_post_id);
                ?>
                    <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                        <div class="flex_1 padding-o-30 border-o-1 border-color-darkgrey">
                            <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                <?php echo rgar( $entry, '1' ) ?>
                            </h3>
                            <div class="txt-normal-s margin-b-30">
                                <p>
                                    <?php echo rgar( $entry, '3' ) ?>
                                </p>
                            </div>
                            <h3 class="txt-sm txt-color-dark txt-medium">
                                For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                            </h3>
                        </div>
                    </div>
                <?php } ?>
                </div>
               
            <?php } else { ?>
                <div class="page-header">
                    <h1 class="page-title">
                        My Service Offers
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form-title=Create a Service Offer" class="cta-btn">
                            Add Offer
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        These are the offers you made in the Service Marketplace.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Service Offers
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Service Offers I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <ul class="row row-10 marketplace-service-cards">
                    <style>
                        .service-img{
                            position: relative;
                        }
                        .publication-status{
                            position: absolute;
                            display: flex;
                            align-items: center;
                            top: 0;
                            right: 0;
                            padding: 10px;
                            color: black;
                            background-color: gainsboro;
                        }
                        
                        .user_published{
                           background-color: #f4c026; 
                        }
                        
                        .admin_published{
                           background-color: #00bfe7; 
                        }
                    </style>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */
                    // Define Tax Query
                    $meta_array = array('relation' => 'AND');

                    // Add Arguements from _REQUEST
                    foreach($_GET as $key => $value){

                        /* Check & Exclude:
                                - S ( WP Search field )
                                - Search-type ( For selecting Search Template View to use )
                                - Empty values
                        */
                        if($key !== 's' && $key !== 'search-type' && $key !== 'view' && !empty($value) && $value != 'all'){

                            // Create Tax Array entry
                            $category_array = array(

                                // Add Term Term Values
                                array (
                                    'key' => trim($key), //Texanomy Type
                                    'value' => $value, //Search field
                                ),

                            );

                            // Add just created Array to Tax Array. 
                            $meta_array = array_merge($meta_array, $category_array); 
                        }
                    }

                    // Create Query Argument
                    $args = array(
                        'post_type' => 'offer-a-service',
                        'showposts' => -1,
                        'author' => $current_user->ID,
                        'meta_query' => $meta_array,
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                          
                    /* Publication status */      
                    $publication_meta = get_post_meta( $post_id, $publication_key, true );
                ?>
                    
                    <li class="col-sm-6 col-lg-3 padding-o-10">
                        <span class="position-relative">
                        <?php
                          
                            /* Twitter */
                            $field = 'offer_image';

                            $meta = get_post_meta($post->ID, $field, true);

                            if($meta){
                                $meta = str_ireplace( 'http:', 'https:', $meta );
                        ?>

                            <figure class="service-img" style="background-image:url('<?php echo $meta; ?>');">

                            </figure>
                        <?php } else{ ?>
                            <figure class="service-img" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/table.jpg');">
                                
                            </figure>
                        <?php } ?>
                            <div class="content">
                                <div class="top">
                                    <div class="title">
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <div class="location">
                                        <?php 
                                            $term_list = wp_get_post_terms($post_id, 'nigerian-state', array("fields" => "names"));

                                            if( $term_list ){
                                                foreach( $term_list as $key => $term_name ){
                                                    echo $term_name;
                                                    echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                                }
                                            } else {
                                                echo "Locations are unvailable";
                                            }
                                        ?>
                                    </div>
                                    <!--<div class="post-fee">
                                        Startting at
                                        <span class="txt-color-dark txt-bold">
                                         }
                                        ?>
                                        </span>
                                    </div>-->
                                </div>
                                
                                <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                    <div class="publication-status <?php echo $publication_meta ?>">
                                        <?php
                                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                            if($publication_meta == 'user_published'){
                                                echo 'Undergoing review';
                                            } elseif($publication_meta == 'admin_published') {
                                                echo 'Published';
                                            }else{
                                                echo 'Unpublish';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </li>
                <?php
                    endwhile;
                ?>
                </ul>
                
            <?php } ?> 
                
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>