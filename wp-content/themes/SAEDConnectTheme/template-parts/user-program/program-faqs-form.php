<!-- FAQs Form -->

<h1 class="txt-2em txt-medium margin-b-20">
    Submit - FAQs
</h1>
<section class="margin-b-20">
    <?php echo do_shortcode('[gravityform id="42" title="false" description="false"]'); ?>
</section>