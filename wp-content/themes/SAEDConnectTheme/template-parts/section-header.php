        <!-- Section Header -->
        <header class="overview-header container-wrapper">
            <!-- Get Topic Area Type: This is used to determine the redirect link -->
            <?php $topic_area_type = rwmb_get_value( 'topic-area-type' ); ?>

            <!-- Determine Relationship value based on Topic Area Type -->          
            <?php
                $topic_area_relationship_id;

                switch($topic_area_type){
                    case 'guide':
                        $topic_area_relationship_id = 'topic_area_to_article';
                        break;
                    case 'advice':
                        $topic_area_relationship_id = 'topic_area_to_relate_advice';
                        break;
                    case 'faq':
                        $topic_area_relationship_id = 'topic_area_to_faq_article';
                        break;
                }
            ?>
           
            <?php
                /* Get Content for this view */
                /* Variables to store data */
                $sectionName;
                $section_id;

                $topicName;
                $sponsorName;
                $sponsorImage;

                $topicArea;
                $articleTitle = get_the_title();
            ?>

            <?php
                /* First Query to get Topic Area Name */
                $topicAreaQuery = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'topic_area_to_faq_article',
                        'to' => get_the_ID(), // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $topicAreaQuery->have_posts() ) : $topicAreaQuery->the_post(); ?>
                    <?php $topicArea = get_the_title(); ?>

                    <?php 
                        /* Second Query to get Topic Name */
                        $topicNameQuery = new WP_Query( array(
                            'relationship' => array(
                                'id'   => 'topic_to_topic_area',
                                'to' => get_the_ID(), // You can pass object ID or full object
                            ),
                            'nopaging' => true,
                        ) );
                        while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post(); ?>   
                            <?php $topicName = rwmb_get_value( 'topic-page-name' ); ?>
                            <?php $sponsorName = rwmb_get_value( 'sponsor-name' ); ?>
                            <?php
                                $images = rwmb_meta( 'sponsor-image', array( 'limit' => 1 ) );
                                $image = reset( $images );
                                $sponsorImage = $image['full_url'];
                            ?>

                            <?php 
                            /* Third Query to get Section Name */
                            $sectionNameQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'section_to_topic',
                                    'to' => get_the_ID(), // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $sectionNameQuery->have_posts() ) : $sectionNameQuery->the_post(); ?>   
                                <?php 
                                    $sectionName = get_the_title();
                                    $section_id = get_the_ID();
                                ?>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* THird Query: END */
                        ?>

                    <?php
                        endwhile;
                        wp_reset_postdata();
                        /* Second Query: END */
                    ?>

            <?php
                endwhile;
                wp_reset_postdata();
                /* First Query: END */
            ?>
            <div class="info-box">
                <div class="info">
                    <h2 class="subtitle rounded-corners">
                        <?php echo $sectionName ?>
                    </h2>
                    <h1 class="title txt-color-lighter">
                        <?php echo $topicName ?>
                    </h1>
                </div>
                <div class="sponsor">
                    <div class="intro-text">
                        Content Sponsored by:
                    </div>
                    <img class="logo" src="<?php echo $sponsorImage ?>" alt="">
                </div>
            </div>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <?php // Display posts
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query(array('post_type' => 'topic-area'));
                            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                               
                                <li class="<?php echo ($topicArea == get_the_title()  ? 'active' : '') ?>">
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_title() ?><br>
                                        <?php echo $topicArea ?>
                                    </a>
                                </li>  

                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php wp_reset_query(); ?>
                </ul>
            </nav>
        </header>