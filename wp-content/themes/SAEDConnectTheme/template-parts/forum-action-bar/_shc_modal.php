<?php
    /* Get Action bar */
    $action_bar_var = get_query_var('action_bar_var');

    /* Theme color */
    $forum_theme = $action_bar_var['forum_theme'];
    $forum_theme_alt = $action_bar_var['forum_theme_alt'];
    
    /* Button Color */
    $button_bg = $action_bar_var['button_bg'];
    $button_color = $action_bar_var['button_color'];
?>
<section
   class="bg-yellow padding-tb-10"
   style="background-color:<?php echo $forum_theme ?>"
>
    <div class="container-wrapper">
        <h3 class="d-flex justify-content-between align-items-center">
            <span class="d-inline-block padding-r-15 txt-height-1-2 txt-medium" style="color:<?php echo $forum_theme_alt ?>">
                Get extra help & resources

                <?php
                    //echo $isVowel ? 'an ' : 'a ';     //Print 'a' or 'an'
                    //echo $title;    //Print Title
                ?>

            </span>
            <div>
                <div class="dropdown">
                    <button
                        class="btn btn-blue txt-sm no-m-b dropdown-toggle"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapseExample"
                        aria-expanded="false"
                        aria-controls="collapseExample"
                        style="background-color:<?php echo $button_bg ?>; color:<?php echo $button_color ?>; border-color:<?php echo $button_bg ?>;"
                    >
                        Learn more
                    </button>
                </div>
            </div>
        </h3>
        <div class="collapse" id="collapseExample">
            <div class="txt-normal-s">
                <!--<p class="padding-b-20">
                    <?php echo bbp_forum_content(); ?>
                </p>
                <p>
                    <button type="button" class="btn btn-blue txt-sm no-m-b" data-toggle="modal" data-target="#formModal">
                        Request
                    </button>
                </p>-->
                <style>
                    .icon-list li {
                        margin-bottom: 10px;
                    }

                    .icon-list.black li a {
                        text-decoration: underline;
                        color:<?php echo $forum_theme_alt ?>;
                    }
                </style>
                <ul class="icon-list black txt-normal-s padding-tb-30">
                    <li>
                        <a href="https://www.saedconnect.org/help-center/forums/topic/we-want-your-suggestions-what-can-we-do-to-help-you-start-your-small-business/" target="_blank">
                            <i class="fa fa-chevron-right"></i>
                            <span>
                                We want your suggestions: What can we do to help you start your
                                <?php
                                    echo $title;    //Print Title
                                ?>
                                business?
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.saedconnect.org/help-center/forums/topic/create-your-own-thread/" target="_blank">
                            <i class="fa fa-chevron-right"></i>
                            <span>
                                Tips to help you engage better in the
                                <?php
                                    echo $title;    //Print Title
                                ?>
                                Community
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="fa fa-chevron-right"></i>
                            <span>
                                Find
                                <?php
                                    //echo $isVowel ? 'an ' : 'a ';     //Print 'a' or 'an'
                                    echo $title;    //Print Title
                                ?>
                                trainers
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>