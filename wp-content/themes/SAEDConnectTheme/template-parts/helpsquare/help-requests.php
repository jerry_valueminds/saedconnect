    <section class="container-wrapper padding-tb-80">
        <h3 class="txt-lg txt-medium margin-b-40">
            Explore Other Requests
        </h3>
        <div class="row row-20 txt-height-1-4">
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="https://www.saedconnect.org/ventures-directory/funding-requests/">
                     Funding Requests
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="https://www.saedconnect.org/ventures-directory/mentor-requests/">
                     Mentorship Requests
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="https://www.saedconnect.org/ventures-directory/workspace-requests/">
                     Workspace/Office Space / Factory Space Requests
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="https://www.saedconnect.org/ventures-directory/tool-requests/">
                    Tool/equipment Requests
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="https://www.saedconnect.org/ventures-directory/land-requests/">
                     Land &amp; Landed Property Requests
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="https://www.saedconnect.org/ventures-directory/marketing-requests/">
                     Marketing &amp; Publicity Support Requests
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="https://www.saedconnect.org/ventures-directory/volunteer-requests/">
                    Volunteer on a project Requests
                </a>
            </div>
        </div>
    </section>