<!-- Education & Training Form -->

<section class="container-wrapper padding-t-80 padding-b-40 text-center text-center txt-light">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <h1 class="txt-2em txt-height-1-2 margin-b-20">
                Education & Training
            </h1>
            <h2 class="txt-lg txt-height-1-5">
                <p>

                </p>
            </h2>
        </div>
    </div>
</section>

<section class="container-wrapper bg-grey padding-t-60 padding-b-40" id="get-started">
    <div class="row">
        <div class="col-md-8 mx-auto esaed-form">
            <?php
                /* Get Form ID from request */
                $gf_id = $_GET['gf-id'];
            
                /* Get Form */
                echo do_shortcode('[gravityform id="'.$gf_id.'" title="false" description="false"]');
            ?>
        </div>
    </div>
</section>