<!-- Contact Details Form -->

<h1 class="txt-2em txt-medium margin-b-20">
    CV - Contact Details
</h1>
<section class="margin-b-20">
    <?php echo do_shortcode('[gravityform id="8" title="false" description="false"]'); ?>
</section>