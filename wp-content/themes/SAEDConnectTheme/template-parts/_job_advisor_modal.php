<?php
    switch_to_blog(20);
?>


<!-- SHC Modal -->
<div class="modal fade font-main shc-modal" id="comingSoonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="container-wrapper">
                <div class="col-md-10 mx-auto">
                    <div class="padding-tb-80">
                        <div class="margin-b-40">
                            <h2 class="txt-2em txt-bold txt-height-1-2">
                                What Job Advisor Community
                                <br>
                                are you interested in?
                            </h2>
                        </div>
                        <ul class="row row-20 shc-list txt-normal-s txt-medium">
                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="https://www.saedconnect.org/job-advisor-forum/forums/forum/cv-and-cover-letters/">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            Crafting your CV, Social Profiles & Cover Letters
                                        </span>
                                    </a>
                                </li>
                            </div>
                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="https://www.saedconnect.org/job-advisor-forum/forums/forum/job-search/">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            Searching for Great Job Openings
                                        </span>
                                    </a>
                                </li>
                            </div>
                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="https://www.saedconnect.org/job-advisor-forum/forums/forum/ace-the-interviews/">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            Preparing for, & Acing your interviews
                                        </span>
                                    </a>
                                </li>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
    //Revert to Previous Multisite
    restore_current_blog();
?>