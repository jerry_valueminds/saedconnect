<?php
    /* Move Action */
    $action = sanitize_text_field( $_GET['move'] );
    $user_id = sanitize_text_field( $_GET['user-id'] );

    if($action){
        /* Check Application Entry in DB */
        $application_entry = $application_db->get_row("SELECT ID, post_id, post_type FROM ".$table." WHERE user_id = ".$user_id." AND post_id = ".$post_id." LIMIT 0,1");

        if( $application_entry ){

            /* Change Applicant Status */
            $application_db->update( 
                $table, 
                array( 
                    "status" => $action,
                ), 
                array( 'ID' => $application_entry->ID ),
                array( "%s" ) 
            );
        }
    }