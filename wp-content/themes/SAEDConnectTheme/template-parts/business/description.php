<?php if( $current_user->ID != $post_author_id && $rendered_view != 'application-manager' && $rendered_view == '' ){ ?>
   
<section class="padding-t-20 padding-b-80">
    <div class="container-wrapper">
        <div class="row row-20">
            <div class="col-md-8 padding-lr-20">
                <!-- Outcome -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            What you will learn
                        </span>
                    </h1>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'outcome', false );
                            $outcome_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                                <div class="row row-10">

                            <?php foreach ($unserialized_data as $serialized_outcome){ ?>

                                <?php 
                                    $outcome = maybe_unserialize( $serialized_outcome );
                                    $outcome_counter++;
                                ?>
                                
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_amazing_support_network.png" alt="" width="50">
                                        </div>
                                        <div class="col padding-l-30">
                                            <p class="txt-sm">
                                                <?php echo $outcome['outcome'] ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                                </div>
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-sm txt-height-1-4">
                                    No Learning Outcomes available.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>

                <!-- Description -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Course Description
                        </span>
                    </h1>
                    <article class="text-box txt-sm txt-height-1-7 padding-o-15 border-o-1 border-color-darkgrey">
                        <?php 
                            $get_description = get_post_meta( $post_id, 'description', true );
                            echo ( $get_description ) ? $get_description : '<p>No description available.</p>'
                        ?>
                    </article>
                </div>
                
                <!-- Curriculum -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Curricula
                        </span>
                    </h1>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'curriculum', false );
                            $curriculum_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="">

                            <?php foreach ($unserialized_data as $serialized_curriculum){ ?>

                                <?php 
                                    $curriculum = maybe_unserialize( $serialized_curriculum );
                                    $curriculum_counter++;
                                ?>
                                
                                <div class="txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="row row-10">
                                        <div class="col-2 padding-lr-10 text-center">
                                            <p class="uppercase padding-b-10">
                                                <?php echo $curriculum['lesson-step'] ?>
                                            </p>
                                            <p class="txt-xxlg">
                                                <?php echo $curriculum['lesson-number'] ?>
                                            </p>
                                        </div>
                                        <div class="col-10 padding-lr-10">
                                            <p class="txt-medium txt-xlg padding-b-10">
                                                <?php echo $curriculum['lesson-title'] ?>
                                            </p>
                                            <p>
                                                <?php echo $curriculum['lesson-summary'] ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-sm txt-height-1-4">
                                    No curricula available.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                
                <!-- Skills -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Skills Learners will Gain
                        </span>
                    </h1>
                    <div class="padding-t-15 padding-lr-15 padding-b-5 border-o-1 border-color-darkgrey margin-b-20">
                        <?php 
                            $term_list = wp_get_post_terms($post_id, 'training-category', array("fields" => "names"));

                            if( $term_list ){
                                foreach( $term_list as $key => $term_name ){
                                    echo '<span class="btn btn-trans-bw txt-xs margin-r-5">'.$term_name.'</span>';
                                }
                            } else {
                                echo '<p class="txt-sm padding-b-10">Skills are unvailable</p>';
                            }
                        ?>
                    </div>
                </div>
                
                <!-- Location -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            This Course is Available in:
                        </span>
                    </h1>
                    <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                        <p>
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'state', array("fields" => "names"));

                                if( $term_list ){
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } else {
                                    echo "Locations are unvailable";
                                }
                            ?>
                        </p>
                    </div>
                </div>
                
                <!-- Fees -->
                <div class="margin-b-40">
                    <?php  
                        $paymentType = get_post_meta( $post_id, 'payment-type', true );
                        $paymentFee = get_post_meta( $post_id, 'payment-fee', true );
                        $unserialized_data = get_post_meta( $post_id, 'package', false );
                    ?>

                   
                    <div class="">
                        <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                            <span>
                                <span class="txt-normal-s txt-color-light txt-medium">
                                    Course Packages
                                </span>
                            </span>
                        </h1>
                    </div>
                    
                    <?php 
                        $unserialized_data = get_post_meta( $post_id, 'package', false );
                        $package_counter = 0;
                    ?>

                    <?php if( $unserialized_data ){ ?>

                        <?php foreach ($unserialized_data as $serialized_package){ ?>

                            <?php 
                                $package = maybe_unserialize( $serialized_package );
                                $package_counter++;
                            ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <p class="txt-normal-s">
                                    <span class="row">
                                        <span class="col-8">
                                            <span class="">
                                                <?php echo $package['payment-package-name'] ?>:
                                            </span>
                                            <span class="txt-bold">
                                                <?php echo ( $package['payment-package-type'] == 'fee' ) ? "₦".$package['payment-package-fee'] : "Free"; ?>
                                            </span>
                                            <span>
                                                <?php echo (  $package['installment-payment'] ) ? " (Installment Payment available)" : ""; ?>
                                            </span>
                                        </span>
                                        <span class="col-4 text-right">
                                            <a class="txt-color-blue" data-toggle="collapse" href="#package-<?php echo $package_counter; ?>" aria-expanded="false">
                                                View Details 
                                                <i class="fa fa-angle-down padding-l-5"></i>
                                            </a>
                                        </span>
                                    </span>
                                </p>
                                <div id="package-<?php echo $package_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                    <div class="row">
                                        <article class="text-box txt-sm txt-height-1-7 padding-t-20">
                                            <?php echo $package['payment-package-description'] ?>
                                        </article>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                    <?php }else{ ?>

                        <div class="padding-o-15 border-o-1 border-color-darkgrey">
                            <h3 class="txt-sm txt-height-1-4">
                                No Course Packages available.
                            </h3>
                        </div>

                    <?php } ?>        
                </div>
                
                <!-- Schedule -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Schedule
                        </span>
                    </h1>
                    <div class="margin-b-20">

                        <?php
                            $custom_query = new WP_Query();
                            $custom_query->query( 
                                array(
                                    'post_type' => 'schedule',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'parent',
                                            'value' => $post_id
                                        )
                                    ),
                                ) 
                            );

                            if ( $custom_query->have_posts() ) {

                                while ($custom_query->have_posts()) : $custom_query->the_post();

                                /* Variables */
                                $schedule_id = get_the_ID();    //Get Program ID
                        ?>


                                <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                    <p class="txt-normal-s">
                                        <span class="row">
                                            <span class="col-8 txt-bold">
                                                <?php $startDate = get_post_meta( $schedule_id, 'date', true ) ?>
                                                <?php 
                                                    if( $startDate ){
                                                        $date = strtotime( $startDate );
                                                        echo date('j F Y', $date);
                                                    }
                                                ?>
                                            </span>
                                            <span class="col-4 text-right">
                                                <a class="txt-color-blue" data-toggle="collapse" href="#scheduleContent-<?php echo $schedule_id; ?>" aria-expanded="false">
                                                    View Details 
                                                    <i class="fa fa-angle-down padding-l-5"></i>
                                                </a>
                                            </span>
                                        </span>
                                    </p>
                                    <div id="scheduleContent-<?php echo $schedule_id; ?>" class="collapse" aria-labelledby="headingOne">
                                        <article class="txt-height-1-7 padding-t-20">
                                            <p class="txt-sm padding-b-10">
                                                <?php 
                                                    $state_id = get_post_meta( $schedule_id, 'state', true );

                                                    if( $state_id ){
                                                        echo "This course is available in: ";
                                                        $state= get_term($state_id);
                                                        echo $state->name;
                                                    } else {
                                                        echo "Location is unvailable";
                                                    }
                                                ?>
                                            </p>
                                            <p class="txt-sm padding-b-10">
                                                <?php 
                                                    $meta = get_post_meta( $schedule_id, 'venue', true );

                                                    if( $meta ){
                                                        echo "Venue: ".$meta;
                                                    } else {
                                                        echo "Venue is unvailable";
                                                    }
                                                ?>
                                            </p>
                                            <p class="txt-sm padding-b-10">
                                                <?php 
                                                    $meta = get_post_meta( $schedule_id, 'duration', true );

                                                    if( $meta ){
                                                        echo "Duration: ".$meta;
                                                    } else {
                                                        echo "Duration is unvailable";
                                                    }
                                                ?>
                                            </p>

                                            <?php 
                                                $schedulePackage = get_post_meta( $schedule_id, 'schedule-package', true );
                                                $package = maybe_unserialize( $schedulePackage );

                                                if( $meta ){
                                            ?>

                                                <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                                    <p class="txt-normal-s">
                                                        <span class="row">
                                                            <span class="col-8">
                                                                <span class="">
                                                                    <?php echo $package['payment-package-name'] ?>:
                                                                </span>
                                                                <span class="txt-bold">
                                                                    <?php echo ( $package['payment-package-type'] == 'fee' ) ? "₦".$package['payment-package-fee'] : "Free"; ?>
                                                                </span>
                                                                <span>
                                                                    <?php echo (  $package['installment-payment'] ) ? " (Installment Payment available)" : ""; ?>
                                                                </span>
                                                            </span>
                                                            <span class="col-4 text-right">
                                                                <a class="txt-color-blue" data-toggle="collapse" href="#schedulePackage-<?php echo $schedule_id; ?>" aria-expanded="false">
                                                                    View Details 
                                                                    <i class="fa fa-angle-down padding-l-5"></i>
                                                                </a>
                                                            </span>
                                                        </span>
                                                    </p>
                                                    <div id="schedulePackage-<?php echo $schedule_id; ?>" class="collapse" aria-labelledby="headingOne">
                                                        <div class="row">
                                                            <article class="text-box txt-sm txt-height-1-7 padding-t-20">
                                                                <?php echo $package['payment-package-description'] ?>
                                                            </article>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else { ?>
                                                <p class="txt-sm">
                                                    Package is unvailable
                                                </p>
                                            <?php } ?>
                                        </article>
                                    </div>
                                </div>

                        <?php
                                endwhile;

                            }else{
                        ?>
                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-sm txt-height-1-4">
                                    No Schedules available.
                                </h3>
                            </div>

                        <?php } ?>
                            
                    </div>
                </div>
                
                <!-- Instructor -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Instructors
                        </span>
                    </h1>
                    <?php 
                        /* Get Saved Users */
                        $saved_users = get_post_meta($post_id, 'instructors');
                        
                        if($saved_users){
                    ?>
                    <div class="txt-normal-s padding-t-20 padding-lr-15 border-o-1 border-color-darkgrey margin-b-20">
                       <div class="row row-10">
                            <?php 
                                /* Get Saved Users */            
                                foreach($saved_users as $instructor){
                            ?>
                            
                            <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($instructor, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $instructor, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                    $email = rgar( $username_entries[0], 2 );
                                    $phone = rgar( $username_entries[0], 7 );
                                }   

                                restore_current_blog();


                                /* Get User Bio */
                                switch_to_blog(109);

                                $gf_id = 84; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $instructor, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                                if($username_entry_count){ //If no entry

                                    $gender = rgar( $username_entries[0], 2 );
                                    $dob = rgar( $username_entries[0], 3 );
                                    $nationality = rgar( $username_entries[0], 4 );
                                    $location = rgar( $username_entries[0], 6 );
                                }   

                                restore_current_blog();
                            ?>
                        
                            <div class="col-lg-6 padding-lr-10 padding-b-20">
                                <div class="row">
                                    <div class="col-auto">
                                        <img src="<?php echo $avatar_url ?>" alt="" width="70">
                                    </div>
                                    <div class="col padding-l-30">
                                        <h1 class="txt-xlg txt-medium margin-b-5">
                                            <?php echo $displayname ?>
                                        </h1>
                                        <p class="txt-sm txt-medium txt-color-lighter margin-b-10">
                                            SAEDConnect LTD
                                        </p>
                                        <p class="txt-sm txt-medium">
                                            <span>
                                                Marketing &amp; Sale
                                            </span>
                                            <span class="padding-lr-10">
                                                |
                                            </span>
                                            <span>
                                                Industry
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <?php } else { ?>
                                        
                        <div>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-sm txt-height-1-4">
                                    No Instructors available.
                                </h3>
                            </div>

                        </div>
                    <?php } ?>
                </div>

                <!-- FAQ -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Frequently Asked Questions
                        </span>
                    </h1>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'faq', false );
                            $faq_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>

                            <?php foreach ($unserialized_data as $serialized_faq){ ?>

                                <?php 
                                    $faq = maybe_unserialize( $serialized_faq );
                                    $faq_counter++;
                                ?>

                                <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                    <p class="txt-normal-s">
                                        <span class="row">
                                            <span class="col-8 txt-bold">
                                                <?php echo $faq['question'] ?>
                                            </span>
                                            <span class="col-4 text-right">
                                                <a class="txt-color-blue" data-toggle="collapse" href="#faq-<?php echo $faq_counter; ?>" aria-expanded="false">
                                                    View Details 
                                                    <i class="fa fa-angle-down padding-l-5"></i>
                                                </a>
                                            </span>
                                        </span>
                                    </p>
                                    <div id="faq-<?php echo $faq_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                        <div class="row">
                                            <article class="text-box txt-sm txt-height-1-7 padding-t-20">
                                                <?php echo $faq['answer'] ?>
                                            </article>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-sm txt-height-1-4">
                                    No FAQs available.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>

            </div>
            
            <div class="col-md-4 padding-lr-20">
            <?php if( !is_user_logged_in() ){ ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Login to Apply Now
                    </h3>
                    
                    <!-- Login Form -->
                    <style>
                        .gfield_checkbox label, .login-form .gfield_label{
                            display: block !important;
                        }

                        .gfield_checkbox li{
                            display: flex;
                            align-items: center;
                        }

                        .gfield_checkbox input{
                            margin: 0 !important;
                            margin-right: 10px !important;
                        }
                    </style>

                    <?php switch_to_blog(1); ?>

                        <!-- Login Tab -->
                        <div class="login">
                            <div class="login-form">
                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                                <?php endif;?>
                            </div>
                        </div>

                    <?php restore_current_blog(); ?>
 
                    <p class="txt-sm txt-medium padding-t-10">
                        Dont have an account?
                        <a href="https://www.saedconnect.org/register" class=" txt-color-blue">
                            Sign up
                        </a>
                    </p>
                </div>
                <?php } ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Share Job Post
                    </h3>
                    <?php echo do_shortcode('[Sassy_Social_Share]') ?>
                    
                </div>
                <p class="txt-sm txt-medium padding-t-10">
                    <a href="" class=" txt-color-blue">
                        Report Job
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

<?php } elseif( $current_user->ID == $post_author_id && $rendered_view != 'application-manager' && $rendered_view != 'messages' ) { ?>

<section class="padding-t-20 padding-b-80">
    <div class="container-wrapper">
        <div class="row row-20">
            <div class="col-md-8 padding-lr-20">
                <!-- Video -->
                <!--<div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <a class="popup-video padding-r-20" href="<?php echo get_post_meta( $post_id, 'video-link', true ) ?>">
                            <span class="txt-color-white">
                                <i class="fa fa-play-circle padding-r-5"></i>
                                Course Video
                            </span>
                        </a>
                        <a class="txt-color-white txt-sm" data-toggle="modal" href="#opportunityVideoModal">
                            + Add / Edit
                        </a>
                    </h1>
                </div>-->
                
                <!-- Business Basics -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Business Basics
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#businessBasicsModal">
                            + Add
                        </a>
                    </h1>
                    <div class="margin-b-20">
                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'outcome', false );
                            $outcome_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                                <div class="row row-10">

                            <?php foreach ($unserialized_data as $serialized_outcome){ ?>

                                <?php 
                                    $outcome = maybe_unserialize( $serialized_outcome );
                                    $outcome_counter++;
                                ?>
                                
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_amazing_support_network.png" alt="" width="50">
                                        </div>
                                        <div class="col padding-l-30">
                                            <p class="txt-sm">
                                                <?php echo $outcome['outcome'] ?>
                                            </p>
                                            <p class="txt-sm padding-t-15">
                                                <a class="txt-color-green padding-r-15" data-toggle="modal" href="#editOutcomeModal-<?php echo $outcome_counter ?>">
                                                    <i class="fa fa-edit"></i>
                                                    Edit
                                                </a>
                                                <a href="<?php echo currentUrl(true).'?view=form-delete-outcome-'.$outcome_counter; ?>" class="confirm-delete txt-color-red padding-r-15">
                                                    <i class="fa fa-trash"></i>
                                                    Delete
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editOutcomeModal-<?php echo $outcome_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentoutcomeModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-outcome-'.$outcome_counter; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment outcome</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php

                                                        if($_GET['view'] == 'form-delete-outcome-'.$outcome_counter ){
                                                            delete_post_meta($post_id, 'outcome', $serialized_outcome);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                        /* Add/Edit Job (Specification) */

                                                        /* Get Post data */

                                                        $unserialized_data = $outcome;

                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-outcome-'.$outcome_counter){

                                                            $serialized_data = maybe_serialize($_POST);
                                                            echo "Current";
                                                            echo "<br>";
                                                            print_r($serialized_data);

                                                            update_post_meta( $post_id, 'outcome', $serialized_data, $serialized_outcome);

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                    ?>

                                                    <div class="form margin-b-40">

                                                        <!-- Outcome -->
                                                        <div class="form-item">
                                                            <label for="answer">
                                                                Outcome
                                                            </label>
                                                            <textarea class="" name="outcome" id="" cols="30" rows="8"><?php echo $unserialized_data['outcome'] ?></textarea>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                                </div>
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-sm txt-height-1-4">
                                    You have not added any learning Outcomes. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>

                <!-- Business Solution -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Describe Your Business/Solution
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#businessSolutionModal">
                            + Add / Edit
                        </a>
                    </h1>
                    <article class="text-box txt-sm txt-height-1-7 padding-o-15 border-o-1 border-color-darkgrey">
                        <?php 
                            $get_description = get_post_meta( $post_id, 'description', true );
                            echo ( $get_description ) ? $get_description : '<p>You have not added a description. Click the Add Button to add one.</p>'
                        ?>
                    </article>
                </div>
                
                <!-- Unique Business Proposition -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Unique Business Proposition
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#businessPropositionModal">
                            + Add
                        </a>
                    </h1>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'curriculum', false );
                            $curriculum_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="">

                            <?php foreach ($unserialized_data as $serialized_curriculum){ ?>

                                <?php 
                                    $curriculum = maybe_unserialize( $serialized_curriculum );
                                    $curriculum_counter++;
                                ?>
                                
                                <div class="txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="row row-10">
                                        <div class="col-2 padding-lr-10 text-center">
                                            <p class="uppercase padding-b-10">
                                                <?php echo $curriculum['lesson-step'] ?>
                                            </p>
                                            <p class="txt-xxlg">
                                                <?php echo $curriculum['lesson-number'] ?>
                                            </p>
                                        </div>
                                        <div class="col-10 padding-lr-10">
                                            <p class="txt-medium txt-xlg padding-b-10">
                                                <?php echo $curriculum['lesson-title'] ?>
                                            </p>
                                            <p>
                                                <?php echo $curriculum['lesson-summary'] ?>
                                            </p>
                                            <p class="padding-t-15">
                                                <a class="txt-color-green padding-r-15" data-toggle="modal" href="#editcurriculumModal-<?php echo $curriculum_counter ?>">
                                                    <i class="fa fa-edit"></i>
                                                    Edit
                                                </a>
                                                <a href="<?php echo currentUrl(true).'?view=form-delete-curriculum-'.$curriculum_counter; ?>" class="confirm-delete txt-color-red padding-r-15">
                                                    <i class="fa fa-trash"></i>
                                                    Delete
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editcurriculumModal-<?php echo $curriculum_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentcurriculumModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-curriculum-'.$curriculum_counter; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment curriculum</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php

                                                        if($_GET['view'] == 'form-delete-curriculum-'.$curriculum_counter ){
                                                            delete_post_meta($post_id, 'curriculum', $serialized_curriculum);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                        /* Add/Edit Job (Specification) */

                                                        /* Get Post data */

                                                        $unserialized_data = $curriculum;

                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-curriculum-'.$curriculum_counter){

                                                            $serialized_data = maybe_serialize($_POST);
                                                            echo "Current";
                                                            echo "<br>";
                                                            print_r($serialized_data);

                                                            update_post_meta( $post_id, 'curriculum', $serialized_data, $serialized_curriculum);

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                    ?>

                                                    <div class="form margin-b-40">
                                                        <!-- Step -->
                                                        <div class="form-item">
                                                            <label for="step">
                                                                Lesson Step
                                                            </label>
                                                            <input 
                                                                type="text" 
                                                                name="lesson-step" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['lesson-step'] ?>"
                                                                placeholder="e.g. Lesson, Module, Week, Month"
                                                            >
                                                        </div>
                                                        
                                                        <!-- Number -->
                                                        <div class="form-item">
                                                            <label for="number">
                                                                Lesson Number
                                                            </label>
                                                            <input 
                                                                type="text" 
                                                                name="lesson-number" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['lesson-number'] ?>"
                                                                placeholder="e.g. 1, 2, 3"
                                                            >
                                                        </div>

                                                        <!-- Title -->
                                                        <div class="form-item">
                                                            <label for="question">
                                                                Title
                                                            </label>
                                                            <input 
                                                                type="text" 
                                                                name="lesson-title" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['lesson-title'] ?>"
                                                            >
                                                        </div>
                                                        
                                                        <!-- Summary -->
                                                        <div class="form-item">
                                                            <label for="answer">
                                                                Summary
                                                            </label>
                                                            <textarea class="" name="lesson-summary" id="" cols="30" rows="8"><?php echo $unserialized_data['lesson-summary'] ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-sm txt-height-1-4">
                                    You have not added any curricula. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                
                <!-- Business Performance -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Business Performance
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#businessPerfomanceModal">
                            + Add / Edit
                        </a>
                    </h1>
                    <div class="padding-t-15 padding-lr-15 padding-b-5 border-o-1 border-color-darkgrey margin-b-20">
                        <?php 
                            $term_list = wp_get_post_terms($post_id, 'training-category', array("fields" => "names"));

                            if( $term_list ){
                                foreach( $term_list as $key => $term_name ){
                                    echo '<span class="btn btn-trans-bw txt-xs margin-r-5">'.$term_name.'</span>';
                                }
                            } else {
                                echo '<p class="txt-sm padding-b-10">Skills are unvailable</p>';
                            }
                        ?>
                    </div>
                </div>
                
                <!-- Business Media -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Business Media
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#locationModal">
                            + Add / Edit
                        </a>
                    </h1>
                    <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                        <p>
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'state', array("fields" => "names"));

                                if( $term_list ){
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } else {
                                    echo "Locations are unvailable";
                                }
                            ?>
                        </p>
                    </div>
                </div>

                <!-- Contact Information -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Contact Information
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#contactInformationModal">
                            + Add / Edit
                        </a>
                    </h1>
                    <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                        <p>
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'state', array("fields" => "names"));

                                if( $term_list ){
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } else {
                                    echo "Locations are unvailable";
                                }
                            ?>
                        </p>
                    </div>
                </div>

                <!-- Add Team -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Add Team
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#teamMemberModal">
                            + Add / Edit
                        </a>
                    </h1>
                    <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                        <p>
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'state', array("fields" => "names"));

                                if( $term_list ){
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } else {
                                    echo "Locations are unvailable";
                                }
                            ?>
                        </p>
                    </div>
                </div>

                <!-- Business Governance & Structure -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Business Governance & Structure
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#businessGovernanceModal">
                            + Add / Edit
                        </a>
                    </h1>
                    <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                        <p>
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'state', array("fields" => "names"));

                                if( $term_list ){
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } else {
                                    echo "Locations are unvailable";
                                }
                            ?>
                        </p>
                    </div>
                </div>
                
                <!-- Upload Relevant Document -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Upload Relevant Document
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#addDocumentModal">
                            + Add / Edit
                        </a>
                    </h1>
                    <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                        <p>
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'state', array("fields" => "names"));

                                if( $term_list ){
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } else {
                                    echo "Locations are unvailable";
                                }
                            ?>
                        </p>
                    </div>
                </div>
                
                <!-- Instructor -->
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-grey-light txt-medium padding-o-15">
                        <span class="txt-normal-s txt-medium txt-color-light">
                            Instructors
                        </span>
                        <a class="txt-sm" data-toggle="modal" href="#instuctorsModal">
                            + Add / Edit
                        </a>
                    </h1>
                    <?php 
                        /* Get Saved Users */
                        $saved_users = get_post_meta($post_id, 'instructors');
                        
                        if($saved_users){
                    ?>
                    <div class="txt-normal-s padding-t-20 padding-lr-15 border-o-1 border-color-darkgrey margin-b-20">
                       <div class="row row-10">
                            <?php 
                                /* Get Saved Users */            
                                foreach($saved_users as $instructor){
                            ?>
                            
                            <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($instructor, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $instructor, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                    $email = rgar( $username_entries[0], 2 );
                                    $phone = rgar( $username_entries[0], 7 );
                                }   

                                restore_current_blog();


                                /* Get User Bio */
                                switch_to_blog(109);

                                $gf_id = 84; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $instructor, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                                if($username_entry_count){ //If no entry

                                    $gender = rgar( $username_entries[0], 2 );
                                    $dob = rgar( $username_entries[0], 3 );
                                    $nationality = rgar( $username_entries[0], 4 );
                                    $location = rgar( $username_entries[0], 6 );
                                }   

                                restore_current_blog();
                            ?>
                        
                            <div class="col-lg-6 padding-lr-10 padding-b-20">
                                <div class="row">
                                    <div class="col-auto">
                                        <img src="<?php echo $avatar_url ?>" alt="" width="70">
                                    </div>
                                    <div class="col padding-l-30">
                                        <h1 class="txt-xlg txt-medium margin-b-5">
                                            <?php echo $displayname ?>
                                        </h1>
                                        <p class="txt-sm txt-medium txt-color-lighter margin-b-10">
                                            SAEDConnect LTD
                                        </p>
                                        <p class="txt-sm txt-medium">
                                            <span>
                                                Marketing &amp; Sale
                                            </span>
                                            <span class="padding-lr-10">
                                                |
                                            </span>
                                            <span>
                                                Industry
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <?php } else { ?>
                                        
                        <div>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-sm txt-height-1-4">
                                    You have not added any Instructors. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        </div>
                    <?php } ?>
                </div>


                

                <!-- Application Requirements -->
                <!--<div class="margin-b-40">
                    <a 
                       class="dropdown-toggle txt-medium txt-color-dark d-flex justify-content-between align-items-center border-o-2 padding-o-15"
                       data-toggle="modal" href="#applicationReqirementModal"
                    >
                        Select Application Requirements
                    </a>
                </div>-->
            </div>
            <div class="col-md-4 padding-lr-20">
                <?php if( $current_user->ID == $post_author_id ) { ?>
                    <div class="bg-grey padding-tb-30 padding-lr-20 margin-b-15">
                        <div class="txt-normal-s txt-medium text-center margin-b-15">
                            Course Status
                        </div>
                        <div class="bg-yellow padding-o-10 rounded-o-12 text-center margin-b-15">
                            <?php
                                $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                if($publication_meta == 'user_published'){
                                    echo 'Undergoing Review';
                                } elseif($publication_meta == 'admin_published') {
                                    echo 'Published';
                                }else{
                                    echo 'Draft';
                                }
                            ?>
                        </div>
                        <p class="txt-xs text-center">
                           Publishing your course allows it to be listed in the SAEDConnect directory.
                           You must complete the Trainer Information section & select at least 1 skill that you train on, before you can publish your Trainer profile.
                        </p>
                        <div class="text-center padding-t-30">
                            <a href="<?php echo $post_link.'?view=publication'; ?>" class="txt-normal-s txt-medium no-m-b">
                               <u class="txt-color-dark"><?php echo ( $publication_meta ) ? 'Unpublish' : 'Publish Profile' ?></u>
                            </a>
                        </div>
                    </div>
                    <div class="padding-o-20 margin-b-20 border-o-1 border-color-blue">
                        <div class="txt-normal-s txt-medium text-center margin-b-20">
                           NYSC Accreditation
                        </div>
                        <?php if( false/*$accreditation_meta['status']*/){ ?>

                            <p class="txt-sm text-center">
                               You have requested accreditation in 
                               <span class="txt-medium"><?php echo $accreditation_meta['state']; ?></span> 
                               state.
                            </p>

                        <?php }else{ ?>
                            <p class="txt-sm text-center padding-b-15">
                               Become recognized as a NYSC approved trainer under the SAED Program. Corp members who attend a training offered by an accredited SAED trainer are eligible to receive a special SAED certificate at the end of their service year.
                            </p>
                            <form class="" method="post" action="<?php echo currentUrl(true).'?view=form-nysc-accreditation'; ?>">
                                <div style="margin-bottom:10px">
                                    <div class="padding-t-20">
                                        <?php 

                                            /*
                                            *
                                            * Populate Form Data from Terms
                                            *
                                            */
                                            //Get Terms
                                            $terms = get_terms( 'state', array('hide_empty' => false));

                                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                                // Check and see if the term is a top-level parent. If so, display it.
                                                $term_id = $term->term_id; //Get the term ID
                                                $term_name = $term->name; //Get the term name
                                                $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term_id, true );
                                        ?>
                                            <?php if( $saved_meta ){ ?>

                                            <div class="txt-sm txt-medium d-flex justify-content-between align-items-center margin-b-20">
                                                <span>
                                                    <?php echo $term_name; ?>
                                                    <span class="txt-color-yellow-dark">
                                                    (
                                                    <?php

                                                        if($saved_meta == 'user_published'){
                                                            echo 'In Review';
                                                        } elseif($saved_meta == 'admin_published') {
                                                            echo 'Published';
                                                        }
                                                    ?>
                                                    )
                                                    </span>
                                                </span>
                                                <a href="<?php echo currentUrl(true).'?view=form-nysc-accreditation&id='.$term_id ?>" class="txt-color-blue confirmation">Cancel</a>
                                            </div>

                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <a data-toggle="collapse" href="#trainerProfileSelectStates" aria-expanded="false" class="d-flex">
                                        <span class="flex_1 padding-o-10 d-flex justify-content-between txt-normal-s bg-blue txt-color-white">
                                            <label for="state">Request Accreditation</label>
                                            <i class="fa fa-angle-down"></i>
                                        </span>

                                    </a>
                                    <div id="trainerProfileSelectStates" class="collapse" aria-labelledby="headingOne">
                                        <div class="padding-t-20">
                                            <p class="txt-sm padding-b-15">Select the state(s) where you want to be accredited</p>
                                            <?php 

                                                /*
                                                *
                                                * Populate Form Data from Terms
                                                *
                                                */
                                                //Get Terms
                                                $terms = get_terms( 'state', array('hide_empty' => false));

                                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                                    // Check and see if the term is a top-level parent. If so, display it.
                                                    $term_id = $term->term_id; //Get the term ID
                                                    $term_name = $term->name; //Get the term name
                                                    $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term_id, true );
                                            ?>
                                                <?php if( $saved_meta ){ ?>


                                                <?php }else{ ?>
                                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                                    <input
                                                        class="margin-r-5 icheck"
                                                        type="checkbox" 
                                                        value="<?php echo $term_id ?>" 
                                                        name="state[]" 
                                                        <?php echo ( $saved_meta ) ? "checked" : "" ?>
                                                    >
                                                    <span class="bg-label padding-l-5" style="padding:5px 10px !important;">
                                                        <?php echo $term_name; ?>
                                                    </span>
                                                </label>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="text-center padding-t-10">
                                            <input type="submit" value="Request Accredition" class="btn btn-blue txt-xs txt-medium no-m-b">
                                        </div>
                                    </div>
                                </div>

                           </form>
                        <?php } ?>
                    </div>
                    
                    <!--<div class="padding-o-20 bg-grey margin-b-20">
                        <h3 class="txt-bold txt-height-1-4 margin-b-15">
                            Recieve &amp; Manage Applications for this Job on SAEDConnect
                        </h3>
                        <p class="txt-height-1-7 txt-sm margin-b-20">
                            Recieve &amp; Manage Applications for this Job on SAEDConnect Recieve &amp; Manage Applications for this Job on SAEDConnect
                        </p>
                        <div>
                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                Get started
                            </a>
                        </div>
                    </div>
                    <div class="padding-o-20 bg-ash margin-b-20">
                        <span class="d-inline-flex align-items-center padding-r-40">
                            <input type="checkbox" id="minimal-checkbox-3" name="radio-2" value="beginner">
                            <label for="minimal-radio-3" class="txt-color-white padding-l-10 txt-medium">Add a Program Brochure</label>
                        </span>
                    </div>
                    <div class="padding-o-20 bg-ash margin-b-20">
                        <h3 class="txt-bold txt-color-white txt-height-1-4 margin-b-15">
                            Advertise this Job
                        </h3>
                        <ul class="icon-list white txt-sm">
                            <li>
                                <a href="http://www.saedconnect.org/contribute/synopsis/corporate/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>Feature on Homepage</span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/contribute/synopsis/schools-youth-development/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>Feature in Jobs Newsletter</span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/contribute/synopsis/government-donor-agencies/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Send a standalaone broadcast
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>-->
                <?php } ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Share Course
                    </h3>
                    <?php echo do_shortcode('[Sassy_Social_Share]') ?>
                </div>
                <p class="txt-sm txt-medium padding-t-10">
                    <a href="" class=" txt-color-blue">
                        Report Course
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

<?php } ?>