<?php if( $current_user->ID == $post_author_id ) { ?>


<style>
    .select2-container{
        width: 100% !important;
    }
</style>


<!-- *********************************************************************** -->

<!-- Specification -->
<div class="modal fade font-main filter-modal" id="specificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-specification'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Specifications</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Meta Key */
                        $tax_types = array(
                            array(
                                'name' => 'Training Category',
                                'slug' => 'training-category',
                                'hierachical' => true,
                            ),
                        );

                        $redirect_link = currentUrl(true);

                        $postName = $post->post_title;
                        $courseLevel = get_post_meta( $post->ID, 'course-level', true );
                        $summary = get_post_meta( $post->ID, 'summary', true );

                        /* Publish / Unpublish & Return */
                        if($_GET['action'] == 'publication'){
                            /* Meta value to save */
                            $value = "user_published";

                            /* Get saved meta */
                            $saved_meta = get_post_meta( $post_id, $publication_key, true );

                            if ( $saved_meta ) //If published, Unpublish
                                delete_post_meta( $post_id, $publication_key );
                            else //If Unpublished, Publish
                                update_post_meta( $post_id, $publication_key, $value );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-specification'){

                            /* Get Post Name */
                            $postName = $_POST['post-name'];
                            $courseLevel = sanitize_text_field( $_POST['course-level'] );
                            $summary = sanitize_text_field( $_POST['summary'] );

                            /* Save Post to DB */
                            $post_id = wp_insert_post(array (
                                'ID' => $post_id,
                                'post_type' => $post_type,
                                'post_title' => $postName,
                                'post_content' => "",
                                'post_status' => 'publish',
                            ));

                            update_post_meta( $post_id, 'course-level', $courseLevel );
                            update_post_meta( $post_id, 'summary', $summary );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>

                    <!-- Title -->
                    <div class="form-item">
                        <label for="deadline">
                            Course Title
                        </label>
                        <input 
                            type="text" 
                            name="post-name" 
                            class="d-block padding-tb-5 padding-lr-10 full-width"
                            value="<?php echo $postName ?>"
                        >
                    </div>

                    <!-- Course Level -->
                    <div class="form-item">
                        <label for="course-level">
                            Course Level
                        </label>
                        <select name="course-level" required>
                            <option value="beginner" <?php echo ($courseLevel == 'beginner')? "selected" : "" ?>>
                                Beginner
                            </option>
                            <option value="intermediate" <?php echo ($courseLevel == 'intermediate')? "selected" : "" ?>>
                                Intermediate
                            </option>
                            <option value="advanced" <?php echo ($courseLevel == 'advanced')? "selected" : "" ?>>
                                Advanced
                            </option>
                            <option value="open" <?php echo ($courseLevel == 'open')? "selected" : "" ?>>
                                Open to All
                            </option>
                        </select>
                    </div>
                    
                    <!-- Summary -->
                    <div class="form-item">
                        <label for="post-name">
                            Summary
                        </label>
                        <textarea name="summary" cols="30" rows="4"><?php echo $summary ?></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Image -->
<div class="modal fade font-main filter-modal" id="featuredImageModal" tabindex="-1" role="dialog" aria-labelledby="opportunityVideoModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="featured_upload" method="post" action="<?php echo currentUrl(true).'?view=form-featured-image'; ?>" enctype="multipart/form-data">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Featured Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <div class="image-upload-card">
                        <?php
                            $images = get_attached_media( 'image', $post_id );

                             if($images){

                                foreach($images as $image) { 
                                    $previousImg_id = $image->ID;
                                }
                            }

                            if( $_POST && $rendered_view == 'form-featured-image'){


                                // Check that the nonce is valid, and the user can edit this post.
                                if ( 
                                    isset( $_POST['my_image_upload_nonce'], $_POST['post_id'] ) 
                                    && wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
                                    && current_user_can( 'edit_post', $_POST['post_id'] )
                                ) {
                                    // The nonce was valid and the user has the capabilities, it is safe to continue.

                                    // These files need to be included as dependencies when on the front end.
                                    require_once( ABSPATH . 'wp-admin/includes/image.php' );
                                    require_once( ABSPATH . 'wp-admin/includes/file.php' );
                                    require_once( ABSPATH . 'wp-admin/includes/media.php' );

                                    /* Delete prevoius image */
                                    $_POST['previous_img'] = $previousImg_id;
                                    if($previousImg_id){
                                        wp_delete_attachment( $previousImg_id );
                                    }

                                    // Let WordPress handle the upload.
                                    // Remember, 'my_image_upload' is the name of our file input in our form above.
                                    $attachment_id = media_handle_upload( 'my_image_upload', $_POST['post_id'] );

                                    if ( is_wp_error( $attachment_id ) ) {
                                        // There was an error uploading the image.
                                    } else {
                                        // The image was uploaded successfully!
                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                    }

                                } else {

                                    // The security check failed, maybe show the user an error.
                                }
                            }
                        ?>
                    
                        <input type="file" name="my_image_upload" id="my_image_upload" class="upload-field" multiple="false">
                        <input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id ?>">
                        <input type="hidden" name="previous_img" id="post_id" value="<?php echo $previousImg_id ?>">
                        <?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
                        
                        <div class="text-align-center bg-darkgrey txt-xlg text-center txt-light" id="featuredImagePreview">
                            Click here to select an image from your device
                        </div>
                     </div>   
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input class="btn btn-blue txt-sm padding-lr-15" id="submit_my_image_upload" name="submit_my_image_upload" type="submit" value="Upload">
                </div>
                <script>
                    function readURL(input) {
                      if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function(e) {
                            $('#featuredImagePreview').css('background-image', 'url(' + e.target.result + ')').text("");
                        }

                        reader.readAsDataURL(input.files[0]);
                      }
                    }

                    $("#my_image_upload").change(function() {
                        readURL(this);
                    });
                </script>
                <style>
                    .image-upload-card{
                        position: relative;
                    }
                    
                    .image-upload-card .upload-field{
                        position: absolute;
                        width: 100%;
                        top: 0;
                        left: 0;
                        right: 0;
                        bottom: 0;
                        opacity: 0;
                        z-index: 100;
                        cursor: pointer;
                    }
                    
                    .image-upload-card #featuredImagePreview {
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        height: 400px;
                        padding: 15px;
                    }
                </style>
            </form>
        </div>
    </div>
</div>

<!-- Video Link -->
<div class="modal fade font-main filter-modal" id="opportunityVideoModal" tabindex="-1" role="dialog" aria-labelledby="opportunityVideoModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-video'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Video Link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $videoLink = get_post_meta( $post_id, 'video-link', true );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-video'){

                            /* Get Post Name */
                            $videoLink = sanitize_text_field( $_POST['video-link'] );

                            update_post_meta( $post_id, 'video-link', $videoLink );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Video Link -->
                        <div class="form-item">
                            <label for="post-name">
                                YouTube Video Link
                            </label>
                            <input 
                                type="url" 
                                name="video-link" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo $videoLink ?>"
                                required
                            >
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Business Basics -->
<div class="modal fade font-main filter-modal" id="businessBasicsModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-outcome'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Business Basics</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-outcome'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'outcome', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <!-- Business Basics -->
                    <div class="form-item">
                        <label for="">Stage of this Business/venture </label>
                        <select name="" id="">
                            <option value="">Idea/concept stage</option>
                            <option value="">I have drafted a business plan</option>
                            <option value="">Prototype built/I have set things up</option>
                            <option value="">I have paying customers</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="">Is your Business/venture Registered?</label>
                        <select name="" id="">
                            <option value="">No</option>
                            <option value="">Yes (Private Entity)</option>
                            <option value="">Yes (NGO/Charity Organization)</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="">In which country is your business registered? (If yes to above) </label>
                        <select name="" id="">
                            <option value="">No</option>
                            <option value="">Yes (Private Entity)</option>
                            <option value="">Yes (NGO/Charity Organization)</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="">
                            Founding Date 
                        </label>
                        <input 
                            type="text" 
                            name="post-name" 
                            class="d-block padding-tb-5 padding-lr-10 full-width"
                            value="<?php echo $postName ?>"
                        >
                    </div>
                    <div class="form-item">
                        <label for="">Location of Business HQ </label>
                        <select name="" id="">
                            <option value="">No</option>
                            <option value="">Yes (Private Entity)</option>
                            <option value="">Yes (NGO/Charity Organization)</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label class="padding-b-10">States where you operate</label>
                        <div>
                            <label class="txt-sm d-inline-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="1" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Abia
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="2" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Adamawa
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Akwa Ibom
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Anambra
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Bauchi
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Bayelsa
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Benue
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Borno
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Cross river
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Delta
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Ebonyi
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Edo
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Ekiti
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Enugu
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Gombe
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Imo
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Jigawa
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kaduna
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kano
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kastina
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kebbi
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kogi
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kwara
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Lagos
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Nassarawa
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Niger
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Ogun
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Ondo
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Osun
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Oyo
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Plateau
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Rivers
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Sokoto
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Taraba
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Yobe
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Zamfara
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Federal Capital Territory (FCT)
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="form-item">
                        <label class="padding-b-10">In what other states is your business/venture active?</label>
                        <div>
                            <label class="txt-sm d-inline-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="1" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Abia
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="2" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Adamawa
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Akwa Ibom
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Anambra
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Bauchi
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Bayelsa
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Benue
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Borno
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Cross river
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Delta
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Ebonyi
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Edo
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Ekiti
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Enugu
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Gombe
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Imo
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Jigawa
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kaduna
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kano
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kastina
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kebbi
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kogi
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Kwara
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Lagos
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Nassarawa
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Niger
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Ogun
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Ondo
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Osun
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Oyo
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Plateau
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Rivers
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Sokoto
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Taraba
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Yobe
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Zamfara
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="3" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Federal Capital Territory (FCT)
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Business Solution -->
<div class="modal fade font-main filter-modal" id="businessSolutionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-description'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Describe Your Business/Solution</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $description = get_post_meta( $post_id, 'description', true );


                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-description'){

                            /* Get Post Name */
                            $description = wp_kses_post( $_POST['description'] );

                            /* Update Meta */
                            update_post_meta( $post_id, 'description', $description );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        
                        <!-- Description -->
                        <div class="form-item">
                            <label for="post-name">
                                What need or challenge is your business/Solution addressing? 
                            </label>
                            <textarea class="editor" name="description" cols="30" rows="20"><?php echo $description ?></textarea>
                        </div>
                        <div class="form-item">
                            <label for="post-name">
                                Describe your Business/Solution idea  
                            </label>
                            <textarea class="editor" name="description" cols="30" rows="20"><?php echo $description ?></textarea>
                        </div>
                        <div class="form-item">
                            <label for="post-name">
                                What is your product/service?  
                            </label>
                            <textarea class="editor" name="description" cols="30" rows="20"><?php echo $description ?></textarea>
                        </div>
                        <div class="form-item">
                            <label for="">Do you have a working prototype of your product/Solution?</label>
                            <select name="" id="">
                                <option value="">Yes</option>
                                <option value="">No</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="post-name">
                                Who are your customers   
                            </label>
                            <textarea class="editor" name="description" cols="30" rows="20"><?php echo $description ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Unique Business Proposition -->
<div class="modal fade font-main filter-modal" id="businessPropositionModal" tabindex="-1" role="dialog" aria-labelledby="FAQModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-curriculum'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Unique Business Proposition</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-curriculum'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'curriculum', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>
                    
                    <!-- Step -->
                    <div class="form-item">
                        <label for="step">
                            How is your Business/venture unique? 
                        </label>
                        <textarea name="" id="" class="editor"></textarea>
                    </div>

                    <!-- Number -->
                    <div class="form-item">
                        <label for="number">
                        What strengths & opportunities do you have that gives you an advantage to be successful in this business/Social initiative? 
                        </label>
                        <textarea name="" id="" class="editor"></textarea>
                    </div>

                    <!-- Summary -->
                    <div class="form-item">
                        <label for="answer">
                            Any Additional Information you want to share about your business/venture?
                        </label>
                        <textarea class="editor" name="lesson-summary"><?php echo $lessonSummary ?></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Business Performance -->
<div class="modal fade font-main filter-modal" id="businessPerfomanceModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-outcome'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Business Performance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-outcome'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'outcome', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form-item">
                        <label class="padding-b-10">How have you raised capital for this business/venture?</label>
                        <div>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="1" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Personal Funds
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="2" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Family & Friends
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="1" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Loan
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="2" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Equity Contribution
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="1" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    Grant
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="2" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    I am yet to raise any capital
                                </span>
                            </label>
                            <label class="txt-sm d-inline-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="2" 
                                    name="area[]" 
                                >
                                <span class="padding-l-10">
                                    I don’t know
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            How much money have you invested in the business/venture till date?
                        </label>
                        <input type="text">
                    </div>
                    <div class="form-item">
                        <label for="">Do you have paying customers already?</label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="">Have you reached Break even?</label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="">Have you reached Break even?</label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                            <option value="I don't know">I don't know</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="">Have you started making profit?</label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                            <option value="I don't know">I don't know</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            What was your revenue last year?
                        </label>
                        <input type="text">
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Any Additional Information you want to share about your business/venture performance
                        </label>
                        <textarea name="" id="" class="editor"></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Contact Information -->
<div class="modal fade font-main filter-modal" id="contactInformationModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-outcome'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Contact Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-outcome'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'outcome', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form-item">
                        <label for="answer">
                            Name of Contact Person (First Name, Last Name)
                        </label>
                        <input type="text">
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Primary Contact Email
                        </label>
                        <input type="email">
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Primary Contact Telephone Number
                        </label>
                        <input type="telephone">
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Address of your Company Headquarters
                        </label>
                        <textarea name="" id="" class="editor"></textarea>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Website Address
                        </label>
                        <input type="url">
                    </div>
                    <div class="form-item">
                        <label for="answer">
                           Twitter Handle
                        </label>
                        <input type="text">
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Instagram Handle
                        </label>
                        <input type="text">
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            WhatsApp Number
                        </label>
                        <input type="telephone">
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Team Member -->
<div class="modal fade font-main filter-modal" id="teamMemberModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-outcome'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add Team</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-outcome'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'outcome', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form-item">
                        <label for="answer">
                            Full Name
                        </label>
                        <input type="text">
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Role in the business
                        </label>
                        <input type="email">
                    </div>
                    <div class="form-item">
                        <label for="">Gender</label>
                        <select name="" id="">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Business Governance -->
<div class="modal fade font-main filter-modal" id="businessGovernanceModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-outcome'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Business Governance & Structure</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-outcome'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'outcome', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form-item">
                        <label for="answer">
                            Do you keep financial records of all your income & expenditure?
                        </label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Do you have a written strategy with annual budget for the year?
                        </label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Do you have a formal Board of Directors?
                        </label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Have you ever audited your accounts
                        </label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    
                    <div class="form-item">
                        <label for="answer">
                            Any Additional Information you want to share about your business governance and structure
                        </label>
                        <textarea name="" id="" class="editor"></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Relevant Documents -->
<div class="modal fade font-main filter-modal" id="addDocumentModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-outcome'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add Document</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-outcome'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'outcome', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form-item">
                        <label for="answer">
                            Do you keep financial records of all your income & expenditure?
                        </label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Do you have a written strategy with annual budget for the year?
                        </label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Do you have a formal Board of Directors?
                        </label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="answer">
                            Have you ever audited your accounts
                        </label>
                        <select name="" id="">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    
                    <div class="form-item">
                        <label for="answer">
                            Any Additional Information you want to share about your business governance and structure
                        </label>
                        <textarea name="" id="" class="editor"></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>





<!-- Fee -->
<div class="modal fade font-main filter-modal" id="FeesModal" tabindex="-1" role="dialog" aria-labelledby="FeesModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-payment-type'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Fees</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $paymentType = get_post_meta( $post_id, 'payment-type', true );
                        $paymentFee = get_post_meta( $post_id, 'payment-fee', true );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-payment-type'){

                            /* Get Post Name */
                            $paymentType = sanitize_text_field( $_POST['payment-type'] );
                            $paymentFee = sanitize_text_field( $_POST['payment-fee'] );

                            update_post_meta( $post_id, 'payment-type', $paymentType );
                            update_post_meta( $post_id, 'payment-fee', $paymentFee );


                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Job Type -->
                        <div class="form-item">
                            <select name="payment-type" required>
                                <option value="free" <?php echo ($paymentType == 'free')? "selected" : "" ?>>
                                    This program is Free
                                </option>
                                <option value="fee" <?php echo ($paymentType == 'fee')? "selected" : "" ?>>
                                    Interested candidates pay a fee
                                </option>
                                <option value="package" <?php echo ($paymentType == 'package')? "selected" : "" ?>>
                                    There are different Fee Packages
                                </option>
                            </select>
                        </div>
                        
                        <!-- Manual Application -->
                        <div class="conditional" data-condition="['free'].includes(payment-type)">
                            <p class="txt-normal-s txt-medium">
                                Applicants will not be required to pay any fee to apply for this Program.
                            </p>
                        </div>
                        <!-- 3rd Party -->
                        <div class="conditional" data-condition="['fee'].includes(payment-type)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Enter the fee for this program in NGN.
                            </p>
                            <div class="form-item">
                                <input 
                                    type="number" 
                                    name="payment-fee" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $paymentFee ?>"
                                >
                            </div>
                        </div>
                        <!-- SAEDConnect -->
                        <div class="conditional" data-condition="['package'].includes(payment-type)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Selecting this option allows you to create and add multiple payment packages to this Program.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Payment Package -->
<div class="modal fade font-main filter-modal" id="AddPaymentPackageModal" tabindex="-1" role="dialog" aria-labelledby="AddPaymentPackageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-payment-package'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment Package</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-payment-package'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'package', $serialized_data, false);
                            
                            $unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Name -->
                        <div class="form-item">
                            <label for="">
                                Name
                            </label>
                            <input 
                                type="text" 
                                name="payment-package-name" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo $unserialized_data['payment-package-name'] ?>"
                            >
                        </div>
                        
                        <!-- Description -->
                        <div class="form-item">
                            <label for="">
                                Description
                            </label>
                            <textarea class="" name="payment-package-description" id="" cols="30" rows="10"><?php echo $unserialized_data['payment-package-description'] ?></textarea>
                        </div>
                        
                        <!-- Payment Type -->
                        <div class="form-item">
                            <label for="">
                                Payment Method
                            </label>
                            <select name="payment-package-type" required>
                                <option value="free" <?php echo ($unserialized_data['payment-package-type'] == 'free')? "selected" : "" ?>>
                                    This package is Free
                                </option>
                                <option value="fee" <?php echo ($unserialized_data['payment-package-type'] == 'fee')? "selected" : "" ?>>
                                    Interested candidates pay a fee
                                </option>
                            </select>
                        </div>
                        
                        <!-- free -->
                        <div class="conditional" data-condition="['free'].includes(payment-package-type)">
                            <p class="txt-normal-s txt-medium">
                                Applicants will not be required to pay a fee for this package.
                            </p>
                        </div>
                        
                        <!-- package Fee -->
                        <div class="conditional" data-condition="['fee'].includes(payment-package-type)">
                            <div class="form-item">
                                <label for="">Enter the fee for this package in NGN.</label>
                                <input 
                                    type="number" 
                                    name="payment-package-fee" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $unserialized_data['payment-package-fee'] ?>"
                                >
                            </div>
                        </div>
                        
                        <!-- Installment -->
                        <div class="conditional" data-condition="['fee'].includes(payment-package-type)">
                            <label class="checkbox-item txt-normal-s d-flex align-items-center padding-t-30 padding-r-10 padding-b-10">
                                <input
                                    class="margin-r-5"
                                    type="checkbox" 
                                    value="installment-payment" 
                                    name="installment-payment" 
                                    <?php echo ($installmentPayment) ? "checked" : "" ?>
                                >
                                <span class="bg-label padding-l-10">
                                    Installment Payment available 
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>



<!-- FAQ -->
<div class="modal fade font-main filter-modal" id="FAQModal" tabindex="-1" role="dialog" aria-labelledby="FAQModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-faq'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit FAQ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-faq'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'faq', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <!-- Title -->
                    <div class="form-item">
                        <label for="question">
                            Question
                        </label>
                        <input 
                            type="text" 
                            name="question" 
                            class="d-block padding-tb-5 padding-lr-10 full-width"
                            value="<?php echo $question ?>"
                        >
                    </div>

                    <!-- Deadline -->
                    <div class="form-item">
                        <label for="answer">
                            Answer
                        </label>
                        <textarea class="editor" name="answer" id="" cols="30" rows="8"><?php echo $answer ?></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Skills -->
<div class="modal fade font-main filter-modal" id="skillsModal" tabindex="-1" role="dialog" aria-labelledby="skillsModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-skills'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Skills</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Meta Key */
                        $tax_types = array(
                            array(
                                'name' => 'Skills',
                                'slug' => 'training-category',
                                'hierachical' => true,
                            ),
                        );

                        $redirect_link = currentUrl(true);

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */

                        if(isset($_POST['submit']) && $rendered_view == 'form-skills'){

                            /* Return Terms assigned to Post and remove all terms */
                            foreach($tax_types as $tax_type){
                                
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids"));
                                
                                foreach($term_list as $remove_id){
                                    wp_remove_object_terms( $post_id, $remove_id, $tax_type['slug'] );
                                }
                            }
                            
                            /* Save terms to post */
                            foreach($tax_types as $tax_type){
                                wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug']);
                            }

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>

                    <!-- Terms -->
                    <div class="txt-normal-s">
                    <?php foreach($tax_types as $tax_type){ ?>

                        <?php if( $tax_type['hierachical']){ ?>

                            <!-- Tax Title -->
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_type['name']; ?>
                            </h2>

                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <?php if( $parent == 0 ){ ?>

                                    <div class="padding-b-20">
                                        <div class="txt-medium txt-color-dark margin-b-15">
                                            <?php echo $term_name; ?>
                                        </div>
                                        <?php
                                            foreach ($terms as $child_term) {
                                                // Check and see if the term is a top-level parent. If so, display it.
                                                $child_parent = $child_term->parent;
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name

                                                if( $child_parent == $term_id ){
                                        ?>
                                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                    <input
                                                        class="margin-r-5"
                                                        type="checkbox" 
                                                        value="<?php echo $child_term_id ?>" 
                                                        name="<?php echo $tax_type['slug'] ?>[]" 
                                                        <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                    >
                                                    <span class="bg-label padding-l-5">
                                                        <?php echo $child_term_name; ?>
                                                    </span>
                                                </label>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </div>

                                <?php } ?>

                            <?php } ?>

                        <?php } else { ?>

                            <!-- Tax Title -->
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_type['name']; ?>
                            </h2>

                            <div class="padding-b-20">
                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                    <input
                                        class="margin-r-5"
                                        type="checkbox" 
                                        value="<?php echo $term_id ?>" 
                                        name="<?php echo $tax_type['slug'] ?>[]" 
                                        <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                    >
                                    <span class="bg-label padding-l-5">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>

                            <?php } ?>
                            </div>
                        <?php } ?>

                    <?php } ?>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" value="Submit" name="submit"  class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Schedules -->
<div class="modal fade font-main filter-modal" id="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-schedule'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add Schedule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php

                        $redirect_link = currentUrl(true);


                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-schedule'){

                            /* Get Post Name */
                            $postName = sanitize_text_field( $_POST['post-name'] );
                            $date = sanitize_text_field( $_POST['date'] ); 
                            $state = $_POST['state']; 
                            
                            $venue = sanitize_text_field( $_POST['venue'] ); 
                            $duration = sanitize_text_field( $_POST['duration'] ); 
                            $schedulePackage = sanitize_text_field( $_POST['schedule-package'] ); 
                                    
                            /* Save Post to DB */
                            $schedule_id = wp_insert_post(array (
                                'post_type' => 'schedule',
                                'post_title' => $date,
                                'post_content' => "",
                                'post_status' => 'publish',
                            ));

                            update_post_meta( $schedule_id, 'parent', $post_id );
                            update_post_meta( $schedule_id, 'date', $date );
                            update_post_meta( $schedule_id, 'state', $state );
                            update_post_meta( $schedule_id, 'venue', $venue );
                            update_post_meta( $schedule_id, 'duration', $duration );
                            update_post_meta( $schedule_id, 'schedule-package', $schedulePackage );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>

                    <!-- Date -->
                    <div class="form-item">
                        <label for="deadline">
                            Date
                        </label>
                        <div class="position-relative">
                            <input type="date" name="date" required class="position-relative" style="z-index:1;">
                            <span class="position-absolute" style="right:12px; top:12px; z-index:0;">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>

                    <!-- State -->
                    <div class="form-item ">
                        <!-- Tax Title -->
                        <label for="state">
                            Select Location
                        </label>
                        <select name="state">
                        <?php 
                            /*
                            *
                            * Populate Form Data from Terms
                            *
                            */
                            //Get Terms
                            $terms = get_terms( 'state', array('hide_empty' => false));

                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                // Check and see if the term is a top-level parent. If so, display it.
                                $parent = $term->parent;
                                $term_id = $term->term_id; //Get the term ID
                                $term_name = $term->name; //Get the term name
                        ?>

                            <option 
                                value="<?php echo $term_id ?>" 
                                <?php echo ($state == $term_id) ? "selected" : "" ?>
                            ><?php echo $term_name; ?></option>

                        <?php } ?>
                        </select>
                    </div>
                    
                    <!-- Venue -->
                    <div class="form-item">
                        <label for="venue">
                            Venue
                        </label>
                        <textarea name="venue" id="venue" cols="30" rows="4"></textarea>
                    </div>
                    
                    <!-- Duration -->
                    <div class="form-item">
                        <label for="duration">
                            Duration
                        </label>
                        <input 
                            type="text" 
                            name="duration" 
                            required
                        >
                    </div>
                    
                    <!-- Package -->
                    <div class="form-item ">
                        <!-- Tax Title -->
                        <label for="package">
                            Select Package
                        </label>
                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'package', false );
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <select name="schedule-package">
                                
                            <?php foreach ($unserialized_data as $serialized_package){ ?>

                                <?php $package = maybe_unserialize( $serialized_package ); ?>
                                
                                <option 
                                    value='<?php echo $serialized_package ?>' 
                                    <?php echo ($schedulePackage == $serialized_package ) ? "selected" : "" ?>
                                ><?php echo $package['payment-package-name'] ?></option>

                            <?php } ?>
                            
                            </select>
                        <?php }else{ ?>

                            <p class="txt-sm txt-height-1-4">
                                You have not added any course packages.
                            </p>

                        <?php } ?>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Instructors -->
<div class="modal fade font-main filter-modal" id="instuctorsModal" tabindex="-1" role="dialog" aria-labelledby="instuctorsModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-instructors'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Instructors</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Meta Key */
                        $tax_types = array(
                            array(
                                'name' => 'Skills',
                                'slug' => 'skill',
                                'hierachical' => false,
                            ),
                        );

                        $redirect_link = currentUrl(true);

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */

                        if(isset($_POST['submit']) && $rendered_view == 'form-instructors'){

                            /* Return Terms assigned to Post and remove all terms */
                            foreach($tax_types as $tax_type){
                                
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids"));
                                
                                foreach($term_list as $remove_id){
                                    wp_remove_object_terms( $post_id, $remove_id, $tax_type['slug'] );
                                }
                            }
                            
                            /* Save terms to post */
                            foreach($tax_types as $tax_type){
                                wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug']);
                            }

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>

                    <?php 
                        $meta_key = 'instructors';
                        $redirect_link = currentUrl(true);

                        if(isset($_POST['submit']) && $rendered_view == 'form-instructors'){
                            /* Delete existing meta */
                            $saved_users = get_post_meta($post_id, $meta_key);

                            foreach($saved_users as $saved_user){
                                delete_post_meta($post_id, $meta_key, $saved_user);
                            }

                            /* Save Submitted User Selection */
                            $submitted_users = $_POST['users'];

                            foreach($submitted_users as $submitted_user){
                                add_post_meta( $post_id, $meta_key, $submitted_user );
                            }

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }

                        /* Get Saved Users */
                        $saved_users = get_post_meta($post_id, $meta_key);

                        /* Get All Users */
                        $blogusers = get_users( 'blog_id=1&orderby=nicename&role=subscriber' );
                    ?>
                        <p class="txt-sm txt-medium margin-b-10">
                            Search & assign Instructors to this course
                        </p>
                        <select class="select-collaborators full-width" name="users[]" multiple="multiple">
                            <option value="1" >
                                Super Admin
                            </option>
                        <?php foreach( $blogusers as $user ){ ?>

                            <option 
                               value="<?php echo esc_html( $user->ID ); ?>"
                               <?php echo ( in_array($user->ID, $saved_users) ) ? "selected" : ""; ?>
                            >
                                <?php echo esc_html( $user->user_login ); ?>
                            </option>

                        <?php } ?>
                        </select>
 
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" value="Submit" name="submit"  class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Location -->
<div class="modal fade font-main filter-modal" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-location'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Location</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Meta Key */
                        $tax_types = array(
                            array(
                                'name' => 'Location',
                                'slug' => 'state',
                                'hierachical' => false,
                            ),
                        );

                        $redirect_link = currentUrl(true);

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */

                        if(isset($_POST['submit']) && $rendered_view == 'form-location'){

                            /* Return Terms assigned to Post and remove all terms */
                            foreach($tax_types as $tax_type){
                                
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids"));
                                
                                foreach($term_list as $remove_id){
                                    wp_remove_object_terms( $post_id, $remove_id, $tax_type['slug'] );
                                }
                            }
                            
                            /* Save terms to post */
                            foreach($tax_types as $tax_type){
                                wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug']);
                            }

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>

                    <!-- Terms -->
                    <div class="txt-normal-s">
                    <?php foreach($tax_types as $tax_type){ ?>

                        <?php if( $tax_type['hierachical']){ ?>

                            <!-- Tax Title -->
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_type['name']; ?>
                            </h2>

                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <?php if( $parent == 0 ){ ?>

                                    <div class="padding-b-20">
                                        <div class="txt-medium txt-color-dark margin-b-15">
                                            <?php echo $term_name; ?>
                                        </div>
                                        <?php
                                            foreach ($terms as $child_term) {
                                                // Check and see if the term is a top-level parent. If so, display it.
                                                $child_parent = $child_term->parent;
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name

                                                if( $child_parent == $term_id ){
                                        ?>
                                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                    <input
                                                        class="margin-r-5"
                                                        type="checkbox" 
                                                        value="<?php echo $child_term_id ?>" 
                                                        name="<?php echo $tax_type['slug'] ?>[]" 
                                                        <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                    >
                                                    <span class="bg-label padding-l-5">
                                                        <?php echo $child_term_name; ?>
                                                    </span>
                                                </label>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </div>

                                <?php } ?>

                            <?php } ?>

                        <?php } else { ?>

                            <!-- Tax Title -->
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_type['name']; ?>
                            </h2>

                            <div class="padding-b-20">
                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                    <input
                                        class="margin-r-5"
                                        type="checkbox" 
                                        value="<?php echo $term_id ?>" 
                                        name="<?php echo $tax_type['slug'] ?>[]" 
                                        <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                    >
                                    <span class="bg-label padding-l-5">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>

                            <?php } ?>
                            </div>
                        <?php } ?>

                    <?php } ?>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" value="Submit" name="submit"  class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Application Method -->
<div class="modal fade font-main filter-modal" id="applicationMethodModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-application-method'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Select Application Method</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                        $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                        $howToApply = get_post_meta( $post_id, 'how-to-apply', true );


                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-application-method'){

                            /* Get Post Name */
                            $applicationMethod = sanitize_text_field( $_POST['application-method'] );
                            $applicationLink = sanitize_text_field( $_POST['3rd-party-application-link'] );
                            $howToApply = wp_kses_post( $_POST['how-to-apply'] );

                            update_post_meta( $post_id, 'application-method', $applicationMethod );
                            update_post_meta( $post_id, '3rd-party-application-link', $applicationLink );
                            update_post_meta( $post_id, 'how-to-apply', $howToApply );


                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Job Type -->
                        <div class="form-item">
                            <select name="application-method" required>
                                <option value="manual" <?php echo ($applicationMethod == 'manual')? "selected" : "" ?>>
                                    Manual Application
                                </option>
                                <option value="3rd-party" <?php echo ($applicationMethod == '3rd-party')? "selected" : "" ?>>
                                    3rd Party Site
                                </option>
                                <!--<option value="saedconnect" <?php echo ($applicationMethod == 'saedconnect')? "selected" : "" ?>>
                                    Recieve Applications through SAEDConnect
                                </option>-->
                            </select>
                        </div>
                        
                        <!-- Manual Application -->
                        <div class="conditional" data-condition="['manual'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Enter detailed instructions on how applicants can submit their application for this job.
                            </p>
                            <div class="form-item">
                                <textarea class="editor" name="how-to-apply" id="" cols="30" rows="8"><?php echo $howToApply ?></textarea>
                            </div>
                        </div>
                        <!-- 3rd Party -->
                        <div class="conditional" data-condition="['3rd-party'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Enter the external application link for this job.
                            </p>
                            <div class="form-item">
                                <input 
                                    type="url" 
                                    name="3rd-party-application-link" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $applicationLink ?>"
                                >
                            </div>
                        </div>
                        <!-- SAEDConnect -->
                        <div class="conditional" data-condition="['saedconnect'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Letting SAEDconnect handle the application process gives you access to the Application Manager. The Application gives you access to a robust set of tools, crafted to give you control and allow you seamlessly manage job applications.
                            </p>
                            <p class="txt-normal-s txt-medium margin-b-20">
                                 Selecting this option requires paying a fee.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Application Requirements -->
<div class="modal fade font-main filter-modal" id="applicationReqirementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-job-requirements'; ?>" method="post">
                <?php
                    /* Add/Edit Job (Specification) */

                    /* Get Post data */
                    $jobSummary = get_post_meta( $post->ID, 'job-summary', true );
                    $jobDescription = get_post_meta( $post->ID, 'job-description', true );
                    $paymentType = get_post_meta( $post->ID, 'payment-type', true );
                    $offerAmount = get_post_meta( $post->ID, 'offer-amount', true );
                    $offerAmountConfidential = get_post_meta( $post->ID, 'offer-amount-confidential', true );
                    $commissionAmount = get_post_meta( $post->ID, 'commission-amount', true );
                    $commissionAmountConfidential = get_post_meta( $post->ID, 'commission-amount-confidential', true );

                    /*
                    *
                    * Save / Retrieve Form Data
                    *
                    */
                    if($_POST && $rendered_view == 'form-job-requirements'){

                        /* Get Post Name */
                        print_r($_POST);
                        //$serialized_data = maybe_serialize($_POST);

                        /* Update Meta */
                        //update_post_meta( $post_id, 'assigned-tasks', $serialized_data );

                        /* Redirect */
                        //printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                    }
                ?>

                <?php 
                    $unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'assigned-tasks', true ) );

                    /* Return Terms assigned to Post */
                    $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                    /*
                    *
                    * Populate Form Data from Terms
                    *
                    */
                ?>
                <div class="modal-header padding-lr-40">
                    <h5 class="modal-title" id="exampleModalLabel">Select Application Requirements</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-40">
                    <div class="accordion" id="jobRequiremenetAccordion">
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementWork" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Work profile
                                </a>
                            </div>
                            <div id="applicationReqirementWork" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-1" 
                                                name="job-requirements[]" 
                                                value="personal-profile"
                                            >
                                            <label 
                                                for="job-requirements-1" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Personal Information
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-1" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-1" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-2" 
                                                name="job-requirements[]" 
                                                value="cover-letter"
                                            >
                                            <label 
                                                for="job-requirements-2" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Cover Letter 
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-2" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-2" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-3" 
                                                name="job-requirements[]" 
                                                value="video-cv"
                                            >
                                            <label 
                                                for="job-requirements-3" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Video CV
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-3" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-3" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-4" 
                                                name="job-requirements[]" 
                                                value="education"
                                            >
                                            <label 
                                                for="job-requirements-4" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Education
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-4" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-4" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-5" 
                                                name="job-requirements[]" 
                                                value="experience"
                                            >
                                            <label 
                                                for="job-requirements-5" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Experience
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-5" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-5" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-6" 
                                                name="job-requirements[]" 
                                                value="skills"
                                            >
                                            <label 
                                                for="job-requirements-6" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Skills
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-6" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-6" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-7" 
                                                name="job-requirements[]" 
                                                value="capabilities"
                                            >
                                            <label 
                                                for="job-requirements-7" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Capabilities
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-7" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-7" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementBusiness" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Business
                                </a>
                            </div>
                            <div id="applicationReqirementBusiness" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-8" 
                                                name="job-requirements[]" 
                                                value="business"
                                            >
                                            <label 
                                                for="job-requirements-7" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Must have a Business
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-8" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-8" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementProject" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Project
                                </a>
                            </div>
                            <div id="applicationReqirementProject" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-9" 
                                                name="job-requirements[]" 
                                                value="capabilities"
                                            >
                                            <label 
                                                for="job-requirements-9" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Must have created a Project
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-9" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-9" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-40">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    /* Delete & Return */
    if( $rendered_view == 'form-delete'){
        /* Delete Post */
        wp_delete_post($post_id);

        /* Redirect */
        printf('<script>window.location.replace("%s")</script>', $dashboard_management_link);
    }
?>

<?php
    /* Publish / Unpublish & Return */
    if( $rendered_view == 'publication'){
        /* Meta value to save */
        $value = "user_published";

        /* Get saved meta */
        $saved_meta = get_post_meta( $post_id, $publication_key, true );

        if ( $saved_meta ) //If published, Unpublish
            delete_post_meta( $post_id, $publication_key );
        else //If Unpublished, Publish
            update_post_meta( $post_id, $publication_key, $value );

        /* Redirect */
        printf('<script>window.location.replace("%s")</script>', $redirect_link);
    }
?>

<?php 
    if( $rendered_view == 'form-nysc-accreditation' ){
            
        if($_POST){
            /* Publish / Unpublish & Return */
            /* Meta value to save */
            $value = 'user_published';

            foreach ($_POST['state'] as $term) { //Cycle through terms, one at a time
                /* Get saved meta */
                $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term, true );

                if ( $saved_meta ) //If published, Unpublish
                    delete_post_meta( $post_id, $accreditation_key.'_'.$term );
                else //If Unpublished, Publish
                    update_post_meta( $post_id, $accreditation_key.'_'.$term, $value );
            }

            /* Redirect */
            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
        } else {
            /* Unpublish & Return */
            /* Meta value to save */
            $value = 'user_published';
            $term = $_GET['id'];

            /* Get saved meta */
            $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term, true );

            if ( $saved_meta ) //If published, Unpublish
                delete_post_meta( $post_id, $accreditation_key.'_'.$term );
            else //If Unpublished, Publish
                update_post_meta( $post_id, $accreditation_key.'_'.$term, $value );
            
            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
        }

    } 
?>



<?php } ?>




<!-- Admin Action -->
<?php if( $current_user->ID == 1){ ?>

    <?php
        /* Admin Publish / Unpublish & Return */
        if( $rendered_view == 'form-admin-approval'){
            /* Meta value to save */
            

            /* Get saved meta */
            $saved_meta = get_post_meta( $post_id, $publication_key, true );

            if ( $saved_meta == 'user_published' ) //If Unpublished, Published
                $value = "admin_published";
            else //If Published, Unpublish
                $value = "user_published";
                
            update_post_meta( $post_id, $publication_key, $value );

            /* Redirect */
            printf('<script>window.location.replace("%s")</script>', $redirect_link);
        }
                                  
                                  
    ?>

    <!-- Admin Message Modal -->
    <div class="modal fade font-main filter-modal" id="adminMessageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(true).'?view=form-admin-message'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Send Admin Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            /* Meta Key */
                            $redirect_link = currentUrl(true);

                            /*
                            *
                            * Send Message
                            *
                            */
                            if($_POST && $rendered_view == 'form-admin-message'){

                                /* Message Data */
                                $sender = $current_user->ID;
                                $receiver = $post_author_id;
                                $parent_message = 0;
                                $type = 'admin';
                                $subject = sanitize_text_field( $_POST['subject'] );
                                $message = wp_kses_post( $_POST['message'] );
                                $connected_post = $post_id;
                                $connected_post_site = get_current_blog_id();
                                $read_status_sender = 'unread';
                                $read_status_receiver = 'unread';

                                $message_db->insert( 
                                    'messages', 
                                    array( 
                                        "sender" => $sender,
                                        "receiver" => $receiver,
                                        "parent_message" => $parent_message,
                                        "type" => $type,
                                        "subject" => $subject,
                                        "content" => $message,
                                        "connected_post" => $connected_post,
                                        "connected_post_site" => $connected_post_site,
                                        "read_status_sender" => $read_status_sender,
                                        "read_status_receiver" => $read_status_receiver,
                                    ), 
                                    array( "%d", "%d", "%d", "%s", "%s", "%s", "%d", "%d", "%s", "%s" ) 
                                );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                            }
                        ?>

                        <!-- Title -->
                        <div class="form-item">
                            <label for="subject">
                                Subject
                            </label>
                            <input 
                                type="text" 
                                name="subject"
                                required
                            >
                        </div>

                        <!-- Summary -->
                        <div class="form-item">
                            <label for="message">
                                Your Message
                            </label>
                            <textarea class="editor" name="message" cols="30" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- Admin Deny Message Modal -->
    <div class="modal fade font-main filter-modal" id="adminDenyApprovalMessageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(true).'?view=form-admin-message'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Send Admin Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            /* Meta Key */
                            $redirect_link = currentUrl(true);

                            /*
                            *
                            * Send Message
                            *
                            */
                            if($_POST && $rendered_view == 'form-admin-message'){

                                /* Message Data */
                                $sender = $current_user->ID;
                                $receiver = $post_author_id;
                                $parent_message = 0;
                                $type = 'admin';
                                $subject = sanitize_text_field( $_POST['subject'] );
                                $message = wp_kses_post( $_POST['message'] );
                                $connected_post = $post_id;
                                $connected_post_site = get_current_blog_id();
                                $read_status_sender = 'unread';
                                $read_status_receiver = 'unread';

                                $message_db->insert( 
                                    'messages', 
                                    array( 
                                        "sender" => $sender,
                                        "receiver" => $receiver,
                                        "parent_message" => $parent_message,
                                        "type" => $type,
                                        "subject" => $subject,
                                        "content" => $message,
                                        "connected_post" => $connected_post,
                                        "connected_post_site" => $connected_post_site,
                                        "read_status_sender" => $read_status_sender,
                                        "read_status_receiver" => $read_status_receiver,
                                    ), 
                                    array( "%d", "%d", "%d", "%s", "%s", "%s", "%d", "%d", "%s", "%s" ) 
                                );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                            }
                        ?>

                        <!-- Title -->
                        <div class="form-item">
                            <label for="subject">
                                Subject
                            </label>
                            <input 
                                type="text" 
                                name="subject"
                                required
                                value="Verification Approval Denied for <?php echo $post_title ?>"
                            >
                        </div>

                        <!-- Summary -->
                        <div class="form-item">
                            <label for="message">
                                Your Message
                            </label>
                            <textarea class="editor" name="message" cols="30" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php } ?>



<!-- Message Modal -->
<div class="modal fade font-main filter-modal" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-message'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Send a Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Meta Key */
                        $redirect_link = currentUrl(true);
                        global $wpdb; //Include WP Global Object
                        $message_db = new wpdb('root','umMv65ekyMRxfNfm','messages','localhost');

                        /*
                        *
                        * Send Message
                        *
                        */
                        if($_POST && $rendered_view == 'form-message'){
                            
                            /* Message Data */
                            $sender = $current_user->ID;
                            $receiver = $post_author_id;
                            $parent_message = 0;
                            $type = 'user';
                            $subject = sanitize_text_field( $_POST['subject'] );
                            $message = wp_kses_post( $_POST['message'] );
                            $connected_post = $post_id;
                            $connected_post_site = get_current_blog_id();
                            $read_status_sender = 'unread';
                            $read_status_receiver = 'unread';
                            
                            $message_db->insert( 
                                'messages', 
                                array( 
                                    "sender" => $sender,
                                    "receiver" => $receiver,
                                    "parent_message" => $parent_message,
                                    "type" => $type,
                                    "subject" => $subject,
                                    "content" => $message,
                                    "connected_post" => $connected_post,
                                    "connected_post_site" => $connected_post_site,
                                    "read_status_sender" => $read_status_sender,
                                    "read_status_receiver" => $read_status_receiver,
                                ), 
                                array( "%d", "%d", "%d", "%s", "%s", "%s", "%d", "%d", "%s", "%s" ) 
                            );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>

                    <!-- Title -->
                    <div class="form-item">
                        <label for="subject">
                            Subject
                        </label>
                        <input 
                            type="text" 
                            name="subject"
                            required
                        >
                    </div>
                    
                    <!-- Summary -->
                    <div class="form-item">
                        <label for="message">
                            Your Message
                        </label>
                        <textarea class="editor" name="message" cols="30" rows="4"></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Manual/3rd Party Application Form -->
<div class="modal fade font-main filter-modal" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="AddPaymentoutcomeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php 
                if($_POST && $rendered_view == 'form-subscribe'){
                    $fullName = sanitize_text_field( $_POST['full-name'] );
                    $email = sanitize_text_field( $_POST['email'] );
                    $phone = sanitize_text_field( $_POST['phone'] );
                    $state = sanitize_text_field( $_POST['state'] );
                    $jobFunction ="";
                    $capabilities ="";
                    
                    /* Create application status*/
                    $application_db->insert( 
                        $table, 
                        array( 
                            "post_id" => $post_id,
                            "post_type" => $post_type,
                            "user_id" => $current_user->ID,
                            "status" => "external",
                            "name" => $fullName,
                            "email" => $email,
                            "location" => $state,
                            "phone" => $phone,
                            "job_function" => $jobFunction,
                            "job_capabilities" => $capabilities,

                        ), 
                        array( "%d", "%s", "%d", "%s", "%s", "%s", "%s", "%s", "%s", "%s" ) 
                    );
                    
                    /* Redirect */
                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                }
            ?>
            
            <form class="signup-form" method="post" action="<?php echo currentUrl(true).'?view=form-subscribe'; ?>">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Apply for <?php echo $post_title; ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <div class="form margin-b-40">
                        <!-- Name -->
                        <div class="form-item">
                            <label for="name">
                                Name
                            </label>
                            <input type="text" name="full-name" id="full-name">
                        </div>
                        <!-- Email -->
                        <div class="form-item">
                            <label for="email">
                                Email
                            </label>
                            <input type="email" name="email" id="email" value="<?php echo $current_user->user_email ?>">
                        </div>
                        <!-- Phone -->
                        <div class="form-item">
                            <label for="phone">
                                Phone
                            </label>
                            <input type="telephone" name="phone" id="phone">
                        </div>
                        <!-- State -->
                        <div class="form-item">
                            <label for="state">
                                State of Residence
                            </label>
                            <select name="state" id="state">
                                <option value="" selected disabled hidden>- Select -</option>
                                <option value="Abuja FCT">Abuja FCT</option>
                                <option value="Abia">Abia</option>
                                <option value="Adamawa">Adamawa</option>
                                <option value="Akwa Ibom">Akwa Ibom</option>
                                <option value="Anambra">Anambra</option>
                                <option value="Bauchi">Bauchi</option>
                                <option value="Bayelsa">Bayelsa</option>
                                <option value="Benue">Benue</option>
                                <option value="Borno">Borno</option>
                                <option value="Cross River">Cross River</option>
                                <option value="Delta">Delta</option>
                                <option value="Ebonyi">Ebonyi</option>
                                <option value="Edo">Edo</option>
                                <option value="Ekiti">Ekiti</option>
                                <option value="Enugu">Enugu</option>
                                <option value="Gombe">Gombe</option>
                                <option value="Imo">Imo</option>
                                <option value="Jigawa">Jigawa</option>
                                <option value="Kaduna">Kaduna</option>
                                <option value="Kano">Kano</option>
                                <option value="Katsina">Katsina</option>
                                <option value="Kebbi">Kebbi</option>
                                <option value="Kogi">Kogi</option>
                                <option value="Kwara">Kwara</option>
                                <option value="Lagos">Lagos</option>
                                <option value="Nassarawa">Nassarawa</option>
                                <option value="Niger">Niger</option>
                                <option value="Ogun">Ogun</option>
                                <option value="Ondo">Ondo</option>
                                <option value="Osun">Osun</option>
                                <option value="Oyo">Oyo</option>
                                <option value="Plateau">Plateau</option>
                                <option value="Rivers">Rivers</option>
                                <option value="Sokoto">Sokoto</option>
                                <option value="Taraba">Taraba</option>
                                <option value="Yobe">Yobe</option>
                                <option value="Zamfara">Zamfara</option>
                                <option value="Outside Nigeria">Outside Nigeria</option>
                            </select>
                        </div>
                        <!-- Program Name -->
                        <input type="text" name="post-name" id="post-name" value="<?php echo $post_title; ?>" hidden>

                        <div class="status"></div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" id="submit-btn" class="submit-btn btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
            <script>
                $(document).ready(function() {

                });
            </script>
        </div>
    </div>
</div>
