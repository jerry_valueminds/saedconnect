<?php if( $rendered_view == 'apply' ){ ?>

<div class="container-wrapper padding-tb-40 bg-grey">
    <div class="row">
        <div class="col-md-8 mx-auto text-center">
            <h1 class="txt-xlg">
                <?php the_title(); ?> Application
            </h1>
        </div>
    </div>
</div>
<section class="container-wrapper padding-t-40 padding-b-80">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <?php
                $current_user = wp_get_current_user();
                $check = array();
             ?>    

            <?php switch_to_blog(109); ?>

            <?php 
                $reqCV = array(

                    array(
                        'id' => 31,
                        'name' => 'Education & Training',
                        'link' => 'https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=31&cv-part=Education%20&%20Training',
                    ),

                    array(
                        'id' => 34,
                        'name' => 'Skills',
                        'link' => 'https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=34&cv-part=Skills',
                    ),
                );
            ?>
               
               
            <!-- Personal Information -->
            <div>    
            <?php                             
                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search
                        array(

                            'key' => 'created_by', 'value' => $current_user->ID //Current logged in user
                        )
                    )
                );

                /* Check Work Profile */
                $profile_count = GFAPI::count_entries( 84, $search_criteria );
                /* Get Entries */
                $user_entries = GFAPI::get_entries( 84, $search_criteria );

                if( $profile_count ){
                    $check[] = 'complete';

                    $gender = rgar( $user_entries[0], 2 );
                    $dob = rgar( $user_entries[0], 3 );
                    $nationality = rgar( $user_entries[0], 4 );
                    $location = rgar( $user_entries[0], 6 );
            ?>

                    <div class="padding-b-10">
                        <div class="txt-normal-s txt-medium">
                            <a 
                               class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                               data-toggle="collapse" 
                               href="#collapsePersonalInfo" aria-expanded="false" aria-controls="collapseOne"
                            >
                                <span>
                                    Personal Information
                                    <i class="txt-color-green fa fa-check"></i>
                                </span>
                            </a>
                        </div>
                        <div id="collapsePersonalInfo" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                            <div class="border-o-1 border-color-darkgrey padding-o-15">
                                <article class="txt-normal-s">
                                    <p class="padding-b-10">
                                        Gender: <?php echo $gender ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Date of Birth: <?php echo $dob ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Nationality: <?php echo $nationality ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Location: <?php echo $location ?>
                                    </p>
                                </article>
                                <div class="text-right txt-sm">
                                    <a href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information" class="btn btn-blue txt-normal-s txt-medium">
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
            <?php } else { ?>
                <?php $check[] = 'incomplete'; ?>

                    <p class="row align-items-center margin-b-10">
                        <span class="col-8">
                            Personal Information
                            <i class="txt-color-red fa fa-times"></i>
                        </span> 
                        <span class="col-md-4 text-right txt-sm">
                            <a href="https://www.saedconnect.org/competency-profile/education-experience-profile/" class="txt-underline txt-color-dark txt-medium">
                                Complete
                            </a>
                        </span>
                    </p>

            <?php } ?>
            </div>
            
            <!-- CV + Cover Letter -->
            <div>    
            <?php                             
                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search
                        array(

                            'key' => 'created_by', 'value' => $current_user->ID //Current logged in user
                        )
                    )
                );

                /* Check Work Profile */
                $profile_count = GFAPI::count_entries( 84, $search_criteria );
                /* Get Entries */
                $user_entries = GFAPI::get_entries( 84, $search_criteria );

                if( $profile_count ){
                    $check[] = 'complete';

                    $gender = rgar( $user_entries[0], 2 );
                    $dob = rgar( $user_entries[0], 3 );
                    $nationality = rgar( $user_entries[0], 4 );
                    $location = rgar( $user_entries[0], 6 );
            ?>

                    <div class="padding-b-10">
                        <div class="txt-normal-s txt-medium">
                            <a 
                               class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                               data-toggle="collapse" 
                               href="#collapseCVCoverLetter" aria-expanded="false" aria-controls="collapseOne"
                            >
                                <span>
                                    CV + Cover Letter
                                    <i class="txt-color-green fa fa-check"></i>
                                </span>
                            </a>
                        </div>
                        <div id="collapseCVCoverLetter" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                            <div class="border-o-1 border-color-darkgrey padding-o-15">
                                <article class="txt-normal-s">
                                    <p class="padding-b-10">
                                        Gender: <?php echo $gender ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Date of Birth: <?php echo $dob ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Nationality: <?php echo $nationality ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Location: <?php echo $location ?>
                                    </p>
                                </article>
                                <div class="text-right txt-sm">
                                    <a href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information" class="btn btn-blue txt-normal-s txt-medium">
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
            <?php } else { ?>
                <?php $check[] = 'incomplete'; ?>

                    <p class="row align-items-center margin-b-10">
                        <span class="col-8">
                            Personal Information
                            <i class="txt-color-red fa fa-times"></i>
                        </span> 
                        <span class="col-md-4 text-right txt-sm">
                            <a href="https://www.saedconnect.org/competency-profile/education-experience-profile/" class="txt-underline txt-color-dark txt-medium">
                                Complete
                            </a>
                        </span>
                    </p>

            <?php } ?>
            </div>
                        
            <!-- Video CV -->
            <div>    
            <?php                             
                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search
                        array(

                            'key' => 'created_by', 'value' => $current_user->ID //Current logged in user
                        )
                    )
                );

                /* Check Work Profile */
                $profile_count = GFAPI::count_entries( 84, $search_criteria );
                /* Get Entries */
                $user_entries = GFAPI::get_entries( 84, $search_criteria );

                if( $profile_count ){
                    $check[] = 'complete';

                    $gender = rgar( $user_entries[0], 2 );
                    $dob = rgar( $user_entries[0], 3 );
                    $nationality = rgar( $user_entries[0], 4 );
                    $location = rgar( $user_entries[0], 6 );
            ?>

                    <div class="padding-b-10">
                        <div class="txt-normal-s txt-medium">
                            <a 
                               class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                               data-toggle="collapse" 
                               href="#collapseVideoCV" aria-expanded="false" aria-controls="collapseOne"
                            >
                                <span>
                                    Video CV
                                    <i class="txt-color-green fa fa-check"></i>
                                </span>
                            </a>
                        </div>
                        <div id="collapseVideoCV" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                            <div class="border-o-1 border-color-darkgrey padding-o-15">
                                <article class="txt-normal-s">
                                    <p class="padding-b-10">
                                        Gender: <?php echo $gender ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Date of Birth: <?php echo $dob ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Nationality: <?php echo $nationality ?>
                                    </p>
                                    <p class="padding-b-10">
                                        Location: <?php echo $location ?>
                                    </p>
                                </article>
                                <div class="text-right txt-sm">
                                    <a href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information" class="btn btn-blue txt-normal-s txt-medium">
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
            <?php } else { ?>
                <?php $check[] = 'incomplete'; ?>

                    <p class="row align-items-center margin-b-10">
                        <span class="col-8">
                            Personal Information
                            <i class="txt-color-red fa fa-times"></i>
                        </span> 
                        <span class="col-md-4 text-right txt-sm">
                            <a href="https://www.saedconnect.org/competency-profile/education-experience-profile/" class="txt-underline txt-color-dark txt-medium">
                                Complete
                            </a>
                        </span>
                    </p>

            <?php } ?>
            </div>

            <!-- Education -->
            <div>    
            <?php                             
                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search
                        array(

                            'key' => 'created_by', 'value' => $current_user->ID //Current logged in user
                        )
                    )
                );

                /* Check Work Profile */
                $profile_count = GFAPI::count_entries( 31, $search_criteria );
                /* Get Entries */
                $user_entries = GFAPI::get_entries( 31, $search_criteria );

                if( $profile_count ){
                    $check[] = 'complete';

                    $gender = rgar( $user_entries[0], 2 );
                    $dob = rgar( $user_entries[0], 3 );
                    $nationality = rgar( $user_entries[0], 4 );
                    $location = rgar( $user_entries[0], 6 );
            ?>

                    <div class="padding-b-10">
                        <div class="txt-normal-s txt-medium">
                            <a 
                               class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                               data-toggle="collapse" 
                               href="#collapseEducation" aria-expanded="false" aria-controls="collapseOne"
                            >
                                <span>
                                    Education
                                    <i class="txt-color-green fa fa-check"></i>
                                </span>
                            </a>
                        </div>
                        <div id="collapseEducation" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                            <article class="border-o-1 border-color-darkgrey padding-t-15 padding-lr-15">
                            <?php foreach( $user_entries as $entry ){ ?>

                                <div class="row row-10">
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Degree Attained (e.g BA, BS, JD, PhD)         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            School      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 8 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 9 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Start Month/Year      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 10 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            End Month/Year (Current students: Enter your expected graduation year)     
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 11 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Achievements      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 12 ); ?>
                                        </p>
                                    </div>
                            </div>

                            <?php } ?>
                            </article>
                            <div class="text-right txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                <a href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information" class="btn btn-blue txt-normal-s txt-medium no-m-b">
                                    Edit
                                </a>
                            </div>
                        </div>
                    </div>
                        
            <?php } else { ?>
                <?php $check[] = 'incomplete'; ?>

                    <p class="row align-items-center margin-b-10">
                        <span class="col-8">
                            Education
                            <i class="txt-color-red fa fa-times"></i>
                        </span> 
                        <span class="col-md-4 text-right txt-sm">
                            <a href="https://www.saedconnect.org/competency-profile/education-experience-profile/" class="txt-underline txt-color-dark txt-medium">
                                Complete
                            </a>
                        </span>
                    </p>

            <?php } ?>
            </div>

            <!-- Experience -->
            <div>    
            <?php                             
                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search
                        array(

                            'key' => 'created_by', 'value' => $current_user->ID //Current logged in user
                        )
                    )
                );

                /* Check Work Profile */
                $profile_count = GFAPI::count_entries( 29, $search_criteria );
                /* Get Entries */
                $user_entries = GFAPI::get_entries( 29, $search_criteria );

                if( $profile_count ){
                    $check[] = 'complete';

                    $gender = rgar( $user_entries[0], 2 );
                    $dob = rgar( $user_entries[0], 3 );
                    $nationality = rgar( $user_entries[0], 4 );
                    $location = rgar( $user_entries[0], 6 );
            ?>

                    <div class="padding-b-10">
                        <div class="txt-normal-s txt-medium">
                            <a 
                               class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                               data-toggle="collapse" 
                               href="#collapseExperience" aria-expanded="false" aria-controls="collapseOne"
                            >
                                <span>
                                    Experience
                                    <i class="txt-color-green fa fa-check"></i>
                                </span>
                            </a>
                        </div>
                        <div id="collapseExperience" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                            <article class="border-o-1 border-color-darkgrey padding-t-15 padding-lr-15">
                            <?php foreach( $user_entries as $entry ){ ?>

                                <div class="row row-10">
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Position/Job Title        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Company/Company Website      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 9 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Location     
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 10 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Start Month/Start Year     
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 11 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            End Month/Year      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 12 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Describe what you did/Your Achievements     
                                        </p>
                                        <article class="text-box txt-normal-s">
                                            <?php echo rgar( $entry, 13 ); ?>
                                        </article>
                                    </div>
                                </div>

                            <?php } ?>
                            </article>
                            <div class="text-right txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                <a href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information" class="btn btn-blue txt-normal-s txt-medium no-m-b">
                                    Edit
                                </a>
                            </div>
                        </div>
                    </div>
                        
            <?php } else { ?>
                <?php $check[] = 'incomplete'; ?>

                    <p class="row align-items-center margin-b-10">
                        <span class="col-8">
                            Experience
                            <i class="txt-color-red fa fa-times"></i>
                        </span> 
                        <span class="col-md-4 text-right txt-sm">
                            <a href="https://www.saedconnect.org/competency-profile/education-experience-profile/" class="txt-underline txt-color-dark txt-medium">
                                Complete
                            </a>
                        </span>
                    </p>

            <?php } ?>
            </div>
                    
            <!-- Skills -->
            <div>    
            <?php                             
                $competency_gf_id = 95; //Form ID
                $competency_gv_id = 1189; //Gravity View ID

                $entry_count = 0;

                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search

                    array(

                        'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                    )
                );

                /* Get Competency Entries */
                $competency_entries = GFAPI::get_entries( $competency_gf_id, $search_criteria );

                /* Get Competency Entry Count */
                $competency_entry_count = GFAPI::count_entries( $competency_gf_id, $search_criteria );

                /* Get User Meta to populate Form */
                $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, 'skill_profile', true) );

                if( $unserialized_data ){
                    $check[] = 'complete';

                    $gender = rgar( $user_entries[0], 2 );
                    $dob = rgar( $user_entries[0], 3 );
                    $nationality = rgar( $user_entries[0], 4 );
                    $location = rgar( $user_entries[0], 6 );
            ?>

                    <div class="padding-b-10">
                        <div class="txt-normal-s txt-medium">
                            <a 
                               class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                               data-toggle="collapse" 
                               href="#collapseSkills" aria-expanded="false" aria-controls="collapseOne"
                            >
                                <span>
                                    Skills
                                    <i class="txt-color-green fa fa-check"></i>
                                </span>
                            </a>
                        </div>
                        <div id="collapseSkills" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                            <div class="border-o-1 border-color-darkgrey padding-o-15">
                                <article class="txt-normal-s">
                                    <?php
                                        foreach($unserialized_data as $key => $subterms){
                                            $term = get_term($key);
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <div class="entry">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-15">
                                                <?php echo $term_name ?>      
                                            </p>
                                                <?php 
                                                    foreach($subterms as $subterm){
                                                        $child_term = get_term($subterm);
                                                        $child_term_id = $child_term->term_id; //Get the term ID
                                                        $child_term_name = $child_term->name; //Get the term name
                                                ?>

                                            <div class="row bg-ghostwhite padding-o-10 margin-b-10">
                                                <div class="col-12">
                                                    <p class="txt-sm txt-color-dark txt-medium">
                                                        <?php echo $child_term_name ?>
                                                    </p>                                 
                                                </div>

                                                    <?php 
                                                        foreach( $competency_entries as $competency_entry ){ 
                                                            if(rgar( $competency_entry, 1 ) == $child_term_id){
                                                    ?>
                                                <div class="col-12 padding-t-10 margin-t-10 border-t-1">
                                                    <div class="row row-10">
                                                        <div class="col-md-12 padding-lr-10 padding-b-10">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                What is your experience in this subject?     
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo rgar( $competency_entry, 2 ); ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 padding-lr-10 padding-b-10">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Number of Years of Experience     
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo rgar( $competency_entry, 3 ); ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 padding-lr-10">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                What training models are you open to providing for this subject area?     
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php
                                                                    $field_id = 4; // Update this number to your field id number
                                                                    $field = RGFormsModel::get_field( $competency_gf_id, $field_id );
                                                                    $value = is_object( $field ) ? $field->get_value_export( $competency_entry ) : '';
                                                                    echo $value;
                                                                ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                    <?php 
                                                            }
                                                        }
                                                    ?>

                                            </div>

                                                <?php } ?>
                                        </div>

                                    <?php } ?>
                                </article>
                                <div class="text-right txt-sm">
                                    <a href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information" class="btn btn-blue txt-normal-s txt-medium">
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
            <?php } else { ?>
                <?php $check[] = 'incomplete'; ?>

                    <p class="row align-items-center margin-b-10">
                        <span class="col-8">
                            Skills Profile
                            <i class="txt-color-red fa fa-times"></i>
                        </span> 
                        <span class="col-md-4 text-right txt-sm">
                            <a href="https://www.saedconnect.org/competency-profile/education-experience-profile/" class="txt-underline txt-color-dark txt-medium">
                                Complete
                            </a>
                        </span>
                    </p>

            <?php } ?>
            </div>

            <?php restore_current_blog(); ?>
            
            <!-- Capabilities -->
            <div>    
            <?php                             
                $competency_gf_id = 12; //Form ID
                $competency_gv_id = 81; //Gravity View ID

                $entry_count = 0;

                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search

                    array(

                        'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                    )
                );

                /* Get Competency Entries */
                $competency_entries = GFAPI::get_entries( $competency_gf_id, $search_criteria );

                /* Get Competency Entry Count */
                $competency_entry_count = GFAPI::count_entries( $competency_gf_id, $search_criteria );

                /* Get User Meta to populate Form */
                $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, 'capability_profile', true) );

                if( $unserialized_data ){
                    $check[] = 'complete';

                    $gender = rgar( $user_entries[0], 2 );
                    $dob = rgar( $user_entries[0], 3 );
                    $nationality = rgar( $user_entries[0], 4 );
                    $location = rgar( $user_entries[0], 6 );
            ?>

                    <div class="padding-b-10">
                        <div class="txt-normal-s txt-medium">
                            <a 
                               class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                               data-toggle="collapse" 
                               href="#collapseCapabilities" aria-expanded="false" aria-controls="collapseOne"
                            >
                                <span>
                                    Capabilities
                                    <i class="txt-color-green fa fa-check"></i>
                                </span>
                            </a>
                        </div>
                        <div id="collapseCapabilities" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                            <div class="border-o-1 border-color-darkgrey padding-o-15">
                                <article class="txt-normal-s">
                                    <?php
                                foreach($unserialized_data as $key => $subterms){
                                    $term = get_term($key);
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <div class="entry">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-15">
                                        <?php echo $term_name ?>      
                                    </p>
                                        <?php 
                                            foreach($subterms as $subterm){
                                                $child_term = get_term($subterm);
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name
                                        ?>

                                    <div class="row bg-ghostwhite padding-o-10 margin-b-10">
                                        <div class="col-12">
                                            <p class="txt-sm txt-color-dark txt-medium">
                                                <?php echo $child_term_name ?>
                                            </p>                                 
                                        </div>

                                            <?php 
                                                foreach( $competency_entries as $competency_entry ){ 
                                                    if(rgar( $competency_entry, 1 ) == $child_term_id){
                                            ?>
                                        <div class="col-12 padding-t-10 margin-t-10 border-t-1">
                                            <div class="row row-10">
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What is your experience in this subject?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 2 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        Number of Years of Experience     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 3 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What training models are you open to providing for this subject area?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php
                                                            $field_id = 4; // Update this number to your field id number
                                                            $field = RGFormsModel::get_field( $competency_gf_id, $field_id );
                                                            $value = is_object( $field ) ? $field->get_value_export( $competency_entry ) : '';
                                                            echo $value;
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                            <?php 
                                                    }
                                                }
                                            ?>

                                    </div>

                                        <?php } ?>
                                </div>

                            <?php } ?>
                                </article>
                                <div class="text-right txt-sm">
                                    <a href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information" class="btn btn-blue txt-normal-s txt-medium">
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
            <?php } else { ?>
                <?php $check[] = 'incomplete'; ?>

                    <p class="row align-items-center margin-b-10">
                        <span class="col-8">
                            Personal Information
                            <i class="txt-color-red fa fa-times"></i>
                        </span> 
                        <span class="col-md-4 text-right txt-sm">
                            <a href="https://www.saedconnect.org/competency-profile/education-experience-profile/" class="txt-underline txt-color-dark txt-medium">
                                Complete
                            </a>
                        </span>
                    </p>

            <?php } ?>
            </div>
                    
            
                
                
                
            <?php

                //print_r($check);
                if (in_array("incomplete", $check)){

                    echo '<p class="txt-medium text-center txt-color-red padding-o-20 margin-t-40 border-o-1 border-color-darkgrey">You must Complete the parts of your CV highlighted above before you can submit an Application</p>';

                } else {
                    printf('<p class="text-center margin-t-40"><a href="%s?view=application-confirmation" class="btn btn-trans-bw txt-sm">Apply</a></p>', $post_link);
                }

            ?>
        </div>
    </div>
</section>

<?php } ?>