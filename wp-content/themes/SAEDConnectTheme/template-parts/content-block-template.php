<?php
    $group_values = rwmb_meta( 'content-block-group' );


    /* Set Unique Accordion suffix */
    $accordion_suffix = 0;


    if ( ! empty( $group_values ) ) {
        foreach ( $group_values as $group_value ) {
            
            /* Increment Accordion Suffix */
            $accordion_suffix += 1;

            /* Get Content type */
            $content_type = $group_value['select-content-type'];

            
            if($content_type == 'cb-plain-text-meta-box'){ ?>

            <!-- Text content -->
            <div class="col-md-<?php echo $group_value[ 'select-column-width' ]; ?> padding-lr-40">
                <div class="margin-b-40">

                    <article class="text-box">
                        <?php
                                                          
                            echo $group_value['cb-plain-text-meta-box']['cb-plain-text-content'];
                        ?>
                        
                    </article>
                </div>
            </div>

        <?php } elseif($content_type == 'cb-accordion-meta-box'){ ?> 

            <!-- Accordion -->
            <div class="col-md-<?php echo $group_value[ 'select-column-width' ]; ?> padding-lr-40">
                <div class="margin-b-40">
                    <div class="faq-accordion margin-b-20">
                        <div id="accordion-<?php echo $accordion_suffix; ?>">
                            <?php 
                                $values = $group_value['cb-accordion-meta-box']['cb-accordion'];
                                $accordionCounter = 1;

                                foreach ( $values as $value ) { ?>

                                    <div class="card">
                                        <div class="card-header" id="heading-<?php echo $post->ID.'-'.$accordionCounter; ?>">
                                            <h5 class="mb-0">
                                                <button 
                                                   class="btn btn-link collapsed" 
                                                   data-toggle="collapse" 
                                                   data-target="#collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>" 
                                                   aria-expanded="false" 
                                                   aria-controls="collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>"
                                                >
                                                    <?php echo $value['cb-accordion-title']; ?>
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>" class="collapse" aria-labelledby="heading-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>>" data-parent="#accordion-<?php echo $accordion_suffix ?>" style="">
                                            <div class="card-body">
                                                <article class="text-box sm">
                                                    <div class="txt-height-1-5">
                                                        <?php echo $value['cb-accordion-content']; ?>
                                                    </div>
                                                </article>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php $accordionCounter++ ?>
                                    
                                <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php } elseif($content_type == 'cb-button-meta-box'){ ?> 

            <!-- Button Group -->
            <div class="col-md-<?php echo $group_value[ 'select-column-width' ]; ?> padding-lr-40">
               <article class="btn-wrapper margin-b-40">
                    <?php
                        /* Get Buttons */
                        $values = $group_value['cb-button-meta-box']['cb-button'];
                                                                    
                        foreach ( $values as $value ) {
                            if($value['cb-button-type'] == 1){ ?>

                                <a
                                   class="btn btn-trans-green margin-r-10 margin-b-10"
                                   href="<?php echo $value['cb-button-url']; ?>"
                                >
                                    <?php echo $value['cb-button-text']; ?>
                                </a>

                            <?php }elseif($value['cb-button-type'] == 2){ ?>
                                <a
                                    class="btn btn-green margin-r-10 margin-b-10"
                                    href="<?php echo $value['cb-button-url']; ?>"
                                >
                                    <?php echo $value['cb-button-text']; ?>
                                </a>
                                
                            <?php }elseif($value['cb-button-type'] == 3){ ?>
                                <a
                                    class="btn btn-black icon margin-r-10 margin-b-10"
                                    href="<?php echo $value['cb-button-url']; ?>"
                                >
                                    <?php echo $value['cb-button-text']; ?>
                                </a>
                            <?php }else{ ?>
                                <a 
                                   class="btn btn-trans-green margin-r-10 margin-b-10" 
                                   href="<?php echo $value['cb-button-url']; ?>"
                                >
                                    <?php echo $value['cb-button-text']; ?>
                                </a>
                            <?php }
                        }
                    ?>
                </article>
            </div>

        <?php } elseif($content_type == 'cb-heading-meta-box'){ ?> 

            <!-- Heading -->
            <div class="col-md-<?php echo $group_value[ 'select-column-width' ]; ?> padding-lr-40">
               <div class="margin-b-20">
                    <h2 class="article-header padding-b-20 border-b-1 border-color-darkgrey">
                        <?php
                            echo $group_value['cb-heading-meta-box']['cb-heading-heading'];
                        ?>
                    </h2>
                </div>
            </div>
            
        <?php } elseif($content_type == 'cb-image-meta-box'){ ?> 

            <!-- Image -->
            <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-lr-40 margin-b-40">
                <?php 
                    $image = 0;

                    $image_ids = isset( $group_value['cb-image-meta-box']['cb-image-content'] ) ? $group_value['cb-image-meta-box']['cb-image-content'] : array();

                    foreach ( $image_ids as $image_id ) {
                        $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                    }
                ?>

                <figure class="image-box">
                    <img src="<?php echo $image['full_url']; ?>" alt="">
                </figure>
            </div>
            
        <?php } elseif($content_type == 'cb-video-meta-box'){ ?> 

            <!-- Video -->
            <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-lr-40 margin-b-40"> 
                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $group_value['cb-video-meta-box']['cb-video-id']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

        <?php } elseif($content_type == 'gi-article-meta-box'){ ?> 

            <!-- Article -->
            <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-lr-40 margin-b-30">
                <?php

                    $images = rwmb_meta( 'article-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                ?>

                <a class="featured-article" href="<?php echo rwmb_get_value( 'article-link' ) ?>">
                    <figure class="image-box">
                        <img src="<?php echo $image['full_url']; ?>" alt="">
                        <?php $article_type = rwmb_get_value( 'select-article-type' ); ?>
                        <?php if($article_type == 'podcast'){ ?>
                            <figcaption class="caption-icon caption-podcast">
                                <?php echo rwmb_get_value( 'category-text' ) ?>
                            </figcaption>
                        <?php }elseif($article_type == 'video'){ ?>
                            <figcaption class="caption-icon caption-video">
                                <?php echo rwmb_get_value( 'category-text' ) ?>
                            </figcaption>
                        <?php }else{ ?>
                            <figcaption class="caption">
                                <?php echo rwmb_get_value( 'category-text' ) ?>
                            </figcaption>
                        <?php } ?>
                    </figure>
                    <h4 class="title">
                        <?php the_title(); ?>
                    </h4>
                    <p class="date">
                        <?php rwmb_the_value( 'article-publication-date', array( 'format' => 'F  d\, Y' ) ); ?>
                    </p>
                </a>
            </div>

         <?php } elseif($content_type == 'gi-link-meta-box'){ ?> 

            <!-- Link -->
            <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-lr-40 margin-b-30">
                <?php

                    $images = rwmb_meta( 'link-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                ?>

                <figure>
                    <img src="<?php echo $image['full_url']; ?>" alt="">
                </figure>
                <figcaption>
                    <h3 class="txt-medium padding-tb-10">
                        <a class="txt-color-blue" href="<?php echo rwmb_get_value( 'link-url' ) ?>"><?php the_title() ?></a>
                    </h3>
                    <p class="txt-normal-s txt-color-lighter">
                        <?php echo rwmb_get_value( 'link-description' ) ?>
                    </p>
                </figcaption>
            </div>       
            
<?php
                } 
        }
    }
?>

<script>
    $(document).ready(function(){

        function getId(url) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = url.match(regExp);

            if (match && match[2].length == 11) {
                return match[2];
            } else {
                return 'error';
            }
        }

        var myId;


        var myUrl = '<?php echo $link ?>';
        myId = getId(myUrl);

        $('#myId').html(myId);

        $('#video').html('<iframe width="100%" height="380" src="//www.youtube.com/embed/' + myId + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');        
    });
</script>