<?php
    /* Get Track ID */
    $track_id = get_the_ID();
    $track_title = rwmb_meta( 'track-short-name' );
    $track_faq = rwmb_meta( 'program-faq' );
    $track_guide_url = rwmb_meta( 'program-guide-url' );
    $track_forum_url = rwmb_meta( 'program-community-forum' );
?>
<div class="padding-t-10">
    <div class="container-wrapper bg-ash">
        <ul class="menu">
            <li>
                <a href="<?php echo get_the_permalink() ?>">
                    Home
                </a>
            </li>
            <li>
                <a href="<?php echo get_the_permalink().'?view=curriculum' ?>" class="active">
                    Fire Side Chat
                </a>
            </li>
            <li>
                <a href="<?php echo $track_guide_url ?>">
                    Guides
                </a>
            </li>
            <li>
                <a href="<?php echo $track_forum_url ?>">
                    Mentor Hub
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="container-wrapper padding-tb-40"> 
    <div class="row">
        <div class="col-md-8 margin-b-40">
            <h2 class="txt-2-4em txt-light txt-height-1-2 padding-b-30">
                The <?php echo $track_title; ?> Track Curriculum
            </h2>
            <article class="txt-normal-s txt-height-1-7">
                <?php echo rwmb_meta( 'program-description' ); ?>
            </article>
        </div> 
    </div>  
    <?php
        /* Get Group Values */
        $group_values = rwmb_meta( 'connected-forum-topics-group' );


        if ( ! empty( $group_values ) ) {

            foreach ( $group_values as $group_value ) {
    ?>

            <!-- Article Group -->
            <div class="padding-tb-30 border-t-1 border-color">

            <?php if( $group_value['group-title-url'] ){ ?>

                <h3>
                    <a
                       href="<?php echo $group_value['group-title-url']; ?>"
                       class="txt-color-dark d-flex align-items-center"
                    >
                        <span class="txt-xs padding-r-5">
                            <i class="fa fa-long-arrow-right"></i>
                        </span>
                        <?php echo $group_value['group-title']; ?>
                    </a>
                </h3>

            <?php } else { ?>

                <h3>
                    <a
                       href="<?php echo $group_value['group-title-url']; ?>"
                       class="txt-color-dark d-flex align-items-center"
                    >
                        <span class="txt-xs padding-r-5">
                            <i class="fa fa-long-arrow-right"></i>
                        </span>
                        <?php echo $group_value['group-title']; ?>
                    </a>
                </h3>

            <?php } ?>

            <?php 
                $values = $group_value['forum-topics-group']['forum-topic-item'];

                    if($values){
            ?>
                <ul class="row row-10 icon-list black txt-normal-s margin-t-20 padding-l-20">
                <?php 
                    foreach ( $values as $value ) { 
                ?>

                    <li class="col-md-3 padding-lr-10">
                        <a href="<?php echo $value['forum-topic-url']; ?>">
                            <i class="fa fa-chevron-right"></i>
                            <span>
                                <?php echo $value['forum-topic-title']; ?>
                            </span>
                        </a>
                    </li>

                <?php } ?>

                </ul>

            <?php } ?>

            </div>

        <?php
            }
        }
    ?>
</div>