<?php
     /* Get current User ID */
    $current_user = wp_get_current_user();

    /* Get Program Featured Image */
    $program_images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ) );
    $program_images = reset( $program_images );

    /* Declare Program Information Var */
    $program_id;
    $program_name;
    $program_url;
    $program_theme_color;
    $program_theme_color_alt;

    /* Get Track ID */
    $track_id = get_the_ID();
    $track_title = rwmb_meta( 'track-short-name' );
    $track_faq = rwmb_meta( 'program-faq' );
    $track_mentor_forum_url = rwmb_meta( 'program-mentor-forum' );
    $coursesgroup = rwmb_meta( 'program-course-days' );
    $start_date = strtotime( rwmb_meta( 'program-next-start-date' ) );
    $course_duration = 
        rwmb_meta( 'program-duration' );

    /* Get Form ID from request */
    $gf_Subscription_form_id = rwmb_meta( 'program-form-shortcode' );

    /*
    *==============================================
    *
    *   Get data from Program
    *
    *==============================================
    */
    $connected = new WP_Query( array(
        'relationship' => array(
            'id'   => 'program_to_track',
            'to' => $track_id, // You can pass object ID or full object
        ),
        'nopaging' => true,
    ) );
    while ( $connected->have_posts() ) : $connected->the_post();

        /* Get Program Information */
        $program_id = $post->ID;
        $program_name = get_the_title();
        $program_url = get_the_permalink();
        $program_mentor_forum_url = rwmb_meta( 'skilli-forum-url' );
        $program_theme_color = rwmb_meta( 'skilli-program-theme-color' );
        $program_theme_color_alt = rwmb_meta( 'skilli-program-alt-theme-color' );

    endwhile;
    wp_reset_postdata();

    /* Get Forum Category */
    //$forum_category = rwmb_meta( 'forum_category', array( 'object_type' => 'setting' ), 'my_options' );

    /* Get forum theme color */
    $program_theme_color = rwmb_meta( 'forum-theme-color', array( 'object_type' => 'setting' ), 'my_options' );
    $program_theme_color_alt = rwmb_meta( 'forum-alt-theme-color', array( 'object_type' => 'setting' ), 'my_options' );

    /* Get Action Button theme color */
    $button_bg = rwmb_meta( 'action-btn-background', array( 'object_type' => 'setting' ), 'my_options' );
    $button_color = rwmb_meta( 'action-btn-color', array( 'object_type' => 'setting' ), 'my_options' );

    $program_id = get_current_blog_id();
    switch ($program_id) {
        case '90':
            $program_name = 'The Entrepreneurship Incubator';
            break;
        case '88':
            $program_name = "Job Advisor";
            break;
        case '26':
            $program_name = "Business Clinic";
            break;
        default:
            $program_name = "Not selected";
    }

    /*
    *==============================================
    *
    *   Prepare Messages based on Parent Program 
    *
    *==============================================
    */
    /* subscription message */
    $subscription_message = array(
        /* TEI */
        '90' => 'Where do you go to get expert advice about discovering great ideas, or building your brilliant idea into a thriving business? The Entrepreneurship Incubator mentor hub is the expert support community you always wished you could reach out to when you needed some motivation, advice or expert experience.',
        
        /* JA */
        '88' => 'Where do you go to get expert advice about crafting a great CV, finding exciting job openings, preparing for interviews and taking the right career decisions? The Job Advisor mentor hub is the expert support community you always wished you could reach out to when you needed some motivation, advice or expert experience.',
        
        /* BC */
        '26' => 'Where do you go to get expert advice about solving that challenge or getting that business question answered? The '.$track_title.' mentor hub is the expert support community you always wished you could reach out to when you needed some motivation, advice or expert experience.'
    );

    /* Help Center Links */
    $help_center_link = array(
        /* TEI */
        '90' => 'https://www.saedconnect.org/help-center/forums/forum/the-entrepreneurship-incubator-help-center/',
        
        /* JA */
        '88' => 'https://www.saedconnect.org/help-center/forums/forum/job-advisor-help-center/',
        
        /* BC */
        '26' => 'https://www.saedconnect.org/help-center/forums/forum/business-clinic-help-center/'
    );

    /* How it Works */
    $how_it_works = array(
        /* TEI */
        /* TEI */
                '90' => array(
                    'summary' => 'The Entrepreneurship. Incubator utilizes an innovative model to deliver game changing support to help you start your own business from anywhere you are. Here is how you can plug in.',
                    'items' => array(
                        array(
                            'title' => 'Join a track of your choice',
                            'content' => 'Select the “Get started Button” under the track of your choice and complete the enrolment form. You will get an email within 24-48hrs to confirm your enrolment, based on space availability.'
                        ),
                        array(
                            'title' => 'Get added to your virtual class',
                            'content' => 'All classes are on WhatsApp and are coordinated by strict rules. Details of class rules will be sent to you'
                        ),
                        array(
                            'title' => 'Learn & Engage',
                            'content' => 'Learn from audio, video & text lessons, participate in quizzes & complete your action ta'
                        ),
                        array(
                            'title' => 'Join the Alumni Community',
                            'content' => 'Your learning journey doesnt end after class. We’ll give you 3 month access to our alumni community where you can meet and interact with mentors and other entrepreneurs.'
                        )
                    )
                ),
        
        /* JA */
        '88' => array(
            'summary' => 'The Job Advisor utilizes an innovative model to deliver game changing support to help you get your dream job, from anywhere you are. Here is how you can plug in.',
            'items' => array(
                array(
                    'title' => 'Access the Knowledge Center',
                    'content' => 'Click the "Explore button" under the track to see all the topics covered in the knowledge center under that track.'
                ),
                array(
                    'title' => 'Learn from Mentors',
                    'content' => 'Select a topic of your interest to access & digest the wisdom contributed by all our mentors under that topic.'
                ),
                array(
                    'title' => 'Ask questions in the Mentor Hub',
                    'content' => 'Ask any questions bothering you. Seasoned experts are waiting to provide any clarifications you require to convert lessons learnt into action.'
                ),
                array(
                    'title' => 'Engage till you are satisfied',
                    'content' => 'Push your conversation with the experts until you are completely satisfied.'
                )
            )
        ),
        
        /* BC */
        '26' => array(
            'summary' => 'The Business utilizes an innovative model to help you grow your business and get past any challenges, from anywhere you are. Here is how you can plug in.',
            'items' => array(
                array(
                    'title' => 'Subscribe to the an Expert Group',
                    'content' => 'Join the expert group of your interest.'
                ),
                array(
                    'title' => 'Ask your Question',
                    'content' => 'Ask any questions bothering you. Seasoned experts are waiting to hear from you and give you customized answers that meet your need.'
                ),
                array(
                    'title' => 'Engage till you are satisfied',
                    'content' => 'Push your conversation with the experts until you are completely satisfied.'
                )
            )
        ),
    );

    /* What You get */
    $what_you_get = array(
        /* TEI */
        '90' => array(
            'summary' => 'We are going all out to ensure you get all the support & resources you require to step out, discover great ideas & build an amazing business.',
            'items' => array(
                array(
                    'title' => 'Tap into the wisdom & experience of a lot of mentors',
                    'content' => 'We get a lot of mentors to contribute their expert knowledge as well as their practical experience to each and every topic in the class, so that you can see that topic from different points of view.'
                ),
                array(
                    'title' => 'Get customised answers to your questions',
                    'content' => 'Ask as many questions as you have. The experts in the class will provide answers personalised to your specific questions. You can keep asking questions till you are satisfied, and if you get stuck again, they are always there to help.'
                ),
                array(
                    'title' => 'Get inspired by your peers',
                    'content' => 'Meet, learn from & get inspired by other focused peers in the class. When you are among a company of focused peers, your chances of success increases drastically.'
                ),
                array(
                    'title' => 'Get access to growth support tools, templates & special opportunities.',
                    'content' => 'We continuously seek out and share tools, resources, opportunities and programs for our alumni that would make your journey easier.'
                )
            )
        ),
        
        /* JA */
        '88' => array(
            'summary' => 'We are going all out to ensure you get all the support & resources you require to stand out among your peers and get your dream jobs.',
            'items' => array(
                array(
                    'title' => 'Tap into the wisdom & experience of a lot of mentors',
                    'content' => 'We get a lot of mentors to contribute their expert knowledge as well as their practical experience to each and every topic in the knowledge center, so that you can see that topic from different points of view.'
                ),
                array(
                    'title' => 'Get customised answers to your questions',
                    'content' => 'The experts in the mentor hub will provide answers personalised to your specific questions. You can keep asking questions till you are satisfied, and if you get stuck again, they are always there to help.'
                ),
                array(
                    'title' => 'Get inspired by your peers',
                    'content' => 'Meet, learn from & get inspired by other focused peers in the mentor hub. When you are among a company of focused peers, your chances of success increases drastically.'
                ),
                array(
                    'title' => 'Get access to growth support tools, templates & special opportunities.',
                    'content' => 'We continuously seek out and share tools, resources and programs that would make your journey easier and even negotiate special concessions for members of the mentor Hub.'
                )
            )
        ),
        
        /* BC */
        '26' => array(
            'summary' => 'We are going all out to ensure you get all the support & resources you require to get past any business challenges and grow',
            'items' => array(
                array(
                    'title' => 'Tap into the wisdom & experience of a lot of mentors',
                    'content' => 'We get a lot of mentors to contribute their expert knowledge as well as their practical experience to each and every topic in the knowledge center, so that you can see that topic from different points of view.'
                ),
                array(
                    'title' => 'Get customised answers to your questions',
                    'content' => 'The experts in the mentor hub will provide answers personalised to your specific questions. You can keep asking questions till you are satisfied, and if you get stuck again, they are always there to help.'
                ),
                array(
                    'title' => 'Get inspired by your peers',
                    'content' => 'Meet, learn from & get inspired by other focused peers in the mentor hub. When you are among a company of focused peers, your chances of success increases drastically.'
                ),
                array(
                    'title' => 'Get access to growth support tools, templates & special opportunities.',
                    'content' => 'We continuously seek out and share tools, resources and programs that would make your journey easier and even negotiate special concessions for members of the mentor Hub.'
                )
            )
        ),
    );

    /* CTA Messages */
    $cta_message = array(
        /* TEI */
        '90' => 'Enroll for the '.$track_title.' Track',
        
        /* JA */
        '88' => 'Join the '.$track_title.' Track',
        
        /* BC */
        '26' => 'Subscribe to the '.$track_title.' Track'
    );


    /*
    *==========================================================
    *   Process Form
    *==========================================================
    */
    if( $_GET['form-subscribe'] == 'mailing-list' ){
        /* Get User Display Name */
        switch_to_blog(1);

        $gf_id = 4; //Form ID
        $username_entry_count = 0;

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                )
            )
        );

        /* Get Entries */
        $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

        /* Get GF Entry Count */
        $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


        if($username_entry_count){ //If no entry
            $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
            $email = rgar( $username_entries[0], 2 );
            $phone = rgar( $username_entries[0], 7 );
        }   

        restore_current_blog();

        /* Update Mailing List */
        /* Data */
        $api_key = '0fkxtL9crlJFX7cqCXUV';
        $list = '2Q3cMbTNkgcT2BFUot3few'; //Master
        
        /* Endpoints */
        $subscribe_url = 'http://maylbox.com/subscribe';

        /* Subscribe Body */
        $body = array(
            'name' => $displayname,
            'email' => $current_user->user_email,
            'Program' => $program_name,
            'Track' => $track_title,
            'list' => $list,
            'boolean' => 'true'
        );

        $args = array(
            'body' => $body,
            'timeout' => '5',
            'redirection' => '5',
            'httpversion' => '1.0',
            'blocking' => true,
            'headers' => array(),
            'cookies' => array()
        );

        $response = wp_remote_post( $subscribe_url, $args );
        $http_code = wp_remote_retrieve_response_code( $response );
        $body = wp_remote_retrieve_body( $response );

        //print_r($http_code);
        //print_r($body);
        /* Update Mailing List End */
        
        $redirect_url = get_current_blog_id();
        switch ($program_id) {
            case '90':
                $redirect_url = "https://www.saedconnect.org/tei-mentor-hub/membership-account/membership-checkout/?level=1"; //TEI
                break;
            case '88':
                $redirect_url = "https://www.saedconnect.org/job-advisor-mentor/membership-account/membership-checkout/?level=1"; //JA
                break;
            case '26':
                $redirect_url = "https://www.saedconnect.org/business-clinic-forum/membership-account/membership-checkout/?level=1"; //BC
                break;
            default:
                $redirect_url = currentUrl(true);
        }

        /* Redirect */
        printf('<script>window.location.replace("%s")</script>', $redirect_url);
                                            
                                        
    }
?>

<style>
    .theme-bg{
        background-color: <?php echo $program_theme_color ?>;
    }

    .theme-alt{
        color: <?php echo $program_theme_color_alt ?>;
    }

    .how-it-works-container::before{
        background-color: <?php echo $program_theme_color ?>;
    }
</style>

<!-- Intro -->
<header 
    class="track-header bg-orange-2 txt-color-white text-center"
    style="background-color: <?php echo $program_theme_color ?>; color: <?php echo $program_theme_color_alt ?>;"
>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <a 
               class="txt-sm d-inline-block bg-yellow-dark txt-color-white padding-tb-10 padding-lr-20 margin-b-30"
               href="<?php echo $program_url; ?>"
               style="color:white"
            >
                <?php echo $program_name ?>
                <i class="fa fa-arrow-right"></i>
                <?php echo $track_title ?>
                Track
            </a>
            <h1 class="txt-light txt-3em margin-b-30">
                <?php the_title() ?>
            </h1>
            <h2 class="txt-xlg txt-medium txt-height-1-2 margin-b-30">
                <?php echo rwmb_meta( 'program-name' ); ?>
            </h2>
            <article class="description">
                <p class="txt-normal-s txt-height-1-7">
                    <?php echo rwmb_meta( 'program-description' ); ?>
                </p>
            </article>
            <div class="padding-t-30">
                <span class="d-inline-block padding-lr-30 padding-tb-15 bg-white txt-color-dark">
                    <span class="d-block">
                       <span class="txt-bold">Next Class Date:</span>
                       <?php 
                            echo date('j F Y', $start_date);
                        ?>
                    </span>
                    <span class="txt-lg d-block padding-t-10 margin-t-10 border-t-1 border-color-darkgrey">
                        <span class="txt-medium ">Duration: </span><?php echo $course_duration ?>
                    </span>
                </span>
            </div>
            <div class="margin-t-40">
                <button 
                    class="btn btn-trans-wb txt-sm scroll-to" 
                >
                    <?php echo $cta_message[$program_id]; ?>
                </button>
                <span class="d-none">
                    <?php
                        /* Check if user is loggedin */

                        if ( is_user_logged_in() ) {
                    ?>

                        <!--<a 
                            class="btn btn-trans-wb txt-sm" 
                            href="<?php echo get_the_permalink().'?view=form' ?>"
                        >
                            Continue
                        </a>-->

                        <!--<button 
                            class="btn btn-trans-wb txt-sm scroll-to" 
                        >
                            <?php echo $cta_message[$program_id]; ?>
                        </button>-->

                        <a 
                            class="btn btn-trans-wb txt-sm" 
                            href="<?php echo get_the_permalink().'?form-subscribe=mailing-list' ?>"
                        >
                            <?php echo $cta_message[$program_id]; ?>
                        </a>

                    <?php
                        } else {
                    ?>

                        <a
                           class="btn btn-trans-wb txt-sm margin-r-10"
                           href="https://www.saedconnect.org/login"
                        >
                            Get Started
                        </a>

                    <?php

                        }
                    ?>
                </span>
            </div>
        </div>
    </div>
</header>

 <!-- Courses --> 
 <section class="padding-tb-80">
    <div class="container-wrapper">
        <div class="row">
            <div class="col-lg-10 mx-auto ">
                <h1 class="txt-xxlg txt-height-1-1 margin-b-20 text-center">
                    What you will learn
                </h1>
                <?php
                    
                    $counter = 1;
                    
                    if ( ! empty( $coursesgroup ) ) {
                        foreach ( $coursesgroup as $group_value ) {
                ?>
                    <p class="txt-bold padding-b-15 padding-t-30"><?php echo $group_value[ 'program-course-day-title' ]; ?></p>
                    
                    <?php foreach ( $group_value['program-courses'] as $course ) { ?>
                    <div class="bg-ghostwhite container-wrapper padding-o-40 margin-b-10">
                        <div class="d-flex align-items-center padding-b-15">
                            <p class="txt-2em txt-bold txt-color-yellow col-auto">
                                <?php
                                    $num_padded = sprintf("%02d", $counter);
                                    echo $num_padded; // returns 04
                                ?>
                            </p>
                            <p class="txt-bold padding-l-15"><?php echo $course['program-course-title']; ?></p>
                        </div>
                        <p class="txt-sm txt-color-light"><?php echo $course['program-course-content']; ?></p>
                    </div>
                        <?php $counter++; ?>
                    <?php } ?>
                    
                    
                <?php
                        }
                    }
                ?>
                <div class="text-center margin-t-60">
                    <button class="btn btn-green txt-normal-s scroll-to"><?php echo $cta_message[$program_id]; ?></button>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- How it works -->
<section class="padding-t-80 padding-b-60">
    <div class="container-wrapper">
        <div class="row text-center"> 
            <div class="col-md-10 mx-auto">
                <h1 class="txt-xxlg txt-height-1-1 margin-b-20 ">
                    How it Works
                </h1>
                <p class="txt-normal-s margin-b-60 text-center">
                    <?php echo $how_it_works[$program_id]['summary']; ?>
                </p>
              </div>  
        </div>
    </div>
    <div class="container-wrapper how-it-works-container">
        <div class="row text-center"> 
            <div class="col-md-10 mx-auto">
                <div class="row row-20 ">
                <?php
                    $group_values = $how_it_works[$program_id]['items'];
                    $counter = 1;
                    
                    if ( ! empty( $group_values ) ) {
                        foreach ( $group_values as $group_value ) {
                ?>
                   
                    <div class="col-md padding-lr-20">
                        <div class="how-card">
                            <div class="number">
                                <span>
                                    <?php
                                        echo $counter;
                                        $counter++;
                                    ?>
                                </span>
                            </div>
                            <div class="content">
                                <h3 class="txt-medium txt-height-1-2 margin-b-5">
                                    <?php echo $group_value[ 'title' ]; ?>
                                </h3>
                                <p class="txt-sm txt-height-1-7">
                                    <?php echo $group_value[ 'content' ]; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                <?php
                        }
                    }
                ?>
                </div>
                
                <?php if ( $program_id == 649 ) { ?>
                <div class="margin-t-20">
                    <h2 class="txt-normal-s txt-height-1-7">
                        Get started in the <?php echo $track_title ?> knowledge Center for free.
                    </h2>
                    <div class="margin-t-20">
                        <button class="btn btn-green txt-sm no-m-b scroll-to">
                            <?php echo $cta_message[$program_id]; ?>
                        </button>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="text-center margin-t-60">
        <button class="btn btn-green txt-normal-s scroll-to"><?php echo $cta_message[$program_id]; ?></button>
    </div>
</section>

<!-- What you get -->
<section class="bg-grey padding-t-80 padding-b-60">
    <div class="container-wrapper text-center">
        <div class="row"> 
            <div class="col-md-8 mx-auto">
                <h1 class="txt-xxlg txt-height-1-1 margin-b-20">
                    What you get
                </h1>
                <p class="txt-normal-s margin-b-60">
                    <?php echo $what_you_get[$program_id]['summary']; ?>
                </p>
                <div class="row row-20">
                <?php
                    $group_values = $what_you_get[$program_id]['items'];
                    
                    if ( ! empty( $group_values ) ) {
                        foreach ( $group_values as $group_value ) {
                ?>
                    <div class="col-md-6 padding-lr-20 d-flex">
                        <div class="bg-white collapsible-card">
                            <div class="header">
                                <div class="cta"></div>
                                <div class="title">
                                    <?php echo $group_value[ 'title' ]; ?>
                                </div>
                            </div>
                            <div class="content">
                                <div class="wrapper">
                                    <div class="margin-b-20">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_mentor.png" alt="" width="100">
                                    </div>
                                    <h3 class="txt-medium margin-b-20">
                                        <?php echo $group_value[ 'title' ]; ?>
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        <?php echo $group_value[ 'content' ]; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                <?php
                        }
                    }
                ?>
                </div>
            </div>  
        </div>
        <div class="text-center margin-t-40">
            <button class="btn btn-green txt-normal-s scroll-to"><?php echo $cta_message[$program_id]; ?></button>
        </div>
        <?php if ( $program_id == 649 ) { ?>
        <div class="margin-t-20">
            <h2 class="txt-normal-s txt-height-1-7">
                Get started in the <?php echo $track_title ?> knowledge Center for free.
            </h2>
            <div class="margin-t-20">
                <button class="btn btn-green txt-sm no-m-b scroll-to">
                    <?php echo $cta_message[$program_id]; ?>
                </button>
            </div>
        </div>
        <?php } ?>
    </div>
</section>

<!-- What you get -->
<!--<section class="bg-grey padding-t-80 padding-b-60">
    <div class="container-wrapper text-center">
        <div class="row"> 
            <div class="col-md-8 mx-auto">
                <h1 class="txt-xxlg txt-height-1-1 margin-b-20">
                    What you get
                </h1>
                <p class="txt-normal-s margin-b-60">
                    <?php echo rwmb_meta( 'skilli-program-wyg-brief', array(), $program_id ); ?>
                </p>
                <div class="row row-20">
                <?php
                    $group_values = rwmb_meta( 'skilli_program_wyg_group', array(), $program_id );
                    
                    if ( ! empty( $group_values ) ) {
                        foreach ( $group_values as $group_value ) {
                ?>
                    <div class="col-md-6 padding-lr-20 d-flex">
                        <div class="bg-white collapsible-card">
                            <div class="header">
                                <div class="cta"></div>
                                <div class="title">
                                    <?php echo $group_value[ 'skilli-program-wyg-title' ]; ?>
                                </div>
                            </div>
                            <div class="content">
                                <div class="wrapper">
                                    <div class="margin-b-20">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_mentor.png" alt="" width="100">
                                    </div>
                                    <h3 class="txt-medium margin-b-20">
                                        <?php echo $group_value[ 'skilli-program-wyg-title' ]; ?>
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        <?php echo $group_value[ 'skilli-program-wyg-content' ]; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                <?php
                        }
                    }
                ?>
                </div>
            </div>  
        </div>
        <?php if ( $program_id == 649 ) { ?>
        <div class="margin-t-20">
            <h2 class="txt-normal-s txt-height-1-7">
                Get started in the <?php echo $track_title ?> knowledge Center for free.
            </h2>
            <div class="margin-t-20">
                <button class="btn btn-green txt-sm no-m-b scroll-to">
                    <?php echo $cta_message[$program_id]; ?>
                </button>
            </div>
        </div>
        <?php } ?>
    </div>
</section>-->

<!-- Meet Your mentors -->
<!--
<section class="padding-tb-80">
    <div class="container-wrapper text-center">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <h1 class="txt-xlg txt-height-1-1 margin-b-20">
                    Meet your Mentors
                </h1> 
                <p class="txt-normal-s margin-b-60">
                    We are constantly reaching out to the best of the best professionals & experts to help you make progress.
                </p>
            </div>
        </div>
       
        <div class="row">
            <div class="col-md-8 mx-auto">                
                <div class="row row-20">
                <?php
                    $mentor_ids = explode (",", rwmb_meta( 'program-mentor-ids' ));
                    
                    /* Check if Menotor mentors on this Track's Program */
                    foreach ( $mentor_ids as $mentor_id ){
                        
                ?>

                    <div class="col-6 col-md-2 padding-lr-20 padding-b-40 d-flex">
                        <div class="mentor-card">
                            <div class="image-box">
                                <?php
                                    /* Get Avatar */
                                    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                    $meta_key = 'user_avatar_url';
                                    $get_avatar_url = get_user_meta($mentor_id, $meta_key, true);

                                    if($get_avatar_url){
                                        $avatar_url = $get_avatar_url;
                                    }

                                    /* Get User Display Name */
                                    switch_to_blog(1);

                                    $gf_id = 4; //Form ID
                                    $entry_count = 0;

                                    /* GF Search Criteria */
                                    $search_criteria = array(

                                    'field_filters' => array( //which fields to search

                                        array(

                                            'key' => 'created_by', 'value' => $mentor_id, //Current logged in user
                                            )
                                        )
                                    );

                                    /* Get Entries */
                                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                    /* Get GF Entry Count */
                                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                                    if($entry_count){ //If no entry
                                        foreach( $entries as $entry ){          
                                            $displayname = rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' );
                                        }                
                                    }   
                                
                                    restore_current_blog();
                                ?>
                                <img src="<?php echo $avatar_url; ?>" alt="">
                            </div>
                            <div class="name">
                                <a data-toggle="modal" href="#mentor-<?php echo $post_id ?>">
                                    <?php echo ($mentor_id == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </a>
                            </div>
                            <div class="position">
                                <?php
                                    $field_position = 'mentor-position';
                                    $field_company = 'mentor-company';

                                    $meta_position = get_post_meta($post->ID, $field_position, true);
                                    $meta_company = get_post_meta($post->ID, $field_company, true);

                                    echo $meta_company;
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade font-main coming-soon-modal" id="mentor-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content padding-o-40">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="text-center">
                                    <div class="margin-b-10">
                                        <?php
                                            $field = 'mentor-profile-image';

                                            $meta = get_post_meta($post->ID, $field, true);

                                        ?>
                                        <img src="<?php if($meta){  echo $meta; } ?>" width="120" class="profile-image">
                                    </div>
                                    <div class="txt-normal-s margin-b-10">
                                        Mentor
                                    </div>
                                    <h4 class="txt-xlg txt-medium margin-b-10">
                                        <?php echo the_title(); ?>
                                    </h4>
                                    <h5 class="txt-medium uppercase txt-sm margin-b-20">
                                        <?php
                                            $field_position = 'mentor-position';
                                            $field_company = 'mentor-company';

                                            $meta_position = get_post_meta($post->ID, $field_position, true);
                                            $meta_company = get_post_meta($post->ID, $field_company, true);

                                            echo $meta_position.' @ '.$meta_company;
                                        ?>
                                    </h5>
                                    <p class="txt-sm txt-height-1-7 margin-b-20">
                                        <?php
                                            $field = 'mentor-short-profile';

                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                                echo $meta;
                                            }
                                        ?>
                                    </p>
                                    <div class="txt-xlg">
                                        <?php
                                            $field = 'mentor-facebook';

                                            $meta = get_post_meta($post->ID, $field, true);

                                        ?>
                                        <a
                                           href="<?php if($meta){  echo $meta; } ?>" 
                                           class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                           target="_blank"
                                        >
                                            <i class="fa fa-facebook fa-fw"></i>
                                        </a>

                                        <?php
                                            $field = 'mentor-twitter';

                                            $meta = get_post_meta($post->ID, $field, true);

                                        ?>
                                        <a 
                                           href="<?php if($meta){  echo $meta; } ?>" 
                                           class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                           target="_blank"
                                        >
                                            <i class="fa fa-twitter fa-fw"></i>
                                        </a>

                                        <?php
                                            $field = 'mentor-linkedin';

                                            $meta = get_post_meta($post->ID, $field, true);

                                        ?>
                                        <a 
                                           href="<?php if($meta){  echo $meta; } ?>" 
                                           class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                           target="_blank"
                                        >
                                            <i class="fa fa-linkedin fa-fw"></i>
                                        </a>
                                    </div>
                                    <div class="padding-t-20 margin-t-20">
                                        <div class="txt-sm txt-medium margin-b-10">
                                            Mentors on
                                        </div>
                                        <p class="txt-sm">
                                            <?php
                                                $field = 'mentor-mentorship-areas';

                                                $meta = get_post_meta($post->ID, $field, false);

                                                if($meta){
                                                    foreach($meta as $key=>$value) {
                                                        echo $value;
                                                        if($key < count($meta) ){
                                                            echo ', ';
                                                        }
                                                    }
                                                }
                                            ?>
                                        </p>
                                    </div>
                                    <div class="border-t-1 border-color-darkgrey padding-t-20 margin-t-20">
                                        <div class="txt-sm txt-medium margin-b-10">
                                            Small Business Mentorship area
                                        </div>
                                        <p class="txt-sm">
                                            <?php
                                                $field = 'mentor-small-business-mentorship-interest';

                                                $meta = get_post_meta($post->ID, $field, false);

                                                if($meta){
                                                    foreach($meta as $key=>$value) {
                                                        echo $value;
                                                        if($key < count($meta) ){
                                                            echo ', ';
                                                        }
                                                    }
                                                }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                    }   // Check if Menotor mentors on this Track's Program: End
                ?>  
                </div>
                <h2 class="txt-normal-s txt-height-1-7">
                    Have questions? Check out the
                        Help Center
                    .
                </h2>
                <div class="margin-t-20">
                    <a href="https://www.saedconnect.org/growth-programs/mentor-directory/" class="btn btn-green txt-sm no-m-b">
                        See Full List
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
-->
<!-- Pay -->
<!--<section class="bg-grey padding-t-80 padding-b-80" id="subscribe">
    <div class="container-wrapper">
        <div class="row"> 
            <div class="col-md-8 mx-auto">
                <h2 class="text-center margin-b-5">
                    Want to speak to someone? Need Special help?
                </h2>
                <h1 class="txt-xxlg txt-height-1-1 margin-b-60 text-center">
                    Engage with Expert Mentors waiting to help you
                </h1>
                <p class="margin-b-60 text-center">
                    <?php echo $subscription_message[$program_id]; ?>
                </p>
                <div class="row">
                    <div class="col-md-9 mx-auto theme-bg theme-alt text-center text-md-left">
                        <div class="padding-o-30">
                            <div class="row row-10 overflow-hidden">
                                <div class="col-md-6 padding-lr-20 payment-border border-color-darkgrey margin-b-10 padding-b-10">
                                    <h3 class="txt-xxlg txt-bold margin-b-5">
                                        Just N10,000 / Qtr
                                    </h3>
                                    <p class="txt-sm">
                                        To join <?php 
                                            if( $program_id == 650 ){
                                                echo 'the '.$track_title;
                                            } else {
                                                echo $program_name;
                                            }
                                        ?>
                                        <br>
                                        Mentor Hub
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-20">
                                    <p class="txt-normal-s">
                                        We charge a small fee to cover our costs, maintain expert team & sustain a high quality moderated Mentor Hub.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <ul class="con-list black txt-normal-s margin-t-40">
                            <li class="padding-b-20">
                                <i class="fa fa-chevron-right"></i>
                                <span>
                                    Tap into the wisdom & experience of a lot of mentors
                                </span>
                            </li>
                            <li class="padding-b-20">
                                <i class="fa fa-chevron-right"></i>
                                <span>
                                    Get customised answers to your questions
                                </span>
                            </li>
                            <li class="padding-b-20">
                                <i class="fa fa-chevron-right"></i>
                                <span>
                                    Get inspired by your peers
                                </span>
                            </li>
                            <li class="padding-b-20">
                                <i class="fa fa-chevron-right"></i>
                                <span>
                                    Get access to growth support tools, templates & special opportunities.
                                </span>
                            </li>
                            <li>
                                <i class="fa fa-chevron-right"></i>
                                <span>
                                    Engage with Experts and Mentors until you are satisfied.
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="text-center margin-t-40">
                    <?php
                        if( $program_id == 650 ){
                            $program_mentor_forum_url = $track_mentor_forum_url;
                        }
                    ?>
                    <a href="<?php echo $program_mentor_forum_url; ?>" class="btn btn-green txt-sm">
                        Subscribe
                    </a>
                </div>
              </div>  
        </div>
    </div>
</section>-->

<!-- FAQ -->
<section class="container-wrapper padding-tb-60">
    <header class="margin-b-60 text-center">
        <h2 class="txt-xxlg txt-light padding-b-20">Frequently Asked Questions</h2>
        <p>
            Got questions? Require clarifications? Find your answers here
        </p> 
    </header>
    <div class="row row-10">
    <?php 
        
        $accordionCounter = 1;

        foreach ( $track_faq as $value ) { 
    ?>
        <div class="col-md-4 padding-lr-10 padding-b-20">
            <div class="faq-collapse padding">
                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-<?php echo $accordionCounter ?>" aria-expanded="false" aria-controls="collapseExample">
                    <?php echo $value['accordion-title']; ?>
                </button>
                <div class="collapse" id="faq-<?php echo $accordionCounter ?>">
                    <div class="card card-body">
                        <article class="">
                            <p class="txt-sm">
                                <?php echo $value['accordion-content']; ?>
                            </p>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    <?php
            $accordionCounter++;
        }
    ?> 
    </div>
    <p class="txt-xlg txt-medium text-center margin-t-40 margin-b-20">
        Didn't find your answer?
    </p>
    <div class="text-center" id="scroll-to-me">
        <a href="https://wa.me/2348104477275" class="btn btn-green txt-normal-s" target="_blank">
            Chat with us on Whatsapp
        </a>
    </div>
</section>

<style>
    .gform_wrapper select, .gform_wrapper input {
        line-height: 1.5;
        width: 100% !important;
    }
    
    .gform_footer {
          display: inline-block !important;
          justify-content: unset;
    }

    .gform_footer input {
        display: inline-block !important;
        width: auto;
        font-size: 0.8rem !important;
    }
    
    .gform_wrapper .gform_button {
        width: auto !important;
    }
</style>

<?php
    /* Check if user is loggedin */

    if ( $program_id != 650 ) {
?>
    <section class="container-wrapper bg-grey padding-tb-80" id="scroll-to-me">
        <div class="row">
            <div class="col-md-7 mx-auto">
                <h2 class="txt-xxlg txt-light txt-height-1-2 text-center padding-b-20">
                    Enroll for <?php echo $track_title; ?> Track <?php //echo $program_name; ?>
                </h2>
                <p class="txt-normal-s text-center padding-b-40">
                    Please complete the form below, to secure your space in the next session of the <?php echo $track_title; ?> track classes
                </p>
                <?php
                    
                    if($gf_Subscription_form_id){
                        /* Get Form */
                        echo do_shortcode('[gravityform id="'.$gf_Subscription_form_id.'" title="false" description="false"]');
                    }
                ?>
            </div>
        </div>
    </section>
<?php } ?>
<script>
    /* Scroll to view */
    $(".scroll-to").click(function(){
        var elmnt = document.getElementById("scroll-to-me");
        elmnt.scrollIntoView({behavior: "smooth", block: "start", }); // Top
    });
</script>
