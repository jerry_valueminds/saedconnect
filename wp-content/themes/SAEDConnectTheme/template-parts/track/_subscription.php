<div class="padding-lr-80">
    <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
        Subscribe to the Mentor forum
    </h2>
    <p class="margin-b-40">
        Lorem animi maxime saepe impedit, natus. Vitae magnam nisi nostrum quaerat non tenetur quis!
    </p>

    <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
    <?php
        $group_values = rwmb_meta( 'subscription-block-group' );


        if ( ! empty( $group_values ) ) {

            foreach ( $group_values as $group_value ) {

            /* Get Content type */
            $content_type = $group_value['select-subscription-type'];

                /* Check Subscription Type */
                if($content_type == 'paid-subscription-meta-box'){
    ?>
        <!-- Paid Subscription -->
        <div class="col-md-3 padding-lr-20 padding-b-20">
            <h3 class="txt-medium margin-b-10">
                <?php
                    echo $group_value['subscription-title'];
                ?>
            </h3>
            <p class="txt-sm margin-b-20">
                <?php
                    echo $group_value['paid-subscription-meta-box']['paid-subscription-description'];
                ?>
            </p>
            <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                <?php
                    echo $group_value['paid-subscription-meta-box']['paid-subscription-fee'];
                ?>
            </div>
            <div>
                <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                    <?php
                        echo $group_value['subscription-button-text'];
                    ?>
                </a>
            </div>
        </div>

    <?php } elseif ($content_type == 'image-subscription-meta-box') { ?>

    <?php
        /* Get Image */
        //$images = rwmb_meta( 'mini_site_feature_image', array( 'limit' => 1 ) );
        $images = $group_value['image-subscription-meta-box']['image-subscription-image'];
        $image = $images[0];
    ?>

        <!-- Image -->
        <div class="col-md-3 padding-lr-20 padding-b-20">
            <h3 class="txt-medium margin-b-10">
                <?php
                    echo $group_value['subscription-title'];
                ?>
            </h3>
            <div class="margin-b-20">
                <img src="<?php echo  wp_get_attachment_image_url( $image, 'size' ); ?>" alt="">
            </div>
            <p class="txt-sm margin-b-20">
                <?php
                    echo $group_value['paid-subscription-meta-box']['paid-subscription-description'];
                ?>
            </p>
            <div>
                <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                    <?php
                        echo $group_value['subscription-button-text'];
                    ?>
                </a>
            </div>
        </div>

    <?php } elseif ($content_type == 'text-subscription-meta-box') { ?> 

        <!-- Text -->
        <div class="col-md-3 padding-lr-20 padding-b-20">
            <h3 class="txt-medium margin-b-10">
                <?php
                    echo $group_value['subscription-title'];
                ?>
            </h3>
            <p class="txt-sm margin-b-20">
                <?php
                    echo $group_value['text-subscription-meta-box']['text-subscription-content'];
                ?>
            </p>
            <div>
                <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                    <?php
                        echo $group_value['subscription-button-text'];
                    ?>
                </a>
            </div>
        </div>

    <?php
                }
            }
        }
    ?>   
    </div>

    <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
        <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
            Are you a Corp Member?
        </h2>
        <p class="col-md-7 padding-lr-20 txt-sm">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
        </p>
        <div class="col-md-2 padding-lr-20">
            <a href="" class="btn btn-green txt-xs full-width no-m-b">
                Learn more
            </a>
        </div>
    </div>
    <div class="row row-20 padding-b-40">
        <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
            Already Subscribed?
        </h2>
        <p class="col-md-7 padding-lr-20 txt-sm">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
        </p>
        <div class="col-md-2 padding-lr-20">
            <a href="" class="btn btn-green txt-xs full-width no-m-b">
                Sign up
            </a>
        </div>
    </div>
</div>