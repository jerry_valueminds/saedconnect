<h2 class="txt-xlg txt-medium txt-height-1-2 padding-b-20">
    Subscribe to <?php echo rwmb_meta( 'program-name' ); ?>
</h2>
<div class="row">
    <div class="">
        <?php
            /* Get Form ID from request */
            $gf_id = rwmb_meta( 'program-form-shortcode' );

            if($gf_id){
                /* Get Form */
                echo do_shortcode('[gravityform id="'.$gf_id.'" title="false" description="false"]');
            }
        ?>
    </div>
</div>
