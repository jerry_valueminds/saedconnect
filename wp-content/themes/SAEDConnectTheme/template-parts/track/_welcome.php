<?php
    /* Get Program Featured Image */
    $program_images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ) );
    $program_images = reset( $program_images );

    /* Declare Program Information Var */
    $program_id;
    $program_name;
    $program_url;
    $program_theme_color;
    $program_theme_color_alt;

    /* Get Track ID */
    $track_id = get_the_ID();
    $track_title = rwmb_meta( 'track-short-name' );
    $track_faq = rwmb_meta( 'program-faq' );
    $track_guide_url = rwmb_meta( 'program-guide-url' );
    $track_forum_url = rwmb_meta( 'program-community-forum' );

    /* Get Form ID from request */
    $gf_id = rwmb_meta( 'program-form-shortcode' );

    /*
    *==============================================
    *
    *   Get data from Program
    *
    *==============================================
    */
    $connected = new WP_Query( array(
        'relationship' => array(
            'id'   => 'program_to_track',
            'to' => $track_id, // You can pass object ID or full object
        ),
        'nopaging' => true,
    ) );
    while ( $connected->have_posts() ) : $connected->the_post();

        /* Get Program Information */
        $program_id = $post->ID;
        $program_name = get_the_title();
        $program_url = get_the_permalink();
        $program_mentor_forum = rwmb_meta( 'skilli-forum-url' );
        $program_theme_color = rwmb_meta( 'skilli-program-theme-color' );
        $program_theme_color_alt = rwmb_meta( 'skilli-program-alt-theme-color' );

    endwhile;
    wp_reset_postdata();

    /*
    *==============================================
    *
    *   Prepare Messages based on Parent Program 
    *
    *==============================================
    */
    /* subscription message */
    $subscription_message = array(
        /* TEI */
        '632' => 'Where do you go to get expert advice about discovering great ideas, or building your brilliant idea into a thriving business? The Entrepreneurship Incubator mentor hub is the expert support community you always wished you could reach out to when you needed some motivation, advice or expert experience.',
        
        /* JA */
        '649' => 'Where do you go to get expert advice about crafting a great CV, finding exciting job openings, preparing for interviews and taking the right career decisions? The Job Advisor mentor hub is the expert support community you always wished you could reach out to when you needed some motivation, advice or expert experience.',
        
        /* BC */
        '650' => 'Where do you go to get expert advice about solving that challenge or getting that business question answered? The '.$track_title.' mentor hub is the expert support community you always wished you could reach out to when you needed some motivation, advice or expert experience.'
    );

    /* Help Center Links */
    $help_center_link = array(
        /* TEI */
        '632' => 'https://www.saedconnect.org/help-center/forums/forum/the-entrepreneurship-incubator-help-center/',
        
        /* JA */
        '649' => 'https://www.saedconnect.org/help-center/forums/forum/job-advisor-help-center/',
        
        /* BC */
        '650' => 'https://www.saedconnect.org/help-center/forums/forum/business-clinic-help-center/'
    );

    /* CTA Messages */
    $cta_message = array(
        /* TEI */
        '632' => 'Join the '.$track_title.' Track',
        
        /* JA */
        '649' => 'Join the '.$track_title.' Track',
        
        /* BC */
        '650' => 'Subscribe to the '.$track_title.' Track'
    );
?>

<style>
    .theme-bg{
        background-color: <?php echo $program_theme_color ?>;
    }

    .theme-alt{
        color: <?php echo $program_theme_color_alt ?>;
    }

    .how-it-works-container::before{
        background-color: <?php echo $program_theme_color ?>;
    }
</style>
<div class="padding-t-10">
    <div class="container-wrapper bg-ash">
        <ul class="menu">
            <li>
                <a href="<?php echo $program_url ?>" class="active">
                    Home
                </a>
            </li>
            <li>
                <a href="<?php echo get_the_permalink().'?view=curriculum' ?>">
                    Fire Side Chat
                </a>
            </li>
            <li>
                <a href="<?php echo $track_guide_url ?>">
                    Guides
                </a>
            </li>
            <li>
                <a href="<?php echo $track_forum_url ?>">
                    Mentor Hub
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="container-wrapper text-center padding-t-40">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <h2 class="txt-2-4em txt-light txt-height-1-2 padding-b-30">
                Welcome to the <?php echo $track_title; ?> Track
            </h2>
            <article>
                <p class="txt-sm txt-height-1-7">
                    <?php echo rwmb_meta( 'program-description' ); ?>
                </p>
            </article>
        </div>
        <div class="col-md-10 mx-auto margin-b-40">
            <div class="margin-t-40">
                <div class="row row-15">
                    <div class="col-md-4 padding-lr-15 d-flex">
                        <div class="bg-white track-action-card">
                            <div class="content">
                                <div class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_mentor.png" alt="" width="80">
                                </div>
                                <h3 class="txt-medium margin-b-20">
                                    Fire Side Chat
                                </h3>
                                <p class="txt-sm txt-height-1-7">
                                    The experts in the mentor hub will provide answers personalised to your specific questions. 
                                </p>
                                <div class="margin-t-40">
                                    <a class="txt-sm txt-bold txt-color-dark" href="<?php echo get_the_permalink().'?view=curriculum' ?>">
                                        Get started 
                                        <i class="fa fa-angle-right padding-l-5"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ( $program_id == 650 ){ ?>
                    <div class="col-md-4 padding-lr-15 d-flex">
                        <div class="bg-white track-action-card">
                            <div class="content">
                                <div class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_contributor.png" alt="" width="80">
                                </div>
                                <h3 class="txt-medium margin-b-20">
                                    Tools & Resources
                                </h3>
                                <p class="txt-sm txt-height-1-7">
                                    The experts in the mentor hub will provide answers personalised to your specific questions. 
                                </p>
                                <div class="margin-t-40">
                                    <a class="txt-sm txt-bold txt-color-dark" href="<?php echo get_the_permalink().'?view=resources' ?>">
                                        Get started 
                                        <i class="fa fa-angle-right padding-l-5"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="col-md-4 padding-lr-15 d-flex">
                        <div class="bg-white track-action-card">
                            <div class="content">
                                <div class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_contributor.png" alt="" width="80">
                                </div>
                                <h3 class="txt-medium margin-b-20">
                                    Guides
                                </h3>
                                <p class="txt-sm txt-height-1-7">
                                    The experts in the mentor hub will provide answers personalised to your specific questions. 
                                </p>
                                <div class="margin-t-40">
                                    <a class="txt-sm txt-bold txt-color-dark" href="<?php echo $track_guide_url ?>">
                                        Get started 
                                        <i class="fa fa-angle-right padding-l-5"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-md-4 padding-lr-15 d-flex">
                        <div class="bg-white track-action-card">
                            <div class="content">
                                <div class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_trainer.png" alt="" width="80">
                                </div>
                                <h3 class="txt-medium margin-b-20">
                                    Mentor Hub
                                </h3>
                                <p class="txt-sm txt-height-1-7">
                                    The experts in the mentor hub will provide answers personalised to your specific questions. 
                                </p>
                                <div class="margin-t-40">
                                    <a class="txt-sm txt-bold txt-color-dark" href="<?php echo $program_mentor_forum ?>">
                                        Get started 
                                        <i class="fa fa-angle-right padding-l-5"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>