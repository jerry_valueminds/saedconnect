<?php
    switch_to_blog(20);
?>


<!-- SHC Modal -->
<div class="modal fade font-main shc-modal" id="comingSoonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="container-wrapper">
                <div class="col-md-10 mx-auto">
                    <div class="padding-tb-80">
                        <div class="margin-b-40">
                            <h2 class="txt-2em txt-bold txt-height-1-2">
                                What Help Center Community
                                <br>
                                are you interested in?
                            </h2>
                        </div>
                        <ul class="row row-20 shc-list txt-normal-s txt-medium">
                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="https://www.saedconnect.org/help-center/forums/forum/get-help/">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            Help Center
                                        </span>
                                    </a>
                                </li>
                            </div>
                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="https://www.saedconnect.org/help-center/forums/forum/get-help/mentor-help-center/">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            Mentor Help Center
                                        </span>
                                    </a>
                                </li>
                            </div>
                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="https://www.saedconnect.org/help-center/forums/forum/get-help/trainer-youth-development-program-provider-help-center/">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            Trainer/Youth Development Program Provider Help Center
                                        </span>
                                    </a>
                                </li>
                            </div>
                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="https://www.saedconnect.org/help-center/forums/forum/get-help/report-an-issue/">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            Report an Issue
                                        </span>
                                    </a>
                                </li>
                            </div>
                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="https://www.saedconnect.org/help-center/forums/forum/get-help/make-a-suggestion/">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            Make a Suggestion
                                        </span>
                                    </a>
                                </li>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
    //Revert to Previous Multisite
    restore_current_blog();
?>