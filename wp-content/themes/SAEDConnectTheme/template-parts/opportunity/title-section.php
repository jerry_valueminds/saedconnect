
<?php if( $current_user->ID != $post_author_id && $rendered_view != 'apply' ){ ?>   

<div class="padding-b-30 border-b-1 border-color-darkgrey">
    <p class="txt-medium txt-color-lighter margin-b-10">
        Program ID: <?php echo $post_id; ?>
    </p>
    <!-- Program Type -->
    <?php 
        $term_list = wp_get_post_terms($post_id, 'program-type', array("fields" => "names"));

        if( $term_list ){
    ?>
    <p class="txt-sm txt-medium txt-color-blue margin-b-10">
        <?php foreach( $term_list as $key => $term_name ){ ?>

        <span>
            <?php echo $term_name; ?>
        </span>

        <?php echo (  $key < ( count( $term_list ) - 1 ) ? '<span class="padding-lr-10">|</span>' : '' ) ?>

        <?php } ?>
    </p>
    <?php } ?>
    
    <!-- Title -->
    <h1 class="txt-xlg txt-medium txt-height-1-1 margin-b-10">
        <?php echo $post_title ?>
    </h1>
    
    <!-- Company -->
    <p class="txt-sm txt-medium txt-color-lighter margin-b-20">
        <?php echo get_post_meta( $post_id, 'company', true ) ?>
    </p>
    
    <?php if( $current_user->ID == $post_author_id ){ ?>
        <!-- Author Action -->
        <p class="txt-sm margin-b-15">
            <a
                data-toggle="modal" href="#specificationModal"
                class="txt-color-green"
            >
                Edit
            </a>
            <span class="padding-lr-5">|</span>
            <a
                href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', $dashboard_management_link, $post_id ) ?>" 
                class="txt-color-red confirm-delete"
            >
                Delete
            </a>
        </p>
    <?php } ?>
    
    <?php if( is_user_logged_in() ){ ?>
        
    <div>
    <?php if( $current_user->ID == $post_author_id ){ ?>
        <!-- Publish -->
        <a href="<?php echo $post_link.'?view=form-specification&action=publication'; ?>" class="btn btn-blue txt-sm no-m-b">
            <?php
                $publication_key = 'publication_status';
                $publication_meta = get_post_meta( $post_id, $publication_key, true );

                if($publication_meta == 'user_published'){
                    echo 'Unpublish';
                } elseif($publication_meta == 'admin_published') {
                    echo 'Unpublish';
                }else{
                    echo 'Publish';
                }
            ?>
        </a>
    <?php } else { ?>
        <?php 

        ?>
        <?php if( $rendered_view == 'apply' ){ ?>
            <!-- Job Details -->
            <a href="<?php echo $post_link; ?>" class="btn btn-blue txt-sm no-m-b">
                Back to Details
            </a>
        <?php } else { ?>
            <!-- Apply -->
            <?php  
                $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                $howToApply = get_post_meta( $post_id, 'how-to-apply', true );
            ?>

            <?php if( $applicationMethod == 'manual' ){ ?>
                <a class="btn btn-blue txt-sm no-m-b" data-toggle="modal" href="#applyModal">
                    Apply
                </a>
            <?php } elseif( $applicationMethod == '3rd-party' ) { ?>
                <a class="btn btn-blue txt-sm no-m-b" data-toggle="modal" href="#applyModal">
                    Apply
                </a>
            <?php } elseif( $applicationMethod == 'saedconnect' ) { ?>
                <a href="<?php echo $post_link.'?view=apply'; ?>" class="btn btn-blue txt-sm no-m-b">
                    Apply
                </a>            
            <?php } ?>

        <?php } ?>
    <?php } ?>
    </div>
        
    <?php } ?>
</div>

<?php } ?>