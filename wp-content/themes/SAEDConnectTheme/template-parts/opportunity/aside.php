<?php if( $rendered_view != 'application-manager' && $rendered_view != 'apply' && $rendered_view != 'application-confirmation' ) { ?>

    <div class="col-md-4 padding-lr-20">
        <figure class="margin-b-20 position-relative">
            <?php $images = get_attached_media( 'image', $post_id ); ?>
            
            <?php if($images){ ?>
           
                <?php  
                    foreach($images as $image) { //print_r( $image ); 
                        $previousImg_id = $image->ID;
                ?>
                    <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">
                
                <?php } ?>
                
            <?php } else { ?>
                <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png" width="150" class="margin-b-60">
                </figure>
                
            <?php } ?>
            <?php if( $current_user->ID == $post_author_id ){ ?>
            <div class="upload-btn">
                <a class="btn btn-blue txt-xs" data-toggle="modal" href="#featuredImageModal">
                     <?php echo ($images)? "Change Image" : "+ Add"; ?>
                </a>
            </div>
            <?php } ?>
        </figure>
        <style>
            .upload-btn{
                position: absolute;
                text-align: center;
                left: 0;
                width: 100%;
                bottom: 15px;
            }
        </style>
    <?php if( $current_user->ID == $post_author_id ) { ?>
        <div class="padding-o-20 bg-grey margin-b-20">
            <h3 class="txt-bold txt-height-1-4 margin-b-15">
                Recieve &amp; Manage Applications for this Job on SAEDConnect
            </h3>
            <p class="txt-height-1-7 txt-sm margin-b-20">
                Recieve &amp; Manage Applications for this Job on SAEDConnect Recieve &amp; Manage Applications for this Job on SAEDConnect
            </p>
            <div>
                <a href="" class="btn btn-blue txt-xs no-m-b">
                    Get started
                </a>
            </div>
        </div>
        <div class="padding-o-20 bg-ash margin-b-20">
            <span class="d-inline-flex align-items-center padding-r-40">
                <input type="checkbox" id="minimal-checkbox-3" name="radio-2" value="beginner">
                <label for="minimal-radio-3" class="txt-color-white padding-l-10 txt-medium">Add a Program Brochure</label>
            </span>
        </div>
        <div class="padding-o-20 bg-ash margin-b-20">
            <h3 class="txt-bold txt-color-white txt-height-1-4 margin-b-15">
                Advertise this Job
            </h3>
            <ul class="icon-list white txt-sm">
                <li>
                    <a href="http://www.saedconnect.org/contribute/synopsis/corporate/">
                        <i class="fa fa-chevron-right"></i>
                        <span>Feature on Homepage</span>
                    </a>
                </li>
                <li>
                    <a href="http://www.saedconnect.org/contribute/synopsis/schools-youth-development/">
                        <i class="fa fa-chevron-right"></i>
                        <span>Feature in Jobs Newsletter</span>
                    </a>
                </li>
                <li>
                    <a href="http://www.saedconnect.org/contribute/synopsis/government-donor-agencies/">
                        <i class="fa fa-chevron-right"></i>
                        <span>
                            Send a standalaone broadcast
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    <?php } ?>
      
    <?php if( !is_user_logged_in() ){ ?>
        <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
            <h3 class="txt-bold txt-height-1-4 margin-b-15">
                Login to Apply Now
            </h3>

            <!-- Login Form -->
            <style>
                .gfield_checkbox label, .login-form .gfield_label{
                    display: block !important;
                }

                .gfield_checkbox li{
                    display: flex;
                    align-items: center;
                }

                .gfield_checkbox input{
                    margin: 0 !important;
                    margin-right: 10px !important;
                }
            </style>

            <?php switch_to_blog(1); ?>

                <!-- Login Tab -->
                <div class="login">
                    <div class="login-form">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                        <?php endif;?>
                    </div>
                </div>

            <?php restore_current_blog(); ?>

            <p class="txt-sm txt-medium padding-t-10">
                Dont have an account?
                <a href="https://www.saedconnect.org/register" class=" txt-color-blue">
                    Sign up
                </a>
            </p>
        </div>
    <?php } ?>
       
        <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
            <h3 class="txt-bold txt-height-1-4 margin-b-15">
                Share Job Post
            </h3>
            <?php echo do_shortcode('[Sassy_Social_Share]') ?>
        </div>
        <p class="txt-sm txt-medium padding-t-10">
            <a href="" class=" txt-color-blue">
                Report Job
            </a>
        </p>
    </div>
    
<?php } ?>