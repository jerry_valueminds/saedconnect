<?php if( $rendered_view == 'apply' ){ ?>
    <style>
        .initials{
            display: inline-flex;
            justify-content: center;
            align-items: center;
            width: 2em;
            height: 2em;
            border-radius: 50%;
        }
    </style>
    <div class="bg-grey">
        <header class="bg-orange padding-tb-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-sm-10 col-md-10 col-lg-6 mx-auto text-center">
                        <div class="padding-b-40 text-left">
                            <a href="<?php echo $post_link ?>" class="txt-color-white">
                                <i class="fa fa-long-arrow-left"></i>
                                <span class="txt-medium padding-r-10">Back</span>
                            </a>
                        </div>
                        <h1 class="txt-xxlg txt-color-white txt-medium padding-b-20">Hello Franklin!</h1>
                        <h2 class="txt-2em txt-height-1-2 padding-b-20">Complete your Application for <span class="text-capitalize"><?php echo $post_title ?></span></h2>
                        <p class="txt-medium txt-color-white">Deadline: 25th August 2019</p>
                    </div>
                </div>
            </div>
        </header>
        
        <!-- Page Info -->   
        <section class="container-wrapper padding-t-20 padding-b-40">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <div class="bg-ash padding-o-30">
                        <h2 class="txt-bold txt-color-dark padding-b-15">Instruction</h2>
                        <p class="txt-sm txt-color-white">Provide all the information requested below to complete your application for this Job. You will only be able to submit this application after you have provided all the requested information</p>
                    </div>
                </div>
            </div>
        </section>

            <?php
                $current_user = wp_get_current_user();
                $check = array();
             ?>    

            
            
            <!-- Business Mentor Capabilities -->
            <div>    
            <?php  
                $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, 'my_mentor_skills', true) );
                $unserialized_requirement = maybe_unserialize( get_post_meta( $post->ID, 'mentorship-capabilities', true ) );
                switch_to_blog(109);  
                $meta_key = 'my_mentor_skills';
                                      
                if( $unserialized_data ){
            ?>

                    <div class="padding-b-10">
                        <div class="txt-normal-s txt-medium">
                            <a 
                               class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                               data-toggle="collapse" 
                               href="#collapseCapabilities" aria-expanded="false" aria-controls="collapseOne"
                            >
                                <span>
                                    Business Mentor Capabilities
                                    
                                </span>
                            </a>
                        </div>
                        <div id="collapseCapabilities" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                            <div class="border-o-1 border-color-darkgrey padding-o-15">
                            
                            
                            <?php
                                foreach($unserialized_requirement  as $saved_id){
                                    $term = get_term($saved_id);
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                                    $post_id = 0;
                            ?>
                               <?php if( in_array( $saved_id, $unserialized_data['my_mentor_skills'] ) ){ ?> 

                                    <?php
                                        $description_query = new WP_Query();
                                        $description_query->query( 
                                            array(
                                                'post_type' => 'cap-description',
                                                'post_status' => 'publish',
                                                'author' => $current_user->ID,
                                                'posts_per_page' => 1,
                                                'tax_query' => array(
                                                    array(
                                                        'taxonomy' => 'business-mentorship',
                                                        'terms' => $term_id,
                                                    )
                                                ),
                                            ) 
                                        );

                                        if ( $description_query->have_posts() ) {
                                            $check[] = 'complete'; 
                                            while ($description_query->have_posts()) : $description_query->the_post();

                                                /* Variables */
                                                $post_id = $post->ID;   //Get Program ID

                                            endwhile; 
                                        } else {
                                            $check[] = 'incomplete'; 
                                        }
                                    ?>

                                    <div class="bg-ghostwhite padding-o-10 margin-b-10">
                                        <div class="row row-15">
                                            <div class="col">
                                                <p class="txt-sm txt-color-dark txt-medium">
                                                    <?php echo $term_name ?>
                                                    <?php if ($post_id){ ?>
                                                        <i class="txt-color-green fa fa-check"></i>
                                                    <?php }else{ ?>
                                                      -
                                                       <span class="txt-normal-s font-italic txt-color-yellow-dark">
                                                           <i class="fa fa-exclamation-triangle"></i>
                                                           You must describe your competence
                                                       </span>
                                                   <?php } ?>
                                                </p>                                 
                                            </div>
                                            <p class="col-auto padding-lr-15 txt-sm text-right">
                                                <?php if ($post_id){ ?>
                                                    <a href="#capability-description-collapse-<?php echo $term_id; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="capability-description-collapse-<?php echo $term_id; ?>">
                                                       View Description
                                                       <i class="fa fa-chevron-down"></i>
                                                   </a>

                                               <?php }else{ ?>
                                                   <a class="" data-toggle="modal" href="#businessMentorCapabilityDescriptionModal-<?php echo $saved_id ?>">
                                                       <i class="fa fa-plus"></i>
                                                       Add Description
                                                   </a>
                                               <?php } ?>
                                            </p>
                                        </div>

                                        <?php if($post_id){ ?>   
                                        <div class="collapse" id="capability-description-collapse-<?php echo $term_id; ?>">
                                            <div class="padding-t-10 margin-t-10 border-t-1">
                                                <div class="row row-10">
                                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                            For how long have you been practicing this business (in Years)    
                                                        </p>
                                                        <p class="txt-sm">
                                                            <?php echo get_post_meta( $post_id, 'years_of_experience', true ); ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                            Describe your experience in this business    
                                                        </p>
                                                        <article class="text-box txt-sm">
                                                            <?php echo get_post_meta( $post_id, 'business_experience', true ); ?>
                                                        </article>
                                                    </div>
                                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                            What area of this business are you great at?    
                                                        </p>
                                                        <p class="txt-sm">
                                                            <?php 
                                                                $meta_ids = get_post_meta( $post_id, 'sub_terms', true );
                                                                $counter = 1;

                                                                foreach( $meta_ids as $meta_id ){
                                                                    $sub_term = get_term($meta_id);
                                                                    echo '<span class="d-block">'.$counter.'. '.$sub_term->name.'</span>';         
                                                                    $counter++;
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                            Apart from the above listed, what other models of this business can you mentor on?    
                                                        </p>
                                                        <article class="text-box txt-sm">
                                                            <?php echo get_post_meta( $post_id, 'other_mentorship_models', true ); ?>
                                                        </article>
                                                    </div>
                                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                            What sort of problems can you help people solve in this area?     
                                                        </p>
                                                        <p class="txt-sm">
                                                            <?php 
                                                                $meta_ids = get_post_meta( $post_id, 'solutions_proferred', true );
                                                                $counter = 1;

                                                                foreach( $meta_ids as $meta_id ){
                                                                    if( $meta_id == 1 )
                                                                        echo '<span class="d-block">'.$counter.'. How to start the business from scratch</span>'; 
                                                                    elseif( $meta_id == 2 )
                                                                        echo '<span class="d-block">'.$counter.'. How to raise money to start the business.</span>'; 
                                                                    elseif( $meta_id == 3 )
                                                                        echo '<span class="d-block">'.$counter.'. Where to get suppliers and tools</span>';
                                                                    elseif( $meta_id == 4 )
                                                                        echo '<span class="d-block">'.$counter.'. How to find partners and staff for the business.</span>'; 
                                                                    elseif( $meta_id == 5 )
                                                                        echo '<span class="d-block">'.$counter.'. How to setup the business</span>'; 
                                                                    elseif( $meta_id == 6 )
                                                                        echo '<span class="d-block">'.$counter.'. How to get customers</span>'; 
                                                                    elseif( $meta_id == 7 )
                                                                        echo '<span class="d-block">'.$counter.'. How to grow the business</span>'; 
                                                                    elseif( $meta_id == 8 )
                                                                        echo '<span class="d-block">'.$counter.'. How to avoid and overcome common challenges in the business</span>'; 
                                                                    elseif( $meta_id == 9 )
                                                                        echo '<span class="d-block">'.$counter.'. Technical Skills in the business</span>'; 
                                                                    elseif( $meta_id == 10 )
                                                                        echo '<span class="d-block">'.$counter.'. How to optimize your profit in the business</span>'; 

                                                                    $counter++;
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                            Have you ever mentored someone in this business before?     
                                                        </p>
                                                        <p class="txt-sm">
                                                            <?php echo get_post_meta( $post_id, 'previous_mentorship_experience', true ); ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                            What mentorship Models can you offer?    
                                                        </p>
                                                        <p class="txt-sm">
                                                            <?php 
                                                                $meta_ids = get_post_meta( $post_id, 'mentorship_models', true );
                                                                $counter = 1;

                                                                foreach( $meta_ids as $meta_id ){
                                                                    if( $meta_id == 1 )
                                                                        echo '<span class="d-block">'.$counter.'. I can mentor over WhatsApp or Telegram</span>'; 
                                                                    elseif( $meta_id == 2 )
                                                                        echo '<span class="d-block">'.$counter.'. I can mentor via Facebook</span>'; 
                                                                    elseif( $meta_id == 3 )
                                                                        echo '<span class="d-block">'.$counter.'. I can mentor in-person</span>'; 

                                                                    $counter++;
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 padding-lr-10">
                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                            In what State can you provide face-to-face mentorship for this business?    
                                                        </p>
                                                        <p class="txt-sm">
                                                            <?php echo get_post_meta( $post_id, 'face_to_face_locations', true ); ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 text-right padding-lr-10 padding-t-20 padding-b-10">
                                                        <a class="txt-xs txt-color-green" data-toggle="modal" href="#businessMentorCapabilityDescriptionModal-<?php echo $saved_id ?>">
                                                           <i class="fa fa-pencil"></i>
                                                           Edit Description
                                                       </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>    

                                    </div>
                                    
                                    <!-- Edit Modal -->
                                    <div class="modal fade font-main filter-modal" id="businessMentorCapabilityDescriptionModal-<?php echo $saved_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <form action="<?php echo currentUrl(false).'&form=business-mentor-capability-description-'.$saved_id; ?>" method="post">
                                                    <div class="modal-header padding-lr-30">
                                                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $term_name ?> Description</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body padding-o-30 form">
                                                        <?php

                                                                       
                                                            if( $_POST && $_GET['form'] == 'business-mentor-capability-description-'.$saved_id ){

                                                                /* Get Post Name */
                                                                $postName = 'Mentor-Business-Capability-Description-'.$current_user->ID.'-'.$term_id;

                                                                /* Meta */
                                                                $years_of_experience = wp_kses_post( $_POST['years_of_experience'] );
                                                                $business_experience = wp_kses_post( $_POST['business_experience'] );
                                                                $other_mentorship_models = wp_kses_post( $_POST['other_mentorship_models'] );
                                                                $face_to_face_locations = wp_kses_post( $_POST['face_to_face_locations'] );

                                                                $sub_terms = $_POST['sub_terms'];
                                                                $solutions_proferred = $_POST['solutions_proferred'];
                                                                $mentorship_models = $_POST['mentorship_models'];

                                                                $previous_mentorship_experience = sanitize_text_field( $_POST['previous_mentorship_experience'] ); 


                                                                /* Save Post to DB */
                                                                $post_id = wp_insert_post(array (
                                                                    'ID' => $post_id,
                                                                    'post_type' => 'cap-description',
                                                                    'post_title' => $postName,
                                                                    'post_content' => "",
                                                                    'post_status' => 'publish',
                                                                ));

                                                                update_post_meta( $post_id, 'years_of_experience', $years_of_experience );
                                                                update_post_meta( $post_id, 'business_experience', $business_experience);
                                                                update_post_meta( $post_id, 'other_mentorship_models', $other_mentorship_models );
                                                                update_post_meta( $post_id, 'face_to_face_locations', $face_to_face_locations );

                                                                update_post_meta( $post_id, 'sub_terms', $sub_terms );
                                                                update_post_meta( $post_id, 'solutions_proferred', $solutions_proferred );
                                                                update_post_meta( $post_id, 'mentorship_models', $mentorship_models );

                                                                update_post_meta( $post_id, 'previous_mentorship_experience', $previous_mentorship_experience );

                                                                wp_set_post_terms( $post_id, $term_id, 'mentorship-capabilities' );

                                                                /* Redirect */
                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                            }
                                                        ?>
                                                        <div class="form-item">
                                                            <label for="years_of_experience">For how long have you been practicing this business (in Years)</label>
                                                            <input type="number" id="years_of_experience" name="years_of_experience" value="<?php echo get_post_meta( $post_id, 'years_of_experience', true ); ?>">
                                                        </div>
                                                        <div class="form-item">
                                                            <label for="business_experience">Describe your experience in this business.</label>
                                                            <textarea name="business_experience" id="business_experience" class="editor"><?php echo get_post_meta( $post_id, 'business_experience', true ); ?></textarea>
                                                        </div>
                                                        <?php 
                                                            $child_terms = get_terms( 'business-mentorship', array('hide_empty' => false, 'parent' => $term_id) );
                                                            if( $child_terms ){
                                                                $meta = get_post_meta( $post_id, 'sub_terms', true );
                                                        ?>
                                                        <div class="form-item">
                                                            <label class="padding-b-10">What area of this business are you great at?</label>
                                                            <?php foreach($child_terms as $child_term ){ ?>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="<?php echo $child_term->term_id ?>" 
                                                                    name="sub_terms[]" 
                                                                    <?php echo ( in_array( $child_term->term_id, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    <?php echo $child_term->name ?>
                                                                </span>
                                                            </label>
                                                            <?php } ?>
                                                        </div>
                                                        <?php } ?>
                                                        <div class="form-item">
                                                            <label for="other_mentorship_models">Apart from the above listed, what other models of this business can you mentor on?</label>
                                                            <textarea name="other_mentorship_models" id="other_mentorship_models" class="editor"><?php echo get_post_meta( $post_id, 'other_mentorship_models', true ); ?></textarea>
                                                        </div>
                                                        <div class="form-item">
                                                            <?php $meta = get_post_meta( $post_id, 'solutions_proferred', true ); ?>
                                                            <label class="padding-b-10">What sort of problems can you help people solve in this area?</label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="1" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array( 1, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    How to start the business from scratch
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="2" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array( 2, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    How to raise money to start the business
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="3" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array( 3, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    Where to get suppliers and tools
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="4" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array( 4, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    How to find partners and staff for the business
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="5" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array( 5, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    How to setup the business
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="6" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array( 6, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    How to get customers
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="7" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array( 7, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    How to grow the business
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="8" 
                                                                    name="solutions_proferred[]"
                                                                    <?php echo ( in_array( 8, $meta) ) ? 'checked' : '' ?> 
                                                                >
                                                                <span class="padding-l-10">
                                                                    How to avoid and overcome common challenges in the business
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="9" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array( 9, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    Technical Skills in the business
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="10" 
                                                                    name="solutions_proferred[]" 
                                                                    <?php echo ( in_array(10, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    How to optimize your profit in the business
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <div class="form-item">
                                                            <?php $meta = get_post_meta( $post_id, 'previous_mentorship_experience', true ); ?>
                                                            <label for="previous_mentorship_experience">Have you ever mentored someone in this business before?</label>
                                                            <select name="previous_mentorship_experience" id="previous_mentorship_experience">
                                                                <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?>>Yes</option>
                                                                <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?>>No</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-item">
                                                            <?php $meta = get_post_meta( $post_id, 'mentorship_models', true ); ?>
                                                            <label class="padding-b-10">What mentorship Models can you offer?</label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="1" 
                                                                    name="mentorship_models[]" 
                                                                    <?php echo ( in_array(1, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    I can mentor over WhatsApp or Telegram
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="2" 
                                                                    name="mentorship_models[]"
                                                                    <?php echo ( in_array(2, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    I can mentor via Facebook
                                                                </span>
                                                            </label>
                                                            <label class="txt-sm d-flex align-items-center">
                                                                <input
                                                                    class="custom-check"
                                                                    type="checkbox" 
                                                                    value="3" 
                                                                    name="mentorship_models[]"
                                                                    <?php echo ( in_array(3, $meta) ) ? 'checked' : '' ?>
                                                                >
                                                                <span class="padding-l-10">
                                                                    I can mentor in-person
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <div class="form-item">
                                                            <label for="face_to_face_locations">In what State can you provide face-to-face mentorship for this business?</label>
                                                            <input type="text" id="face_to_face_locations" name="face_to_face_locations" value="<?php echo get_post_meta( $post_id, 'face_to_face_locations', true ); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer padding-lr-30">
                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                <?php }else{ ?>
                                    <div class="bg-beige padding-o-10 margin-b-10">
                                        <div class="row row-15">
                                            <div class="col">
                                                <p class="txt-sm txt-color-dark txt-medium">
                                                    <?php echo $term_name ?> -
                                                    <span class="txt-normal-s txt-color-red font-italic">
                                                        <i class="fa fa-times"></i>
                                                       This capability is required Required
                                                    </span>
                                                </p>                                 
                                            </div>
                                            <p class="col-auto padding-lr-15 txt-sm text-right">
                                               <a class="txt-color-red" data-toggle="modal" href="#myBusinessMentorCapabilitiesModal">
                                                   <i class="fa fa-plus"></i>
                                                   Add Capability
                                               </a>
                                            </p>
                                        </div>
                                    </div>  
                                        
                                <?php } ?>
                                
                                
                                
                                
                                
                           
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                        
            <?php } else { ?>
                <?php $check[] = 'incomplete'; ?>

                <p class="row align-items-center margin-b-10">
                    <span class="col-8">
                        Business Mentor Capabilities
                        <i class="txt-color-red fa fa-times"></i>
                    </span> 
                    <span class="col-md-4 text-right txt-sm">
                        <a class="txt-underline txt-color-dark txt-medium" data-toggle="modal" href="#myBusinessMentorCapabilitiesModal">
                            Select your Business Mentor Capabilities
                        </a>
                    </span>
                </p>

            <?php } ?>
            
            <!-- Add Modal -->
            <div class="modal fade font-main filter-modal" id="myBusinessMentorCapabilitiesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <form action="<?php echo currentUrl(false).'&form=business-mentor-capabilities'; ?>" method="post">
                            <div class="modal-header padding-lr-30">
                                <h5 class="modal-title" id="exampleModalLabel">Confirm Your Mentorship Capabilities</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body padding-o-30 form">
                                <p class="txt-sm padding-b-15">
                                   Select the required competetence. 
                                </p>
                                <?php

                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if( $_POST && $_GET['form'] == 'business-mentor-capabilities' ){
                                    /* Serialize Form Submission */
                                    $serialized_data = maybe_serialize($_POST);

                                    /* Save usermeta */
                                    update_user_meta( $current_user->ID, $meta_key, $serialized_data);

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                }

                                /* Get User Meta to populate Form */
                                $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, $meta_key, true) );

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                            ?>
                            <?php
                                foreach($unserialized_requirement  as $saved_id){
                                    $term = get_term($saved_id);
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                                    $post_id = 0;
                            ?>
                               <?php if( !in_array( $saved_id, $unserialized_data['my_mentor_skills'] ) ){ ?>

                                        <label class="txt-sm d-inline-flex align-items-center padding-r-20 padding-b-15">
                                            <input
                                                class="custom-check"
                                                type="checkbox" 
                                                value="<?php echo $term_id ?>" 
                                                name="<?php echo $meta_key ?>[]" 
                                                <?php echo in_array( $term_id, $unserialized_data[$meta_key] ) ? "checked" : "" ?>
                                            >
                                            <span class="padding-l-10">
                                                <?php echo $term_name; ?>
                                            </span>
                                        </label>

                                <?php } ?>

                            <?php 
                                } 
                            ?>
                            </div>
                            <div class="modal-footer padding-lr-30">
                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <?php restore_current_blog(); ?>
            </div>
                    
            
            
            
            <!-- CV -->
            <?php switch_to_blog(109); ?>
                    
            
            
            <!-- Your Education & Experience -->  
            <section class="container-wrapper padding-b-10">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h3 class="txt-lg txt-medium txt-color-orange padding-l-50 padding-r-40 padding-b-20">Your Education & Experience</h3>
                        
                        <!-- Personal Information -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'personal-info',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => 1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-personal-information-collapse" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">Basic information about you</span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="cv-personal-information-collapse">
                                    <div class="padding-lr-30">
                                        <!--<p class="txt-sm txt-light padding-t-10">
                                            What schools have you attended? When? What did you study? What degrees do you have?
                                        </p>-->
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <div class="row row-10 padding-b-20 txt-color-light">
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Gender          
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'gender', true ); ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                State of Origin          
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php 
                                                                    $saved_id = get_post_meta( $post_id, 'state_of_origin', true ); 
                                                                    switch_to_blog(110);
                                                                    $term = get_term( $saved_id );
                                                                    echo $term->name;
                                                                    restore_current_blog();
                                                                ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Yeah of Birth          
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'date_of_birth', true ); ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Marital Status          
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'marital_status', true ); ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Nationality          
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'nationality', true ); ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Mobile Number
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'phone_work', true ); ?>                                               
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Whatsapp Number        
                                                            </p>
                                                            <p class="txt-sm">
                                                                +<?php echo get_post_meta( $post_id, 'whatsapp_work', true ); ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Where you currently reside        
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php 
                                                                    $saved_id = get_post_meta( $post_id, 'work_location', true ); 
                                                                    switch_to_blog(110);
                                                                    $term = get_term( $saved_id );
                                                                    echo $term->name;
                                                                    restore_current_blog();
                                                                ?>                                               
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Email         
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'email', true ); ?>                                                                                                            </p>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?> 
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s padding-b-20">
                                                    You haven't added your personal information yet.
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVPersonalInformationModal">
                                                <?php echo ( $profile_query->found_posts > 0 ) ? 'Edit' : 'Create Profile'; ?>
                                            </a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add / Edit Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVPersonalInformationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=cv-personal-information'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">CV: Personal Informationr</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php

                                                    if( $_POST && $_GET['form'] == 'cv-personal-information' ){

                                                        /* Get Post Name */
                                                        $postName = 'Personal-Information-'.$current_user->ID;

                                                        /* Get Form Data */
                                                        $gender = sanitize_text_field( $_POST['gender'] ); 
                                                        $date_of_birth = sanitize_text_field( $_POST['date_of_birth'] ); 
                                                        $marital_status = sanitize_text_field( $_POST['marital_status'] ); 
                                                        $nationality = sanitize_text_field( $_POST['nationality'] ); 
                                                        $state_of_origin = sanitize_text_field( $_POST['state_of_origin'] ); 
                                                        $work_location = sanitize_text_field( $_POST['work_location'] ); 
                                                        $phone_work = sanitize_text_field( $_POST['phone_work'] ); 
                                                        $whatsapp_work = sanitize_text_field( $_POST['whatsapp_work'] ); 
                                                        $email = sanitize_text_field( $_POST['email'] ); 

                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => 'personal-info',
                                                            'post_title' => $postName,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        /* Add Meta */
                                                        update_post_meta( $post_id, 'gender', $gender );
                                                        update_post_meta( $post_id, 'date_of_birth', $date_of_birth );
                                                        update_post_meta( $post_id, 'marital_status', $marital_status );
                                                        update_post_meta( $post_id, 'nationality', $nationality );
                                                        update_post_meta( $post_id, 'state_of_origin', $state_of_origin );
                                                        update_post_meta( $post_id, 'work_location', $work_location );
                                                        update_post_meta( $post_id, 'phone_work', $phone_work );
                                                        update_post_meta( $post_id, 'whatsapp_work', $whatsapp_work );
                                                        update_post_meta( $post_id, 'email', $email );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <label for="gender">
                                                        Gender
                                                    </label>
                                                    <select name="gender" id="gender">
                                                        <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                                        <option value="Male" <?php echo ($meta == 'Male') ? 'selected' : '' ?>>Male</option>
                                                        <option value="Female" <?php echo ($meta == 'Female') ? 'selected' : '' ?>>Female</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="date_of_birth">
                                                        Year of Birth
                                                    </label>
                                                    <input 
                                                        type="year" 
                                                        name="date_of_birth" 
                                                        id="date_of_birth"
                                                        value="<?php echo get_post_meta( $post_id, 'date_of_birth', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="marital_status">
                                                        Marital Status
                                                    </label>
                                                    <select name="marital_status" id="marital_status">
                                                        <?php $meta = get_post_meta( $post_id, 'marital_status', true ); ?>
                                                        <option value="Single" <?php echo ($meta == 'Single') ? 'selected' : '' ?>>Single</option>
                                                        <option value="Married" <?php echo ($meta == 'Married') ? 'selected' : '' ?>>Married</option>
                                                        <option value="Divorced" <?php echo ($meta == 'Divorced') ? 'selected' : '' ?>>Divorced</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="nationality">
                                                        Nationality
                                                    </label>
                                                    <select name="nationality" id="nationality">
                                                        <?php $meta = get_post_meta( $post_id, 'nationality', true ); ?>
                                                        <option value="Nigerian" <?php echo ($meta == 'Nigerian') ? 'selected' : '' ?>>Nigerian</option>
                                                        <option value="Other" <?php echo ($meta == 'Other') ? 'selected' : '' ?>>Other</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="state_of_origin">
                                                        State of Origin
                                                    </label>
                                                    <select name="state_of_origin" id="state_of_origin">
                                                        <?php $meta = get_post_meta( $post_id, 'state_of_origin', true ); ?>
                                                        <?php 
                                                            switch_to_blog(110);
                                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                                            restore_current_blog();

                                                            foreach ($terms as $term) { 
                                                        ?>
                                                        <option value="<?php echo $term->term_id ?>" <?php echo ( $term->term_id == $meta ) ? 'selected' : ''; ?>><?php echo $term->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="work_location">
                                                        Where you currently reside
                                                    </label>
                                                    <select name="work_location" id="work_location">
                                                        <?php $meta = get_post_meta( $post_id, 'work_location', true ); ?>
                                                        <?php 
                                                            switch_to_blog(110);
                                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                                            restore_current_blog();

                                                            foreach ($terms as $term) { 
                                                        ?>
                                                        <option value="<?php echo $term->term_id ?>" <?php echo ( $term->term_id == $meta ) ? 'selected' : ''; ?>><?php echo $term->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="phone_work">
                                                        Mobile Number
                                                    </label>
                                                    <input 
                                                        type="telephone" 
                                                        name="phone_work" 
                                                        id="phone_work" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo get_post_meta( $post_id, 'phone_work', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="whatsapp_work">
                                                        Whatsapp Number
                                                    </label>
                                                    <input 
                                                        type="telephone" 
                                                        name="whatsapp_work" 
                                                        id="whatsapp_work" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo get_post_meta( $post_id, 'whatsapp_work', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="whatsapp_work">
                                                        Email
                                                    </label>
                                                    <input 
                                                        type="email" 
                                                        name="email" 
                                                        id="email" 
                                                        value="<?php echo get_post_meta( $post_id, 'email', true ); ?>"
                                                    >
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Education & Work experience snapshot -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'snapshot',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => 1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-snapshot-collapse" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">Education & Work experience snapshot</span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="cv-snapshot-collapse">
                                    <div class="padding-lr-30">
                                        <p class="txt-sm txt-light padding-t-10">
                                            Kindly provide highlight information about your secondary school and first degree education as well as your work experience
                                        </p>
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <div class="row row-10 padding-b-20 txt-color-light">
                                                        <div class="col-lg-6 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                O-level Examination          
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'o_level', true ); ?> |
                                                                <?php echo get_post_meta( $post_id, 'number_of_credits', true ); ?> Distinctions/Credits |
                                                                Maths <?php echo get_post_meta( $post_id, 'maths_grade', true ); ?>,
                                                                English <?php echo get_post_meta( $post_id, 'english_grade', true ); ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-6 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Best Subject         
                                                            </p>
                                                            <p class="txt-sm">
                                                                                                               
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-6 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Your First degree         
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'date_of_birth', true ); ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-6 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Highest qualification          
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'marital_status', true ); ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-6 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Work experience          
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'nationality', true ); ?>                                                
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-6 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Minimum salary expectation
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'phone_work', true ); ?>                                               
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-6 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                You are willing to work in        
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php 
                                                                    $saved_id = get_post_meta( $post_id, 'state_of_origin', true ); 
                                                                    switch_to_blog(110);
                                                                    $term = get_term( $saved_id );
                                                                    echo $term->name;
                                                                    restore_current_blog();
                                                                ?>        
                                                            </p>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?> 
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s padding-b-20">
                                                    You haven't added your snapshot yet.
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVSnapshotModal">
                                                <?php echo ( $profile_query->found_posts > 0 ) ? 'Edit' : 'Create Profile'; ?>
                                            </a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add / Edit Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVSnapshotModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=cv-snapshot'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">Education & Work experience snapshot</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php

                                                    if( $_POST && $_GET['form'] == 'cv-snapshot' ){

                                                        /* Get Post Name */
                                                        $postName = 'Snapshot-'.$current_user->ID;

                                                        /* Get Form Data */
                                                        $o_level = sanitize_text_field( $_POST['o_level'] ); 
                                                        $number_of_credits = sanitize_text_field( $_POST['number_of_credits'] ); 
                                                        $english_grade = sanitize_text_field( $_POST['english_grade'] ); 
                                                        $maths_grade = sanitize_text_field( $_POST['maths_grade'] ); 
                                                        $best_secondary_school_subject = sanitize_text_field( $_POST['best_secondary_school_subject'] ); 
                                                        $first_degree_institution_type = sanitize_text_field( $_POST['first_degree_institution_type'] ); 
                                                        $first_degree_country = sanitize_text_field( $_POST['first_degree_country'] ); 
                                                        $first_degree_grade = sanitize_text_field( $_POST['first_degree_grade'] ); 
                                                        $highest_qualification = sanitize_text_field( $_POST['highest_qualification'] ); 
                                                        $work_experience_post_secondary = sanitize_text_field( $_POST['work_experience_post_secondary'] ); 
                                                        $work_experience_years = sanitize_text_field( $_POST['work_experience_years'] ); 
                                                        $state_i_can_work = $_POST['state_i_can_work']; 


                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => 'snapshot',
                                                            'post_title' => $postName,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        /* Add Meta */
                                                        update_post_meta( $post_id, 'o_level', $o_level );
                                                        update_post_meta( $post_id, 'number_of_credits', $number_of_credits );
                                                        update_post_meta( $post_id, 'english_grade', $english_grade );
                                                        update_post_meta( $post_id, 'maths_grade', $maths_grade );
                                                        update_post_meta( $post_id, 'best_secondary_school_subject', $best_secondary_school_subject );
                                                        update_post_meta( $post_id, 'first_degree_institution_type', $first_degree_institution_type );
                                                        update_post_meta( $post_id, 'first_degree_country', $first_degree_country );
                                                        update_post_meta( $post_id, 'first_degree_grade', $first_degree_grade );
                                                        update_post_meta( $post_id, 'highest_qualification', $highest_qualification );
                                                        update_post_meta( $post_id, 'work_experience_post_secondary', $work_experience_post_secondary );
                                                        update_post_meta( $post_id, 'work_experience_years', $work_experience_years );
                                                        update_post_meta( $post_id, 'state_i_can_work', $state_i_can_work );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <label for="o_level">
                                                        What O-Level Certification do you have?
                                                    </label>
                                                    <select name="o_level" id="o_level">
                                                        <?php $meta = get_post_meta( $post_id, 'o_level', true ); ?>
                                                        <option value="SSCE" <?php echo ($meta == 'SSCE') ? 'selected' : '' ?>>SSCE</option>
                                                        <option value="GCE" <?php echo ($meta == 'GCE') ? 'selected' : '' ?>>GCE</option>
                                                        <option value="NECO" <?php echo ($meta == 'NECO') ? 'selected' : '' ?>>NECO</option>
                                                        <option value="Others" <?php echo ($meta == 'Others') ? 'selected' : '' ?>>Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="number_of_credits">
                                                        How many Credits & distinctions did you make?
                                                    </label>
                                                    <select name="number_of_credits" id="number_of_credits">
                                                        <?php $meta = get_post_meta( $post_id, 'o_level', true ); ?>
                                                        <option value="1" <?php echo ($meta == '1') ? 'selected' : '' ?>>1</option>
                                                        <option value="2" <?php echo ($meta == '2') ? 'selected' : '' ?>>2</option>
                                                        <option value="3" <?php echo ($meta == '3') ? 'selected' : '' ?>>3</option>
                                                        <option value="4" <?php echo ($meta == '4') ? 'selected' : '' ?>>4</option>
                                                        <option value="5" <?php echo ($meta == '5') ? 'selected' : '' ?>>5</option>
                                                        <option value="6" <?php echo ($meta == '6') ? 'selected' : '' ?>>6</option>
                                                        <option value="7" <?php echo ($meta == '7') ? 'selected' : '' ?>>7</option>
                                                        <option value="8" <?php echo ($meta == '8') ? 'selected' : '' ?>>8</option>
                                                        <option value="9" <?php echo ($meta == '9') ? 'selected' : '' ?>>9</option>
                                                        <option value="10" <?php echo ($meta == '10') ? 'selected' : '' ?>>10</option>
                                                        <option value="11" <?php echo ($meta == '11') ? 'selected' : '' ?>>11</option>
                                                        <option value="12" <?php echo ($meta == '12') ? 'selected' : '' ?>>12</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="english_grade">
                                                        What grade did you get in English at O-Levels?
                                                    </label>
                                                    <select name="english_grade" id="english_grade">
                                                        <?php $meta = get_post_meta( $post_id, 'o_level', true ); ?>
                                                        <option value="Distinction (A1,B2, or B3)" <?php echo ($meta == 'Distinction (A1,B2, or B3)') ? 'selected' : '' ?>>Distinction (A1,B2, or B3)</option>
                                                        <option value="Credit (C4 to C6)" <?php echo ($meta == 'Credit (C4 to C6)') ? 'selected' : '' ?>>Credit (C4 to C6)</option>
                                                        <option value="Pass (D7 & E8)" <?php echo ($meta == 'Pass (D7 & E8)') ? 'selected' : '' ?>>Pass (D7 & E8)</option>
                                                        <option value="Fail" <?php echo ($meta == 'Fail') ? 'selected' : '' ?>>Fail</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="maths_grade">
                                                        What grade did you get in Maths at O-Levels?
                                                    </label>
                                                    <select name="maths_grade" id="maths_grade">
                                                        <?php $meta = get_post_meta( $post_id, 'o_level', true ); ?>
                                                        <option value="Distinction (A1,B2, or B3)" <?php echo ($meta == 'Distinction (A1,B2, or B3)') ? 'selected' : '' ?>>Distinction (A1,B2, or B3)</option>
                                                        <option value="Credit (C4 to C6)" <?php echo ($meta == 'Credit (C4 to C6)') ? 'selected' : '' ?>>Credit (C4 to C6)</option>
                                                        <option value="Pass (D7 & E8)" <?php echo ($meta == 'Pass (D7 & E8)') ? 'selected' : '' ?>>Pass (D7 & E8)</option>
                                                        <option value="Fail" <?php echo ($meta == 'Fail') ? 'selected' : '' ?>>Fail</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="best_secondary_school_subject">
                                                        What was your best subject in Secondary School?
                                                    </label>
                                                    <select name="best_secondary_school_subject" id="best_secondary_school_subject">
                                                        <?php $meta = get_post_meta( $post_id, 'best_secondary_school_subject', true ); ?>
                                                        <option value="1" <?php echo ($meta == '1') ? 'selected' : '' ?>>1</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="first_degree_institution_type">
                                                        What sort of tertiary institution did you attend for your first degree?
                                                    </label>
                                                    <select name="first_degree_institution_type" id="first_degree_institution_type">
                                                        <?php $meta = get_post_meta( $post_id, 'first_degree_institution_type', true ); ?>
                                                        <option value="University" <?php echo ($meta == 'University') ? 'selected' : '' ?>>University</option>
                                                        <option value="Polytechnic" <?php echo ($meta == 'Polytechnic') ? 'selected' : '' ?>>Polytechnic</option>
                                                        <option value="College" <?php echo ($meta == 'College') ? 'selected' : '' ?>>College</option>
                                                        <option value="Vocational Institute" <?php echo ($meta == 'Vocational Institute') ? 'selected' : '' ?>>Vocational Institute</option>
                                                        <option value="Other" <?php echo ($meta == 'Other') ? 'selected' : '' ?>>Other</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="first_degree_country">
                                                        In what country did you study for your first degree?
                                                    </label>
                                                    <select name="first_degree_country" id="first_degree_country">
                                                        <?php $meta = get_post_meta( $post_id, 'first_degree_country', true ); ?>
                                                        <option value="1" <?php echo ($meta == '1') ? 'selected' : '' ?>>1</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="first_degree_grade">
                                                        What grade did you get in your first degree?
                                                    </label>
                                                    <select name="first_degree_grade" id="first_degree_grade">
                                                        <?php $meta = get_post_meta( $post_id, 'first_degree_grade', true ); ?>
                                                        <option value="First Class" <?php echo ($meta == 'First Class') ? 'selected' : '' ?>>First Class</option>
                                                        <option value="Distinction" <?php echo ($meta == 'Distinction') ? 'selected' : '' ?>>Distinction</option>
                                                        <option value="2nd Class Upper" <?php echo ($meta == '2nd Class Upper') ? 'selected' : '' ?>>2nd Class Upper</option>
                                                        <option value="Upper Credit" <?php echo ($meta == 'Upper Credit') ? 'selected' : '' ?>>Upper Credit</option>
                                                        <option value="2nd Class Lower" <?php echo ($meta == '2nd Class Lower') ? 'selected' : '' ?>>2nd Class Lower</option>
                                                        <option value="Lower Credit" <?php echo ($meta == 'Lower Credit') ? 'selected' : '' ?>>Lower Credit</option>
                                                        <option value="3rd Class" <?php echo ($meta == '3rd Class') ? 'selected' : '' ?>>3rd Class</option>
                                                        <option value="Pass" <?php echo ($meta == 'Pass') ? 'selected' : '' ?>>Pass</option>
                                                        <option value="Other" <?php echo ($meta == 'Other') ? 'selected' : '' ?>>Other</option>
                                                        <option value="Not Applicable" <?php echo ($meta == 'Not Applicable') ? 'selected' : '' ?>>Not Applicable</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="highest_qualification">
                                                        What is your highest educational Qualification
                                                    </label>
                                                    <select name="highest_qualification" id="highest_qualification">
                                                        <?php $meta = get_post_meta( $post_id, 'highest_qualification', true ); ?>
                                                        <option value="Secondary School Certificate" <?php echo ($meta == 'Secondary School Certificate') ? 'selected' : '' ?>>Secondary School Certificate</option>
                                                        <option value="Diploma" <?php echo ($meta == 'Diploma') ? 'selected' : '' ?>>Diploma</option>
                                                        <option value="HND" <?php echo ($meta == 'HND') ? 'selected' : '' ?>>HND</option>
                                                        <option value="Bachelors Degree" <?php echo ($meta == 'Bachelors Degree') ? 'selected' : '' ?>>Bachelors Degree</option>
                                                        <option value="Post Graduate Degree" <?php echo ($meta == 'Post Graduate Degree') ? 'selected' : '' ?>>Post Graduate Degree</option>
                                                        <option value="Masters" <?php echo ($meta == 'Masters') ? 'selected' : '' ?>>Masters</option>
                                                        <option value="Phd" <?php echo ($meta == 'Phd') ? 'selected' : '' ?>>Phd</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="work_experience_post_secondary">
                                                        Have you worked anywhere before (Post secondary school)?
                                                    </label>
                                                    <select name="work_experience_post_secondary" id="work_experience_post_secondary">
                                                        <?php $meta = get_post_meta( $post_id, 'work_experience_post_secondary', true ); ?>
                                                        <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?>>Yes</option>
                                                        <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?>>No</option>
                                                    </select>
                                                </div>
                                                <div class="form-item">
                                                    <label for="work_experience_years">
                                                        How long have you worked (Your combined post secondary school work experience)
                                                    </label>
                                                    <input 
                                                        type="number" 
                                                        name="work_experience_years" 
                                                        id="work_experience_years"
                                                        value="<?php echo get_post_meta( $post_id, 'work_experience_years', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="state_i_can_work">
                                                        In what state(s) in Nigeria are you willing to work in?
                                                    </label>
                                                    <?php 
                                                        /* Return Terms assigned to Post */
                                                        $term_list = get_post_meta( $post_id, 'state_i_can_work', true );

                                                        switch_to_blog(110);
                                                        $terms = get_terms( 'state', array('hide_empty' => false));
                                                        restore_current_blog();

                                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                                            // Check and see if the term is a top-level parent. If so, display it.
                                                            $parent = $term->parent;
                                                            $term_id = $term->term_id; //Get the term ID
                                                            $term_name = $term->name; //Get the term name
                                                    ?>

                                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                            <input
                                                                class="margin-r-5"
                                                                type="checkbox" 
                                                                value="<?php echo $term_id ?>" 
                                                                name="state_i_can_work[]" 
                                                                <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                                            >
                                                            <span class="bg-label padding-l-5">
                                                                <?php echo $term_name; ?>
                                                            </span>
                                                        </label>

                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Education & Training -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'education',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-education-collapse" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">Tell us about your education</span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="cv-education-collapse">
                                    <div class="padding-lr-30">
                                        <p class="txt-sm txt-light padding-t-10">What schools have you attended? When? What did you study? What degrees do you have?</p>
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <!-- Entry -->
                                                    <div class="row row-10 padding-b-20">
                                                        <figure class="col-auto padding-lr-10">
                                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/application_manager/education.png" alt="" width="40">
                                                        </figure>
                                                        <div class="col padding-lr-10 txt-color-light">
                                                            <p class="txt-normal-s txt-medium">
                                                                <?php echo get_post_meta( $post_id, 'school', true ); ?>
                                                                <span class="padding-l-10">(
                                                                    <?php 
                                                                        $date = strtotime( get_post_meta( $post_id, 'start_date', true ) );
                                                                        echo date('M Y', $date);
                                                                          
                                                                        echo ' - ';
                                                                                       
                                                                        $date = strtotime( get_post_meta( $post_id, 'end_date', true ) );
                                                                        echo date('M Y', $date);
                                                                    ?>
                                                                    )
                                                                </span>
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_the_title() ?> |
                                                                <?php echo get_post_meta( $post_id, 'field_of_study', true ); ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                            <a class="txt-green" data-toggle="modal" href="#myCVEditEducatiionModal-<?php echo $post_id ?>">
                                                                <i class="fa fa-pencil"></i>
                                                                Edit
                                                            </a>
                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-education-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <!-- Edit Modal -->
                                                    <div class="modal fade font-main filter-modal" id="myCVEditEducatiionModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                                <form action="<?php echo currentUrl(false).'&form=cv-edit-education-'.$post_id; ?>" method="post">
                                                                    <div class="modal-header padding-lr-30">
                                                                        <h5 class="modal-title" id="exampleModalLabel">CV: Edit Education</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body padding-o-30 form">
                                                                        <?php

                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-education-'.$post_id ){
                                                                                wp_delete_post($post_id); // Delete
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }

                                                                            if( $_POST && $_GET['form'] == 'cv-edit-education-'.$post_id ){

                                                                                /* Get Post Data */
                                                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                                                $school = sanitize_text_field( $_POST['school'] ); 
                                                                                $field_of_study = sanitize_text_field( $_POST['field_of_study'] ); 
                                                                                $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                                                                $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                                                                $achievements = sanitize_text_field( $_POST['achievements'] ); 

                                                                                /* Save Post to DB */
                                                                                $post_id = wp_insert_post(array (
                                                                                    'ID' => $post_id,
                                                                                    'post_type' => 'education',
                                                                                    'post_title' => $post_name,
                                                                                    'post_content' => "",
                                                                                    'post_status' => 'publish',
                                                                                ));

                                                                                /* Add Post Meta */
                                                                                update_post_meta( $post_id, 'school', $school );
                                                                                update_post_meta( $post_id, 'field_of_study', $field_of_study );
                                                                                update_post_meta( $post_id, 'start_date', $start_date );
                                                                                update_post_meta( $post_id, 'end_date', $end_date );
                                                                                update_post_meta( $post_id, 'achievements', $achievements );

                                                                                /* Redirect */
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }
                                                                        ?>
                                                                        <div class="form-item">
                                                                            <label for="deadline">
                                                                                Degree Attained (e.g BA, BS, JD, PhD)
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="post_name" 
                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="school">
                                                                                School
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="school" 
                                                                                value="<?php echo get_post_meta( $post_id, 'school', true ); ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="field_of_study">
                                                                                Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="field_of_study" 
                                                                                value="<?php echo get_post_meta( $post_id, 'field_of_study', true ); ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="start_date">
                                                                                Start Month/Year
                                                                            </label>
                                                                            <input 
                                                                                type="month" 
                                                                                name="start_date" 
                                                                                value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="end_date">
                                                                                End Month/Year (Current students: Enter your expected graduation year)
                                                                            </label>
                                                                            <input 
                                                                                type="month" 
                                                                                name="end_date" 
                                                                                value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                                                                            >
                                                                        </div>
                                                                        <!--<div class="form-item">
                                                                            <label for="achievements">
                                                                                Achievements
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="achievements" 
                                                                                value="<?php echo get_post_meta( $post_id, 'achievements', true ); ?>"
                                                                            >
                                                                        </div>-->
                                                                    </div>
                                                                    <div class="modal-footer padding-lr-30">
                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?> 
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s padding-b-20">
                                                    You haven't added your education yet.
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddEducatiionModal">Add New</a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVAddEducatiionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=cv-add-education'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">CV: Education</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php
                                                    $post_id = 0;

                                                    if( $_POST && $_GET['form'] == 'cv-add-education' ){

                                                        /* Get Post Data */
                                                        $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                        $school = sanitize_text_field( $_POST['school'] ); 
                                                        $field_of_study = sanitize_text_field( $_POST['field_of_study'] ); 
                                                        $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                                        $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                                        $achievements = sanitize_text_field( $_POST['achievements'] ); 


                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => 'education',
                                                            'post_title' => $post_name,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        /* Add Post Meta */
                                                        update_post_meta( $post_id, 'school', $school );
                                                        update_post_meta( $post_id, 'field_of_study', $field_of_study );
                                                        update_post_meta( $post_id, 'start_date', $start_date );
                                                        update_post_meta( $post_id, 'end_date', $end_date );
                                                        update_post_meta( $post_id, 'achievements', $achievements );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <label for="deadline">
                                                        Degree Attained (e.g BA, BS, JD, PhD)
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="post_name" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="school">
                                                        School
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="school" 
                                                        value="<?php echo get_post_meta( $post_id, 'school', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="field_of_study">
                                                        Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="field_of_study" 
                                                        value="<?php echo get_post_meta( $post_id, 'field_of_study', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="start_date">
                                                        Start Month/Year
                                                    </label>
                                                    <input 
                                                        type="month" 
                                                        name="start_date" 
                                                        value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="end_date">
                                                        End Month/Year (Current students: Enter your expected graduation year)
                                                    </label>
                                                    <input 
                                                        type="month" 
                                                        name="end_date" 
                                                        value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                                                    >
                                                </div>
                                                <!--<div class="form-item">
                                                    <label for="achievements">
                                                        Achievements
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="achievements" 
                                                        value="<?php echo get_post_meta( $post_id, 'achievements', true ); ?>"
                                                    >
                                                </div>-->
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Work Experience -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'work-experience',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-experience-collapse" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">Tell us about your previous experience</span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="cv-experience-collapse">
                                    <div class="padding-lr-30">
                                        <p class="txt-sm txt-light padding-t-10">Where have you worked in the past? What roles did you handle? State the period from and to when you performed in this role?</p>
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <!-- Entry -->
                                                    <div class="row row-10 padding-b-20">
                                                        <figure class="col-auto padding-lr-10">
                                                            <div class="initials bg-ash txt-lg txt-bold txt-color-white">
                                                                <?php 
                                                                    $meta = get_post_meta( $post_id, 'company', true );
                                                                    echo $meta[0];
                                                                ?>
                                                            </div>
                                                        </figure>
                                                        <div class="col padding-lr-10 txt-color-light">
                                                            <p class="txt-normal-s txt-medium">
                                                                <?php echo get_post_meta( $post_id, 'company', true ); ?>,
                                                                <?php echo get_post_meta( $post_id, 'location', true ); ?>
                                                                <span class="padding-l-10">(
                                                                    <?php 
                                                                        $date = strtotime( get_post_meta( $post_id, 'start_date', true ) );
                                                                        echo date('M Y', $date);
                                                                          
                                                                        echo ' - ';
                                                                                       
                                                                        $date = strtotime( get_post_meta( $post_id, 'end_date', true ) );
                                                                        echo date('M Y', $date);
                                                                    ?>
                                                                    )
                                                                </span>
                                                            </p>
                                                            <p class="txt-sm txt-medium font-italic padding-b-10">
                                                                <?php echo get_the_title() ?>
                                                            </p>
                                                            <article class="text-box txt-sm txt-height-1-4">
                                                                <?php echo get_post_meta( $post_id, 'description', true ); ?>
                                                            </article>
                                                        </div>
                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                            <a class="txt-green" data-toggle="modal" href="#myCVEditExperienceModal-<?php echo $post_id ?>">
                                                                <i class="fa fa-pencil"></i>
                                                                Edit
                                                            </a>
                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-experience-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <!-- Edit Modal -->
                                                    <div class="modal fade font-main filter-modal" id="myCVEditExperienceModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                                <form action="<?php echo currentUrl(false).'&form=cv-edit-experience-'.$post_id; ?>" method="post">
                                                                    <div class="modal-header padding-lr-30">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Experience</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body padding-o-30 form">
                                                                        <?php

                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-experience-'.$post_id ){
                                                                                wp_delete_post($post_id); // Delete
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }

                                                                            if( $_POST && $_GET['form'] == 'cv-edit-experience-'.$post_id ){

                                                                                /* Get Post Data */
                                                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                                                $company = sanitize_text_field( $_POST['company'] ); 
                                                                                $location = sanitize_text_field( $_POST['location'] ); 
                                                                                $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                                                                $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                                                                $description = sanitize_text_field( $_POST['description'] ); 

                                                                                /* Save Post to DB */
                                                                                $post_id = wp_insert_post(array (
                                                                                    'ID' => $post_id,
                                                                                    'post_type' => 'work-experience',
                                                                                    'post_title' => $post_name,
                                                                                    'post_content' => "",
                                                                                    'post_status' => 'publish',
                                                                                ));

                                                                                /* Add Post Meta */
                                                                                update_post_meta( $post_id, 'company', $company );
                                                                                update_post_meta( $post_id, 'location', $location );
                                                                                update_post_meta( $post_id, 'start_date', $start_date );
                                                                                update_post_meta( $post_id, 'end_date', $end_date );
                                                                                update_post_meta( $post_id, 'description', $description );

                                                                                /* Redirect */
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }
                                                                        ?>
                                                                        <div class="form-item">
                                                                            <label for="deadline">
                                                                                Position/Job Title
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="post_name" 
                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="company">
                                                                                Company
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="company" 
                                                                                value="<?php echo get_post_meta( $post_id, 'company', true ); ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="location">
                                                                                Location
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="location" 
                                                                                value="<?php echo get_post_meta( $post_id, 'location', true ); ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="start_date">
                                                                                Start Month/Year
                                                                            </label>
                                                                            <input 
                                                                                type="month" 
                                                                                name="start_date" 
                                                                                value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="end_date">
                                                                                End Month/Year
                                                                            </label>
                                                                            <input 
                                                                                type="month" 
                                                                                name="end_date" 
                                                                                value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="description">
                                                                                Describe what you did/Your Achievements
                                                                            </label>
                                                                            <textarea id="description" name="description" class="editor" ><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer padding-lr-30">
                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?>
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s txt-color-red padding-b-20">
                                                    You haven't added your experience yet.
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddExperienceModal">Add New</a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVAddExperienceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=cv-add-experience'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">Add an Experience</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php
                                                    $post_id = 0;

                                                    if( $_POST && $_GET['form'] == 'cv-add-experience' ){

                                                        /* Get Post Data */
                                                        $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                        $company = sanitize_text_field( $_POST['company'] ); 
                                                        $location = sanitize_text_field( $_POST['location'] ); 
                                                        $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                                        $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                                        $description = sanitize_text_field( $_POST['description'] ); 

                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => 'work-experience',
                                                            'post_title' => $post_name,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        /* Add Post Meta */
                                                        update_post_meta( $post_id, 'company', $company );
                                                        update_post_meta( $post_id, 'location', $location );
                                                        update_post_meta( $post_id, 'start_date', $start_date );
                                                        update_post_meta( $post_id, 'end_date', $end_date );
                                                        update_post_meta( $post_id, 'description', $description );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <label for="deadline">
                                                        Position/Job Title
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="post_name" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="company">
                                                        Company
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="company" 
                                                        value="<?php echo get_post_meta( $post_id, 'company', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="location">
                                                        Location
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="location" 
                                                        value="<?php echo get_post_meta( $post_id, 'location', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="start_date">
                                                        Start Month/Year
                                                    </label>
                                                    <input 
                                                        type="month" 
                                                        name="start_date" 
                                                        value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="end_date">
                                                        End Month/Year
                                                    </label>
                                                    <input 
                                                        type="month" 
                                                        name="end_date" 
                                                        value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="description">
                                                        Describe what you did/Your Achievements
                                                    </label>
                                                    <textarea id="description" name="description" class="editor" ><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Interests -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'interest',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-interest-collapse" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">Let's know your interests</span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="cv-interest-collapse">
                                    <div class="padding-lr-30">
                                        <p class="txt-sm txt-light padding-t-10">What do you love to do? What are you passionate about?</p>
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <!-- Entry -->
                                                    <div class="row row-10 padding-b-20 txt-color-light">
                                                        <div class="col-4 col-lg-2 padding-lr-10">
                                                            <p class="txt-normal-s txt-medium"><?php echo get_the_title() ?></p>
                                                        </div>
                                                        <article class="col padding-lr-10 text-box txt-sm">
                                                            <?php echo get_post_meta( $post_id, 'description', true ); ?>
                                                        </article>
                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                            <a class="txt-green" data-toggle="modal" href="#myCVInterestModal-<?php echo $post_id ?>">
                                                                <i class="fa fa-pencil"></i>
                                                                Edit
                                                            </a>
                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-interest-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <!-- Edit Modal -->
                                                    <div class="modal fade font-main filter-modal" id="myCVInterestModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                                <form action="<?php echo currentUrl(false).'&form=cv-edit-interest-'.$post_id; ?>" method="post">
                                                                    <div class="modal-header padding-lr-30">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit this Interest Area</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body padding-o-30 form">
                                                                        <?php

                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-interest-'.$post_id ){
                                                                                wp_delete_post($post_id); // Delete
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }

                                                                            if( $_POST && $_GET['form'] == 'cv-edit-interest-'.$post_id ){

                                                                                /* Get Post Data */
                                                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                                                $description = wp_kses_post( $_POST['description'] );

                                                                                /* Save Post to DB */
                                                                                $post_id = wp_insert_post(array (
                                                                                    'ID' => $post_id,
                                                                                    'post_type' => 'interest',
                                                                                    'post_title' => $post_name,
                                                                                    'post_content' => "",
                                                                                    'post_status' => 'publish',
                                                                                ));

                                                                                /* Add Post Meta */
                                                                                update_post_meta( $post_id, 'description', $description );

                                                                                /* Redirect */
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }
                                                                        ?>
                                                                        <div class="form-item">
                                                                            <label for="deadline">
                                                                                Title
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="post_name" 
                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="description">
                                                                                Description
                                                                            </label>
                                                                            <textarea id="description" name="description" class="editor" ><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer padding-lr-30">
                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?> 
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s padding-b-20">
                                                    You haven't added your education yet.
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddInterestModal">Add New</a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVAddInterestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=cv-add-interest'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">Add an Interest Area</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php
                                                    $post_id = 0;

                                                    if( $_POST && $_GET['form'] == 'cv-add-interest' ){

                                                        /* Get Post Data */
                                                        $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                        $description = wp_kses_post( $_POST['description'] );

                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => 'interest',
                                                            'post_title' => $post_name,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        /* Add Post Meta */
                                                        update_post_meta( $post_id, 'description', $description );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <label for="deadline">
                                                        Title
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="post_name" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="description">
                                                        Description
                                                    </label>
                                                    <textarea id="description" name="description" class="editor" ><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Languages -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'language',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-languages-collapse" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">What languages can you speak?</span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="cv-languages-collapse">
                                    <div class="padding-lr-30">
                                        <p class="txt-sm txt-light padding-t-10">Apart from English, tell us other languages you speak</p>
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <!-- Entry -->
                                                    <div class="row row-10 padding-b-20 txt-color-light">
                                                        <div class="col-md-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Language        
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_the_title() ?>                                            
                                                            </p>
                                                        </div>
                                                        <div class="col-md-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Spoken Level    
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'spoken_level', true ); ?>                                            
                                                            </p>
                                                        </div>
                                                        <div class="col-md-3 padding-lr-10 padding-b-20">
                                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                Written Level    
                                                            </p>
                                                            <p class="txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'written_level', true ); ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-3 padding-lr-10 text-right txt-sm">
                                                            <a class="txt-green" data-toggle="modal" href="#myCVLanguageModal-<?php echo $post_id ?>">
                                                                <i class="fa fa-pencil"></i>
                                                                Edit
                                                            </a>
                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-language-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <!-- Edit Modal -->
                                                    <div class="modal fade font-main filter-modal" id="myCVLanguageModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                                <form action="<?php echo currentUrl(false).'&form=cv-edit-language-'.$post_id; ?>" method="post">
                                                                    <div class="modal-header padding-lr-30">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Language</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body padding-o-30 form">
                                                                        <?php

                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-language-'.$post_id ){
                                                                                wp_delete_post($post_id); // Delete
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }

                                                                            if( $_POST && $_GET['form'] == 'cv-edit-language-'.$post_id ){

                                                                                /* Get Post Data */
                                                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                                                $spoken_level = sanitize_text_field( $_POST['spoken_level'] );
                                                                                $written_level = sanitize_text_field( $_POST['written_level'] );

                                                                                /* Save Post to DB */
                                                                                $post_id = wp_insert_post(array (
                                                                                    'ID' => $post_id,
                                                                                    'post_type' => 'language',
                                                                                    'post_title' => $post_name,
                                                                                    'post_content' => "",
                                                                                    'post_status' => 'publish',
                                                                                ));

                                                                                /* Add Post Meta */
                                                                                update_post_meta( $post_id, 'spoken_level', $spoken_level );
                                                                                update_post_meta( $post_id, 'written_level', $written_level );

                                                                                /* Redirect */
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }
                                                                        ?>
                                                                        <div class="form-item">
                                                                            <div class="form-group">
                                                                                <label for="post_name">
                                                                                    Language
                                                                                </label>
                                                                                <select name="post_name" id="post_name">
                                                                                    <?php $meta = get_the_title( $post_id ); ?>
                                                                                    <option value="English" <?php echo ($meta == 'English') ? 'selected' : '' ?>>English</option>
                                                                                    <option value="Yoruba" <?php echo ($meta == 'Yoruba') ? 'selected' : '' ?>>Yoruba</option>
                                                                                    <option value="Hausa" <?php echo ($meta == 'Hausa') ? 'selected' : '' ?>>Hausa</option>
                                                                                    <option value="Fulani" <?php echo ($meta == 'Fulani') ? 'selected' : '' ?>>Fulani</option>
                                                                                    <option value="Ibo" <?php echo ($meta == 'Ibo') ? 'selected' : '' ?>>Ibo</option>
                                                                                    <option value="Swahili" <?php echo ($meta == 'Swahili') ? 'selected' : '' ?>>Swahili</option>
                                                                                    <option value="Chinese" <?php echo ($meta == 'Chinese') ? 'selected' : '' ?>>Chinese</option>
                                                                                    <option value="French" <?php echo ($meta == 'French') ? 'selected' : '' ?>>French</option>
                                                                                    <option value="Arabic" <?php echo ($meta == 'Arabic') ? 'selected' : '' ?>>Arabic</option>
                                                                                    <option value="Portuguese" <?php echo ($meta == 'Portuguese') ? 'selected' : '' ?>>Portuguese</option>
                                                                                    <option value="Spanish" <?php echo ($meta == 'Spanish') ? 'selected' : '' ?>>Spanish</option>
                                                                                    <option value="German" <?php echo ($meta == 'German') ? 'selected' : '' ?>>German</option>
                                                                                    <option value="Hindi" <?php echo ($meta == 'Hindi') ? 'selected' : '' ?>>Hindi</option>
                                                                                    <option value="Gujarati" <?php echo ($meta == 'Gujarati') ? 'selected' : '' ?>>Gujarati</option>
                                                                                    <option value="Punjabi" <?php echo ($meta == 'Punjabi') ? 'selected' : '' ?>>Punjabi</option>
                                                                                    <option value="Swedish" <?php echo ($meta == 'Swedish') ? 'selected' : '' ?>>Swedish</option>
                                                                                    <option value="Japanese" <?php echo ($meta == 'Japanese') ? 'selected' : '' ?>>Japanese</option>
                                                                                    <option value="Russian" <?php echo ($meta == 'Russian') ? 'selected' : '' ?>>Russian</option>
                                                                                    <option value="Korean" <?php echo ($meta == 'Korean') ? 'selected' : '' ?>>Korean</option>
                                                                                    <option value="Turkish" <?php echo ($meta == 'Turkish') ? 'selected' : '' ?>>Turkish</option>
                                                                                    <option value="Italian" <?php echo ($meta == 'Italian') ? 'selected' : '' ?>>Italian</option>
                                                                                    <option value="Finnish" <?php echo ($meta == 'Finnish') ? 'selected' : '' ?>>Finnish</option>
                                                                                    <option value="Polish" <?php echo ($meta == 'Polish') ? 'selected' : '' ?>>Polish</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <div class="form-group">
                                                                                <label for="spoken_level">
                                                                                    Spoken level
                                                                                </label>
                                                                                <select name="spoken_level" id="spoken_level">
                                                                                    <?php $meta = get_post_meta( $post_id, 'spoken_level', true ); ?>
                                                                                    <option value="None" <?php echo ($meta == 'None') ? 'selected' : '' ?>>None</option>
                                                                                    <option value="Basic" <?php echo ($meta == 'Basic') ? 'selected' : '' ?>>Basic</option>
                                                                                    <option value="Fluent" <?php echo ($meta == 'Fluent') ? 'selected' : '' ?>>Fluent</option>
                                                                                    <option value="Native" <?php echo ($meta == 'Native') ? 'selected' : '' ?>>Native</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <div class="form-group">
                                                                                <label for="written_level">
                                                                                    Written level
                                                                                </label>
                                                                                <select name="written_level" id="written_level">
                                                                                    <?php $meta = get_post_meta( $post_id, 'written_level', true ); ?>
                                                                                    <option value="None" <?php echo ($meta == 'None') ? 'selected' : '' ?>>None</option>
                                                                                    <option value="Basic" <?php echo ($meta == 'Basic') ? 'selected' : '' ?>>Basic</option>
                                                                                    <option value="Fluent" <?php echo ($meta == 'Fluent') ? 'selected' : '' ?>>Fluent</option>
                                                                                    <option value="Native" <?php echo ($meta == 'Native') ? 'selected' : '' ?>>Native</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer padding-lr-30">
                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?> 
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s txt-color-red padding-b-20">
                                                    Are you sure you don't speak any other languages
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddLanguageModal">Add New</a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVAddLanguageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=cv-add-language'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">Add a Language</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php
                                                    $post_id = 0;

                                                    if( $_POST && $_GET['form'] == 'cv-add-language' ){

                                                        /* Get Post Data */
                                                        $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                        $spoken_level = sanitize_text_field( $_POST['spoken_level'] );
                                                        $written_level = sanitize_text_field( $_POST['written_level'] );

                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => 'language',
                                                            'post_title' => $post_name,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        /* Add Post Meta */
                                                        update_post_meta( $post_id, 'spoken_level', $spoken_level );
                                                        update_post_meta( $post_id, 'written_level', $written_level );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <div class="form-group">
                                                        <label for="post_name">
                                                            Language
                                                        </label>
                                                        <select name="post_name" id="post_name">
                                                            <?php $meta = get_the_title( $post_id ); ?>
                                                            <option value="English" <?php echo ($meta == 'English') ? 'selected' : '' ?>>English</option>
                                                            <option value="Yoruba" <?php echo ($meta == 'Yoruba') ? 'selected' : '' ?>>Yoruba</option>
                                                            <option value="Hausa" <?php echo ($meta == 'Hausa') ? 'selected' : '' ?>>Hausa</option>
                                                            <option value="Fulani" <?php echo ($meta == 'Fulani') ? 'selected' : '' ?>>Fulani</option>
                                                            <option value="Ibo" <?php echo ($meta == 'Ibo') ? 'selected' : '' ?>>Ibo</option>
                                                            <option value="Swahili" <?php echo ($meta == 'Swahili') ? 'selected' : '' ?>>Swahili</option>
                                                            <option value="Chinese" <?php echo ($meta == 'Chinese') ? 'selected' : '' ?>>Chinese</option>
                                                            <option value="French" <?php echo ($meta == 'French') ? 'selected' : '' ?>>French</option>
                                                            <option value="Arabic" <?php echo ($meta == 'Arabic') ? 'selected' : '' ?>>Arabic</option>
                                                            <option value="Portuguese" <?php echo ($meta == 'Portuguese') ? 'selected' : '' ?>>Portuguese</option>
                                                            <option value="Spanish" <?php echo ($meta == 'Spanish') ? 'selected' : '' ?>>Spanish</option>
                                                            <option value="German" <?php echo ($meta == 'German') ? 'selected' : '' ?>>German</option>
                                                            <option value="Hindi" <?php echo ($meta == 'Hindi') ? 'selected' : '' ?>>Hindi</option>
                                                            <option value="Gujarati" <?php echo ($meta == 'Gujarati') ? 'selected' : '' ?>>Gujarati</option>
                                                            <option value="Punjabi" <?php echo ($meta == 'Punjabi') ? 'selected' : '' ?>>Punjabi</option>
                                                            <option value="Swedish" <?php echo ($meta == 'Swedish') ? 'selected' : '' ?>>Swedish</option>
                                                            <option value="Japanese" <?php echo ($meta == 'Japanese') ? 'selected' : '' ?>>Japanese</option>
                                                            <option value="Russian" <?php echo ($meta == 'Russian') ? 'selected' : '' ?>>Russian</option>
                                                            <option value="Korean" <?php echo ($meta == 'Korean') ? 'selected' : '' ?>>Korean</option>
                                                            <option value="Turkish" <?php echo ($meta == 'Turkish') ? 'selected' : '' ?>>Turkish</option>
                                                            <option value="Italian" <?php echo ($meta == 'Italian') ? 'selected' : '' ?>>Italian</option>
                                                            <option value="Finnish" <?php echo ($meta == 'Finnish') ? 'selected' : '' ?>>Finnish</option>
                                                            <option value="Polish" <?php echo ($meta == 'Polish') ? 'selected' : '' ?>>Polish</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-item">
                                                    <div class="form-group">
                                                        <label for="spoken_level">
                                                            Spoken level
                                                        </label>
                                                        <select name="spoken_level" id="spoken_level">
                                                            <?php $meta = get_post_meta( $post_id, 'spoken_level', true ); ?>
                                                            <option value="None" <?php echo ($meta == 'None') ? 'selected' : '' ?>>None</option>
                                                            <option value="Basic" <?php echo ($meta == 'Basic') ? 'selected' : '' ?>>Basic</option>
                                                            <option value="Fluent" <?php echo ($meta == 'Fluent') ? 'selected' : '' ?>>Fluent</option>
                                                            <option value="Native" <?php echo ($meta == 'Native') ? 'selected' : '' ?>>Native</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-item">
                                                    <div class="form-group">
                                                        <label for="written_level">
                                                            Written level
                                                        </label>
                                                        <select name="written_level" id="written_level">
                                                            <?php $meta = get_post_meta( $post_id, 'written_level', true ); ?>
                                                            <option value="None" <?php echo ($meta == 'None') ? 'selected' : '' ?>>None</option>
                                                            <option value="Basic" <?php echo ($meta == 'Basic') ? 'selected' : '' ?>>Basic</option>
                                                            <option value="Fluent" <?php echo ($meta == 'Fluent') ? 'selected' : '' ?>>Fluent</option>
                                                            <option value="Native" <?php echo ($meta == 'Native') ? 'selected' : '' ?>>Native</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Certifications -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'certification',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-certifications-collapse" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">Let's know your certifications & affiliations </span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="cv-certifications-collapse">
                                    <div class="padding-lr-30">
                                        <p class="txt-sm txt-light padding-t-10">What is the name of your center, your training models, locations where you have centers and contact information?</p>
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <!-- Entry -->
                                                    <div class="row row-10 padding-b-20 txt-color-light">
                                                        <article class="col padding-lr-10">
                                                            <p class="txt-normal-s txt-medium"><?php echo get_the_title() ?></p>
                                                            <article class="text-box txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'description', true ); ?>
                                                            </article>
                                                        </article>
                                                        <div class="col-4 col-lg-2 padding-lr-10">
                                                            <p class="txt-normal-s txt-medium font-italic"><?php echo get_post_meta( $post_id, 'type', true ); ?></p>
                                                        </div>
                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                            <a class="txt-green" data-toggle="modal" href="#myCVCertificationModal-<?php echo $post_id ?>">
                                                                <i class="fa fa-pencil"></i>
                                                                Edit
                                                            </a>
                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-certification-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <!-- Edit Modal -->
                                                    <div class="modal fade font-main filter-modal" id="myCVCertificationModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                                <form action="<?php echo currentUrl(false).'&form=cv-edit-certification-'.$post_id; ?>" method="post">
                                                                    <div class="modal-header padding-lr-30">
                                                                        <h5 class="modal-title" id="exampleModalLabel">CV: Edit Interest</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body padding-o-30 form">
                                                                        <?php

                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-certification-'.$post_id ){
                                                                                wp_delete_post($post_id); // Delete
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }

                                                                            if( $_POST && $_GET['form'] == 'cv-edit-certification-'.$post_id ){

                                                                                /* Get Post Data */
                                                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                                                $type = sanitize_text_field( $_POST['type'] );
                                                                                $description = wp_kses_post( $_POST['description'] );

                                                                                /* Save Post to DB */
                                                                                $post_id = wp_insert_post(array (
                                                                                    'ID' => $post_id,
                                                                                    'post_type' => 'certification',
                                                                                    'post_title' => $post_name,
                                                                                    'post_content' => "",
                                                                                    'post_status' => 'publish',
                                                                                ));

                                                                                /* Add Post Meta */
                                                                                update_post_meta( $post_id, 'type', $type );
                                                                                update_post_meta( $post_id, 'description', $description );

                                                                                /* Redirect */
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }
                                                                        ?>
                                                                        <div class="form-item">
                                                                            <label for="deadline">
                                                                                Name
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="post_name" 
                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="type">Certification or Affiliation?</label>
                                                                            <select name="type" id="type">
                                                                                <?php $meta = get_post_meta( $post_id, 'type', true ); ?>
                                                                                <option value="Certification" <?php echo ($meta == 'Certification') ? 'selected' : '' ?>>Certification</option>
                                                                                <option value="Affiliation" <?php echo ($meta == 'Affiliation') ? 'selected' : '' ?>>Affiliation</option>
                                                                            </select>
                                                                        </div>                                        
                                                                        <div class="form-item">
                                                                            <label for="description">
                                                                                Desription
                                                                            </label>
                                                                            <textarea id="description" name="description" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer padding-lr-30">
                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?> 
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s padding-b-20">
                                                    You haven't added your education yet.
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddCertificationModal">Add New</a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVAddCertificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=cv-add-certification'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">Add a Certification</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php
                                                    $post_id = 0;

                                                    if( $_POST && $_GET['form'] == 'cv-add-certification' ){

                                                        /* Get Post Data */
                                                        $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                        $type = sanitize_text_field( $_POST['type'] );
                                                        $description = wp_kses_post( $_POST['description'] );

                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => 'certification',
                                                            'post_title' => $post_name,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        /* Add Post Meta */
                                                        update_post_meta( $post_id, 'type', $type );
                                                        update_post_meta( $post_id, 'description', $description );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <label for="deadline">
                                                        Name
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="post_name" 
                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="type">Certification or Affiliation?</label>
                                                    <select name="type" id="type">
                                                        <?php $meta = get_post_meta( $post_id, 'type', true ); ?>
                                                        <option value="Certification" <?php echo ($meta == 'Certification') ? 'selected' : '' ?>>Certification</option>
                                                        <option value="Affiliation" <?php echo ($meta == 'Affiliation') ? 'selected' : '' ?>>Affiliation</option>
                                                    </select>
                                                </div>                                        
                                                <div class="form-item">
                                                    <label for="description">
                                                        Desription
                                                    </label>
                                                    <textarea id="description" name="description" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Social Profile -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'social-profile',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => 1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-2" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">Your Social Media & Contact Details</span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="faq-2">
                                    <div class="padding-lr-30">
                                        <p class="txt-sm txt-light padding-t-10">What schools have you attended? When? What did you study? What degrees do you have?</p>
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <!-- Entry -->
                                                    <p class="txt-normal-s padding-b-10">
                                                        <?php echo get_post_meta( $post_id, 'address', true ); ?>
                                                    </p>
                                                    <p class="txt-normal-s padding-b-15">
                                                        You currently reside in <?php echo get_post_meta( $post_id, 'state', true ); ?>
                                                    </p>
                                                    <div class="row row-10 padding-b-20 txt-color-light">
                                                        <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                                            <p class="txt-sm txt-medium">
                                                                <i class="fa fa-facebook"></i>
                                                                <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'facebook', true ); ?></span>
                                                            </p>
                                                        </div>
                                                        <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                                            <p class="txt-sm txt-medium">
                                                                <i class="fa fa-twitter"></i>
                                                                <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'twitter', true ); ?></span>
                                                            </p>
                                                        </div>
                                                        <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                                            <p class="txt-sm txt-medium">
                                                                <i class="fa fa-instagram"></i>
                                                                <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'instagram', true ); ?></span>
                                                            </p>
                                                        </div>
                                                        <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                                            <p class="txt-sm txt-medium">
                                                                <i class="fa fa-linkedin"></i>
                                                                <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'linkedin', true ); ?></span>
                                                            </p>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?> 
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s padding-b-20">
                                                    You haven't added your social profile yet.
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddSocialDetailsModal">Add New</a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVAddSocialDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=cv-social-profile'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">My Social Profile</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php
                                                    $post_id = 0;
                                      
                                                    $post_type = 'social-profile';
                                                    $profile_query = new WP_Query();
                                                    $profile_query->query( 
                                                        array(
                                                            'post_type' => $post_type,
                                                            'post_status' => 'publish',
                                                            'author' => $current_user->ID,
                                                            'posts_per_page' => 1,
                                                        ) 
                                                    );

                                                    if ( $profile_query->have_posts() ) {

                                                        while ($profile_query->have_posts()) : $profile_query->the_post();

                                                        /* Variables */
                                                        $post_id = $post->ID;   //Get Program ID

                                                        endwhile;

                                                    }

                                                    if( $_POST && $_GET['form'] == 'cv-social-profile' ){

                                                        /* Get Post Name */
                                                        $postName = 'social-Profile-'.$current_user->ID;

                                                        /* Meta */
                                                        $email = sanitize_text_field( $_POST['email'] ); 
                                                        $phone = sanitize_text_field( $_POST['phone'] ); 
                                                        $website = sanitize_text_field( $_POST['website'] ); 
                                                        $facebook = sanitize_text_field( $_POST['facebook'] ); 
                                                        $twitter = sanitize_text_field( $_POST['twitter'] ); 
                                                        $instagram = sanitize_text_field( $_POST['instagram'] ); 
                                                        $linkedin = sanitize_text_field( $_POST['linkedin'] ); 
                                                        $address= sanitize_text_field( $_POST['address'] ); 

                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => $post_type,
                                                            'post_title' => $postName,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        update_post_meta( $post_id, 'email', $email );
                                                        update_post_meta( $post_id, 'phone', $phone );
                                                        update_post_meta( $post_id, 'website', $website );
                                                        update_post_meta( $post_id, 'facebook', $facebook );
                                                        update_post_meta( $post_id, 'twitter', $twitter );
                                                        update_post_meta( $post_id, 'instagram', $instagram );
                                                        update_post_meta( $post_id, 'linkedin', $linkedin );
                                                        update_post_meta( $post_id, 'address', $address );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <label for="email">Your Email Address</label>
                                                    <input type="email" name="email" id="email" value="<?php echo get_post_meta( $post_id, 'email', true ); ?>">
                                                </div>
                                                <div class="form-item">
                                                    <label for="phone">Your Phone Number</label>
                                                    <input type="tel" name="phone" id="phone" value="<?php echo get_post_meta( $post_id, 'phone', true ); ?>">
                                                </div>
                                                <div class="form-item">
                                                    <label for="website">Personal Blog/Website</label>
                                                    <input type="url" name="website" id="website" value="<?php echo get_post_meta( $post_id, 'website', true ); ?>">
                                                </div>
                                                <div class="form-item">
                                                    <label for="facebook">Facebook</label>
                                                    <input type="url" name="facebook" id="facebook" value="<?php echo get_post_meta( $post_id, 'facebook', true ); ?>">
                                                </div>
                                                <div class="form-item">
                                                    <label for="twitter">Twitter</label>
                                                    <input type="url" name="twitter" id="twitter" value="<?php echo get_post_meta( $post_id, 'twitter', true ); ?>">
                                                </div>
                                                <div class="form-item">
                                                    <label for="instagram">Instagram</label>
                                                    <input type="url" name="instagram" id="instagram" value="<?php echo get_post_meta( $post_id, 'instagram', true ); ?>">
                                                </div>
                                                <div class="form-item">
                                                    <label for="linkedin">Linkedin</label>
                                                    <input type="url" name="linkedin" id="linkedin" value="<?php echo get_post_meta( $post_id, 'linkedin', true ); ?>">
                                                </div>                                       
                                                <div class="form-item">
                                                    <label for="addresss">
                                                        Your current Address
                                                    </label>
                                                    <textarea id="address" name="address"><?php echo get_post_meta( $post_id, 'address', true ); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </section>
            
            <!-- Your Cover Letter -->  
            <section class="container-wrapper padding-b-10">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h3 class="txt-lg txt-medium txt-color-orange padding-l-50 padding-r-40 padding-b-20">Your Cover Letter</h3>
                        <!-- Cover Letters -->
                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'cover-letter',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );
                            ?>
                            <div class="profile-collapse padding">
                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cover-letter-collapse" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                        <span class="txt-medium txt-color-light">Tell us why you are the best person for this job</span>
                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                        <?php }else{ ?>
                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                        <?php } ?>
                                    </span>
                                </button>
                                <div class="collapse" id="cover-letter-collapse">
                                    <div class="padding-lr-30">
                                        <p class="txt-sm txt-light padding-t-10">Leave a cover note</p>
                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                <?php $check[] = 'complete'; ?>
                                                    
                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                                   
                                                    <?php $post_id = $post->ID; ?> 

                                                    <!-- Entry -->
                                                    <div class="row row-10 padding-b-20 txt-color-light">
                                                        <div class="col-12 col-lg padding-lr-10">
                                                            <p class="txt-normal-s txt-medium padding-b-5"><?php echo get_the_title() ?></p>
                                                            <article class="text-box txt-sm">
                                                                <?php echo get_post_meta( $post_id, 'body', true ); ?>
                                                            </article>
                                                        </div>
                                                        
                                                        <div class="col-lg-auto padding-lr-10 text-right txt-sm">
                                                            <a class="txt-green" data-toggle="modal" href="#editCoverLetterModal-<?php echo $post_id ?>">
                                                                <i class="fa fa-pencil"></i>
                                                                Edit
                                                            </a>
                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=edit-cover-letter-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <!-- Edit Modal -->
                                                    <div class="modal fade font-main filter-modal" id="editCoverLetterModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                                <form action="<?php echo currentUrl(false).'&form=edit-cover-letter-'.$post_id; ?>" method="post">
                                                                    <div class="modal-header padding-lr-30">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit this Cover Letter</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body padding-o-30 form">
                                                                        <?php

                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'edit-cover-letter-'.$post_id ){
                                                                                wp_delete_post($post_id); // Delete
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }

                                                                            if( $_POST && $_GET['form'] == 'edit-cover-letter-'.$post_id ){

                                                                                /* Get Post Data */
                                                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                                                $description = wp_kses_post( $_POST['body'] );

                                                                                /* Save Post to DB */
                                                                                $post_id = wp_insert_post(array (
                                                                                    'ID' => $post_id,
                                                                                    'post_type' => 'cover-letter',
                                                                                    'post_title' => $post_name,
                                                                                    'post_content' => "",
                                                                                    'post_status' => 'publish',
                                                                                ));

                                                                                /* Add Post Meta */
                                                                                update_post_meta( $post_id, 'body', $description );

                                                                                /* Redirect */
                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                                            }
                                                                        ?>
                                                                        <div class="form-item">
                                                                            <label for="deadline">
                                                                                Title
                                                                            </label>
                                                                            <input 
                                                                                type="text" 
                                                                                name="post_name" 
                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                            >
                                                                        </div>
                                                                        <div class="form-item">
                                                                            <label for="body">
                                                                                Body
                                                                            </label>
                                                                            <textarea id="description" name="body" class="editor" ><?php echo get_post_meta( $post_id, 'body', true ); ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer padding-lr-30">
                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endwhile; ?> 
                                                    
                                            <?php }else{ ?> 
                                                <?php $check[] = 'incomplete'; ?> 
                                                <p class="txt-normal-s txt-color-red padding-b-20">
                                                    Try and write something, it doesn't have to be very fancy
                                                </p>
                                            <?php } ?>
                                        </article>
                                        <!--CTA-->
                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddCoverLetterModal">Create a cover note</a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Modal -->
                            <div class="modal fade font-main filter-modal" id="myCVAddCoverLetterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=add-cover-letter'; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">Add a Cover Letter</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php
                                                    $post_id = 0;

                                                    if( $_POST && $_GET['form'] == 'add-cover-letter' ){

                                                        /* Get Post Data */
                                                        $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                        $description = wp_kses_post( $_POST['body'] );

                                                        /* Save Post to DB */
                                                        $post_id = wp_insert_post(array (
                                                            'ID' => $post_id,
                                                            'post_type' => 'cover-letter',
                                                            'post_title' => $post_name,
                                                            'post_content' => "",
                                                            'post_status' => 'publish',
                                                        ));

                                                        /* Add Post Meta */
                                                        update_post_meta( $post_id, 'body', $description );

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                    }
                                                ?>
                                                <div class="form-item">
                                                    <label for="post_name">
                                                        Title
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="post_name" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                    >
                                                </div>
                                                <div class="form-item">
                                                    <label for="body">
                                                        Body
                                                    </label>
                                                    <textarea id="body" name="body" class="editor" ><?php echo get_post_meta( $post_id, 'body', true ); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php restore_current_blog(); ?>
            
            
            
            <!-- Mentor Profile -->
            <?php switch_to_blog(109); ?>
                    
            <div class="padding-b-10">
                <div class="txt-normal-s txt-medium">
                    <a 
                       class="dropdown-toggle d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-15" 
                       data-toggle="collapse" 
                       href="#collapseSkills" aria-expanded="false" aria-controls="collapseOne"
                    >
                        <span>
                            Mentor Profile
                        </span>
                    </a>
                </div>
                <div id="collapseSkills" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                    <div class="border-o-1 border-color-darkgrey padding-o-15">
                        <div class="bg-ghostwhite padding-lr-10 padding-t-10">
                        <?php
                            $profile_query = new WP_Query();
                            $profile_query->query( 
                                array(
                                    'post_type' => 'mentor-profile',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => 1,
                                ) 
                            );

                            if ( $profile_query->have_posts() ) {
                                $check[] = 'complete';
                                while ($profile_query->have_posts()) : $profile_query->the_post();

                                /* Variables */
                                $post_id = 0;   //Get Program ID
                                $post_id = $post->ID;   //Get Program ID
                        ?>
                            <!-- View -->
                            <div class="row row-10">
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        What is the difference between Training & Mentorship?      
                                    </p>
                                    <article class="text-box txt-sm">
                                        <?php echo get_post_meta( $post_id, 'training_mentorship_difference', true ); ?>
                                    </article>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Why do you want to be a mentor?       
                                    </p>
                                    <article class="text-box txt-sm">
                                        <?php echo get_post_meta( $post_id, 'why_become_mentor', true ); ?>
                                    </article>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        What qualifies you to be a mentor?      
                                    </p>
                                    <article class="text-box txt-sm">
                                        <?php echo get_post_meta( $post_id, 'mentor_qualification', true ); ?>
                                    </article>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        In your opinion, what are the qualities of a great mentor?      
                                    </p>
                                    <article class="text-box txt-sm">
                                        <?php echo get_post_meta( $post_id, 'mentor_qualities', true ); ?>
                                    </article>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Do you have a Smart Phone?     
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'have_smartphone', true ); ?>
                                    </p>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        How regularly do you subscribe for internet data    
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'internet_subscription', true ); ?>
                                    </p>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Do you have an active WhatsApp Number?    
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'have_whatsapp', true ); ?>
                                    </p>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Are you on the Telegram Messaging App?     
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'have_telegram', true ); ?>
                                    </p>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        If you were to mentor someone over WhatsApp or Telegram, how will you go about it?     
                                    </p>
                                    <article class="text-box txt-sm">
                                        <?php echo get_post_meta( $post_id, 'about_virtual_mentorship', true ); ?>
                                    </article>
                                </div>
                            </div>
                            <div class="text-right txt-sm padding-tb-20 border-t-1">
                                <a class="txt-color-green" data-toggle="modal" href="#myMentorProfileModal">
                                    <i class="fa fa-pencil"></i>
                                    Edit Profile
                                </a>
                            </div>

                        <?php
                                endwhile;

                            }else{
                                $check[] = 'incomplete';
                        ?>

                            <p class="txt-sm padding-b-15">You have not added your mentor information.</p>
                            <p>
                                <a class="btn btn-blue txt-xs no-m-b" data-toggle="modal" href="#myMentorProfileModal">
                                    Add Information
                                </a>
                            </p>

                        <?php } ?>
                        
                        <!-- Add / Edit Modal -->
                        <div class="modal fade font-main filter-modal" id="myMentorProfileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="<?php echo currentUrl(false).'&form=mentor-profile'; ?>" method="post">
                                        <div class="modal-header padding-lr-30">
                                            <h5 class="modal-title" id="exampleModalLabel">My Mentor Profile</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body padding-o-30 form">
                                            <?php

                                                if( $_POST && $_GET['form'] == 'mentor-profile' ){

                                                    /* Get Post Name */
                                                    $postName = 'Trainer-Profile-'.$current_user->ID;

                                                    /* Meta */
                                                    $training_mentorship_difference = wp_kses_post( $_POST['training_mentorship_difference'] );
                                                    $why_become_mentor = wp_kses_post( $_POST['why_become_mentor'] );
                                                    $mentor_qualification = wp_kses_post( $_POST['mentor_qualification'] );
                                                    $mentor_qualities = wp_kses_post( $_POST['mentor_qualities'] );

                                                    $have_smartphone = sanitize_text_field( $_POST['have_smartphone'] ); 
                                                    $internet_subscription = sanitize_text_field( $_POST['internet_subscription'] ); 
                                                    $have_whatsapp = sanitize_text_field( $_POST['have_whatsapp'] ); 
                                                    $have_telegram = sanitize_text_field( $_POST['have_telegram'] ); 

                                                    $about_virtual_mentorship = wp_kses_post( $_POST['about_virtual_mentorship'] );

                                                    /* Save Post to DB */
                                                    $post_id = wp_insert_post(array (
                                                        'ID' => $post_id,
                                                        'post_type' => 'mentor-profile',
                                                        'post_title' => $postName,
                                                        'post_content' => "",
                                                        'post_status' => 'publish',
                                                    ));

                                                    update_post_meta( $post_id, 'training_mentorship_difference', $training_mentorship_difference );
                                                    update_post_meta( $post_id, 'why_become_mentor', $why_become_mentor );
                                                    update_post_meta( $post_id, 'mentor_qualification', $mentor_qualification );
                                                    update_post_meta( $post_id, 'mentor_qualities', $mentor_qualities );

                                                    update_post_meta( $post_id, 'have_smartphone', $have_smartphone );
                                                    update_post_meta( $post_id, 'internet_subscription', $internet_subscription );
                                                    update_post_meta( $post_id, 'have_whatsapp', $have_whatsapp );
                                                    update_post_meta( $post_id, 'have_telegram', $have_telegram );

                                                    update_post_meta( $post_id, 'about_virtual_mentorship', $about_virtual_mentorship );

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                }
                                            ?>
                                            <div class="form-item">
                                                <label for="">What is the difference between Training & Mentorship?</label>
                                                <textarea name="training_mentorship_difference" id="training_mentorship_difference" class="editor"><?php echo get_post_meta( $post_id, 'training_mentorship_difference', true ); ?></textarea>
                                            </div>
                                            <div class="form-item">
                                                <label for="why_become_mentor">Why do you want to be a mentor?</label>
                                                <textarea name="why_become_mentor" id="why_become_mentor" class="editor"><?php echo get_post_meta( $post_id, 'why_become_mentor', true ); ?></textarea>
                                            </div>
                                            <div class="form-item">
                                                <label for="mentor_qualification">What qualifies you to be a mentor?</label>
                                                <textarea name="mentor_qualification" id="mentor_qualification" class="editor"><?php echo get_post_meta( $post_id, 'mentor_qualification', true ); ?></textarea>
                                            </div>
                                            <div class="form-item">
                                                <label for="mentor_qualities">In your opinion, what are the qualities of a great mentor?</label>
                                                <textarea name="mentor_qualities" id="mentor_qualities" class="editor"><?php echo get_post_meta( $post_id, 'mentor_qualities', true ); ?></textarea>
                                            </div>
                                            <div class="form-item">
                                                <label for="have_smartphone">Do you have a Smart Phone?</label>
                                                <select name="have_smartphone" id="have_smartphone">
                                                    <?php $meta = get_post_meta( $post_id, 'have_smartphone', true ); ?>
                                                    <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?> >Yes</option>
                                                    <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?> >No</option>
                                                </select>
                                            </div>
                                            <div class="form-item">
                                                <label for="internet_subscription">How regularly do you subscribe for internet data</label>
                                                <select name="internet_subscription" id="internet_subscription">
                                                    <?php $meta = get_post_meta( $post_id, 'internet_subscription', true ); ?>
                                                    <option value="I always have internet on my phone" <?php echo ($meta == 'I always have internet on my phone') ? 'selected' : '' ?> >I always have internet on my phone</option>
                                                    <option value="I subscribe only when I need to" <?php echo ($meta == 'I subscribe only when I need to') ? 'selected' : '' ?> >I subscribe only when I need to</option>
                                                </select>
                                            </div>
                                            <div class="form-item">
                                                <label for="have_whatsapp">Do you have an active WhatsApp Number?</label>
                                                <select name="have_whatsapp" id="have_whatsapp">
                                                    <?php $meta = get_post_meta( $post_id, 'have_whatsapp', true ); ?>
                                                    <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?> >Yes</option>
                                                    <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?> >No</option>
                                                </select>
                                            </div>
                                            <div class="form-item">
                                                <label for="have_telegram">Are you on the Telegram Messaging App?</label>
                                                <select name="have_telegram" id="have_telegram">
                                                    <?php $meta = get_post_meta( $post_id, 'have_telegram', true ); ?>
                                                    <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?> >Yes</option>
                                                    <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?> >No</option>
                                                </select>
                                            </div>
                                            <div class="form-item">
                                                <label for="about_virtual_mentorship">If you were to mentor someone over WhatsApp or Telegram, how will you go about it?</label>
                                                <textarea name="about_virtual_mentorship" id="about_virtual_mentorship" class="editor"><?php echo get_post_meta( $post_id, 'about_virtual_mentorship', true ); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer padding-lr-30">
                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php restore_current_blog(); ?> 
 
        
    <?php if (in_array("incomplete", $check)){ ?>
               
        <!-- Incomplete Info -->   
        <section class="container-wrapper padding-t-20 padding-b-40">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <div class="bg-ash row padding-o-30">
                        <div class="col-auto ">
                            <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x txt-color-red"></i>
                                <i class="fa fa-times fa-stack-1x fa-inverse"></i>
                            </span>
                        </div>
                        <p class="col txt-sm txt-color-white padding-l-15">You have uncompleted sections marked with this symbol. You must complete those section before you will be able to submit your application.</p>
                    </div>
                </div>
            </div>
        </section>

    <?php } else { ?>

        <!-- Submit-->   
        <section class="container-wrapper padding-t-20 padding-b-40">
            <div class="row">
                <div class="col-lg-10 mx-auto text-center">
                    <a href="<?php echo $post_link ?>?view=application-confirmation" class="btn btn-orange">Submit</a>
                </div>
            </div>
        </section>

    <?php } ?>
    </div>
<?php } ?>