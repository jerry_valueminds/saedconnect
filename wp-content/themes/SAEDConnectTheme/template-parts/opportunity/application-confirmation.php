<?php if( $rendered_view == 'application-confirmation' ){ ?>

<div class="container-wrapper padding-tb-40">
    <div class="row">
        <div class="col-md-8 mx-auto text-center">
            <h1 class="txt-xlg margin-b-20">
            <?php
                /* Check if User is signed */
                if ( is_user_logged_in() ) {
                    
                    /* Check Application Entry in DB */
                    $application_entry = $application_db->get_row("SELECT post_id, post_type FROM ".$table." WHERE user_id = ".$current_user->ID." AND post_id = ".$post_id." LIMIT 0,1");
                                                
                    /* Check if User has applied*/
                    if( $application_entry ){

                        echo "You have already applied.";

                    } else {
                        
                        $videoCV="";
                        
                        /* Get Avatar */
                        $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                        $meta_key = 'user_avatar_url';
                        $get_avatar_url = get_user_meta($application_entry->user_id, $meta_key, true);

                        if($get_avatar_url){
                            $avatar_url = $get_avatar_url;
                        }


                        /* Get User Display Name */
                        switch_to_blog(1);

                        $gf_id = 4; //Form ID
                        $username_entry_count = 0;

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                            )
                        );

                        /* Get Entries */
                        $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* Get GF Entry Count */
                        $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                        if($username_entry_count){ //If no entry
                            $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                            $email = rgar( $username_entries[0], 2 );
                            $phone = rgar( $username_entries[0], 7 );
                        }   

                        restore_current_blog();


                        /* Get User Bio */
                        switch_to_blog(109);

                        $gf_id = 84; //Form ID
                        $username_entry_count = 0;

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                            )
                        );

                        /* Get Entries */
                        $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* Get GF Entry Count */
                        $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                        if($username_entry_count){ //If no entry

                            $gender = rgar( $username_entries[0], 2 );
                            $dob = 2000;
                            $nationality = rgar( $username_entries[0], 4 );
                            $location = rgar( $username_entries[0], 6 );
                        }   

                        restore_current_blog();
                

                        /* Create application status*/
                        $application_db->insert( 
                            $table, 
                            array( 
                                "post_id" => $post_id,
                                "post_type" => $post_type,
                                "user_id" => $current_user->ID,
                                "status" => "unsorted",
                                "name" => $displayname,
                                "video_cv" => $videoCV,
                                "gender" => $gender,
                                "year_of_birth" => "hello",
                                "email" => "hello",
                                "location" => "hello",
                                "nationality" => "hello",
                                "phone" => "hello",
                                "cover_letter" => "hello",
                                "completed_tasks" => "hello",
                                "highest_qualification" => "hello",
                                
                            ), 
                            array( "%d", "%s", "%d", "%s", "%s", "%s", "%s", "%d", "%s", "%s", "%s", "%s", "%s", "%s", "%s" ) 
                        );

                        echo "Your Application for was succesfully submitted.";
                    }
            ?>
            </h1>
              
            <?php } else { ?>
            
            <h1 class="txt-xxlg margin-b-20">
                You must be logged in to apply for this job
            </h1>
            <p class="txt-normal-s txt-height-1-7 margin-b-40">
                Click the 'Sign In' button below if you have an existing SAEDConnect account, or click the 'Register an Account' button to create your SAEDConnect account.
            </p>
            <div class="txt-normal-s">
                <a href="https://www.saedconnect.org/login" class="btn btn-blue">
                    Sign In
                </a>
                <a href="https://www.saedconnect.org/register" class="btn btn-trans-blue">
                    Register an Account
                </a>
            </div>
            
            <?php } ?>
            
        </div>
    </div>
</div>

<?php } ?>