<?php
    
    $group_values = rwmb_meta( 'content-block-group' );

    /* Set Unique Accordion suffix */
    $accordion_suffix = 0;
    $heading_counter = 0;
    $content_counter = 0;


    if ( ! empty( $group_values ) ) {
        foreach ( $group_values as $group_value ) {
            
            /* Increment Accordion Suffix */
            $accordion_suffix += 1;

            /* Get Content type */
            $content_type = $group_value['select-content-type'];
            
            /* Increment */
            $content_counter++;
            
            /* Reset */
            $image = 0;
            $image_ids = isset( $group_value['bg-image'] ) ? $group_value['bg-image'] : array();

            foreach ( $image_ids as $image_id ) {
                $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
            }
            
?>            
            
            <style>
                /* BG Color / Image */
                #content-<?php echo $content_counter; ?>{
                    position: relative;
                    background-color: <?php echo $group_value['bg-color']; ?>;
                    background-image: url('<?php echo $image['full_url']; ?>');
                    background-position: center;
                    background-size: cover;
                    background-repeat: no-repeat;
                    color: <?php echo $group_value['alt-color']; ?>;
                }
                
                /* Content */
                #content-<?php echo $content_counter; ?> >*{
                    position: relative;
                    z-index: 1;
                }
                
                /* Overlay */
                #content-<?php echo $content_counter; ?>:after {
                    content: "";
                    position: absolute;
                    z-index: 0;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    background-color: black;
                    opacity: <?php echo ($group_value['show-overlay']) ? "0.6" : "0"; ?> !important;
                }
            </style>

       <?php   
            
            if($content_type == 'cp-text-meta-box'){ ?>

            <!-- Text content -->
            
            <?php
                $fontSize = $group_value['cp-text-meta-box']['cp-text-font-size'];
                $bold_text = $group_value['cp-text-meta-box']['cp-text-bold'];
                $width = $group_value['cp-text-meta-box']['cp-text-width'];
                $justify = $group_value['cp-text-meta-box']['cp-text-justify-content'];
                $text_align = $group_value['cp-text-meta-box']['cp-text-text-align'];
                $p_top = $group_value['cp-text-meta-box']['cp-text-padding-top'];
                $p_bottom = $group_value['cp-text-meta-box']['cp-text-padding-bottom'];
            ?>
            
            <style>
                #content-<?php echo $content_counter; ?>{
                    text-align: <?php echo $text_align; ?>;
                    padding-top: <?php echo $p_top; ?>px;
                    padding-bottom: <?php echo $p_bottom; ?>px;
                }
                
                #content-<?php echo $content_counter; ?> h1{
                    <?php 
                        if($fontSize == 'normal-s')
                            echo "line-height: 1.5em;";
                        elseif($fontSize == 'lg')
                            echo "line-height: 1.4em;";
                        elseif($fontSize == '2em')
                            echo "line-height: 1.3em;";
                        elseif($fontSize == '3em')
                            echo "line-height: 1em;";
                    ?>
                }
            </style>
        
            <section id="content-<?php echo $content_counter; ?>">
                <div class="row">
                    <div class="<?php echo $width.' '.$justify.' txt-'.$text_align; echo ($bold_text)? " txt-bold" : "" ?>">
                        <header class="container-wrapper text-<?php echo $text_align; ?>">
                            <h1 class="txt-<?php echo $fontSize ?>">
                                <?php echo $group_value['cp-text-meta-box']['cp-text-content']; ?>
                            </h1>
                        </header>
                    </div>
                </div>
            </section>

        <?php } elseif($content_type == 'cp-hero-meta-box'){ ?>
        
            <?php
                $text_align = $group_value['cp-hero-meta-box']['cp-hero-text-align'];              
            ?>
               
                <?php
                
                    $text_align = $group_value['cp-hero-meta-box']['cp-hero-text-align'];
                    $p_top = $group_value['cp-hero-meta-box']['cp-hero-padding'];
                    $p_bottom = $group_value['cp-hero-meta-box']['cp-hero-padding'];
                ?>

                <style>
                    #content-<?php echo $content_counter; ?>{
                        height: auto;
                        text-align: <?php echo $text_align; ?>;
                        padding-top: <?php echo $p_top; ?>px;
                        padding-bottom: <?php echo $p_bottom; ?>px;
                    }
                </style>
                
                <header 
                   class="bg-change-program" 
                   style="background-image: url('<?php echo $image['full_url']; ?>')"
                   id="content-<?php echo $content_counter; ?>"
                >
                    <?php if( $group_value['cp-hero-meta-box']['cp-hero-show-content'] ){ ?>
                    <div class="container-wrapper content">
                        <div class="row"> 
                            <div class="col-md-11 mx-auto">
                                <div class="row <?php echo ($text_align == 'right') ? "justify-content-end" : ""; ?>">
                                    <div class="col-md-8 <?php echo ($text_align == 'center') ? "mx-auto" : ""; ?>"> 
                                        <h1 class="txt-3em txt-bold txt-height-1-2 margin-b-15">
                                            <?php echo $group_value['cp-hero-meta-box']['cp-hero-title']; ?> 
                                        </h1>
                                        <h2 class="txt-lg txt-height-1-5 ">
                                            <?php echo $group_value['cp-hero-meta-box']['cp-hero-summary']; ?>
                                        </h2>
                                        <?php if( $group_value['cp-hero-meta-box']['cp-hero-cta-text'] ){ ?>
                                            <div class="margin-t-30">
                                                <a href="<?php echo $group_value['cp-hero-meta-box']['cp-hero-cta-url']; ?>" class="btn btn-blue no-m-b">
                                                    <?php echo $group_value['cp-hero-meta-box']['cp-hero-cta-text']; ?>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </header>
                
        <?php } elseif($content_type == 'cp-image-meta-box'){ ?>

               
                <?php                
                    $width = $group_value['cp-image-meta-box']['cp-image-width'];
                    $justify = $group_value['cp-image-meta-box']['cp-image-justify-content'];
                    $p_top = $group_value['cp-image-meta-box']['cp-image-padding-top'];
                    $p_bottom = $group_value['cp-image-meta-box']['cp-image-padding-bottom'];
                ?>

                <style>
                    #content-<?php echo $content_counter; ?>{
                        height: auto;
                        text-align: <?php echo $text_align; ?>;
                        padding-top: <?php echo $p_top; ?>px;
                        padding-bottom: <?php echo $p_bottom; ?>px;
                    }
                </style>
                
                <header 
                   style="background-image: url('<?php echo $image['full_url']; ?>')"
                   id="content-<?php echo $content_counter; ?>"
                >
                   
                    <?php 
                        $image = 0;

                        $image_size = $group_value['cp-image-meta-box']['cp-image-image-size'];

                        $image_ids = isset( $group_value['cp-image-meta-box']['cp-image-image'] ) ? $group_value['cp-image-meta-box']['cp-image-image'] : array();

                        foreach ( $image_ids as $image_id ) {
                            $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                        }
                    ?>
                    <div class="<?php echo ( $width != "1" )? "container-wrapper" : "" ?> content">
                        <div class="row"> 
                            <div class="<?php echo $justify.' '; echo ($width == 1) ? 'col-12' : $width; ?>">
                                <img src="<?php echo $image['full_url']; ?>" alt="" class="w-100">
                            </div>
                        </div>
                    </div>
                </header>
                
        <?php } elseif($content_type == 'cp-heading-meta-box'){ ?>
        
            <?php
                
                $text_align = $group_value['cp-heading-meta-box']['cp-heading-text-align'];
                $p_top = $group_value['cp-heading-meta-box']['cp-heading-padding'];
                $p_bottom = $group_value['cp-heading-meta-box']['cp-heading-padding'];
            ?>
            
            <style>
                #content-<?php echo $content_counter; ?>{
                    text-align: <?php echo $text_align; ?>;
                    padding-top: <?php echo $p_top; ?>px;
                    padding-bottom: <?php echo $p_bottom; ?>px;
                }
            </style>
        
        <section id="content-<?php echo $content_counter; ?>">
            <div class="row">
                <div class="col-11 mx-auto">
                    <div class="row <?php echo ($text_align == 'right') ? "justify-content-end" : ""; ?>">
                        <div class="col-md-8 <?php echo ($text_align == 'center') ? "mx-auto" : ""; ?>">                
                            <header class="container-wrapper text-<?php echo $text_align; ?>">
                                <h1 class="txt-2-2em txt-bold txt-height-1-2 margin-b-20">
                                    <?php echo $group_value['cp-heading-meta-box']['cp-heading-title']; ?>
                                </h1>
                                <h2 class="txt-height-1-7">
                                    <?php echo $group_value['cp-heading-meta-box']['cp-heading-content']; ?>
                                </h2>
                            </header>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-featured-action-meta-box'){ ?>
        
        <section class="padding-tb-80" id="content-<?php echo $content_counter; ?>">
            <div class="row">
                <div class="col-md-9 mx-auto">
                    <div class="row row-30">
                        <?php
                            $image = 0;
                            $image_position = $group_value['cp-featured-action-meta-box']['cp-featured-action-image-position'];
                                                                       
                            $image_ids = isset( $group_value['cp-featured-action-meta-box']['cp-featured-action-image'] ) ? $group_value['cp-featured-action-meta-box']['cp-featured-action-image'] : array();

                            foreach ( $image_ids as $image_id ) {
                                $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                                //echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '">';
                            }
                        ?>
                        <div class="col-md-6 order-md-<?php echo $image_position; ?> padding-lr-30">
                            <img src="<?php echo $image['full_url']; ?>" alt="">
                        </div>
                        <div class="col-md-6 order-md-0 padding-lr-30">
                            <h2 class="txt-2-2em txt-medium txt-bold margin-b-20">
                                <?php echo $group_value['cp-featured-action-meta-box']['cp-featured-action-title']; ?>
                            </h2>
                            <p class="txt-color-light txt-height-1-7">
                                <?php echo $group_value['cp-featured-action-meta-box']['cp-featured-action-content']; ?>
                            </p>
                            <div class="margin-t-40">
                                <a href="<?php echo $group_value['cp-featured-action-meta-box']['cp-featured-action-cta-link']; ?>" class="btn btn-blue txt-normal-s">
                                    <?php echo $group_value['cp-featured-action-meta-box']['cp-featured-action-cta-text']; ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-text-image-meta-box'){ ?>
        
        <?php
            $image = 0;
            $image_position = $group_value['cp-text-image-meta-box']['cp-text-image-image-position'];

            $image_size = $group_value['cp-text-image-meta-box']['cp-text-image-image-size'];
            $section_width = $group_value['cp-text-image-meta-box']['cp-text-image-width'];
            $border_bottom = $group_value['cp-text-image-meta-box']['cp-text-image-border-bottom'];

            $image_ids = isset( $group_value['cp-text-image-meta-box']['cp-text-image-image'] ) ? $group_value['cp-text-image-meta-box']['cp-text-image-image'] : array();

            foreach ( $image_ids as $image_id ) {
                $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
            }
        ?>
        
        <?php if( $image_size == 0 ){ ?>
           
            <section class="container-wrapper padding-t-40 padding-b-20 <?php echo ($border_bottom)? "border-b-1 border-color-darkgrey" : ""?>" id="content-<?php echo $content_counter; ?>">
                <div class="row">
                    <div class="<?php echo $section_width ?> mx-auto">
                       <div class="row row-40 align-items-center">
                           <div class="col-md-6 order-md-<?php echo $image_position; ?> padding-lr-40 padding-b-20">
                               <img src="<?php echo $image['full_url']; ?>" alt="" style="border-radius:12px;">
                           </div>
                           <div class="col-md-6 order-md-0 padding-lr-40 padding-b-20">
                                <div class="wrapper">
                                    <?php if( $group_value['cp-text-image-meta-box']['cp-text-image-subtitle'] ){ ?>
                                        <h2 class="txt-xxlg txt-medium txt-bold txt-height-1-2 txt-color-lighter">
                                            <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-subtitle']; ?>
                                        </h2>
                                    <?php } ?>
                                    <h2 class="txt-2-2em txt-medium txt-bold txt-height-1-2 margin-b-20">
                                        <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-title']; ?>
                                    </h2>
                                    <article class="text-box txt-normal-s">
                                        <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-content']; ?>
                                    </article>
                                    <?php
                                        $list = $group_value['cp-text-image-meta-box']['cp-text-image-list'];

                                        if($list){
                                    ?>
                                    <div class="padding-t-40">
                                        <?php foreach($list as $list_item){ ?>
                                            <div class="row row-10 align-items-center padding-b-20">
                                               <?php
                                                    $image = 0;
                                                    $image_ids = isset( $list_item['cp-text-image-list-image'] ) ? $list_item['cp-text-image-list-image'] : array();

                                                    foreach ( $image_ids as $image_id ) {
                                                        $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                                                    }
                                                ?>
                                                <div 
                                                   class="col-auto padding-lr-10 padding-b-10"
                                                >
                                                    <img src="<?php echo $image['full_url']; ?>" alt="" width="40">
                                                </div>
                                                <div class="col padding-lr-10 padding-b-10">
                                                    <p class="txt-normal-s">
                                                        <?php echo $list_item[ 'cp-text-image-list-content' ]; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                       </div>
                    </div>
               </div>
           </section>
        
        <?php } elseif( $image_size == 1 ) { ?>
           
           <section class="row row-30 <?php echo ($border_bottom)? "border-b-1 border-color-darkgrey" : ""?>" id="content-<?php echo $content_counter; ?>">
                <div 
                    class="col-md-6 image order-md-<?php echo $image_position; ?>" 
                    style="background-image: url('<?php echo $image['full_url']; ?>'); background-size: cover;"
                >

                </div>

                <div class="col-md-6 order-md-0 padding-tb-80">
                    <div class="wrapper container-wrapper">
                        <?php if( $group_value['cp-text-image-meta-box']['cp-text-image-subtitle'] ){ ?>
                            <h2 class="txt-xxlg txt-medium txt-bold txt-height-1-2 txt-color-lighter">
                                <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-subtitle']; ?>
                            </h2>
                        <?php } ?>
                        <h2 class="txt-2-2em txt-medium txt-bold margin-b-20">
                            <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-title']; ?>
                        </h2>
                        <article class="text-box txt-normal-s">
                            <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-content']; ?>
                        </article>
                        <?php
                            $list = $group_value['cp-text-image-meta-box']['cp-text-image-list'];

                            if($list){
                        ?>
                        <div class="padding-t-40">
                            <?php foreach($list as $list_item){ ?>
                                <div class="row row-10 align-items-center padding-b-20">
                                   <?php
                                        $image = 0;
                                        $image_ids = isset( $list_item['cp-text-image-list-image'] ) ? $list_item['cp-text-image-list-image'] : array();

                                        foreach ( $image_ids as $image_id ) {
                                            $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                                        }
                                    ?>
                                    <div 
                                       class="col-auto padding-lr-10 padding-b-10"
                                    >
                                        <img src="<?php echo $image['full_url']; ?>" alt="" width="40">
                                    </div>
                                    <div class="col padding-lr-10 padding-b-10">
                                        <p class="txt-normal-s">
                                            <?php echo $list_item[ 'cp-text-image-list-content' ]; ?>
                                        </p>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
           
        <?php } elseif( $image_size == 2 || $image_size == 3 ) { ?>
           
            <section class="container-wrapper padding-tb-40 <?php echo ($border_bottom)? "border-b-1 border-color-darkgrey" : ""?> order-md-<?php echo $image_position; ?>" id="content-<?php echo $content_counter; ?>">
                <div class="row">
                    <div class="<?php echo $section_width ?> mx-auto">
                       <div class="row <?php echo ($image_size == 2)? "row-40" : "row-30"?>">
                           <div class="<?php echo ($image_size == 2)? "col-md-4 padding-lr-40" : "col-md-2 padding-lr-30"?> order-md-<?php echo $image_position; ?>">
                               <img src="<?php echo $image['full_url']; ?>" alt="" style="border-radius:12px;">
                           </div>
                           <div class="<?php echo ($image_size == 2)? "col-md-8 padding-lr-40" : "col-md-10 padding-lr-30"?> order-md-<?php echo $image_position; ?> order-md-0 padding-lr-40 padding-t-20">
                                <div class="wrapper">
                                    <?php if( $group_value['cp-text-image-meta-box']['cp-text-image-subtitle'] ){ ?>
                                        <h2 class="txt-lg txt-medium txt-bold txt-height-1-2 txt-color-lighter margin-b-10">
                                            <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-subtitle']; ?>
                                        </h2>
                                    <?php } ?>
                                    <h2 class="txt-xlg txt-medium txt-bold txt-height-1-2 margin-b-10">
                                        <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-title']; ?>
                                    </h2>
                                    <article class="text-box txt-normal-s">
                                        <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-content']; ?>
                                    </article>
                                    <?php
                                        $list = $group_value['cp-text-image-meta-box']['cp-text-image-list'];

                                        if($list && isset( $list[0]['cp-text-image-list-content']) ){
                                    ?>
                                    <div class="padding-t-40">
                                        <?php foreach($list as $list_item){ ?>
                                            <div class="row row-10 align-items-center padding-b-5">
                                               <?php
                                                    $image = 0;
                                                    $image_ids = isset( $list_item['cp-text-image-list-image'] ) ? $list_item['cp-text-image-list-image'] : array();

                                                    foreach ( $image_ids as $image_id ) {
                                                        $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                                                    }
                                                ?>
                                                <div 
                                                   class="col-auto padding-lr-10"
                                                >
                                                    <img src="<?php echo $image['full_url']; ?>" alt="" width="10">
                                                </div>
                                                <div class="col padding-lr-10">
                                                    <p class="txt-normal-s">
                                                        <?php echo $list_item[ 'cp-text-image-list-content' ]; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                       </div>
                    </div>
               </div>
           </section>
            
        <?php } ?>
        
        <?php } elseif($content_type == 'cp-icon-group-meta-box'){ ?>
        
            <?php
                $text_align = $group_value['cp-icon-group-meta-box']['cp-icon-group-text-align'];
                $p_top = $group_value['cp-icon-group-meta-box']['cp-icon-group-padding-top'];
                $p_bottom = $group_value['cp-icon-group-meta-box']['cp-icon-group-padding-bottom'];
                $column_width = $group_value['cp-icon-group-meta-box']['cp-icon-group--columns'];
                $icon_position = $group_value['cp-icon-group-meta-box']['cp-icon-group-icon-position'];
            ?>
            
            <style>
                #content-<?php echo $content_counter; ?>{
                    text-align: <?php echo $text_align; ?>;
                    padding-top: <?php echo $p_top; ?>px;
                    
                    <?php if( $p_bottom == "-30"){ ?>
                        margin-bottom: <?php echo $p_bottom; ?>px;
                    <?php }elseif( $p_bottom == "-20"){ ?>
                        margin-bottom: <?php echo $p_bottom; ?>px;
                    <?php }elseif( $p_bottom == "-10"){ ?>
                        margin-bottom: <?php echo $p_bottom; ?>px;
                    <?php }elseif( $p_bottom == "30"){ ?>
                        
                    <?php }else{ ?>
                        padding-bottom: <?php echo $p_bottom; ?>px;
                    <?php } ?>
                }
            </style>
    
        <div class="overflow-hidden">
        <section class="text-<?php echo $text_align; ?>" id="content-<?php echo $content_counter; ?>">
            <div class="container-wrapper">
                
                <div class="row">
                    <div class="col-md-10 mx-auto">
                       <?php if ($group_value['cp-icon-group-meta-box']['cp-icon-group-title'] || $group_value['cp-icon-group-meta-box']['cp-icon-group-content'] ){ ?>
                        <div class="row <?php echo ($text_align == 'right') ? "justify-content-end" : ""; ?>">
                            <div class="col-md-8 <?php echo ($text_align == 'center') ? "mx-auto" : ""; ?>">
                                <header class="padding-b-80">
                                <?php if ($group_value['cp-icon-group-meta-box']['cp-icon-group-title'] ){ ?>
                                    <h1 class="txt-2-2em txt-bold txt-height-1-1">
                                        <?php echo $group_value['cp-icon-group-meta-box']['cp-icon-group-title']; ?>
                                    </h1> 
                                <?php } ?>

                                <?php if ( $group_value['cp-icon-group-meta-box']['cp-icon-group-content'] ){ ?>                      
                                    <h2 class="txt-height-1-5 txt-normal-s margin-t-20">
                                        <?php echo $group_value['cp-icon-group-meta-box']['cp-icon-group-content']; ?>
                                    </h2>
                                <?php } ?>
                                </header>
                            </div>
                        </div>  
                        <?php } ?>
                       
                       
                       
                        <div class="row row-60">
                        <?php
                            $group_values = $group_value['cp-icon-group-meta-box']['cp-icon-group'];

                            if ( ! empty( $group_values ) ) {
                                foreach ( $group_values as $group_value ) {
                        ?>
                            <div class="col-md-<?php echo $column_width; ?> padding-lr-60 padding-b-30">
                                <div class="row align-items-center">
                                   <?php
                                        $image = 0;
                                        $image_ids = isset( $group_value['cp-icon-image'] ) ? $group_value['cp-icon-image'] : array();

                                        foreach ( $image_ids as $image_id ) {
                                            $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                                        }
                                    ?>
                                    <div 
                                       class="padding-b-10 <?php echo ($icon_position == top)? "col-12" : "col-auto"?>"
                                    >
                                        <img src="<?php echo $image['full_url']; ?>" alt="" width="<?php echo ($icon_position == top)? "40" : "40"?>">
                                    </div>
                                    <div class="col-9 <?php echo ($icon_position == top)? "col-12" : "col-9 padding-l-20"?>">
                                        <p class=" txt-height-1-4 txt-normal-s">
                                            <?php echo $group_value[ 'cp-icon-content' ]; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php
                                }
                            }
                        ?>  
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
        
        <?php } elseif($content_type == 'cp-timeline-group-meta-box'){ ?>
        
        <?php
                
            $p_top = $group_value['cp-timeline-group-meta-box']['cp-timeline-group-padding'];
            $p_bottom = $group_value['cp-timeline-group-meta-box']['cp-timeline-group-padding'];
        ?>

        <style>
            #content-<?php echo $content_counter; ?>{
                padding-top: <?php echo $p_top; ?>px;
                padding-bottom: calc(<?php echo $p_bottom; ?>px - 20px);
            }
        </style>
        
        <section id="content-<?php echo $content_counter; ?>">
            <div class="container-wrapper">
                <div class="row text-center"> 
                    <div class="col-md-11 mx-auto">
                        <h1 class="txt-2-2em txt-bold txt-height-1-1 margin-b-20 ">
                            <?php echo $group_value['cp-timeline-group-meta-box']['cp-timeline-group-title']; ?>
                        </h1>
                        <p class="txt-height-1-4 padding-b-60 text-center">
                            <?php echo $group_value['cp-timeline-group-meta-box']['cp-timeline-group-content']; ?>
                        </p>
                      </div>  
                </div>
            </div>
            <div class="container-wrapper how-it-works-container margin-t-40">
                <div class="row text-center"> 
                    <div class="col-md-10 mx-auto">
                        <div class="row row-20 ">
                        <?php
                            $group_values = $group_value['cp-timeline-group-meta-box']['cp-timeline'];
                            $counter = 1;

                            if ( ! empty( $group_values ) ) {
                                foreach ( $group_values as $group_value ) {
                        ?>

                            <div class="col-md padding-lr-20">
                                <div class="how-card">
                                    <div class="number">
                                        <span>
                                            <?php
                                                echo $counter;
                                                $counter++;
                                            ?>
                                        </span>
                                    </div>
                                    <div class="content">
                                        <h3 class="txt-bold txt-height-1-2 margin-b-5">
                                            <?php echo $group_value[ 'cp-timeline-title' ]; ?>
                                        </h3>
                                        <p class="txt-sm txt-height-1-7">
                                            <?php echo $group_value[ 'cp-timeline-content' ]; ?>
                                        </p>
                                        <?php if( 
                                                $group_value[ 'cp-timeline-cta-text' ] && $group_value[ 'cp-timeline-cta-link' ]
                                        ){ ?>
                                        
                                        <div class="padding-t-20">
                                            <a href="<?php echo $group_value[ 'cp-timeline-cta-link' ]; ?>" class="btn btn-blue txt-xs no-m-b">
                                                <?php echo $group_value[ 'cp-timeline-cta-text' ]; ?>
                                            </a>
                                        </div>
                                        
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        <?php
                                }
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-information-card-meta-box'){ ?>
        
            <?php
                $text_align = $group_value['cp-information-card-meta-box']['cp-information-card-group-text-align'];
                $column_width = $group_value['cp-information-card-meta-box']['cp-information-card-group-columns'];
                $bg_color = $group_value['cp-information-card-meta-box']['cp-information-card-group-bg-color'];
            ?>
        <section 
           class="bg-grey padding-t-80 padding-b-80"
           style="background-color: <?php echo $bg_color; ?>;"
           id="content-<?php echo $content_counter; ?>"
        >
            <div class="container-wrapper row text-<?php echo $text_align; ?>">
                <div class="row">
                    <div class="col-md-11 mx-auto">
                        <h1 class="txt-2-2em txt-bold txt-height-1-1 margin-b-20">
                            <?php echo $group_value['cp-information-card-meta-box']['cp-information-card-group-title']; ?>
                        </h1>
                        <p class="txt-normal-s margin-b-60">
                            <?php echo $group_value['cp-information-card-meta-box']['cp-information-card-group-content']; ?>
                        </p>
                        <div class="row row-20">
                        <?php 
                                $values = $group_value['cp-information-card-meta-box']['cp-information-card'];

                                foreach ( $values as $value ) { 
                        ?>
                            <div class="col-md-<?php echo $column_width; ?> padding-lr-20 d-flex">
                                <div class="bg-white collapsible-card">
                                    <div class="header">
                                        <div class="cta"></div>
                                        <div class="title">
                                            <?php echo $value['cp-information-card-title']; ?>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="wrapper">
                                            <?php
                                                $image = 0;
                                                $image_ids = isset( $value['cp-information-card-image'] ) ? $value['cp-information-card-image'] : array();

                                                foreach ( $image_ids as $image_id ) {
                                                    $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                                                }
                                            ?>
                                            
                                            <?php if($image){ ?>
                                            <div class="margin-b-20">
                                                <img src="<?php echo $image['full_url']; ?>" alt="" width="50">
                                            </div>
                                            <?php } ?>
                                            
                                            <h3 class="txt-medium txt-color-dark margin-b-20">
                                                <?php echo $value['cp-information-card-title']; ?>
                                            </h3>
                                            <p class="txt-sm txt-height-1-7 txt-color-dark">
                                                <?php echo $value['cp-information-card-content']; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-job-card-meta-box'){ ?>
        
            <?php
                $column_width = $group_value['cp-job-card-meta-box']['cp-job-card-group-columns'];
                $text_align = $group_value['cp-job-card-meta-box']['cp-job-card-group-text-align'];
                $column_width = $group_value['cp-job-card-meta-box']['cp-job-card-group-columns'];
                $bg_color = $group_value['cp-job-card-meta-box']['cp-job-card-group-bg-color'];
            ?>
            
        <section 
           class="padding-t-80 padding-b-80"
           style="background-color: <?php echo $bg_color; ?>;"
           id="content-<?php echo $content_counter; ?>"
        >
            <div class="container-wrapper text-<?php echo $text_align; ?>">
                        
                <div class="row row-15">
                <?php 
                        $values = $group_value['cp-job-card-meta-box']['cp-job-card'];

                        foreach ( $values as $value ) { 
                ?>
                    <div class="col-md-<?php echo $column_width; ?> padding-lr-15 d-flex padding-b-20">
                        <div class="flex full-width bg-white border-o-1 border-color-darkgrey">
                            <h3 class="txt-xlg txt-bold padding-lr-20 padding-t-20">
                                <?php echo $value['cp-job-card-title']; ?>
                            </h3>
                            <article class="padding-o-20">
                                <p class="txt-normal-s txt-height-1-7 padding-tb-10 border-tb-1 border-color-darkgrey">

                                    <span class="txt-bold txt-color-blue">
                                        <?php echo $value['cp-job-card-status-label']; ?>:
                                    </span>
                                    <span class="">
                                        <?php echo $value['cp-job-card-value-value']; ?>
                                    </span>
                                </p>
                                <p class="txt-sm txt-height-1-5 padding-tb-10 border-b-0 border-color-darkgrey">
                                    <?php echo $value['cp-job-card-content']; ?>
                                </p>
                            </article>
                            <article class="padding-lr-20 padding-b-20">
                                <a href="<?php echo $value['cp-job-card-cta-link']; ?>" class="btn btn-blue txt-normal-s no-m-b">
                                    <?php echo $value['cp-job-card-cta-text']; ?>
                                </a>
                            </article>
                        </div>
                    </div>

                <?php } ?>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-opportunity-card-meta-box'){ ?>
        
            <?php
                $text_align = $group_value['cp-opportunity-card-meta-box']['cp-opportunity-card-group-text-align'];
                $column_width = $group_value['cp-opportunity-card-meta-box']['cp-opportunity-card-group-columns'];
                $bg_color = $group_value['cp-opportunity-card-meta-box']['cp-opportunity-card-group-bg-color'];
            ?>
        <section 
           class="bg-grey padding-t-80 padding-b-50"
           style="background-color: <?php echo $bg_color; ?>;"
           id="content-<?php //echo $content_counter; ?>"
        >
            <div class="container-wrapper text-<?php echo $text_align; ?>">
                <div class="row">
                    <div class="col-md-12 mx-auto">
                        <div class="row row-15">
                        <?php 
                            $values = $group_value['cp-opportunity-card-meta-box']['cp-opportunity-card'];
                            $card_counter = 1;

                            foreach ( $values as $value ) { 
                        ?>
                            <div class="col-lg-<?php echo $column_width; ?> padding-lr-15 padding-b-30 d-flex">
                                <div class="flip-card flex_1 d-flex">
                                    <div class="info-program-card front flex_1">
                                        <?php
                                            $image = 0;
                                            $image_ids = isset( $value['cp-opportunity-card-image'] ) ? $value['cp-opportunity-card-image'] : array();

                                            foreach ( $image_ids as $image_id ) {
                                                $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                                            }
                                        ?>
                                        <figure style="background-image: url('<?php echo $image['full_url']; ?>')">
                                        <?php if( $value['cp-opportunity-card-notification-label'] ){ ?>
                                            <div class="flash bg-blue txt-sm txt-color-white padding-lr-15 padding-tb-10">
                                                <?php echo $value['cp-opportunity-card-notification-label']; ?>
                                            </div>
                                        <?php } ?>
                                        </figure>
                                        <div class="content padding-o-15">
                                            <?php if( $value['cp-opportunity-card-notification-label'] ){ ?>
                                                <p class="txt-sm padding-b-10">
                                                    <?php echo $value['cp-opportunity-card-subtitle']; ?>
                                                </p>
                                            <?php } ?>
                                            <h4 class="txt-lg txt-medium txt-height-1-2 padding-b-10 <?php echo ( $value['cp-opportunity-card-flippable'] ) ? "flip-trigger" : ""; ?>">
                                                <?php echo $value['cp-opportunity-card-title']; ?>
                                            </h4>
                                            <p class="txt-normal-s">
                                                <?php echo $value['cp-opportunity-card-content']; ?>
                                            </p>
                                            <p class="txt-normal-s txt-height-1-7 padding-tb-10 margin-t-30 border-tb-1 border-color-darkgrey">
                                                <span class="txt-bold txt-color-blue">
                                                    <?php echo $value['cp-opportunity-card-status-label']; ?>:
                                                </span>
                                                <span class="">
                                                    <?php echo $value['cp-opportunity-card-value-value']; ?>
                                                </span>
                                            </p>
                                            <p class="margin-t-15">
                                            <?php if( $value['cp-opportunity-card-link'] ){ ?>
                                                <a class="btn btn-blue txt-sm no-m-b" href="<?php echo $value['cp-opportunity-card-link']; ?>">
                                                    <?php echo ( $value['cp-opportunity-card-cta-text'] ) ? $value['cp-opportunity-card-cta-text'] : "Apply"; ?>                                        
                                                </a>
                                            <?php }else{ ?>
                                                <a class="btn btn-blue txt-sm no-m-b" data-toggle="modal" href="#registerModal-<?php echo $content_counter.'-'.$card_counter; ?>">
                                                    <?php echo ( $value['cp-opportunity-card-cta-text'] ) ? $value['cp-opportunity-card-cta-text'] : "Apply"; ?>                                        
                                                </a>
                                            <?php } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="bg-black back flip-trigger padding-o-30">
                                        <div class="content txt-color-white">
                                           <h4 class="txt-lg txt-medium txt-height-1-2 margin-b-15">
                                               <?php echo $value['cp-opportunity-card-title']; ?>
                                           </h4>
                                            <article class="text-box txt-normal-s txt-height-1-5 margin-b-30">
                                                <?php echo $value['cp-opportunity-card-description']; ?>
                                            </article>
                                            <article class="btn-wrapper">
                                                <?php if( $value['cp-opportunity-card-link'] ){ ?>
                                                    <a class="btn btn-trans-wb txt-sm" href="<?php echo $value['cp-opportunity-card-link']; ?>">
                                                        <?php echo ( $value['cp-opportunity-card-cta-text'] ) ? $value['cp-opportunity-card-cta-text'] : "Apply"; ?>                                        
                                                    </a>
                                                <?php }else{ ?>
                                                    <a class="btn btn-trans-wb txt-sm" data-toggle="modal" href="#registerModal-<?php echo $content_counter.'-'.$card_counter; ?>">
                                                        <?php echo ( $value['cp-opportunity-card-cta-text'] ) ? $value['cp-opportunity-card-cta-text'] : "Apply"; ?>                                        
                                                    </a>
                                                <?php } ?>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="modal fade font-main filter-modal" id="registerModal-<?php echo $content_counter.'-'.$card_counter; ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentoutcomeModal" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <?php 
                                            if($_GET){
                                                /* Serialize Form Submission */
                                                $serialized_data = maybe_serialize($_POST);

                                                /* Save usermeta */
                                                update_user_meta( $current_user->ID, $meta_key, $serialized_data);

                                                /* Update Mailing List */
                                                /* Data */
                                                $api_key = '0fkxtL9crlJFX7cqCXUV';
                                                $list = 'k34dwelDkiw1kbc2fcbw1A'; //Master
                                                $email = $_GET['email'];
                                                $name = $_GET['full-name'];
                                                $phone = $_GET['phone'];
                                                $location = $_GET['state'];
                                                $opportunity_name = $_GET['program-name'];

                                                /* Endpoints */
                                                $subscribe_url = 'http://maylbox.com/subscribe';

                                                /* Subscribe Body */
                                                $body = array(
                                                    'Opportunity' => $opportunity_name,
                                                    'name' => $name,
                                                    'email' => $email,
                                                    'Phone' => $phone,
                                                    'Location' => $location,
                                                    'list' => $list,
                                                    'boolean' => 'true'
                                                );

                                                $args = array(
                                                    'body' => $body,
                                                    'timeout' => '5',
                                                    'redirection' => '5',
                                                    'httpversion' => '1.0',
                                                    'blocking' => true,
                                                    'headers' => array(),
                                                    'cookies' => array()
                                                );

                                                $response = wp_remote_post( $subscribe_url, $args );
                                                $http_code = wp_remote_retrieve_response_code( $response );
                                                $body = wp_remote_retrieve_body( $response );
                                                
                                                print_r($http_code);
                                                print_r($body);
                                                /* Update Mailing List End */

                                                /* Redirect */
                                                printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                            }
                                        ?>
                                        <form class="signup-form" method="get" action="<?php echo currentUrl(true).'?view=subscribe'; ?>">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Pre-register for <?php echo $value['cp-opportunity-card-title']; ?>
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30">
                                                <div class="form margin-b-40">
                                                    <!-- Name -->
                                                    <div class="form-item">
                                                        <label for="name">
                                                            Name
                                                        </label>
                                                        <input type="text" name="full-name" id="full-name">
                                                    </div>
                                                    <!-- Email -->
                                                    <div class="form-item">
                                                        <label for="email">
                                                            Email
                                                        </label>
                                                        <input type="email" name="email" id="email">
                                                    </div>
                                                    <!-- Phone -->
                                                    <div class="form-item">
                                                        <label for="phone">
                                                            Phone
                                                        </label>
                                                        <input type="telephone" name="phone" id="phone">
                                                    </div>
                                                    <!-- State -->
                                                    <div class="form-item">
                                                        <label for="state">
                                                            State of Residence
                                                        </label>
                                                        <select name="state" id="state">
                                                            <option value="" selected disabled hidden>- Select -</option>
                                                            <option value="Abuja FCT">Abuja FCT</option>
                                                            <option value="Abia">Abia</option>
                                                            <option value="Adamawa">Adamawa</option>
                                                            <option value="Akwa Ibom">Akwa Ibom</option>
                                                            <option value="Anambra">Anambra</option>
                                                            <option value="Bauchi">Bauchi</option>
                                                            <option value="Bayelsa">Bayelsa</option>
                                                            <option value="Benue">Benue</option>
                                                            <option value="Borno">Borno</option>
                                                            <option value="Cross River">Cross River</option>
                                                            <option value="Delta">Delta</option>
                                                            <option value="Ebonyi">Ebonyi</option>
                                                            <option value="Edo">Edo</option>
                                                            <option value="Ekiti">Ekiti</option>
                                                            <option value="Enugu">Enugu</option>
                                                            <option value="Gombe">Gombe</option>
                                                            <option value="Imo">Imo</option>
                                                            <option value="Jigawa">Jigawa</option>
                                                            <option value="Kaduna">Kaduna</option>
                                                            <option value="Kano">Kano</option>
                                                            <option value="Katsina">Katsina</option>
                                                            <option value="Kebbi">Kebbi</option>
                                                            <option value="Kogi">Kogi</option>
                                                            <option value="Kwara">Kwara</option>
                                                            <option value="Lagos">Lagos</option>
                                                            <option value="Nassarawa">Nassarawa</option>
                                                            <option value="Niger">Niger</option>
                                                            <option value="Ogun">Ogun</option>
                                                            <option value="Ondo">Ondo</option>
                                                            <option value="Osun">Osun</option>
                                                            <option value="Oyo">Oyo</option>
                                                            <option value="Plateau">Plateau</option>
                                                            <option value="Rivers">Rivers</option>
                                                            <option value="Sokoto">Sokoto</option>
                                                            <option value="Taraba">Taraba</option>
                                                            <option value="Yobe">Yobe</option>
                                                            <option value="Zamfara">Zamfara</option>
                                                            <option value="Outside Nigeria">Outside Nigeria</option>
                                                        </select>
                                                    </div>
                                                    <!-- Program Name -->
                                                    <input type="text" name="program-name" id="program-name" value="<?php echo $value['cp-opportunity-card-title']; ?>" hidden>
                                                    
                                                    <div class="status"></div>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" id="submit-btn" class="submit-btn btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                        <script>
                                            $(document).ready(function() {
                                                
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <?php $card_counter++; ?>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-image-card-group-meta-box'){ ?>
                
        <?php } elseif($content_type == 'cp-faq-meta-box'){ ?>
        
        <section class="container-wrapper padding-tb-60" id="content-<?php echo $content_counter; ?>">
            <div class="row">
                <div class="col-md-11 mx-auto">
                    <header class="margin-b-60 text-center">
                        <h2 class="txt-2-2em txt-bold padding-b-20">
                            <?php echo $group_value['cp-faq-meta-box']['cp-faq-accordion-group-title']; ?>
                        </h2>
                        <p class="txt-height-1-4">
                            <?php echo $group_value['cp-faq-meta-box']['cp-faq-accordion-group-content']; ?>
                        </p> 
                    </header>
                    <div class="row row-10">
                    <?php 

                        $accordionCounter = 1;

                        $values = $group_value['cp-faq-meta-box']['cp-faq-accordion'];

                        foreach ( $values as $value ) { 
                    ?>
                        <div class="col-md-4 padding-lr-10 padding-b-20">
                            <div class="faq-collapse padding">
                                <button class="faq-btn txt-sm txt-height-1-4" type="button" data-toggle="collapse" data-target="#faq-<?php echo $accordionCounter ?>" aria-expanded="false" aria-controls="collapseExample" style="padding-left:30px;">
                                    <?php echo $value['cp-faq-accordion-title']; ?>
                                </button>
                                <div class="collapse" id="faq-<?php echo $accordionCounter ?>">
                                    <div class="card card-body">
                                        <article class="txt-sm txt-height-1-4">
                                            <?php echo $value['cp-faq-accordion-content']; ?>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                            $accordionCounter++;
                        }
                    ?> 
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-form-section-meta-box'){ ?>
        
        <section id="content-<?php echo $content_counter; ?>">
            <div class="row">
                <?php
                    $form_content_type = $group_value['cp-form-section-meta-box']['cp-form-section-content-type'];
                
                    $image_position = $group_value['cp-form-section-meta-box']['cp-form-section-content-type-position'];
                    
                    $image = 0;
                
                    $image_ids = isset( $group_value['cp-form-section-meta-box']['cp-form-section-bg-image'] ) ? $group_value['cp-form-section-meta-box']['cp-form-section-bg-image'] : array();
                                                                  
                    foreach ( $image_ids as $image_id ) {
                        $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                        //echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '">';
                    }
                ?>
                <style>
                    .form-section-bg{
                        background-position: center;
                        background-size: cover;
                        background-repeat: no-repeat;
                        min-height: 280px;
                        
                        <?php if($form_content_type == 'image') { ?>
                            background-image: url('<?php echo $image['full_url']; ?>')
                        <?php } ?>
                    }
                </style>
                <div class="col-md-6 form-section-bg order-md-<?php echo $image_position; ?>">
                    <?php if($form_content_type == 'text') { ?>
                    <div class="container-wrapper padding-tb-40">
                        <h1 class="txt-2em txt-height-1-1 margin-b-20">
                            <?php echo $group_value['cp-form-section-meta-box']['cp-form-section-title']; ?>
                        </h1>
                        <h2 class="txt-height-1-5">
                            <?php echo $group_value['cp-form-section-meta-box']['cp-form-section-content']; ?>
                        </h2>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-md-6 order-md-0">
                    <div class="container-wrapper padding-tb-40">
                        <h3 class="txt-bold txt-lg padding-b-10">
                            <?php echo $group_value['cp-form-section-meta-box']['cp-form-section-form-title']; ?>
                        </h3>
                        <p class="padding-b-20">
                            <?php echo $group_value['cp-form-section-meta-box']['cp-form-section-form-summary']; ?>
                        </p>
                    <style>
                        .gfield  input{
                            width: 100% !important;
                        }    
                    </style>
                    <?php
                        /* Get Form ID from request */
                        $gf_id = $group_value['cp-form-section-meta-box']['cp-form-section-form-id'];

                        if($gf_id){
                            /* Get Form */
                            echo do_shortcode('[gravityform id="'.$gf_id.'" title="false" description="false"]');
                        }
                    ?>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-accordion-meta-box'){ ?>
        
        <section class="row" id="content-<?php echo $content_counter; ?>">
            <div class="col-md-6 txt-color-white">
                <style>
                    .bg-accordion{
                        background-color: <?php echo $group_value['cp-accordion-meta-box']['cp-accordion-group-bg-color']; ?>;
                    }
                </style>
                <div class="bg-accordion container-wrapper padding-tb-80">
                    <h2 class="parent-name">
                        <a href="" class="d-block txt-color-white margin-b-15">
                            <?php echo $group_value['cp-accordion-meta-box']['cp-accordion-group-title']; ?>
                        </a>
                    </h2>
                    <h2 class="txt-2em margin-b-40">
                        <?php echo $group_value['cp-accordion-meta-box']['cp-accordion-group-summary']; ?>
                    </h2>
                    <article class="text-box">
                        <p>
                            <?php echo $group_value['cp-accordion-meta-box']['cp-accordion-group-content']; ?>
                        </p>
                    </article>
                </div>
            </div>
            <div class="col-md-6 bg-grey">
                <div class="container-wrapper padding-tb-80">
                    <div class="faq-accordion margin-b-20">
                        <div id="accordion-<?php echo $accordion_suffix; ?>">
                            <?php 
                                $values = $group_value['cp-accordion-meta-box']['cp-accordion'];
                                $accordionCounter = 1;

                                foreach ( $values as $value ) { ?>

                                    <div class="card">
                                        <div class="card-header" id="heading-<?php echo $post->ID.'-'.$accordionCounter; ?>">
                                            <h5 class="mb-0">
                                                <button 
                                                   class="btn btn-link collapsed" 
                                                   data-toggle="collapse" 
                                                   data-target="#collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>" 
                                                   aria-expanded="false" 
                                                   aria-controls="collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>"
                                                >
                                                    <?php echo $value['cp-accordion-title']; ?>
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>" class="collapse" aria-labelledby="heading-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>>" data-parent="#accordion-<?php echo $accordion_suffix ?>" style="">
                                            <div class="card-body">
                                                <article class="text-box sm">
                                                    <div class="txt-height-1-5">
                                                        <?php echo $value['cp-accordion-content']; ?>
                                                    </div>
                                                </article>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php $accordionCounter++ ?>
                                    
                                <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } ?>
        
    <?php 
        }
    }
?>

<style>
    .how-it-works-container .how-card .number {
        background-color: #f4c026;
        font-size: 1.5em;
        font-weight: 600;
        color: black;
    }
    .how-it-works-container:before {
        background-color: gainsboro;
    }
    
    .collapsible-card {
        border-radius: 4px;
    }
</style>