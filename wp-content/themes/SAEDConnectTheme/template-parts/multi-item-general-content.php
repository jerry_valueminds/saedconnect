    <?php 
        /* Get relationship ID */
        global $relashionship_id;
        global $relationship_parent_id;
    
        $connected = new WP_Query( array(
            'relationship' => array(
                'id'   => $relashionship_id,
                'from' => $relationship_parent_id, // You can pass object ID or full object
            ),
            'nopaging'     => true,
        ) );
        while ( $connected->have_posts() ) : $connected->the_post();
    ?>

            <?php
                $content_type = rwmb_get_value( 'select-content-type' );
                if($content_type == 'gi-text-meta-box'){ ?>

                    <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> cta-block cta-blue">
                        <article>
                            <h2 class="title">
                                <?php the_title(); ?>
                            </h2>
                            <article class="content">
                                <p>
                                    <?php echo rwmb_get_value( 'text_content' ); ?>
                                </p>
                            </article>
                            <div class="btn-wrapper">
                                <a class="cta-btn" href="">
                                    Schedule an Appointment
                                </a>
                            </div>
                        </article>
                    </div>

            <?php } elseif($content_type == 'gi-plain-text-meta-box'){ ?> 

                <!-- Text content -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-lr-40">
                    <div class="margin-b-40">

                        <article class="text-box">
                            <?php echo rwmb_get_value( 'plain-text-content' ); ?>
                        </article>
                    </div>
                </div>
                
            <?php } elseif($content_type == 'gi-text-strip-meta-box'){ ?> 

                <!-- Text Strip content -->
                <div
                    class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-tb-20"
                    style="background-color:<?php echo rwmb_meta( 'text-strip-background-color' ); ?>"
                >
                    <div class="container-wrapper">
                        <h2
                            class="txt-xlg txt-height-1-3"
                            style="color:<?php echo rwmb_meta( 'text-strip-text-color' ); ?>"    
                        >
                            <?php echo rwmb_get_value( 'text-strip-content' ); ?>
                        </h2>
                    </div>
                </div>
                <section class="bg-yellow">
                    
                </section>
                
            <?php } elseif($content_type == 'gi-image-box-meta-box'){ ?> 
                <!-- Image Box -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php

                        $images = rwmb_meta( 'image-box-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>
                    <img src="<?php echo $image['full_url']; ?>" alt="">
                </div>
                
            <?php } elseif($content_type == 'gi-image-meta-box'){ ?> 
                <!-- Background Image & Caption -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php

                        $images = rwmb_meta( 'mini_site_feature_image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>


                    <a class="feature-video-block" style="background-image:url('<?php echo $image['full_url']; ?>');">

                    </a>
                </div>

            <?php } elseif($content_type == 'gi-video-meta-box'){ ?> 
                <!-- Video -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php

                        $images = rwmb_meta( 'video-feature-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>

                    <a class="feature-video-block popup-video" href="<?php echo rwmb_get_value( 'video-link' ); ?>" style="background-image:url('<?php echo $image['full_url']; ?>">
                        <span class="caption">
                            <h3 class="title txt-color-white">
                                <?php the_title() ?>
                            </h3>
                        </span>
                        <div class="play-btn">
                            <i class="fa fa-play"></i>
                        </div>      
                    </a>
                </div>
                
            <?php } elseif($content_type == 'gi-cta-1-meta-box'){ ?> 
                <!-- CTA 1 -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>"
                    style="background-color:<?php echo rwmb_meta( 'cta-1--background-color' ); ?>;"   
                >
                    <div class="youth-dev-card">
                        <div class="content">
                            <header style="color:<?php echo rwmb_meta( 'cta-1--text-color' ); ?>;" class="header">
                                <div class="title">
                                    <?php echo rwmb_meta( 'cta-1-title' ); ?>
                                </div>
                                <p class="txt-normal-s txt-he-ght-1-5 margin-b-30">
                                    <?php echo rwmb_meta( 'cta-1-subtitle' ); ?>
                                </p>
                                <article class="btn-wrapper">
                                    <?php $values = rwmb_meta('cta-1-btns');
                                        foreach ( $values as $value ) {
                                            if($value['footer-btn-type'] == 1){ ?>

                                                <a class="btn btn-trans-wb icon" href="<?php echo $value['footer-btn-url']; ?>">
                                                    <?php echo $value['footer-btn-text']; ?>
                                                </a>

                                            <?php }elseif($value['footer-btn-type'] == 2){ ?>
                                                <a class="btn btn-trans-wb" href="<?php echo $value['footer-btn-url']; ?>">
                                                    <?php echo $value['footer-btn-text']; ?>
                                                </a>
                                            <?php }elseif($value['footer-btn-type'] == 4){ ?>
                                                <a class="btn btn-trans-bw" href="<?php echo $value['footer-btn-url']; ?>">
                                                    <?php echo $value['footer-btn-text']; ?>
                                                </a>
                                            <?php }else{ ?>
                                                <a class="btn btn-white" href="<?php echo $value['footer-btn-url']; ?>">
                                                    <?php echo $value['footer-btn-text']; ?>
                                                </a>
                                            <?php }
                                        }
                                    ?>
                                </article>
                            </header>
                        </div>
                    </div>
                </div>

            <?php } elseif($content_type == 'gi-cta-2-meta-box'){ ?> 
                <!-- CTA 2 -->
                <?php
                    $images = rwmb_meta( 'cta-2-background-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                ?>
                <div
                    class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> feature-image-block"
                    style="background-image:url('<?php echo $image['full_url']; ?>"   
                >

                    <div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							<?php echo rwmb_get_value( 'cta-2-title' ); ?>
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							<?php echo rwmb_get_value( 'cta-2-subtitle' ); ?>
						</p>
                        <article class="btn-wrapper">
                            <?php $values = rwmb_meta('cta-2-btns');
                                foreach ( $values as $value ) {
                                    if($value['footer-btn-type'] == 1){ ?>

                                        <a class="btn btn-trans-wb icon" href="<?php echo $value['footer-btn-url']; ?>">
                                            <?php echo $value['footer-btn-text']; ?>
                                        </a>

                                    <?php }elseif($value['footer-btn-type'] == 2){ ?>
                                        <a class="btn btn-trans-wb" href="<?php echo $value['footer-btn-url']; ?>">
                                            <?php echo $value['footer-btn-text']; ?>
                                        </a>
                                    <?php }else{ ?>
                                        <a class="btn btn-white" href="<?php echo $value['footer-btn-url']; ?>">
                                            <?php echo $value['footer-btn-text']; ?>
                                        </a>
                                    <?php }
                                }
                            ?>
                        </article>
					</div>
                </div>

            <?php } elseif($content_type == 'gi-image-gallery-meta-box'){ ?> 
               
                <!-- Image Gallery -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php

                        $images = rwmb_meta( 'image-gallery', array( 'size' => 'thumbnail' ) );
                    ?>

                    <div class="generic-gallery relative">
                        <a class="swiper-wrapper" href="gallery">
                        <?php foreach ( $images as $image ) { ?>  



                            <div class="swiper-slide">
                                <div class="home-slider-item">
                                    <div
                                        class="slide-bg"
                                        style="background-image:url('<?php echo $image['full_url']; ?>');">
                                    </div>
                                </div>
                            </div>


                        <?php } ?>    
                        </a>
                        <!-- Slider Caption -->
                        <div class="caption">
                            <h4>
                                <?php echo rwmb_get_value( 'image-galley-subtitle' ); ?>
                            </h4>
                            <h3 class="title">
                                <?php the_title() ?>
                            </h3>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div> 

            <?php } elseif($content_type == 'gi-text-gallery-meta-box'){ ?> 
                <!-- Text Gallery -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php $values = rwmb_meta('text-gallery'); ?>

                    <div class="generic-gallery-text relative">
                        <div class="swiper-wrapper">
                        <?php foreach ( $values as $value ) { ?>  

                            <div class="swiper-slide">
                                <div class="slider-content">
                                    <h3 class="title"><?php echo $value['text-slide-title']; ?></h3>
                                    <p class="description">
                                        <?php echo $value['text-slide-content']; ?>    
                                    </p>
                                    <div class="margin-t-30">
                                        <a class="btn btn-trans-wb" href="<?php echo $value['text-slide-btn-url']; ?>">
                                            <?php echo $value['text-slide-btn-text']; ?>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>    
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>   
                </div>

            <?php } elseif($content_type == 'gi-quote-meta-box'){ ?> 
                <!-- Quote -->
                <div
                    class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> cta-block bg-black txt-color-white"
                    style="background-color:<?php echo rwmb_meta( 'quote-background-color' ); ?>"   
                >
                    <div class="txt-3em txt-color-white padding-tb-20 border-t-4 border-color-white">
                        <i class="fa fa-quote-left"></i>
                    </div>
                    <h2 class="title">
                        <?php echo rwmb_get_value( 'quote-content' ); ?>
                    </h2>
                    <div class="txt-medium txt-height-1-2">
                        <?php echo rwmb_get_value( 'name-of-quoter' ); ?>
                        <br>
                        <?php echo rwmb_get_value( 'description-of-quoter' ); ?>
                    </div>
                    <?php if(rwmb_get_value( 'quote-btn-text' )){ ?>
                        <div class="margin-t-30">
                            <a
                               class="btn btn-trans-wb"
                               href="<?php echo rwmb_get_value( 'quote-btn-link' ); ?>"
                               <?php
                                    if(rwmb_get_value( 'quote-link-target' )=="new") echo "target='_blank'";
                               ?>
                               >
                                <?php echo rwmb_get_value( 'quote-btn-text' ); ?>
                            </a>
                        </div>
                    <?php } ?>
                </div>

            <?php } elseif($content_type == 'gi-event-meta-box'){ ?> 
                <!-- Event -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php

                        $images = rwmb_meta( 'event-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>


                    <a class="feature-video-block" style="background-image:url('<?php echo $image['full_url']; ?>');">
                        <span class="caption">
                            <h3 class="title txt-color-white">
                                <?php the_title(); ?>
                            </h3>
                        </span>
                        <div class="play-btn">
                            <i>
                                <div class="month" style="text-transform: uppercase;">
                                    <?php rwmb_the_value( 'event-date', array( 'format' => 'M' ) ); ?>
                                </div>
                                <div class="day">
                                    <?php rwmb_the_value( 'event-date', array( 'format' => 'd' ) ); ?>
                                </div>
                            </i>
                        </div>   
                    </a>
                </div>

            <?php } elseif($content_type == 'gi-get-in-touch-meta-box'){ ?> 
                <!-- Get in Touch -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> cta-block">
                    <h4 class="txt-xlg margin-b-30">
                        <?php echo rwmb_get_value( 'get-in-touch-title' ); ?>
                    </h4>
                    <div class="row row-20">
                    <?php $values = rwmb_meta('Contact-items'); ?>
                    <?php foreach($values as $value){ ?>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                <?php echo $value['Contact-item-name']; ?>
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                <?php echo $value['Contact-item-position']; ?>
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                <?php echo $value['Contact-item-email']; ?>
                            </h4>
                        </div>
                    <?php } ?>
                    </div>
                </div>
                
            <?php } elseif($content_type == 'gi-footer-meta-box'){ ?> 
                <!-- Pre-footer -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php

                        $images = rwmb_meta( 'feature-background-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>

                    <div class="pre-footer" style="background-image:url('<?php echo $image['full_url']; ?>');">
                        <div class="content">
                            <h4 class="title">
                                <?php the_title(); ?>
                            </h4>
                            <article class="btn-wrapper">
                                <?php $values = rwmb_meta('footer-btns');
                                    foreach ( $values as $value ) {
                                        if($value['footer-btn-type'] == 1){ ?>

                                            <a class="btn btn-trans-wb icon" href="<?php echo $value['footer-btn-url']; ?>">
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>

                                        <?php }elseif($value['footer-btn-type'] == 2){ ?>
                                            <a class="btn btn-trans-wb" href="<?php echo $value['footer-btn-url']; ?>">
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>
                                        <?php }else{ ?>
                                            <a class="btn btn-white" href="<?php echo $value['footer-btn-url']; ?>">
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>
                                        <?php }
                                    }
                                ?>
                            </article>
                        </div>
                    </div>
                </div>
                
            <?php } elseif($content_type == 'gi-footer-contact-meta-box'){ ?> 
                <!-- Footer - Contact -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php

                        $images = rwmb_meta( 'footer-contact-background-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>

                    <div class="pre-footer" style="background-image:url('<?php echo $image['full_url']; ?>');">
                        
                        <div class="content">
                            <h4 class="title">
                                <?php echo rwmb_get_value( 'footer-contact-title' ); ?>
                            </h4>
                            <p class="txt-height-1-4 txt-color-white margin-b-40">
                                <?php echo rwmb_get_value( 'footer-contact-content' ); ?>
                            </p>
                            <article class="btn-wrapper margin-b-40">
                                <a class="btn btn-trans-wb-icon" href="<?php echo rwmb_get_value( 'footer-contact-facebook' ); ?>">
                                    Connect on Facebook
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a class="btn btn-trans-wb-icon" href="<?php echo rwmb_get_value( 'footer-contact-twitter' ); ?>">
                                    Follow us on Twitter
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a class="btn btn-trans-wb-icon" href="<?php echo rwmb_get_value( 'footer-contact-telegram' ); ?>">
                                    Join the Telegram Channel
                                    <i class="fa fa-telegram"></i>
                                </a>
                            </article>
                            <article>
                                <h4 class="txt-xlg txt-color-white margin-b-20">Join the Training Support Mailing List</h4>
                                <form action="" class="mailing-list-form">
                                    <input class="mail-field" type="email" placeholder="Enter your email address">
                                    <input class="submit-btn" type="submit" value="subscribe">
                                </form>
                            </article>
                        </div>
                    </div>
                    
                </div> 
            
            <?php } ?> 

    <?php
        endwhile;
        wp_reset_postdata();
    ?>
