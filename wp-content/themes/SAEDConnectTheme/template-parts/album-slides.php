   <?php 
        global $relashionship_id;
        global $relationship_parent_id;
    
        $connected = new WP_Query( array(
            'relationship' => array(
                'id'   => $relashionship_id,
                'from' => $relationship_parent_id, // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
        while ( $connected->have_posts() ) : $connected->the_post();
    ?>

            <?php
                $content_type = rwmb_get_value( 'select-content-type' );
                if($content_type == 'gi-image-meta-box'){ 
            ?>

                <div class="swiper-slide">
                    <div class="slider-item">
                        <?php
                            $images = rwmb_meta( 'mini_site_feature_image', array( 'limit' => 1 ) );
                            $image = reset( $images );
                        ?>
                        <img src="<?php echo $image['full_url']; ?>" alt="">
                    </div>
                    <div class="container-wrapper padding-tb-40 gallery-item-details">
                        <h2 class="subtitle"><?php echo rwmb_get_value( 'album-subtitle' ); ?></h2>
                        <h1 class="title"><?php echo rwmb_get_value( 'album-title' ); ?></h1>
                        <article class="text-box">
                            <?php echo rwmb_get_value( 'album-description' ); ?>
                        </article>
                    </div>
                </div>

            <?php } elseif($content_type == 'gi-video-meta-box'){ ?> 
              
                <div class="swiper-slide">
                    <div class="slider-item">
                        <iframe width="100%" height="100%" src="<?php echo rwmb_get_value( 'video-link' ); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                    <div class="container-wrapper padding-tb-40 gallery-item-details">
                        <h2 class="subtitle"><?php echo rwmb_get_value( 'album-subtitle' ); ?></h2>
                        <h1 class="title"><?php echo rwmb_get_value( 'album-title' ); ?></h1>
                        <article class="text-box">
                            <?php echo rwmb_get_value( 'album-description' ); ?>
                        </article>
                    </div>
                </div>
                
            <?php } ?>

    <?php
        endwhile;
        wp_reset_postdata();
    ?>
