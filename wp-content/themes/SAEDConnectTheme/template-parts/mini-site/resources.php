<?php
    $topicID = get_the_ID();
    $topicName = get_the_title();
    $topic_url = get_permalink();
    $resource_view_url = get_permalink().'?view=resources';
    $images = rwmb_meta( 'mini-site-sponsor-image', array( 'limit' => 1 ) );
    $image = reset( $images );
    $sponsorImage = $image['full_url'];

    $filter = 'all';

    if( $_REQUEST['filter'] ){
        $filter = $_REQUEST['filter'];
    }

    $group_values = rwmb_meta( 'article-resources-group' );

        /*
        *=============================================
        * Second Query to get Section Name 
        *=============================================
        */
        $sectionNameQuery = new WP_Query( array(
            'relationship' => array(
                'id'   => 'og_section_to_guide_topic',
                'to' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
        while ( $sectionNameQuery->have_posts() ) : $sectionNameQuery->the_post();  
                $sectionName = get_the_title();
                $section_id = get_the_ID();
                $section_theme_color = rwmb_meta( 'section-theme-color' );
                $section_hover_color = rwmb_meta( 'section-link-hover-color' );

        endwhile;
        wp_reset_postdata();
?>

<?php get_header() ?>

<main class="main-content">
    <header class="overview-header container-wrapper">
        <div class="info-box">
            <div class="info">
                <h2 class="subtitle">
                    <?php echo $sectionName ?>
                </h2>
                <h1 class="title txt-color-lighter">
                    <?php echo $topicName ?>
                </h1>
            </div>
            <div class="sponsor">
                <div class="intro-text">
                    Content Sponsored by:
                </div>
                <img class="logo" src="<?php echo $sponsorImage ?>" alt="">
            </div>
        </div>
        <nav class="header-nav">
            <div class="header-nav-title">
                <span class="name">
                    Submenu
                </span>
                <span class="icon"></span>
            </div>
            <ul>
                <li>
                    <a href="<?php echo $topic_url ?>">Your Guide</a>
                </li>
                <li class="active">
                    <a href="<?php echo $topic_url ?>/?view=resources">Tools &amp; Resources</a>
                </li>
            </ul>
        </nav>
    </header>
    <section class="container-wrapper">
        <div class="row row-40">
            <!-- Left Side Menu -->               
            <div class="col-md-3 padding-lr-40">
                <article class="side-nav white bg-white padding-tb-40" id="side-nav">
                    <h2 class="title">
                        Filter
                    </h2>
                    <ul class="list txt-normal-s">
                        <li class="<?php echo $filter == 'all'  ? 'active' : '' ?>">
                            <a
                                href="<?php echo $resource_view_url ?>"
                                class="<?php echo $filter == 'all'  ? 'padding-l-10' : '' ?>"
                            >
                               All
                           </a>
                        </li>
                        <li class="<?php echo $filter == 'video'  ? 'active' : '' ?>">
                            <a
                                href="<?php echo $resource_view_url.'&filter=video' ?>"
                                class="<?php echo $filter == 'video'  ? 'padding-l-10' : '' ?>"
                            >
                               Video
                           </a>
                        </li>
                        <li class="<?php echo $filter == 'podcast'  ? 'active' : '' ?>">
                            <a
                                href="<?php echo $resource_view_url.'&filter=podcast' ?>"
                                class="<?php echo $filter == 'podcast'  ? 'padding-l-10' : '' ?>"
                            >
                               Podcast
                           </a>
                        </li>
                        <li class="<?php echo $filter == 'article'  ? 'active' : '' ?>">
                            <a
                                href="<?php echo $resource_view_url.'&filter=article' ?>"
                                class="<?php echo $filter == 'article'  ? 'padding-l-10' : '' ?>"
                            >
                               Article
                           </a>
                        </li>
                    </ul>
                </article>
            </div>
            <div class="col-md-9 padding-t-40 padding-lr-40 border-l-1 border-color-darkgrey">
            <?php 
                if ( ! empty( $group_values ) ) {
                    foreach ( $group_values as $group_value ) {    
                        if( $filter == 'all' || $filter == $group_value['resource-content-type'] ){
            ?>
                    <figure class="newsroom-card padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                        <figcaption>
                            <h3 class="subtitle uppercase margin-b-10">
                                <?php echo $group_value['resource-content-type'] ?>
                            </h3>
                            <h2 class="txt-lg txt-medium margin-b-20">
                                <a class="txt-color-dark" href="<?php echo $group_value['resource-link'] ?>" target="_blank">
                                    <?php echo $group_value['resource-title'] ?>
                                </a>
                            </h2>
                            <p class="txt-normal-s">
                                <?php echo $group_value['resource-summary'] ?>
                            </p>
                        </figcaption>
                    </figure>
                
            <?php
                        }
                    }
                }
            ?>
                <!--<section class="container-wrapper">
                    <div class="btn-wrapper text-center">
                        <a class="btn btn-trans-green margin-r-40" href="">
                            Show More
                        </a>
                    </div>
                </section>-->
            </div>
            <!-- Right Side Content -->
            <!--<div class="col-md-3">
                <div class="cta-block fluid padding-lr-40 border-b-1 border-color-darkgrey">
                    <article>
                        <figure class="margin-b-20">
                            <img class="rounded-corners" src="images/heroes/hero_1.jpg" alt="">
                        </figure>
                        <h2 class="txt-lg txt-bold txt-color-green margin-b-20">
                            Maero Uwede
                        </h2>
                        <article class="margin-b-20">
                            <p class="txt-normal-s">
                                Maero has sold screenplays and written for many studios, including Disney.
                            </p>
                        </article>
                        <div class="btn-wrapper">
                            <a class="btn btn-trans-gg txt-sm" href="">
                                Read More
                            </a>
                        </div>
                    </article>
                </div>
                <div class="cta-block fluid padding-lr-40 border-b-1 border-color-darkgrey">
                    <article>
                        <h2 class="txt-lg txt-bold margin-b-20">
                            Maero Uwede
                        </h2>
                        <article class="margin-b-20">
                            <p class="txt-normal-s">
                                Maero has sold screenplays and written for many studios, including Disney.
                            </p>
                        </article>
                        <div class="btn-wrapper">
                            <a class="btn btn-trans-gg txt-sm" href="">
                                Read More
                            </a>
                        </div>
                    </article>
                </div>
                <div class="cta-block fluid padding-lr-40 border-b-1 border-color-darkgrey">
                    <article>
                        <figure class="margin-b-20">
                            <img class="rounded-corners" src="images/heroes/hero_2.jpg" alt="">
                        </figure>
                    </article>
                </div>
            </div>-->
        </div>
    </section>
</main>

<?php get_footer() ?>