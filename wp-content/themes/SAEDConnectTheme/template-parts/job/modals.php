<?php if( $current_user->ID == $post_author_id ) { ?>

<!-- FIlter -->
<div class="modal fade font-main filter-modal" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filter Applicants</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="margin-b-5">
                    <h2>
                        <a class=" d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" data-toggle="collapse" href="#collapse-filter-1" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <span class="txt-color-dark">
                                Hightest Qualification
                            </span>
                            <span class="dropdown-toggle txt-color-dark"></span>
                        </a>
                    </h2>
                    <div class="collapse txt-sm" id="collapse-filter-1">
                        <div class="border-o-1 border-color-darkgrey padding-o-15">
                            <span class="d-inline-flex align-items-center padding-r-40">
                                <input type="checkbox" id="minimal-radio-3" name="radio-1" value="beginner">
                                <label for="minimal-radio-3" class="padding-l-5">BSc</label>
                            </span>
                            <span class="d-inline-flex align-items-center padding-r-40">
                                <input type="checkbox" id="minimal-radio-3" name="radio-1" value="beginner" checked>
                                <label for="minimal-radio-3" class="padding-l-5">MSc</label>
                            </span>
                            <span class="d-inline-flex align-items-center padding-r-40">
                                <input type="checkbox" id="minimal-radio-3" name="radio-1" value="beginner">
                                <label for="minimal-radio-3" class="padding-l-5">Phd</label>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="margin-b-5">
                    <h2>
                        <a class=" d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" data-toggle="collapse" href="#collapse-filter-2" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <span class="txt-color-dark">
                                Gender
                            </span>
                            <span class="dropdown-toggle txt-color-dark"></span>
                        </a>
                    </h2>
                    <div class="collapse txt-sm" id="collapse-filter-2">
                        <div class="border-o-1 border-color-darkgrey padding-o-15">
                            <span class="d-inline-flex align-items-center padding-r-40">
                                <input type="checkbox" id="minimal-checkbox-3" name="radio-2" value="beginner" checked>
                                <label for="minimal-radio-3" class="padding-l-5">Male</label>
                            </span>
                            <span class="d-inline-flex align-items-center padding-r-40">
                                <input type="checkbox" id="minimal-checkbox-3" name="radio-2" value="beginner">
                                <label for="minimal-radio-3" class="padding-l-5">Female</label>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-blue txt-xs">Filter Applicants</button>
            </div>
        </div>
    </div>
</div>

<!-- Job Specification -->
<div class="modal fade font-main filter-modal" id="specificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-specification'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Specifications</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $postName = $post->post_title;
                        $jobType = get_post_meta( $post->ID, 'job-type', true );
                        $company = get_post_meta( $post->ID, 'company', true );
                        $jobFunction = get_post_meta( $post->ID, 'job-function', true );
                        $jobLevel = get_post_meta( $post->ID, 'job-level', true );
                        $physicalPresence = get_post_meta( $post->ID, 'physical-presence', true );
                        $deadline = get_post_meta( $post->ID, 'deadline', true );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-specification'){

                            /* Get Post Name */
                            $postName = sanitize_text_field( $_POST['post-name'] );
                            $jobType = sanitize_text_field( $_POST['job-type'] );
                            $company = sanitize_text_field( $_POST['company'] );
                            $jobFunction = sanitize_text_field( $_POST['job-function'] );
                            $jobLevel = sanitize_text_field( $_POST['job-level'] );
                            $physicalPresence = sanitize_text_field( $_POST['physical-presence'] );
                            $deadline = sanitize_text_field( $_POST['deadline'] );

                            /* Save Post to DB */
                            $post_id = wp_insert_post(array (
                                'ID' => $post_id,
                                'post_type' => $post_type,
                                'post_title' => $postName,
                                'post_content' => "",
                                'post_status' => 'publish',
                            ));

                            update_post_meta( $post_id, 'job-type', $jobType );
                            update_post_meta( $post_id, 'company', $company );
                            update_post_meta( $post_id, 'job-function', $jobFunction );
                            update_post_meta( $post_id, 'job-level', $jobLevel );
                            update_post_meta( $post_id, 'physical-presence', $physicalPresence );
                            update_post_meta( $post_id, 'deadline', $deadline );

                            /* Save terms to post */
                            wp_set_post_terms( $post_id, $_POST['nigerian-state'], 'nigerian-state' );
                            wp_set_post_terms( $post_id, $_POST['industry'], 'industry' );
                            wp_set_post_terms( $post_id, $_POST['capability'], 'capability' );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Job Type -->
                        <div class="form-item">
                            <label for="job-type">
                                Job Type
                            </label>
                            <select name="job-type" required>
                                <option value="fullTime" <?php echo ($jobType == 'fullTime')? "selected" : "" ?>>
                                    Full-time
                                </option>
                                <option value="freelancer" <?php echo ($jobType == 'freelancer')? "selected" : "" ?>>
                                    Freelancer/Independent Contractor
                                </option>
                                <option value="distributorOpportunity" <?php echo ($jobType == 'distributorOpportunity')? "selected" : "" ?>>
                                    Distributor Opportunities
                                </option>
                                <option value="trainingJob" <?php echo ($jobType == 'trainingJob')? "selected" : "" ?>>
                                    Training Job
                                </option>
                                <option value="dataCollection" <?php echo ($jobType == 'dataCollection')? "selected" : "" ?>>
                                    Data Collection Opportunities
                                </option>
                            </select>
                        </div>
                        <!-- Job Name -->
                        <div class="form-item">
                            <label for="post-name" class="conditional" data-condition="['fullTime','partTime'].includes(job-type)">
                                Job Title
                            </label>
                            <label for="post-name" class="conditional" data-condition="['freelancer'].includes(job-type)">
                                What do you need the freelancer to help you do?
                            </label>
                            <input 
                                type="text" 
                                name="post-name" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo $postName ?>"
                                required
                            >
                        </div>
                        <!-- Job Name -->
                        <div class="form-item">
                            <label for="post-name">
                                Company
                            </label>
                            <input 
                                type="text" 
                                name="company" 
                                value="<?php echo $company ?>"
                            >
                        </div>
                        <!-- Location -->
                        <div>
                           <?php
                                $tax_name = 'Locations';
                                $tax_slug = 'nigerian-state';
                            ?>
                            <h2 class="txt-medium margin-b-20">
                                <a 
                                   class="d-flex justify-content-between dropdown-toggle txt-color-blue" 
                                   data-toggle="collapse" 
                                   href="#collapseLocation" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Select <?php echo $tax_name; ?>
                                </a>
                            </h2>
                            <div id="collapseLocation" class="collapse" aria-labelledby="headingOne">
                                <div class="padding-b-20">
                                <?php 
                                    /* Return Terms assigned to Post */
                                    $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                                    /*
                                    *
                                    * Populate Form Data from Terms
                                    *
                                    */
                                    //Get Terms
                                    $terms = get_terms( $tax_slug, array('hide_empty' => false));

                                    foreach ($terms as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $parent = $term->parent;
                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                ?>

                                    <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                        <input
                                            class="margin-r-5"
                                            type="checkbox" 
                                            value="<?php echo $term_id ?>" 
                                            name="<?php echo $tax_slug ?>[]" 
                                            <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                        >
                                        <span class="bg-label padding-l-5">
                                            <?php echo $term_name; ?>
                                        </span>
                                    </label>

                                <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!-- Job Function -->
                        <div class="form-item conditional" data-condition="['fullTime','partTime'].includes(job-type)">
                            <label for="job-function">
                                Job Function
                            </label>
                            <input 
                                type="text" 
                                name="job-function" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo $jobFunction ?>"
                            >
                        </div>
                        <!-- Industries-->
                        <div class="conditional" data-condition="['fullTime','partTime'].includes(job-type)">
                           <?php
                                $tax_name = 'Industries';
                                $tax_slug = 'industry';
                            ?>
                            <h2 class="txt-medium margin-b-20">
                                <a 
                                   class="d-flex justify-content-between dropdown-toggle txt-color-blue" 
                                   data-toggle="collapse" 
                                   href="#collapseIndustries" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Select <?php echo $tax_name; ?>
                                </a>
                            </h2>
                            <div id="collapseIndustries" class="collapse" aria-labelledby="headingOne">
                                <div class="padding-b-20">
                                <?php 
                                    /* Return Terms assigned to Post */
                                    $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                                    /*
                                    *
                                    * Populate Form Data from Terms
                                    *
                                    */
                                    //Get Terms
                                    $terms = get_terms( $tax_slug, array('hide_empty' => false));

                                    foreach ($terms as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $parent = $term->parent;
                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                ?>

                                    <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                        <input
                                            class="margin-r-5"
                                            type="checkbox" 
                                            value="<?php echo $term_id ?>" 
                                            name="<?php echo $tax_slug ?>[]" 
                                            <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                        >
                                        <span class="bg-label padding-l-5">
                                            <?php echo $term_name; ?>
                                        </span>
                                    </label>

                                <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!-- Job Level -->
                        <div class="form-item conditional" data-condition="['fullTime','partTime'].includes(job-type)">
                            <label for="job-type">
                                Job Level
                            </label>
                            <select name="job-level">
                                <option value="internship" <?php echo ($jobLevel == 'internship')? "selected" : "" ?>>
                                    Internship
                                </option>
                                <option value="entry" <?php echo ($jobLevel == 'entry')? "selected" : "" ?>>
                                    Entry Level
                                </option>
                                <option value="officer" <?php echo ($jobLevel == 'officer')? "selected" : "" ?>>
                                    Officer Level
                                </option>
                                <option value="mid" <?php echo ($jobLevel == 'mid')? "selected" : "" ?>>
                                    Mid Level
                                </option>
                                <option value="management" <?php echo ($jobLevel == 'management')? "selected" : "" ?>>
                                    Management Level
                                </option>
                            </select>
                        </div>
                        <!-- Type of work -->
                        <div class="conditional" data-condition="['freelancer'].includes(job-type)">
                            <div class="accordion" id="capabilityAccordion">


                            <?php
                                $tax_name = 'Type of Work';
                                $tax_slug = 'capability';
                            ?>
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_name; ?>
                            </h2>

                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_slug, array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <?php if( $parent == 0 ){ ?>

                                    <div class="padding-b-10">
                                        <div class="txt-sm txt-medium  margin-b-15">
                                            <a 
                                               class="d-flex justify-content-between dropdown-toggle txt-color-dark" 
                                               data-toggle="collapse" 
                                               href="#collapseCapability-<?php echo $term_id ?>" aria-expanded="false" aria-controls="collapseOne"
                                            >
                                                <?php echo $term_name; ?>
                                            </a>
                                        </div>
                                        <div id="collapseCapability-<?php echo $term_id ?>" class="collapse" aria-labelledby="headingOne" data-parent="#capabilityAccordion">
                                            <?php
                                                foreach ($terms as $child_term) {
                                                    // Check and see if the term is a top-level parent. If so, display it.
                                                    $child_parent = $child_term->parent;
                                                    $child_term_id = $child_term->term_id; //Get the term ID
                                                    $child_term_name = $child_term->name; //Get the term name

                                                    if( $child_parent == $term_id ){
                                            ?>
                                                    <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                        <input
                                                            class="margin-r-5"
                                                            type="checkbox" 
                                                            value="<?php echo $child_term_id ?>" 
                                                            name="<?php echo $tax_slug ?>[]" 
                                                            <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                        >
                                                        <span class="bg-label">
                                                            <?php echo $child_term_name; ?>
                                                        </span>
                                                    </label>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </div>
                                    </div>

                                <?php } ?>

                            <?php } ?>
                            </div>
                        </div>
                        <!-- Physical Presence -->
                        <div class="form-item conditional" data-condition="['freelancer'].includes(job-type)">
                            <label for="job-type">
                                Does the job require candidate's physical presence?
                            </label>
                            <select name="physical-presence">
                                <option value="yes" <?php echo ($physicalPresence == 'yes')? "selected" : "" ?>>
                                    Yes
                                </option>
                                <option value="no" <?php echo ($physicalPresence == 'no')? "selected" : "" ?>>
                                    No. Candidate can work from anywhere
                                </option>
                                <option value="flexible" <?php echo ($physicalPresence == 'flexible')? "selected" : "" ?>>
                                    Flexible
                                </option>
                            </select>
                        </div>
                        <!-- Deadline -->
                        <div class="form-item conditional" data-condition="['freelancer'].includes(job-type)">
                            <label for="deadline">
                                Deadline to deliver job
                            </label>
                            <input type="date" name="deadline" value="<?php echo $deadline ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>
    
<!-- Job Description --><!-- Job Description -->
<div class="modal fade font-main filter-modal" id="descriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-description'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Job Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $jobSummary = get_post_meta( $post->ID, 'job-summary', true );
                        $jobDescription = get_post_meta( $post->ID, 'job-description', true );
                        $paymentType = get_post_meta( $post->ID, 'payment-type', true );
                        $offerAmount = get_post_meta( $post->ID, 'offer-amount', true );
                        $offerAmountConfidential = get_post_meta( $post->ID, 'offer-amount-confidential', true );
                        $commissionAmount = get_post_meta( $post->ID, 'commission-amount', true );
                        $commissionAmountConfidential = get_post_meta( $post->ID, 'commission-amount-confidential', true );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-description'){

                            /* Get Post Name */
                            $jobSummary = sanitize_text_field( $_POST['job-summary'] );
                            $jobDescription = wp_kses_post( $_POST['job-description'] );
                            $paymentType = sanitize_text_field( $_POST['payment-type'] );
                            $offerAmount = sanitize_text_field( $_POST['offer-amount'] );
                            $offerAmountConfidential = sanitize_text_field( $_POST['offer-amount-confidential'] );
                            $commissionAmount = sanitize_text_field( $_POST['commission-amount'] );
                            $commissionAmountConfidential = sanitize_text_field( $_POST['commission-amount-confidential'] );

                            /* Update Meta */
                            update_post_meta( $post_id, 'job-summary', $jobSummary );
                            update_post_meta( $post_id, 'job-description', $jobDescription );
                            update_post_meta( $post_id, 'payment-type', $paymentType );
                            update_post_meta( $post_id, 'offer-amount', $offerAmount );
                            update_post_meta( $post_id, 'offer-amount-confidential', $offerAmountConfidential );
                            update_post_meta( $post_id, 'commission-amount', $commissionAmount );
                            update_post_meta( $post_id, 'commission-amount-confidential', $commissionAmountConfidential );


                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Summary -->
                        <div class="form-item">
                            <label for="post-name">
                                Job Summary
                            </label>
                            <textarea name="job-summary" cols="30" rows="4"><?php echo $jobSummary ?></textarea>
                        </div>
                        <!-- Description -->
                        <div class="form-item">
                            <label for="post-name">
                                Full Job Description
                            </label>
                            <textarea class="editor" name="job-description" cols="30" rows="20"><?php echo $jobDescription ?></textarea>
                        </div>
                        <!-- Job Type -->
                        <div class="form-item">
                            <label for="job-type">
                                Payment Type
                            </label>
                            <select name="payment-type" required>
                                <option value="commission" <?php echo ($paymentType == 'commission')? "selected" : "" ?>>
                                    Commission
                                </option>
                                <option value="oneOff" <?php echo ($paymentType == 'oneOff')? "selected" : "" ?>>
                                    One-off
                                </option>
                                <option value="salary" <?php echo ($paymentType == 'salary')? "selected" : "" ?>>
                                    Salary
                                </option>
                            </select>
                        </div>

                        <!-- Offer Amount -->
                        <div class="row conditional" data-condition="['salary','oneOff'].includes(payment-type)">
                            <div class="col-12 form-item">
                                <label for="offer-amount">
                                    Offer Amount
                                </label>
                                <input 
                                    type="number" 
                                    name="offer-amount" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $offerAmount ?>"
                                >
                            </div>
                            <div class="col-12 d-flex align-items-center">
                                <label for="offer-amount-confidential" class="txt-normal-s txt-medium padding-r-10">
                                    Confidential
                                </label>
                                <input 
                                    type="checkbox" 
                                    name="offer-amount-confidential" 
                                    <?php echo ($offerAmountConfidential)? "checked" : "" ?>
                                >
                            </div>
                        </div>


                        <!-- Offer Amount -->
                        <div class="row conditional" data-condition="['commission'].includes(payment-type)">
                            <div class="col-12 form-item">
                                <label for="commission-amount">
                                    Commission Amount
                                </label>
                                <input 
                                    type="number" 
                                    name="commission-amount" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $commissionAmount ?>"
                                >
                            </div>
                            <div class="col-12 d-flex align-items-center">
                                <label for="commission-amount-confidential" class="txt-normal-s txt-medium padding-r-10">
                                    Confidential
                                </label>
                                <input 
                                    type="checkbox" 
                                    name="commission-amount-confidential" 
                                    <?php echo ($commissionAmountConfidential)? "checked" : "" ?>
                                >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Task -->
<div class="modal fade font-main filter-modal" id="taskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-tasks'; ?>" method="post">
                <div class="modal-header padding-lr-40">
                    <h5 class="modal-title" id="exampleModalLabel">Assign Tasks to this Job</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-40">
                    <div class="accordion" id="taskCategory">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $jobSummary = get_post_meta( $post->ID, 'job-summary', true );
                        $jobDescription = get_post_meta( $post->ID, 'job-description', true );
                        $paymentType = get_post_meta( $post->ID, 'payment-type', true );
                        $offerAmount = get_post_meta( $post->ID, 'offer-amount', true );
                        $offerAmountConfidential = get_post_meta( $post->ID, 'offer-amount-confidential', true );
                        $commissionAmount = get_post_meta( $post->ID, 'commission-amount', true );
                        $commissionAmountConfidential = get_post_meta( $post->ID, 'commission-amount-confidential', true );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-tasks'){

                            /* Get Post Name */
                            print_r($_POST);
                            $serialized_data = maybe_serialize($_POST);

                            /* Update Meta */
                            update_post_meta( $post_id, 'assigned-tasks', $serialized_data );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>
                    
                    
                    
                    <?php
                        /*
                        *==================================================================================
                        *==================================================================================
                        *   WP Query
                        *==================================================================================
                        *==================================================================================
                        */
                        // Create Query Argument
                        $args = array(
                            'post_type' => 'job-task',
                            'showposts' => -1,
                        );


                        $custom_query = new WP_Query($args);

                        $tax_name = 'Type of Work';
                        $tax_slug = 'industry';
                    ?>


                    <?php 
                        $unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'assigned-tasks', true ) );
                                                  
                        /* Return Terms assigned to Post */
                        $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                        /*
                        *
                        * Populate Form Data from Terms
                        *
                        */
                        //Get Terms
                        $terms = get_terms( $tax_slug, array('hide_empty' => false));

                        foreach ($terms as $term) { //Cycle through terms, one at a time

                            // Check and see if the term is a top-level parent. If so, display it.
                            $parent = $term->parent;
                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                    ?>


                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#collapseCapability-<?php echo $term_id ?>" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    <?php echo $term_name; ?>
                                </a>
                            </div>
                            <div id="collapseCapability-<?php echo $term_id ?>" class="collapse" aria-labelledby="headingOne" data-parent="#taskCategory">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                <?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>
                                    <?php if( has_term( $term_id, $tax_slug ) ) { ?>
                                        <div class="margin-b-10 d-flex justify-content-between align-items-center">
                                            <span class="d-inline-flex align-items-center padding-r-40">
                                                <input 
                                                    type="checkbox" 
                                                    id="minimal-checkbox-<?php echo get_the_ID() ?>" 
                                                    name="job-tasks[]" 
                                                    value="<?php echo get_the_ID() ?>"
                                                    <?php echo in_array(get_the_ID(), $unserialized_data['job-tasks']) ? "checked" : "" ?>
                                                >
                                                <label 
                                                    for="minimal-checkbox-<?php echo get_the_ID() ?>" 
                                                    class="txt-sm padding-l-15"
                                                >
                                                    <?php echo the_title(); ?>
                                                </label>
                                            </span>
                                            <a 
                                                class="txt-sm txt-color-blue"
                                                data-toggle="collapse" 
                                                href="#collapseTaskDetails-<?php echo get_the_ID() ?>" aria-expanded="false"
                                            >
                                                View Details 
                                                <i class="fa fa-angle-down padding-l-5"></i>
                                            </a>
                                        </div>
                                        <div id="collapseTaskDetails-<?php echo get_the_ID() ?>" class="collapse" aria-labelledby="headingOne">
                                            <article class="text-box txt-sm">
                                                <p>
                                                    Each applicant must desmonstrate competence in developing a WordPress site. 
                                                </p>
                                            </article>
                                        </div>
                                    <?php } ?>
                                <?php endwhile; ?>
                                </div>
                            </div>
                        </div>


                    <?php } ?>
                    </div>
                </div>
                <div class="modal-footer padding-lr-40">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Application Method -->
<div class="modal fade font-main filter-modal" id="applicationMethodModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-application-method'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Select Application Method</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                        $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                        $howToApply = get_post_meta( $post_id, 'how-to-apply', true );


                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-application-method'){

                            /* Get Post Name */
                            $applicationMethod = sanitize_text_field( $_POST['application-method'] );
                            $applicationLink = sanitize_text_field( $_POST['3rd-party-application-link'] );
                            $howToApply = wp_kses_post( $_POST['how-to-apply'] );

                            update_post_meta( $post_id, 'application-method', $applicationMethod );
                            update_post_meta( $post_id, '3rd-party-application-link', $applicationLink );
                            update_post_meta( $post_id, 'how-to-apply', $howToApply );


                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Job Type -->
                        <div class="form-item">
                            <select name="application-method" required>
                                <option value="manual" <?php echo ($applicationMethod == 'manual')? "selected" : "" ?>>
                                    Manual Application
                                </option>
                                <option value="3rd-party" <?php echo ($applicationMethod == '3rd-party')? "selected" : "" ?>>
                                    3rd Party Site
                                </option>
                                <option value="saedconnect" <?php echo ($applicationMethod == 'saedconnect')? "selected" : "" ?>>
                                    Recieve Applications through SAEDConnect
                                </option>
                            </select>
                        </div>
                        
                        <!-- Manual Application -->
                        <div class="conditional" data-condition="['manual'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Enter detailed instructions on how applicants can submit their application for this job.
                            </p>
                            <div class="form-item">
                                <textarea class="editor" name="how-to-apply" id="" cols="30" rows="8"><?php echo $howToApply ?></textarea>
                            </div>
                        </div>
                        <!-- 3rd Party -->
                        <div class="conditional" data-condition="['3rd-party'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Enter the external application link for this job.
                            </p>
                            <div class="form-item">
                                <input 
                                    type="url" 
                                    name="3rd-party-application-link" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $applicationLink ?>"
                                >
                            </div>
                        </div>
                        <!-- SAEDConnect -->
                        <div class="conditional" data-condition="['saedconnect'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Letting SAEDconnect handle the application process gives you access to the Application Manager. The Application gives you access to a robust set of tools, crafted to give you control and allow you seamlessly manage job applications.
                            </p>
                            <p class="txt-normal-s txt-medium margin-b-20">
                                 Selecting this option requires paying a fee.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Application Requirements -->
<div class="modal fade font-main filter-modal" id="applicationReqirementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-job-requirements'; ?>" method="post">
                <?php
                    /* Add/Edit Job (Specification) */

                    /* Get Post data */
                    $jobSummary = get_post_meta( $post->ID, 'job-summary', true );
                    $jobDescription = get_post_meta( $post->ID, 'job-description', true );
                    $paymentType = get_post_meta( $post->ID, 'payment-type', true );
                    $offerAmount = get_post_meta( $post->ID, 'offer-amount', true );
                    $offerAmountConfidential = get_post_meta( $post->ID, 'offer-amount-confidential', true );
                    $commissionAmount = get_post_meta( $post->ID, 'commission-amount', true );
                    $commissionAmountConfidential = get_post_meta( $post->ID, 'commission-amount-confidential', true );

                    /*
                    *
                    * Save / Retrieve Form Data
                    *
                    */
                    if($_POST && $rendered_view == 'form-job-requirements'){

                        /* Get Post Name */
                        print_r($_POST);
                        //$serialized_data = maybe_serialize($_POST);

                        /* Update Meta */
                        //update_post_meta( $post_id, 'assigned-tasks', $serialized_data );

                        /* Redirect */
                        //printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                    }
                ?>

                <?php 
                    $unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'assigned-tasks', true ) );

                    /* Return Terms assigned to Post */
                    $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                    /*
                    *
                    * Populate Form Data from Terms
                    *
                    */
                ?>
                <div class="modal-header padding-lr-40">
                    <h5 class="modal-title" id="exampleModalLabel">Select Application Requirements</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-40">
                    <div class="accordion" id="jobRequiremenetAccordion">
                        <!-- Work Profile -->
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementWork" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Work profile
                                </a>
                            </div>
                            <div id="applicationReqirementWork" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-1" 
                                                name="job-requirements[]" 
                                                value="personal-profile"
                                            >
                                            <label 
                                                for="job-requirements-1" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Personal Information
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-1" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-1" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-2" 
                                                name="job-requirements[]" 
                                                value="cover-letter"
                                            >
                                            <label 
                                                for="job-requirements-2" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Cover Letter 
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-2" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-2" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-3" 
                                                name="job-requirements[]" 
                                                value="video-cv"
                                            >
                                            <label 
                                                for="job-requirements-3" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Video CV
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-3" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-3" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-4" 
                                                name="job-requirements[]" 
                                                value="education"
                                            >
                                            <label 
                                                for="job-requirements-4" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Education
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-4" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-4" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-5" 
                                                name="job-requirements[]" 
                                                value="experience"
                                            >
                                            <label 
                                                for="job-requirements-5" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Experience
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-5" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-5" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-6" 
                                                name="job-requirements[]" 
                                                value="skills"
                                            >
                                            <label 
                                                for="job-requirements-6" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Skills
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-6" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-6" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-7" 
                                                name="job-requirements[]" 
                                                value="capabilities"
                                            >
                                            <label 
                                                for="job-requirements-7" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Capabilities
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-7" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-7" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Mentor Profile -->
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementWork" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Mentor profile
                                </a>
                            </div>
                            <div id="applicationReqirementWork" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-1" 
                                                name="job-requirements[]" 
                                                value="personal-profile"
                                            >
                                            <label 
                                                for="job-requirements-1" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Personal Information
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-1" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-1" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-2" 
                                                name="job-requirements[]" 
                                                value="cover-letter"
                                            >
                                            <label 
                                                for="job-requirements-2" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Cover Letter 
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-2" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-2" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-3" 
                                                name="job-requirements[]" 
                                                value="video-cv"
                                            >
                                            <label 
                                                for="job-requirements-3" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Video CV
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-3" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-3" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-4" 
                                                name="job-requirements[]" 
                                                value="education"
                                            >
                                            <label 
                                                for="job-requirements-4" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Education
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-4" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-4" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-5" 
                                                name="job-requirements[]" 
                                                value="experience"
                                            >
                                            <label 
                                                for="job-requirements-5" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Experience
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-5" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-5" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-6" 
                                                name="job-requirements[]" 
                                                value="skills"
                                            >
                                            <label 
                                                for="job-requirements-6" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Skills
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-6" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-6" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-7" 
                                                name="job-requirements[]" 
                                                value="capabilities"
                                            >
                                            <label 
                                                for="job-requirements-7" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Capabilities
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-7" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-7" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Business -->
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementBusiness" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Business
                                </a>
                            </div>
                            <div id="applicationReqirementBusiness" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-8" 
                                                name="job-requirements[]" 
                                                value="business"
                                            >
                                            <label 
                                                for="job-requirements-7" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Must have a Business
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-8" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-8" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Project -->
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementProject" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Project
                                </a>
                            </div>
                            <div id="applicationReqirementProject" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-9" 
                                                name="job-requirements[]" 
                                                value="capabilities"
                                            >
                                            <label 
                                                for="job-requirements-9" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Must have created a Project
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-9" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-9" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-40">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<?php } ?>

<!-- How to Apply -->
<div class="modal fade font-main filter-modal" id="howToApplyModalx" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header padding-lr-30">
                <h5 class="modal-title" id="exampleModalLabel">How to Apply</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body padding-o-30">
                <?php $howToApply = get_post_meta( $post_id, 'how-to-apply', true ); ?>
                <article class="text-box txt-normal-s txt-height-1-5 margin-b-20">
                    <?php echo $howToApply ?>
                </article>
            </div>
            <div class="modal-footer padding-lr-30">
                <button type="button" class="btn btn-blue txt-xs" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade font-main filter-modal" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="AddPaymentoutcomeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php 
                if($_POST && $rendered_view == 'form-subscribe'){
                    $fullName = sanitize_text_field( $_POST['full-name'] );
                    $email = sanitize_text_field( $_POST['email'] );
                    $phone = sanitize_text_field( $_POST['phone'] );
                    $state = sanitize_text_field( $_POST['state'] );
                    $jobFunction ="";
                    $capabilities ="";
                    
                    /* Create application status*/
                    $application_db->insert( 
                        $table, 
                        array( 
                            "post_id" => $post_id,
                            "post_type" => $post_type,
                            "user_id" => $current_user->ID,
                            "status" => "external",
                            "name" => $fullName,
                            "email" => $email,
                            "location" => $state,
                            "phone" => $phone,
                            "job_function" => $jobFunction,
                            "job_capabilities" => $capabilities,

                        ), 
                        array( "%d", "%s", "%d", "%s", "%s", "%s", "%s", "%s", "%s", "%s" ) 
                    );
                    
                    /* Redirect */
                    $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                    $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                    
                    if( $applicationMethod == 'manual' ){
                        printf('<script>window.location.replace("%s")</script>', currentUrl(true).'?view=how-to-apply' );
                    } else {
                        printf('<script>window.location.replace("%s")</script>', $applicationLink);
                    }
                }
            ?>
            
            <form class="signup-form" method="post" action="<?php echo currentUrl(true).'?view=form-subscribe'; ?>">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Apply for <?php echo $post_title; ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <div class="form margin-b-40">
                        <!-- Name -->
                        <div class="form-item">
                            <label for="name">
                                Name
                            </label>
                            <input type="text" name="full-name" id="full-name" required>
                        </div>
                        <!-- Email -->
                        <div class="form-item">
                            <label for="email">
                                Email
                            </label>
                            <input type="email" name="email" id="email" value="<?php echo $current_user->user_email ?>" required>
                        </div>
                        <!-- Phone -->
                        <div class="form-item">
                            <label for="phone">
                                Phone
                            </label>
                            <input type="telephone" name="phone" id="phone" required>
                        </div>
                        <!-- State -->
                        <div class="form-item">
                            <label for="state">
                                State of Residence
                            </label>
                            <select name="state" id="state" required>
                                <option value="" selected disabled hidden>- Select -</option>
                                <option value="Abuja FCT">Abuja FCT</option>
                                <option value="Abia">Abia</option>
                                <option value="Adamawa">Adamawa</option>
                                <option value="Akwa Ibom">Akwa Ibom</option>
                                <option value="Anambra">Anambra</option>
                                <option value="Bauchi">Bauchi</option>
                                <option value="Bayelsa">Bayelsa</option>
                                <option value="Benue">Benue</option>
                                <option value="Borno">Borno</option>
                                <option value="Cross River">Cross River</option>
                                <option value="Delta">Delta</option>
                                <option value="Ebonyi">Ebonyi</option>
                                <option value="Edo">Edo</option>
                                <option value="Ekiti">Ekiti</option>
                                <option value="Enugu">Enugu</option>
                                <option value="Gombe">Gombe</option>
                                <option value="Imo">Imo</option>
                                <option value="Jigawa">Jigawa</option>
                                <option value="Kaduna">Kaduna</option>
                                <option value="Kano">Kano</option>
                                <option value="Katsina">Katsina</option>
                                <option value="Kebbi">Kebbi</option>
                                <option value="Kogi">Kogi</option>
                                <option value="Kwara">Kwara</option>
                                <option value="Lagos">Lagos</option>
                                <option value="Nassarawa">Nassarawa</option>
                                <option value="Niger">Niger</option>
                                <option value="Ogun">Ogun</option>
                                <option value="Ondo">Ondo</option>
                                <option value="Osun">Osun</option>
                                <option value="Oyo">Oyo</option>
                                <option value="Plateau">Plateau</option>
                                <option value="Rivers">Rivers</option>
                                <option value="Sokoto">Sokoto</option>
                                <option value="Taraba">Taraba</option>
                                <option value="Yobe">Yobe</option>
                                <option value="Zamfara">Zamfara</option>
                                <option value="Outside Nigeria">Outside Nigeria</option>
                            </select>
                        </div>
                        <!-- Program Name -->
                        <input type="text" name="post-name" id="post-name" value="<?php echo $post_title; ?>" hidden>

                        <div class="status"></div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" id="submit-btn" class="submit-btn btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
            <script>
                $(document).ready(function() {

                });
            </script>
        </div>
    </div>
</div>
