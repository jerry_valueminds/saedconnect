<?php if( $current_user->ID != $post_author_id && $rendered_view != 'application-manager' && $rendered_view == '' ){ ?>
   
<section class="padding-t-20 padding-b-80">
    <div class="container-wrapper">
        <div class="row row-20">
            <div class="col-md-8 padding-lr-20">
                <h1 class="bg-ash txt-medium txt-color-white padding-o-15 ">
                    Summary
                </h1>
                <article class="txt-sm margin-b-40">
                    <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                        <span class="col-2 txt-medium txt-dark">
                            Location:
                        </span>
                        <span class="padding-l-10  col txt-color-lighter">
                            <!-- Locations -->
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'nigerian-state', array("fields" => "names"));

                                if( $term_list ){
                            
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } 
                            ?>
                        </span>
                    </p>
                    <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                        <span class="col-2 txt-medium txt-dark">
                            Job Type:
                        </span>
                        <span class="padding-l-10  col txt-color-lighter">
                            <?php 
                                $getJobType = get_post_meta( $post_id, 'job-type', true );
    
                                if ($getJobType == 'fullTime'){
                                    echo "Full-time";
                                } elseif ($getJobType == 'partTime'){
                                    echo "Part-time";
                                } elseif ($getJobType == 'freelancer'){
                                    echo "Freelancer/Independent Contractore";
                                }else{
                                    echo "--";
                                }
                            ?>
                        </span>
                    </p>
                    <?php 
                        $getJobType = get_post_meta( $post_id, 'job-level', true );
                        if($getJobType){
                    ?>
                        <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                            <span class="col-2 txt-medium txt-dark">
                                Job Level:
                            </span>
                            <span class="padding-l-10  col txt-color-lighter">
                                <?php 

                                    if ($getJobType == 'internship'){
                                        echo "Internship";
                                    } elseif ($getJobType == 'entry'){
                                        echo "Entry Level";
                                    } elseif ($getJobType == 'officer'){
                                        echo "Officer Level";                    
                                    } elseif ($getJobType == 'mid'){
                                        echo "Mid Level";
                                    } elseif ($getJobType == 'management'){
                                        echo "Management Level";
                                    }else{
                                        echo "--";
                                    }
                                ?>
                            </span>
                        </p>
                    <?php } ?>
                    <?php 
                        $getWorkStyle = get_post_meta( $post_id, 'physical-presence', true );
                        if($getWorkStyle){
                    ?>
                        <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                            <span class="col-2 txt-medium txt-dark">
                                Work Style:
                            </span>
                            <span class="padding-l-10  col txt-color-lighter">
                                <?php 

                                    if ($getWorkStyle == 'yes'){
                                        echo "Candidate's physical presence is required";
                                    } elseif ($getWorkStyle == 'no'){
                                        echo "Candidate can work from anywhere";
                                    } elseif ($getWorkStyle == 'flexible'){
                                        echo "Flexible";                    
                                    }else{
                                        echo "--";
                                    }
                                ?>
                            </span>
                        </p>
                    <?php } ?>
                    <?php 
                        $deadline = get_post_meta( $post_id, 'deadline', true );
                        if($deadline){
                    ?>
                        <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                            <span class="col-2 txt-medium txt-dark">
                                Application Deadline:
                            </span>
                            <span class="padding-l-10  col txt-color-lighter">
                                <?php 
                                    $date = strtotime( $deadline );
                                    echo date('j F Y', $date);
                                ?>
                            </span>
                        </p>
                    <?php } ?>   
                </article>

                <style>
                    .application-method-content{
                        display: none;

                    }

                    .application-method-content.show{
                        display: block;
                    }
                </style>
                <div class="margin-b-40">
                    <h1 class="bg-ash txt-medium txt-color-white padding-o-15">
                        Job Description
                    </h1>
                    <article class="text-box sm padding-o-15 border-o-1 border-color-darkgrey">
                        <?php echo get_post_meta( $post_id, 'job-description', true ) ?>
                    </article>
                </div>
                <?php if( is_user_logged_in() ){ ?>
                <div class="text-center">
                    <?php if( $current_user->ID == $post_author_id ){ ?>
                        <!-- Publish -->
                        <a href="<?php echo $post_link.'?view=application-manager'; ?>" class="btn btn-blue txt-sm no-m-b">
                            Publish
                        </a>
                    <?php } else { ?>
                        <?php 

                        ?>
                        <?php if( $rendered_view == 'apply' ){ ?>
                            <!-- Job Details -->
                            <a href="<?php echo $post_link; ?>" class="btn btn-blue txt-normal-s no-m-b w-100">
                                Back to Details
                            </a>
                        <?php } else { ?>
                            <!-- Apply -->
                            <?php  
                                $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                                $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                                $howToApply = get_post_meta( $post_id, 'how-to-apply', true );
                            ?>

                            <?php if( $applicationMethod == 'manual' ){ ?>
                                <a class="btn btn-blue txt-normal-s no-m-b w-100" data-toggle="modal" href="#howToApplyModal">
                                    Apply
                                </a>
                            <?php } elseif( $applicationMethod == '3rd-party' ) { ?>
                                <a href="<?php echo $applicationLink ?>" class="btn btn-blue txt-normal-s no-m-b w-100" target="_blank">
                                    Apply
                                </a>
                            <?php } elseif( $applicationMethod == 'saedconnect' ) { ?>
                                <a href="<?php echo $post_link.'?view=apply'; ?>" class="btn btn-blue txt-normal-s no-m-b w-100">
                                    Apply
                                </a>            
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-4 padding-lr-20">
            <?php if( !is_user_logged_in() ){ ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Login to Apply Now
                    </h3>
                    
                    <!-- Login Form -->
                    <style>
                        .gfield_checkbox label, .login-form .gfield_label{
                            display: block !important;
                        }

                        .gfield_checkbox li{
                            display: flex;
                            align-items: center;
                        }

                        .gfield_checkbox input{
                            margin: 0 !important;
                            margin-right: 10px !important;
                        }
                    </style>

                    <?php switch_to_blog(1); ?>

                        <!-- Login Tab -->
                        <div class="login">
                            <div class="login-form">
                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                                <?php endif;?>
                            </div>
                        </div>

                    <?php restore_current_blog(); ?>
 
                    <p class="txt-sm txt-medium padding-t-10">
                        Dont have an account?
                        <a href="https://www.saedconnect.org/register" class=" txt-color-blue">
                            Sign up
                        </a>
                    </p>
                </div>
                <?php } ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Share Job Post
                    </h3>
                    <?php echo do_shortcode('[Sassy_Social_Share]') ?>

                    <p class="txt-sm">
                        <a href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-facebook fa-stack-1x fa-inverse txt-color-white"></i>
                            </span>
                        </a>
                        <a href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-twitter fa-stack-1x fa-inverse txt-color-white"></i>
                            </span>
                        </a>
                        <a href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-instagram fa-stack-1x fa-inverse txt-color-white"></i>
                            </span>
                        </a>
                        <a href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-linkedin fa-stack-1x fa-inverse txt-color-white"></i>
                            </span>
                        </a>
                    </p>
                </div>
                <p class="txt-sm txt-medium padding-t-10">
                    <a href="" class=" txt-color-blue">
                        Report Job
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

<?php } elseif( $current_user->ID == $post_author_id && $rendered_view != 'application-manager' ) { ?>

<section class="padding-t-20 padding-b-80">
    <div class="container-wrapper">
        <div class="row row-20">
            <div class="col-md-8 padding-lr-20">
                <h1 class="bg-ash txt-medium txt-color-white padding-o-15 ">
                    Summary
                </h1>
                <article class="txt-sm margin-b-40">
                    <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                        <span class="col-2 txt-medium txt-dark">
                            Location:
                        </span>
                        <span class="padding-l-10  col txt-color-lighter">
                            <!-- Locations -->
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'nigerian-state', array("fields" => "names"));

                                if( $term_list ){
                            
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } 
                            ?>
                        </span>
                    </p>
                    <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                        <span class="col-2 txt-medium txt-dark">
                            Job Type:
                        </span>
                        <span class="padding-l-10  col txt-color-lighter">
                            <?php 
                                $getJobType = get_post_meta( $post_id, 'job-type', true );
    
                                if ($getJobType == 'fullTime'){
                                    echo "Full-time";
                                } elseif ($getJobType == 'partTime'){
                                    echo "Part-time";
                                } elseif ($getJobType == 'freelancer'){
                                    echo "Freelancer/Independent Contractore";
                                }else{
                                    echo "--";
                                }
                            ?>
                        </span>
                    </p>
                    <?php 
                        $getJobLevel = get_post_meta( $post_id, 'job-level', true );
                        if($getJobLevel){
                    ?>
                        <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                            <span class="col-2 txt-medium txt-dark">
                                Job Level:
                            </span>
                            <span class="padding-l-10  col txt-color-lighter">
                                <?php 

                                    if ($getJobLevel == 'internship'){
                                        echo "Internship";
                                    } elseif ($getJobLevel == 'entry'){
                                        echo "Entry Level";
                                    } elseif ($getJobLevel == 'officer'){
                                        echo "Officer Level";                    
                                    } elseif ($getJobLevel == 'mid'){
                                        echo "Mid Level";
                                    } elseif ($getJobLevel == 'management'){
                                        echo "Management Level";
                                    }else{
                                        echo "--";
                                    }
                                ?>
                            </span>
                        </p>
                    <?php } ?>
                    <?php 
                        $getWorkStyle = get_post_meta( $post_id, 'physical-presence', true );
                        if($getWorkStyle){
                    ?>
                        <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                            <span class="col-2 txt-medium txt-dark">
                                Work Style:
                            </span>
                            <span class="padding-l-10  col txt-color-lighter">
                                <?php 

                                    if ($getWorkStyle == 'yes'){
                                        echo "Candidate's physical presence is required";
                                    } elseif ($getWorkStyle == 'no'){
                                        echo "Candidate can work from anywhere";
                                    } elseif ($getWorkStyle == 'flexible'){
                                        echo "Flexible";                    
                                    }else{
                                        echo "--";
                                    }
                                ?>
                            </span>
                        </p>
                    <?php } ?>
                    <?php 
                        $deadline = get_post_meta( $post_id, 'deadline', true );
                        if($deadline){
                    ?>
                        <p class="d-flex border-b-1 border-color-darkgrey padding-tb-10">
                            <span class="col-2 txt-medium txt-dark">
                                Application Deadline:
                            </span>
                            <span class="padding-l-10  col txt-color-lighter">
                                <?php 
                                    $date = strtotime( $deadline );
                                    echo date('j F Y', $date);
                                ?>
                            </span>
                        </p>
                    <?php } ?>
                </article>
                <style>
                    .application-method-content{
                        display: none;

                    }

                    .application-method-content.show{
                        display: block;
                    }
                </style>
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            Job Description
                        </span>
                        <a class="btn btn-white txt-xs no-m-b" data-toggle="modal" href="#descriptionModal">
                            Add / Edit
                        </a>
                    </h1>
                    <article class="text-box sm padding-o-15 border-o-1 border-color-darkgrey">
                        <?php echo get_post_meta( $post_id, 'job-description', true ) ?>
                    </article>
                </div>
                <div class="margin-b-40">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            + Specific tasks you want candidate to demonstrate experience for
                        </span>
                        <a class="btn btn-white txt-xs no-m-b" data-toggle="modal" href="#taskModal">
                            Add
                        </a>
                    </h1>
                    <article class="txt-sm">
                    <?php
                        $unserialized_data = maybe_unserialize( get_post_meta( $post_id, 'assigned-tasks', true ) );
                        $tasks_array = $unserialized_data['job-tasks'];
        
                        foreach( $tasks_array as $key => $value ){
                    ?>
                        <div class="border-o-1 border-color-darkgrey padding-o-15">
                            <p>
                                <a
                                    class="row"
                                    data-toggle="collapse" 
                                    href="#collapseTaskDetailsContent-<?php echo $key ?>" aria-expanded="false"   
                                >
                                    <span class="col-1">
                                        <span class="task-number">
                                            <?php echo $key + 1 ?>
                                        </span>
                                    </span>
                                    <span class="col-8">
                                        <?php 
                                            $task = get_post($value);
                                            echo $task->post_title;
                                        ?>
                                    </span>
                                    <span class="col-3 text-right txt-color-blue">
                                        View Details 
                                        <i class="fa fa-angle-down padding-l-5"></i>
                                    </span>
                                </a>
                            </p>
                            <div id="collapseTaskDetailsContent-<?php echo $key ?>" class="collapse" aria-labelledby="headingOne">
                                <div class="row">
                                    <article class="col-11 ml-auto text-box padding-t-10">
                                        <p>
                                            Each applicant must desmonstrate competence in developing a WordPress site. 
                                        </p>
                                    </article>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </article>                            
                </div>
                <div class="margin-b-20">
                    <a 
                       class="dropdown-toggle txt-medium txt-color-dark d-flex justify-content-between align-items-center border-o-2 padding-o-15"
                       data-toggle="modal" href="#applicationMethodModal"
                    >
                            How do you want to recieve Applications?
                    </a>
                </div>
                <?php  
                    $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                    $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                    $howToApply = get_post_meta( $post_id, 'how-to-apply', true );
                ?>
                
                <?php if( $applicationMethod == 'manual' ){ ?>
                <div>
                    <h3 class="txt-bold txt-height-1-4 margin-b-10">
                        Manual Application Instructions
                    </h3>
                    <article class="text-box txt-normal-s txt-height-1-5 margin-b-20">
                        <?php echo $howToApply ?>
                    </article>
                </div>
                <?php } elseif( $applicationMethod == '3rd-party' ) { ?>
                    <div>
                        <h3 class="txt-bold txt-height-1-4 margin-b-10">
                            3rd Party Site
                        </h3>
                        <p class="txt-sm margin-b-10">
                            When applicants click the Apply Button on this job, they will be redirected to this site:
                        </p>
                        <p class="txt-sm margin-b-20">
                            <a href="<?php echo $applicationLink ?>" class="txt-bold txt-color-blue" target="_blank"><?php echo $applicationLink ?></a>
                        </p>
                    </div>
                <?php } elseif( $applicationMethod == 'saedconnect' ) { ?>
                    <div>
                        <h3 class="txt-bold txt-height-1-4 margin-b-10">
                            Recieve Applications through SAEDConnect
                        </h3>
                        <p class="txt-sm margin-b-20">
                            Letting SAEDconnect handle the application process gives you access to the Application Manager. The Application gives you access to a robust set of tools, crafted to give you control and allow you seamlessly manage job applications.
                        </p>
                        <p class="txt-sm margin-b-20">
                            Selecting this option requires paying a fee.
                        </p>
                        <p>
                            <a href="" class="btn btn-blue txt-sm">
                                Subscribe Now
                            </a>
                        </p>
                    </div>             
                <?php } else { ?>
                    <div>
                        <h3 class="txt-bold txt-height-1-4 margin-b-10">
                            You have not selected an Application for this job. You must select an Application Method before you can publish this job.
                        </h3>
                    </div>
                <?php } ?>
                
                <!-- Application Requirements -->
                <div class="margin-b-20">
                    <a 
                       class="dropdown-toggle txt-medium txt-color-dark d-flex justify-content-between align-items-center border-o-2 padding-o-15"
                       data-toggle="modal" href="#applicationReqirementModal"
                    >
                        Select Application Requuirements
                    </a>
                </div>
            </div>
            <div class="col-md-4 padding-lr-20">
            <?php if( !is_user_logged_in() ){ ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Login to Apply Now
                    </h3>
                    
                    <!-- Login Form -->
                    <style>
                        .gfield_checkbox label, .login-form .gfield_label{
                            display: block !important;
                        }

                        .gfield_checkbox li{
                            display: flex;
                            align-items: center;
                        }

                        .gfield_checkbox input{
                            margin: 0 !important;
                            margin-right: 10px !important;
                        }
                    </style>

                    <?php switch_to_blog(1); ?>

                        <!-- Login Tab -->
                        <div class="login">
                            <div class="login-form">
                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                                <?php endif;?>
                            </div>
                        </div>

                    <?php restore_current_blog(); ?>
 
                    <p class="txt-sm txt-medium padding-t-10">
                        Dont have an account?
                        <a href="https://www.saedconnect.org/register" class=" txt-color-blue">
                            Sign up
                        </a>
                    </p>
                </div>
                <?php } ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Share Job Post
                    </h3>
                    <?php echo do_shortcode('[Sassy_Social_Share]') ?>
                </div>
                <p class="txt-sm txt-medium padding-t-10">
                    <a href="" class=" txt-color-blue">
                        Report Job
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

<?php } ?>