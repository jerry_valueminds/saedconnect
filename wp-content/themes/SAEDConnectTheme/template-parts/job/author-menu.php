<?php if( $current_user->ID == $post_author_id ){ ?>

<style>
    .menu{
    }

    .menu li{
        display: inline;

    }

    .menu li a{
        display: inline-block;
        font-size: 0.9em;
        padding: 20px 20px;
        color: white;
    }

    .menu li a.active{
        background-color: white;
        color: black;
    }
</style>

<div class="container-wrapper bg-ash">
    <ul class="menu">
        <li>
            <a href="<?php echo $post_link; ?>" class="<?php echo ( $rendered_view == "" ) ? "active" : "" ?>">
                Job Details
            </a>
        </li>
        <li>
            <a href="<?php echo $post_link.'/?view=application-manager'; ?>" class="<?php echo ( $rendered_view == "" ) ? "" : "active" ?>">
                Application Manager
            </a>
        </li>
    </ul>
</div>

<?php } ?>