<?php $isOpen = false; ?>
<!-- Contact Details -->
<?php include( locate_template( 'template-parts/competency/partials/contact-details.php', false, false ) ); ?>
<!-- Education & Work experience snapshot -->
<?php include( locate_template( 'template-parts/competency/partials/work-profile-snapshot.php', false, false ) ); ?>
<!-- Education & Work experience snapshot -->
<?php include( locate_template( 'template-parts/competency/partials/education-and-training.php', false, false ) ); ?>
<!-- Education & Training -->
<?php include( locate_template( 'template-parts/competency/partials/work-experience.php', false, false ) ); ?>
<!-- Languages -->
<?php include( locate_template( 'template-parts/competency/partials/skills-and-capabilities.php', false, false ) ); ?>
<!-- Languages -->
<?php include( locate_template( 'template-parts/competency/partials/languages.php', false, false ) ); ?>
<!-- Interests -->
<?php include( locate_template( 'template-parts/competency/partials/interests.php', false, false ) ); ?>
<!-- Certifications -->
<?php include( locate_template( 'template-parts/competency/partials/certifications.php', false, false ) ); ?>
<!-- Affiliations -->
<?php include( locate_template( 'template-parts/competency/partials/affiliations.php', false, false ) ); ?>