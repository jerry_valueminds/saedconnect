<?php $current_user = wp_get_current_user(); ?>
<div class="bg-white txt-color-light padding-tb-30 padding-lr-40 margin-b-10">
    <?php
        $profile_query = new WP_Query();
        $profile_query->query( 
            array(
                'post_type' => 'social-profile',
                'post_status' => 'publish',
                'author' => $current_user->ID,
                'posts_per_page' => 1,
            ) 
        );
    
        if( $profile_query->found_posts > 0 )
            $check[] = 'complete';
        else
            $check[] = 'incomplete';
            
    ?>
    <div class="profile-collapse padding">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-2" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Your contact details</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="faq-2">
            <article class="padding-t-30">
                <?php if ( $profile_query->have_posts() ) { ?>

                    <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                        <?php $post_id = $post->ID; ?> 

                        <!-- Entry -->
                        <div class="txt-sm padding-tb-15 padding-lr-20 border margin-b-30">
                            <p class="txt-medium padding-b-5">Residential address</p>
                            <p><?php echo get_post_meta( $post_id, 'address', true ); ?></p>
                        </div>
                        <div class="row row-10 padding-b-20 txt-color-light">
                            <div class="col-md-6 col-xl-3 padding-lr-10 padding-b-10">
                                <p class="txt-sm txt-medium">
                                    <i class="fa fa-phone"></i>
                                    <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'phone', true ); ?></span>
                                </p>
                            </div>
                            <div class="col-md-6 col-xl-3 padding-lr-10 padding-b-10">
                                <p class="txt-sm txt-medium">
                                    <i class="fa fa-whatsapp"></i>
                                    <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'whatsapp', true ); ?></span>
                                </p>
                            </div>
                            <div class="col-md-6 col-xl-3 padding-lr-10 padding-b-10">
                                <p class="txt-sm txt-medium">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'email', true ); ?></span>
                                </p>
                            </div>
                            <div class="col-md-6 col-xl-3 padding-lr-10 padding-b-10">
                                <p class="txt-sm txt-medium">
                                    <i class="fa fa-globe"></i>
                                    <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'website', true ); ?></span>
                                </p>
                            </div>
                            <div class="col-md-6 col-xl-3 padding-lr-10 padding-b-10">
                                <p class="txt-sm txt-medium">
                                    <i class="fa fa-facebook"></i>
                                    <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'facebook', true ); ?></span>
                                </p>
                            </div>
                            <div class="col-md-6 col-xl-3 padding-lr-10 padding-b-10">
                                <p class="txt-sm txt-medium">
                                    <i class="fa fa-twitter"></i>
                                    <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'twitter', true ); ?></span>
                                </p>
                            </div>
                            <div class="col-md-6 col-xl-3 padding-lr-10 padding-b-10">
                                <p class="txt-sm txt-medium">
                                    <i class="fa fa-instagram"></i>
                                    <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'instagram', true ); ?></span>
                                </p>
                            </div>
                            <div class="col-md-6 col-xl-3 padding-lr-10 padding-b-10">
                                <p class="txt-sm txt-medium">
                                    <i class="fa fa-linkedin"></i>
                                    <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'linkedin', true ); ?></span>
                                </p>
                            </div>
                        </div>

                    <?php endwhile; ?> 

                <?php }else{ ?> 
                   
                    <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                        You need to add your contact information to continue. Adding your contact information would enable us reach you with next steps. Click the add button to do so.
                    </p>
                    
                <?php } ?>
            </article>
            <!--CTA-->
            <article class="padding-t-10">
                <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddSocialDetailsModal">
                    <?php echo ( $profile_query->found_posts > 0 ) ? 'Edit' : 'Add'; ?>
                </a>
            </article>
        </div>
    </div>
    <!-- Add Modal -->
    <div class="modal fade font-main filter-modal" id="myCVAddSocialDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(false).'&form=cv-social-profile'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">My Social Profile</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            $post_id = 0;

                            $post_type = 'social-profile';
                            $profile_query = new WP_Query();
                            $profile_query->query( 
                                array(
                                    'post_type' => $post_type,
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => 1,
                                ) 
                            );

                            if ( $profile_query->have_posts() ) {

                                while ($profile_query->have_posts()) : $profile_query->the_post();

                                /* Variables */
                                $post_id = $post->ID;   //Get Program ID

                                endwhile;

                            }

                            if( $_POST && $_GET['form'] == 'cv-social-profile' ){

                                /* Get Post Name */
                                $postName = 'social-Profile-'.$current_user->ID;

                                /* Meta */
                                $email = sanitize_text_field( $_POST['email'] ); 
                                $phone = sanitize_text_field( $_POST['phone'] ); 
                                $whatsapp = sanitize_text_field( $_POST['whatsapp'] ); 
                                $website = sanitize_text_field( $_POST['website'] ); 
                                $facebook = sanitize_text_field( $_POST['facebook'] ); 
                                $twitter = sanitize_text_field( $_POST['twitter'] ); 
                                $instagram = sanitize_text_field( $_POST['instagram'] ); 
                                $linkedin = sanitize_text_field( $_POST['linkedin'] ); 
                                $address= sanitize_text_field( $_POST['address'] ); 

                                /* Save Post to DB */
                                $post_id = wp_insert_post(array (
                                    'ID' => $post_id,
                                    'post_type' => $post_type,
                                    'post_title' => $postName,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                update_post_meta( $post_id, 'email', $email );
                                update_post_meta( $post_id, 'phone', $phone );
                                update_post_meta( $post_id, 'whatsapp', $whatsapp );
                                update_post_meta( $post_id, 'website', $website );
                                update_post_meta( $post_id, 'facebook', $facebook );
                                update_post_meta( $post_id, 'twitter', $twitter );
                                update_post_meta( $post_id, 'instagram', $instagram );
                                update_post_meta( $post_id, 'linkedin', $linkedin );
                                update_post_meta( $post_id, 'address', $address );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                            }
                        ?>
                        <div class="form-item">
                            <label for="email">Your Email Address</label>
                            <input type="email" name="email" id="email" value="<?php echo get_post_meta( $post_id, 'email', true ); ?>">
                        </div>
                        <div class="form-item">
                            <label for="phone">Your Phone Number</label>
                            <input type="tel" name="phone" id="phone" value="<?php echo get_post_meta( $post_id, 'phone', true ); ?>">
                        </div>
                        <div class="form-item">
                            <label for="whatsapp">WhatsApp Number</label>
                            <input type="tel" name="whatsapp" id="phone" value="<?php echo get_post_meta( $post_id, 'whatsapp', true ); ?>">
                        </div>
                        <div class="form-item">
                            <label for="website">Personal Blog/Website</label>
                            <input type="url" name="website" id="website" value="<?php echo get_post_meta( $post_id, 'website', true ); ?>">
                        </div>
                        <div class="form-item">
                            <label for="facebook">Facebook</label>
                            <input type="url" name="facebook" id="facebook" value="<?php echo get_post_meta( $post_id, 'facebook', true ); ?>">
                        </div>
                        <div class="form-item">
                            <label for="twitter">Twitter</label>
                            <input type="url" name="twitter" id="twitter" value="<?php echo get_post_meta( $post_id, 'twitter', true ); ?>">
                        </div>
                        <div class="form-item">
                            <label for="instagram">Instagram</label>
                            <input type="url" name="instagram" id="instagram" value="<?php echo get_post_meta( $post_id, 'instagram', true ); ?>">
                        </div>
                        <div class="form-item">
                            <label for="linkedin">Linkedin</label>
                            <input type="url" name="linkedin" id="linkedin" value="<?php echo get_post_meta( $post_id, 'linkedin', true ); ?>">
                        </div>                                       
                        <div class="form-item">
                            <label for="addresss">
                                Your current Address
                            </label>
                            <textarea id="address" name="address"><?php echo get_post_meta( $post_id, 'address', true ); ?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>