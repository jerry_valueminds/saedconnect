<?php $current_user = wp_get_current_user(); ?>
<div class="bg-white txt-color-light padding-t-30 padding-b-30 padding-lr-40 margin-b-10">
    <?php
        //ssprint_r($_POST);
        $meta_key = 'user_avatar';
        $avatar_id = get_user_meta($current_user->ID, $meta_key, true);
        $avatar_display_url = get_user_meta($current_user->ID, 'user_avatar_url', true);


        // Check that the nonce is valid, and the user can edit this post.
        if ( 
            isset( $_POST['my_image_upload_nonce'] ) 
            && wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
        ) {
            // The nonce was valid and the user has the capabilities, it is safe to continue.

            // These files need to be included as dependencies when on the front end.
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            /* Delete prevoius image */
            if($avatar_id){
                wp_delete_attachment( $avatar_id );
            }
            
            // Let WordPress handle the upload.
            // Remember, 'my_image_upload' is the name of our file input in our form above.
            $attachment_id = media_handle_upload( 'my_image_upload', 0);

            if ( is_wp_error( $attachment_id ) ) {
                
                // There was an error uploading the image.
                echo 'An unknown Error occurred, Try again';
                
            } else {
                /* The image was uploaded successfully: Save Attachement ID as User Avatar Meta */
                $avatar_url = wp_get_attachment_url($attachment_id);
                
                update_user_meta( $current_user->ID, $meta_key, $attachment_id);
                update_user_meta( $current_user->ID, 'user_avatar_url', $avatar_url);
                            
                /* Redirect */
                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                
            }

        } else {

            // The security check failed, maybe show the user an error.
            //echo '<br><br>Security fail';
        }
    ?>
    
    <?php
        
        /* Get Avatar */
        $meta_key = 'user_avatar_url';
        $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

        if($get_avatar_url){
            $avatar_url = $get_avatar_url;
            $check[] = 'complete';
        }else{
            $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
            $check[] = 'incomplete';
        }
            
    ?>
    <div class="profile-collapse padding">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#profile-picture" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Upload your profile picture</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="profile-picture">
            <article class="padding-t-10">
                <p class="txt-sm padding-b-30">What is the name of your center, your training models, locations where you have centers and contact information?</p>
                <?php if ( $get_avatar_url ) { ?>

                    <div class="flex_1 row row-30 align-items-center">
                        <div class="col-auto padding-lr-30 padding-b-20">
                            <figure class="user-avatar" style="background-image: url('<?php echo $avatar_url ?>')"></figure>
                        </div>
                        <div class="col padding-lr-30 padding-b-20">
                            <form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
                                <div class="image-field-wrapper">
                                    <input 
                                        type="file" 
                                        name="my_image_upload" 
                                        id="my_image_upload"
                                        class="my_image_upload"
                                        multiple="false"
                                    />
                                    <span class="btn btn-trans-bw txt-xs my_image_upload_btn">
                                        Change
                                    </span>
                                </div>

                                <?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
                                
                                <input 
                                    id="submit_my_image_upload" 
                                    class="btn btn-blue txt-xs submit_my_image_upload"
                                    name="submit_my_image_upload" 
                                    type="submit" 
                                    value="Upload" 
                                />
                            </form>
                        </div>
                    </div>

                <?php }else{ ?> 
                   
                    <div class="flex_1 row row-30 align-items-center">
                        <div class="col-auto padding-lr-30">
                            <figure class="user-avatar" style="background-image: url('<?php echo $avatar_url ?>')"></figure>
                        </div>
                        <div class="col padding-lr-30 padding-b-20">
                            <form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
                                <div class="image-field-wrapper">
                                    <input 
                                        type="file" 
                                        name="my_image_upload" 
                                        id="my_image_upload"
                                        class="my_image_upload"
                                        multiple="false"
                                    />
                                    <span class="btn btn-trans-bw txt-xs my_image_upload_btn">
                                        Choose file
                                    </span>
                                </div>

                                <?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
                                
                                <input 
                                    id="submit_my_image_upload" 
                                    class="btn btn-blue txt-xs submit_my_image_upload"
                                    name="submit_my_image_upload" 
                                    type="submit" 
                                    value="Upload" 
                                />
                            </form>
                        </div>
                    </div>
                    
                <?php } ?>
            </article>
        </div>
    </div>
</div>


<style>
    .image-field-wrapper{
        display: inline-block;
        position: relative;
    }
    
    .image-field-wrapper .btn{
        position: relative;
        z-index: 0;
        cursor: pointer;
    }
    
    .image-field-wrapper .my_image_upload{
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        opacity: 0;
        cursor: pointer;
    }
    
    .submit_my_image_upload{
        position: relative;
        z-index: 10;
        display: none;
    }
</style>


<script>
    $(".my_image_upload").change(function() {
        var preview = $('.user-avatar');
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(preview).css('background-image', 'url(' + e.target.result + ')');
                $('.submit_my_image_upload').fadeIn();
            }

            reader.readAsDataURL(this.files[0]);
            
        }
    });
</script>