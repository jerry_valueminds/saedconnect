<?php $current_user = wp_get_current_user(); ?>
<div class="bg-white txt-color-light padding-tb-30 padding-lr-40 margin-b-10">
    <?php
        $profile_query = new WP_Query();
        $profile_query->query( 
            array(
                'post_type' => 'work-experience',
                'post_status' => 'publish',
                'author' => $current_user->ID,
                'posts_per_page' => -1,
            ) 
        );
    
        if( $profile_query->found_posts > 0 || get_user_meta($current_user->ID, 'cv_no_work_experience', true) )
            $check[] = 'complete';
        else
            $check[] = 'incomplete';
    ?>
    <div class="profile-collapse padding">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-experience-collapse" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Tell us about your previous experience</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="cv-experience-collapse">
            <article class="padding-t-30">
                <?php if ( $profile_query->have_posts() ) { ?>

                    <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                        <?php $post_id = $post->ID; ?> 

                        <!-- Entry -->
                        <div class="row row-10 padding-b-20">
                            <figure class="col-auto padding-lr-10">
                                <div class="initials bg txt-lg txt-bold txt-color-white" style="background-color:#A8A4A2; width:2.5em; height:2.5em;">
                                    <?php 
                                        $meta = get_post_meta( $post_id, 'company', true );
                                        echo $meta[0];
                                    ?>
                                </div>
                            </figure>
                            <div class="col padding-lr-10 txt-color-light">
                                <p class="txt-normal-s txt-medium">
                                    <?php echo get_post_meta( $post_id, 'company', true ); ?>,
                                    <?php echo get_post_meta( $post_id, 'location', true ); ?>
                                    <span class="padding-l-10">(
                                        <?php 
                                            $date = strtotime( get_post_meta( $post_id, 'start_date', true ) );
                                            echo date('M Y', $date);

                                            echo ' - ';

                                            $date = strtotime( get_post_meta( $post_id, 'end_date', true ) );
                                            echo date('M Y', $date);
                                        ?>
                                        )
                                    </span>
                                </p>
                                <p class="txt-sm txt-medium font-italic padding-b-10">
                                    <?php echo get_the_title() ?>
                                </p>
                                <article class="text-box txt-sm txt-height-1-4">
                                    <?php echo get_post_meta( $post_id, 'description', true ); ?>
                                </article>
                            </div>
                            <div class="col-auto padding-lr-10 text-right txt-sm">
                                <a class="txt-green" data-toggle="modal" href="#myCVEditExperienceModal-<?php echo $post_id ?>">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                <a href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-experience-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                    <i class="fa fa-trash"></i>
                                    Delete
                                </a>
                            </div>
                        </div>

                        <!-- Edit Modal -->
                        <div class="modal fade font-main filter-modal" id="myCVEditExperienceModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="<?php echo currentUrl(false).'&form=cv-edit-experience-'.$post_id; ?>" method="post">
                                        <div class="modal-header padding-lr-30">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Experience</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body padding-o-30 form">
                                            <?php

                                                if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-experience-'.$post_id ){
                                                    wp_delete_post($post_id); // Delete
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                }

                                                if( $_POST && $_GET['form'] == 'cv-edit-experience-'.$post_id ){

                                                    /* Get Post Data */
                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                    $company = sanitize_text_field( $_POST['company'] ); 
                                                    $location = sanitize_text_field( $_POST['location'] ); 
                                                    $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                                    $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                                    $description = sanitize_text_field( $_POST['description'] ); 

                                                    /* Save Post to DB */
                                                    $post_id = wp_insert_post(array (
                                                        'ID' => $post_id,
                                                        'post_type' => 'work-experience',
                                                        'post_title' => $post_name,
                                                        'post_content' => "",
                                                        'post_status' => 'publish',
                                                    ));

                                                    /* Add Post Meta */
                                                    update_post_meta( $post_id, 'company', $company );
                                                    update_post_meta( $post_id, 'location', $location );
                                                    update_post_meta( $post_id, 'start_date', $start_date );
                                                    update_post_meta( $post_id, 'end_date', $end_date );
                                                    update_post_meta( $post_id, 'description', $description );

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                }
                                            ?>
                                            <div class="form-item">
                                                <label for="deadline">
                                                    Position/Job Title
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="post_name" 
                                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="company">
                                                    Company
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="company" 
                                                    value="<?php echo get_post_meta( $post_id, 'company', true ); ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="location">
                                                    Location
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="location" 
                                                    value="<?php echo get_post_meta( $post_id, 'location', true ); ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="start_date">
                                                    Start Month/Year
                                                </label>
                                                <input 
                                                    type="month" 
                                                    name="start_date" 
                                                    value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="end_date">
                                                    End Month/Year
                                                </label>
                                                <input 
                                                    type="month" 
                                                    name="end_date" 
                                                    value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="description">
                                                    Describe what you did/Your Achievements
                                                </label>
                                                <textarea id="description" name="description" class="editor" ><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer padding-lr-30">
                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>

                <?php }else{ ?> 
                    
                    <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                        <?php if( get_user_meta($current_user->ID, 'cv_no_work_experience', true) ){ ?>
                            You don't have any work experience.
                        <?php }else{ ?> 
                            You need to add your formal work experience to proceed, but if you don't have any, you can signify by checking the box provided above.
                        <?php } ?>
                    </p> 
                    
                <?php } ?>
            </article>
            <?php if( true /*get_user_meta($current_user->ID, 'cv_no_work_experience', true)*/ ){ ?>
                <!--CTA-->
                <article class="padding-t-10">
                    <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddExperienceModal">Add</a>
                    <?php if( !get_user_meta($current_user->ID, 'cv_no_work_experience', true) && $profile_query->found_posts == 0 ){ ?>
                        <a href="<?php echo currentUrl(false).'&form=cv-no-work-experience'; ?>" class="btn btn-application-manager no-m-b txt-xs">
                            I don't have any work experience
                        </a>
                    <?php } ?>
                </article>
            <?php } ?>
        </div>
    </div>
    <!-- Add Modal -->
    <div class="modal fade font-main filter-modal" id="myCVAddExperienceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(false).'&form=cv-add-experience'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Add Work Experience</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            $post_id = 0;
                        
                            if( $_GET['form'] == 'cv-no-work-experience' ){
                                
                                $value = get_user_meta($current_user->ID, 'cv_no_work_experience', true);
                                
                                if( $value )
                                    $value = !$value;
                                else
                                    $value = 1;
                                
                                update_user_meta( $current_user->ID, 'cv_no_work_experience', $value );
                                
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                            }

                            if( $_POST && $_GET['form'] == 'cv-add-experience' ){

                                /* Get Post Data */
                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                $company = sanitize_text_field( $_POST['company'] ); 
                                $location = sanitize_text_field( $_POST['location'] ); 
                                $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                $description = sanitize_text_field( $_POST['description'] ); 

                                /* Save Post to DB */
                                $post_id = wp_insert_post(array (
                                    'ID' => $post_id,
                                    'post_type' => 'work-experience',
                                    'post_title' => $post_name,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                /* Add Post Meta */
                                update_post_meta( $post_id, 'company', $company );
                                update_post_meta( $post_id, 'location', $location );
                                update_post_meta( $post_id, 'start_date', $start_date );
                                update_post_meta( $post_id, 'end_date', $end_date );
                                update_post_meta( $post_id, 'description', $description );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                            }
                        ?>
                        <div class="form-item">
                            <label for="deadline">
                                Position/Job Title
                            </label>
                            <input 
                                type="text" 
                                name="post_name" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="company">
                                Company
                            </label>
                            <input 
                                type="text" 
                                name="company" 
                                value="<?php echo get_post_meta( $post_id, 'company', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="location">
                                Location
                            </label>
                            <input 
                                type="text" 
                                name="location" 
                                value="<?php echo get_post_meta( $post_id, 'location', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="start_date">
                                Start Month/Year
                            </label>
                            <input 
                                type="month" 
                                name="start_date" 
                                value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="end_date">
                                End Month/Year
                            </label>
                            <input 
                                type="month" 
                                name="end_date" 
                                value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="description">
                                Describe what you did/Your Achievements
                            </label>
                            <textarea id="description" name="description" class="editor" ><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>