<?php $current_user = wp_get_current_user(); ?>
<div class="bg-white txt-color-light padding-tb-30 padding-lr-40 margin-b-10">
    <?php
        $profile_query = new WP_Query();
        $profile_query->query( 
            array(
                'post_type' => 'language',
                'post_status' => 'publish',
                'author' => $current_user->ID,
                'posts_per_page' => -1,
            ) 
        );
    
        if( $profile_query->found_posts > 0 )
            $check[] = 'complete';
        else
            $check[] = 'incomplete';
    ?>
    <div class="profile-collapse">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-languages-collapse" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Languages you speak</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="cv-languages-collapse">
            <article class="padding-t-10">
                <p class="txt-sm padding-b-30">1 = Basic Understanding but can't speak, 2 = Fluent Spoken, 3 = Fluent Written & Spoken</p>
                <?php if ( $profile_query->have_posts() ) { ?>
                   
                    <div class="row row-10 txt-color-light">
                    <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                        <?php $post_id = $post->ID; ?> 

                        <!-- Entry -->
                        <div class="col-6 col-md-3 col-lg-2 padding-lr-10 padding-b-20 d-flex">
                            <a 
                                data-toggle="modal" href="#myCVLanguageModal-<?php echo $post_id ?>"
                                class="flex_1 d-flex align-items-center justify-content-between border padding-tb-10 padding-lr-15"
                            >
                                <p class="txt-sm txt-color-dark txt-medium">
                                    <?php echo get_the_title() ?>                                            
                                </p>
                                <div class="txt-normal-s">
                                    <div class="initials bg-ash txt-lg txt-medium txt-color-white">
                                        <?php 
                                            $meta = get_post_meta( $post_id, 'spoken_level', true ); 
                                            if($meta == 'None' )
                                                echo 0;
                                            elseif($meta == 'Basic' )
                                                echo 1;
                                            elseif($meta == 'Fluent' )
                                                echo 2;
                                            elseif($meta == 'Native' )
                                                echo 3;
                                            else
                                                echo 0;
                                        ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                        <!-- Edit Modal -->
                        <div class="modal fade font-main filter-modal" id="myCVLanguageModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="<?php echo currentUrl(false).'&form=cv-edit-language-'.$post_id; ?>" method="post">
                                        <div class="modal-header padding-lr-30">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Language</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body padding-o-30 form">
                                            <?php

                                                if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-language-'.$post_id ){
                                                    wp_delete_post($post_id); // Delete
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                }

                                                if( $_POST && $_GET['form'] == 'cv-edit-language-'.$post_id ){

                                                    /* Get Post Data */
                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                    $spoken_level = sanitize_text_field( $_POST['spoken_level'] );
                                                    $written_level = sanitize_text_field( $_POST['written_level'] );

                                                    /* Save Post to DB */
                                                    $post_id = wp_insert_post(array (
                                                        'ID' => $post_id,
                                                        'post_type' => 'language',
                                                        'post_title' => $post_name,
                                                        'post_content' => "",
                                                        'post_status' => 'publish',
                                                    ));

                                                    /* Add Post Meta */
                                                    update_post_meta( $post_id, 'spoken_level', $spoken_level );
                                                    update_post_meta( $post_id, 'written_level', $written_level );

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                }
                                            ?>
                                            <div class="form-item">
                                                <div class="form-group">
                                                    <label for="post_name">
                                                        Language
                                                    </label>
                                                    <select name="post_name" id="post_name">
                                                        <?php $meta = get_the_title( $post_id ); ?>
                                                        <option value="English" <?php echo ($meta == 'English') ? 'selected' : '' ?>>English</option>
                                                        <option value="Yoruba" <?php echo ($meta == 'Yoruba') ? 'selected' : '' ?>>Yoruba</option>
                                                        <option value="Hausa" <?php echo ($meta == 'Hausa') ? 'selected' : '' ?>>Hausa</option>
                                                        <option value="Fulani" <?php echo ($meta == 'Fulani') ? 'selected' : '' ?>>Fulani</option>
                                                        <option value="Ibo" <?php echo ($meta == 'Ibo') ? 'selected' : '' ?>>Ibo</option>
                                                        <option value="Swahili" <?php echo ($meta == 'Swahili') ? 'selected' : '' ?>>Swahili</option>
                                                        <option value="Chinese" <?php echo ($meta == 'Chinese') ? 'selected' : '' ?>>Chinese</option>
                                                        <option value="French" <?php echo ($meta == 'French') ? 'selected' : '' ?>>French</option>
                                                        <option value="Arabic" <?php echo ($meta == 'Arabic') ? 'selected' : '' ?>>Arabic</option>
                                                        <option value="Portuguese" <?php echo ($meta == 'Portuguese') ? 'selected' : '' ?>>Portuguese</option>
                                                        <option value="Spanish" <?php echo ($meta == 'Spanish') ? 'selected' : '' ?>>Spanish</option>
                                                        <option value="German" <?php echo ($meta == 'German') ? 'selected' : '' ?>>German</option>
                                                        <option value="Hindi" <?php echo ($meta == 'Hindi') ? 'selected' : '' ?>>Hindi</option>
                                                        <option value="Gujarati" <?php echo ($meta == 'Gujarati') ? 'selected' : '' ?>>Gujarati</option>
                                                        <option value="Punjabi" <?php echo ($meta == 'Punjabi') ? 'selected' : '' ?>>Punjabi</option>
                                                        <option value="Swedish" <?php echo ($meta == 'Swedish') ? 'selected' : '' ?>>Swedish</option>
                                                        <option value="Japanese" <?php echo ($meta == 'Japanese') ? 'selected' : '' ?>>Japanese</option>
                                                        <option value="Russian" <?php echo ($meta == 'Russian') ? 'selected' : '' ?>>Russian</option>
                                                        <option value="Korean" <?php echo ($meta == 'Korean') ? 'selected' : '' ?>>Korean</option>
                                                        <option value="Turkish" <?php echo ($meta == 'Turkish') ? 'selected' : '' ?>>Turkish</option>
                                                        <option value="Italian" <?php echo ($meta == 'Italian') ? 'selected' : '' ?>>Italian</option>
                                                        <option value="Finnish" <?php echo ($meta == 'Finnish') ? 'selected' : '' ?>>Finnish</option>
                                                        <option value="Polish" <?php echo ($meta == 'Polish') ? 'selected' : '' ?>>Polish</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-item">
                                                <div class="form-group">
                                                    <label for="spoken_level">
                                                        Fluency
                                                    </label>
                                                    <select name="spoken_level" id="spoken_level">
                                                        <?php $meta = get_post_meta( $post_id, 'spoken_level', true ); ?>
                                                        <option value="None" <?php echo ($meta == 'None') ? 'selected' : '' ?>>None</option>
                                                        <option value="Basic" <?php echo ($meta == 'Basic') ? 'selected' : '' ?>>Basic</option>
                                                        <option value="Fluent" <?php echo ($meta == 'Fluent') ? 'selected' : '' ?>>Fluent</option>
                                                        <option value="Native" <?php echo ($meta == 'Native') ? 'selected' : '' ?>>Native</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between padding-lr-30">
                                            <div>
                                                <a 
                                                    href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-language-'.$post_id; ?>" class="btn btn-trans-bw txt-sm confirm-delete"
                                                >
                                                    Delete
                                                </a>
                                            </div>
                                            <div>
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?> 
                    </div>
                    
                <?php }else{ ?> 
                    
                    <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                        You need to add a language you can speak before you can proceed.
                    </p>
                    
                <?php } ?>
            </article>
            <!--CTA-->
            <article class="padding-t-10">
                <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddLanguageModal">Add New</a>
            </article>
        </div>
    </div>
    <!-- Add Modal -->
    <div class="modal fade font-main filter-modal" id="myCVAddLanguageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(false).'&form=cv-add-language'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Add a Language</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            $post_id = 0;

                            if( $_POST && $_GET['form'] == 'cv-add-language' ){

                                /* Get Post Data */
                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                $spoken_level = sanitize_text_field( $_POST['spoken_level'] );
                                $written_level = sanitize_text_field( $_POST['written_level'] );

                                /* Save Post to DB */
                                $post_id = wp_insert_post(array (
                                    'ID' => $post_id,
                                    'post_type' => 'language',
                                    'post_title' => $post_name,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                /* Add Post Meta */
                                update_post_meta( $post_id, 'spoken_level', $spoken_level );
                                update_post_meta( $post_id, 'written_level', $written_level );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                            }
                        ?>
                        <div class="form-item">
                            <div class="form-group">
                                <label for="post_name">
                                    Language
                                </label>
                                <select name="post_name" id="post_name">
                                    <?php $meta = get_the_title( $post_id ); ?>
                                    <option value="English" <?php echo ($meta == 'English') ? 'selected' : '' ?>>English</option>
                                    <option value="Yoruba" <?php echo ($meta == 'Yoruba') ? 'selected' : '' ?>>Yoruba</option>
                                    <option value="Hausa" <?php echo ($meta == 'Hausa') ? 'selected' : '' ?>>Hausa</option>
                                    <option value="Fulani" <?php echo ($meta == 'Fulani') ? 'selected' : '' ?>>Fulani</option>
                                    <option value="Ibo" <?php echo ($meta == 'Ibo') ? 'selected' : '' ?>>Ibo</option>
                                    <option value="Swahili" <?php echo ($meta == 'Swahili') ? 'selected' : '' ?>>Swahili</option>
                                    <option value="Chinese" <?php echo ($meta == 'Chinese') ? 'selected' : '' ?>>Chinese</option>
                                    <option value="French" <?php echo ($meta == 'French') ? 'selected' : '' ?>>French</option>
                                    <option value="Arabic" <?php echo ($meta == 'Arabic') ? 'selected' : '' ?>>Arabic</option>
                                    <option value="Portuguese" <?php echo ($meta == 'Portuguese') ? 'selected' : '' ?>>Portuguese</option>
                                    <option value="Spanish" <?php echo ($meta == 'Spanish') ? 'selected' : '' ?>>Spanish</option>
                                    <option value="German" <?php echo ($meta == 'German') ? 'selected' : '' ?>>German</option>
                                    <option value="Hindi" <?php echo ($meta == 'Hindi') ? 'selected' : '' ?>>Hindi</option>
                                    <option value="Gujarati" <?php echo ($meta == 'Gujarati') ? 'selected' : '' ?>>Gujarati</option>
                                    <option value="Punjabi" <?php echo ($meta == 'Punjabi') ? 'selected' : '' ?>>Punjabi</option>
                                    <option value="Swedish" <?php echo ($meta == 'Swedish') ? 'selected' : '' ?>>Swedish</option>
                                    <option value="Japanese" <?php echo ($meta == 'Japanese') ? 'selected' : '' ?>>Japanese</option>
                                    <option value="Russian" <?php echo ($meta == 'Russian') ? 'selected' : '' ?>>Russian</option>
                                    <option value="Korean" <?php echo ($meta == 'Korean') ? 'selected' : '' ?>>Korean</option>
                                    <option value="Turkish" <?php echo ($meta == 'Turkish') ? 'selected' : '' ?>>Turkish</option>
                                    <option value="Italian" <?php echo ($meta == 'Italian') ? 'selected' : '' ?>>Italian</option>
                                    <option value="Finnish" <?php echo ($meta == 'Finnish') ? 'selected' : '' ?>>Finnish</option>
                                    <option value="Polish" <?php echo ($meta == 'Polish') ? 'selected' : '' ?>>Polish</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-item">
                            <div class="form-group">
                                <label for="spoken_level">
                                    Fluency
                                </label>
                                <select name="spoken_level" id="spoken_level">
                                    <?php $meta = get_post_meta( $post_id, 'spoken_level', true ); ?>
                                    <option value="None" <?php echo ($meta == 'None') ? 'selected' : '' ?>>None</option>
                                    <option value="Basic" <?php echo ($meta == 'Basic') ? 'selected' : '' ?>>Basic</option>
                                    <option value="Fluent" <?php echo ($meta == 'Fluent') ? 'selected' : '' ?>>Fluent</option>
                                    <option value="Native" <?php echo ($meta == 'Native') ? 'selected' : '' ?>>Native</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>