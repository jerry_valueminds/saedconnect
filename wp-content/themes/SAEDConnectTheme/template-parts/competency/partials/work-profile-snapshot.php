<?php $current_user = wp_get_current_user(); ?>
<?php 
    $months_array = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    
    switch_to_blog(110);
    $terms = get_terms( 'state', array('hide_empty' => false));
    $coverage = get_terms( 'coverage-nigeria', array('hide_empty' => false));
    $countries = get_terms( 'country', array('hide_empty' => false));
    $subjects = get_terms( 'subject', array('hide_empty' => false));
    restore_current_blog();
?>
<div class="bg-white txt-color-light padding-tb-30 padding-lr-40 margin-b-10">
    <?php
        $profile_query = new WP_Query();
        $profile_query->query( 
            array(
                'post_type' => 'snapshot',
                'post_status' => 'publish',
                'author' => $current_user->ID,
                'posts_per_page' => 1,
            ) 
        );
    
        if( $profile_query->found_posts > 0 )
            $check[] = 'complete';
        else
            $check[] = 'incomplete';
    ?>
    <div class="profile-collapse">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-snapshot-collapse" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Your Work profile Snapshot</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="cv-snapshot-collapse">
            <article class="padding-t-30">
                <?php if ( $profile_query->have_posts() ) { ?>

                    <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                        <?php $post_id = $post->ID; ?> 

                        <h4 class="txt-normal-s txt-medium txt-color-yellow margin-b-15">Your Personal Information Snap Shot</h4>
                        <div class="overflow-hidden">
                            <div class="row row-20 txt-color-light padding-b-10 margin-b-30 border-bottom">
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Date of Birth        
                                    </p>
                                    <p class="txt-sm">
                                        <?php 
                                            $date = strtotime( get_post_meta( $post_id, 'date_of_birth', true ) );
                                            echo date('j F Y', $date);
                                        ?>
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Gender   
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'gender', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Marital Status    
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'marital_status', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        State of Origin    
                                    </p>
                                    <p class="txt-sm">
                                        <?php 
                                            $meta = get_post_meta( $post_id, 'state_of_origin', true ); 
                                            foreach($coverage as $term){
                                                if( $meta == $term->term_id ){
                                                    echo $term->name;
                                                }
                                            }
                                        ?>  
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        LGA 
                                    </p>
                                    <p class="txt-sm">
                                        <?php 
                                            $meta = get_post_meta( $post_id, 'lga', true ); 
                                            foreach($coverage as $term){
                                                if( $meta == $term->term_id ){
                                                    echo $term->name;
                                                }
                                            }
                                        ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Your Current Location    
                                    </p>
                                    <p class="txt-sm">
                                        <?php 
                                            $saved_id = get_post_meta( $post_id, 'work_location', true ); 
                                            switch_to_blog(110);
                                            $term = get_term( $saved_id );
                                            echo $term->name;
                                            restore_current_blog();
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <h4 class="txt-normal-s txt-medium txt-color-yellow margin-b-15">Your O-Levels Snap Shot</h4>
                        <div class="overflow-hidden">
                            <div class="row row-20 txt-color-light padding-b-10 margin-b-30 border-bottom">
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        O-Level Certification        
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'o_level', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        No of Credits/ Distinctions       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'number_of_credits', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Grade (English)      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'english_grade', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Grade (Maths)      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'maths_grade', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Favorite Subject         
                                    </p>
                                    <p class="txt-sm">
                                        <?php 
                                            $meta = get_post_meta( $post_id, 'best_secondary_school_subject', true ); 
                                            foreach($subjects as $term){
                                                if( $meta == $term->term_id ){
                                                    echo $term->name;
                                                }
                                            }
                                        ?> 
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <h4 class="txt-normal-s txt-medium txt-color-yellow margin-b-15">Your First Degree Snapshot</h4>
                        <div class="overflow-hidden">
                            <div class="row row-20 txt-color-light padding-b-10 margin-b-30 border-bottom">
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Institution Type       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'first_degree_institution_type', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Graduation Grade       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'first_degree_grade', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Country      
                                    </p>
                                    <p class="txt-sm">
                                        <?php 
                                            $meta = get_post_meta( $post_id, 'first_degree_country', true ); 
                                            foreach($countries as $term){
                                                if( $meta == $term->term_id ){
                                                    echo $term->name;
                                                }
                                            }
                                        ?>
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Graduation Year      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'first_degree_graduation_year', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        School      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'first_degree_institution', true ); ?> 
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <h4 class="txt-normal-s txt-medium txt-color-yellow margin-b-15">Your Work Preferences</h4>
                        <div class="overflow-hidden">
                            <div class="row row-20 txt-color-light padding-b-10 margin-b-30 border-bottom">
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        Highest Qualification      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'highest_qualification', true ); ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        States you are willing to work      
                                    </p>
                                    <p class="txt-sm">
                                        <?php 
                                            $display_list = '';
                                            $saved_ids = get_post_meta( $post_id, 'state_i_can_work', true ); 
                                            foreach( $terms as $term ){
                                                echo $term->ID;
                                                if( in_array($term->term_id, $saved_ids) )
                                                    $display_list .= $term->name.', ';
                                            }
                                                           
                                            echo substr_replace($display_list ,"",-2);
                                        ?> 
                                    </p>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3 padding-lr-20 padding-b-20">
                                    <p class="txt-sm txt-bold txt-color-light padding-b-5">
                                        International Work      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo get_post_meta( $post_id, 'international_work_experience', true ); ?> 
                                    </p>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?> 

                <?php }else{ ?> 
                   
                    <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                        You need to complete your work profile snapshot before you can proceed
                    </p>
                    
                <?php } ?>
            </article>
            <!--CTA-->
            <article class="padding-t-10">
                <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVSnapshotModal">
                    <?php echo ( $profile_query->found_posts > 0 ) ? 'Update' : 'Create Profile'; ?>
                </a>
            </article>
        </div>
    </div>
    <!-- Add / Edit Modal -->
    <div class="modal fade font-main filter-modal" id="myCVSnapshotModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(false).'&form=cv-snapshot'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Education & Work experience snapshot</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php

                            if( $_POST && $_GET['form'] == 'cv-snapshot' ){

                                /* Get Post Name */
                                $postName = 'Snapshot-'.$current_user->ID;

                                /* Get Form Data */
                                $gender = sanitize_text_field( $_POST['gender'] ); 
                                $date_of_birth = sanitize_text_field( $_POST['date_of_birth'] ); 
                                $marital_status = sanitize_text_field( $_POST['marital_status'] ); 
                                $nationality = sanitize_text_field( $_POST['nationality'] ); 
                                $state_of_origin = sanitize_text_field( $_POST['state_of_origin'] ); 
                                $lga = sanitize_text_field( $_POST['lga_'.$state_of_origin] ); 
                                
                                $o_level = sanitize_text_field( $_POST['o_level'] ); 
                                $number_of_credits = sanitize_text_field( $_POST['number_of_credits'] ); 
                                $english_grade = sanitize_text_field( $_POST['english_grade'] ); 
                                $maths_grade = sanitize_text_field( $_POST['maths_grade'] ); 
                                $best_secondary_school_subject = sanitize_text_field( $_POST['best_secondary_school_subject'] ); 
                                
                                $first_degree_institution_type = sanitize_text_field( $_POST['first_degree_institution_type'] ); 
                                $first_degree_country = sanitize_text_field( $_POST['first_degree_country'] ); 
                                $first_degree_grade = sanitize_text_field( $_POST['first_degree_grade'] ); 
                                $first_degree_graduation_month = sanitize_text_field( $_POST['first_degree_graduation_month'] ); 
                                $first_degree_graduation_year = sanitize_text_field( $_POST['first_degree_graduation_year'] ); 
                                $first_degree_institution = sanitize_text_field( $_POST['first_degree_institution'] ); 
                                
                                $highest_qualification = sanitize_text_field( $_POST['highest_qualification'] ); 
                                $work_experience_post_secondary = sanitize_text_field( $_POST['work_experience_post_secondary'] ); 
                                $international_work_experience = sanitize_text_field( $_POST['international_work_experience'] ); 
                                $state_i_can_work = $_POST['state_i_can_work']; 


                                /* Save Post to DB */
                                $post_id = wp_insert_post(array (
                                    'ID' => $post_id,
                                    'post_type' => 'snapshot',
                                    'post_title' => $postName,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                /* Add Meta */
                                update_post_meta( $post_id, 'gender', $gender );
                                update_post_meta( $post_id, 'date_of_birth', $date_of_birth );
                                update_post_meta( $post_id, 'marital_status', $marital_status );
                                update_post_meta( $post_id, 'nationality', $nationality );
                                update_post_meta( $post_id, 'state_of_origin', $state_of_origin );
                                update_post_meta( $post_id, 'lga', $lga );
                                
                                update_post_meta( $post_id, 'o_level', $o_level );
                                update_post_meta( $post_id, 'number_of_credits', $number_of_credits );
                                update_post_meta( $post_id, 'english_grade', $english_grade );
                                update_post_meta( $post_id, 'maths_grade', $maths_grade );
                                update_post_meta( $post_id, 'best_secondary_school_subject', $best_secondary_school_subject );
                                
                                update_post_meta( $post_id, 'first_degree_institution_type', $first_degree_institution_type );
                                update_post_meta( $post_id, 'first_degree_country', $first_degree_country );
                                update_post_meta( $post_id, 'first_degree_grade', $first_degree_grade );
                                update_post_meta( $post_id, 'first_degree_graduation_month', $first_degree_graduation_month );
                                update_post_meta( $post_id, 'first_degree_graduation_year', $first_degree_graduation_year );
                                update_post_meta( $post_id, 'first_degree_institution', $first_degree_institution );
                                
                                update_post_meta( $post_id, 'highest_qualification', $highest_qualification );
                                update_post_meta( $post_id, 'work_experience_post_secondary', $work_experience_post_secondary );
                                update_post_meta( $post_id, 'international_work_experience', $international_work_experience );
                                update_post_meta( $post_id, 'state_i_can_work', $state_i_can_work );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                            }
                        ?>
                        <div class="form-item">
                            <label for="date_of_birth">
                                Date of Birth
                            </label>
                            <input 
                                type="date" 
                                name="date_of_birth" 
                                id="date_of_birth"
                                value="<?php echo get_post_meta( $post_id, 'date_of_birth', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="gender">
                                Gender
                            </label>
                            <select name="gender" id="gender">
                                <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                <option value="Male" <?php echo ($meta == 'Male') ? 'selected' : '' ?>>Male</option>
                                <option value="Female" <?php echo ($meta == 'Female') ? 'selected' : '' ?>>Female</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="marital_status">
                                Marital Status
                            </label>
                            <select name="marital_status" id="marital_status">
                                <?php $meta = get_post_meta( $post_id, 'marital_status', true ); ?>
                                <option value="Single" <?php echo ($meta == 'Single') ? 'selected' : '' ?>>Single</option>
                                <option value="Married" <?php echo ($meta == 'Married') ? 'selected' : '' ?>>Married</option>
                                <option value="Divorced" <?php echo ($meta == 'Divorced') ? 'selected' : '' ?>>Divorced</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="first_degree_country">
                                Nationality
                            </label>
                            <select name="nationality" id="nationality">
                                <?php 
                                    $meta = get_post_meta( $post_id, 'nationality', true );
                                    /* Return Terms assigned to Post */
                                    $term_list = get_post_meta( $post_id, 'nationality', true );

                                    foreach ($countries as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $parent = $term->parent;
                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                ?>

                                    <option value="<?php echo $term_id ?>" <?php echo ($meta == $term_id) ? 'selected' : '' ?>>
                                        <?php echo $term_name ?>
                                    </option>
                                    
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="state_of_origin">
                                State of Origin
                            </label>
                            <select name="state_of_origin" id="state_of_origin" class="select-state">
                                <?php $meta = get_post_meta( $post_id, 'state_of_origin', true ); ?>
                                <?php foreach ($coverage as $term) { ?>
                                    <?php if ($term->parent == 0) { ?>
                                        <option value="<?php echo $term->term_id ?>" <?php echo ( $term->term_id == $meta ) ? 'selected' : ''; ?>>
                                            <?php echo $term->name ?>
                                        </option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                        <?php $meta = get_post_meta( $post_id, 'lga', true ); ?>
                        <?php foreach ($coverage as $term) { ?>
                            <?php $parent_id = $term->term_id; ?>
                            <?php if ($term->parent == 0) { ?>
                                <div class="form-item select-lga d-none" id="lga-<?php echo $parent_id ?>">
                                    <label for="lga_<?php echo $parent_id ?>">
                                        Local Government Area
                                    </label>
                                    <select name="lga_<?php echo $parent_id ?>" id="lga_<?php echo $parent_id ?>">
                                    <?php foreach ($coverage as $term) { ?>
                                        <?php if ($term->parent == $parent_id) { ?>

                                            <option value="<?php echo $term->term_id ?>" <?php echo ( $term->term_id == $meta ) ? 'selected' : ''; ?>>
                                                <?php echo $term->name ?>
                                            </option>
                                        <?php } ?>
                                    <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <div class="form-item">
                            <label for="work_location">
                                Where you currently reside
                            </label>
                            <select name="work_location" id="work_location">
                                <?php $meta = get_post_meta( $post_id, 'work_location', true ); ?>
                                <?php foreach ($terms as $term) { ?>
                                    <option value="<?php echo $term->term_id ?>" <?php echo ( $term->term_id == $meta ) ? 'selected' : ''; ?>>
                                        <?php echo $term->name ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        
                        <div class="form-item">
                            <label for="o_level">
                                What O-Level Certification do you have?
                            </label>
                            <select name="o_level" id="o_level">
                                <?php $meta = get_post_meta( $post_id, 'o_level', true ); ?>
                                <option value="SSCE" <?php echo ($meta == 'SSCE') ? 'selected' : '' ?>>SSCE</option>
                                <option value="GCE" <?php echo ($meta == 'GCE') ? 'selected' : '' ?>>GCE</option>
                                <option value="NECO" <?php echo ($meta == 'NECO') ? 'selected' : '' ?>>NECO</option>
                                <option value="Others" <?php echo ($meta == 'Others') ? 'selected' : '' ?>>Others</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="number_of_credits">
                                How many Credits & distinctions did you make?
                            </label>
                            <select name="number_of_credits" id="number_of_credits">
                                <?php $meta = get_post_meta( $post_id, 'o_level', true ); ?>
                                <option value="1" <?php echo ($meta == '1') ? 'selected' : '' ?>>1</option>
                                <option value="2" <?php echo ($meta == '2') ? 'selected' : '' ?>>2</option>
                                <option value="3" <?php echo ($meta == '3') ? 'selected' : '' ?>>3</option>
                                <option value="4" <?php echo ($meta == '4') ? 'selected' : '' ?>>4</option>
                                <option value="5" <?php echo ($meta == '5') ? 'selected' : '' ?>>5</option>
                                <option value="6" <?php echo ($meta == '6') ? 'selected' : '' ?>>6</option>
                                <option value="7" <?php echo ($meta == '7') ? 'selected' : '' ?>>7</option>
                                <option value="8" <?php echo ($meta == '8') ? 'selected' : '' ?>>8</option>
                                <option value="9" <?php echo ($meta == '9') ? 'selected' : '' ?>>9</option>
                                <option value="10" <?php echo ($meta == '10') ? 'selected' : '' ?>>10</option>
                                <option value="11" <?php echo ($meta == '11') ? 'selected' : '' ?>>11</option>
                                <option value="12" <?php echo ($meta == '12') ? 'selected' : '' ?>>12</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="english_grade">
                                What grade did you get in English at O-Levels?
                            </label>
                            <select name="english_grade" id="english_grade">
                                <?php $meta = get_post_meta( $post_id, 'o_level', true ); ?>
                                <option value="Distinction (A1,B2, or B3)" <?php echo ($meta == 'Distinction (A1,B2, or B3)') ? 'selected' : '' ?>>Distinction (A1,B2, or B3)</option>
                                <option value="Credit (C4 to C6)" <?php echo ($meta == 'Credit (C4 to C6)') ? 'selected' : '' ?>>Credit (C4 to C6)</option>
                                <option value="Pass (D7 & E8)" <?php echo ($meta == 'Pass (D7 & E8)') ? 'selected' : '' ?>>Pass (D7 & E8)</option>
                                <option value="Fail" <?php echo ($meta == 'Fail') ? 'selected' : '' ?>>Fail</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="maths_grade">
                                What grade did you get in Maths at O-Levels?
                            </label>
                            <select name="maths_grade" id="maths_grade">
                                <?php $meta = get_post_meta( $post_id, 'o_level', true ); ?>
                                <option value="Distinction (A1,B2, or B3)" <?php echo ($meta == 'Distinction (A1,B2, or B3)') ? 'selected' : '' ?>>Distinction (A1,B2, or B3)</option>
                                <option value="Credit (C4 to C6)" <?php echo ($meta == 'Credit (C4 to C6)') ? 'selected' : '' ?>>Credit (C4 to C6)</option>
                                <option value="Pass (D7 & E8)" <?php echo ($meta == 'Pass (D7 & E8)') ? 'selected' : '' ?>>Pass (D7 & E8)</option>
                                <option value="Fail" <?php echo ($meta == 'Fail') ? 'selected' : '' ?>>Fail</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="best_secondary_school_subject">
                                What was your best subject in Secondary School?
                            </label>
                            <select name="best_secondary_school_subject" id="best_secondary_school_subject">
                                <?php 
                                    /* Return Terms assigned to Post */
                                    $term_list = get_post_meta( $post_id, 'best_secondary_school_subject', true );

                                    foreach ($subjects as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $parent = $term->parent;
                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                ?>

                                    <option value="<?php echo $term_id ?>" <?php echo ($meta == '1') ? 'selected' : '' ?>>
                                        <?php echo $term_name ?>
                                    </option>
                                    
                                <?php } ?>
                            </select>
                        </div>
                        
                        <div class="form-item">
                            <label for="first_degree_institution_type">
                                What sort of tertiary institution did you attend for your first degree?
                            </label>
                            <select name="first_degree_institution_type" id="first_degree_institution_type">
                                <?php $meta = get_post_meta( $post_id, 'first_degree_institution_type', true ); ?>
                                <option value="University" <?php echo ($meta == 'University') ? 'selected' : '' ?>>University</option>
                                <option value="Polytechnic" <?php echo ($meta == 'Polytechnic') ? 'selected' : '' ?>>Polytechnic</option>
                                <option value="College" <?php echo ($meta == 'College') ? 'selected' : '' ?>>College</option>
                                <option value="Vocational Institute" <?php echo ($meta == 'Vocational Institute') ? 'selected' : '' ?>>Vocational Institute</option>
                                <option value="Other" <?php echo ($meta == 'Other') ? 'selected' : '' ?>>Other</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="first_degree_country">
                                In what country did you study for your first degree?
                            </label>
                            <select name="first_degree_country" id="first_degree_country">
                                <?php 
                                    /* Return Terms assigned to Post */
                                    $term_list = get_post_meta( $post_id, 'first_degree_country', true );

                                    foreach ($countries as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $parent = $term->parent;
                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                ?>

                                    <option value="<?php echo $term_id ?>" <?php echo ($meta == '1') ? 'selected' : '' ?>>
                                        <?php echo $term_name ?>
                                    </option>
                                    
                                <?php } ?>
                            </select>
                        </div>
                        <div class="row row-10">
                            <div class="form-item col-md-6 padding-lr-10">
                                <label for="first_degree_graduation_month">
                                    When did you graduate?
                                </label>
                                <select name="first_degree_graduation_month" id="first_degree_graduation_month">
                                    <?php $meta = get_post_meta( $post_id, 'first_degree_graduation_month', true ); ?>
                                    <?php foreach ($months_array as $key => $month){ ?>
                                        <option value="<?php echo ($key + 1); ?>" <?php echo ($meta == ($key + 1)) ? 'selected' : '' ?>><?php echo $month ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-item col-md-6 padding-lr-10">
                                <label for="first_degree_graduation_year" style="opacity:0;">
                                    When did you graduate?
                                </label>
                                <select name="first_degree_graduation_year" id="first_degree_graduation_year">
                                    <?php $meta = get_post_meta( $post_id, 'first_degree_graduation_year', true ); ?>
                                    <?php $current_year = date('Y'); ?>
                                    <?php for ( $i = ( $current_year - 50 ); $i <= ( $current_year + 10 ); $i++ ){ ?>
                                        <option value="<?php echo $i; ?>" <?php echo ($meta == $i) ? 'selected' : '' ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-item">
                            <label for="first_degree_grade">
                                What grade did you get in your first degree?
                            </label>
                            <select name="first_degree_grade" id="first_degree_grade">
                                <?php $meta = get_post_meta( $post_id, 'first_degree_grade', true ); ?>
                                <option value="First Class" <?php echo ($meta == 'First Class') ? 'selected' : '' ?>>First Class</option>
                                <option value="Distinction" <?php echo ($meta == 'Distinction') ? 'selected' : '' ?>>Distinction</option>
                                <option value="2nd Class Upper" <?php echo ($meta == '2nd Class Upper') ? 'selected' : '' ?>>2nd Class Upper</option>
                                <option value="Upper Credit" <?php echo ($meta == 'Upper Credit') ? 'selected' : '' ?>>Upper Credit</option>
                                <option value="2nd Class Lower" <?php echo ($meta == '2nd Class Lower') ? 'selected' : '' ?>>2nd Class Lower</option>
                                <option value="Lower Credit" <?php echo ($meta == 'Lower Credit') ? 'selected' : '' ?>>Lower Credit</option>
                                <option value="3rd Class" <?php echo ($meta == '3rd Class') ? 'selected' : '' ?>>3rd Class</option>
                                <option value="Pass" <?php echo ($meta == 'Pass') ? 'selected' : '' ?>>Pass</option>
                                <option value="Other" <?php echo ($meta == 'Other') ? 'selected' : '' ?>>Other</option>
                                <option value="Not Applicable" <?php echo ($meta == 'Not Applicable') ? 'selected' : '' ?>>Not Applicable</option>
                            </select>
                        </div>
                        
                        <div class="form-item">
                            <label for="highest_qualification">
                                What is your highest educational Qualification
                            </label>
                            <select name="highest_qualification" id="highest_qualification">
                                <?php $meta = get_post_meta( $post_id, 'highest_qualification', true ); ?>
                                <option value="Secondary School Certificate" <?php echo ($meta == 'Secondary School Certificate') ? 'selected' : '' ?>>Secondary School Certificate</option>
                                <option value="Diploma" <?php echo ($meta == 'Diploma') ? 'selected' : '' ?>>Diploma</option>
                                <option value="HND" <?php echo ($meta == 'HND') ? 'selected' : '' ?>>HND</option>
                                <option value="Bachelors Degree" <?php echo ($meta == 'Bachelors Degree') ? 'selected' : '' ?>>Bachelors Degree</option>
                                <option value="Post Graduate Degree" <?php echo ($meta == 'Post Graduate Degree') ? 'selected' : '' ?>>Post Graduate Degree</option>
                                <option value="Masters" <?php echo ($meta == 'Masters') ? 'selected' : '' ?>>Masters</option>
                                <option value="Phd" <?php echo ($meta == 'Phd') ? 'selected' : '' ?>>Phd</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="work_experience_post_secondary">
                                Have you worked anywhere before (Post secondary school)?
                            </label>
                            <select name="work_experience_post_secondary" id="work_experience_post_secondary">
                                <?php $meta = get_post_meta( $post_id, 'work_experience_post_secondary', true ); ?>
                                <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?>>Yes</option>
                                <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?>>No</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="work_experience_years">
                                How long have you worked (Your combined post secondary school work experience)
                            </label>
                            <input 
                                type="number" 
                                name="work_experience_years" 
                                id="work_experience_years"
                                value="<?php echo get_post_meta( $post_id, 'work_experience_years', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <div>
                                <label for="state_i_can_work">
                                    In what state(s) in Nigeria are you willing to work in?
                                </label>
                            </div>
                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = get_post_meta( $post_id, 'state_i_can_work', true );

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-15 padding-b-10">
                                    <input
                                        class="margin-r-5"
                                        type="checkbox" 
                                        value="<?php echo $term_id ?>" 
                                        name="state_i_can_work[]" 
                                        <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                    >
                                    <span class="bg-label padding-l-5">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>

                            <?php } ?>
                        </div>
                        <div class="form-item">
                            <label for="international_work_experience">
                                Do you have any international work experience?
                            </label>
                            <select name="international_work_experience" id="international_work_experience">
                                <?php $meta = get_post_meta( $post_id, 'international_work_experience', true ); ?>
                                <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?>>No</option>
                                <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?>>Yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    /* Onload */
    var parent_id = parseInt( $('#state_of_origin').find(":selected").val() );
    console.log(parent_id);
    $('.select-lga').addClass('d-none');
    $('#lga-' + parent_id).removeClass('d-none');

    /* Onchange */
    $(".select-state").on("change", function() {
        var parent_id = parseInt( $(this).find(":selected").val() );
        console.log(parent_id);
        $('.select-lga').addClass('d-none');
        $('#lga-' + parent_id).removeClass('d-none');
    });
</script>