<?php $current_user = wp_get_current_user(); ?>
<?php switch_to_blog(101); ?>

<div class="bg-white txt-color-light padding-tb-30 padding-lr-40 margin-b-10">
    <div class="profile-collapse">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-capabilities-collapse" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Your Skills and capabilities</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="cv-capabilities-collapse">
            <article class="padding-t-10">
                <p class="txt-sm padding-b-30">Add as many skills and capabilities as you have. Make sure to rate your competency in each skill and describe your experience to give potential employers a good sense of what you can do.</p>
                <?php
                    /* Get User Meta to populate Form */
                    $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, 'capability_profile', true) );

                ?>

                <?php if($unserialized_data){ //If no entry ?>

                    <div class="row row-10">
                    <?php
                        foreach($unserialized_data as $key => $subterms){
                            $term = get_term($key);
                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                    ?>

                            <!--<p class="txt-xs txt-color-dark txt-medium padding-b-15">
                            <?php echo $term_name ?>      
                            </p>-->
                            <?php 
                                foreach($subterms as $subterm){
                                    $child_term = get_term($subterm);
                                    $child_term_id = $child_term->term_id; //Get the term ID
                                    $child_term_name = $child_term->name; //Get the term name
                                    $meta = get_user_meta($current_user->ID, 'skill_level_'.$child_term_id, true);
                            ?>
                            <div class="col-lg-2 padding-lr-10 padding-b-20 d-flex">
                                <div class="border padding-o-10 flex_1 text-center">
                                    <p class="txt-sm txt-color-dark txt-bold padding-b-10">
                                        <?php echo $child_term_name ?>
                                    </p>       
                                    <?php if($meta){ ?>
                                        <div class="margin-b-5">
                                            <div class="initials border-o-1 padding-o-20">
                                                <?php echo $meta ?>
                                            </div>
                                        </div>   
                                        <p class="txt-normal-s txt-color-light padding-b-10">
                                            <?php 
                                                if($meta == '1')
                                                    echo 'Newbie';    
                                                elseif($meta == '2')
                                                    echo 'Novice'; 
                                                elseif($meta == '3')
                                                    echo 'Apprentice'; 
                                                elseif($meta == '4')
                                                    echo 'Task Leader'; 
                                                elseif($meta == '5')
                                                    echo 'Expert'; 
                                                elseif($meta == '5')
                                                    echo 'Specialist'; 
                                            ?>
                                        </p> 
                                    <?php } ?>                      
                                    <p>
                                       <a 
                                          data-toggle="modal" 
                                          href="#myCapabilityDescriptionModal-<?php echo $child_term_id ?>"
                                          class="txt-sm txt-medium"
                                        >
                                            <?php if($meta){ ?>
                                               <u class="txt-color-green"><?php echo ($meta) ? 'Update' : '' ?></u>
                                            <?php }else{ ?>
                                                <u class="txt-color-red">Rate yourself</u>
                                            <?php } ?>
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <!-- Add Description -->
                            <div class="modal fade font-main filter-modal" id="myCapabilityDescriptionModal-<?php echo $child_term_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form action="<?php echo currentUrl(false).'&form=add-capability-description-'.$child_term_id; ?>" method="post">
                                            <div class="modal-header padding-lr-30">
                                                <h5 class="modal-title" id="exampleModalLabel">Rate yourself</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body padding-o-30 form">
                                                <?php
                                                    $post_id = 0;

                                                    if( $_POST && $_GET['form'] == 'add-capability-description-'.$child_term_id ){

                                                        /* Add Post Meta */
                                                        update_user_meta( $current_user->ID, 'skill_level_'.$child_term_id, sanitize_text_field( $_POST['skill_level'] ));

                                                        /* Redirect */
                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                                                    }
                                                ?>
                                                <div class="competency-form bg-white">
                                                    <h2 class="txt-xlg text-center txt-bold txt-color-dark margin-b-40">
                                                        Rate yourself in <?php echo $child_term_name ?> 
                                                    </h2>
                                                    <div class="margin-b-20">
                                                        <div class="form-item">
                                                            <div class="form-group">
                                                                <select name="skill_level" id="skill_level_<?php echo $child_term_id ?>" class="dropdown-item">
                                                                    <?php $meta = get_user_meta($current_user->ID, 'skill_level_'.$child_term_id, true); ?>
                                                                    <option value="1" <?php echo ($meta == '1') ? 'selected' : '' ?>>Newbie</option>
                                                                    <option value="2" <?php echo ($meta == '2') ? 'selected' : '' ?>>Novice</option>
                                                                    <option value="3" <?php echo ($meta == '3') ? 'selected' : '' ?>>Apprentice</option>
                                                                    <option value="4" <?php echo ($meta == '4') ? 'selected' : '' ?>>Task Leader</option>
                                                                    <option value="5" <?php echo ($meta == '5') ? 'selected' : '' ?>>Expert</option>
                                                                    <option value="6" <?php echo ($meta == '6') ? 'selected' : '' ?>>Specialist</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="padding-tb-40 padding-lr-30 border-o-1 border-color-lighter competency-card">
                                                        <figure class="image-box margin-b-20">
                                                            <img src="images/icons/levels/1-levels.png" alt="">
                                                            <!--<figcation>
                                                                <span class="level">
                                                                    Level
                                                                </span>
                                                                <span class="number">
                                                                    1
                                                                </span>
                                                            </figcation>-->
                                                        </figure>
                                                        <p class="selection-title txt-normal-s txt-bold txt-color-dark padding-b-10"></p>
                                                        <p class="selection-text txt-normal-s txt-regular txt-color-light txt-height-1-7"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer padding-lr-30">
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                    <?php } ?>
                    </div>
                <?php } else { ?>

                <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                    You need to complete your Skills and capabilities before you can proceed
                </p>

                <?php } ?>
            </article>
            <!--CTA-->
            <article class="padding-t-10">
                <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCapabilitiesModal">Add</a>
            </article>
        </div>
    </div>
    
    <!-- Add Modal -->
    <div class="modal fade font-main filter-modal" id="myCapabilitiesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(false).'&form=cv-add-capabilities'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Select your capabilities</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            $meta_key = 'capability_profile';
                            $tax_type = 'capability';
                        
                            if( $_POST && $_GET['form'] == 'cv-add-capabilities' ){

                                /* Serialize Form Submission */
                                $serialized_data = maybe_serialize($_POST);

                                /* Save usermeta */
                                update_user_meta( $current_user->ID, $meta_key, $serialized_data);

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                            }
                        ?>
                        <?php
                                /* Get User Meta to populate Form */
                                $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, $meta_key, true) );

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type, array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <div class="margin-b-20">
                                <?php if( $parent == 0 ){ ?>

                                    <div class="txt-normal-s txt-medium txt-color-dark margin-b-15">
                                        <?php echo $term_name; ?>
                                    </div>
                                    <?php
                                        foreach ($terms as $child_term) {
                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $child_parent = $child_term->parent;
                                            $child_term_id = $child_term->term_id; //Get the term ID
                                            $child_term_name = $child_term->name; //Get the term name

                                            if( $child_parent == $term_id ){
                                    ?>
                                            <label class="txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                <input
                                                    class="margin-r-5"
                                                    type="checkbox" 
                                                    value="<?php echo $child_term_id ?>" 
                                                    name="<?php echo $term_id ?>[]" 
                                                    <?php echo in_array($child_term_id, $unserialized_data[$term_id]) ? "checked" : "" ?>
                                                >
                                                <span class="padding-l-10"><?php echo $child_term_name; ?></span>
                                            </label>
                                    <?php
                                            }
                                        }
                                    ?>
                                <?php } ?>
                                </div>

                            <?php 
                                } 
                            ?>
                        
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var textContent = [
            {
                title: 'I don’t have this competency',
                description: ''
            },
            {
                title: 'I Know about this but I’ve never done it before',
                description: 'You have common knowledge or an understanding of basic techniques and concepts of this competency, but have never done it before.'
            },
            {
                title: 'I have limited experience doing this',
                description: 'You have the level of experience gained in a classroom and/or experimental scenarios or as a trainee on-the-job. You will need help when doing this task.'
            },
            {
                title: 'I can complete this task independently',
                description: 'You are able to successfully complete this task all by yourself with minimal or no supervision. Help from an expert may be required from time to time, but you can usually perform this task independently.'
            },
            {
                title: 'I am highly experienced at this task',
                description: 'You can complete this task without any assistance. You have deep experience doing this task. You can coach people who have difficulty completing this task.'
            },
            {
                title: 'I am an expert',
                description: 'You are known as an expert in this area. You perform this task with speed and can tweak it in various ways to improve its outcomes. You can build a training manual for this task.'
            }
    ];

    $( ".competency-form .dropdown-item" ).each( function(){
        var selection = parseInt( $(this).find(":selected").val() );     //Get selection made
        
        var parentForm = $(this).closest('.competency-form');    //Get Parent Form
        
        /* Web Server */
        var imageURL = 'https://www.saedconnect.org/wp-content/themes/SAEDConnectTheme/images/icons/levels/level-' + selection + '.png';

        /* Local File */
        //var imageURL = 'images/icons/levels/' + (selection) + '-levels.png';

        parentForm.find('.selection-title').text(textContent[ selection - 1].title);    //Update hidden Displayed title
        parentForm.find('.selection-text').text(textContent[ selection - 1].description);    //Update hidden Displayed Description
        parentForm.find('.number').text(selection);    //Update hidden Displayed Description
        parentForm.find('.image-box img').attr('src', imageURL);    //Update hidden Displayed Description
    });

    $(".competency-form .dropdown-item").change(function(){

        var selection = parseInt( $(this).find(":selected").val() );     //Get selection made

        var parentForm = $(this).closest('.competency-form');    //Get Parent Form

        /* Web Server */
        var imageURL = 'https://www.saedconnect.org/wp-content/themes/SAEDConnectTheme/images/icons/levels/level-' + selection + '.png';

        /* Local File */
        //var imageURL = 'images/icons/levels/' + (selection) + '-levels.png';

        parentForm.find('.selection-title').text(textContent[ selection - 1].title);    //Update hidden Displayed title
        parentForm.find('.selection-text').text(textContent[ selection - 1].description);    //Update hidden Displayed Description
        parentForm.find('.number').text(selection);    //Update hidden Displayed Description
        parentForm.find('.image-box img').attr('src', imageURL);    //Update hidden Displayed Description

    });
</script>
<?php restore_current_blog(); ?>