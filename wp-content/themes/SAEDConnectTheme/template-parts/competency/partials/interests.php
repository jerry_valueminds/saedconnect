<?php $current_user = wp_get_current_user(); ?>
<div class="bg-white txt-color-light padding-tb-30 padding-lr-40 margin-b-10">
    <?php
        $profile_query = new WP_Query();
        $profile_query->query( 
            array(
                'post_type' => 'interest',
                'post_status' => 'publish',
                'author' => $current_user->ID,
                'posts_per_page' => -1,
            ) 
        );
    
        if( $profile_query->found_posts > 0 || get_user_meta($current_user->ID, 'cv_no_interest', true) )
            $check[] = 'complete';
        else
            $check[] = 'incomplete';
    ?>
    <div class="profile-collapse">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-interest-collapse" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Your Hobbies & Interests</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="cv-interest-collapse">
            <article class="padding-t-10">
                <p class="txt-sm padding-b-30">Your Hobbies & Interest gives potential employers a broader view of your personality and could influence an employer's decision to hire you.</p>
                
                <?php if ( $profile_query->have_posts() ) { ?>

                    <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                        <div class="row row-10 padding-b-20 txt-color-light">
                        <?php $post_id = $post->ID; ?> 

                        <!-- Entry -->
                        <div class="col-6 col-md-3 col-lg-2 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 d-flex align-items-center justify-content-between border padding-tb-10 padding-lr-15">
                                <a data-toggle="modal" href="#myCVInterestModal-<?php echo $post_id ?>"
                                   class="txt-sm txt-color-dark txt-medium"
                                >
                                    <?php echo get_the_title() ?>                                            
                                </a>
                                <div class="txt-xxs">
                                    <span data-toggle="tooltip" data-placement="top" title="<?php echo get_post_meta( $post_id, 'description', true ); ?>">
                                        <span class="fa-stack fa-lg">
                                            <i class="fa fa-circle-o fa-stack-2x"></i>
                                            <i class="fa fa-info fa-stack-1x"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
  
                        <!-- Edit Modal -->
                        <div class="modal fade font-main filter-modal" id="myCVInterestModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="<?php echo currentUrl(false).'?'.$_SERVER['QUERY_STRING'].'&form=cv-edit-interest-'.$post_id; ?>" method="post">
                                        <div class="modal-header padding-lr-30">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit this Interest Area</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body padding-o-30 form">
                                            <?php

                                                if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-interest-'.$post_id ){
                                                    wp_delete_post($post_id); // Delete
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                                                }

                                                if( $_POST && $_GET['form'] == 'cv-edit-interest-'.$post_id ){

                                                    /* Get Post Data */
                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                    $description = wp_kses_post( $_POST['description'] );

                                                    /* Save Post to DB */
                                                    $post_id = wp_insert_post(array (
                                                        'ID' => $post_id,
                                                        'post_type' => 'interest',
                                                        'post_title' => $post_name,
                                                        'post_content' => "",
                                                        'post_status' => 'publish',
                                                    ));

                                                    /* Add Post Meta */
                                                    update_post_meta( $post_id, 'description', $description );

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                                                }
                                            ?>
                                            <div class="form-item">
                                                <label for="deadline">
                                                    Title
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="post_name" 
                                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="description">
                                                    Description
                                                </label>
                                                <textarea id="description" name="description" rows="8"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between padding-lr-30">
                                            <div>
                                                <a 
                                                    href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-interest-'.$post_id; ?>" class="btn btn-trans-bw txt-sm confirm-delete"
                                                >
                                                    Delete
                                                </a>
                                            </div>
                                            <div>
                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?> 
                    </div>
                <?php }else{ ?> 
                   
                    <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                        <?php if( get_user_meta($current_user->ID, 'cv_no_interest', true) ){ ?>
                            You don't have any hobby or interests.
                        <?php }else{ ?> 
                            You need to add a hobby or interest to proceed to the next phase.
                        <?php } ?>
                    </p>
                <?php } ?>
            </article>
            <?php if( true /*get_user_meta($current_user->ID, 'cv_no_interest', true)*/ ){ ?>
                <!--CTA-->
                <article class="padding-t-10">
                    <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddInterestModal">Add</a>
                    <?php if( !get_user_meta($current_user->ID, 'cv_no_interest', true) && $profile_query->found_posts == 0){ ?>
                        <a href="<?php echo currentUrl(false).'&form=cv-no-interest'; ?>" class="btn btn-application-manager no-m-b txt-xs">
                            I don't have an affiliation
                        </a>
                    <?php } ?>
                </article>
            <?php } ?>
        </div>
    </div>
    <!-- Add Modal -->
    <div class="modal fade font-main filter-modal" id="myCVAddInterestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(false).'?'.$_SERVER['QUERY_STRING'].'&form=cv-add-interest'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Add an Interest Area</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            $post_id = 0;
                        
                            if( $_GET['form'] == 'cv-no-interest' ){
                                
                                $value = get_user_meta($current_user->ID, 'cv_no_interest', true);
                                
                                if( $value )
                                    $value = !$value;
                                else
                                    $value = 1;
                                
                                update_user_meta( $current_user->ID, 'cv_no_interest', $value );
                                
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                            }

                            if( $_POST && $_GET['form'] == 'cv-add-interest' ){

                                /* Get Post Data */
                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                $description = wp_kses_post( $_POST['description'] );

                                /* Save Post to DB */
                                $post_id = wp_insert_post(array (
                                    'ID' => $post_id,
                                    'post_type' => 'interest',
                                    'post_title' => $post_name,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                /* Add Post Meta */
                                update_post_meta( $post_id, 'description', $description );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                            }
                        ?>
                        <div class="form-item">
                            <label for="deadline">
                                Title
                            </label>
                            <input 
                                type="text" 
                                name="post_name" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="description">
                                Description
                            </label>
                            <textarea id="description" name="description" class="editor" ><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>