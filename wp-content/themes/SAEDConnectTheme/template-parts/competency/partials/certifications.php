<?php $current_user = wp_get_current_user(); ?>
<div class="bg-white txt-color-light padding-tb-30 padding-lr-40 margin-b-10">
    <?php
        $profile_query = new WP_Query();
        $profile_query->query( 
            array(
                'post_type' => 'certification',
                'post_status' => 'publish',
                'author' => $current_user->ID,
                'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key' => 'type',
                        'value' => 'Certification',
                    )
                )
            ) 
        );
    
        if( $profile_query->found_posts > 0 || get_user_meta($current_user->ID, 'cv_no_certification', true) )
            $check[] = 'complete';
        else
            $check[] = 'incomplete';
    ?>
    <div class="profile-collapse">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-certifications-collapse" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Certifications</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="cv-certifications-collapse">
            <article class="padding-t-10">
                <p class="txt-sm padding-b-30">What is the name of your center, your training models, locations where you have centers and contact information?</p>
                <?php if ( $profile_query->have_posts() ) { ?>
                    <?php $check[] = 'complete'; ?>

                    <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                        <?php $post_id = $post->ID; ?> 

                        <!-- Entry -->
                        <div class="row row-10 padding-b-20">
                            <figure class="col-auto padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/application_manager/certification.png" alt="" width="40">
                            </figure>
                            <div class="col padding-lr-10 txt-color-light">
                                <div class="row row-10">
                                    <div class="col-md-4 padding-lr-10">
                                        <p class="txt-normal-s txt-medium"><?php echo get_the_title() ?></p>
                                        <p class="txt-sm"><?php echo get_post_meta( $post_id, 'awarding_body', true ); ?></p>
                                    </div>
                                    <div class="col-md-8 padding-lr-10">
                                        <div class="col-auto padding-lr-10">
                                            <p class="txt-normal-s">
                                                <?php 
                                                    $date = strtotime( get_post_meta( $post_id, 'date', true ) );
                                                    echo date('M Y', $date);
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="col-auto padding-lr-10 text-right txt-sm">
                                <a class="txt-green" data-toggle="modal" href="#myCVCertificationModal-<?php echo $post_id ?>">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                <a href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-certification-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                    <i class="fa fa-trash"></i>
                                    Delete
                                </a>
                            </div>
                        </div>

                        <!-- Edit Modal -->
                        <div class="modal fade font-main filter-modal" id="myCVCertificationModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="<?php echo currentUrl(false).'&form=cv-edit-certification-'.$post_id; ?>" method="post">
                                        <div class="modal-header padding-lr-30">
                                            <h5 class="modal-title" id="exampleModalLabel">CV: Edit Interest</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body padding-o-30 form">
                                            <?php

                                                if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-certification-'.$post_id ){
                                                    wp_delete_post($post_id); // Delete
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                                                }

                                                if( $_POST && $_GET['form'] == 'cv-edit-certification-'.$post_id ){

                                                    /* Get Post Data */
                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                    $type = sanitize_text_field( $_POST['type'] );
                                                    $awarding_body = sanitize_text_field( $_POST['awarding_body'] );
                                                    $date = sanitize_text_field( $_POST['date'] );
                                                    $description = wp_kses_post( $_POST['description'] );

                                                    /* Save Post to DB */
                                                    $post_id = wp_insert_post(array (
                                                        'ID' => $post_id,
                                                        'post_type' => 'certification',
                                                        'post_title' => $post_name,
                                                        'post_content' => "",
                                                        'post_status' => 'publish',
                                                    ));

                                                    /* Add Post Meta */
                                                    update_post_meta( $post_id, 'type', $type );
                                                    update_post_meta( $post_id, 'awarding_body', $awarding_body );
                                                    update_post_meta( $post_id, 'date', $date );
                                                    update_post_meta( $post_id, 'description', $description );

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                                                }
                                            ?>
                                            <div class="form-item">
                                                <label for="deadline">
                                                    Name
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="post_name" 
                                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="type">Certification or Affiliation?</label>
                                                <select name="type" id="type">
                                                    <?php $meta = get_post_meta( $post_id, 'type', true ); ?>
                                                    <option value="Certification" <?php echo ($meta == 'Certification') ? 'selected' : '' ?>>Certification</option>
                                                    <option value="Affiliation" <?php echo ($meta == 'Affiliation') ? 'selected' : '' ?>>Affiliation</option>
                                                </select>
                                            </div>  
                                            <div class="form-item">
                                                <label for="awarding_body">
                                                    Awarding Body
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="awarding_body" 
                                                    value="<?php echo get_post_meta( $post_id, 'awarding_body', true ); ?>"
                                                >
                                            </div> 
                                            <div class="form-item">
                                                <label for="date">
                                                    Date
                                                </label>
                                                <input 
                                                    type="month" 
                                                    name="date" 
                                                    value="<?php echo get_post_meta( $post_id, 'date', true ); ?>"
                                                >
                                            </div>                                        
                                            <div class="form-item">
                                                <label for="description">
                                                    Desription
                                                </label>
                                                <textarea id="description" name="description" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer padding-lr-30">
                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?> 

                <?php }else{ ?> 
                    
                    <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                        <?php if( get_user_meta($current_user->ID, 'cv_no_certification', true) ){ ?>
                            You don't have any certification.
                        <?php }else{ ?> 
                            You haven't added your certifications yet.
                        <?php } ?>
                    </p> 
                    
                <?php } ?>
            </article>
            <?php if( true /*get_user_meta($current_user->ID, 'cv_no_certification', true)*/ ){ ?>
                <!--CTA-->
                <article class="padding-t-10">
                    <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddCertificationModal">Add</a>
                    <?php if( !get_user_meta($current_user->ID, 'cv_no_certification', true) && $profile_query->found_posts == 0 ){ ?>
                        <a href="<?php echo currentUrl(false).'&form=cv-no-certification'; ?>" class="btn btn-application-manager no-m-b txt-xs">
                            I don't have an affiliation
                        </a>
                    <?php } ?>
                </article>
            <?php } ?>
        </div>
    </div>
    <!-- Add Modal -->
    <div class="modal fade font-main filter-modal" id="myCVAddCertificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(false).'&form=cv-add-certification'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Add a Certification</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            $post_id = 0;
                        
                            if( $_GET['form'] == 'cv-no-certification' ){
                                
                                $value = get_user_meta($current_user->ID, 'cv_no_certification', true);
                                
                                if( $value )
                                    $value = !$value;
                                else
                                    $value = 1;
                                
                                update_user_meta( $current_user->ID, 'cv_no_certification', $value );
                                
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                            }

                            if( $_POST && $_GET['form'] == 'cv-add-certification' ){

                                /* Get Post Data */
                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                $type = sanitize_text_field( $_POST['type'] );
                                $awarding_body = sanitize_text_field( $_POST['awarding_body'] );
                                $date = sanitize_text_field( $_POST['date'] );
                                $description = wp_kses_post( $_POST['description'] );

                                /* Save Post to DB */
                                $post_id = wp_insert_post(array (
                                    'ID' => $post_id,
                                    'post_type' => 'certification',
                                    'post_title' => $post_name,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                /* Add Post Meta */
                                update_post_meta( $post_id, 'type', $type );
                                update_post_meta( $post_id, 'awarding_body', $awarding_body );
                                update_post_meta( $post_id, 'date', $date );
                                update_post_meta( $post_id, 'description', $description );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?step='.$page_step );
                            }
                        ?>
                        <div class="form-item">
                            <label for="deadline">
                                Name
                            </label>
                            <input 
                                type="text" 
                                name="post_name" 
                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="type">Certification or Affiliation?</label>
                            <select name="type" id="type">
                                <?php $meta = get_post_meta( $post_id, 'type', true ); ?>
                                <option value="Certification" <?php echo ($meta == 'Certification') ? 'selected' : '' ?>>Certification</option>
                                <option value="Affiliation" <?php echo ($meta == 'Affiliation') ? 'selected' : '' ?>>Affiliation</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="awarding_body">
                                Awarding Body
                            </label>
                            <input 
                                type="text" 
                                name="awarding_body" 
                                value="<?php echo get_post_meta( $post_id, 'awarding_body', true ); ?>"
                            >
                        </div> 
                        <div class="form-item">
                            <label for="date">
                                Date
                            </label>
                            <input 
                                type="month" 
                                name="date" 
                                value="<?php echo get_post_meta( $post_id, 'date', true ); ?>"
                            >
                        </div>                                      
                        <div class="form-item">
                            <label for="description">
                                Desription
                            </label>
                            <textarea id="description" name="description" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>