<?php $current_user = wp_get_current_user(); ?>
<div class="bg-white txt-color-light padding-tb-30 padding-lr-40 margin-b-10">
    <?php
        $profile_query = new WP_Query();
        $profile_query->query( 
            array(
                'post_type' => 'education',
                'post_status' => 'publish',
                'author' => $current_user->ID,
                'posts_per_page' => -1,
            ) 
        );

    ?>
    <div class="profile-collapse">
        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#cv-education-collapse" aria-expanded="<?php echo ($isOpen) ? 'true' : 'false'; ?>" aria-controls="collapseExample">
            <span class="txt-medium txt-color-yellow">Tell us about your education</span>
        </button>
        <div class="collapse <?php echo ($isOpen) ? 'show hideInMobile' : ''; ?>" id="cv-education-collapse">
            <article class="padding-t-30">
                <?php if ( $profile_query->have_posts() ) { ?>

                    <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                        <?php $post_id = $post->ID; ?> 

                        <!-- Entry -->
                        <div class="row row-10 padding-b-20">
                            <figure class="col-auto padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/application_manager/education.png" alt="" width="40">
                            </figure>
                            <div class="col padding-lr-10 txt-color-light">
                                <p class="txt-normal-s txt-medium">
                                    <?php echo get_post_meta( $post_id, 'school', true ); ?>
                                    <span class="padding-l-10">(
                                        <?php 
                                            $date = strtotime( get_post_meta( $post_id, 'start_date', true ) );
                                            echo date('M Y', $date);

                                            echo ' - ';

                                            $date = strtotime( get_post_meta( $post_id, 'end_date', true ) );
                                            echo date('M Y', $date);
                                        ?>
                                        )
                                    </span>
                                </p>
                                <p class="txt-sm">
                                    <?php echo get_the_title() ?> |
                                    <?php echo get_post_meta( $post_id, 'field_of_study', true ); ?>
                                </p>
                            </div>
                            <div class="col-auto padding-lr-10 text-right txt-sm">
                                <a class="txt-green" data-toggle="modal" href="#myCVEditEducatiionModal-<?php echo $post_id ?>">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                <a href="<?php echo currentUrl(false).'&action=delete&form=cv-edit-education-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                    <i class="fa fa-trash"></i>
                                    Delete
                                </a>
                            </div>
                        </div>

                        <!-- Edit Modal -->
                        <div class="modal fade font-main filter-modal" id="myCVEditEducatiionModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="<?php echo currentUrl(false).'&form=cv-edit-education-'.$post_id; ?>" method="post">
                                        <div class="modal-header padding-lr-30">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Education</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body padding-o-30 form">
                                            <?php

                                                if( $_GET['action'] == 'delete' && $_GET['form'] == 'cv-edit-education-'.$post_id ){
                                                    wp_delete_post($post_id); // Delete
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                }

                                                if( $_POST && $_GET['form'] == 'cv-edit-education-'.$post_id ){

                                                    /* Get Post Data */
                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                    $school = sanitize_text_field( $_POST['school'] ); 
                                                    $field_of_study = sanitize_text_field( $_POST['field_of_study'] ); 
                                                    $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                                    $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                                    $achievements = sanitize_text_field( $_POST['achievements'] ); 

                                                    /* Save Post to DB */
                                                    $post_id = wp_insert_post(array (
                                                        'ID' => $post_id,
                                                        'post_type' => 'education',
                                                        'post_title' => $post_name,
                                                        'post_content' => "",
                                                        'post_status' => 'publish',
                                                    ));

                                                    /* Add Post Meta */
                                                    update_post_meta( $post_id, 'school', $school );
                                                    update_post_meta( $post_id, 'field_of_study', $field_of_study );
                                                    update_post_meta( $post_id, 'start_date', $start_date );
                                                    update_post_meta( $post_id, 'end_date', $end_date );
                                                    update_post_meta( $post_id, 'achievements', $achievements );

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                                                }
                                            ?>
                                            <div class="form-item">
                                                <label for="deadline">
                                                    Degree Attained (e.g BA, BS, JD, PhD)
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="post_name" 
                                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="school">
                                                    School
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="school" 
                                                    value="<?php echo get_post_meta( $post_id, 'school', true ); ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="field_of_study">
                                                    Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="field_of_study" 
                                                    value="<?php echo get_post_meta( $post_id, 'field_of_study', true ); ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="start_date">
                                                    Start Month/Year
                                                </label>
                                                <input 
                                                    type="month" 
                                                    name="start_date" 
                                                    value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                                                >
                                            </div>
                                            <div class="form-item">
                                                <label for="end_date">
                                                    End Month/Year (Current students: Enter your expected graduation year)
                                                </label>
                                                <input 
                                                    type="month" 
                                                    name="end_date" 
                                                    value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                                                >
                                            </div>
                                            <!--<div class="form-item">
                                                <label for="achievements">
                                                    Achievements
                                                </label>
                                                <input 
                                                    type="text" 
                                                    name="achievements" 
                                                    value="<?php echo get_post_meta( $post_id, 'achievements', true ); ?>"
                                                >
                                            </div>-->
                                        </div>
                                        <div class="modal-footer padding-lr-30">
                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?> 

                <?php }else{ ?> 

                    <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                        You need to complete your education and experience before you can proceed
                    </p>
                    
                <?php } ?>
            </article>
            <!--CTA-->
            <article class="padding-t-10">
                <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#myCVAddEducatiionModal">Add New</a>
            </article>
        </div>
    </div>
    <!-- Add Modal -->
    <div class="modal fade font-main filter-modal" id="myCVAddEducatiionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="<?php echo currentUrl(false).'&form=cv-add-education'; ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel">Add Education</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            $post_id = 0;

                            if( $_POST && $_GET['form'] == 'cv-add-education' ){

                                /* Get Post Data */
                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                $school = sanitize_text_field( $_POST['school'] ); 
                                $field_of_study = sanitize_text_field( $_POST['field_of_study'] ); 
                                $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                $achievements = sanitize_text_field( $_POST['achievements'] ); 


                                /* Save Post to DB */
                                $post_id = wp_insert_post(array (
                                    'ID' => $post_id,
                                    'post_type' => 'education',
                                    'post_title' => $post_name,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                /* Add Post Meta */
                                update_post_meta( $post_id, 'school', $school );
                                update_post_meta( $post_id, 'field_of_study', $field_of_study );
                                update_post_meta( $post_id, 'start_date', $start_date );
                                update_post_meta( $post_id, 'end_date', $end_date );
                                update_post_meta( $post_id, 'achievements', $achievements );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=apply' );
                            }
                        ?>
                        <div class="form-item">
                            <label for="deadline">
                                Degree Attained (e.g BA, BS, JD, PhD)
                            </label>
                            <input 
                                type="text" 
                                name="post_name" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="school">
                                School
                            </label>
                            <input 
                                type="text" 
                                name="school" 
                                value="<?php echo get_post_meta( $post_id, 'school', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="field_of_study">
                                Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)
                            </label>
                            <input 
                                type="text" 
                                name="field_of_study" 
                                value="<?php echo get_post_meta( $post_id, 'field_of_study', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="start_date">
                                Start Month/Year
                            </label>
                            <input 
                                type="month" 
                                name="start_date" 
                                value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="end_date">
                                End Month/Year (Current students: Enter your expected graduation year)
                            </label>
                            <input 
                                type="month" 
                                name="end_date" 
                                value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                            >
                        </div>
                        <!--<div class="form-item">
                            <label for="achievements">
                                Achievements
                            </label>
                            <input 
                                type="text" 
                                name="achievements" 
                                value="<?php echo get_post_meta( $post_id, 'achievements', true ); ?>"
                            >
                        </div>-->
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>