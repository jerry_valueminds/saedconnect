<?php 
    /* Get relationship ID */
    global $relashionship_id;
    global $relationship_parent_id;

    $connected = new WP_Query( array(
        'relationship' => array(
            'id'   => $relashionship_id,
            'from' => $relationship_parent_id, // You can pass object ID or full object
        ),
        'nopaging'     => true,
    ) );
    while ( $connected->have_posts() ) : $connected->the_post();
        ?>

         <?php
            $content_type = rwmb_get_value( 'select-content-type' );
            if($content_type == 'gi-text-image-button-meta-box'){ ?>
               
                <?php
                    $images = rwmb_meta( 'tib-feature-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                ?>
                
                <div class="cta-block fluid padding-lr-40 border-b-1 border-color-darkgrey">
                    <article>
                        <figure class="margin-b-20">
                            <img class="rounded-corners" src="<?php echo $image['full_url']; ?>" alt="">
                        </figure>
                        <h2 class="txt-lg txt-bold txt-color-green margin-b-20">
                            <?php the_title(); ?>
                        </h2>
                        <article class="margin-b-20">
                            <article class="txt-normal-s">
                                <?php echo rwmb_get_value( 'tib-text-content' ); ?>
                            </article>
                        </article>
                        <div class="btn-wrapper">
                            <a class="btn btn-trans-gg txt-sm" href="<?php echo rwmb_get_value( 'tib-button-url' ); ?>">
                                <?php echo rwmb_get_value( 'tib-button-text' ); ?>
                            </a>
                        </div>
                    </article>
                </div>

            <?php } elseif($content_type == 'gi-text-video-button-meta-box'){ ?> 

                <?php
                    $images = rwmb_meta( 'tvb-feature-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                ?>
                
                <div class="cta-block fluid padding-lr-40 border-b-1 border-color-darkgrey">
                    <article>
                        <figure class="margin-b-20">
                            <a class="feature-video-block popup-video rounded-corners" href="<?php echo rwmb_get_value( 'tvb-video-url' ); ?>" style="background-image:url('<?php echo $image['full_url']; ?>');min-height:150px;">
                                <div class="play-btn">
                                    <i class="fa fa-play"></i>
                                </div>      
                            </a>
                        </figure>
                        <h2 class="txt-lg txt-bold txt-color-green margin-b-20">
                            <?php the_title(); ?>
                        </h2>
                        <article class="margin-b-20">
                            <article class="txt-normal-s">
                                <?php echo rwmb_get_value( 'tvb-text-content' ); ?>
                            </article>
                        </article>
                        <div class="btn-wrapper">
                            <a class="btn btn-trans-gg txt-sm" href="<?php echo rwmb_get_value( 'tvb-button-url' ); ?>">
                                <?php echo rwmb_get_value( 'tvb-button-text' ); ?>
                            </a>
                        </div>
                    </article>
                </div>   

        <?php } ?>

<?php
    endwhile;
    wp_reset_postdata();
?>