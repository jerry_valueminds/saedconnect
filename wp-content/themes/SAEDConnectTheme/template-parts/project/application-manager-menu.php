<?php if( $current_user->ID == $post_author_id && $rendered_view == 'application-manager' ){ ?>

<nav class="job-sub-nav bg-ghostwhite">
    <div>
        <ul class="container-wrapper">
            <li>
                <a href="<?php echo $post_link ?>" >
                    Overview                         
                 </a>
            </li>
            <?php foreach($manager_views_array as $manager_view_item){ ?>
            <li>
                <a href="<?php echo $post_link.'/?view=application-manager&manager-view='.$manager_view_item['slug']; ?>" class="<?php echo ( $manager_view == $manager_view_item['slug'] ) ? "active" : "" ?>">
                    <?php echo $manager_view_item['name']; ?>
                    <span class="count">
                        <?php 
                            $applicant_count = $application_db->get_var( "SELECT COUNT(*) FROM ".$table." WHERE post_id=".$post_id." AND status='".$manager_view_item['slug']."' LIMIT 0,10" );
                            echo $applicant_count;
                        ?>
                    </span>                           
                 </a>
            </li>
            <?php } ?>
        </ul>
    </div>
</nav>

<?php } ?>