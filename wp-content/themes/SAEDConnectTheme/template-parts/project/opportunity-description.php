<?php if( $current_user->ID != $post_author_id && $rendered_view != 'application-manager' && $rendered_view == '' ){ ?>
   
<section class="padding-t-30 padding-b-80">
    <div>
        <!-- Video -->
        <?php if(get_post_meta( $post_id, 'video-link', true )){ ?>
            <div class="margin-b-20">
                <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                    <a class="popup-video padding-r-20" href="<?php echo get_post_meta( $post_id, 'video-link', true ) ?>">
                        <span class="txt-color-white">
                            <i class="fa fa-play-circle padding-r-5"></i>
                            Program Video
                        </span>
                    </a>
                </h1>
            </div>
        <?php } ?>
        <!-- Description -->
        <div class="margin-b-20">
            <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                <span>
                    Program Description
                </span>
            </h1>
            <article class="text-box txt-height-1-7 txt-sm padding-o-15 border-o-1 border-color-darkgrey">
                <?php 
                    echo ( get_post_meta( $post_id, 'description', true ) ) ? get_post_meta( $post_id, 'description', true ) : '<span>Description is unvailable.</span>'
                ?>
            </article>
        </div>
        
        <!-- Fees -->
        <div>
            <?php  
                $paymentType = get_post_meta( $post_id, 'payment-type', true );
                $paymentFee = get_post_meta( $post_id, 'payment-fee', true );
            ?>

            <?php if( $paymentType == 'free' ){ ?>
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            <span class="txt-medium">
                                Program Fees:
                            </span>
                            <span>
                                Free
                            </span>
                        </span>
                    </h1>
                </div>
            <?php } elseif( $paymentType == 'fee' ) { ?>
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            <span class="txt-medium">
                                Program Fees:
                            </span>
                            <span>
                                ₦<?php echo $paymentFee ?>
                            </span>
                        </span>
                    </h1>
                </div>
            <?php } elseif( $paymentType == 'package' ) { ?>
                <div class="">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            <span class="txt-medium">
                                Payment Packages
                            </span>
                        </span>
                    </h1>
                </div>
                <?php 
                    $unserialized_data = get_post_meta( $post_id, 'package', false );
                    $package_counter = 0;
                ?>
                   
                <?php if( $unserialized_data ){ ?>
                    
                    <?php foreach ($unserialized_data as $serialized_package){ ?>
                    
                        <?php 
                            $package = maybe_unserialize( $serialized_package );
                            $package_counter++;
                        ?>
                        
                        <div class="padding-o-15 border-o-1 border-color-darkgrey">
                            <p class="txt-normal-s">
                                <span class="row">
                                    <span class="col-8">
                                        <span class="">
                                            <?php echo $package['payment-package-name'] ?>:
                                        </span>
                                        <span class="txt-bold">
                                            <?php echo ( $package['payment-package-type'] == 'fee' ) ? "₦".$package['payment-package-fee'] : "Free" ?>
                                        </span>
                                    </span>
                                    <span class="col-4 text-right">
                                        <a class="txt-color-blue" data-toggle="collapse" href="#package-<?php echo $package_counter; ?>" aria-expanded="false">
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </span>
                                </span>
                            </p>
                            <div id="package-<?php echo $package_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                <div class="row">
                                    <article class="text-box txt-sm txt-height-1-7 padding-t-20">
                                        <?php echo $package['payment-package-description'] ?>
                                    </article>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                    
                <?php }else{ ?>
                
                    <div class="padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                        <h3 class="txt-normal-s txt-height-1-4">
                            No Payment Packages available currently.
                        </h3>
                    </div>
                
                <?php } ?>       
            <?php } else { ?>
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            <span class="txt-medium">
                                Program Method
                            </span>
                        </span>
                    </h1>
                </div>
                <div>
                    <h3 class="txt-bold txt-height-1-4 margin-b-20">
                        This opportunity does not have a Payment Plan
                    </h3>
                </div>
            <?php } ?>
        </div>
        
        <!-- FAQ -->
        <div class="margin-b-20">
            <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                <span>
                    Frequently Asked Questions
                </span>
            </h1>
            <div class="margin-b-20">
                
                <?php 
                    $unserialized_data = get_post_meta( $post_id, 'faq', false );
                    $faq_counter = 0;
                ?>
                   
                <?php if( $unserialized_data ){ ?>
                    
                    <?php foreach ($unserialized_data as $serialized_faq){ ?>
                    
                        <?php 
                            $faq = maybe_unserialize( $serialized_faq );
                            $faq_counter++;
                        ?>
                        
                        <div class="padding-o-15 border-o-1 border-color-darkgrey">
                            <p class="txt-normal-s">
                                <span class="row">
                                    <span class="col-8 txt-bold">
                                        <?php echo $faq['question'] ?>
                                    </span>
                                    <span class="col-4 text-right">
                                        <a class="txt-color-blue" data-toggle="collapse" href="#faq-<?php echo $faq_counter; ?>" aria-expanded="false">
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </span>
                                </span>
                            </p>
                            <div id="faq-<?php echo $faq_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                <div class="row">
                                    <article class="text-box txt-sm txt-height-1-7 padding-t-20">
                                        <?php echo $faq['answer'] ?>
                                    </article>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                    
                <?php }else{ ?>
                
                    <div class="padding-o-15 border-o-1 border-color-darkgrey">
                        <h3 class="txt-height-1-4">
                            No FAQs available.
                        </h3>
                    </div>
                
                <?php } ?>
            </div>
        </div>
        
        <!-- Location -->
        <div class="margin-b-20">
            <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                <span>
                    This Program is Available in:
                </span>
            </h1>
            <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                <p class="txt-bold">
                    <?php 
                        $term_list = wp_get_post_terms($post_id, 'state', array("fields" => "names"));

                        if( $term_list ){
                            foreach( $term_list as $key => $term_name ){
                                echo $term_name;
                                echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                            }
                        } else {
                            echo "Locations are unvailable";
                        }
                    ?>
                </p>
            </div>
        </div>
    </div>
</section>

<?php } elseif( $current_user->ID == $post_author_id && $rendered_view != 'application-manager' ) { ?>

<section class="padding-t-30 padding-b-80">
    <div>
        <!-- Video -->
        <div class="margin-b-20">
            <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                <a class="popup-video padding-r-20" href="<?php echo get_post_meta( $post_id, 'video-link', true ) ?>">
                    <span class="txt-color-white">
                        <i class="fa fa-play-circle padding-r-5"></i>
                        Program Video
                    </span>
                </a>
                <a class="txt-color-white txt-sm" data-toggle="modal" href="#opportunityVideoModal">
                    + Add / Edit
                </a>
            </h1>
        </div>
        
        <!-- Description -->
        <div class="margin-b-20">
            <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                <span>
                    Program Description
                </span>
                <a class="txt-color-white txt-sm" data-toggle="modal" href="#descriptionModal">
                    + Add / Edit
                </a>
            </h1>
            <article class="text-box sm txt-height-1-7 padding-o-15 border-o-1 border-color-darkgrey">
                <?php echo get_post_meta( $post_id, 'description', true ) ?>
            </article>
        </div>
        
        <!-- Fees -->
        <div>
            <?php  
                $paymentType = get_post_meta( $post_id, 'payment-type', true );
                $paymentFee = get_post_meta( $post_id, 'payment-fee', true );
            ?>

            <?php if( $paymentType == 'free' ){ ?>
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            <span class="txt-medium">
                                Program Fees:
                            </span>
                            <span>
                                Free
                            </span>
                        </span>
                        <a class="txt-color-white txt-sm" data-toggle="modal" href="#FeesModal">
                            + Change Payment Type
                        </a>
                    </h1>
                </div>
            <?php } elseif( $paymentType == 'fee' ) { ?>
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            <span class="txt-medium">
                                Program Fees:
                            </span>
                            <span>
                                ₦<?php echo $paymentFee ?>
                            </span>
                        </span>
                        <a class="txt-color-white txt-sm" data-toggle="modal" href="#FeesModal">
                            + Change Payment Type
                        </a>
                    </h1>
                </div>
            <?php } elseif( $paymentType == 'package' ) { ?>
                <div class="">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            <span class="txt-medium">
                                Payment Packages
                            </span>
                        </span>
                        <a class="txt-color-white txt-sm" data-toggle="modal" href="#FeesModal">
                            + Change Payment Type
                        </a>
                    </h1>
                </div>
                <?php 
                    $unserialized_data = get_post_meta( $post_id, 'package', false );
                    $package_counter = 0;
                ?>
                   
                <?php if( $unserialized_data ){ ?>
                    
                    <?php foreach ($unserialized_data as $serialized_package){ ?>
                    
                        <?php 
                            $package = maybe_unserialize( $serialized_package );
                            $package_counter++;
                        ?>
                        
                        <div class="padding-o-15 border-o-1 border-color-darkgrey">
                            <p class="txt-normal-s">
                                <span class="row">
                                    <span class="col-8">
                                        <span class="">
                                            <?php echo $package['payment-package-name'] ?>:
                                        </span>
                                        <span class="txt-bold">
                                            <?php echo ( $package['payment-package-type'] == 'fee' ) ? "₦".$package['payment-package-fee'] : "Free" ?>
                                        </span>
                                    </span>
                                    <span class="col-4 text-right">
                                        <a class="txt-color-green padding-r-15" data-toggle="modal" href="#editPaymentPackageModal-<?php echo $package_counter ?>">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        <a href="<?php echo currentUrl(true).'?view=form-delete-payment-package-'.$package_counter; ?>" class="confirm-delete txt-color-red padding-r-15">
                                            <i class="fa fa-trash"></i>
                                            Delete
                                        </a>
                                        <a class="txt-color-blue" data-toggle="collapse" href="#package-<?php echo $package_counter; ?>" aria-expanded="false">
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </span>
                                </span>
                            </p>
                            <div id="package-<?php echo $package_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                <div class="row">
                                    <article class="text-box txt-sm txt-height-1-7 padding-t-20">
                                        <?php echo $package['payment-package-description'] ?>
                                    </article>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Edit Modal -->
                        <div class="modal fade font-main filter-modal" id="editPaymentPackageModal-<?php echo $package_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentPackageModal" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="<?php echo currentUrl(true).'?view=form-edit-payment-package-'.$package_counter; ?>" method="post">
                                        <div class="modal-header padding-lr-30">
                                            <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment Package</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body padding-o-30">
                                            <?php
                    
                                                if($_GET['view'] == 'form-delete-payment-package-'.$package_counter ){
                                                    delete_post_meta($post_id, 'package', $serialized_package);
                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                }
                                                /* Add/Edit Job (Specification) */

                                                /* Get Post data */

                                                $unserialized_data = $package;

                                                /*
                                                *
                                                * Save / Retrieve Form Data
                                                *
                                                */
                                                if($_POST && $rendered_view == 'form-edit-payment-package-'.$package_counter){

                                                    $serialized_data = maybe_serialize($_POST);
                                                    echo "Current";
                                                    echo "<br>";
                                                    print_r($serialized_data);

                                                    update_post_meta( $post_id, 'package', $serialized_data, $serialized_package);

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                }
                                            ?>

                                            <div class="form margin-b-40">
                                                <!-- Name -->
                                                <div class="form-item">
                                                    <label for="">
                                                        Name
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="payment-package-name" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo $unserialized_data['payment-package-name'] ?>"
                                                    >
                                                </div>

                                                <!-- Description -->
                                                <div class="form-item">
                                                    <label for="">
                                                        Description
                                                    </label>
                                                    <textarea class="editor" name="payment-package-description" id="" cols="30" rows="10"><?php echo $unserialized_data['payment-package-description'] ?></textarea>
                                                </div>

                                                <!-- Payment Type -->
                                                <div class="form-item">
                                                    <label for="">
                                                        Payment Method
                                                    </label>
                                                    <select name="payment-package-type" required>
                                                        <option value="free" <?php echo ($unserialized_data['payment-package-type'] == 'free')? "selected" : "" ?>>
                                                            This package is Free
                                                        </option>
                                                        <option value="fee" <?php echo ($unserialized_data['payment-package-type'] == 'fee')? "selected" : "" ?>>
                                                            Interested candidates pay a fee
                                                        </option>
                                                    </select>
                                                </div>

                                                <!-- free -->
                                                <!--<div class="" >
                                                    <p class="txt-normal-s txt-medium">
                                                        Applicants will not be required to pay a fee for this package.
                                                    </p>
                                                </div>-->

                                                <!-- package Fee -->
                                                <div class="form-item">
                                                    <label for="">Enter the fee for this package in NGN.</label>
                                                    <input 
                                                        type="number" 
                                                        name="payment-package-fee" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo $unserialized_data['payment-package-fee'] ?>"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer padding-lr-30">
                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                    
                <?php }else{ ?>
                
                    <div class="padding-o-15 border-o-1 border-color-darkgrey">
                        <h3 class="txt-normal-s txt-height-1-4">
                            You have not added any payment packages. Click the <span class="txt-bold">Add Payment Package</span> to  add a package.
                        </h3>
                    </div>
                
                <?php } ?>
                <p class="padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                    <a class="btn btn-blue txt-xs no-m-b" data-toggle="modal" href="#AddPaymentPackageModal">
                        Add Payment Package
                    </a>
                </p>         
            <?php } else { ?>
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            <span class="txt-medium">
                                Program Method
                            </span>
                        </span>
                        <a class="btn btn-white txt-xs no-m-b" data-toggle="modal" href="#FeesModal">
                            Add
                        </a>
                    </h1>
                </div>
                <div>
                    <h3 class="txt-bold txt-height-1-4 margin-b-10">
                        You have not selected a Payment Type for this opportunity. You must select a Payment Type before you can publish this opportunity.
                    </h3>
                </div>
            <?php } ?>
        </div>
        
        <!-- FAQ -->
        <div class="margin-b-20">
            <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                <span>
                    Frequently Asked Questions
                </span>
                <a class="txt-color-white txt-sm" data-toggle="modal" href="#FAQModal">
                    + Add FAQ
                </a>
            </h1>
            <div class="margin-b-20">
                
                <?php 
                    $unserialized_data = get_post_meta( $post_id, 'faq', false );
                    $faq_counter = 0;
                ?>
                   
                <?php if( $unserialized_data ){ ?>
                    
                    <?php foreach ($unserialized_data as $serialized_faq){ ?>
                    
                        <?php 
                            $faq = maybe_unserialize( $serialized_faq );
                            $faq_counter++;
                        ?>
                        
                        <div class="padding-o-15 border-o-1 border-color-darkgrey">
                            <p class="txt-normal-s">
                                <span class="row">
                                    <span class="col-8 txt-bold">
                                        <?php echo $faq['question'] ?>
                                    </span>
                                    <span class="col-4 text-right">
                                        <a class="txt-color-green padding-r-15" data-toggle="modal" href="#editPaymentfaqModal-<?php echo $faq_counter ?>">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        <a href="<?php echo currentUrl(true).'?view=form-delete-payment-faq-'.$faq_counter; ?>" class="confirm-delete txt-color-red padding-r-15">
                                            <i class="fa fa-trash"></i>
                                            Delete
                                        </a>
                                        <a class="txt-color-blue" data-toggle="collapse" href="#faq-<?php echo $faq_counter; ?>" aria-expanded="false">
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </span>
                                </span>
                            </p>
                            <div id="faq-<?php echo $faq_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                <div class="row">
                                    <article class="text-box txt-sm txt-height-1-7 padding-t-20">
                                        <?php echo $faq['answer'] ?>
                                    </article>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Edit Modal -->
                        <div class="modal fade font-main filter-modal" id="editPaymentfaqModal-<?php echo $faq_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentfaqModal" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="<?php echo currentUrl(true).'?view=form-edit-payment-faq-'.$faq_counter; ?>" method="post">
                                        <div class="modal-header padding-lr-30">
                                            <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment faq</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body padding-o-30">
                                            <?php
                    
                                                if($_GET['view'] == 'form-delete-payment-faq-'.$faq_counter ){
                                                    delete_post_meta($post_id, 'faq', $serialized_faq);
                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                }
                                                /* Add/Edit Job (Specification) */

                                                /* Get Post data */

                                                $unserialized_data = $faq;

                                                /*
                                                *
                                                * Save / Retrieve Form Data
                                                *
                                                */
                                                if($_POST && $rendered_view == 'form-edit-payment-faq-'.$faq_counter){

                                                    $serialized_data = maybe_serialize($_POST);
                                                    echo "Current";
                                                    echo "<br>";
                                                    print_r($serialized_data);

                                                    update_post_meta( $post_id, 'faq', $serialized_data, $serialized_faq);

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                }
                                            ?>

                                            <div class="form margin-b-40">
                
                                                <!-- Title -->
                                                <div class="form-item">
                                                    <label for="question">
                                                        Question
                                                    </label>
                                                    <input 
                                                        type="text" 
                                                        name="question" 
                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                        value="<?php echo $unserialized_data['question'] ?>"
                                                    >
                                                </div>

                                                <!-- Deadline -->
                                                <div class="form-item">
                                                    <label for="answer">
                                                        Answer
                                                    </label>
                                                    <textarea class="editor" name="answer" id="" cols="30" rows="8"><?php echo $unserialized_data['answer'] ?></textarea>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="modal-footer padding-lr-30">
                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                    
                <?php }else{ ?>
                
                    <div class="padding-o-15 border-o-1 border-color-darkgrey">
                        <h3 class="txt-height-1-4">
                            You have not added any FAQs. Click the <span class="txt-bold">Add FAQ Button</span> to add one.
                        </h3>
                    </div>
                
                <?php } ?>
            </div>
        </div>
        
        <!-- Location -->
        <div class="margin-b-20">
            <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                <span>
                    This Program is Available in:
                </span>
                <a class="txt-color-white txt-sm" data-toggle="modal" href="#locationModal">
                    + Add / Edit
                </a>
            </h1>
            <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                <p class="txt-bold">
                    <?php 
                        $term_list = wp_get_post_terms($post_id, 'state', array("fields" => "names"));

                        if( $term_list ){
                            foreach( $term_list as $key => $term_name ){
                                echo $term_name;
                                echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                            }
                        } 
                    ?>
                </p>
            </div>
        </div>
        
        <div class="margin-b-20">
            <a 
               class="dropdown-toggle txt-medium txt-color-dark d-flex justify-content-between align-items-center border-o-2 padding-o-15"
               data-toggle="modal" href="#applicationMethodModal"
            >
                    How do you want to recieve Applications?
            </a>
        </div>
        <div class="margin-b-20">
            <?php  
                $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                $howToApply = get_post_meta( $post_id, 'how-to-apply', true );
            ?>

            <?php if( $applicationMethod == 'manual' ){ ?>
            <div>
                <h3 class="txt-bold txt-height-1-4 margin-b-10">
                    Manual Application Instructions
                </h3>
                <article class="text-box txt-normal-s txt-height-1-5 margin-b-20">
                    <?php echo $howToApply ?>
                </article>
            </div>
            <?php } elseif( $applicationMethod == '3rd-party' ) { ?>
                <div>
                    <h3 class="txt-bold txt-height-1-4 margin-b-10">
                        3rd Party Site
                    </h3>
                    <p class="txt-sm margin-b-10">
                        When applicants click the Apply Button on this job, they will be redirected to this site:
                    </p>
                    <p class="txt-sm margin-b-20">
                        <a href="<?php echo $applicationLink ?>" class="txt-bold txt-color-blue" target="_blank"><?php echo $applicationLink ?></a>
                    </p>
                </div>
            <?php } elseif( $applicationMethod == 'saedconnect' ) { ?>
                <div>
                    <h3 class="txt-bold txt-height-1-4 margin-b-10">
                        Recieve Applications through SAEDConnect
                    </h3>
                    <p class="txt-sm margin-b-20">
                        Letting SAEDconnect handle the application process gives you access to the Application Manager. The Application gives you access to a robust set of tools, crafted to give you control and allow you seamlessly manage applications.
                    </p>
                    <p class="txt-sm margin-b-20">
                        Selecting this option requires paying a fee.
                    </p>
                    <p>
                        <a href="" class="btn btn-blue txt-sm">
                            Subscribe Now
                        </a>
                    </p>
                </div>             
            <?php } else { ?>
                <div>
                    <h3 class="txt-bold txt-height-1-4 margin-b-10">
                        You have not selected an Application for this job. You must select an Application Method before you can publish this job.
                    </h3>
                </div>
            <?php } ?>
        </div>

        <!-- Application Requirements -->
        <div class="margin-b-20">
            <a 
               class="dropdown-toggle txt-medium txt-color-dark d-flex justify-content-between align-items-center border-o-2 padding-o-15"
               data-toggle="modal" href="#applicationReqirementModal"
            >
                Select Application Requirements
            </a>
        </div>
    </div>
</section>

<?php } ?>