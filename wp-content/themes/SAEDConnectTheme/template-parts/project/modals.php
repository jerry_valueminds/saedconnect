<?php if( $current_user->ID == $post_author_id ) { ?>





<!-- *********************************************************************** -->

<!-- Specification -->
<div class="modal fade font-main filter-modal" id="specificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-specification'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Specifications</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Meta Key */
                        $tax_types = array(
                            array(
                                'name' => 'Program Type',
                                'slug' => 'program-type',
                                'hierachical' => true,
                            ),
                            array(
                                'name' => 'Industries',
                                'slug' => 'industry',
                                'hierachical' => false,
                            ),
                        );

                        $redirect_link = currentUrl(true);

                        $postName = $post->post_title;
                        $deadline = get_post_meta( $post->ID, 'deadline', true );

                        /* Publish / Unpublish & Return */
                        if($_GET['action'] == 'publication'){
                            $publication_key = 'publication_status';
                            /* Meta value to save */
                            $value = "user_published";

                            /* Get saved meta */
                            $saved_meta = get_post_meta( $post_id, $publication_key, true );

                            if ( $saved_meta ) //If published, Unpublish
                                delete_post_meta( $post_id, $publication_key );
                            else //If Unpublished, Publish
                                update_post_meta( $post_id, $publication_key, $value );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-specification'){

                            /* Get Post Name */
                            $postName = $_POST['post-name'];
                            $deadline = sanitize_text_field( $_POST['deadline'] ); 

                            /* Save Post to DB */
                            $post_id = wp_insert_post(array (
                                'ID' => $post_id,
                                'post_type' => $post_type,
                                'post_title' => $postName,
                                'post_content' => "",
                                'post_status' => 'publish',
                            ));

                            update_post_meta( $post_id, 'deadline', $deadline );

                            /* Save terms to post */
                            foreach($tax_types as $tax_type){
                                wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug']);
                            }

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <!-- Title -->
                    <div class="form-item">
                        <label for="deadline">
                            Project / Goal Title
                        </label>
                        <input 
                            type="text" 
                            name="post-name" 
                            class="d-block padding-tb-5 padding-lr-10 full-width"
                            value="<?php echo $postName ?>"
                        >
                    </div>

                    <!-- Terms -->
                    <div class="txt-normal-s">
                    <?php foreach($tax_types as $tax_type){ ?>

                        <?php if( $tax_type['hierachical']){ ?>

                            <!-- Tax Title -->
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_type['name']; ?>
                            </h2>

                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <?php if( $parent == 0 ){ ?>

                                    <div class="padding-b-20">
                                        <div class="txt-medium txt-color-dark margin-b-15">
                                            <?php echo $term_name; ?>
                                        </div>
                                        <?php
                                            foreach ($terms as $child_term) {
                                                // Check and see if the term is a top-level parent. If so, display it.
                                                $child_parent = $child_term->parent;
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name

                                                if( $child_parent == $term_id ){
                                        ?>
                                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                    <input
                                                        class="margin-r-5"
                                                        type="checkbox" 
                                                        value="<?php echo $child_term_id ?>" 
                                                        name="<?php echo $tax_type['slug'] ?>[]" 
                                                        <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                    >
                                                    <span class="bg-label padding-l-5">
                                                        <?php echo $child_term_name; ?>
                                                    </span>
                                                </label>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </div>

                                <?php } ?>

                            <?php } ?>

                        <?php } else { ?>

                            <!-- Tax Title -->
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_type['name']; ?>
                            </h2>

                            <div class="padding-b-20">
                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                    <input
                                        class="margin-r-5"
                                        type="checkbox" 
                                        value="<?php echo $term_id ?>" 
                                        name="<?php echo $tax_type['slug'] ?>[]" 
                                        <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                    >
                                    <span class="bg-label padding-l-5">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>

                            <?php } ?>
                            </div>
                        <?php } ?>

                    <?php } ?>
                    </div>

                    <!-- Deadline -->
                    <div class="form-item">
                        <label for="deadline">
                            Expiry Date
                        </label>
                        <input type="date" name="deadline" value="<?php echo $deadline ?>">
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Image -->
<div class="modal fade font-main filter-modal" id="featuredImageModal" tabindex="-1" role="dialog" aria-labelledby="opportunityVideoModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="featured_upload" method="post" action="<?php echo currentUrl(true).'?view=form-featured-image'; ?>" enctype="multipart/form-data">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Featured Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <div class="image-upload-card">
                        <?php
                            $images = get_attached_media( 'image', $post_id );

                             if($images){

                                foreach($images as $image) { 
                                    $previousImg_id = $image->ID;
                                }
                            }

                            if( $_POST && $rendered_view == 'form-featured-image'){


                                // Check that the nonce is valid, and the user can edit this post.
                                if ( 
                                    isset( $_POST['my_image_upload_nonce'], $_POST['post_id'] ) 
                                    && wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
                                    && current_user_can( 'edit_post', $_POST['post_id'] )
                                ) {
                                    // The nonce was valid and the user has the capabilities, it is safe to continue.

                                    // These files need to be included as dependencies when on the front end.
                                    require_once( ABSPATH . 'wp-admin/includes/image.php' );
                                    require_once( ABSPATH . 'wp-admin/includes/file.php' );
                                    require_once( ABSPATH . 'wp-admin/includes/media.php' );

                                    /* Delete prevoius image */
                                    $_POST['previous_img'] = $previousImg_id;
                                    if($previousImg_id){
                                        wp_delete_attachment( $previousImg_id );
                                    }

                                    // Let WordPress handle the upload.
                                    // Remember, 'my_image_upload' is the name of our file input in our form above.
                                    $attachment_id = media_handle_upload( 'my_image_upload', $_POST['post_id'] );

                                    if ( is_wp_error( $attachment_id ) ) {
                                        // There was an error uploading the image.
                                    } else {
                                        // The image was uploaded successfully!
                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                    }

                                } else {

                                    // The security check failed, maybe show the user an error.
                                }
                            }
                        ?>
                    
                        <input type="file" name="my_image_upload" id="my_image_upload" class="upload-field" multiple="false">
                        <input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id ?>">
                        <input type="hidden" name="previous_img" id="post_id" value="<?php echo $previousImg_id ?>">
                        <?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
                        
                        <div class="text-align-center bg-darkgrey txt-xlg text-center txt-light" id="featuredImagePreview">
                            Click here to select an image from your device
                        </div>
                     </div>   
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input class="btn btn-blue txt-sm padding-lr-15" id="submit_my_image_upload" name="submit_my_image_upload" type="submit" value="Upload">
                </div>
                <script>
                    function readURL(input) {
                      if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function(e) {
                            $('#featuredImagePreview').css('background-image', 'url(' + e.target.result + ')').text("");
                        }

                        reader.readAsDataURL(input.files[0]);
                      }
                    }

                    $("#my_image_upload").change(function() {
                        readURL(this);
                    });
                </script>
                <style>
                    .image-upload-card{
                        position: relative;
                    }
                    
                    .image-upload-card .upload-field{
                        position: absolute;
                        width: 100%;
                        top: 0;
                        left: 0;
                        right: 0;
                        bottom: 0;
                        opacity: 0;
                        z-index: 100;
                        cursor: pointer;
                    }
                    
                    .image-upload-card #featuredImagePreview {
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        height: 400px;
                        padding: 15px;
                    }
                </style>
            </form>
        </div>
    </div>
</div>

<!-- Video Link -->
<div class="modal fade font-main filter-modal" id="opportunityVideoModal" tabindex="-1" role="dialog" aria-labelledby="opportunityVideoModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-video'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Video Link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $videoLink = get_post_meta( $post_id, 'video-link', true );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-video'){

                            /* Get Post Name */
                            $videoLink = sanitize_text_field( $_POST['video-link'] );

                            update_post_meta( $post_id, 'video-link', $videoLink );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Video Link -->
                        <div class="form-item">
                            <label for="post-name">
                                YouTube Video Link
                            </label>
                            <input 
                                type="url" 
                                name="video-link" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo $videoLink ?>"
                                required
                            >
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Description -->
<div class="modal fade font-main filter-modal" id="descriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-description'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $summary = get_post_meta( $post->ID, 'summary', true );
                        $description = get_post_meta( $post->ID, 'description', true );


                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-description'){

                            /* Get Post Name */
                            $summary = sanitize_text_field( $_POST['summary'] );
                            $description = wp_kses_post( $_POST['description'] );

                            /* Update Meta */
                            update_post_meta( $post_id, 'summary', $summary );
                            update_post_meta( $post_id, 'description', $description );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Summary -->
                        <div class="form-item">
                            <label for="post-name">
                                Summary
                            </label>
                            <textarea name="summary" cols="30" rows="4"><?php echo $summary ?></textarea>
                        </div>
                        <!-- Description -->
                        <div class="form-item">
                            <label for="post-name">
                                Description
                            </label>
                            <textarea class="editor" name="description" cols="30" rows="20"><?php echo $description ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Fee -->
<div class="modal fade font-main filter-modal" id="FeesModal" tabindex="-1" role="dialog" aria-labelledby="FeesModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-payment-type'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Fees</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $paymentType = get_post_meta( $post_id, 'payment-type', true );
                        $paymentFee = get_post_meta( $post_id, 'payment-fee', true );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-payment-type'){

                            /* Get Post Name */
                            $paymentType = sanitize_text_field( $_POST['payment-type'] );
                            $paymentFee = sanitize_text_field( $_POST['payment-fee'] );

                            update_post_meta( $post_id, 'payment-type', $paymentType );
                            update_post_meta( $post_id, 'payment-fee', $paymentFee );


                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Job Type -->
                        <div class="form-item">
                            <select name="payment-type" required>
                                <option value="free" <?php echo ($paymentType == 'free')? "selected" : "" ?>>
                                    This program is Free
                                </option>
                                <option value="fee" <?php echo ($paymentType == 'fee')? "selected" : "" ?>>
                                    Interested candidates pay a fee
                                </option>
                                <option value="package" <?php echo ($paymentType == 'package')? "selected" : "" ?>>
                                    There are different Fee Packages
                                </option>
                            </select>
                        </div>
                        
                        <!-- Manual Application -->
                        <div class="conditional" data-condition="['free'].includes(payment-type)">
                            <p class="txt-normal-s txt-medium">
                                Applicants will not be required to pay any fee to apply for this Program.
                            </p>
                        </div>
                        <!-- 3rd Party -->
                        <div class="conditional" data-condition="['fee'].includes(payment-type)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Enter the fee for this program in NGN.
                            </p>
                            <div class="form-item">
                                <input 
                                    type="number" 
                                    name="payment-fee" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $paymentFee ?>"
                                >
                            </div>
                        </div>
                        <!-- SAEDConnect -->
                        <div class="conditional" data-condition="['package'].includes(payment-type)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Selecting this option allows you to create and add multiple payment packages to this Program.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Payment Package -->
<div class="modal fade font-main filter-modal" id="AddPaymentPackageModal" tabindex="-1" role="dialog" aria-labelledby="AddPaymentPackageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-payment-package'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment Package</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-payment-package'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'package', $serialized_data, false);
                            
                            $unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Name -->
                        <div class="form-item">
                            <label for="">
                                Name
                            </label>
                            <input 
                                type="text" 
                                name="payment-package-name" 
                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                value="<?php echo $unserialized_data['payment-package-name'] ?>"
                            >
                        </div>
                        
                        <!-- Description -->
                        <div class="form-item">
                            <label for="">
                                Description
                            </label>
                            <textarea class="editor" name="payment-package-description" id="" cols="30" rows="10"><?php echo $unserialized_data['payment-package-description'] ?></textarea>
                        </div>
                        
                        <!-- Payment Type -->
                        <div class="form-item">
                            <label for="">
                                Payment Method
                            </label>
                            <select name="payment-package-type" required>
                                <option value="free" <?php echo ($unserialized_data['payment-package-type'] == 'free')? "selected" : "" ?>>
                                    This package is Free
                                </option>
                                <option value="fee" <?php echo ($unserialized_data['payment-package-type'] == 'fee')? "selected" : "" ?>>
                                    Interested candidates pay a fee
                                </option>
                            </select>
                        </div>
                        
                        <!-- free -->
                        <div class="conditional" data-condition="['free'].includes(payment-package-type)">
                            <p class="txt-normal-s txt-medium">
                                Applicants will not be required to pay a fee for this package.
                            </p>
                        </div>
                        
                        <!-- package Fee -->
                        <div class="conditional" data-condition="['fee'].includes(payment-package-type)">
                            <div class="form-item">
                                <label for="">Enter the fee for this package in NGN.</label>
                                <input 
                                    type="number" 
                                    name="payment-package-fee" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $unserialized_data['payment-package-fee'] ?>"
                                >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- FAQ -->
<div class="modal fade font-main filter-modal" id="FAQModal" tabindex="-1" role="dialog" aria-labelledby="FAQModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-faq'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit FAQ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                                                  
                        //$unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'package', true ) );

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-faq'){
                            
                            $serialized_data = maybe_serialize($_POST);
                                                        
                            //update_post_meta( $post_id, 'package', $serialized_data );
                            
                            add_post_meta($post_id, 'faq', $serialized_data, false);
                            
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <!-- Title -->
                    <div class="form-item">
                        <label for="question">
                            Question
                        </label>
                        <input 
                            type="text" 
                            name="question" 
                            class="d-block padding-tb-5 padding-lr-10 full-width"
                            value="<?php echo $question ?>"
                        >
                    </div>

                    <!-- Deadline -->
                    <div class="form-item">
                        <label for="answer">
                            Answer
                        </label>
                        <textarea class="editor" name="answer" id="" cols="30" rows="8"><?php echo $answer ?></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Location -->
<div class="modal fade font-main filter-modal" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-location'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Location</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Meta Key */
                        $tax_types = array(
                            array(
                                'name' => 'Location',
                                'slug' => 'state',
                                'hierachical' => false,
                            ),
                        );

                        $redirect_link = currentUrl(true);

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */

                        if(isset($_POST['submit']) && $rendered_view == 'form-location'){

                            /* Return Terms assigned to Post and remove all terms */
                            foreach($tax_types as $tax_type){
                                
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids"));
                                
                                foreach($term_list as $remove_id){
                                    wp_remove_object_terms( $post_id, $remove_id, $tax_type['slug'] );
                                }
                            }
                            
                            /* Save terms to post */
                            foreach($tax_types as $tax_type){
                                wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug']);
                            }

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>

                    <!-- Terms -->
                    <div class="txt-normal-s">
                    <?php foreach($tax_types as $tax_type){ ?>

                        <?php if( $tax_type['hierachical']){ ?>

                            <!-- Tax Title -->
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_type['name']; ?>
                            </h2>

                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <?php if( $parent == 0 ){ ?>

                                    <div class="padding-b-20">
                                        <div class="txt-medium txt-color-dark margin-b-15">
                                            <?php echo $term_name; ?>
                                        </div>
                                        <?php
                                            foreach ($terms as $child_term) {
                                                // Check and see if the term is a top-level parent. If so, display it.
                                                $child_parent = $child_term->parent;
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name

                                                if( $child_parent == $term_id ){
                                        ?>
                                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                    <input
                                                        class="margin-r-5"
                                                        type="checkbox" 
                                                        value="<?php echo $child_term_id ?>" 
                                                        name="<?php echo $tax_type['slug'] ?>[]" 
                                                        <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                    >
                                                    <span class="bg-label padding-l-5">
                                                        <?php echo $child_term_name; ?>
                                                    </span>
                                                </label>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </div>

                                <?php } ?>

                            <?php } ?>

                        <?php } else { ?>

                            <!-- Tax Title -->
                            <h2 class="txt-medium txt-color-blue margin-b-20">
                                Select <?php echo $tax_type['name']; ?>
                            </h2>

                            <div class="padding-b-20">
                            <?php 
                                /* Return Terms assigned to Post */
                                $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                    <input
                                        class="margin-r-5"
                                        type="checkbox" 
                                        value="<?php echo $term_id ?>" 
                                        name="<?php echo $tax_type['slug'] ?>[]" 
                                        <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                    >
                                    <span class="bg-label padding-l-5">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>

                            <?php } ?>
                            </div>
                        <?php } ?>

                    <?php } ?>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" value="Submit" name="submit"  class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Application Method -->
<div class="modal fade font-main filter-modal" id="applicationMethodModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-application-method'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Select Application Method</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <?php
                        /* Add/Edit Job (Specification) */

                        /* Get Post data */
                        $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                        $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                        $howToApply = get_post_meta( $post_id, 'how-to-apply', true );


                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST && $rendered_view == 'form-application-method'){

                            /* Get Post Name */
                            $applicationMethod = sanitize_text_field( $_POST['application-method'] );
                            $applicationLink = sanitize_text_field( $_POST['3rd-party-application-link'] );
                            $howToApply = wp_kses_post( $_POST['how-to-apply'] );

                            update_post_meta( $post_id, 'application-method', $applicationMethod );
                            update_post_meta( $post_id, '3rd-party-application-link', $applicationLink );
                            update_post_meta( $post_id, 'how-to-apply', $howToApply );


                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                        }
                    ?>

                    <div class="form margin-b-40">
                        <!-- Job Type -->
                        <div class="form-item">
                            <select name="application-method" required>
                                <option value="manual" <?php echo ($applicationMethod == 'manual')? "selected" : "" ?>>
                                    Manual Application
                                </option>
                                <option value="3rd-party" <?php echo ($applicationMethod == '3rd-party')? "selected" : "" ?>>
                                    3rd Party Site
                                </option>
                                <option value="saedconnect" <?php echo ($applicationMethod == 'saedconnect')? "selected" : "" ?>>
                                    Recieve Applications through SAEDConnect
                                </option>
                            </select>
                        </div>
                        
                        <!-- Manual Application -->
                        <div class="conditional" data-condition="['manual'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Enter detailed instructions on how applicants can submit their application for this job.
                            </p>
                            <div class="form-item">
                                <textarea class="editor" name="how-to-apply" id="" cols="30" rows="8"><?php echo $howToApply ?></textarea>
                            </div>
                        </div>
                        <!-- 3rd Party -->
                        <div class="conditional" data-condition="['3rd-party'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Enter the external application link for this job.
                            </p>
                            <div class="form-item">
                                <input 
                                    type="url" 
                                    name="3rd-party-application-link" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $applicationLink ?>"
                                >
                            </div>
                        </div>
                        <!-- SAEDConnect -->
                        <div class="conditional" data-condition="['saedconnect'].includes(application-method)">
                            <p class="txt-normal-s txt-medium margin-b-20">
                                Letting SAEDconnect handle the application process gives you access to the Application Manager. The Application gives you access to a robust set of tools, crafted to give you control and allow you seamlessly manage job applications.
                            </p>
                            <p class="txt-normal-s txt-medium margin-b-20">
                                 Selecting this option requires paying a fee.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Application Requirements -->
<div class="modal fade font-main filter-modal" id="applicationReqirementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(true).'?view=form-application-requirements'; ?>" method="post">
                <?php
                    /* Add/Edit Job (Specification) */

                    /* Get Post data */
                    /*$jobSummary = get_post_meta( $post->ID, 'job-summary', true );
                    $jobDescription = get_post_meta( $post->ID, 'job-description', true );
                    $paymentType = get_post_meta( $post->ID, 'payment-type', true );
                    $offerAmount = get_post_meta( $post->ID, 'offer-amount', true );
                    $offerAmountConfidential = get_post_meta( $post->ID, 'offer-amount-confidential', true );
                    $commissionAmount = get_post_meta( $post->ID, 'commission-amount', true );
                    $commissionAmountConfidential = get_post_meta( $post->ID, 'commission-amount-confidential', true );*/

                    /*
                    *
                    * Save / Retrieve Form Data
                    *
                    */
                    if($_POST && $rendered_view == 'form-application-requirements'){

                        /* Get Post Name */
                        $cv_requirement = $_POST['cv_requirement'];
                        $mentorship_profile_requirement = $_POST['mentorship_profile_requirement'];
                        $serialized_data = maybe_serialize($_POST['mentorship-capabilities']);

                        /* Update Meta */
                        update_post_meta( $post_id, 'cv_requirement', $cv_requirement ); //CV Requirement
                        update_post_meta( $post_id, 'mentorship_profile_requirement', $mentorship_profile_requirement ); //mentor Profile Requirement
                        update_post_meta( $post_id, 'mentorship-capabilities', $serialized_data ); //Business Mentor Capabilities

                        /* Redirect */
                        printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                    }
                ?>

                <?php 
                    $unserialized_data = maybe_unserialize( get_post_meta( $post->ID, 'mentorship-capabilities', true ) );

                    /* Return Terms assigned to Post */
                    //$term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                    /*
                    *
                    * Populate Form Data from Terms
                    *
                    */
                ?>
                <div class="modal-header padding-lr-40">
                    <h5 class="modal-title" id="exampleModalLabel">Select Application Requirements</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-40">
                    <div class="accordion" id="jobRequiremenetAccordion">
                        
                        <!-- CV -->
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementCV" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    CV
                                </a>
                            </div>
                            <div id="applicationReqirementCV" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                            <?php $meta = get_post_meta( $post_id, 'cv_requirement', true ); ?>
                               
                            <div class="border-o-1 border-color-darkgrey padding-o-15">
                                <div class="d-flex justify-content-between align-items-center">
                                    <span class="d-inline-flex align-items-center padding-r-40">
                                        <input 
                                            type="checkbox" 
                                            id="cv_requirement" 
                                            name="cv_requirement" 
                                            <?php echo ( $meta ) ? "checked" : "" ?>
                                        >
                                        <label 
                                            for="cv_requirement" 
                                            class="txt-sm padding-l-15"
                                        >
                                            Required
                                        </label>
                                    </span>
                                    <a 
                                        class="txt-sm txt-color-blue"
                                        data-toggle="collapse" 
                                        href="#collapseBusinessMentorRequirement" aria-expanded="false"
                                    >
                                        View Details 
                                        <i class="fa fa-angle-down padding-l-5"></i>
                                    </a>
                                </div>
                                <div id="collapseBusinessMentorRequirement" class="collapse" aria-labelledby="headingOne">
                                    <article class="text-box txt-sm padding-t-10">
                                        <p>
                                            Each applicant must complete their mentor profile before they can apply. 
                                        </p>
                                    </article>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <!-- Mentor Profile -->
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementMentor" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Mentor Profile
                                </a>
                            </div>
                            <div id="applicationReqirementMentor" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                            <?php $meta = get_post_meta( $post_id, 'mentorship_profile_requirement', true ); ?>
                               
                            <div class="border-o-1 border-color-darkgrey padding-o-15">
                                <div class="d-flex justify-content-between align-items-center">
                                    <span class="d-inline-flex align-items-center padding-r-40">
                                        <input 
                                            type="checkbox" 
                                            id="mentorship_profile_requirement" 
                                            name="mentorship_profile_requirement" 
                                            <?php echo ( $meta ) ? "checked" : "" ?>
                                        >
                                        <label 
                                            for="mentorship_profile_requirement" 
                                            class="txt-sm padding-l-15"
                                        >
                                            Required
                                        </label>
                                    </span>
                                    <a 
                                        class="txt-sm txt-color-blue"
                                        data-toggle="collapse" 
                                        href="#collapseBusinessMentorRequirement" aria-expanded="false"
                                    >
                                        View Details 
                                        <i class="fa fa-angle-down padding-l-5"></i>
                                    </a>
                                </div>
                                <div id="collapseBusinessMentorRequirement" class="collapse" aria-labelledby="headingOne">
                                    <article class="text-box txt-sm padding-t-10">
                                        <p>
                                            Each applicant must complete their mentor profile before they can apply. 
                                        </p>
                                    </article>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <!-- Business Mentor Capabilities -->
                        <div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementBusinessMentorCapability" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Business Mentor Capabilities
                                </a>
                            </div>
                            <div id="applicationReqirementBusinessMentorCapability" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                            <?php 
                                switch_to_blog(109);
                                $mentor_skill_terms = get_terms( 'business-mentorship', array('hide_empty' => false));
                                restore_current_blog();
                            ?>
                               
                            <?php foreach( $mentor_skill_terms as $mentor_skill_term ){ ?>
                                <?php if( $mentor_skill_term->parent == 0 ){ ?>
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="<?php echo $mentor_skill_term->term_id ?>" 
                                                name="mentorship-capabilities[]" 
                                                value="<?php echo $mentor_skill_term->term_id ?>"
                                                <?php echo in_array($mentor_skill_term->term_id, $unserialized_data) ? "checked" : "" ?>
                                            >
                                            <label 
                                                for="<?php echo $mentor_skill_term->term_id ?>" 
                                                class="txt-sm padding-l-15"
                                            >
                                                <?php echo $mentor_skill_term->name ?>
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-<?php echo $mentor_skill_term->term_id ?>" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-<?php echo $mentor_skill_term->term_id ?>" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in this skill. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <?php } ?>
                            <?php } ?>
                            </div>
                        </div>
                        
                        <!-- Business -->
                        <!--<div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementBusiness" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Business
                                </a>
                            </div>
                            <div id="applicationReqirementBusiness" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-8" 
                                                name="job-requirements[]" 
                                                value="business"
                                            >
                                            <label 
                                                for="job-requirements-7" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Must have a Business
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-8" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-8" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <!-- Project -->
                        <!--<div class="padding-b-10">
                            <div class="txt-normal-s txt-medium">
                                <a 
                                   class="d-flex align-items-center justify-content-between bg-grey txt-normal-s  padding-o-10" 
                                   data-toggle="collapse" 
                                   href="#applicationReqirementProject" aria-expanded="false" aria-controls="collapseOne"
                                >
                                    Project
                                </a>
                            </div>
                            <div id="applicationReqirementProject" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-inline-flex align-items-center padding-r-40">
                                            <input 
                                                type="checkbox" 
                                                id="job-requirements-9" 
                                                name="job-requirements[]" 
                                                value="capabilities"
                                            >
                                            <label 
                                                for="job-requirements-9" 
                                                class="txt-sm padding-l-15"
                                            >
                                                Must have created a Project
                                            </label>
                                        </span>
                                        <a 
                                            class="txt-sm txt-color-blue"
                                            data-toggle="collapse" 
                                            href="#collapseTaskRequirement-9" aria-expanded="false"
                                        >
                                            View Details 
                                            <i class="fa fa-angle-down padding-l-5"></i>
                                        </a>
                                    </div>
                                    <div id="collapseTaskRequirement-9" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-sm padding-t-10">
                                            <p>
                                                Each applicant must desmonstrate competence in developing a WordPress site. 
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="modal-footer padding-lr-40">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>

<?php } ?>

<!-- How to Apply -->
<div class="modal fade font-main filter-modal" id="howToApplyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header padding-lr-30">
                <h5 class="modal-title" id="exampleModalLabel">How to Apply</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body padding-o-30">
                <?php $howToApply = get_post_meta( $post_id, 'how-to-apply', true ); ?>
                <article class="text-box txt-normal-s txt-height-1-5 margin-b-20">
                    <?php echo $howToApply ?>
                </article>
            </div>
            <div class="modal-footer padding-lr-30">
                <button type="button" class="btn btn-blue txt-xs" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade font-main filter-modal" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="AddPaymentoutcomeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php 
                if($_POST && $rendered_view == 'form-subscribe'){
                    $fullName = sanitize_text_field( $_POST['full-name'] );
                    $email = sanitize_text_field( $_POST['email'] );
                    $phone = sanitize_text_field( $_POST['phone'] );
                    $state = sanitize_text_field( $_POST['state'] );
                    $jobFunction ="";
                    $capabilities ="";
                    
                    /* Create application status*/
                    $application_db->insert( 
                        $table, 
                        array( 
                            "post_id" => $post_id,
                            "post_type" => $post_type,
                            "user_id" => $current_user->ID,
                            "status" => "external",
                            "name" => $fullName,
                            "email" => $email,
                            "location" => $state,
                            "phone" => $phone,
                            "job_function" => $jobFunction,
                            "job_capabilities" => $capabilities,

                        ), 
                        array( "%d", "%s", "%d", "%s", "%s", "%s", "%s", "%s", "%s", "%s" ) 
                    );
                    
                    /* Redirect */
                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                }
            ?>
            
            <form class="signup-form" method="post" action="<?php echo currentUrl(true).'?view=form-subscribe'; ?>">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Apply for <?php echo $post_title; ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30">
                    <div class="form margin-b-40">
                        <!-- Name -->
                        <div class="form-item">
                            <label for="name">
                                Name
                            </label>
                            <input type="text" name="full-name" id="full-name">
                        </div>
                        <!-- Email -->
                        <div class="form-item">
                            <label for="email">
                                Email
                            </label>
                            <input type="email" name="email" id="email" value="<?php echo $current_user->user_email ?>">
                        </div>
                        <!-- Phone -->
                        <div class="form-item">
                            <label for="phone">
                                Phone
                            </label>
                            <input type="telephone" name="phone" id="phone">
                        </div>
                        <!-- State -->
                        <div class="form-item">
                            <label for="state">
                                State of Residence
                            </label>
                            <select name="state" id="state">
                                <option value="" selected disabled hidden>- Select -</option>
                                <option value="Abuja FCT">Abuja FCT</option>
                                <option value="Abia">Abia</option>
                                <option value="Adamawa">Adamawa</option>
                                <option value="Akwa Ibom">Akwa Ibom</option>
                                <option value="Anambra">Anambra</option>
                                <option value="Bauchi">Bauchi</option>
                                <option value="Bayelsa">Bayelsa</option>
                                <option value="Benue">Benue</option>
                                <option value="Borno">Borno</option>
                                <option value="Cross River">Cross River</option>
                                <option value="Delta">Delta</option>
                                <option value="Ebonyi">Ebonyi</option>
                                <option value="Edo">Edo</option>
                                <option value="Ekiti">Ekiti</option>
                                <option value="Enugu">Enugu</option>
                                <option value="Gombe">Gombe</option>
                                <option value="Imo">Imo</option>
                                <option value="Jigawa">Jigawa</option>
                                <option value="Kaduna">Kaduna</option>
                                <option value="Kano">Kano</option>
                                <option value="Katsina">Katsina</option>
                                <option value="Kebbi">Kebbi</option>
                                <option value="Kogi">Kogi</option>
                                <option value="Kwara">Kwara</option>
                                <option value="Lagos">Lagos</option>
                                <option value="Nassarawa">Nassarawa</option>
                                <option value="Niger">Niger</option>
                                <option value="Ogun">Ogun</option>
                                <option value="Ondo">Ondo</option>
                                <option value="Osun">Osun</option>
                                <option value="Oyo">Oyo</option>
                                <option value="Plateau">Plateau</option>
                                <option value="Rivers">Rivers</option>
                                <option value="Sokoto">Sokoto</option>
                                <option value="Taraba">Taraba</option>
                                <option value="Yobe">Yobe</option>
                                <option value="Zamfara">Zamfara</option>
                                <option value="Outside Nigeria">Outside Nigeria</option>
                            </select>
                        </div>
                        <!-- Program Name -->
                        <input type="text" name="post-name" id="post-name" value="<?php echo $post_title; ?>" hidden>

                        <div class="status"></div>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" id="submit-btn" class="submit-btn btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
            <script>
                $(document).ready(function() {

                });
            </script>
        </div>
    </div>
</div>
