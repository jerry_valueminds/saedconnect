<?php $bundles = get_post_meta($post_id, 'bundle', false); ?>

    
<?php switch_to_blog(110); ?>

<?php 
    /* Get all assigned bundles */

    foreach( $bundles as $bundle_id ){
        $bundle_url = get_post_meta($bundle_id, 'info-page-url', true);
        $bundle_summary = get_post_meta($bundle_id, 'summary', true);
        $bundle_bg_color = get_post_meta($bundle_id, 'bg-color', true);
        $bundle_alt_color = get_post_meta($bundle_id, 'alt-color', true);
?>
  
    <div class="container-wrapper" 
        style="background-color: <?php echo $bundle_bg_color ?>; color: <?php echo $bundle_alt_color ?>;"
    >
        <div class="padding-tb-15">
            <div class="row row-10 align-items-center">
                <p class="col-8 padding-lr-10">
                    This Opportunity is part of the
                    <span class="txt-bold">
                        <?php echo get_the_title($bundle_id) ?>
                    </span>
                </p>
                <!--<p><?php echo $bundle_summary ?></p>-->
                <p class="col-4 padding-lr-10 text-right txt-medum">
                    <a href="<?php echo $bundle_url ?>" target="_blank" style="color: <?php echo $bundle_alt_color ?>;">
                        Learn More
                        <i class="fa fa-long-arrow-right"></i>
                    </a>
                </p>
            </div>
        </div>
    </div>
   
<?php
    }

?>

<?php restore_current_blog(); ?>