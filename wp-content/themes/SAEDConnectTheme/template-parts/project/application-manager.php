<?php if( $current_user->ID == $post_author_id && $rendered_view == 'application-manager' ){ ?>
<?php 
    /*
        //VARIABLES USED ON THIS COMPONENT
        $application_entries
        $manager_view
        $table
        
        $$$$$$$$$
    */
?>
<section class="padding-t-20 padding-b-80">
    <div class="container-wrapper">
        <?php 
            /* Get Application Entries */
            $application_entries = $application_db->get_results("SELECT user_id, status FROM ".$table." WHERE post_id = ".$post_id." AND status='".$manager_view."' LIMIT 0,10");
                                                                                            
            if( $application_entries ){
        ?>
        
        <div class="row row-20">
            <div class="col-md-4 padding-lr-20">
                <div class="">
                    <form class="search-form margin-r-5 margin-b-10">
                        <input type="text" placeholder="Search applicants">
                        <button>
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                    <div class="">
                        <a class="padding-o-15 bg-ash txt-normal-s no-m-b d-block" data-toggle="modal" href="#filterModal">
                            <span class="txt-color-white">
                                <i class="fa fa-sort-amount-desc padding-r-5"></i>
                                FIlter Applicants
                            </span>
                        </a>
                    </div>
                </div>
                <ul class="applicants-list">
                <?php foreach( $application_entries as $application_entry ){ ?>
                   
                <?php
                    /* Get Avatar */
                    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                    $meta_key = 'user_avatar_url';
                    $get_avatar_url = get_user_meta($application_entry->user_id, $meta_key, true);

                    if($get_avatar_url){
                        $avatar_url = $get_avatar_url;
                    }

                        
                    /* Get User Display Name */
                    switch_to_blog(1);

                    $gf_id = 4; //Form ID
                    $username_entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $application_entry->user_id, //Current logged in user
                            )
                        )
                    );

                    /* Get Entries */
                    $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* Get GF Entry Count */
                    $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                    if($username_entry_count){ //If no entry
                        $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                        $email = rgar( $username_entries[0], 2 );
                        $phone = rgar( $username_entries[0], 7 );
                    }   

                    restore_current_blog();
                        
                        
                    /* Get User Bio */
                    switch_to_blog(109);

                    $gf_id = 84; //Form ID
                    $username_entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $application_entry->user_id, //Current logged in user
                            )
                        )
                    );

                    /* Get Entries */
                    $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* Get GF Entry Count */
                    $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                    if($username_entry_count){ //If no entry
                        
                        $gender = rgar( $username_entries[0], 2 );
                        $dob = rgar( $username_entries[0], 3 );
                        $nationality = rgar( $username_entries[0], 4 );
                        $location = rgar( $username_entries[0], 6 );
                    }   

                    restore_current_blog();
                ?>
                    <li
                        data-id="<?php echo $application_entry->user_id ?>"
                        data-name="<?php echo $displayname ?>"
                        data-gender="<?php echo $gender ?>"  
                        data-dob="<?php echo $dob ?>"  
                        data-email="<?php echo $email ?>"  
                        data-nationality="<?php echo $nationality ?>"  
                        data-location="<?php echo $location ?>"  
                        data-phone="<?php echo $phone ?>"
                        data-qualification="<?php echo $qualification ?>"
                        data-avatar="<?php echo $avatar_url ?>"
                        data-coverletter = "<?php echo ($cover_letter)? $cover_letter : "No content available" ?>"
                        data-video="<?php echo $video_cv ?>"

                    >
                        <figure class="avatar">

                        </figure>
                        <div class="info txt-medium">
                            <div class="name txt-lg margin-b-5">

                            </div>
                            <div class="qualification txt-normal-s txt-light">

                            </div>
                        </div>
                    </li>
                <?php } ?>  
                </ul>
            </div>
            <div class="col-md-8 padding-lr-20 applicant-info">
                <div class="d-flex align-items-center flex-wrap bg-yellow padding-tb-10 padding-lr-15 margin-b-5">
                    <div class="txt-normal-s txt-bold padding-r-20">
                       Move To
                    </div>
                <?php foreach($manager_views_array as $manager_view_item){ ?>
                   <?php if( $manager_view_item['slug'] != $manager_view ){ ?>
                        <a 
                           href="<?php echo currentUrl(false).'&move='.$manager_view_item['slug'].'&user-id=0'; ?>" 
                           class="action-btn btn btn-white txt-xs no-m-b margin-r-10"
                        >
                            <?php echo $manager_view_item['name']; ?>                           
                         </a>
                    <?php } ?>
                <?php } ?>
                </div>
                
                <!-- Info -->
                <div class="margin-b-5">
                    <h2>
                        <a class=" d-flex align-items-center justify-content-between bg-ash txt-medium  padding-o-15" data-toggle="collapse" href="#collapseInfo" role="button" aria-expanded="true" aria-controls="collapseExample">
                            <span class="txt-color-white">
                                Applicant Details
                            </span>
                            <span class="dropdown-toggle txt-color-white"></span>
                        </a>
                    </h2>
                    <div class="collapse show" id="collapseInfo">
                        <article class=" txt-normal-s">
                            <div class="row border-o-1 border-color-darkgrey padding-o-15">
                                <figure class="col-2 profile-image">
                                    <a class="popup-image" href="images/people/onewoman.png">
                                        <img src="images/people/onewoman.png" alt="">     
                                    </a>
                                </figure>
                                <div class="col-8 padding-l-40">
                                    <p class=" d-flex padding-b-20" id="name">
                                        <span class="value txt-xlg"></span>
                                    </p>
                                    <div class="txt-color-dark">
                                        <a class="popup-video padding-r-20" href="#">
                                            <i class="fa fa-play-circle"></i>
                                            Video CV    
                                        </a>
                                        <a href="">
                                            <i class="fa fa-envelope-o"></i>
                                            Mail Applicant
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <p class="d-flex border-o-1 border-color-darkgrey padding-o-15" id="gender">
                                <span class="col-2 txt-medium txt-dark">
                                    Gender:
                                </span>
                                <span class="padding-l-10 col txt-color-light value">

                                </span>
                            </p>
                            <p class="d-flex border-o-1 border-color-darkgrey padding-o-15" id="dob">
                                <span class="col-2 txt-medium txt-dark">
                                    Year of Birth:
                                </span>
                                <span class="padding-l-10 col txt-color-light value">

                                </span>
                            </p>
                            <p class="d-flex border-o-1 border-color-darkgrey padding-o-15" id="email">
                                <span class="col-2 txt-medium txt-dark" id="nationality">
                                    Email:
                                </span>
                                <span class="padding-l-10 col txt-color-light value">

                                </span>
                            </p>
                            <p class="d-flex border-o-1 border-color-darkgrey padding-o-15" id="location">
                                <span class="col-2 txt-medium txt-dark">
                                    Location:
                                </span>
                                <span class="padding-l-10 col txt-color-light value">

                                </span>
                            </p>
                            <p class="d-flex border-o-1 border-color-darkgrey padding-o-15" id="phone">
                                <span class="col-2 txt-medium txt-dark">
                                    Phone:
                                </span>
                                <span class="padding-l-10 col txt-color-light value">

                                </span>
                            </p>
                        </article>
                    </div>
                </div>

                <!-- Cover Letter -->
                <div class="margin-b-5">
                    <h2>
                        <a class=" d-flex align-items-center justify-content-between bg-ash txt-medium  padding-o-15" data-toggle="collapse" href="#collapseCoverLetter" role="button" aria-expanded="true" aria-controls="collapseExample">
                            <span class="txt-color-white">
                                Cover Letter
                            </span>
                            <span class="dropdown-toggle txt-color-white"></span>
                        </a>
                    </h2>
                    <div class="collapse show" id="collapseCoverLetter">
                        <div id="coverletter">
                            <article class="text-box txt-sm value border-o-1 border-color-darkgrey padding-o-15">

                            </article>
                        </div>

                    </div>
                </div>

                <!-- Tasks -->
                <div class="margin-b-5">
                    <h2>
                        <a class=" d-flex align-items-center justify-content-between bg-ash txt-medium  padding-o-15" data-toggle="collapse" href="#collapsetasks" role="button" aria-expanded="true" aria-controls="collapseExample">
                            <span class="txt-color-white">
                                Completed Tasks
                            </span>
                            <span class="dropdown-toggle txt-color-white"></span>
                        </a>
                    </h2>
                    <div class="collapse show" id="collapsetasks">
                        <div id="tasks">
                            <article class="txt-sm value">
                                <p class="d-flex border-o-1 border-color-darkgrey padding-o-15">
                                    <span class="task-number">
                                        1
                                    </span>
                                    <span class="col-auto padding-l-15">
                                        Build a responsive HTML/CSS webpage
                                    </span>
                                </p>
                                <p class="d-flex border-o-1 border-color-darkgrey padding-o-15">
                                    <span class="task-number">
                                        2
                                    </span>
                                    <span class="col-auto padding-l-15">
                                        Build a responsive HTML/CSS webpage
                                    </span>
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php } else { ?>
        
        <h2 class="txt-lg padding-t-40">
            No Applicants in this category.
        </h2>
        
        <?php } ?>
    </div>
</section>

<script>
    function updateQueryStringParameter(uri, key, value) {
      var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
      var separator = uri.indexOf('?') !== -1 ? "&" : "?";
      if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
      }
      else {
        return uri + separator + key + "=" + value;
      }
    }  
</script>

<script>
    $(document).ready(function() {

        /* Applicants Info: On Page Load, Populate with first applicant on list */
        $(".applicants-list li").first().addClass('active');

        $( '.applicant-info .profile-image img' ).attr('src', $(".applicants-list li").data('avatar'));
        $( '.applicant-info .profile-image .popup-image' ).attr('href', $(".applicants-list li").data('avatar'));
        $( '.applicant-info .popup-video' ).attr('href', $(".applicants-list li").data('video'));

        /* Action Buttons */
        $.each($(".action-btn"), function(key, value) {
            $( '.applicant-info #'+key+' .value' ).html( value );


            var url = $(this).attr('href');
            var id = "user-id";
            var value = $(".applicants-list li").data('id');

            var updatesURI = updateQueryStringParameter(url, id, value);

            $(this).attr('href', updatesURI );
        });



        /*  */
        $.each($(".applicants-list li").data(), function(key, value) {
            $( '.applicant-info #'+key+' .value' ).html( value );
        });

        $(".applicant-info").fadeIn();

        /* Applicants list: Fill each item with data from data attribute */
        $(".applicants-list li").each(function(index, value){
            var name = $(this).data('name');
            var qualification = $(this).data('qualification');
            var avatarUrl = $(this).data('avatar');

            $(this).find('.name').text(name); //Name
            $(this).find('.qualification').text(qualification); //Qualification
            $(this).find('.avatar').css('background-image', 'url(' + avatarUrl + ')'); //Avatar Image                
        });

        /* Applicants List: When an applicant is clicked */
        $(".applicants-list li").click(function(){
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            var thisApplicant = $(this);

            $( '.applicant-info .profile-image img' ).attr('src', $(this).data('avatar'));
            $( '.applicant-info .profile-image .popup-image' ).attr('href', $(this).data('avatar'));
            $( '.applicant-info .popup-video' ).attr('href', $(this).data('video'));

            /* Action Buttons */
            $.each($(".action-btn"), function(key, value) {

                var url = $(this).attr('href');
                var id = "user-id";
                var value = $(thisApplicant).data('id');

                var updatesURI = updateQueryStringParameter(url, id, value);

                $(this).attr('href', updatesURI );
            });

            $.each($(this).data(), function(key, value) {

                $( '.applicant-info #'+key+' .value' ).html( value );
            });
        });
    });
</script>

<?php } ?>