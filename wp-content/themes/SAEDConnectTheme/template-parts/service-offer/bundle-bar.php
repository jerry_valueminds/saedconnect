<?php $bundles = get_post_meta($post_id, 'bundle', false); ?>

    
<?php switch_to_blog(110); ?>

<?php 
    /* Get all assigned bundles */

    foreach( $bundles as $bundle_id ){
        $bundle_url = get_post_meta($bundle_id, 'info-page-url', true);
        $bundle_summary = get_post_meta($bundle_id, 'summary', true);
        $bundle_bg_color = get_post_meta($bundle_id, 'bg-color', true);
        $bundle_alt_color = get_post_meta($bundle_id, 'alt-color', true);
?>
  
  
                        
                            
            
                        
                    
                    
    <div class="container-wrapper" 
        style="background-color: <?php echo $bundle_bg_color ?>; color: <?php echo $bundle_alt_color ?>;"
    >
        <a class="d-block padding-tb-15" data-toggle="collapse" href="#collapseBundle-<?php echo $bundle_id ?>" role="button" aria-expanded="false" aria-controls="collapseExample">
            <div class="d-flex align-items-center justify-content-between" style="color: <?php echo $bundle_alt_color ?>;">
                <p class="padding-r-10">
                    This Job is part of the
                    <span class="txt-bold">
                        <?php echo get_the_title($bundle_id) ?>
                    </span>
                </p>
                <p class="">
                    <i class="fa fa-chevron-down"></i>
                </p>
            </div>
        </a>
        <div class="collapse txt-sm" id="collapseBundle-<?php echo $bundle_id ?>">
            <div class="border-t-1 padding-tb-15" style="border-color: <?php echo $bundle_alt_color ?>;">
                <p class="padding-b-20" style="color: <?php echo $bundle_alt_color ?>;"><?php echo $bundle_summary ?></p>
                <p class="txt-medum">
                    <a class="paading-r-5" href="<?php echo $bundle_url ?>" target="_blank" style="color: <?php echo $bundle_alt_color ?>;">
                        Learn More
                        <i class="fa fa-long-arrow-right"></i>
                    </a>
                </p>
            </div>
        </div>
    </div>
   
<?php
    }
?>

<?php restore_current_blog(); ?>