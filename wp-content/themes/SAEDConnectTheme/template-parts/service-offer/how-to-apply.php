<?php if( $rendered_view == 'how-to-apply' ){ ?>
   
<section class="padding-t-20 padding-b-80">
    <div class="container-wrapper">
        <div class="row row-20">
            <div class="col-md-8 padding-lr-20">
                <div class="margin-b-40">
                    <h1 class="bg-ash txt-medium txt-color-white padding-o-15">
                        How to Apply
                    </h1>
                    <article class="text-box sm padding-o-15 border-o-1 border-color-darkgrey">
                        <?php echo get_post_meta( $post_id, 'how-to-apply', true ); ?>
                    </article>
                </div>
                <div class="text-center">
                    <a href="<?php echo $post_link; ?>" class="btn btn-blue txt-normal-s no-m-b w-100">
                        Back to Details
                    </a>    
                </div>
            </div>
            <div class="col-md-4 padding-lr-20">
            <?php if( !is_user_logged_in() ){ ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Login to Apply Now
                    </h3>
                    
                    <!-- Login Form -->
                    <style>
                        .gfield_checkbox label, .login-form .gfield_label{
                            display: block !important;
                        }

                        .gfield_checkbox li{
                            display: flex;
                            align-items: center;
                        }

                        .gfield_checkbox input{
                            margin: 0 !important;
                            margin-right: 10px !important;
                        }
                    </style>

                    <?php switch_to_blog(1); ?>

                        <!-- Login Tab -->
                        <div class="login">
                            <div class="login-form">
                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                                <?php endif;?>
                            </div>
                        </div>

                    <?php restore_current_blog(); ?>
 
                    <p class="txt-sm txt-medium padding-t-10">
                        Dont have an account?
                        <a href="https://www.saedconnect.org/register" class=" txt-color-blue">
                            Sign up
                        </a>
                    </p>
                </div>
                <?php } ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Share Job Post
                    </h3>
                    <p class="txt-sm">
                        <a href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-facebook fa-stack-1x fa-inverse txt-color-white"></i>
                            </span>
                        </a>
                        <a href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-twitter fa-stack-1x fa-inverse txt-color-white"></i>
                            </span>
                        </a>
                        <a href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-instagram fa-stack-1x fa-inverse txt-color-white"></i>
                            </span>
                        </a>
                        <a href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-linkedin fa-stack-1x fa-inverse txt-color-white"></i>
                            </span>
                        </a>
                    </p>
                </div>
                <p class="txt-sm txt-medium padding-t-10">
                    <a href="" class=" txt-color-blue">
                        Report Job
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

<?php } ?>