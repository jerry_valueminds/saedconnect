<?php if( $current_user->ID != $post_author_id && $rendered_view != 'application-manager' && $rendered_view == '' ){ ?>
   
<section class="padding-t-20 padding-b-80">
    <div class="container-wrapper">
        <div class="row row-20">
            <div class="col-md-8 padding-lr-20">
                <!-- Image -->
                <div class="position-relative margin-b-20">
                    <style>
                        .upload-btn{
                            position: absolute;
                            text-align: center;
                            left: 0;
                            width: 100%;
                            bottom: 15px;
                        }
                    </style>
                    <?php $images = get_attached_media( 'image', $post_id ); ?>

                    <?php if($images){ ?>

                        <?php  
                            foreach($images as $image) { //print_r( $image ); 
                                $previousImg_id = $image->ID;
                        ?>
                            <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">

                        <?php } ?>

                    <?php } else { ?>
                        <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png" width="150" class="margin-b-60">
                        </figure>

                    <?php } ?>
                </div>
                
                <!-- Brochure -->
                <div class="margin-b-20">

                    <?php 
                        $unserialized_data = get_post_meta( $post_id, 'brochure', false );
                        $brochure_counter = 0;
                    ?>

                    <?php if( $unserialized_data ){ ?>
                        <article class="">

                        <?php foreach ($unserialized_data as $serialized_brochure){ ?>

                            <?php 
                                $brochure = maybe_unserialize( $serialized_brochure );
                                $brochure_counter++;
                            ?>

                            <div class="margin-b-5">
                                <h1 class="d-flex align-items-center justify-content-between bg-ash txt-color-white padding-o-15">
                                    <a class="txt-color-white" href="<?php echo $brochure['brochure-link'] ?>" target="_blank">
                                        Download Brochure
                                    </a>
                                </h1>
                            </div>

                        <?php } ?>

                        </article>

                    <?php }else{ ?>

                        <div class="padding-o-15 border-o-1 border-color-darkgrey">
                            <h3 class="txt-height-1-4">
                                You have not added any Brochures. Click the <span class="txt-bold">Add Button</span> to add one.
                            </h3>
                        </div>

                    <?php } ?>
                </div>
                
                <!-- Description -->
                <div class="margin-b-20">
                    <h1 class="txt-xxlg txt-medium padding-b-15">
                        About this offer
                    </h1>
                    <article class="text-box sm">
                        <?php 
                            if( get_post_meta( $post_id, 'description', true ) )
                                echo get_post_meta( $post_id, 'description', true );
                            else
                                echo '<p>No description available.</p>'
                        ?>
                    </article>
                </div>
                
                <!-- Location -->
                <div class="bg-grey padding-o-15 margin-b-30">
                    <h1 class=" txt-bold padding-b-15 margin-b-15 border-b-1 border-color-darkgrey">
                        This Offer is Available in:
                    </h1>
                    <div class="txt-sm">
                        <p class="txt-medium">
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'nigerian-state', array("fields" => "names"));

                                if( $term_list ){
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } else {
                                    echo "Locations are unvailable";
                                }
                            ?>
                        </p>
                    </div>
                </div>
                  
                <!-- Offer Packages -->
                <div class="margin-b-30">
                    <h1 class="txt-xxlg txt-medium padding-b-15">
                        Need this Service?
                    </h1>
                    <div class="padding-b-15 margin-b-15 border-b-1 border-color-darkgrey">
                        <a class="btn btn-blue txt-sm no-m-b" href="">Request a Quote</a>
                    </div>
                    <p class="txt-normal-s margin-b-15">
                        Outline the steps you will take to complete this task if you are hired
                    </p>
                    
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'package', false );
                            $package_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="">

                            <?php foreach ($unserialized_data as $serialized_package){ ?>

                                <?php 
                                    $package = maybe_unserialize( $serialized_package );
                                    $package_counter++;
                                ?>
                                
                                <div class="margin-b-5">
                                    <h1 class="d-flex align-items-center justify-content-between bg-grey padding-o-10">
                                        <span class="txt-normal-s">
                                            <a data-toggle="collapse" href="#packageContent-<?php echo $package_counter; ?>" aria-expanded="false">
                                                <span class="txt-color-dark txt-medium">
                                                    <i class="fa fa-plus padding-r-5"></i>
                                                    <?php echo 'Package '.$package_counter.': '.$package['package-name'] ?>
                                                </span>
                                            </a>
                                        </span>
                                        <span class="btn btn-ash txt-normal-s no-m-b" style="padding: 0.4em 1.5em;">
                                            ₦<?php echo ($package['package-fee'])? number_format( $package['package-fee'] ) : '0' ; ?>
                                        </span>
                                    </h1>
                                    <div id="packageContent-<?php echo $package_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                        <div class="txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                            <article class="text-box">
                                                <?php echo $package['package-description'] ?>
                                            </article>
                                            <div class="margin-t-15">
                                                <a href="#" class="btn btn-blue txt-normal-s no-m-b">
                                                    Order
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editPackageModal-<?php echo $package_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentcurriculumModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-package-'.$package_counter; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment curriculum</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php

                                                        if($_GET['view'] == 'form-delete-package-'.$package_counter ){
                                                            delete_post_meta($post_id, 'package', $serialized_package);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                        /* Add/Edit Job (Specification) */

                                                        /* Get Post data */

                                                        $unserialized_data = $package;

                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-package-'.$package_counter){

                                                            $serialized_data = maybe_serialize($_POST);
                                                            echo "Current";
                                                            echo "<br>";
                                                            print_r($serialized_data);

                                                            update_post_meta( $post_id, 'package', $serialized_data, $serialized_package);

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                    ?>

                                                    <div class="form margin-b-40">
                                                        <!-- Name -->
                                                        <div class="form-item">
                                                            <label for="package-name">
                                                                Name
                                                            </label>
                                                            <input 
                                                                type="text" 
                                                                name="package-name" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['package-name'] ?>"
                                                            >
                                                        </div>

                                                        <!-- Description -->
                                                        <div class="form-item">
                                                            <label for="package-description">
                                                                Description
                                                            </label>
                                                            <textarea class="editor" name="package-description" id="" cols="30" rows="8"><?php echo $unserialized_data['package-description'] ?></textarea>
                                                        </div>

                                                        <!-- Step -->
                                                        <div class="form-item">
                                                            <label for="step">
                                                                Fee
                                                            </label>
                                                            <input 
                                                                type="number" 
                                                                name="package-fee" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['package-fee'] ?>"
                                                            >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    You have not added any packages. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                             
                <!-- Workflow -->
                <div class="margin-b-30">
                    <h1 class="txt-xxlg txt-medium padding-b-15">
                        Workflow
                    </h1>
                    <p class="txt-normal-s margin-b-15">
                        Here is how I will complete this task for you, if you hire me.
                    </p>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'workflow', false );
                            $workflow_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="row row-10">

                            <?php foreach ($unserialized_data as $serialized_workflow){ ?>

                                <?php 
                                    $workflow = maybe_unserialize( $serialized_workflow );
                                    $workflow_counter++;
                                ?>
                                
                                <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                                    <div class="flex_1 text-center txt-normal-s padding-o-15 border-o-1 border-color-darkgrey">
                                        <p class="txt-bold padding-b-10">
                                           <?php echo 'Step '.$workflow_counter; ?>
                                        </p>
                                        <p>
                                           <?php echo wp_strip_all_tags( $workflow['step-description']) ?>
                                        </p>
                                        <p class="">
                                           (<?php echo $workflow['step-duration']; ?>)
                                        </p>
                                    </div>
                                </div>

                            <?php } ?>
                            
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    No Workflow available.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                             
                <!-- FAQ -->
                <div class="margin-b-20">
                    <div class="d-flex align-items-center justify-content-between padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                        <div>
                            <h1 class=" txt-lg txt-medium padding-b-10">
                                Frequently Asked Questions
                            </h1>
                            <p class="txt-normal-s">
                                Find answers to Frequently Asked Questions
                            </p>
                        </div>
                    </div>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'faq', false );
                            $faq_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>

                            <?php foreach ($unserialized_data as $serialized_faq){ ?>

                                <?php 
                                    $faq = maybe_unserialize( $serialized_faq );
                                    $faq_counter++;
                                ?>

                                <div class=" border-o-1 border-color-darkgrey">
                                    <p class="txt-normal-s">
                                        <a class="txt-bold d-flex justify-content-between padding-o-15" data-toggle="collapse" href="#faq-<?php echo $faq_counter; ?>" aria-expanded="false">
                                           <?php echo $faq['question'] ?>
                                            <i class="fa fa-chevron-down padding-l-5"></i>
                                        </a>
                                    </p>
                                    <div id="faq-<?php echo $faq_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                        <div class="row padding-lr-15 padding-b-15">
                                            <article class="text-box txt-sm txt-height-1-7">
                                                <?php echo $faq['answer'] ?>
                                            </article>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    No FAQs available.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                
            </div>
            <div class="col-md-4 padding-lr-20">
                <!-- Freelancer -->
                <div class="margin-b-20">
                    <div class="margin-b-5">
                        <?php 
                            /* Get Avatar */
                            $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                            $meta_key = 'user_avatar_url';
                            $get_avatar_url = get_user_meta($post_author_id, $meta_key, true);

                            if($get_avatar_url){
                                $avatar_url = $get_avatar_url;
                            }

                            /* Get User Display Name */
                            switch_to_blog(1);

                            $gf_id = 4; //Form ID
                            $entry_count = 0;

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $post_author_id, //Current logged in user
                                    )
                                )
                            );

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                            if($entry_count){ //If no entry
                                foreach( $entries as $entry ){          
                                    $displayname = rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' );
                                }                
                            } 
                                                                                                                    
                            /* Get capability Profile */
                            $unserialized_capabilities = maybe_unserialize( get_user_meta($post_author_id, 'capability_profile', true) );
                                                                                                                    
                            restore_current_blog();

                        ?>

                        <div class="padding-o-15 border-o-1 border-color-darkgrey">
                            <div class="row">
                                <figure class="col-2">
                                    <img src="<?php echo $avatar_url; ?>" alt="" style="border-radius:8px;">
                                </figure>
                                <div class="col-10 padding-l-30">
                                    <p class="txt-lg txt-medium padding-b-10">
                                        <?php echo ($post_author_id == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                    </p>
                                    <!--<p class="txt-normal-s padding-b-15">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum at minus autem. Cum libero ea rem dolor.
                                    </p>-->
                                    <p class="txt-sm txt-bold padding-b-15">
                                    <?php 
                                        switch_to_blog(109);

                                        $gf_id = 84; //Form ID
                                        $entry_count = 0;

                                        /* GF Search Criteria */
                                        $search_criteria = array(
                                            'field_filters' => array( //which fields to search
                                                array('key' => 'created_by', 'value' => $post_author_id,)
                                            )
                                        );

                                        /* Get Entries */
                                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                        /* Get GF Entry Count */
                                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria ); 

                                        if($entry_count){ //If no entry

                                            foreach( $entries as $entry ){
                                                $field_id = 7; // Update this number to your field id number
                                                $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                echo $value;
                                            }

                                        } 
                                        restore_current_blog(); 
                                    ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="padding-t-15 padding-b-5 padding-lr-15 border-o-1 border-color-darkgrey">
                            <p class="txt-medium padding-b-10">
                                <?php echo ($post_author_id == 1)? 'SAEDConnect Admin' : $displayname; ?>'s capabilities
                            </p>
                            <p class="txt-normal-s">
                                <?php
                                    foreach($unserialized_capabilities as $key => $subterms){
                                        
                                        foreach($subterms as $subterm){
                                            $child_term = get_term($subterm);
                                            $child_term_name = $child_term->name; //Get the term name

                                            echo '<span class="btn btn-trans-bw txt-sm margin-r-5" style=" margin-bottom: 10px;">'.$child_term_name.'</span>';

                                        }
                                    } 
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                
                
               
                <!-- Portfolio -->
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                        <span class="txt-lg txt-medium">
                            Relevant Portfolio 
                        </span>
                    </h1>
                    <div class="margin-b-20">
                        <?php
                            $custom_query = new WP_Query();
                            $custom_query->query( 
                                array(
                                    'post_type' => 'portfolio',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'parent',
                                            'value' => $post_id
                                        )
                                    ),
                                ) 
                            );

                            if ( $custom_query->have_posts() ) {

                                while ($custom_query->have_posts()) : $custom_query->the_post();

                                /* Variables */
                                $portfolio_id = get_the_ID();    //Get Program ID
                        ?>

                            <div class="margin-b-5 border-o-1 border-color-darkgrey">
                                <p class="txt-normal-s">
                                    <a class="d-block padding-o-10" data-toggle="collapse" href="#portfolioContent-<?php echo $portfolio_id; ?>" aria-expanded="false">
                                        <span class="txt-color-dark">
                                            <i class="fa fa-plus padding-r-5"></i>
                                            <span class="txt-medium">
                                                <?php the_title() ?>
                                            </span>
                                        </span>
                                    </a>
                                </p>
                                <div id="portfolioContent-<?php echo $portfolio_id; ?>" class="collapse" aria-labelledby="headingOne">
                                    <div class="padding-lr-10 padding-b-10">
                                        <article class="text-box txt-normal-s txt-height-1-7 padding-t-20">
                                            <?php echo get_post_meta( $portfolio_id, 'description', true )  ?>
                                        </article>
                                        <div class="images padding-t-30">
                                            <?php $images = get_attached_media( 'image', $portfolio_id ); ?>

                                            <?php if($images){ ?>

                                                <?php  
                                                    foreach($images as $image) { //print_r( $image ); 
                                                        $previousImg_id = $image->ID;
                                                ?>
                                                    <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>" height="100" class="d-inline-block padding-r-5 padding-b-5">

                                                <?php } ?>

                                            <?php } else { ?>
                                                <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png" width="150" class="margin-b-60">
                                                </figure>

                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                                endwhile;

                            }else{
                        ?>
                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    You have not added your Portfolio. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                            
                    </div>
                </div>
                
                <?php if( !is_user_logged_in() ){ ?>
                <!-- Login -->
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Login to Apply Now
                    </h3>
                    
                    <!-- Login Form -->
                    <style>
                        .gfield_checkbox label, .login-form .gfield_label{
                            display: block !important;
                        }

                        .gfield_checkbox li{
                            display: flex;
                            align-items: center;
                        }

                        .gfield_checkbox input{
                            margin: 0 !important;
                            margin-right: 10px !important;
                        }
                    </style>

                    <?php switch_to_blog(1); ?>

                        <!-- Login Tab -->
                        <div class="login">
                            <div class="login-form">
                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                                <?php endif;?>
                            </div>
                        </div>

                    <?php restore_current_blog(); ?>
 
                    <p class="txt-sm txt-medium padding-t-10">
                        Dont have an account?
                        <a href="https://www.saedconnect.org/register" class=" txt-color-blue">
                            Sign up
                        </a>
                    </p>
                </div>
                <?php } ?>
                
                <!-- Share -->
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Share Job Post
                    </h3>
                    <?php echo do_shortcode('[Sassy_Social_Share]') ?>
                </div>
                
                <!-- Report -->
                <p class="txt-sm txt-medium padding-t-10">
                    <a href="" class=" txt-color-blue">
                        Report Job
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

<?php } elseif( $current_user->ID == $post_author_id && $rendered_view != 'application-manager' ) { ?>

<section class="padding-t-20 padding-b-80">
    <div class="container-wrapper">
        <div class="row row-20">
            <div class="col-md-8 padding-lr-20">
                <!-- Image -->
                <div class="position-relative margin-b-20">
                    <style>
                        .upload-btn{
                            position: absolute;
                            text-align: center;
                            left: 0;
                            width: 100%;
                            bottom: 15px;
                        }
                    </style>
                    <?php $images = get_attached_media( 'image', $post_id ); ?>

                    <?php if($images){ ?>

                        <?php  
                            foreach($images as $image) { //print_r( $image ); 
                                $previousImg_id = $image->ID;
                        ?>
                            <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">

                        <?php } ?>

                    <?php } else { ?>
                        <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png" width="150" class="margin-b-60">
                        </figure>

                    <?php } ?>
                    <?php if( $current_user->ID == $post_author_id ){ ?>
                    <div class="upload-btn">
                        <a class="btn btn-blue txt-xs" data-toggle="modal" href="#featuredImageModal">
                             <?php echo ($images)? "Change Image" : "+ Add"; ?>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                
                <!-- Description -->
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            + Describe what your offer will contain
                        </span>
                        <a class="btn btn-white txt-xs no-m-b" data-toggle="modal" href="#descriptionModal">
                            Add / Edit
                        </a>
                    </h1>
                    <article class="text-box sm padding-o-15 border-o-1 border-color-darkgrey">
                        <?php 
                            if( get_post_meta( $post_id, 'description', true ) )
                                echo get_post_meta( $post_id, 'description', true );
                            else
                                echo '<p>You have not added a description yet.</p>'
                        ?>
                    </article>
                </div>
                
                <!-- Location -->
                <div class="margin-b-30">
                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-medium txt-color-white padding-o-15">
                        <span>
                            This Offer is Available in:
                        </span>
                    </h1>
                    <div class="txt-sm padding-o-15 border-o-1 border-color-darkgrey margin-b-20">
                        <p class="txt-medium">
                            <?php 
                                $term_list = wp_get_post_terms($post_id, 'nigerian-state', array("fields" => "names"));

                                if( $term_list ){
                                    foreach( $term_list as $key => $term_name ){
                                        echo $term_name;
                                        echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' );
                                    }
                                } else {
                                    echo "Locations are unvailable";
                                }
                            ?>
                        </p>
                    </div>
                </div>
                  
                <!-- Offer Packages -->
                <div class="margin-b-30">
                    <h1 class="d-flex align-items-center justify-content-between padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                        <span class="txt-lg txt-medium">
                            Add your Package & Pricelist
                        </span>
                        <a class="txt-medium" data-toggle="modal" href="#packageModal">
                            + Add
                        </a>
                    </h1>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'package', false );
                            $package_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="">

                            <?php foreach ($unserialized_data as $serialized_package){ ?>

                                <?php 
                                    $package = maybe_unserialize( $serialized_package );
                                    $package_counter++;
                                ?>
                                
                                <div class="margin-b-5">
                                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-color-white padding-o-15">
                                        <span class="txt-normal-s">
                                            <a data-toggle="collapse" href="#packageContent-<?php echo $package_counter; ?>" aria-expanded="false">
                                                <span class="txt-color-white">
                                                    <i class="fa fa-plus padding-r-5"></i>
                                                    <?php echo 'Package '.$package_counter.': '.$package['package-name'] ?>
                                                </span>
                                            </a>
                                        </span>
                                        <span class="txt-normal-s">
                                            <a class="txt-color-white padding-r-15" data-toggle="modal" href="#editPackageModal-<?php echo $package_counter ?>">
                                                <i class="fa fa-edit"></i>
                                                Edit
                                            </a>
                                            <a href="<?php echo currentUrl(true).'?view=form-delete-package-'.$package_counter; ?>" class="confirm-delete txt-color-white padding-r-15">
                                                <i class="fa fa-trash"></i>
                                                Delete
                                            </a>
                                        </span>
                                    </h1>
                                    <div id="packageContent-<?php echo $package_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                        <div class="txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                            <article class="text-box">
                                                <?php echo $package['package-description'] ?>
                                            </article>
                                            <div class="margin-t-15">
                                                <span class="btn btn-ash txt-normal-s no-m-b">
                                                    ₦<?php echo number_format( $package['package-fee'] ); ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editPackageModal-<?php echo $package_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentcurriculumModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-package-'.$package_counter; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment curriculum</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php

                                                        if($_GET['view'] == 'form-delete-package-'.$package_counter ){
                                                            delete_post_meta($post_id, 'package', $serialized_package);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                        /* Add/Edit Job (Specification) */

                                                        /* Get Post data */

                                                        $unserialized_data = $package;

                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-package-'.$package_counter){

                                                            $serialized_data = maybe_serialize($_POST);
                                                            echo "Current";
                                                            echo "<br>";
                                                            print_r($serialized_data);

                                                            update_post_meta( $post_id, 'package', $serialized_data, $serialized_package);

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                    ?>

                                                    <div class="form margin-b-40">
                                                        <!-- Name -->
                                                        <div class="form-item">
                                                            <label for="package-name">
                                                                Name
                                                            </label>
                                                            <input 
                                                                type="text" 
                                                                name="package-name" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['package-name'] ?>"
                                                            >
                                                        </div>

                                                        <!-- Description -->
                                                        <div class="form-item">
                                                            <label for="package-description">
                                                                Description
                                                            </label>
                                                            <textarea class="editor" name="package-description" id="" cols="30" rows="8"><?php echo $unserialized_data['package-description'] ?></textarea>
                                                        </div>

                                                        <!-- Step -->
                                                        <div class="form-item">
                                                            <label for="step">
                                                                Fee
                                                            </label>
                                                            <input 
                                                                type="number" 
                                                                name="package-fee" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['package-fee'] ?>"
                                                            >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    You have not added any packages. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                             
                <!-- Workflow -->
                <div class="margin-b-30">
                    <div class="d-flex align-items-center justify-content-between padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                        <div>
                            <h1 class=" txt-lg txt-medium padding-b-10">
                                Setup your Workflow
                            </h1>
                            <p class="txt-normal-s">
                                Outline the steps you will take to complete this task if you are hired
                            </p>
                        </div>
                        <a class="txt-medium" data-toggle="modal" href="#workflowModal">
                            + Add
                        </a>
                    </div>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'workflow', false );
                            $workflow_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="">

                            <?php foreach ($unserialized_data as $serialized_workflow){ ?>

                                <?php 
                                    $workflow = maybe_unserialize( $serialized_workflow );
                                    $workflow_counter++;
                                ?>
                                
                                <div class="margin-b-5">
                                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-color-white padding-o-15">
                                        <span class="txt-normal-s">
                                            <a data-toggle="collapse" href="#workflowContent-<?php echo $workflow_counter; ?>" aria-expanded="false">
                                                <span class="txt-color-white">
                                                    <i class="fa fa-plus padding-r-5"></i>
                                                    <?php echo 'Step '.$workflow_counter.': '.truncate( wp_strip_all_tags( $workflow['step-description']), 50 ) ?>
                                                </span>
                                            </a>
                                        </span>
                                        <span class="txt-normal-s">
                                            <a class="txt-color-white padding-r-15" data-toggle="modal" href="#editWorkflowModal-<?php echo $workflow_counter ?>">
                                                <i class="fa fa-edit"></i>
                                                Edit
                                            </a>
                                            <a href="<?php echo currentUrl(true).'?view=form-delete-workflow-'.$workflow_counter; ?>" class="confirm-delete txt-color-white padding-r-15">
                                                <i class="fa fa-trash"></i>
                                                Delete
                                            </a>
                                        </span>
                                    </h1>
                                    <div id="workflowContent-<?php echo $workflow_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                        <div class="txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                            <article class="text-box">
                                                <?php echo $workflow['step-description'] ?>
                                            </article>
                                            <div class="margin-t-15">
                                                <span class="btn btn-ash txt-normal-s no-m-b">
                                                    <?php echo $workflow['step-duration']; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editWorkflowModal-<?php echo $workflow_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentcurriculumModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-workflow-'.$workflow_counter; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Workflow Step</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php

                                                        if($_GET['view'] == 'form-delete-workflow-'.$workflow_counter ){
                                                            delete_post_meta($post_id, 'workflow', $serialized_workflow);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                        /* Add/Edit Job (Specification) */

                                                        /* Get Post data */

                                                        $unserialized_data = $workflow;

                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-workflow-'.$workflow_counter){

                                                            $serialized_data = maybe_serialize($_POST);
                                                            echo "Current";
                                                            echo "<br>";
                                                            print_r($serialized_data);

                                                            update_post_meta( $post_id, 'workflow', $serialized_data, $serialized_workflow);

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                    ?>

                                                    <div class="form margin-b-40">
                                                        <!-- Description -->
                                                        <div class="form-item">
                                                            <label for="package-description">
                                                                Description
                                                            </label>
                                                            <textarea class="editor" name="step-description" id="" cols="30" rows="8"><?php echo $unserialized_data['step-description'] ?></textarea>
                                                        </div>

                                                        <!-- Duration -->
                                                        <div class="form-item">
                                                            <label for="step">
                                                                Duration
                                                            </label>
                                                            <input 
                                                                type="text" 
                                                                name="step-duration" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['step-duration'] ?>"
                                                            >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    You have not added your Workflow. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                             
                <!-- FAQ -->
                <div class="margin-b-20">
                    <div class="d-flex align-items-center justify-content-between padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                        <div>
                            <h1 class=" txt-lg txt-medium padding-b-10">
                                Frequently Asked Questions
                            </h1>
                            <p class="txt-normal-s">
                                Create answers for questions potential clients will likely ask
                            </p>
                        </div>
                        <a class="txt-medium" data-toggle="modal" href="#FAQModal">
                            + Add
                        </a>
                    </div>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'faq', false );
                            $faq_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>

                            <?php foreach ($unserialized_data as $serialized_faq){ ?>

                                <?php 
                                    $faq = maybe_unserialize( $serialized_faq );
                                    $faq_counter++;
                                ?>

                                <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                    <p class="txt-normal-s">
                                        <span class="row">
                                            <span class="col-8 txt-bold">
                                                <?php echo $faq['question'] ?>
                                            </span>
                                            <span class="col-4 text-right">
                                                <a class="txt-color-green padding-r-15" data-toggle="modal" href="#editPaymentfaqModal-<?php echo $faq_counter ?>">
                                                    <i class="fa fa-edit"></i>
                                                    Edit
                                                </a>
                                                <a href="<?php echo currentUrl(true).'?view=form-delete-payment-faq-'.$faq_counter; ?>" class="confirm-delete txt-color-red padding-r-15">
                                                    <i class="fa fa-trash"></i>
                                                    Delete
                                                </a>
                                                <a class="txt-color-blue" data-toggle="collapse" href="#faq-<?php echo $faq_counter; ?>" aria-expanded="false">
                                                    View Details 
                                                    <i class="fa fa-angle-down padding-l-5"></i>
                                                </a>
                                            </span>
                                        </span>
                                    </p>
                                    <div id="faq-<?php echo $faq_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                        <div class="row">
                                            <article class="text-box txt-sm txt-height-1-7 padding-t-20">
                                                <?php echo $faq['answer'] ?>
                                            </article>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editPaymentfaqModal-<?php echo $faq_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentfaqModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-payment-faq-'.$faq_counter; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Payment faq</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php

                                                        if($_GET['view'] == 'form-delete-payment-faq-'.$faq_counter ){
                                                            delete_post_meta($post_id, 'faq', $serialized_faq);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                        /* Add/Edit Job (Specification) */

                                                        /* Get Post data */

                                                        $unserialized_data = $faq;

                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-payment-faq-'.$faq_counter){

                                                            $serialized_data = maybe_serialize($_POST);
                                                            echo "Current";
                                                            echo "<br>";
                                                            print_r($serialized_data);

                                                            update_post_meta( $post_id, 'faq', $serialized_data, $serialized_faq);

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                    ?>

                                                    <div class="form margin-b-40">

                                                        <!-- Title -->
                                                        <div class="form-item">
                                                            <label for="question">
                                                                Question
                                                            </label>
                                                            <input 
                                                                type="text" 
                                                                name="question" 
                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                value="<?php echo $unserialized_data['question'] ?>"
                                                            >
                                                        </div>

                                                        <!-- Deadline -->
                                                        <div class="form-item">
                                                            <label for="answer">
                                                                Answer
                                                            </label>
                                                            <textarea class="editor" name="answer" id="" cols="30" rows="8"><?php echo $unserialized_data['answer'] ?></textarea>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    You have not added any FAQs. Click the <span class="txt-bold">Add FAQ Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                             
                <!-- Testimonials -->
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                        <span class="txt-lg txt-medium">
                            Testimonial 
                        </span>
                        <a class="txt-medium" data-toggle="modal" href="#testimonialModal">
                            + Add
                        </a>
                    </h1>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'testimonial', false );
                            $testimonial_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="">

                            <?php foreach ($unserialized_data as $serialized_testimonial){ ?>

                                <?php 
                                    $testimonial = maybe_unserialize( $serialized_testimonial );
                                    $testimonial_counter++;
                                ?>
                                
                                <div class="margin-b-5">
                                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-color-white padding-o-15">
                                        <span class="txt-normal-s">
                                            <a data-toggle="collapse" href="#testimonialContent-<?php echo $testimonial_counter; ?>" aria-expanded="false">
                                                <span class="txt-color-white">
                                                    <i class="fa fa-plus padding-r-5"></i>
                                                    <?php echo $testimonial_counter.': '.$testimonial['testimonial-username'] ?>
                                                </span>
                                            </a>
                                        </span>
                                        <span class="txt-normal-s">
                                            <a class="txt-color-white padding-r-15" data-toggle="modal" href="#editTestimonialModal-<?php echo $testimonial_counter ?>">
                                                <i class="fa fa-edit"></i>
                                                Edit
                                            </a>
                                            <a href="<?php echo currentUrl(true).'?view=form-delete-testimonial-'.$testimonial_counter; ?>" class="confirm-delete txt-color-white padding-r-15">
                                                <i class="fa fa-trash"></i>
                                                Delete
                                            </a>
                                        </span>
                                    </h1>
                                    <div id="testimonialContent-<?php echo $testimonial_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                        <div class="txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                            <article class="text-box">
                                                <?php echo $testimonial['testimonial-description'] ?>
                                            </article>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editTestimonialModal-<?php echo $testimonial_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentcurriculumModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-testimonial-'.$testimonial_counter; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Testimonial</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php

                                                        if($_GET['view'] == 'form-delete-testimonial-'.$testimonial_counter ){
                                                            delete_post_meta($post_id, 'testimonial', $serialized_testimonial);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                        /* Add/Edit Job (Specification) */

                                                        /* Get Post data */

                                                        $unserialized_data = $testimonial;

                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-testimonial-'.$testimonial_counter){

                                                            $serialized_data = maybe_serialize($_POST);
                                                            echo "Current";
                                                            echo "<br>";
                                                            print_r($serialized_data);

                                                            update_post_meta( $post_id, 'testimonial', $serialized_data, $serialized_testimonial);

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                            
                                                        /* Get All Users */
                                                        $blogusers = get_users( 'blog_id=1&orderby=nicename&role=subscriber' );
                                                    ?>

                                                    <div class="form margin-b-40">
                                                        <!-- Name -->
                                                        <div class="form-item">
                                                            <label for="package-name">
                                                                Username of customer
                                                            </label>
                                    
                                                            <select class="full-width" name="testimonial-username">
                                                                <option value="1" >
                                                                    Super Admin
                                                                </option>
                                                            <?php foreach( $blogusers as $user ){ ?>

                                                                <option 
                                                                   value="<?php echo esc_html( $user->ID ); ?>"
                                                                   <?php echo ( $unserialized_data['testimonial-username'] == $user->ID ) ? "selected" : ""; ?>
                                                                >
                                                                    <?php echo esc_html( $user->user_login ); ?>
                                                                </option>

                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                        

                                                        <!-- Description -->
                                                        <div class="form-item">
                                                            <label for="package-description">
                                                                Testimonial content
                                                            </label>
                                                            <textarea class="editor" name="testimonial-description" id="" cols="30" rows="8"><?php echo $unserialized_data['testimonial-description'] ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    You have not added any packages. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                
                <!-- Brochure -->
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                        <span class="txt-lg txt-medium">
                            Brochures 
                        </span>
                        <a class="txt-medium" data-toggle="modal" href="#brochureModal">
                            + Add
                        </a>
                    </h1>
                    <div class="margin-b-20">

                        <?php 
                            $unserialized_data = get_post_meta( $post_id, 'brochure', false );
                            $brochure_counter = 0;
                        ?>

                        <?php if( $unserialized_data ){ ?>
                            <article class="">

                            <?php foreach ($unserialized_data as $serialized_brochure){ ?>

                                <?php 
                                    $brochure = maybe_unserialize( $serialized_brochure );
                                    $brochure_counter++;
                                ?>
                                
                                <div class="margin-b-5">
                                    <h1 class="d-flex align-items-center justify-content-between bg-ash txt-color-white padding-o-15">
                                        <span class="txt-normal-s">
                                            <a data-toggle="collapse" href="#brochureContent-<?php echo $brochure_counter; ?>" aria-expanded="false">
                                                <span class="txt-color-white">
                                                    <i class="fa fa-plus padding-r-5"></i>
                                                    <?php echo $brochure_counter.': '.$brochure['brochure-title'] ?>
                                                </span>
                                            </a>
                                        </span>
                                        <span class="txt-normal-s">
                                            <a class="txt-color-white padding-r-15" data-toggle="modal" href="#editBrochureModal-<?php echo $brochure_counter ?>">
                                                <i class="fa fa-edit"></i>
                                                Edit
                                            </a>
                                            <a href="<?php echo currentUrl(true).'?view=form-delete-brochure-'.$brochure_counter; ?>" class="confirm-delete txt-color-white padding-r-15">
                                                <i class="fa fa-trash"></i>
                                                Delete
                                            </a>
                                        </span>
                                    </h1>
                                    <div id="brochureContent-<?php echo $brochure_counter; ?>" class="collapse" aria-labelledby="headingOne">
                                        <div class="txt-sm border-o-1 border-color-darkgrey padding-o-15">
                                            <article>
                                                <a class="btn btn-blue txt-normal-s uppercase no-m-b" href="<?php echo $brochure['brochure-link'] ?>" target="_blank">
                                                    Download <?php echo $brochure['brochure-type'] ?>
                                                </a>
                                            </article>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editBrochureModal-<?php echo $brochure_counter ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentcurriculumModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-brochure-'.$brochure_counter; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Brochure</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php

                                                        if($_GET['view'] == 'form-delete-brochure-'.$brochure_counter ){
                                                            delete_post_meta($post_id, 'brochure', $serialized_brochure);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                        /* Add/Edit Job (Specification) */

                                                        /* Get Post data */

                                                        $unserialized_data = $brochure;

                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-brochure-'.$brochure_counter){

                                                            $serialized_data = maybe_serialize($_POST);
                                                            echo "Current";
                                                            echo "<br>";
                                                            print_r($serialized_data);

                                                            update_post_meta( $post_id, 'brochure', $serialized_data, $serialized_brochure);

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }
                                                    ?>

                                                    <div class="form margin-b-40">

                                                        <!-- Title -->
                                                        <div class="form-item">
                                                            <label for="brochure-title">
                                                                Title
                                                            </label>
                                                            <input 
                                                                type="text" 
                                                                name="brochure-title" 
                                                                value="<?php echo $unserialized_data['brochure-title'] ?>"
                                                            >
                                                        </div>

                                                        <!-- Type -->
                                                        <div class="form-item">
                                                            <label for="brochure-type">
                                                                Type
                                                            </label>
                                                            <select name="brochure-type">
                                                                <option value="picture" <?php echo ($unserialized_data['brochure-type'] == 'picture')? "selected" : "" ?>>
                                                                    Picture
                                                                </option>
                                                                <option value="pdf" <?php echo ($unserialized_data['brochure-type'] == 'pdf')? "selected" : "" ?>>
                                                                    PDF
                                                                </option>
                                                                <option value="msWord" <?php echo ($unserialized_data['brochure-type'] == 'msWord')? "selected" : "" ?>>
                                                                    MSWord
                                                                </option>
                                                                <option value="excel" <?php echo ($unserialized_data['brochure-type'] == 'excel')? "selected" : "" ?>>
                                                                    Excel
                                                                </option>
                                                            </select>
                                                        </div>

                                                        <!-- Link -->
                                                        <div class="form-item">
                                                            <label for="step">
                                                                Upload Link
                                                            </label>
                                                            <input 
                                                                type="url" 
                                                                name="brochure-link" 
                                                                value="<?php echo $unserialized_data['brochure-link'] ?>"
                                                            >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            
                            </article>

                        <?php }else{ ?>

                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    You have not added any Brochures. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                
                <!-- Portfolio -->
                <div class="margin-b-20">
                    <h1 class="d-flex align-items-center justify-content-between padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                        <span class="txt-lg txt-medium">
                            Portfolio 
                        </span>
                        <a class="txt-medium" data-toggle="modal" href="#portfolioModal">
                            + Add
                        </a>
                    </h1>
                    <div class="margin-b-20">

                        <?php
                            $custom_query = new WP_Query();
                            $custom_query->query( 
                                array(
                                    'post_type' => 'portfolio',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'parent',
                                            'value' => $post_id
                                        )
                                    ),
                                ) 
                            );

                            if ( $custom_query->have_posts() ) {

                                while ($custom_query->have_posts()) : $custom_query->the_post();

                                /* Variables */
                                $portfolio_id = get_the_ID();    //Get Program ID
                        ?>


                                <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                    <p class="txt-normal-s">
                                        <span class="row">
                                            <span class="col-8 txt-bold">
                                                <?php the_title() ?>
                                            </span>
                                            <span class="col-4 text-right">
                                                <a class="txt-color-green padding-r-15" data-toggle="modal" href="#editPortfolioModal-<?php echo $portfolio_id ?>">
                                                    <i class="fa fa-edit"></i>
                                                    Edit
                                                </a>
                                                <a href="<?php echo currentUrl(true).'?view=form-delete-portfolio-'.$portfolio_id; ?>" class="confirm-delete txt-color-red padding-r-15">
                                                    <i class="fa fa-trash"></i>
                                                    Delete
                                                </a>
                                                <a class="txt-color-blue" data-toggle="collapse" href="#portfolioContent-<?php echo $portfolio_id; ?>" aria-expanded="false">
                                                    View Details 
                                                    <i class="fa fa-angle-down padding-l-5"></i>
                                                </a>
                                            </span>
                                        </span>
                                    </p>
                                    <div id="portfolioContent-<?php echo $portfolio_id; ?>" class="collapse" aria-labelledby="headingOne">
                                        <article class="text-box txt-normal-s txt-height-1-7 padding-t-20">
                                            <?php echo get_post_meta( $portfolio_id, 'description', true )  ?>
                                        </article>
                                        <div class="images padding-t-30">
                                            <?php $images = get_attached_media( 'image', $portfolio_id ); ?>

                                            <?php if($images){ ?>

                                                <?php  
                                                    foreach($images as $image) { //print_r( $image ); 
                                                        $previousImg_id = $image->ID;
                                                ?>
                                                    <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>" height="100" class="d-inline-block padding-r-5 padding-b-5">

                                                <?php } ?>

                                            <?php } else { ?>
                                                <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png" width="150" class="margin-b-60">
                                                </figure>

                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade font-main filter-modal" id="editPortfolioModal-<?php echo $portfolio_id ?>" tabindex="-1" role="dialog" aria-labelledby="AddPaymentfaqModal" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-edit-portfolio'.$portfolio_id; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Portfolio</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30 form">
                                                    <?php
                                                        if($_GET['view'] == 'form-delete-portfolio-'.$portfolio_id ){
                                                            wp_delete_post($portfolio_id);
                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                                        }


                                                        $redirect_link = currentUrl(true);


                                                        /*
                                                        *
                                                        * Save / Retrieve Form Data
                                                        *
                                                        */
                                                        if($_POST && $rendered_view == 'form-edit-portfolio'.$portfolio_id){

                                                            /* Get Post Name */
                                                            $postName = sanitize_text_field( $_POST['post-name'] );
                                                            $link = sanitize_text_field( $_POST['link'] ); 
                                                            $description = wp_kses_post( $_POST['description'] );

                                                            /* Save Post to DB */
                                                            $portfolio_id = wp_insert_post(array (
                                                                'ID' => $portfolio_id,
                                                                'post_type' => 'portfolio',
                                                                'post_title' => $postName,
                                                                'post_content' => "",
                                                                'post_status' => 'publish',
                                                            ));

                                                            update_post_meta( $portfolio_id, 'link', $link );
                                                            update_post_meta( $portfolio_id, 'description', $description);
                                                            
                                                            /* Upload Images */
                                                            // Check that the nonce is valid, and the user can edit this post.
                                                            if ( 
                                                                isset( $_POST['portfolio_image_upload_'.$portfolio_id.'_nonce'] ) 
                                                                && wp_verify_nonce( $_POST['portfolio_image_upload_'.$portfolio_id.'_nonce'], 'portfolio_image_upload_'.$portfolio_id )
                                                                && current_user_can( 'edit_post', $portfolio_id )
                                                            ) {
                                                                // The nonce was valid and the user has the capabilities, it is safe to continue.

                                                                // These files need to be included as dependencies when on the front end.
                                                                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                                                                require_once( ABSPATH . 'wp-admin/includes/file.php' );
                                                                require_once( ABSPATH . 'wp-admin/includes/media.php' );



                                                                $files = $_FILES['portfolio_image_upload_'.$portfolio_id];
                                                                foreach ($files['name'] as $key => $value) {
                                                                    if ($files['name'][$key]) {
                                                                        $file = array(
                                                                            'name' => $files['name'][$key],
                                                                            'type' => $files['type'][$key],
                                                                            'tmp_name' => $files['tmp_name'][$key],
                                                                            'error' => $files['error'][$key],
                                                                            'size' => $files['size'][$key]
                                                                        );
                                                                        $_FILES = array("upload_file" => $file);
                                                                        $attachment_id = media_handle_upload("upload_file", $portfolio_id);

                                                                        if (is_wp_error($attachment_id)) {
                                                                            // There was an error uploading the image.
                                                                            echo "Error adding file";
                                                                        } else {
                                                                            // The image was uploaded successfully!
                                                                            echo "File added successfully with ID: " . $attachment_id . "<br>";
                                                                            echo wp_get_attachment_image($attachment_id, array(800, 600)) . "<br>"; //Display the uploaded image with a size you wish. In this case it is 800x600
                                                                        }
                                                                    }
                                                                }



                                                            } else {

                                                                // The security check failed, maybe show the user an error.
                                                            }

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                                        }
                                
                                                        
                                                    ?>

                                                    <!-- Title -->
                                                    <div class="form-item">
                                                        <label for="post-name">
                                                            Title
                                                        </label>
                                                        <input 
                                                            type="text" 
                                                            name="post-name" 
                                                            class="d-block padding-tb-5 padding-lr-10 full-width"
                                                            value="<?php the_title() ?>"
                                                            required
                                                        >
                                                    </div>

                                                    <!-- Date -->
                                                    <div class="form-item">
                                                        <label for="link">
                                                            Link
                                                        </label>
                                                        <input type="url" name="link" value="<?php echo get_post_meta( $portfolio_id, 'link', true ) ?>" required>
                                                    </div>
                                                    
                                                    <!-- Description -->
                                                    <div class="form-item">
                                                        <label for="">
                                                            Description
                                                        </label>
                                                        <textarea class="editor" name="description" id="" cols="30" rows="10"><?php echo get_post_meta( $portfolio_id, 'description', true ) ?></textarea>
                                                    </div>
                                                    
                                                    <div class="image-upload-card">                    
                                                        <input 
                                                            type="file" 
                                                            name="<?php echo 'portfolio_image_upload_'.$portfolio_id.'[]'; ?>" id="<?php echo 'portfolio_image_upload_'.$portfolio_id; ?>" class="upload-field" 
                                                            multiple="multiple"
                                                        >
                                                        <input type="hidden" name="post_id" id="post_id" value="<?php echo $portfolio_id ?>">
                                                        <input type="hidden" name="previous_img" id="post_id" value="<?php echo $previousImg_id ?>">
                                                        
                                                        <?php wp_nonce_field( 'portfolio_image_upload_'.$portfolio_id, 'portfolio_image_upload_'.$portfolio_id.'_nonce' ); ?>

                                                        <div class="text-align-center bg-darkgrey txt-xlg text-center txt-light" id="featuredImagePreview">
                                                            Click here to select an image from your device
                                                        </div>
                                                     </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                        <?php
                                endwhile;

                            }else{
                        ?>
                            <div class="padding-o-15 border-o-1 border-color-darkgrey">
                                <h3 class="txt-height-1-4">
                                    You have not added your Portfolio. Click the <span class="txt-bold">Add Button</span> to add one.
                                </h3>
                            </div>

                        <?php } ?>
                            
                    </div>
                </div>
                                           
                                                                 
                                            
                                                                        
                
                

                
                <div class="margin-b-20">
                    <a 
                       class="dropdown-toggle txt-medium txt-color-dark d-flex justify-content-between align-items-center border-o-2 padding-o-15"
                       data-toggle="modal" href="#applicationMethodModal"
                    >
                            How do you want to recieve Applications?
                    </a>
                </div>
                <?php  
                    $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                    $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                    $howToApply = get_post_meta( $post_id, 'how-to-apply', true );
                ?>
                
                <?php if( $applicationMethod == 'manual' ){ ?>
                <div>
                    <h3 class="txt-bold txt-height-1-4 margin-b-10">
                        Manual Application Instructions
                    </h3>
                    <article class="text-box txt-normal-s txt-height-1-5 margin-b-20">
                        <?php echo $howToApply ?>
                    </article>
                </div>
                <?php } elseif( $applicationMethod == '3rd-party' ) { ?>
                    <div>
                        <h3 class="txt-bold txt-height-1-4 margin-b-10">
                            3rd Party Site
                        </h3>
                        <p class="txt-sm margin-b-10">
                            When applicants click the Apply Button on this job, they will be redirected to this site:
                        </p>
                        <p class="txt-sm margin-b-20">
                            <a href="<?php echo $applicationLink ?>" class="txt-bold txt-color-blue" target="_blank"><?php echo $applicationLink ?></a>
                        </p>
                    </div>
                <?php } elseif( $applicationMethod == 'saedconnect' ) { ?>
                    <div>
                        <h3 class="txt-bold txt-height-1-4 margin-b-10">
                            Recieve Applications through SAEDConnect
                        </h3>
                        <p class="txt-sm margin-b-20">
                            Letting SAEDconnect handle the application process gives you access to the Application Manager. The Application gives you access to a robust set of tools, crafted to give you control and allow you seamlessly manage job applications.
                        </p>
                        <p class="txt-sm margin-b-20">
                            Selecting this option requires paying a fee.
                        </p>
                        <p>
                            <a href="" class="btn btn-blue txt-sm">
                                Subscribe Now
                            </a>
                        </p>
                    </div>             
                <?php } else { ?>
                    <div>
                        <h3 class="txt-bold txt-height-1-4 margin-b-10">
                            You have not selected an Application for this job. You must select an Application Method before you can publish this job.
                        </h3>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-4 padding-lr-20">
            <?php if( !is_user_logged_in() ){ ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Login to Apply Now
                    </h3>
                    
                    <!-- Login Form -->
                    <style>
                        .gfield_checkbox label, .login-form .gfield_label{
                            display: block !important;
                        }

                        .gfield_checkbox li{
                            display: flex;
                            align-items: center;
                        }

                        .gfield_checkbox input{
                            margin: 0 !important;
                            margin-right: 10px !important;
                        }
                    </style>

                    <?php switch_to_blog(1); ?>

                        <!-- Login Tab -->
                        <div class="login">
                            <div class="login-form">
                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                                <?php endif;?>
                            </div>
                        </div>

                    <?php restore_current_blog(); ?>
 
                    <p class="txt-sm txt-medium padding-t-10">
                        Dont have an account?
                        <a href="https://www.saedconnect.org/register" class=" txt-color-blue">
                            Sign up
                        </a>
                    </p>
                </div>
                <?php } ?>
                <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
                    <h3 class="txt-bold txt-height-1-4 margin-b-15">
                        Share Job Post
                    </h3>
                    <?php echo do_shortcode('[Sassy_Social_Share]') ?>
                </div>
                <p class="txt-sm txt-medium padding-t-10">
                    <a href="" class=" txt-color-blue">
                        Report Job
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

<?php } ?>