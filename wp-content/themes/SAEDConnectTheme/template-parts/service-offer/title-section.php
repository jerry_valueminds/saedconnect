<div class="container-wrapper padding-t-40 border-b-1 border-color-darkgrey">
    <div class="row">
        <div class="col-md-8">
            <p class="txt-medium txt-color-lighter margin-b-10">
                Offer ID: <?php echo $post_id; ?>
            </p>
            <div class="">

                <!-- Offer Title -->
                <h1 class="txt-xlg txt-medium margin-b-5">
                    <?php echo $post_title ?>
                </h1>

                <!-- Industries -->
                <?php 
                    $term_list = wp_get_post_terms($post_id, 'nigerian-state', array("fields" => "names"));

                    if( $term_list ){
                ?>
                <p class="txt-sm">
                    <span class="txt-bold">Available in:</span>
                    <?php foreach( $term_list as $key => $term_name ){ ?>

                    <span>
                        <?php echo $term_name; ?>
                    </span>

                    <?php echo (  $key < ( count( $term_list ) - 1 ) ? ', ' : '' ) ?>

                    <?php } ?>
                </p>
                <?php } ?>

                <?php if( $current_user->ID == $post_author_id ){ ?>
                    <!-- Author Action -->
                    <p class="txt-sm margin-t-15">
                        <a
                            data-toggle="modal" href="#specificationModal"
                            class="txt-color-green"
                        >
                            Edit
                        </a>
                        <span class="padding-lr-5">|</span>
                        <a
                            href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', $dashboard_management_link, $post_id ) ?>" 
                            class="txt-color-red confirm-delete"
                        >
                            Delete
                        </a>
                    </p>
                <?php } ?>
            </div>
        </div>
        <?php if( is_user_logged_in() ){ ?>
        
        <div class="col-md-4 text-md-right">
        <?php if( $current_user->ID == $post_author_id ){ ?>
            <!-- Publish -->
            <a href="<?php echo $post_link.'?view=application-manager'; ?>" class="btn btn-blue txt-sm no-m-b">
                Publish
            </a>
        <?php } else { ?>
            <?php 
                
            ?>
            <?php if( $rendered_view == 'apply' ){ ?>
                <!-- Job Details -->
                <a href="<?php echo $post_link; ?>" class="btn btn-blue txt-sm no-m-b">
                    Back to Details
                </a>
            <?php } else { ?>
                <!-- Apply -->
                <?php  
                    $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                    $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                    $howToApply = get_post_meta( $post_id, 'how-to-apply', true );
                ?>
                
                <?php if( $applicationMethod == 'manual' ){ ?>
                    <a class="btn btn-blue txt-sm no-m-b" data-toggle="modal" href="#applyModal">
                        Apply
                    </a>
                <?php } elseif( $applicationMethod == '3rd-party' ) { ?>
                    <a class="btn btn-blue txt-sm no-m-b" data-toggle="modal" href="#applyModal">
                        Apply
                    </a>
                <?php } elseif( $applicationMethod == 'saedconnect' ) { ?>
                    <a href="<?php echo $post_link.'?view=apply'; ?>" class="btn btn-blue txt-sm no-m-b">
                        Apply
                    </a>            
                <?php } ?>
                
            <?php } ?>
        <?php } ?>
        </div>
        
        <?php } ?>
    </div>
    <h1 class="bg-ash txt-medium txt-color-white padding-o-15 margin-t-20">
        Starting from ₦<?php echo number_format( get_post_meta( $post_id, 'fee', true ) ); ?>
    </h1>
</div>