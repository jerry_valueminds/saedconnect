<div class="row row-40">
   <?php 
        global $main_content_relashionship_id;
        global $main_content_relationship_parent_id;
    
        $connected = new WP_Query( array(
            'relationship' => array(
                'id'   => $main_content_relashionship_id,
                'from' => $main_content_relationship_parent_id, // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
        while ( $connected->have_posts() ) : $connected->the_post();
    ?>

             <?php
                $content_type = rwmb_get_value( 'select-content-type' );
                if($content_type == 'gi-plain-text-meta-box'){ ?>

                    <!-- Text content -->
                    <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-lr-40">
                        <div class="margin-b-40">
                            
                            <article class="text-box">
                                <?php echo rwmb_get_value( 'plain-text-content' ); ?>
                            </article>
                            Plain text
                        </div>
                    </div>

            <?php } elseif($content_type == 'gi-accordion-meta-box'){ ?> 

                <!-- Accordion -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>  padding-lr-40">
                    <div class="margin-b-40">
                        <div class="faq-accordion margin-b-20">
                            <div id="accordion-<?php echo $post->ID ?>">
                                <?php 
                                    $values = rwmb_meta( 'accordion' );
                                    $accordionCounter = 1;

                                    foreach ( $values as $value ) { ?>

                                        <div class="card">
                                            <div class="card-header" id="heading-<?php echo $post->ID.'-'.$accordionCounter; ?>">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $post->ID.'-'.$accordionCounter; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $post->ID.'-'.$accordionCounter; ?>">
                                                        <?php echo $value['accordion-title']; ?>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapse-<?php echo $post->ID.'-'.$accordionCounter; ?>" class="collapse" aria-labelledby="heading-<?php echo $post->ID.'-'.$accordionCounter; ?>>" data-parent="#accordion-<?php echo $post->ID ?>" style="">
                                                <div class="card-body">
                                                    <article class="text-box sm">
                                                        <div class="txt-height-1-5">
                                                            <?php echo $value['accordion-content']; ?>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $accordionCounter++ ?>
                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } elseif($content_type == 'gi-button-group-meta-box'){ ?> 

                <!-- Button Group -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>  padding-lr-40">
                   <article class="btn-wrapper margin-b-40">
                        <?php $values = rwmb_meta('btn-group');
                            foreach ( $values as $value ) {
                                if($value['btn-type'] == 1){ ?>

                                    <a class="btn btn-trans-green margin-r-10 margin-b-10" href="<?php echo $value['btn-url']; ?>">
                                        <?php echo $value['btn-text']; ?>
                                    </a>

                                <?php }elseif($value['btn-type'] == 2){ ?>
                                    <a class="btn btn-trans-green margin-r-10 margin-b-10" href="<?php echo $value['btn-url']; ?>">
                                        <?php echo $value['btn-text']; ?>
                                    </a>
                                <?php }else{ ?>
                                    <a class="btn btn-trans-green margin-r-10 margin-b-10" href="<?php echo $value['btn-url']; ?>">
                                        <?php echo $value['btn-text']; ?>
                                    </a>
                                <?php }
                            }
                        ?>
                    </article>
                </div>
            <?php } elseif($content_type == 'gi-heading-meta-box'){ ?> 

                <!-- Heading -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>  padding-lr-40">
                   <div class="margin-b-20">
                        <h2 class="article-header padding-b-20 border-b-1 border-color-darkgrey">
                            <?php the_title(); ?>
                        </h2>
                    </div>
                </div>

            <?php } elseif($content_type == 'gi-article-meta-box'){ ?> 
                <!-- Article -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-lr-40 margin-b-30">
                    <?php

                        $images = rwmb_meta( 'article-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>

                    <a class="featured-article" href="<?php echo rwmb_get_value( 'article-link' ) ?>">
                        <figure class="image-box">
                            <img src="<?php echo $image['full_url']; ?>" alt="">
                            <?php $article_type = rwmb_get_value( 'select-article-type' ); ?>
                            <?php if($article_type == 'podcast'){ ?>
                                <figcaption class="caption-icon caption-podcast">
                                    <?php echo rwmb_get_value( 'category-text' ) ?>
                                </figcaption>
                            <?php }elseif($article_type == 'video'){ ?>
                                <figcaption class="caption-icon caption-video">
                                    <?php echo rwmb_get_value( 'category-text' ) ?>
                                </figcaption>
                            <?php }else{ ?>
                                <figcaption class="caption">
                                    <?php echo rwmb_get_value( 'category-text' ) ?>
                                </figcaption>
                            <?php } ?>
                        </figure>
                        <h4 class="title">
                            <?php the_title(); ?>
                        </h4>
                        <p class="date">
                            <?php rwmb_the_value( 'article-publication-date', array( 'format' => 'F  d\, Y' ) ); ?>
                        </p>
                    </a>
                </div>

             <?php } elseif($content_type == 'gi-link-meta-box'){ ?> 
                <!-- Link -->
                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?> padding-lr-40 margin-b-30">
                    <?php

                        $images = rwmb_meta( 'link-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>

                    <figure>
                        <img src="<?php echo $image['full_url']; ?>" alt="">
                    </figure>
                    <figcaption>
                        <h3 class="txt-medium padding-tb-10">
                            <a class="txt-color-blue" href="<?php echo rwmb_get_value( 'link-url' ) ?>"><?php the_title() ?></a>
                        </h3>
                        <p class="txt-normal-s txt-color-lighter">
                            <?php echo rwmb_get_value( 'link-description' ) ?>
                        </p>
                    </figcaption>
                </div>       

            <?php } ?>

    <?php
        endwhile;
        wp_reset_postdata();
    ?>
</div>