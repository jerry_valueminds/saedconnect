<?php

    global $side_content_relashionship_id;
    global $side_content_relationship_parent_id;

    $connected = new WP_Query( array(
        'relationship' => array(
            'id'   => $side_content_relashionship_id,
            'from' => $side_content_relationship_parent_id, // You can pass object ID or full object
        ),
        'nopaging'     => true,
    ) );
    while ( $connected->have_posts() ) : $connected->the_post();
        ?>

         <?php
            $content_type = rwmb_get_value( 'select-content-type' );
            if($content_type == 'gi-text-meta-box'){ ?>

                <div 
                   class="cta-block cta-blue">
                    <article>
                        <h2 class="title">
                            <?php the_title(); ?>
                        </h2>
                        <article class="content">
                            <p>
                                <?php echo rwmb_get_value( 'text_content' ); ?>
                            </p>
                        </article>
                        <div class="btn-wrapper">
                            <a class="cta-btn" href="">
                                Schedule an Appointment
                            </a>
                        </div>
                    </article>
                </div>

        <?php } elseif($content_type == 'gi-image-meta-box'){ ?> 

            <?php

                $images = rwmb_meta( 'mini_site_feature_image', array( 'limit' => 1 ) );
                $image = reset( $images );
            ?>


            <a class="feature-video-block" style="background-image:url('<?php echo $image['full_url']; ?>');">

            </a>
            
        <?php } elseif($content_type == 'gi-video-meta-box'){ ?> 

                <div class="col-md-<?php echo rwmb_get_value( 'select-column-width' ); ?>">
                    <?php

                        $images = rwmb_meta( 'video-feature-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>

                    <a class="feature-video-block popup-video" href="<?php echo rwmb_get_value( 'video-link' ); ?>" style="background-image:url('<?php echo $image['full_url']; ?>">
                        <span class="caption">
                            <h3 class="title txt-color-white">
                                <?php the_title() ?>
                            </h3>
                        </span>
                        <div class="play-btn">
                            <i class="fa fa-play"></i>
                        </div>      
                    </a>
                </div>

        <?php } elseif($content_type == 'gi-image-gallery-meta-box'){ ?> 

            <?php

                $images = rwmb_meta( 'image-gallery', array( 'size' => 'thumbnail' ) );
            ?>

            <div class="generic-gallery relative">
                <a class="swiper-wrapper" href="gallery">
                <?php foreach ( $images as $image ) { ?>  



                    <div class="swiper-slide">
                        <div class="home-slider-item">
                            <div
                                class="slide-bg"
                                style="background-image:url('<?php echo $image['full_url']; ?>');">
                            </div>
                        </div>
                    </div>


                <?php } ?>    
                </a>
                <!-- Slider Caption -->
                <div class="caption">
                    <h4>
                        <?php echo rwmb_get_value( 'image-galley-subtitle' ); ?>
                    </h4>
                    <h3 class="title">
                        <?php the_title() ?>
                    </h3>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>

        <?php } elseif($content_type == 'gi-text-gallery-meta-box'){ ?> 

            <?php $values = rwmb_meta('text-gallery'); ?>

            <div class="generic-gallery-text relative">
                <div class="swiper-wrapper">
                <?php foreach ( $values as $value ) { ?>  

                    <div class="swiper-slide">
                        <div class="slider-content">
                            <h3 class="title"><?php echo $value['text-slide-title']; ?></h3>
                            <p class="description">
                                <?php echo $value['text-slide-content']; ?>    
                            </p>
                            <div class="margin-t-30">
                                <a class="btn btn-trans-wb" href="<?php echo $value['text-slide-btn-url']; ?>">
                                    <?php echo $value['text-slide-btn-text']; ?>
                                </a>
                            </div>
                        </div>
                    </div>

                <?php } ?>    
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>   

        <?php } elseif($content_type == 'gi-quote-meta-box'){ ?> 

            <div class="cta-block bg-black txt-color-white">
                <div class="txt-3em txt-color-white padding-tb-20 border-t-4 border-color-white">
                    <i class="fa fa-quote-left"></i>
                </div>
                <h2 class="title">
                    <?php echo rwmb_get_value( 'quote-content' ); ?>
                </h2>
                <div class="txt-medium txt-height-1-2">
                    <?php echo rwmb_get_value( 'name-of-quoter' ); ?>
                    <br>
                    <?php echo rwmb_get_value( 'description-of-quoter' ); ?>
                </div>
                <div class="margin-t-30">
                    <a
                       class="btn btn-trans-wb"
                       href="<?php echo rwmb_get_value( 'quote-btn-link' ); ?>"
                       <?php
                            if(rwmb_get_value( 'quote-link-target' )=="new") echo "target='_blank'";
                       ?>
                       >
                        <?php echo rwmb_get_value( 'quote-btn-text' ); ?>
                    </a>
                </div>
            </div>

        <?php } elseif($content_type == 'gi-event-meta-box'){ ?> 

            <?php

                $images = rwmb_meta( 'event-image', array( 'limit' => 1 ) );
                $image = reset( $images );
            ?>


            <a class="feature-video-block" style="background-image:url('<?php echo $image['full_url']; ?>');">
                <span class="caption">
                    <h3 class="title txt-color-white">
                        <?php the_title(); ?>
                    </h3>
                </span>
                <div class="play-btn">
                    <i>
                        <div class="month" style="text-transform: uppercase;">
                            <?php rwmb_the_value( 'event-date', array( 'format' => 'M' ) ); ?>
                        </div>
                        <div class="day">
                            <?php rwmb_the_value( 'event-date', array( 'format' => 'd' ) ); ?>
                        </div>
                    </i>
                </div>   
            </a>

        <?php } elseif($content_type == 'gi-quicklinks-meta-box'){ ?> 

            <!-- Quick links -->
            <div class="right-content padding-t-40">
                <div class="border-t-4 border-color-black padding-tb-20">
                    <h2 class="title">
                        <?php the_title() ?>
                    </h2>
                    <article class="text-box">
                        <p>
                            <?php echo rwmb_get_value( 'quicklink-content' ); ?>
                        </p>
                    </article>
                    <ul class="icon-list blue">
                        <?php $values = rwmb_meta('quick-links');
                            foreach ( $values as $value ) { ?>
                                <li>     
                                    <a href="<?php echo $value['quicklink-url']; ?>">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            <?php echo $value['link-text']; ?>    
                                        </span>
                                    </a>
                                </li>
                        <?php } ?>
                    </ul>  
                </div>
            </div>

        <?php } elseif($content_type == 'gi-pft-meta-box'){ ?> 
            <?php

                $images = rwmb_meta( 'pft-image', array( 'limit' => 1 ) );
                $image = reset( $images );
            ?>

            <!-- Team -->
            <div class="cta-block bg-black-off">
                <article>
                    <h3 class="subtitle txt-color-white-off margin-b-30">
                        <?php echo rwmb_get_value( 'pft-department' ); ?>
                    </h3>
                    <figure class="margin-b-20">
                        <img class="profile-image" src="<?php echo $image['full_url']; ?>" alt="" width="60">
                    </figure>
                    <h2 class="title txt-color-white">
                        <?php the_title() ?>
                    </h2>
                    <article class="content txt-color-white">
                        <p>
                            <?php echo rwmb_get_value( 'pft-description' ); ?>
                        </p>
                    </article>
                    <div class="btn-wrapper">
                        <a class="btn btn-trans-gg" href="<?php echo rwmb_get_value( 'pft-url' ); ?>">
                            <?php echo rwmb_get_value( 'pft-button' ); ?>
                        </a>
                    </div>
                </article>
            </div>

            <?php } elseif($content_type == 'gi-cta-2-meta-box'){ ?> 

                <?php

                    $images = rwmb_meta( 'cta-2-background-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                ?>

                <article class="feature-image-block" style="background-image:url('<?php echo $image['full_url']; ?>">
                    <div class="content txt-color-white">
                        <h4 class="margin-b-30"><?php echo rwmb_get_value( 'cta-2-subtitle' ); ?></h4>
                        <h3 class="txt-xxlg txt-medium margin-b-30">
                            <?php the_title(); ?>
                        </h3>
                        <article class="btn-wrapper">
                            <?php $values = rwmb_meta('cta-2-btns');
                                foreach ( $values as $value ) {
                                    if($value['footer-btn-type'] == 1){ ?>

                                        <a class="btn btn-trans-wb icon margin-r-10 margin-b-10" href="<?php echo $value['footer-btn-url']; ?>">
                                            <?php echo $value['footer-btn-text']; ?>
                                        </a>

                                    <?php }elseif($value['footer-btn-type'] == 2){ ?>
                                        <a class="btn btn-trans-wb margin-r-10 margin-b-10" href="<?php echo $value['footer-btn-url']; ?>">
                                            <?php echo $value['footer-btn-text']; ?>
                                        </a>
                                    <?php }else{ ?>
                                        <a class="btn btn-white margin-r-10 margin-b-10" href="<?php echo $value['footer-btn-url']; ?>">
                                            <?php echo $value['footer-btn-text']; ?>
                                        </a>
                                    <?php }
                                }
                            ?>
                        </article>
                    </div>   
                </article>


        <?php } ?>

<?php
    endwhile;
    wp_reset_postdata();

?>