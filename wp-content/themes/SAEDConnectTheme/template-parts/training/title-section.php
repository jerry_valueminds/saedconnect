<div class="container-wrapper padding-t-40 padding-b-40 margin-b-20 border-b-1 border-color-darkgrey bg-grey">
    <?php if( $current_user->ID == $post_author_id ){ ?>
    <p class="margin-b-15">
        <a href="https://www.saedconnect.org/service-provider-directory/my-training-offers/" class="txt-normal-s">
            <i class="fa fa-arrow-left"></i>
            All Courses
        </a>
    </p>
    <?php } ?>
    <div class="row row-15">
        <div class="order-1 order-md-2 col-md-8 padding-lr-15 padding-b-20">
           <p class="txt-medium txt-color-lighter margin-b-10">
                Course ID: <?php echo $post_id; ?>
            </p>
            <h1 class="txt-2em txt-medium txt-height-1-1 margin-b-10">
                <?php echo $post_title ?>
            </h1>
            <p class="txt-sm txt-medium">
                <?php echo get_post_meta( $post_id, 'summary', true ) ?>
            </p>
            <?php if( $current_user->ID == $post_author_id ){ ?>
                <!-- Author Action -->
                <p class="txt-sm margin-t-20">
                    <a
                        data-toggle="modal" href="#specificationModal"
                        class="txt-color-green"
                    >
                        <i class="fa fa-pencil"></i>
                        Edit
                    </a>
                    <span class="padding-lr-5">|</span>
                    <a
                        href="<?php echo currentUrl(true).'?view=form-delete'; ?>" 
                        class="txt-color-red confirm-delete"
                    >
                        <i class="fa fa-trash"></i>
                        Delete
                    </a>
                </p>
            <?php } ?>
            
            
            
            
            
            
            <?php if( is_user_logged_in() ){ ?>
        
                <div class="padding-t-20">
                <?php
                                                
                    $nysc_saved_meta = get_user_meta($current_user->ID, 'nysc_assigned_state');
                    if( $nysc_saved_meta ){ 
                ?>
                    <div class="d-inline-block dropdown show">
                        <a class="btn btn-green txt-xs no-m-b dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            NYSC Admin Tools
                        </a>

                        <div class="dropdown-menu txt-sm" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" data-toggle="modal" href="#adminMessageModal">Send Admin Message</a>
                            <a class="dropdown-item confirmation" href="<?php echo currentUrl(true).'?view=form-admin-approval'; ?>">
                                <?php
                                    $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                    if($publication_meta == 'user_published'){
                                        echo 'Publish';
                                    } elseif($publication_meta == 'admin_published') {
                                        echo 'Unpublish';
                                    }else{
                                        echo 'Draft';
                                    }
                                ?>
                            </a>
                            <a class="dropdown-item" data-toggle="modal" href="#adminDenyApprovalMessageModal">Decline</a>
                        </div>
                    </div>
                <?php } ?>
                
                <?php if( $current_user->ID == 1){ ?>
                    <div class="d-inline-block dropdown show">
                        <a class="btn btn-blue txt-xs no-m-b dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Admin Tools
                        </a>

                        <div class="dropdown-menu txt-sm" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" data-toggle="modal" href="#adminMessageModal">Send Admin Message</a>
                            <a class="dropdown-item confirmation" href="<?php echo currentUrl(true).'?view=form-admin-approval'; ?>">
                                <?php
                                    $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                    if($publication_meta == 'user_published'){
                                        echo 'Publish';
                                    } elseif($publication_meta == 'admin_published') {
                                        echo 'Unpublish';
                                    }else{
                                        echo 'Draft';
                                    }
                                ?>
                            </a>
                            <a class="dropdown-item" data-toggle="modal" href="#adminDenyApprovalMessageModal">Decline</a>
                        </div>
                    </div>
                    
                <?php } else { ?>
                    <?php if( $rendered_view == 'apply' ){ ?>
                        <!-- Job Details -->
                        <a href="<?php echo $post_link; ?>" class="btn btn-blue txt-sm no-m-b">
                            Back to Details
                        </a>
                    <?php } else { ?>
                        <!-- Apply -->
                        <?php  
                            $applicationMethod = get_post_meta( $post_id, 'application-method', true );
                            $applicationLink = get_post_meta( $post_id, '3rd-party-application-link', true );
                            $howToApply = get_post_meta( $post_id, 'how-to-apply', true );
                        ?>

                        <?php if( $applicationMethod == 'manual' ){ ?>
                            <a class="btn btn-blue txt-sm no-m-b" data-toggle="modal" href="#applyModal">
                                Apply
                            </a>
                        <?php } elseif( $applicationMethod == '3rd-party' ) { ?>
                            <a class="btn btn-blue txt-sm no-m-b" data-toggle="modal" href="#applyModal">
                                Apply
                            </a>
                        <?php } elseif( $applicationMethod == 'saedconnect' ) { ?>
                            <a href="<?php echo $post_link.'?view=apply'; ?>" class="btn btn-blue txt-sm no-m-b">
                                Apply
                            </a>            
                        <?php } ?>

                    <?php } ?>
                <?php } ?>
                <?php if( $current_user->ID != $post_author_id ){ ?>
                    <a class="btn btn-ash txt-xs no-m-b" data-toggle="modal" href="#messageModal">Contact</a>
                <?php } ?>
                </div>

            <?php } ?>
        </div>
        <div class="order-2 order-md-1 col-md-4 padding-lr-15 position-relative">
            <?php $images = get_attached_media( 'image', $post_id ); ?>
            
            <?php if($images){ ?>
           
                <?php  
                    foreach($images as $image) { //print_r( $image ); 
                        $previousImg_id = $image->ID;
                ?>
                    <figure class="col border-o-1 border-color-darkgrey bg-white" style="height: 300px;background-position: center;background-repeat: no-repeat;background-size: contain;background-image: url('<?php echo wp_get_attachment_url($image->ID,'full'); ?>')">
                        
                    </figure>
                
                <?php } ?>
                
            <?php } else { ?>
                <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey bg-white">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png" width="150" class="margin-b-60">
                </figure>
                
            <?php } ?>
            <?php if( $current_user->ID == $post_author_id ){ ?>
            <div class="upload-btn">
                <a class="btn btn-blue txt-xs" data-toggle="modal" href="#featuredImageModal">
                     <?php echo ($images)? "Change Image" : "+ Add"; ?>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<style>
    .upload-btn{
        position: absolute;
        text-align: center;
        left: 0;
        width: 100%;
        bottom: 15px;
    }
</style>