<?php if( $rendered_view != 'application-manager' ) { ?>

    <div class="col-md-4 padding-lr-20">
        <figure class="margin-b-20">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/Banners/01.jpg" alt="">
        </figure>
    <?php if( $current_user->ID == $post_author_id ) { ?>
        <div class="padding-o-20 border-o-1 border-color-blue">
            <div class="txt-normal-s txt-medium text-center margin-b-20">
               NYSC Accreditation
            </div>
            <!--<div class="bg-blue txt-color-white padding-o-10 rounded-o-12 text-center margin-b-15">
                <?php
                    $accreditation_meta = maybe_unserialize( get_post_meta( $post_id, $accreditation_key, true ) );

                    if($accreditation_meta['status'] == 'user_published'){
                        echo 'Undergoing Review';
                    } elseif($accreditation_meta['status'] == 'admin_published') {
                        echo 'Published';
                    }else{
                        echo 'Draft';
                    }
                ?>
            </div>-->
            <?php if( false/*$accreditation_meta['status']*/){ ?>

                <p class="txt-sm text-center">
                   You have requested accreditation in 
                   <span class="txt-medium"><?php echo $accreditation_meta['state']; ?></span> 
                   state.
                </p>

            <?php }else{ ?>
                <p class="txt-xs text-center padding-b-15 margin-b-15">
                   Become recognized as a NYSC approved trainer under the SAED Program. Corp members who attend a training offered by an accredited SAED trainer are eligible to receive a special SAED certificate at the end of their service year.
                </p>
                <form class="" method="post" action="<?php echo currentUrl(true).'?view=form-nysc-accreditation'; ?>">
                    <div style="margin-bottom:10px">
                        <div class="padding-t-20 margin-b-40">
                        <?php 

                            /*
                            *
                            * Populate Form Data from Terms
                            *
                            */
                            //Get Terms
                            $terms = get_terms( 'state', array('hide_empty' => false));

                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                // Check and see if the term is a top-level parent. If so, display it.
                                $term_id = $term->term_id; //Get the term ID
                                $term_name = $term->name; //Get the term name
                                $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term_id, true );
                        ?>
                            <?php if( $saved_meta ){ ?>

                            <div class="txt-sm txt-medium d-flex justify-content-between align-items-center margin-b-20">
                                <span>
                                    <?php echo $term_name; ?>
                                    <span class="txt-color-yellow-dark">
                                    (
                                    <?php

                                        if($saved_meta == 'user_published'){
                                            echo 'In Review';
                                        } elseif($saved_meta == 'admin_published') {
                                            echo 'Published';
                                        }
                                    ?>
                                    )
                                    </span>
                                </span>
                                <a href="" class="txt-color-blue">Cancel</a>
                            </div>

                            <?php } ?>
                        <?php } ?>
                    </div>
                        <a data-toggle="collapse" href="#trainerProfileSelectStates" aria-expanded="false" class="d-flex">
                            <span class="flex_1 padding-o-10 d-flex justify-content-between txt-normal-s bg-blue txt-color-white">
                                <label for="state">Request Accreditation</label>
                                <i class="fa fa-angle-down"></i>
                            </span>

                        </a>
                        <div id="trainerProfileSelectStates" class="collapse" aria-labelledby="headingOne">
                            <div class="padding-t-20">
                                <p class="txt-sm padding-b-15">Select the state(s) where you want to be accredited</p>
                                <?php 

                                    /*
                                    *
                                    * Populate Form Data from Terms
                                    *
                                    */
                                    //Get Terms
                                    $terms = get_terms( 'state', array('hide_empty' => false));

                                    foreach ($terms as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                        $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term_id, true );
                                ?>
                                    <?php if( $saved_meta ){ ?>


                                    <?php }else{ ?>
                                    <label class="txt-sm d-flex align-items-center padding-b-10">
                                        <input
                                            class="margin-r-5 icheck"
                                            type="checkbox" 
                                            value="<?php echo $term_id ?>" 
                                            name="state[]" 
                                            <?php echo ( $saved_meta ) ? "checked" : "" ?>
                                        >
                                        <span class="bg-label padding-l-5" style="padding:5px 10px !important;">
                                            <?php echo $term_name; ?>

                                        </span>
                                    </label>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <div class="text-center padding-t-10">
                                <input type="submit" value="Request Accredition" class="btn btn-blue txt-xs txt-medium no-m-b">
                            </div>
                        </div>
                    </div>

               </form>
            <?php } ?>
        </div>
        <div class="padding-o-20 bg-grey margin-b-20">
            <h3 class="txt-bold txt-height-1-4 margin-b-15">
                Recieve &amp; Manage Applications for this Job on SAEDConnect
            </h3>
            <p class="txt-height-1-7 txt-sm margin-b-20">
                Recieve &amp; Manage Applications for this Job on SAEDConnect Recieve &amp; Manage Applications for this Job on SAEDConnect
            </p>
            <div>
                <a href="" class="btn btn-blue txt-xs no-m-b">
                    Get started
                </a>
            </div>
        </div>
        <div class="padding-o-20 bg-ash margin-b-20">
            <span class="d-inline-flex align-items-center padding-r-40">
                <input type="checkbox" id="minimal-checkbox-3" name="radio-2" value="beginner">
                <label for="minimal-radio-3" class="txt-color-white padding-l-10 txt-medium">Add a Program Brochure</label>
            </span>
        </div>
        <div class="padding-o-20 bg-ash margin-b-20">
            <h3 class="txt-bold txt-color-white txt-height-1-4 margin-b-15">
                Advertise this Job
            </h3>
            <ul class="icon-list white txt-sm">
                <li>
                    <a href="http://www.saedconnect.org/contribute/synopsis/corporate/">
                        <i class="fa fa-chevron-right"></i>
                        <span>Feature on Homepage</span>
                    </a>
                </li>
                <li>
                    <a href="http://www.saedconnect.org/contribute/synopsis/schools-youth-development/">
                        <i class="fa fa-chevron-right"></i>
                        <span>Feature in Jobs Newsletter</span>
                    </a>
                </li>
                <li>
                    <a href="http://www.saedconnect.org/contribute/synopsis/government-donor-agencies/">
                        <i class="fa fa-chevron-right"></i>
                        <span>
                            Send a standalaone broadcast
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    <?php } ?>
      
    <?php if( !is_user_logged_in() ){ ?>
        <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
            <h3 class="txt-bold txt-height-1-4 margin-b-15">
                Login to Apply Now
            </h3>

            <!-- Login Form -->
            <style>
                .gfield_checkbox label, .login-form .gfield_label{
                    display: block !important;
                }

                .gfield_checkbox li{
                    display: flex;
                    align-items: center;
                }

                .gfield_checkbox input{
                    margin: 0 !important;
                    margin-right: 10px !important;
                }
            </style>

            <?php switch_to_blog(1); ?>

                <!-- Login Tab -->
                <div class="login">
                    <div class="login-form">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                        <?php endif;?>
                    </div>
                </div>

            <?php restore_current_blog(); ?>

            <p class="txt-sm txt-medium padding-t-10">
                Dont have an account?
                <a href="https://www.saedconnect.org/register" class=" txt-color-blue">
                    Sign up
                </a>
            </p>
        </div>
    <?php } ?>
       
        <div class="padding-o-20 border-o-1 border-color-darkgrey margin-b-20">
            <h3 class="txt-bold txt-height-1-4 margin-b-15">
                Share Job Post
            </h3>
            <p class="txt-sm">
                <a href="">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-facebook fa-stack-1x fa-inverse txt-color-white"></i>
                    </span>
                </a>
                <a href="">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-twitter fa-stack-1x fa-inverse txt-color-white"></i>
                    </span>
                </a>
                <a href="">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-instagram fa-stack-1x fa-inverse txt-color-white"></i>
                    </span>
                </a>
                <a href="">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-linkedin fa-stack-1x fa-inverse txt-color-white"></i>
                    </span>
                </a>
            </p>
        </div>
        <p class="txt-sm txt-medium padding-t-10">
            <a href="" class=" txt-color-blue">
                Report Job
            </a>
        </p>
    </div>
    
<?php } ?>