<?php if( $current_user->ID == $post_author_id ){ ?>

<style>
    .menu{
    }

    .menu li{
        display: inline;

    }

    .menu li a{
        display: inline-block;
        font-size: 0.9em;
        padding: 20px 20px;
        color: white;
    }

    .menu li a.active{
        background-color: white;
        color: black;
    }
    
    .dropdown-item {
        padding: .5rem 1.5rem;
    }   
    
    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #828181;
    }
</style>

<!-- Desktop -->
<div class="container-wrapper bg-ash d-none d-md-block">
    <ul class="menu">
        <li>
            <a href="<?php echo $post_link; ?>" class="<?php echo ( $rendered_view == "" ) ? "active" : "" ?>">
                Course Details
            </a>
        </li>
        <li>
            <a href="<?php echo $post_link.'/?view=application-manager'; ?>" class="<?php echo ( $rendered_view == "application-manager" ) ? "active" : "" ?>">
                Student Manager
            </a>
        </li>
        <li>
            <a href="<?php echo $post_link.'/?view=messages'; ?>" class="<?php echo ( $rendered_view == "messages" ) ? "active" : "" ?>">
                Messages
            </a>
        </li>
        <!--<li>
            <a href="<?php echo $post_link.'/?view=preview'; ?>" class="<?php echo ( $rendered_view == "preview" ) ? "active" : "" ?>">
                Preview Training
            </a>
        </li>-->
    </ul>
</div>

<!-- Mobile -->
<div class="d-md-none">
    <div class="dropdown show">
        <a class="bg-ash d-flex align-items-center justify-content-between" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="txt-medium txt-color-white dropdown-toggle col d-flex align-items-center justify-content-between padding-o-15">
                <?php 
                    if($rendered_view == "")
                        echo "Course Details";
                    elseif($rendered_view == "application-manager")
                        echo "Student Manager";
                    elseif($rendered_view == "messages")
                        echo "Messages";            
                ?>
            </span>
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">            
            <a href="<?php echo $post_link; ?>" class="dropdown-item <?php echo ( $rendered_view == "" ) ? "active" : "" ?>">
                Course Details
            </a>
            <a href="<?php echo $post_link.'/?view=application-manager'; ?>" class="dropdown-item <?php echo ( $rendered_view == "application-manager" ) ? "active" : "" ?>">
                Student Manager
            </a>
            <a href="<?php echo $post_link.'/?view=messages'; ?>" class="dropdown-item <?php echo ( $rendered_view == "messages" ) ? "active" : "" ?>">
                Messages
            </a>
        </div>
    </div>
</div>

<?php } ?>