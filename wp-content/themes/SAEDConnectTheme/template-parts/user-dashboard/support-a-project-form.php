<?php
    // Get Base URL
    $base_url = get_site_url().'/my-dashboard';

    // Get Currently logged in User
    $current_user = wp_get_current_user();
?>

<div class="col-md-12 mx-auto padding-tb-20 padding-lr-30 esaed-form">
    <!-- SUPPORT A PROJECT FORM -->
    <?php
        echo do_shortcode( "[gravityform id='35' title='false' description='false' ajax='false' field_values='post-title=User-".$current_user->ID."' ]"); 
    ?>
</div>