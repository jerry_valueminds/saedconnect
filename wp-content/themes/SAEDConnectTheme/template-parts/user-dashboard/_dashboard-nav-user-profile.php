<?php
    /* Get current user */
    $current_user = wp_get_current_user();
    $user_id = $_GET['user-id'];

    /* Get Avatar */
    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
    $meta_key = 'user_avatar_url';
    $get_avatar_url = get_user_meta($user_id, $meta_key, true);

    if($get_avatar_url){
        $avatar_url = $get_avatar_url;
    }       
?>

<div class="dashboard-multi-main-menu-light">
    <figure class="avatar">
        <div class="img" style="background-image: url('<?php echo $avatar_url ?>')"></div>
    </figure>
    <div class="user-info-wrapper">
        <div class="user-info">
            <div class="name" style="padding: 0.5em 0.75em;">
                <?php
                    switch_to_blog(1);

                    $gf_id = 4; //Form ID
                    $gv_id = 1445; //Gravity View ID

                    $entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $user_id, //Current logged in user
                            )
                        )
                    );

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria ); 

                    if(!$entry_count){ //If no entry

                    } else {
                        foreach( $entries as $entry ){
                            echo '@'.rgar( $entry, 1 );
                        }
                    }

                    restore_current_blog(); 
                ?>            
            </div>
        </div>
    </div>
    <nav class="nav-wrapper">
        <ul class="cv-menu-list">
            <!--<li>
                <a href="https://www.saedconnect.org/user-profile/?user-id=<?php echo $user_id ?>">
                    My Profile                 
                </a>
            </li>-->
            <li>
                <a href="https://www.saedconnect.org/competency-profile/user-cv/?user-id=<?php echo $user_id ?>">
                    My CV                
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/service-provider-directory/trainer-preview/?user-id=<?php echo $user_id ?>">
                    My Trainer Profile                 
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/service-provider-directory/user-courses/?user-id=<?php echo $user_id ?>">
                    My Courses                 
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/ventures-directory/user-businesses/?user-id=<?php echo $user_id ?>">
                    My Businesses              
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/ventures-directory/user-projects/?user-id=<?php echo $user_id ?>">
                    My Projects              
                </a>
            </li>
        </ul>
    </nav>
</div>

<!-- Message Modal -->
<div class="modal fade font-main filter-modal" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo currentUrl(false).'&view=form-message'; ?>" method="post">
                <div class="modal-header padding-lr-30">
                    <h5 class="modal-title" id="exampleModalLabel">Send a Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body padding-o-30 form">
                    <?php
                        /* Meta Key */
                        $redirect_link = currentUrl(true).'?user-id='.$_GET['user-id'].'&state-id='.$_GET['state-id'];
                        global $wpdb; //Include WP Global Object
                        $message_db = new wpdb('root','umMv65ekyMRxfNfm','messages','localhost');

                        /*
                        *
                        * Send Message
                        *
                        */
                        if($_POST && $_GET['view'] == 'form-message'){
                            
                            /* Message Data */
                            $sender = $current_user->ID;
                            $receiver = $_GET['user-id'];
                            $parent_message = 0;
                            $type = 'user';
                            $subject = sanitize_text_field( $_POST['subject'] );
                            $message = wp_kses_post( $_POST['message'] );
                            $connected_post = 0;
                            $connected_post_site = 0;
                            $read_status_sender = 'unread';
                            $read_status_receiver = 'unread';
                            
                            $message_db->insert( 
                                'messages', 
                                array( 
                                    "sender" => $sender,
                                    "receiver" => $receiver,
                                    "parent_message" => $parent_message,
                                    "type" => $type,
                                    "subject" => $subject,
                                    "content" => $message,
                                    "connected_post" => $connected_post,
                                    "connected_post_site" => $connected_post_site,
                                    "read_status_sender" => $read_status_sender,
                                    "read_status_receiver" => $read_status_receiver,
                                ), 
                                array( "%d", "%d", "%d", "%s", "%s", "%s", "%d", "%d", "%s", "%s" ) 
                            );
                            
                            /* Send Notification */
                            $message = 'New have a new message ';

                            $to = $user_info->user_email;
                            $subject = 'SAEDConnect: You have a new message';

                            wp_mail( $to, $subject, $message );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>

                    <!-- Title -->
                    <div class="form-item">
                        <label for="subject">
                            Subject
                        </label>
                        <input 
                            type="text" 
                            name="subject"
                            required
                        >
                    </div>
                    
                    <!-- Summary -->
                    <div class="form-item">
                        <label for="message">
                            Your Message
                        </label>
                        <textarea class="editor" name="message" cols="30" rows="4"></textarea>
                    </div>
                </div>
                <div class="modal-footer padding-lr-30">
                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                </div>
            </form>
        </div>
    </div>
</div>
