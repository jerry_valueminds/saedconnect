<main class="main-content txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper margin-b-20">
        <div class="row">
            <div class="col-md-6">
                <h1 class="txt-2em txt-bold margin-b-30">
                    My Programs
                </h1>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
    
    <section class="container-wrapper margin-b-20">
        <div class="container-wrapper bg-white padding-tb-80">
        <?php
            switch_to_blog(12);
        
            $gf_id = 1;
        
            $entry_count = 0;

            /* Get current User ID */
            $current_user = wp_get_current_user();

            /* GF Search Criteria */
            $search_criteria = array(

            'field_filters' => array( //which fields to search

                array(

                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                )
              )
            );

            /* Get GF Entry Count */
            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

            
            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
        
            foreach( $entries as $entry ){
        ?>
                <div class="row row-10 border-b-1 padding-b-20 margin-b-20">
                    <div class="col-md-12 padding-lr-10 margin-b-20">
                        <p class="txt-lg txt-bold">
                            <?php echo rgar( $entry, '1' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-12 padding-lr-10 margin-b-20">
                        <p class="txt-lg txt-bold">
                            <img src="<?php echo rgar( $entry, '12' ); ?>" alt="">
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Brochure Download Link
                        </p>
                        <p>
                            <?php echo rgar( $entry, '14' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Application Link
                        </p>
                        <p>
                            <?php echo rgar( $entry, '16' ); ?>
                        </p>
                    </div>

                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Powered by
                        </p>
                        <p>
                            <?php
                
                                $field_id = 2; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Opportunity Tagline (If any)
                        </p>
                        <p>
                            <?php
                
                                $field_id = 3; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Fee
                        </p>
                        <p>
                            <?php echo rgar( $entry, '4' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Opens
                        </p>
                        <p>
                            <?php echo rgar( $entry, '6' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Expires
                        </p>
                        <p>
                            <?php echo rgar( $entry, '7' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Event Start Date
                        </p>
                        <p>
                            <?php echo rgar( $entry, '8' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Event End Date
                        </p>
                        <p>
                            <?php echo rgar( $entry, '10' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Short Description
                        </p>
                        <p>
                            <?php echo rgar( $entry, '13' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Website Link (if any)
                        </p>
                        <p>
                            <?php echo rgar( $entry, '15' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Venue
                        </p>
                        <p>
                            <?php echo rgar( $entry, '17' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Time
                        </p>
                        <p>
                            <?php echo rgar( $entry, '18' ); ?>
                        </p>
                    </div>
                    
                    <!-- Header -->
                    <div class="col-md-12 padding-lr-10 margin-b-20">
                        <p class="txt-lg txt-bold">
                            Opportunity Information
                        </p>
                    </div>
                    
                    <div class="col-md-12 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Full Opportunity Description
                        </p>
                        <p>
                            <?php echo rgar( $entry, '21' ); ?>
                        </p>
                    </div>
                    
                    <!-- Header -->
                    <div class="col-md-12 padding-lr-10 margin-b-20">
                        <p class="txt-lg txt-bold">
                            Organizer Contact
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Email
                        </p>
                        <p>
                            <?php echo rgar( $entry, '22' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Telephone
                        </p>
                        <p>
                            <?php echo rgar( $entry, '24' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            WhatsApp
                        </p>
                        <p>
                            <?php echo rgar( $entry, '25' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Facebook
                        </p>
                        <p>
                            <?php echo rgar( $entry, '26' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Twitter
                        </p>
                        <p>
                            <?php echo rgar( $entry, '28' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Instagram
                        </p>
                        <p>
                            <?php echo rgar( $entry, '27' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-12 padding-lr-10">
                        <a 
                           href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="637" action="edit" return="url" /]');  ?>" 
                           class="btn btn-blue txt-xs no-m-b"
                        >
                            Edit
                        </a>
                    </div>
            </div>
        <?php
            }
        
            restore_current_blog();
        ?>
        </div>
    </section>
</main>