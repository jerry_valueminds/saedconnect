<?php 

    get_header('user-dashboard');

    $permalink_esc;

    if( $_REQUEST['status'] == 'deleted' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv' );
?>
   
    <script>
        window.location.replace( "<?php echo $permalink_esc; ?>" );
    </script>
    
<?php } ?>

<?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>

<?php
    /* Reset Global Queried Object  */
    wp_reset_postdata();
    wp_reset_query();
   
    while ( have_posts() ) : the_post();
        get_template_part( 'template-parts/user-dashboard/cv' );
    endwhile; // end of the loop.

    wp_footer('user-dashboard');

?>
