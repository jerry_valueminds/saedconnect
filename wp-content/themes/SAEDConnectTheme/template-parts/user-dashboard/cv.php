<?php
       
    $base_cv_url = 'https://www.saedconnect.org/competency-profile/my-dashboard/';

    /* CV Array Links */
    $cv_links = array(
        array(
            'title' => 'Personal Information',
            'template' => 'personal-information',
            'gravityFormID' => 84,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Name',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Gender',
                    'field_id' => 2,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Yeah of Birth',
                    'field_id' => 3,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Marital Status',
                    'field_id' => 21,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Nationality',
                    'field_id' => 4,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'State of Origin',
                    'field_id' => 5,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Location in Nigeria where you currently reside',
                    'field_id' => 6,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'States in Nigeria where you can work',
                    'field_id' => 7,
                    'type' => 'checkbox'
                ),
                
                array(
                    'label' => 'Preferred Contact Telephone Number (For Work)',
                    'field_id' => 8,
                    'type' => 'checkbox'
                ),
                
                array(
                    'label' => 'Preferred Contact Whatsapp Number (For Work)',
                    'field_id' => 9,
                    'type' => 'checkbox'
                ),
                
                array(
                    'label' => 'Preferred Contact Email Address (For Work)',
                    'field_id' => 10,
                    'type' => 'checkbox'
                ),
                
                array(
                    'label' => 'Highest Level of Education',
                    'field_id' => 11,
                    'type' => 'checkbox'
                ),
            )
        ),
        
        array(
            'title' => 'Summary',
            'template' => 'summary',
            'gravityFormID' => 27,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Headline',
                    'field_id' => 4,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Summary about yourself',
                    'field_id' => 5,
                    'type' => 'textarea'
                ),
            )
        ),

        array(
            'title' => 'Accomplishments',
            'template' => 'accomplishments',
            'gravityFormID' => 33,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 3,
                    'type' => 'textarea'
                ),
            )
        ),

        array(
            'title' => 'Additional Information',
            'template' => 'additional-information',
            'gravityFormID' => 42,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                /*array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),*/
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Certifications / Affiliations',
            'template' => 'certifications-affiliations',
            'gravityFormID' => 35,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Time Period',
                    'field_id' => 9,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Certification Expiration',
                    'field_id' => 10,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Valid from',
                    'field_id' => 11,
                    'type' => 'date'
                ),
                
                array(
                    'label' => 'To',
                    'field_id' => 12,
                    'type' => 'date'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Social Details',
            'template' => 'contact-details',
            'gravityFormID' => 28,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Email',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Phone',
                    'field_id' => 12,
                    'type' => 'text'
                ),
                                
                array(
                    'label' => 'Address',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Current State of Residence',
                    'field_id' => 14,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Personal Blog/Website',
                    'field_id' => 15,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Facebook',
                    'field_id' => 16,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Twitter',
                    'field_id' => 17,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Instagram',
                    'field_id' => 18,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Linkedin',
                    'field_id' => 19,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Education & Training',
            'template' => 'education-training',
            'gravityFormID' => 31,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Degree Attained (e.g BA, BS, JD, PhD)',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'School',
                    'field_id' => 8,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Start Month/Year',
                    'field_id' => 10,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'End Month/Year (Current students: Enter your expected graduation year)',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Achievements',
                    'field_id' => 12,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Experiences',
            'template' => 'experiences',
            'gravityFormID' => 29,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Position/Job Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Company/Company Website',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Location',
                    'field_id' => 10,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Start Month/Start Year',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'End Month/Year',
                    'field_id' => 12,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Describe what you did/Your Achievements',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Interests',
            'template' => 'Interests',
            'gravityFormID' => 39,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Languages',
            'template' => 'languages',
            'gravityFormID' => 40,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Language Name',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'References',
            'template' => 'references',
            'gravityFormID' => 41,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Name of Referee',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Position',
                    'field_id' => 7,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Email Address',
                    'field_id' => 8,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Telephone Number',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Reference Available on Request?',
                    'field_id' => 10,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Skills',
            'template' => 'skills',
            'gravityFormID' => 34,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Skill Name',
                    'field_id' => 4,
                    'type' => 'text'
                ),
            )
        ),
    );
    
    /* Get currently active CV from Query String  */
    $current_cv_view = $_REQUEST['cv-view'];
    $cv_form = $_REQUEST['cv-form'];

    /* Get current User ID */
    $current_user = wp_get_current_user();
?>

<main class="main-content txt-color-light bg-white" style="margin-top: 70px">
    <section class="row">
        <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>

        <div class="dashboard-multi-main-content full">
            <div class="page-header">
                <h1 class="page-title">
                    My CV
                </h1>
            </div>
            <article class="page-summary">
                <p>
                    Your Personal Profile is the Key information about you which would be required by potential employers
                </p>
            </article>
            <div class="row row-15">
                <div class="col-md-8 padding-lr-15">
            
                    <?php
                        /* If Add form */
                        if ( $cv_form ){
                    ?>

                        <div class="section-wrapper">
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    Add <?php echo $_REQUEST['cv-part']; ?>          
                                </h2>
                                <div class="text-right">
                                    <a 
                                        href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv" 
                                        class="edit-btn"
                                    >
                                        Cancel
                                    </a>
                                </div>
                            </div>

                            <div class="entry">
                                <?php 
                                    echo do_shortcode( "[gravityform id='".$cv_form."' title='false' description='false' ajax='false']"); 
                                ?>
                            </div>
                        </div>

                    <?php
                        /* Else view template */    
                        } elseif( $_REQUEST['view'] == 'cv' ) {

                    ?>

                       <div class="section-wrapper">
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    Edit <?php echo $_REQUEST['cv-form-title']; ?>          
                                </h2>
                                <div class="text-right">
                                    <a 
                                        href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv" 
                                        class="edit-btn"
                                    >
                                        Cancel
                                    </a>
                                </div>
                            </div>

                            <div class="entry">
                               <?php the_content() ?>
                            </div>
                        </div>

                    <?php
                        /* Else view template */    
                        } else {

                    ?>

                        <!-- Personal Information -->
                        <div class="section-wrapper">
                        <?php
                            $gf_id = 84; //Form ID
                            $gv_id = 1116; //Gravity View ID
                            $title = 'Personal Information';

                            $entry_count = 0;

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    )
                                )
                            );

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria ); 
                        ?>

                            <?php if(!$entry_count){ //If no entry ?>

                                    <div class="header">
                                        <h2 class="section-wrapper-title">
                                            <?php echo $title ?>
                                        </h2>
                                    </div>
                                    <div class="entry">
                                        <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                            Input basic information about yourself like your name, gender, year of birth, etc.
                                        </h3>

                                        <div class="padding-b-20">
                                            <a 
                                                class="btn btn-ash txt-xxs no-m-b" 
                                                href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                            >
                                                Add <?php echo $title ?>
                                            </a>
                                        </div>
                                    </div>

                            <?php } else { ?>
                                <?php foreach( $entries as $entry ){ ?>

                                    <div class="header">
                                        <h2 class="section-wrapper-title">
                                            <?php echo $title ?>
                                        </h2>
                                        <div class="text-right">
                                            <a 
                                                href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&form-title='.$title ?>" 
                                                class="edit-btn"
                                            >
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                    <div class="entry">
                                        <div class="row row-10">
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Gender          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 2 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Yeah of Birth          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 3 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Marital Status          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 21 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Nationality          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 4 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    State of Origin          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 5 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Location in Nigeria where you currently reside          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 6 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    States in Nigeria where you can work
                                                </p>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field_id = 7; // Update this number to your field id number
                                                        $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                        $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                        echo $value;
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Preferred Contact Telephone Number (For Work)          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 8 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Preferred Contact Whatsapp Number (For Work)          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 9 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Preferred Contact Email Address (For Work)         
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 10 ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 padding-lr-10 padding-b-20">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Highest Level of Education          
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo rgar( $entry, 11 ); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>

                            <?php } ?>
                        </div>

                        <!-- Education & Training -->
                        <div class="section-wrapper">
                        <?php
                            $gf_id = 31; //Form ID
                            $gv_id = 1128; //Gravity View ID
                            $title = 'Education & Training';

                            $entry_count = 0;

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    )
                                )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );   

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                        ?>
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>

                            <?php if(!$entry_count){ //If no entry ?>

                                <div class="entry">
                                    <h3 class="txt-color-dark txt-sm margin-t-10 margin-b-20">
                                        What degree do you possess? What school did you attend? What did you study? input all of that information here.
                                    </h3>

                                    <div class="padding-b-20">
                                        <a 
                                            class="btn btn-ash txt-xxs no-m-b" 
                                            href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                        >
                                            Add <?php echo $title ?>
                                        </a>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <?php foreach( $entries as $entry ){ ?>

                                <div class="entry">
                                    <div class="row row-10">
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Degree Attained (e.g BA, BS, JD, PhD)         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 1 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                School      
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 8 ); ?>
                                            </p>
                                        </div>

                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)      
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 9 ); ?>
                                            </p>
                                        </div>

                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Start Month/Year      
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 10 ); ?>
                                            </p>
                                        </div>

                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                End Month/Year (Current students: Enter your expected graduation year)     
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 11 ); ?>
                                            </p>
                                        </div>

                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Achievements      
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 12 ); ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&form-title=Education / Training' ?>" 
                                           class="edit-btn"
                                        >
                                            Edit
                                        </a>
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=cv';  ?>" 
                                           class="delete-btn margin-l-5 confirm-delete"
                                        >
                                            Delete
                                        </a>
                                    </div>
                                </div>

                                <?php } ?>

                                <div class="padding-o-20">
                                    <a 
                                        class="btn btn-ash txt-xxs no-m-b" 
                                        href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                    >
                                        Add <?php echo $title ?>
                                    </a>
                                </div>

                            <?php } ?>
                        </div>

                        <!-- Additional Information -->
                        <!--<div class="section-wrapper">
                        <?php
                            $gf_id = 42; //Form ID
                            $gv_id = 1118; //Gravity View ID
                            $title = 'Additional Information';

                            $entry_count = 0;

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    )
                                )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                        ?>

                            <?php if(!$entry_count){ //If no entry ?>
                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        <?php echo $title ?>
                                    </h2>
                                </div>

                                <div class="entry">
                                    <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                        Empty! Start by adding your <?php echo $title ?>.
                                    </h3>

                                    <div class="padding-b-20">
                                        <a 
                                            class="btn btn-ash txt-xxs no-m-b" 
                                            href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                        >
                                            Add <?php echo $title ?>
                                        </a>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <?php foreach( $entries as $entry ){ ?>

                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        <?php echo $title ?>
                                    </h2>
                                    <div class="text-right">
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&cv-form-title='.$title ?>" 
                                            class="edit-btn"
                                        >
                                            Edit
                                        </a>
                                    </div>
                                </div>

                                <div class="entry">
                                    <div class="row row-10">
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 4 ); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <?php } ?>

                            <?php } ?>
                        </div>-->

                        <!-- Interests -->
                        <div class="section-wrapper">
                        <?php
                            $gf_id = 39; //Form ID
                            $gv_id = 1132; //Gravity View ID
                            $title = 'Interests';

                            $entry_count = 0;

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    )
                                )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                        ?>
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>

                            <?php if(!$entry_count){ //If no entry ?>

                                <div class="entry">
                                    <h3 class="txt-color-dark txt-sm margin-t-10 margin-b-20">
                                        What interests you? What do you like doing? List and describe what your interests are.
                                    </h3>

                                    <div class="padding-b-20">
                                        <a 
                                            class="btn btn-ash txt-xxs no-m-b" 
                                            href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                        >
                                            Add <?php echo $title ?>
                                        </a>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <?php foreach( $entries as $entry ){ ?>

                                <div class="entry">
                                    <div class="row row-10">
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Title        
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 1 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Description    
                                            </p>
                                            <article class="text-area txt-sm">
                                                <?php echo rgar( $entry, 4 ); ?>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&form-title=Interest' ?>" 
                                           class="edit-btn"
                                        >
                                            Edit
                                        </a>
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=cv';  ?>" 
                                           class="delete-btn margin-l-5 confirm-delete"
                                        >
                                            Delete
                                        </a>
                                    </div>
                                </div>

                                <?php } ?>

                                <div class="padding-o-20">
                                    <a 
                                        class="btn btn-ash txt-xxs no-m-b" 
                                        href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                    >
                                        Add <?php echo $title ?>
                                    </a>
                                </div>

                            <?php } ?>
                        </div>

                        <!-- Languages -->
                        <div class="section-wrapper">
                        <?php
                            $gf_id = 40; //Form ID
                            $gv_id = 1134; //Gravity View ID
                            $title = 'Languages';

                            $entry_count = 0;

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    )
                                )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                        ?>
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>

                            <?php if(!$entry_count){ //If no entry ?>

                                <div class="entry">
                                    <h3 class="txt-color-dark txt-sm margin-t-10 margin-b-20">
                                        Inform us on the language(s) you speak along with your proficiency in the language(s).
                                    </h3>

                                    <div class="padding-b-20">
                                        <a 
                                            class="btn btn-ash txt-xxs no-m-b" 
                                            href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                        >
                                            Add <?php echo $title ?>
                                        </a>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <?php foreach( $entries as $entry ){ ?>

                                <div class="entry">
                                    <div class="row row-10">
                                        <div class="col-md-4 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Language        
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 1 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-4 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Spoken Level    
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 2 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-4 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Written Level    
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 3 ); ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&form-title=Language' ?>" 
                                           class="edit-btn"
                                        >
                                            Edit
                                        </a>
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=cv';  ?>" 
                                           class="delete-btn margin-l-5 confirm-delete"
                                        >
                                            Delete
                                        </a>
                                    </div>
                                </div>

                                <?php } ?>

                                <div class="padding-o-20">
                                    <a 
                                        class="btn btn-ash txt-xxs no-m-b" 
                                        href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                    >
                                        Add <?php echo $title ?>
                                    </a>
                                </div>

                            <?php } ?>
                        </div>

                        <!-- References -->
                        <!--<div class="section-wrapper">
                        <?php
                            $gf_id = 41; //Form ID
                            $gv_id = 1136; //Gravity View ID
                            $title = 'References';

                            $entry_count = 0;

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    )
                                )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                        ?>
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                                <div class="text-right">
                                    <a 
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="637" action="edit" return="url" /]');  ?>" 
                                       class="edit-btn"
                                    >
                                        Edit
                                    </a>
                                </div>
                            </div>

                            <?php if(!$entry_count){ //If no entry ?>

                                <div class="entry">
                                    <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                        Empty! Start by adding your <?php echo $title ?>.
                                    </h3>

                                    <div class="padding-b-20">
                                        <a 
                                            class="btn btn-ash txt-xxs no-m-b" 
                                            href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                        >
                                            Add <?php echo $title ?>
                                        </a>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <?php foreach( $entries as $entry ){ ?>

                                <div class="entry">
                                    <div class="row row-10">
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Name of Referee       
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 1 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Position     
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 7 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Email     
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 8 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Telephone     
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 9 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Reference Available on Request?
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 10 ); ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&cv-form-title=Referee' ?>" 
                                           class="edit-btn"
                                        >
                                            Edit
                                        </a>
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=cv';  ?>" 
                                           class="delete-btn margin-l-5 confirm-delete"
                                        >
                                            Delete
                                        </a>
                                    </div>
                                </div>

                                <?php } ?>

                                <div class="padding-o-20">
                                    <a 
                                        class="btn btn-ash txt-xxs no-m-b" 
                                        href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                    >
                                        Add <?php echo $title ?>
                                    </a>
                                </div>

                            <?php } ?>
                        </div>-->

                    <?php } ?>
                </div>
                <div class="col-md-4 padding-lr-15">
                    <div class="dashboard-multi-main-sidebar w-100 p-0">
                        <div class="side-bar-card">
                            <h4 class="txt-medium txt-color-dark margin-b-20">
                                Download CV
                            </h4>
                            <p class="txt-sm">
                                Download a PDF of DOC version of your completed CV.
                            </p>
                            <div class="margin-t-20">
                                <!--<a href="" class="btn btn-ash txt-xxs no-m-b">
                                    <i class="fa fa-download"></i>
                                    <span class="padding-l-5">
                                        - Coming soon -
                                    </span>
                                </a>-->
                                <span class="btn btn-ash txt-xxs no-m-b">
                                    - Coming soon -
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>


