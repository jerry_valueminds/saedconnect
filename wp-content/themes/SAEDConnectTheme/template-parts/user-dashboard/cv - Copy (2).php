<style>
    .work-profile{
        display: none !important;
    }
    
    .gform_wrapper .top_label .gfield_label {
        font-size: 0.8rem !important;
        font-weight: 500 !important;
    }
    
    .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
        font-size: 0.9rem !important;
        width: 100% !important;
    }
    
    .gform_wrapper .gform_button {
        background-color: #b55085 !important;
        font-size: 0.7rem !important;
        width: auto !important;
    }
</style>
<?php
    $base_cv_url = 'https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv';

    /* CV Array Links */
    $cv_links = array(
        array(
            'title' => 'Personal Information',
            'template' => 'personal-information',
            'gravityFormID' => 84,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Name',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Gender',
                    'field_id' => 2,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Yeah of Birth',
                    'field_id' => 3,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Marital Status',
                    'field_id' => 21,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Nationality',
                    'field_id' => 4,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'State of Origin',
                    'field_id' => 5,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Location in Nigeria where you currently reside',
                    'field_id' => 6,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'States in Nigeria where you can work',
                    'field_id' => 7,
                    'type' => 'checkbox'
                ),
                
                array(
                    'label' => 'Preferred Contact Telephone Number (For Work)',
                    'field_id' => 8,
                    'type' => 'checkbox'
                ),
                
                array(
                    'label' => 'Preferred Contact Whatsapp Number (For Work)',
                    'field_id' => 9,
                    'type' => 'checkbox'
                ),
                
                array(
                    'label' => 'Preferred Contact Email Address (For Work)',
                    'field_id' => 10,
                    'type' => 'checkbox'
                ),
                
                array(
                    'label' => 'Highest Level of Education',
                    'field_id' => 11,
                    'type' => 'checkbox'
                ),
            )
        ),
        
        array(
            'title' => 'Summary',
            'template' => 'summary',
            'gravityFormID' => 27,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Headline',
                    'field_id' => 4,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Summary about yourself',
                    'field_id' => 5,
                    'type' => 'textarea'
                ),
            )
        ),

        array(
            'title' => 'Accomplishments',
            'template' => 'accomplishments',
            'gravityFormID' => 33,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 3,
                    'type' => 'textarea'
                ),
            )
        ),

        array(
            'title' => 'Additional Information',
            'template' => 'additional-information',
            'gravityFormID' => 42,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                /*array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),*/
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Certifications / Affiliations',
            'template' => 'certifications-affiliations',
            'gravityFormID' => 35,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Time Period',
                    'field_id' => 9,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Certification Expiration',
                    'field_id' => 10,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Valid from',
                    'field_id' => 11,
                    'type' => 'date'
                ),
                
                array(
                    'label' => 'To',
                    'field_id' => 12,
                    'type' => 'date'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Social Details',
            'template' => 'contact-details',
            'gravityFormID' => 28,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Email',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Phone',
                    'field_id' => 12,
                    'type' => 'text'
                ),
                                
                array(
                    'label' => 'Address',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Current State of Residence',
                    'field_id' => 14,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Personal Blog/Website',
                    'field_id' => 15,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Facebook',
                    'field_id' => 16,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Twitter',
                    'field_id' => 17,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Instagram',
                    'field_id' => 18,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Linkedin',
                    'field_id' => 19,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Education & Training',
            'template' => 'education-training',
            'gravityFormID' => 31,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Degree Attained (e.g BA, BS, JD, PhD)',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'School',
                    'field_id' => 8,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Start Month/Year',
                    'field_id' => 10,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'End Month/Year (Current students: Enter your expected graduation year)',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Achievements',
                    'field_id' => 12,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Experiences',
            'template' => 'experiences',
            'gravityFormID' => 29,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Position/Job Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Company/Company Website',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Location',
                    'field_id' => 10,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Start Month/Start Year',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'End Month/Year',
                    'field_id' => 12,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Describe what you did/Your Achievements',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Interests',
            'template' => 'Interests',
            'gravityFormID' => 39,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Languages',
            'template' => 'languages',
            'gravityFormID' => 40,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Language Name',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'References',
            'template' => 'references',
            'gravityFormID' => 41,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Name of Referee',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Position',
                    'field_id' => 7,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Email Address',
                    'field_id' => 8,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Telephone Number',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Reference Available on Request?',
                    'field_id' => 10,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Skills',
            'template' => 'skills',
            'gravityFormID' => 34,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Skill Name',
                    'field_id' => 4,
                    'type' => 'text'
                ),
            )
        ),
    );
    
    /* Get currently active CV from Query String  */
    $current_cv_view = $_REQUEST['cv-view'];
    $cv_form = $_REQUEST['cv-form'];
?>

<main class="main-content txt-color-light bg-white" style="margin-top: 70px">
    <header class="container-wrapper dashboard-multi-header bg-purple-career padding-tb-20">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h1 class="txt-xlg txt-medium txt-color-white">
                    My CV
                </h1>
            </div>
        </div>
    </header>

    <section class="container-wrapper" style="padding-top: 62px">
        <div class="row">
            <div class="col-md-2 dashboard-multi-main-menu">
                <div class="cv-menu-toggle">
                    CV Menu
                </div>
                <ul class="cv-menu-list">
                    <li>
                        <a
                            class="active"
                            href="<?php echo $base_cv_url ?>"
                        >
                            CV Overview
                        </a>
                    </li>
                </ul>
            </div>
            
            <div class="col-md-7 dashboard-multi-main-content">
            <?php
                /* If Add form */
                if ( $cv_form ){
            ?>
                
                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['cv-part']; ?>          
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$cv_form."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                </div>
                
            <?php
                /* Else view template */    
                } else {
                
                    foreach($cv_links as $cv_part){    
            ?>
                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php
                                /* Get View Title */
                                echo $cv_part['title'];
                            ?>                
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="637" action="edit" return="url" /]');  ?>" 
                               class="edit-btn"
                            >
                                Edit
                            </a>
                        </div>
                    </div>
                    
                    <?php

                        $gf_id = $cv_part['gravityFormID']; //Form ID
                        $gv_id = $cv_part['gravityviewID']; //Gravity View ID

                        $entry_count = 0;

                        /* Get current User ID */
                        $current_user = wp_get_current_user();

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                            )
                        );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                        /* If no entry */
                        if(!$entry_count){
                    ?>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Empty! Start by adding your <?php echo $cv_part['title'] ?>.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-career-purple txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $cv_part['title']); ?>"
                                >
                                    Add <?php echo $cv_part['title'] ?>
                                </a>
                            </div>
                        </div>
                    <?php

                        } else {

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            foreach( $entries as $entry ){
                    ?>
                        
                        <div class="entry">
                            <?php 

                                foreach( $cv_part['fields'] as $field ) { 

                                    if( $cv_part['template'] != 'skills' ){

                            ?>
                            
                            <div class="margin-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    <?php echo $field['label']; ?>                        
                                </p>
                                <p class="txt-sm">
                                    <?php echo rgar( $entry, $field['field_id'] ); ?>                       
                                </p>
                            </div>
                            
                            <?php 
                                    } else {            
                            ?>         
                                        
                            <div class="">
                                <div class="row ">
                                    <p class="col-8 txt-xs txt-color-dark txt-medium padding-b-5">
                                        <?php echo rgar( $entry, $field['field_id'] ); ?>
                                    </p>
                                    <p class="col-4 txt-sm text-right">
                                       <a href="">
                                           <i class="fa fa-plus padding-r-5"></i>
                                           Add Description
                                       </a>                     
                                    </p>
                                </div>
                            </div>         
                                        
                            <?php
                                    }
                                } 
                            ?>

                        </div>

                    <?php
                            }

                            /* if Multiple Entries allowed */
                            if( $cv_part['numberOfEntries'] == 'multiple' ){
                    ?>

                        <div class="padding-o-20">
                            <a 
                                class="btn btn-career-purple txt-xxs no-m-b" 
                                href="<?php printf("https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $cv_part['title']); ?>"
                            >
                                Add more <?php echo $cv_part['title'] ?>
                            </a>
                        </div>
                        
                    <?php
                            }

                        }
                    ?>
                </div>
            <?php 
                    } 
                }
            ?>
            </div>
            
            <div class="col-md-3 dashboard-multi-main-sidebar">
                <div class="side-bar-card">
                    <h4 class="txt-medium txt-normal-s txt-color-dark margin-b-20">
                        Print CV
                    </h4>
                    <p class="txt-xs">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ut, commodi, ea nisi voluptatibus error.
                    </p>
                    <div class="margin-t-20">
                        <a href="" class="btn btn-career-purple txt-xxs no-m-b">
                            <i class="fa fa-print"></i>
                            <span class="padding-l-5">
                                Print
                            </span>
                        </a>
                    </div>
                </div>
                <div class="side-bar-card">
                    <h4 class="txt-medium txt-color-dark margin-b-20">
                        Download CV
                    </h4>
                    <p class="txt-sm">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ut, commodi, ea nisi voluptatibus error.
                    </p>
                    <div class="margin-t-20">
                        <a href="" class="btn btn-career-purple txt-xxs no-m-b">
                            <i class="fa fa-download"></i>
                            <span class="padding-l-5">
                                Download
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>


