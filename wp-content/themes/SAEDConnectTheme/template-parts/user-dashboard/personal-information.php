<?php
    // Get Base URL
    $base_url = get_site_url().'/my-dashboard';

    // Get Currently logged in User
    $current_user = wp_get_current_user();
?>  
   
<header class="txt-light">
    <div class="row">
        <div class="col-md-8">
            <h1 class="txt-2em txt-height-1-2 margin-b-20">
                Welcome,
                <?php

                    echo $current_user->display_name;
                ?>
            </h1>
            <h2 class="txt-lg txt-height-1-7 margin-b-20">
                Technology roles are becoming one of the highest-paying jobs globally, and you don’t require a college degree to be a tech star. The Tech Career Mentorship Scheme is designed to help people across all backgrounds to start up a brilliant career in tech related areas.
            </h2>
        </div>
    </div>
</header>

<div id="main-1" class="collapse show" aria-labelledby="heading-1" data-parent="#accordion">                                   
<?php

    //echo do_shortcode( "[stickylist id='45' ]" );


    $form_id = 92;  // Get eSAED Registration Form ID

    // Prepare Search Parameters
    $search_criteria = array(
        'status'        => 'active',
        'field_filters' => array(
            'created_by' => $current_user->ID
        )
    );

    // Search for entries for this User
    $entries = GFAPI::get_entries( $form_id );

?>

<?php if($entries){ ?>

    <?php foreach($entries as $entry){ // Start foreach ?>

        <?php    if( $entry['created_by'] == $current_user->ID ){ ?>
            <?php
                //var_dump($entry);
                $post_id = $entry['post_id'];
                $entry_id = $entry['id'];
            ?>

            <div class="row row-20 margin-t-40">
                <div class="col-md-3 padding-lr-20 padding-b-20">
                    <p class="txt-bold txt-normal-s">
                        <?php
                            //  Get First Name
                            $meta = get_post_meta($post_id, 'user_personal_info_picture', true);

                            if($meta){
                        ?>
                        
                            <img src="<?php echo $meta ?>" alt="">
                        
                        <?php
                            } else {
                                echo '-';
                            }
                        ?>
                    </p>
                </div>
                <div class="col-md-8 padding-lr-20">
                    <div class="row row-20">
                        <div class="col-md-6 padding-lr-20 padding-b-20">
                            <h3 class="txt-normal-s margin-b-5">Name</h3>
                            <p class="txt-bold txt-normal-s">
                                <?php
                                    $full_name = '-';

                                    //  Get First Name
                                    $meta = get_post_meta($post_id, 'user_personal_info_f_name', true);

                                    if($meta){
                                        $full_name = $meta;
                                    }

                                    //  Get Last Name
                                    $meta = get_post_meta($post_id, 'user_personal_info_l_name', true);

                                    if($meta){
                                        $full_name .=' ';
                                        $full_name .= $meta;
                                    }

                                    echo $full_name;
                                ?>
                            </p>
                        </div>

                        <div class="col-md-6 padding-lr-20 padding-b-20">
                            <h3 class="txt-normal-s margin-b-5">Current Location</h3>
                            <p class="txt-bold txt-normal-s">
                                <?php
                                    //  Get First Name
                                    $meta = get_post_meta($post_id, 'user_personal_info_location', true);

                                    if($meta){
                                        echo $meta;
                                    } else {
                                        echo '-';
                                    }
                                ?>
                            </p>
                        </div>
                        
                        <div class="col-md-6 padding-lr-20 padding-b-20">
                            <h3 class="txt-normal-s margin-b-5">First Degree</h3>
                            <p class="txt-bold txt-normal-s">
                                <?php
                                    //  Get First Name
                                    $meta = get_post_meta($post_id, 'user_personal_info_first_degree', true);

                                    if($meta){
                                        echo $meta;
                                    } else {
                                        echo '-';
                                    }
                                ?>
                            </p>
                        </div>

                        <div class="col-md-6 padding-lr-20 padding-b-20">
                            <h3 class="txt-normal-s margin-b-5">Degree Class</h3>
                            <p class="txt-bold txt-normal-s">
                                <?php
                                    //  Get First Name
                                    $meta = get_post_meta($post_id, 'esaed_personal_degree_class', true);

                                    if($meta){
                                        echo $meta;
                                    } else {
                                        echo '-';
                                    }
                                ?>
                            </p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="margin-t-40">                                            
                <form action="<?php echo $base_url ?>/?action=personal-information-form" method="post">
                    <button class="sticky-list-edit submit btn btn btn-trans-bw txt-normal-s">Edit Profile</button>
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="edit_id" value="<?php echo $entry_id; ?>">
                </form>
            </div>

        <?php } //End If ?>


    <?php } //  End Foreach ?>

<?php } else { ?>

    <div class="margin-t-40">
        <a class="btn btn-green txt-normal-s" href="<?php echo $base_url ?>/?action=personal-information-form">
            Add Profile
        </a>
    </div>

<?php } ?>
</div>