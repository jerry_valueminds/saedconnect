<?php
    // Get Base URL
    $base_url = get_site_url().'/my-dashboard';

    // Get Currently logged in User
    $current_user = wp_get_current_user();
?> 

<div class="row row-10">
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Co-Create or Co-Execute a Youth Development Program with us
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Build compelling multimedia CVs employers can’t ignore and download as pdf in multiple designs.
            </h2>
            <div>
                <a class="btn btn-blue txt-normal-s full-width" href="<?php echo $base_url ?>/?action=co-create-a-program-form">
                    View
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Share a Story
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Create your solid business plan using the interactive Business Plan creator.
            </h2>
            <div>
                <a class="btn btn-blue txt-normal-s full-width" href="<?php echo $base_url ?>/?action=share-a-story-form">
                    View
                </a>
            </div>
        </div>
    </div>

    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Support a Project
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                The best growth opportunities from across the world, delivered to you in one place.
            </h2>
            <div>
                <a class="btn btn-blue txt-normal-s full-width" href="<?php echo $base_url ?>/?action=support-a-project-form">
                    View
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Mentor / Service Provider Profile
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Earn badges that demonstrate your competence in a skill, and get a job guarantee.
            </h2>
            <div>
                <a class="btn btn-blue txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Submit a Program
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Get matched to virtual tasks, remote jobs, short and full-time jobs that match your skill set &amp; competence.
            </h2>
            <div>
                <a class="btn btn-blue txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>

    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Submit a Course
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Make Money fro your blog or social media profile. Get paid for sharing advert campaigns and posts on your social media platforms.
            </h2>
            <div>
                <a class="btn btn-blue txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>
</div>