<main class="main-content txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper margin-b-20">
        <div class="container-wrapper bg-white padding-tb-80">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="txt-2-4em txt-bold margin-b-30">
                        Mentor Profile
                    </h1>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-2">
                    <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/eSAED/eS_test_your_ideas.png" alt="" width="160">
                </div>
            </div>
        </div>
    </section>
    
    <section class="container-wrapper margin-b-20">
        <div class="container-wrapper bg-white padding-tb-80">
        <?php
            switch_to_blog(20);
        
            $gf_id = 57;
        
            $entry_count = 0;

            /* Get current User ID */
            $current_user = wp_get_current_user();

            /* GF Search Criteria */
            $search_criteria = array(

            'field_filters' => array( //which fields to search

                array(

                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                )
              )
            );

            /* Get GF Entry Count */
            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
            
            if(!$entry_count){
                    /*$redirect_link = 'https://www.saedconnect.org/service-provider-directory/trainer-information-form/'; //Get Daasboard Page Link by ID

                    if ( wp_redirect( $redirect_link ) ) {
                        exit;
                    }*/
                
                echo '<script>window.location.replace("https://www.saedconnect.org/growth-programs/mentor-application-form/");</script>';
            }
            
            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
        
            foreach( $entries as $entry ){
        ?>
                <div class="row row-10 border-b-1 padding-b-20 margin-b-20">
                    <div class="col-md-12 padding-lr-10 margin-b-20">
                        <p class="txt-lg txt-bold">
                            <?php echo rgar( $entry, '1' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-12 padding-lr-10 margin-b-20">
                        <p class="txt-lg txt-bold">
                            <img src="<?php echo rgar( $entry, '44' ); ?>" alt="">
                        </p>
                    </div>

                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Telephone Number
                        </p>
                        <p>
                            <?php echo rgar( $entry, '24' ); ?>
                        </p>
                    </div>
                     
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Email Address
                        </p>
                        <p>
                            <?php echo rgar( $entry, '25' ); ?>
                        </p>
                    </div> 
                     
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Company
                        </p>
                        <p>
                            <?php echo rgar( $entry, '41' ); ?>
                        </p>
                    </div> 
                     
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Position
                        </p>
                        <p>
                            <?php echo rgar( $entry, '42' ); ?>
                        </p>
                    </div> 
                     
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Facebook
                        </p>
                        <p>
                            <?php echo rgar( $entry, '45' ); ?>
                        </p>
                    </div> 
                     
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Twitter
                        </p>
                        <p>
                            <?php echo rgar( $entry, '46' ); ?>
                        </p>
                    </div> 
                     
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            LinkedIn
                        </p>
                        <p>
                            <?php echo rgar( $entry, '47' ); ?>
                        </p>
                    </div> 
                     
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Short Profile
                        </p>
                        <p>
                            <?php echo rgar( $entry, '27' ); ?>
                        </p>
                    </div> 
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Where are you based?
                        </p>
                        <p>
                            <?php echo rgar( $entry, '28' ); ?>
                        </p>
                    </div> 
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Please tell us your location
                        </p>
                        <p>
                            <?php echo rgar( $entry, '29' ); ?>
                        </p>
                    </div> 
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Select state
                        </p>
                        <p>
                            <?php echo rgar( $entry, '30' ); ?>
                        </p>
                    </div> 

                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Mentorship Area
                        </p>
                        <p>
                            <?php
                
                                $field_id = 31; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div> 

                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Select Entrepreneurship Mentorship Area you are interested in
                        </p>
                        <p>
                            <?php
                
                                $field_id = 43; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div> 

                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Select Career Mentorship Area you are interested in
                        </p>
                        <p>
                            <?php
                
                                $field_id = 32; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div> 

                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Select Business Growth Mentorship Area you are interested in
                        </p>
                        <p>
                            <?php
                
                                $field_id = 33; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            What experience qualifies you as an Entrepreneurship/Career/Business Growth Mentor?
                        </p>
                        <p>
                            <?php echo rgar( $entry, '34' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            What small business do you want to be a Mentor for?
                        </p>
                        <p>
                            <?php
                
                                $field_id = 35; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Where is your business based?
                        </p>
                        <p>
                            <?php echo rgar( $entry, '36' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            What is your business Name?
                        </p>
                        <p>
                            <?php echo rgar( $entry, '37' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            What do you do in this business? Please be as detailed as possible
                        </p>
                        <p>
                            <?php echo rgar( $entry, '38' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            For how long have you been practicing this business, as at January 2019?
                        </p>
                        <p>
                            <?php echo rgar( $entry, '39' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            How big is your business?
                        </p>
                        <p>
                            <?php echo rgar( $entry, '40' ); ?>
                        </p>
                    </div>
                    
                    
                    
                    
                    
                    <div class="col-md-12 padding-lr-10">
                        <a 
                           href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="637" action="edit" return="url" /]');  ?>" 
                           class="btn btn-blue txt-xs no-m-b"
                        >
                            Edit
                        </a>
                    </div>
            </div>
        <?php
            }
        
            restore_current_blog();
        ?>
        </div>
    </section>
</main>