<main class="main-content txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper margin-b-20">
        <div class="row">
            <div class="col-md-6">
                <h1 class="txt-2em txt-bold margin-b-30">
                    Password Reset
                </h1>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
    
    <section class="container-wrapper margin-b-20">
        <div class="container-wrapper bg-white padding-tb-80">
        <?php 
            echo do_shortcode( "[reset_password]"); 
        ?>
        </div>
    </section>
</main>