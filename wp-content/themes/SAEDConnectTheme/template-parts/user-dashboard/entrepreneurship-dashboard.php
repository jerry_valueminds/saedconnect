<?php 

    $current_user = wp_get_current_user();

    /* Get Base URL */
    $base_url = get_site_url().'/cv-forms';
?>
   
<?php
    //Switch to Learn a Skill Multisite (id = 3)
    switch_to_blog(20);

?>
<main class="txt-color-light font-main " style="margin-top: 100px">
    <section class="container-wrapper margin-b-20">
        <div class="container-wrapper bg-white padding-tb-80">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="txt-2-4em txt-bold margin-b-30">
                        Business Dashboard
                    </h1>
                    <p class="txt-height-1-7">
                        Welcome to your Entrepreneurship Development Dashboard. From your dashboard, you will access all the innovative tools and programs designed to help you start your own business and make money for yourself.
                    </p>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-2">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_new_markets.png" alt="" width="160">
                </div>
            </div>
        </div>
    </section>
    
    <section class="margin-b-40">
        <div class="container-wrapper">
            <div class="row row-10">
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            Business/Ventures Directory
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Create your solid business plan using the interactive Business Plan creator.
                        </h2>
                        <div>
                            <!--<a class="btn btn-blue txt-xs full-width" href="<?php echo $dashboard_base.'?action=service-provider-profile' ?>https://www.saedconnect.org/growth-programs/venture-business-information/">
                                View
                            </a>-->
                            
                            <a class="btn btn-blue txt-xs full-width" href="<?php echo $dashboard_base.'?action=business-venture-list' ?>">
                                View
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            Explore a Side Hustle 
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Create your solid business plan using the interactive Business Plan creator.
                        </h2>
                        <div>
                            <a class="btn btn-blue txt-xs full-width" href="">
                                View
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            The Entrepreneurship Incubator
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Earn badges that demonstrate your competence in a skill, and get a job guarantee.
                        </h2>
                        <div>
                            <a class="btn btn-blue txt-xs full-width" href="https://www.saedconnect.org/growth-programs/program/the-entrepreneurship-incubator/">
                                View
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            Business Clinic
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Earn badges that demonstrate your competence in a skill, and get a job guarantee.
                        </h2>
                        <div>
                            <a class="btn btn-blue txt-xs full-width" href="https://www.saedconnect.org/growth-programs/program/business-clinic/">
                                View
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            Request Business Expert
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Create your solid business plan using the interactive Business Plan creator.
                        </h2>
                        <div>
                            <ul class="icon-list blue txt-sm margin-l-10">
                                <li>
                                    <a href="https://www.saedconnect.org/growth-programs/subscribe-for-business-support-service-form/">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Business Plan creation
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.saedconnect.org/growth-programs/subscribe-for-business-support-service-form/">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Help to Raise Finance
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.saedconnect.org/growth-programs/subscribe-for-business-support-service-form/">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Business Coaching
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                       
            </div>
        </div>
    </section>
</main>
<?php
    //Revert to Previous Multisite
    restore_current_blog();
?>


<!-- Enroll Online Modal -->
<style>
    .gform_wrapper .top_label .gfield_label {
        font-size: 0.8rem !important;
        margin-bottom: 0 !important;
    }

    .ginput_container input, .ginput_container select, .ginput_container textarea{
        width: 100% !important;
        font-size: 0.9rem !important;
        padding: 0.4rem 0.8rem !important;
    }

    .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
        margin-top: 0px !important;
    }

    .gform_button.button{
        margin: 0 auto !important;
        width: auto !important;
    }
    
    input[name="input_6"], input[name="input_3"], input[name="input_2"]{
        opacity: 0.5;
    }
</style>

<script type="text/javascript">
    /* Disable inputs */
    $(window).on('load',function(){
        //$( 'input[name="input_6"], input[name="input_3"], input[name="input_2"]' ).prop( "disabled", true ); //Disable
        
        /* Remove options already registered for */
        <?php foreach($exclude_list as $list_item){ ?>
            $("select[name='input_8'] option[value='<?php echo $list_item ?>']").remove();
        <?php } ?>
    });
</script>

<!-- Form Modal -->
<div class="modal fade font-main coming-soon-modal show" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row content">
                <div class="left gf-white full-width">
                    <div class="gform_heading">
                        <h3 class="gform_title">
                            Join a Community
                        </h3>
                    </div>
                    
                    <?php
                        /* Get User Phone number from GF User Registratiion Form */
                        
                        /* Variables */
                        $phone = 000;
                        //$gf_id = 37;
                        $gf_id = 21;
                        $user_id = $current_user->ID;
                    
                        $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'value' => $user_id );

                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                    
                        if($entries){
                            foreach($entries as $entry){
                                if( rgar( $entry, '4' ) ){
                                    $phone = rgar( $entry, '4' );
                                }
                            }
                        }
                    ?>

                    <?php echo do_shortcode('[gravityform id="21" title="false" description="false" field_values="full-name='.$current_user->display_name.'&email='.$current_user->user_email.'&phone='.$phone.'"]'); ?>
                    <?php //echo do_shortcode('[gravityform id="21" title="false" description="false"]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Confirmation Modal -->
<div class="modal fade font-main coming-soon-modal show" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row content">
                <div class="left gf-white full-width">
                    <div class="gform_heading">
                        <h3 class="txt-bold txt-height-1-5 text-center">
                            <?php echo $_GET['confirmation'] ?>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
    /* If There's an error, Open Form Modal */
    if ( $_POST ) {
?>
   
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#formModal').modal('show');
        });
    </script>
    
<?php } ?>


<?php
    /* On form submission, show confirmation */
    if ( $_GET['confirmation'] ) {
?>
   
    <script type="text/javascript">
        $(window).on('load',function(){
            
            /* Open Confirmation Modal */
            $('#confirmationModal').modal('show');
            
            /* Clean URL (Remove Confirmation Query String)  */
            var uri = window.location.toString();

            if (uri.indexOf("?") > 0) {

                var clean_uri = uri.substring(0, uri.indexOf("?"));

                window.history.replaceState({}, document.title, clean_uri);

            }
        });
    </script>
    
<?php 
        $_GET = array();
    } 
?>