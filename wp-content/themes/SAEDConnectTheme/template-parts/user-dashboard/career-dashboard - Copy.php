<?php 

    $current_user = wp_get_current_user();

    /* Get Base URL */
    $base_url = get_site_url().'/growth-programs/cv-forms';
?>
  
<?php
 
    $postTitleError = '';

    if ( isset( $_POST['submitted'] ) ) {

        if ( trim( $_POST['postTitle'] ) === '' ) {
            $postTitleError = 'Please select a role.';
            $hasError = true;
        }else{
            // eSAED CODE Generation
            $role_id = $_POST['postTitle'];
            
            // Get User ID
            $user_id = 1;
            
            // Save User Meta
            add_user_meta( get_current_user_id(), 'role_id', $role_id );
            
            
            // Redirect after regustration
            $url = 'http://www.google.com/';
            
            //
            if ( wp_redirect( $url ) ) {
                exit;
            }
        }

    }
 
?>
   

<main class="txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper margin-b-60">
        <div class="container-wrapper bg-white padding-tb-80">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="txt-2-4em txt-bold margin-b-30">
                        Career Dashboard
                    </h1>
                    <p class="txt-height-1-7">
                        Welcome to your Career Development Dashboard. From your dashboard, you will be able to access the Job Advisor Forum, create your resume, Add Job roles you are interested in getting hired for, and manage your experience profile across each Job role.
                    </p>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-2">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_test_your_ideas.png" alt="" width="160">
                </div>
            </div>
        </div>
    </section>
    
    <section class="container-wrapper dashboard-accordion white margin-b-20">
        <div class="">
            <div class=" border-b-1 border-color-darkgrey">
                <div class="container-wrapper bg-blue padding-tb-20">
                    <button class="btn-link collapsed txt-color-white" data-toggle="collapse" data-target="#main-2" aria-expanded="false" aria-controls="main-1">
                        <span class="row align-items-center txt-lg txt-bold">
                            <span class="uppercase txt-color-white">
                                JobAdvisor
                            </span>
                        </span>
                        <article class="margin-t-10 txt-color-white">
                           <p class="txt-normal-s">
                               <span class="">
                                   Access the entrepreneurship incubator mentor community of your choice from here.
                               </span>
                               <a href="https://www.saedconnect.org/growth-programs/program/career-forum/" class="txt-color-white txt-underline">
                                   Learn more about the Entrepreneurship Incubator
                               </a>
                           </p>
                        </article>
                    </button>
                </div>
                
                <div id="main-2" class="bg-blue collapse" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper">
                        <div class="border-t-1 border-color-darkgrey">
                            <div class="row row-15 padding-t-30 padding-b-10">
                            <?php // Display posts
                                $wp_query = new WP_Query( array(
                                    'relationship' => array(
                                        'id'   => 'program_to_track',
                                        'from' => 649, // You can pass object ID or full object
                                    ),
                                    'nopaging' => true,
                                ) );
                                while ( $wp_query->have_posts() ) : $wp_query->the_post();

                                        $program_id = $post->ID;    //Get Program 
                                
                                        //  Get Program Featured Image
                                        $images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ), $post->ID );

                                        $image = reset( $images );

                            ?>
                                <div class="col-md-3 padding-lr-15 margin-b-30 d-flex text-center">
                                    <div class="txt-color-white padding-lr-20 padding-tb-20 border-o-1 border-color-white b-s-dashed full-width">
                                        <h4 class="txt-normal-s txt-bold txt-height-1-2 uppercase margin-b-20">
                                            <?php echo the_title(); ?>
                                        </h4>
                                        <figure
                                            class="margin-b-20"
                                            style=  "
                                                        width:80px;
                                                        height:80px;
                                                        border-radius:50%;
                                                        margin-left:auto;
                                                        margin-right:auto;
                                                        background-image:url(<?php echo $image['full_url']; ?>);
                                                        backgound-position:center;
                                                        background-size:cover;
                                                    "
                                        >

                                        </figure>
                                        <article class="txt-box txt-sm txt-height-1-7 ">
                                            <?php echo rwmb_meta( 'program-name' ); ?>
                                        </article>
                                        <article class="margin-t-20">
                                            <a class="btn btn-entre-orange txt-xs no-m-b" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                                Go to community
                                            </a>
                                        </article>
                                    </div>
                                </div>
                            <?php
                                endwhile;
                                wp_reset_postdata();
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="">
        <div class="container-wrapper">
            <div class="bg-white padding-o-40">
                <header class="row margin-b-40">
                    <div class="col-md-10">
                        <h2 class="txt-xlg txt-medium margin-b-20">
                            My CV
                        </h2>
                        <p class="txt-color-dark txt-normal-s txt-medium txt-height-1-7">
                            Create & Manage your Resume here. You will need to complete your resume information before you can select a Job Role to prepare for.
                        </p>
                    </div>
                    <div class="col-md-2 text-md-right">
                        <a href="https://www.saedconnect.org/jobtrac/" class="btn btn-blue txt-normal-s">
                            Download CV
                        </a>
                    </div>
                </header>
                <div class="row row-10">

                <?php

                    /*
                        CV Section Function for single Entry Item

                        PARAMETERS:
                            - User ID (Number)
                            - Gravity Form ID (Number)
                            - Section Name (String)
                            - Gravity View ID (String)
                            - Form Fields (Array)
                    */

                    function cvSection ($user_id, $gf_id, $section_name, $gv_id, $fields_array, $form_template){ // Function Declaration
                ?>

                    <?php
                        $base_url = get_site_url().'/cv-form';
                        
                        $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'value' => $user_id );

                        $entries = GFAPI::get_entries( $gf_id, $search_criteria ); 
                    ?>

                    <!--Using Get entries-->
                    <?php                               
                        /* Loop Through Entries */
                        if($entries){
                            foreach($entries as $entry){
                                /* Get Entry ID */
                                $entry_id = $entry['id'];

                                /* Get Post attached to entry */
                                $post = get_post( $entry['post_id'] );

                                /* Generate Edit Entry Link */
                                $edit_entry_link = do_shortcode('[gv_entry_link action="edit" entry_id="'.$entry_id.'" view_id="'.$gv_id.'" return="url" /]');


                                /*
                                *   Form Completion Calculation
                                */

                                /* Get Total Number of Fields */
                                $field_count = count($fields_array);

                                /* Completed Fiels */
                                $completed_fields = 0;

                                /* Get Total completed Fields */
                                foreach($fields_array as $field){
                                    $meta = get_post_meta($post->ID, $field, true);

                                    if($meta){
                                        $completed_fields++;
                                    }
                                }

                                /* Calculate Percenage Based on Completed Fields */
                                $completion_percentage = ($completed_fields / $field_count) * 100;

                    ?>
                        <div class="col-md-3 padding-lr-10 padding-b-20">
                            <div class="bg-grey full-width">
                                <a
                                   class="row align-items-center border-b-1 border-color-darkgrey"
                                   data-toggle="collapse"
                                   href="#resume-content-<?php echo $gf_id ?>"
                                   role="button"
                                   aria-expanded="false"
                                   aria-controls="resume-content-1"
                                >
                                    <figure 
                                       class="
                                                col
                                                padding-o-10
                                                txt-color-white
                                                <?php 
                                                    if($completion_percentage < 50){
                                                        echo "bg-red";
                                                    } elseif ($completion_percentage < 75) {
                                                        echo "bg-ash";
                                                    }else{
                                                        echo "bg-green";
                                                    }
                                                ?>
                                            "
                                    >
                                        <div class="txt-lg txt-medium margin-b-5">
                                            <?php echo $completion_percentage; ?>%
                                        </div>
                                        <div class="txt-xxs">
                                            complete
                                        </div>
                                    </figure>
                                    <div class="col-md-9 padding-o-10">
                                        <h3 class="txt-normal-s txt-medium txt-color-dark d-flex justify-content-between">
                                            <span class="padding-r-10">
                                                <?php echo $section_name ?>
                                            </span>
                                            <i class="fa fa-pencil txt-color-green"></i>
                                        </h3>
                                    </div>
                                </a>

                                <div class="collapse" id="resume-content-<?php echo $gf_id ?>">
                                    <div class="padding-o-10">

                                        <?php foreach($fields_array as $key => $value){ ?>
                                            <div class="col-md-12 margin-b-20">
                                                <p class="txt-sm txt-bold">
                                                    <?php echo $key ?>
                                                </p>
                                                <p class="txt-normal-s txt-height-1-7 ">
                                                <?php
                                                    $meta = get_post_meta($post->ID, $value, true);
                                                    if($meta){
                                                        echo $meta;
                                                    } else {
                                                        echo "--";
                                                    }
                                                ?>
                                                </p>
                                            </div>
                                        <?php } ?>

                                        <article class="padding-b-10">
                                            <a href="<?php echo $edit_entry_link ?>" class="btn btn-blue txt-normal-s no-m-b">
                                                Edit
                                            </a>
                                        </article>


                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php
                            }
                        } else {
                    ?>    

                    <div class="col-md-3 padding-lr-10 padding-b-20 d-flex">
                        <div class="padding-o-15 border-o-1 border-color-darkgrey flex_1 d-flex flex-column justify-content-between">
                            <p class="txt-sm txt-medium">
                                You have not added your <?php echo $section_name ?>.
                            </p>
                            <p class="margin-t-10">
                                <a
                                href="<?php echo $base_url.'/?action='.$form_template.'&gf-id='.$gf_id; ?>"
                                class="btn btn-blue txt-xs no-m-b"
                                >
                                    Add <?php echo $section_name ?>
                                </a>
                            </p>
                        </div>
                    </div>

                    <?php } ?>

                <?php } // End Funtion ?>



                <?php

                    /*
                        CV Section Function for Multi Entry Items

                        PARAMETERS:
                            - User ID (Number)
                            - Gravity Form ID (Number)
                            - Section Name (String)
                            - Gravity View ID (String)
                            - Form Fields (Array)
                    */

                    function cvSection_multi_entries ($user_id, $gf_id, $section_name, $gv_id, $fields_array, $form_template){ // Function Declaration
                ?>

                    <?php
                        $base_url = get_site_url().'/cv-form';

                        $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'value' => $user_id );

                        $entries = GFAPI::get_entries( $gf_id, $search_criteria ); 

                        $entries_count = count($entries);
                        
                        if($entries){
                    ?>


                        <div class="col-md-3 padding-lr-10 padding-b-20">
                            <div class="bg-grey full-width">
                                <a
                                   class="row align-items-center border-b-1 border-color-darkgrey"
                                   data-toggle="collapse"
                                   href="#resume-content-<?php echo $gf_id ?>"
                                   role="button"
                                   aria-expanded="false"
                                   aria-controls="resume-content-1"
                                >
                                    <figure class="col padding-o-10 txt-color-white bg-green">
                                        <div class="txt-lg txt-medium margin-b-5">
                                            <?php echo $entries_count ?>
                                        </div>
                                        <div class="txt-xs">
                                            <?php
                                                if($entries_count < 2){
                                                    echo 'Item';
                                                } else {
                                                    echo 'Items';
                                                }

                                            ?>
                                        </div>
                                    </figure>
                                    <div class="col-md-9 padding-o-10">
                                        <h3 class="txt-normal-s txt-medium txt-color-dark d-flex justify-content-between">
                                            <span class="padding-r-10">
                                                <?php echo $section_name ?>
                                            </span>
                                            <i class="fa fa-pencil txt-color-green"></i>
                                        </h3>
                                    </div>
                                </a>

                                <div class="collapse" id="resume-content-<?php echo $gf_id ?>">
                                <!--Using Get entries-->

                                
                                <?php                               
                                /* Loop Through Entries */
                                    foreach($entries as $entry){
                                        /* Get Entry ID */
                                        $entry_id = $entry['id'];

                                        /* Get Post attached to entry */
                                        $post = get_post( $entry['post_id'] );

                                        /* Generate Edit Entry Link */
                                        $edit_entry_link = do_shortcode('[gv_entry_link action="edit" entry_id="'.$entry_id.'" view_id="'.$gv_id.'" return="url" /]');


                                        /*
                                        *   Form Completion Calculation
                                        */

                                        /* Get Total Number of Fields */
                                        $field_count = count($fields_array);

                                        /* Completed Fiels */
                                        $completed_fields = 0;

                                        /* Get Total completed Fields */
                                        foreach($fields_array as $field){
                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                                $completed_fields++;
                                            }
                                        }

                                        /* Calculate Percenage Based on Completed Fields */
                                        $completion_percentage = ($completed_fields / $field_count) * 100;

                                ?>

                                <div class="padding-o-10 border-b-1 border-color-darkgrey">
                                    <div class="row row-6">
                                    <?php foreach($fields_array as $key => $value){ ?>
                                        <div class="col-md-6 padding-lr-5 margin-b-20">
                                            <p class="txt-sm txt-bold">
                                                <?php echo $key ?>
                                            </p>
                                            <p class="txt-normal-s txt-height-1-7 ">
                                            <?php
                                                $meta = get_post_meta($post->ID, $value, true);
                                                if($meta){
                                                    echo $meta;
                                                } else {
                                                    echo "--";
                                                }
                                            ?>
                                            </p>
                                        </div>
                                    <?php } ?>
                                    </div>
                                    <article class="padding-b-10">
                                        <a href="<?php echo $edit_entry_link ?>" class="btn btn-blue txt-normal-s no-m-b">
                                            Edit
                                        </a>
                                    </article>
                                </div>

                                <?php 
                                        }
                                ?>
                    

                                </div>
                            </div>
                        </div>
                        
                    <?php } else { ?> 
                        
                        <div class="col-md-3 padding-lr-10 padding-b-20 d-flex">
                            <div class="padding-o-15 border-o-1 border-color-darkgrey flex_1 d-flex flex-column justify-content-between">
                                <p class="txt-sm txt-medium">
                                    You have not added your <?php echo $section_name ?>.
                                </p>
                                <p class="margin-t-10">
                                    <a
                                    href="<?php echo $base_url.'/?action='.$form_template.'&gf-id='.$gf_id; ?>"
                                    class="btn btn-blue txt-xs no-m-b"
                                    >
                                        Add <?php echo $section_name ?>
                                    </a>
                                </p>
                            </div>
                        </div>
                        
                    <?php } ?>

                <?php } // End Funtion ?> 








                <!-- Get Sections -->
                <?php
                    /* Personal Information */
                    $fields_args = array(
                        'Date of Birth' => 'cv-personal-info-dob',
                        'Marital Status' => 'cv-personal-info-marital',
                        'Gender' => 'cv-personal-info-gender',
                        'Natinality' => 'cv-personal-info-nationality'
                    );

                    cvSection($current_user->ID, 26, 'Personal Information', 1192, $fields_args, 'cv-personal-information-form');
                ?>

                <?php
                    /* Summary */
                    $fields_args = array(
                        'Summary headline' => 'cv-summary-headline',
                        'About' => 'cv-summary-about',
                    );

                    cvSection($current_user->ID, 27, 'Summary', 1192, $fields_args, 'cv-summary-form');
                ?>

                <?php
                    /* Education */
                    $fields_args = array(
                        'Degree Attained' => 'cv-education-degree',
                        'School' => 'cv-education-school',
                        'Field of Study' => 'cv-education-field',
                        'Start Month/Year' => 'cv-education-start-date',
                        'End Month/Year' => 'cv-education-end-date',
                        'Achievements' => 'cv-education-achievements',
                    );

                    cvSection_multi_entries($current_user->ID, 31, 'Education & Training', 1192, $fields_args, 'cv-education-form');
                ?>


                <?php
                    /* Employment History */
                    $fields_args = array(
                        'Position/Job Title' => 'cv-experience-position',
                        'Company/Company Website' => 'cv-experience-website',
                        'Location' => 'cv-experience-location',
                        'Start Month/Start Year' => 'cv-experience-start-date',
                        'End Month/Start Year' => 'cv-experience-end-date',
                        'Describe what you did/Your Achievements' => 'cv-experience-description',
                    );

                    cvSection_multi_entries($current_user->ID, 29, 'Employment History', 1192, $fields_args, 'cv-experience-form');
                ?>

                <?php
                    /* Personal Projects */
                    $fields_args = array(
                        'Project Name' => 'cv-personal-project-name',
                        'Description' => 'cv-personal-project-description',
                    );

                    cvSection_multi_entries($current_user->ID, 30, 'Personal Projects', 10, $fields_args, 'cv-experience-form');
                ?>

                <?php
                    /* Award */
                    $fields_args = array(
                        'Title' => 'cv-award-title',
                        'Website' => 'cv-award-website',
                        'Date' => 'cv-award-date',
                        'Description' => 'cv-award-description',
                    );

                    cvSection_multi_entries($current_user->ID, 32, 'Awards & Certifications', 10, $fields_args, 'cv-award-form');
                ?>

                <?php
                    /* Skill */
                    $fields_args = array(
                        'Skill Name' => 'cv-skill-name',
                        'Description' => 'cv-skill-description',
                    );

                    cvSection_multi_entries($current_user->ID, 34, 'Skills ', 10, $fields_args, 'cv-skill-form');
                ?>

                <?php
                    /* Affiliations */
                    $fields_args = array(
                        'Title' => 'cv-affiliation-title',
                        'Your Role in the Group' => 'cv-affiliation-role',
                        'Time Period' => 'cv-affiliation-time-period',
                        'Are you still a member of this group?' => 'cv-affiliation-mentorship',
                    );

                    cvSection_multi_entries($current_user->ID, 36, 'Affiliations', 10, $fields_args, 'cv-affiliations-form');
                ?>



                </div>
            </div>
        </div>
    </section>

    <section class="container-wrapper margin-b-40">
        <div class="bg-white padding-o-40 border-b-1 border-color-darkgrey">
            <header class="row margin-b-30">
                <div class="col-md-10">
                    <h2 class="txt-xlg txt-medium margin-b-20">
                        My Roles
                    </h2>
                    <p class="txt-color-dark txt-normal-s txt-medium txt-height-1-7">
                        Start and manage your Journey to getting hired across a variety of roles.
                    </p>
                </div>
            </header>
            <div class="row row-15">
                <div class="col-md-3 d-flex padding-lr-15 padding-b-20 text-center">
                    <a
                       class="d-flex padding-o-20 border-o-1 dashed-border align-items-center justify-content-center full-width"
                       data-toggle="modal"
                       data-target="#formModal"
                       role="button"
                    >  
                        <span class="">
                            <h2 class="txt-xxlg txt-color-blue margin-b-10">
                                <i class="fa fa-plus-circle"></i>
                            </h2>
                            <h1 class="txt-medium txt-color-blue">
                                Add Roles
                            </h1>
                        </span>
                    </a>
                </div>
                <?php 
                    $user_id = 1;
                    $key = 'role_id';
                    $single = false;
                
                    $results = get_user_meta( get_current_user_id(), $key, $single );
                
                    $roles_array = array_unique($results);
                                
                    foreach($roles_array as $role){
                        
                        $post = get_post( $role );
                        
                ?>
                    <div class="col-md-3 padding-lr-15 d-flex padding-b-20">
                        <div class="flex full-width bg-white border-o-1 border-color-darkgrey">
                            <h3 class="txt-normal-s txt-bold uppercase padding-lr-20 padding-t-20">
                                <?php echo $post->post_title ?>
                            </h3>
                            <article class="padding-o-20 margin-b-20">
                                <p class="txt-normal-s txt-height-1-7 ">
                                    Role Description - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur pariatur delectus aut, similique.
                                </p>
                            </article>
                            <article class="padding-lr-20 padding-b-20">
                                <a href="<?php echo get_permalink($role); ?>" class="btn btn-blue txt-normal-s no-m-b">
                                    View Role
                                </a>
                            </article>
                        </div>
                    </div>
                <?php } ?>

                <!--<div class="col-md-3 d-flex padding-lr-15 padding-b-30 text-center">
                    <div
                       class="competency-card padding-o-20 card-shadow-5  full-width"
                       data-toggle="modal"
                       data-target="#ratingModal"
                       role="button"
                    >  
                        <div class="txt-xlg text-right txt-color-lighter">
                            <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">
                                    Request External Assessment (Coming Soon)
                                </a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>

                        <figure class="image-box margin-b-10">
                            <img src="images/icons/levels/6-levels.png" alt="">
                            <figcation>
                                <span class="level">
                                    Level
                                </span>
                                <span class="number">
                                    6
                                </span>
                            </figcation>
                        </figure>
                        <h3 class=" txt-regular txt-color-dark margin-b-10">
                            Create a Presentation using a software
                        </h3>
                        <p class="txt-normal-s txt-regular uppercase">
                            <a
                               data-toggle="modal"
                               data-target="#ratingModal"
                               role="button"
                            > 
                            Self Rating
                            </a>
                        </p>
                    </div>
                </div>-->
            </div>
        </div>
    </section>
    
    <!-- Enroll Online Modal -->
    <div class="modal fade font-main coming-soon-modal show" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row content">
                    <div class="left gf-white full-width">
                        <form action="" id="primaryPostForm" method="POST">
                            <div class="gform_heading">
                                <h3 class="gform_title">
                                    Add Role
                                </h3>
                            </div>
                            <div class="gform_body">
                                <ul id="gform_fields_13" class="gform_fields top_label form_sublabel_below description_below">
                                    <li id="field_13_1" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
                                        <label class="gfield_label" for="input_13_1">
                                            
                                        </label>
                                        <div class="ginput_container ginput_container_post_title">
                                            
                                            <select name="postTitle" id="postTitle" class="required">
                                                <option value="0" selected hidden disabled>
                                                    - Select Role - 
                                                </option>
                                            <?php // Display posts
                                                $temp = $wp_query; $wp_query= null;
                                                $wp_query = new WP_Query();
                                                $wp_query->query(array('post_type' => 'career-role'));
                                                    while ($wp_query->have_posts()) : $wp_query->the_post();
                                            ?>
                                                       
                                                    <option value="<?php the_id() ?>">
                                                        <?php the_title() ?>
                                                    </option>
                                                    
                                            <?php
                                                endwhile;

                                                //Reset Query
                                                wp_reset_postdata();
                                                wp_reset_query();
                                                $wp_query = $temp;
                                            ?>
                                            </select>
                                        </div>
                                        <?php if ( $postTitleError != '' ) { ?>
                                            <p class="txt-normal-s txt-color-white margin-t-5"><?php echo $postTitleError; ?></p>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="gform_footer top_label">
                                <input type="hidden" name="submitted" id="submitted" value="true" />
                                <button class="gform_button" type="submit"><?php _e('Submit', 'framework') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Confirmation Modal -->
    <div class="modal fade font-main coming-soon-modal show" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row content">
                    <div class="left gf-white full-width">
                        <div class="gform_heading">
                            <h3 class="txt-bold txt-height-1-5 text-center">
                                <?php echo $_GET['confirmation'] ?>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php if ( $postTitleError != '' ) { ?>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#formModal').modal('show');
            });
        </script>
    <?php } ?>
    
    <?php
        /* On form submission, show confirmation */
        if ( $_GET['confirmation'] ) {
    ?>

        <script type="text/javascript">
            $(window).on('load',function(){

                /* Open Confirmation Modal */
                $('#confirmationModal').modal('show');

                /* Clean URL (Remove Confirmation Query String)  */
                var uri = window.location.toString();

                if (uri.indexOf("?") > 0) {

                    var clean_uri = uri.substring(0, uri.indexOf("?"));

                    window.history.replaceState({}, document.title, clean_uri);

                }
            });
        </script>

    <?php 
            $_GET = array();
        } 
    ?>
    
</main>