<div class="bg-yellow padding-tb-20">
    <div class="container-wrapper txt-bold txt-xlg">
        <?php
            /* Get User Display Name */
            switch_to_blog(1);

            $gf_id = 4; //Form ID

            $entry_count = 0;

            /* GF Search Criteria */
            $search_criteria = array(

            'field_filters' => array( //which fields to search

                array(

                    'key' => 'created_by', 'value' => $_GET['user-id'], //Current logged in user
                    )
                )
            );

            /* Get Entries */
            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

            /* Get GF Entry Count */
            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

            if($entry_count){ //If no entry
                foreach( $entries as $entry ){          
                    $displayname = rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' );
                }   
            } 
        
            echo $displayname;
        
            restore_current_blog();
        ?>
    </div>
</div>