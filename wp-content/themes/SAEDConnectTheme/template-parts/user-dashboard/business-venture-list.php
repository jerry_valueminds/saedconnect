<main class="main-content txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper margin-b-20">
        <div class="row">
            <div class="col-md-6">
                <h1 class="txt-2em txt-bold margin-b-30">
                    My Business Ventures
                </h1>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
    
    <section class="container-wrapper margin-b-20">
        <div class="container-wrapper bg-white padding-tb-80">
        <?php
        
            $gf_id = 59;
        
            $entry_count = 0;

            /* Get current User ID */
            $current_user = wp_get_current_user();

            /* GF Search Criteria */
            $search_criteria = array(

            'field_filters' => array( //which fields to search

                array(

                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                )
              )
            );

            /* Get GF Entry Count */
            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

            
            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
        
            foreach( $entries as $entry ){
                
                $post_id = $entry['post_id'];
        ?>
                <div class="row row-10 border-b-1 padding-b-10 margin-b-10">
                    <div class="col-md-6">
                        <p class="txt-bold">
                            <?php echo rgar( $entry, '1' ); ?>
                        </p>
                    </div>

                    <div class="col-md-6">
                        <a 
                           href="<?php echo the_permalink( $post_id );  ?>" 
                           class="btn btn-blue txt-xs no-m-b"
                        >
                            View
                        </a>
                    </div>
                </div>
        <?php
            }
        
        ?>
        </div>
    </section>
</main>