                <?php
                    // Get Base URL
                    $base_url = get_site_url().'/my-dashboard';

                    // Get Currently logged in User
                    $current_user = wp_get_current_user();
                ?>                
               
                <div class="row">
                    <div class="col-md-11 mx-auto">
                        <header class="padding-b-60 txt-light">
                            <div class="row">
                                <div class="col-md-8">
                                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                                        eSAED Profile
                                    </h1>
                                    <h2 class="txt-lg txt-height-1-7">
                                        Technology roles are becoming one of the highest-paying jobs globally, and you don’t require a college degree to be a tech star. The Tech Career Mentorship Scheme is designed to help people across all backgrounds to start up a brilliant career in tech related areas.
                                    </h2>
                                </div>
                            </div>
                        </header>
                        
                        <section class="user-dashboard-accordion">
                            <div class="border-b-1 border-color-darkgrey margin-b-10">
                                <button class="btn-link collapsed" data-toggle="collapse" data-target="#main-1" aria-expanded="false" aria-controls="main-1">
                                    <span class="row align-items-center">
                                        <span class="padding-l-10">
                                            Personal Profile
                                        </span>
                                    </span>
                                </button>
                                <div id="main-1" class="collapse show" aria-labelledby="heading-1" data-parent="#accordion">                                   
                                    <?php
                                        
                                        //echo do_shortcode( "[stickylist id='45' ]" );
                                    
                                    
                                        $form_id = 45;  // Get eSAED Registration Form ID

                                        // Prepare Search Parameters
                                        $search_criteria = array(
                                            'status'        => 'active',
                                            'field_filters' => array(
                                                'created_by' => $current_user->ID
                                            )
                                        );

                                        // Search for entries for this User
                                        $entries = GFAPI::get_entries( $form_id );
                                        
                                    ?>

                                    <?php if($entries){ ?>

                                        <?php foreach($entries as $entry){ // Start foreach ?>
                                           
                                            <?php    if( $entry['created_by'] == $current_user->ID ){ ?>
                                                <?php
                                                    //var_dump($entry);
                                                    $post_id = $entry['post_id'];
                                                    $entry_id = $entry['id'];
                                                ?>
                                            
                                                <div class="row row-20 margin-t-40">
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Name</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                $full_name = '-';

                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_f_name', true);

                                                                if($meta){
                                                                    $full_name = $meta;
                                                                }

                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_m_name', true);

                                                                if($meta){
                                                                    $full_name .=' ';
                                                                    $full_name .= $meta;
                                                                }

                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_l_name', true);

                                                                if($meta){
                                                                    $full_name .=' ';
                                                                    $full_name .= $meta;
                                                                }

                                                                echo $full_name;
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Email Address</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_email', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Telephone Number</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_phone', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Gender</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_gender', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">State of Deployment</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_state_of_deployment', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">State Code</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_state_code', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Univerversity Attended</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_institution', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Course of Study</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_course_studied', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Class of Degree</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_degree_class', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Do you have a smartphone?</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_have_smartphone', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">WhatsApp Number</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_whatsapp', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Telegram Number</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_personal_telegram', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <div class="margin-t-40">                                            
                                                    <form action="http://127.0.0.1/saedconnect_wp/my-dashboard/?action=esaed-personal-profile-form" method="post">
                                                        <button class="sticky-list-edit submit btn btn btn-trans-bw txt-normal-s">Edit Profile</button>
                                                        <input type="hidden" name="mode" value="edit">
                                                        <input type="hidden" name="edit_id" value="<?php echo $entry_id; ?>">
                                                    </form>
                                                </div>
                                            
                                            <?php } //End If ?>
                                            
                                            
                                        <?php } //  End Foreach ?>
                                        
                                    <?php } else { ?>

                                        <div class="margin-t-40">
                                            <p>
                                                You do not have a Personal profile.
                                            </p>
                                        </div>
                                        <div class="margin-t-40">
                                            <a class="btn btn-trans-bw txt-normal-s" href="<?php echo $base_url ?>/?action=esaed-personal-profile-form">
                                                Add Profile
                                            </a>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>

                            <div class="border-b-1 border-color-darkgrey margin-b-20">
                                <button class="btn-link collapsed txt-bold bg-grey" data-toggle="collapse" data-target="#main-2" aria-expanded="false" aria-controls="main-2">
                                    <span class="row align-items-center">
                                        <span class="padding-l-10">
                                            Skills profile
                                        </span>
                                    </span>
                                </button>
                                <div id="main-2" class="collapse show" aria-labelledby="heading-1" data-parent="#accordion">                                   
                                    <?php                                    
                                    
                                        $form_id = 39;  // Get eSAED Registration Form ID

                                        // Prepare Search Parameters
                                        $search_criteria = array(
                                            'status'        => 'active',
                                            'field_filters' => array(
                                                'created_by' => $current_user->ID
                                            )
                                        );

                                        // Search for entries for this User
                                        $entries = GFAPI::get_entries( $form_id );
                                        
                                    ?>

                                    <?php if($entries){ ?>

                                        <?php foreach($entries as $entry){ // Start foreach ?>
                                           
                                            <?php    if( $entry['created_by'] == $current_user->ID ){ ?>
                                                <?php
                                                    //var_dump($entry);
                                                    $post_id = $entry['post_id'];
                                                    $entry_id = $entry['id'];
                                                ?>
                                            
                                                <div class="row row-20 margin-t-40">
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Business Skills</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_skills_business', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Entertainment Skills</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_skills_entertainment', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Graphic Design & Social Media
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_skills_graphics_and_social_media', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Tech Related Skills</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_skills_tech', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Agro Skills</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_skills_agro', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Livestock farming skills you have</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_skills_livestock', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Craft Skills</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_skills_craft', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Additional Skills</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_skills_additional', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <div class="margin-t-40">                                            
                                                    <form action="http://127.0.0.1/saedconnect_wp/my-dashboard/?action=esaed-skills-profile-form" method="post">
                                                        <button class="sticky-list-edit submit btn btn btn-trans-bw txt-normal-s">Edit Profile</button>
                                                        <input type="hidden" name="mode" value="edit">
                                                        <input type="hidden" name="edit_id" value="<?php echo $entry_id; ?>">
                                                    </form>
                                                </div>
                                            
                                            <?php } //End If ?>
                                            
                                            
                                        <?php } //  End Foreach ?>
                                        
                                    <?php } else { ?>

                                        <div class="margin-t-40">
                                            <p>
                                                You do not have a Skills profile.
                                            </p>
                                        </div>
                                        <div class="margin-t-40">
                                            <a class="btn btn-trans-bw txt-normal-s" href="<?php echo $base_url ?>/?action=esaed-skills-profile-form">
                                                Add Profile
                                            </a>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>

                            <div class="border-b-1 border-color-darkgrey margin-b-10">
                                <button class="btn-link collapsed show" data-toggle="collapse" data-target="#main-3" aria-expanded="false" aria-controls="main-3">
                                    <span class="row align-items-center">
                                        <span class="padding-l-10">
                                            Influencer Profile
                                        </span>
                                    </span>
                                </button>
                                <div id="main-3" class="collapse show" aria-labelledby="heading-1" data-parent="#accordion">                                   
                                    <?php                                    
                                    
                                        $form_id = 40;  // Get eSAED Registration Form ID

                                        // Prepare Search Parameters
                                        $search_criteria = array(
                                            'status'        => 'active',
                                            'field_filters' => array(
                                                'created_by' => $current_user->ID
                                            )
                                        );

                                        // Search for entries for this User
                                        $entries = GFAPI::get_entries( $form_id );
                                        
                                    ?>

                                    <?php if($entries){ ?>

                                        <?php foreach($entries as $entry){ // Start foreach ?>
                                           
                                            <?php    if( $entry['created_by'] == $current_user->ID ){ ?>
                                                <?php
                                                    //var_dump($entry);
                                                    $post_id = $entry['post_id'];
                                                    $entry_id = $entry['id'];
                                                ?>
                                            
                                                <div class="row row-20 margin-t-40">
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Instagram Handle</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_instagram', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Number of Instagram Followers</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_instagram_followers', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Twitter Handle</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_twitter', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Number of Twitter Followers</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_twitter_followers', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">YouTube Channel</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_youtube', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Number of YouTube Subsribers</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_youtube_subscribers', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Blog URL</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_blog_url', true);

                                                                if($meta){
                                                            ?>
                                                                <a href="<?php echo $meta; ?>">
                                                                    <?php echo $meta; ?>
                                                                </a>  
                                                            <?php } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Number of blog visitors per month</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_blog_visitors', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Craft Skills</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_influencer_social_media_niche', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <div class="margin-t-40">                                            
                                                    <form action="<?php echo $base_url ?>/?action=esaed-influencer-profile-form" method="post">
                                                        <button class="sticky-list-edit submit btn btn btn-trans-bw txt-normal-s">Edit Profile</button>
                                                        <input type="hidden" name="mode" value="edit">
                                                        <input type="hidden" name="edit_id" value="<?php echo $entry_id; ?>">
                                                    </form>
                                                </div>
                                            
                                            <?php } //End If ?>
                                            
                                            
                                        <?php } //  End Foreach ?>
                                        
                                    <?php } else { ?>

                                        <div class="margin-t-40">
                                            <p>
                                                You do not have a Skills profile.
                                            </p>
                                        </div>
                                        <div class="margin-t-40">
                                            <a class="btn btn-trans-bw txt-normal-s" href="<?php echo $base_url ?>/?action=esaed-influencer-profile-form">
                                                Add Profile
                                            </a>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>

                            <div class="border-b-1 border-color-darkgrey margin-b-10">
                                <button class="btn-link collapsed show txt-xlg" data-toggle="collapse" data-target="#main-4" aria-expanded="false" aria-controls="main-3">
                                    <span class="row align-items-center">
                                        <span class="padding-l-10">
                                            Entrepreneurship Profile
                                        </span>
                                    </span>
                                </button>
                                <div id="main-4" class="collapse show" aria-labelledby="heading-1" data-parent="#accordion">                                   
                                    <?php                                    
                                    
                                        $form_id = 41;  // Get eSAED Registration Form ID

                                        // Prepare Search Parameters
                                        $search_criteria = array(
                                            'status'        => 'active',
                                            'field_filters' => array(
                                                'created_by' => $current_user->ID
                                            )
                                        );

                                        // Search for entries for this User
                                        $entries = GFAPI::get_entries( $form_id );
                                        
                                    ?>

                                    <?php if($entries){ ?>

                                        <?php foreach($entries as $entry){ // Start foreach ?>
                                           
                                            <?php    if( $entry['created_by'] == $current_user->ID ){ ?>
                                                <?php
                                                    //var_dump($entry);
                                                    $post_id = $entry['post_id'];
                                                    $entry_id = $entry['id'];
                                                ?>
                                            
                                                <div class="row row-20 margin-t-40">
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Stage you are in the entrepreneurship journey
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_entrepreneurship_stage', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <div class="margin-t-40">                                            
                                                    <form action="<?php echo $base_url ?>/?action=esaed-entrepreneurship-profile-form" method="post">
                                                        <button class="sticky-list-edit submit btn btn btn-trans-bw txt-normal-s">Edit Profile</button>
                                                        <input type="hidden" name="mode" value="edit">
                                                        <input type="hidden" name="edit_id" value="<?php echo $entry_id; ?>">
                                                    </form>
                                                </div>
                                            
                                            <?php } //End If ?>
                                            
                                            
                                        <?php } //  End Foreach ?>
                                        
                                    <?php } else { ?>

                                        <div class="margin-t-40">
                                            <p>
                                                You do not have an Entrepreneurship profile.
                                            </p>
                                        </div>
                                        <div class="margin-t-40">
                                            <a class="btn btn-trans-bw txt-normal-s" href="<?php echo $base_url ?>/?action=esaed-entrepreneurship-profile-form">
                                                Add Profile
                                            </a>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>

                            <div class="border-b-1 border-color-darkgrey margin-b-10">
                                <button class="btn-link collapsed show txt-xlg" data-toggle="collapse" data-target="#main-5" aria-expanded="false" aria-controls="main-3">
                                    <span class="row align-items-center">
                                        <span class="padding-l-10">
                                            Tutorlink Profile
                                        </span>
                                    </span>
                                </button>
                                <div id="main-5" class="collapse show" aria-labelledby="heading-1" data-parent="#accordion">                                   
                                    <?php                                    
                                    
                                        $form_id = 42;  // Get eSAED Registration Form ID

                                        // Prepare Search Parameters
                                        $search_criteria = array(
                                            'status'        => 'active',
                                            'field_filters' => array(
                                                'created_by' => $current_user->ID
                                            )
                                        );

                                        // Search for entries for this User
                                        $entries = GFAPI::get_entries( $form_id );
                                        
                                    ?>

                                    <?php if($entries){ ?>

                                        <?php foreach($entries as $entry){ // Start foreach ?>
                                           
                                            <?php    if( $entry['created_by'] == $current_user->ID ){ ?>
                                                <?php
                                                    //var_dump($entry);
                                                    $post_id = $entry['post_id'];
                                                    $entry_id = $entry['id'];
                                                ?>
                                            
                                                <div class="row row-20 margin-t-40">
                                                    <div class="col-md-12">
                                                        <h3 class="txt-bold padding-lr-20 padding-b-20">
                                                            Nursery Teaching Experience
                                                        </h3>
                                                    </div>
                                                    <div class="col-md-4 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Classes of Nursery selected
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_nursery_classes', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Previous Nursery Teaching Experience</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_nursery_experience', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">For how long?</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_nursery_experience_length', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <div class="row row-20 margin-t-40">
                                                    <div class="col-md-12">
                                                        <h3 class="txt-bold padding-lr-20 padding-b-20">
                                                            Primary Teaching Experience
                                                        </h3>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Subjects you can teach
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_primary_subjects', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Select Class of Primary
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_primary_classes', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Previous Primary Teaching Experience</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_primary_experience', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Have you prepared pupils for common entrance exam?</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_primary_experience_length', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <div class="row row-20 margin-t-40">
                                                    <div class="col-md-12">
                                                        <h3 class="txt-bold padding-lr-20 padding-b-20">
                                                            Junior Secondary School Teaching Experience
                                                        </h3>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Subjects you can teach
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_jss_subjects', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Junior Secondary School Level you can teach
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_jss_level', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Previous Junior Secondary School Teaching Experience</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_jss_experience', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Which of these exams can you prepare students for?
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_jss_exams', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <div class="row row-20 margin-t-40">
                                                    <div class="col-md-12">
                                                        <h3 class="txt-bold padding-lr-20 padding-b-20">
                                                            Senior Secondary School Teaching Experience
                                                        </h3>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Subjects you can teach
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_sss_subjects', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Senior Secondary School Level you can teach
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_sss_level', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">Previous Junior Secondary School Teaching Experience</h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_sss_experience', true);

                                                                if($meta){
                                                                    echo $meta;
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-20">
                                                        <h3 class="txt-normal-s margin-b-5">
                                                            Which of these exams can you prepare students for?
                                                        </h3>
                                                        <p class="txt-bold txt-normal-s">
                                                            <?php
                                                                //  Get First Name
                                                                $meta = get_post_meta($post_id, 'esaed_tutorlink_sss_exams', false);

                                                                if($meta){
                                                                    for($i = 0; $i < count($meta); $i++){
                                                                        echo $meta[$i];
                                                                        
                                                                        if( count($meta) - $i > 1){
                                                                            echo ', ';
                                                                        }
                                                                    };
                                                                } else {
                                                                    echo '-';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                                <div class="margin-t-40">                                            
                                                    <form action="<?php echo $base_url ?>/?action=esaed-tutorlink-profile-form" method="post">
                                                        <button class="sticky-list-edit submit btn btn btn-trans-bw txt-normal-s">Edit Profile</button>
                                                        <input type="hidden" name="mode" value="edit">
                                                        <input type="hidden" name="edit_id" value="<?php echo $entry_id; ?>">
                                                    </form>
                                                </div>
                                            
                                            <?php } //End If ?>
                                            
                                            
                                        <?php } //  End Foreach ?>
                                        
                                    <?php } else { ?>

                                        <div class="margin-t-40">
                                            <p>
                                                You do not have an Tutorlink profile.
                                            </p>
                                        </div>
                                        <div class="margin-t-40">
                                            <a class="btn btn-trans-bw txt-normal-s" href="<?php echo $base_url ?>/?action=esaed-tutorlink-profile-form">
                                                Add Profile
                                            </a>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>
                        </section>
                        
                        <div class="margin-t-40">
                            <a class="btn btn-green txt-normal-s" href="https://skilli.ng/program/entrepreneurship-incubator/">
                                Submit eSAED Profile
                            </a>
                        </div>
                    </div>
                </div>
  

                <div class="col-md-12 mx-auto padding-tb-20 padding-lr-30 esaed-form">


                    <!-- eSAED FORM -->
                    <?php
                        // echo do_shortcode( "[gravityform id='36' title='false' description='false' ajax='false']"); 
                    ?>
                </div>