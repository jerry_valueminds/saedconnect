<div class="row row-10">
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Skills Splash
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Build world-class skills from wherever you are – as long as you have a smart phone.
            </h2>
            <div>
                <a class="btn btn-green txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                CV Creator
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Build compelling multimedia CVs employers can’t ignore and download as pdf in multiple designs.
            </h2>
            <div>
                <a class="btn btn-green txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Interactive Business Plan Creator
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Create your solid business plan using the interactive Business Plan creator.
            </h2>
            <div>
                <a class="btn btn-green txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Skills Splash
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Build world-class skills from wherever you are – as long as you have a smart phone.
            </h2>
            <div>
                <a class="btn btn-green txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                CV Creator
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Build compelling multimedia CVs employers can’t ignore and download as pdf in multiple designs.
            </h2>
            <div>
                <a class="btn btn-green txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex padding-lr-10 padding-b-20">
        <div class="bg-grey padding-o-20 card-shadow-4">
            <figure class="margin-b-20">
                <img src="images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
            </figure>
            <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                Interactive Business Plan Creator
            </h1>
            <h2 class="txt-sm txt-height-1-7 margin-b-30">
                Create your solid business plan using the interactive Business Plan creator.
            </h2>
            <div>
                <a class="btn btn-green txt-normal-s full-width" href="index_sms_states.html">
                    View
                </a>
            </div>
        </div>
    </div>
</div>