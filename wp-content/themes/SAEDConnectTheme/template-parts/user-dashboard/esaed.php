<?php
    /* Get Base URL */
    $base_url = get_site_url().'/my-dashboard';
?>


<?php
 
    $postTitleError = '';

    if ( isset( $_POST['submitted'] ) ) {

        if ( trim( $_POST['postTitle'] ) === '' ) {
            $postTitleError = 'Please enter a State code.';
            $hasError = true;
        }else{
            // eSAED CODE Generation
            $state = $_POST['postTitle'];
            $regno = substr($state, 0,4);
            $regno .= date("y");
            $regno .= time();
            
            
            // Get User ID
            $user_id = 1;
            
            // Save User Meta
            add_user_meta( $user_id, 'esaed_code', $regno );
            
            
            // Redirect after regustration
            $url = 'http://www.google.com/';
            
            //
            if ( wp_redirect( $url ) ) {
                exit;
            }
        }

    }
 
?>
   

<?php
    $current_user = wp_get_current_user();  // Get Currently logged in User

    $form_id = 36;  // Get eSAED Registration Form ID

    // Prepare Search Parameters
    $search_criteria = array(
        'status'        => 'active',
        'field_filters' => array(
            'mode' => 'any',
            'created_by' => $current_user->ID
        )
    );

    // Search for entries for this User
    $entries = GFAPI::get_entries( $form_id, $search_criteria );
?>
   

   
<?php if(!$entries){ ?>

    <header class="padding-b-40 margin-b-40 txt-light">
        <div class="row">
            <div class="col-md-8">
                <h1 class="txt-2em txt-height-1-2 margin-b-20">
                    Welcome,
                    <?php
                        
                        echo $current_user->display_name;
                    ?>
                </h1>
                <h2 class="txt-lg txt-height-1-7 margin-b-20">
                    Technology roles are becoming one of the highest-paying jobs globally, and you don’t require a college degree to be a tech star. The Tech Career Mentorship Scheme is designed to help people across all backgrounds to start up a brilliant career in tech related areas.
                </h2>
                <div class="margin-t-20">
                    <button type="button" class="btn btn-trans-green txt-normal-s" data-toggle="modal" data-target="#smsEnrollModal">
                        I have an eSAED Registration Code
                    </button>
                </div>
                <div class="margin-t-20">
                    <button type="button" class="btn btn-trans-bw txt-normal-s" data-toggle="modal" data-target="#formModal">
                        I do not have an eSAED Registration Code
                    </button>
                </div>
                <div class="margin-t-20">
                    <a class="btn btn-green txt-normal-s" href="<?php echo $base_url ?>/?action=esaed-form">
                        Continue eSAED Registration
                    </a>
                </div>
            </div>
        </div>
    </header>
    
    
    
    
    
    
    
    
    <!-- Enroll Online Modal -->
    <div class="modal fade font-main coming-soon-modal show" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row content">
                    <div class="left gf-white">
                        <form action="" id="primaryPostForm" method="POST">
                            <div class="gform_heading">
                                <h3 class="gform_title">
                                    Give us your information & we will contact you.
                                </h3>
                                <span class="gform_description">
                                    Tip: Enter outstanding achievements (not listed under your experiences) that show you can go above and beyond basic job expectations.)
                                </span>
                            </div>
                            <div class="gform_body">
                                <ul id="gform_fields_13" class="gform_fields top_label form_sublabel_below description_below">
                                    <li id="field_13_1" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
                                        <label class="gfield_label" for="input_13_1">
                                            State Code
                                        </label>
                                        <div class="ginput_container ginput_container_post_title">
                                            <input type="text" name="postTitle" value="" id="postTitle" class="required" />
                                        </div>
                                        <?php if ( $postTitleError != '' ) { ?>
                                            <p class="txt-normal-s txt-color-white margin-t-5"><?php echo $postTitleError; ?></p>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="gform_footer top_label">
                                <input type="hidden" name="submitted" id="submitted" value="true" />
                                <button class="gform_button" type="submit"><?php _e('Submit', 'framework') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php if ( $postTitleError != '' ) { ?>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#formModal').modal('show');
            });
        </script>
    <?php } ?>
    
<?php } else { 

    get_template_part( 'template-parts/user-dashboard/esaed-form' );    //Get eSAED Form View

} ?>


