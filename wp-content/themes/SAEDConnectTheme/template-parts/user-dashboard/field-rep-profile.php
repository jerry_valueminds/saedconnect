<main class="main-content txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper margin-b-20">
        <h1 class="txt-2-4em txt-bold margin-b-30">
            Field Rep/Data Collector Profile
        </h1>
    </section>
    
    <section class="container-wrapper margin-b-20">
        <div class="container-wrapper bg-white padding-tb-80">
        <?php
            switch_to_blog(20);
        
            $gf_id = 85;
        
            $entry_count = 0;

            /* Get current User ID */
            $current_user = wp_get_current_user();

            /* GF Search Criteria */
            $search_criteria = array(

            'field_filters' => array( //which fields to search

                array(

                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                )
              )
            );

            /* Get GF Entry Count */
            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
            
            if(!$entry_count){
                    /*$redirect_link = 'https://www.saedconnect.org/service-provider-directory/trainer-information-form/'; //Get Daasboard Page Link by ID

                    if ( wp_redirect( $redirect_link ) ) {
                        exit;
                    }*/
                
                echo '<script>window.location.replace("https://www.saedconnect.org/growth-programs/field-rep-data-collector-profile-form/");</script>';
            }
            
            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
        
            foreach( $entries as $entry ){
        ?>
                <div class="row row-10 border-b-1 padding-b-20 margin-b-20">
                    <div class="col-md-12 padding-lr-10 margin-b-20">
                        <p class="txt-lg txt-bold">
                            <?php echo rgar( $entry, '1' ); ?>
                        </p>
                    </div>

                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Skill Areas where you provide training
                        </p>
                        <p>
                            <?php
                
                                $field_id = 2; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Types of Trainings Offered
                        </p>
                        <p>
                            <?php
                
                                $field_id = 3; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Number of Years of Training Experience
                        </p>
                        <p>
                            <?php echo rgar( $entry, '4' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Brief Profile
                        </p>
                        <p>
                            <?php echo rgar( $entry, '5' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Location of your head office (Your Primary location)
                        </p>
                        <p>
                            <?php echo rgar( $entry, '6' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Locations where you offer these trainings
                        </p>
                        <p>
                            <?php
                
                                $field_id = 7; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $entry['form_id'], $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            
                            ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Name of Contact person for any enquiries
                        </p>
                        <p>
                            <?php echo rgar( $entry, '8' ); ?>
                        </p>
                    </div>
                    
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Primary Contact Address
                        </p>
                        <p>
                            <?php echo rgar( $entry, '9' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Contact Email address (Where anyone interested in this training should email for enquiry)
                        </p>
                        <p>
                            <?php echo rgar( $entry, '10' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Contact Phone Number (Where anyone interested in training with you should call for enquiry)
                        </p>
                        <p>
                            <?php echo rgar( $entry, '11' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Contact WhatsApp Number (Where anyone interested in training with you should WhatsApp for enquiry)
                        </p>
                        <p>
                            <?php echo rgar( $entry, '12' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Website
                        </p>
                        <p>
                            <?php echo rgar( $entry, '13' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Facebook
                        </p>
                        <p>
                            <?php echo rgar( $entry, '14' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Linkedin
                        </p>
                        <p>
                            <?php echo rgar( $entry, '15' ); ?>
                        </p>
                    </div>
                       
                    <div class="col-md-4 padding-lr-10 margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium margin-b-5">
                            Twitter
                        </p>
                        <p>
                            <?php echo rgar( $entry, '16' ); ?>
                        </p>
                    </div>
                    
                    
                    
                    
                    
                    <div class="col-md-12 padding-lr-10">
                        <a 
                           href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="637" action="edit" return="url" /]');  ?>" 
                           class="btn btn-blue txt-xs no-m-b"
                        >
                            Edit
                        </a>
                    </div>
            </div>
        <?php
            }
        
            restore_current_blog();
        ?>
        </div>
    </section>
</main>