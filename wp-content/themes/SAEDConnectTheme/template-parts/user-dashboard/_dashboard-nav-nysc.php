<?php
    /* Get current user */
    $current_user = wp_get_current_user();

    /* Get Avatar */
    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
    $meta_key = 'user_avatar_url';
    $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

    if($get_avatar_url){
        $avatar_url = $get_avatar_url;
    }

    /* Get User Display Name */
    switch_to_blog(1);

    $gf_id = 4; //Form ID
    $gv_id = 1385; //Gravity View ID
    $title = 'Profile';

    $entry_count = 0;

    /* GF Search Criteria */
    $search_criteria = array(

    'field_filters' => array( //which fields to search

        array(

            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
            )
        )
    );

    /* Get Entries */
    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

    /* Get GF Entry Count */
    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

    if($entry_count){ //If no entry
        foreach( $entries as $entry ){          
            $displayname = rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' );
        }                
    }                    
?>

<div class="dashboard-multi-main-menu">
    <figure class="avatar">
        <div class="img" style="background-image: url('<?php echo $avatar_url ?>')">
            <a href="https://www.saedconnect.org/my-avatar/" class="edit">
                <i class="fa fa-camera"></i>
            </a>
        </div>
    </figure>
    <div class="user-info-wrapper">
        <div class="user-info">
            <div class="name nysc">
                <?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?>

                <a 
                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=edit-profile&form-title='.$title ?>" 
                    class="edit"
                >
                    <i class="fa fa-pencil"></i>
                </a>
            </div>
        </div>
    </div>
    <?php restore_current_blog(); ?>
    <nav class="nav-wrapper">
        <ul class="cv-menu-list nysc">
            <li>
                <a href="https://www.saedconnect.org/nysc-dashboard/">
                    Dashboard                
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/nysc-national-monitor/">
                    National Monitor                 
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/nysc-state-monitor/">
                    State Monitor                 
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/nysc-complaint-manager/">
                    Complaint Manager                      
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/nysc-partner-manager/">
                    Partner Manager                  
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/nysc-annoucements/">
                    Annoucements                      
                </a>
            </li>
        </ul>
    </nav>
</div>