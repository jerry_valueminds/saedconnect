<header class="dashboard-main-nav">
    <style>
        .dashboard-main-nav {
            padding-top: 0;
        }
    </style>
    <div class="container-wrapper">
        <div class="wrapper">
            <a class="brand" href="https://www.saedconnect.org/">
                <span class="name">My SAEDConnect</span>
            </a> 
            <div class="user-account">
            <?php

                if ( is_user_logged_in() ) {
                    $current_user = wp_get_current_user();
            ?>

                <div class="dropdown">
                    <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="account-info">
                            <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                /* Get GF Entry Count */
                                $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                                if($entry_count){ //If no entry
                                    foreach( $entries as $entry ){          
                                        $displayname = rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' );
                                    }                
                                }   

                                restore_current_blog();
                            ?>
                            <figure class="profile-image" style="background-image: url('<?php echo $avatar_url; ?>')"></figure>
                            <span class="profile-name" style="text-transform:capitalize">
                                <?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?>
                            </span>
                        </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                        <a class="dropdown-item" href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information">
                            My Profile
                        </a>
                        <a class="dropdown-item" href="<?php echo wp_logout_url( 'https://www.saedconnect.org' ); ?> ">
                            Logout
                        </a>
                    </div>
                </div>

            <?php } else { ?>        

                <a class="login" href="https://www.saedconnect.org/login">Login</a>
                <a class="signup" href="https://www.saedconnect.org/register">Sign up</a>

            <?php } ?>
            </div>
            <div class="home-button bg-trans cv-menu-toggle">
                <button class="hamburger hamburger--spring menu-btn" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>

        </div>
    </div>
</header>

<?php 
    function currentUrl( $trim_query_string = false ) {
        $pageURL = (isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on') ? "https://" : "http://";
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        if( ! $trim_query_string ) {
            return $pageURL;
        } else {
            $url = explode( '?', $pageURL );
            return $url[0];
        }
    }
?>

<style>
    .work-profile{
        display: none !important;
    }

    .gform_wrapper .top_label .gfield_label {
        font-size: 0.8rem !important;
        font-weight: 500 !important;
    }

    .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
        font-size: 0.9rem !important;
        width: 100% !important;
    }

    

    .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
        display: none !important;
    }

    .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
        font-weight: 500;
        font-size: 0.9rem !important;
    }

    .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
        margin-right: 15px !important;
        display: inline-flex !important;
        align-items: center;
    }

    .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
        max-width: unset !important;
    }

</style>

<style>
    .gfield {
        margin-bottom: 40px;
    }
    
    .gfield_label{
        margin-bottom: 0 !important;
    }
    
    .gfield_checkbox li, .checkbox-item{
        position: relative;
    }
    
    .gfield_checkbox li input, .gfield_radio li input, .checkbox-item input{
        position: absolute;
        opacity: 0;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }    
    
    .gfield_checkbox li label, .gfield_radio li label, .checkbox-item .bg-label{
        font-weight: 400 !important;
        background-color: gainsboro;
        padding: 0.25em 1em !important;
        border-radius: 1em;
        cursor: pointer;
    }
    
    .gform_wrapper ul.gfield_checkbox li input[type=checkbox]:checked+label,
    
    .gform_wrapper ul.gfield_radio li input[type=radio]:checked+label,
    
    .checkbox-item input[type=checkbox]:checked+span
    
    {
        font-weight: 400;
        color: white !important;
        background-color: #00bfe7;
    }
    
    .gchoice_select_all label{
        background-color: darkgray !important;
    }
    
    .gchoice_select_all input[type=checkbox]:checked+label{
        background-color: green !important;
    }
    
    .gfield_required{
        color: #00bfe7 !important;
    }
    
    .gform_wrapper .gform_button, .gform_next_button {
        background-color: #00bfe7 !important;
        font-size: 0.9em !important;
        padding: 0.75em 1.5em !important;
        width: auto !important;
    }
    
    .gform_next_button{
        display: inline-block !important;
        color: white !important;
        width: auto !important;
    }
    
    .ginput_container_date{
        display: flex !important;
        align-items: center !important;
    }
    
    .checkbox-item .bg-label{
        padding: 0.75em 1em !important;
    }
</style>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>