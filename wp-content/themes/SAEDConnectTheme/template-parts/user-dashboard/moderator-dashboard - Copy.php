<?php 

    $current_user = wp_get_current_user();

    /* Get Base URL */
    $base_url = get_site_url().'/cv-forms';
?>
   
<main class="main-content">
    <header class="container-wrapper padding-t-60 padding-b-40">
        <div class="row">
            <div class="col-md-6">
                <h1 class="txt-2-4em txt-medium txt-2em margin-b-20">
                    Mentor | Moderator Dashboard
                </h1>
                <article>
                    <p class="txt-normal-s">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </p>
                </article>
            </div>
        </div>
        <!--<figure class="task-image" style="background-image: url(images/heroes/honda_1.jpg)">

        </figure>-->
        <!--<div class="margin-t-40">
            <a class="btn btn-trans-blue no-m-b" href="">
                Sign me up
            </a>
        </div>-->
    </header>

    <section class="container-wrapper padding-t-40 padding-b-40 bg-white">
        <div class="row">
            <div class="col-md-10">
                <header>
                    <h1 class="txt-xlg txt-height-1-1 margin-b-20">
                        My Forums
                    </h1>
                    <h2 class="txt-normal-s txt-height-1-7">
                        We are going all out to ensure you get all the support &amp; resources you require to start and excel in the Import/Export Business. 
                    </h2>
                </header>
            </div>
        </div>
    </section>

    <section class="container-wrapper bg-white padding-b-80">
    <!-- Side Hustle Communities -->
    <?php
        /* Get Assigned Communities */
        $communities = get_user_meta( $current_user->ID, 'moderator_select_shc', true );
        
        if( $communities ){
    ?>
        <div class="overflow-hidden">
            <div class="row row-15 txt-normal-s txt-bold padding-b-20 margin-b-20 border-b-1 border-color-grey">
                <div class="col-md-4 padding-lr-15">
                    Forum
                </div>
                <div class="col-md-5 padding-lr-15">
                    Description
                </div>
                <div class="col-md-2 text-center padding-lr-15 text-center">

                </div>
            </div>
        </div>
        
        <?php
            foreach($communities as $community ){
                $post = get_post($community);
        ?>
       
        <div class="row row-15 forum-list">
            <div class="col-md-4 padding-lr-15">
                <h3 class="txt-height-1-2 txt-color-dark">
                    <a class="txt-medium txt-color-dark" href="<?php the_permalink() ?>">
                        <?php the_title() ?>
                    </a>
                </h3>
                <div class="margin-t-10">
                <?php
                    foreach( $current_user->roles as $role ){
                        if($role == 'administrator'){
                ?>
                    <span class="d-inline-block txt-xs txt-color-white txt-medium padding-tb-5 padding-lr-10 bg-blue rounded-corners">
                        <?php echo 'Admin' ?>
                    </span>
                    
                <?php
                        } else if ($role == 'bbp_keymaster'){
                ?>
                    <span class="d-inline-block txt-xs txt-medium padding-tb-5 padding-lr-10 bg-yellow rounded-corners">
                        <?php echo 'Moderator'; ?>
                    </span>
                <?php
                        }
                    }
                ?>
                </div>
            </div>
            <div class="col-md-5 padding-lr-15">
                <article class="txt-sm txt-height-1-7">
                    <?php echo rwmb_meta( 'info-session-description', $post->ID ); ?>
                </article>
            </div>
            <div class="col-md-3 padding-lr-15 text-md-right">
                <a href="<?php echo rwmb_meta( 'info-session-community-forum', $post->ID ); ?>" class="btn btn-blue txt-sm no-m-b">
                    Go to Forum
                </a>
            </div>
        </div>
        
        <?php } ?>
        
    <?php
        } else {
            echo '<p class="txt-normal-s txt-bold">No Communities have been assigned.</p>';
        }
    ?>
    </section>
</main>



<!-- Enroll Online Modal -->
<style>
    .gform_wrapper .top_label .gfield_label {
        font-size: 0.8rem !important;
        margin-bottom: 0 !important;
    }

    .ginput_container input, .ginput_container select, .ginput_container textarea{
        width: 100% !important;
        font-size: 0.9rem !important;
        padding: 0.4rem 0.8rem !important;
    }

    .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
        margin-top: 0px !important;
    }

    .gform_button.button{
        margin: 0 auto !important;
        width: auto !important;
    }
    
    input[name="input_6"], input[name="input_3"], input[name="input_2"]{
        opacity: 0.5;
    }
</style>

<script type="text/javascript">
    /* Disable inputs */
    $(window).on('load',function(){
        //$( 'input[name="input_6"], input[name="input_3"], input[name="input_2"]' ).prop( "disabled", true ); //Disable
        
        /* Remove options already registered for */
        <?php foreach($exclude_list as $list_item){ ?>
            $("select[name='input_8'] option[value='<?php echo $list_item ?>']").remove();
        <?php } ?>
    });
</script>

<!-- Form Modal -->
<div class="modal fade font-main coming-soon-modal show" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row content">
                <div class="left gf-white full-width">
                    <div class="gform_heading">
                        <h3 class="gform_title">
                            Join a Community
                        </h3>
                    </div>
                    
                    <?php
                        /* Get User Phone number from GF User Registratiion Form */
                        
                        /* Variables */
                        $phone = 000;
                        //$gf_id = 37;
                        $gf_id = 21;
                        $user_id = $current_user->ID;
                    
                        $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'value' => $user_id );

                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                    
                        if($entries){
                            foreach($entries as $entry){
                                if( rgar( $entry, '4' ) ){
                                    $phone = rgar( $entry, '4' );
                                }
                            }
                        }
                    ?>

                    <?php echo do_shortcode('[gravityform id="21" title="false" description="false" field_values="full-name='.$current_user->display_name.'&email='.$current_user->user_email.'&phone='.$phone.'"]'); ?>
                    <?php //echo do_shortcode('[gravityform id="21" title="false" description="false"]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Confirmation Modal -->
<div class="modal fade font-main coming-soon-modal show" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row content">
                <div class="left gf-white full-width">
                    <div class="gform_heading">
                        <h3 class="txt-bold txt-height-1-5 text-center">
                            <?php echo $_GET['confirmation'] ?>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
    /* If There's an error, Open Form Modal */
    if ( $_POST ) {
?>
   
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#formModal').modal('show');
        });
    </script>
    
<?php } ?>


<?php
    /* On form submission, show confirmation */
    if ( $_GET['confirmation'] ) {
?>
   
    <script type="text/javascript">
        $(window).on('load',function(){
            
            /* Open Confirmation Modal */
            $('#confirmationModal').modal('show');
            
            /* Clean URL (Remove Confirmation Query String)  */
            var uri = window.location.toString();

            if (uri.indexOf("?") > 0) {

                var clean_uri = uri.substring(0, uri.indexOf("?"));

                window.history.replaceState({}, document.title, clean_uri);

            }
        });
    </script>
    
<?php 
        $_GET = array();
    } 
?>