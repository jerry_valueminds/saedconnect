<?php 

    $current_user = wp_get_current_user();

    /* Get Base URL */
    $base_url = get_site_url().'/growth-programs/cv-forms';
?>
  
<?php
 
    $postTitleError = '';

    if ( isset( $_POST['submitted'] ) ) {

        if ( trim( $_POST['postTitle'] ) === '' ) {
            $postTitleError = 'Please select a role.';
            $hasError = true;
        }else{
            // eSAED CODE Generation
            $role_id = $_POST['postTitle'];
            
            // Get User ID
            $user_id = 1;
            
            // Save User Meta
            add_user_meta( get_current_user_id(), 'role_id', $role_id );
            
            
            // Redirect after regustration
            $url = 'http://www.google.com/';
            
            //
            if ( wp_redirect( $url ) ) {
                exit;
            }
        }

    }
 
?>
   

<main class="txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper margin-b-20">
        <div class="container-wrapper bg-white padding-tb-80">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="txt-2-4em txt-bold margin-b-30">
                        Career Dashboard
                    </h1>
                    <p class="txt-height-1-7">
                        Welcome to your Career Development Dashboard. From your dashboard, you will be able to access the Job Advisor Forum, create your resume, Add Job roles you are interested in getting hired for, and manage your experience profile across each Job role.
                    </p>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-2">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_test_your_ideas.png" alt="" width="160">
                </div>
            </div>
        </div>
    </section>
    
    <section class="margin-b-40">
        <div class="container-wrapper">
            <div class="row row-10">
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            My CV
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Create your solid business plan using the interactive Business Plan creator.
                        </h2>
                        <div>
                            <a 
                                class="btn btn-blue txt-xs full-width" 
                                href="<?php echo $dashboard_base.'?action=cv&cv-view=overview' ?>"
                            >
                                View
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            My Skills Profile
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Create your solid business plan using the interactive Business Plan creator.
                        </h2>
                        <div>
                            <a class="btn btn-blue txt-xs full-width" href="<?php echo $dashboard_base.'?action=skills-profile' ?>">
                                View
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            Job Advisor
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Earn badges that demonstrate your competence in a skill, and get a job guarantee.
                        </h2>
                        <div>
                            <a class="btn btn-blue txt-xs full-width" href="https://www.saedconnect.org/growth-programs/program/career-forum/">
                                View
                            </a>
                        </div>
                    </div>
                </div>
                               
                <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                    <div class="bg-white padding-o-20 card-shadow-4">
                        <figure class="margin-b-20">
                            <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/skilli/access-to-Growth-opportunities.png" alt="" width="50">
                        </figure>
                        <h1 class="txt-bold txt-normal-s txt-height-1-4 margin-b-10">
                            Request Expert Help
                        </h1>
                        <h2 class="txt-sm txt-height-1-5 margin-b-20">
                            Earn badges that demonstrate your competence in a skill, and get a job guarantee.
                        </h2>
                        <ul class="icon-list blue txt-sm margin-l-10">
                            <li>
                                <a href="https://www.saedconnect.org/growth-programs/find-your-path-form/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Career & Personality Clarity
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/growth-programs/request-cv-creation-linkedin-page-creation/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        CV Creation
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/growth-programs/request-cv-creation-linkedin-page-creation/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Linked in Profile Optimization
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Job Search Coaching
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Interview Coaching
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                                
            </div>
        </div>
    </section>
    
    <!-- Enroll Online Modal -->
    <div class="modal fade font-main coming-soon-modal show" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row content">
                    <div class="left gf-white full-width">
                        <form action="" id="primaryPostForm" method="POST">
                            <div class="gform_heading">
                                <h3 class="gform_title">
                                    Add Role
                                </h3>
                            </div>
                            <div class="gform_body">
                                <ul id="gform_fields_13" class="gform_fields top_label form_sublabel_below description_below">
                                    <li id="field_13_1" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
                                        <label class="gfield_label" for="input_13_1">
                                            
                                        </label>
                                        <div class="ginput_container ginput_container_post_title">
                                            
                                            <select name="postTitle" id="postTitle" class="required">
                                                <option value="0" selected hidden disabled>
                                                    - Select Role - 
                                                </option>
                                            <?php // Display posts
                                                $temp = $wp_query; $wp_query= null;
                                                $wp_query = new WP_Query();
                                                $wp_query->query(array('post_type' => 'career-role'));
                                                    while ($wp_query->have_posts()) : $wp_query->the_post();
                                            ?>
                                                       
                                                    <option value="<?php the_id() ?>">
                                                        <?php the_title() ?>
                                                    </option>
                                                    
                                            <?php
                                                endwhile;

                                                //Reset Query
                                                wp_reset_postdata();
                                                wp_reset_query();
                                                $wp_query = $temp;
                                            ?>
                                            </select>
                                        </div>
                                        <?php if ( $postTitleError != '' ) { ?>
                                            <p class="txt-normal-s txt-color-white margin-t-5"><?php echo $postTitleError; ?></p>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="gform_footer top_label">
                                <input type="hidden" name="submitted" id="submitted" value="true" />
                                <button class="gform_button" type="submit"><?php _e('Submit', 'framework') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Confirmation Modal -->
    <div class="modal fade font-main coming-soon-modal show" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row content">
                    <div class="left gf-white full-width">
                        <div class="gform_heading">
                            <h3 class="txt-bold txt-height-1-5 text-center">
                                <?php echo $_GET['confirmation'] ?>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php if ( $postTitleError != '' ) { ?>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#formModal').modal('show');
            });
        </script>
    <?php } ?>
    
    <?php
        /* On form submission, show confirmation */
        if ( $_GET['confirmation'] ) {
    ?>

        <script type="text/javascript">
            $(window).on('load',function(){

                /* Open Confirmation Modal */
                $('#confirmationModal').modal('show');

                /* Clean URL (Remove Confirmation Query String)  */
                var uri = window.location.toString();

                if (uri.indexOf("?") > 0) {

                    var clean_uri = uri.substring(0, uri.indexOf("?"));

                    window.history.replaceState({}, document.title, clean_uri);

                }
            });
        </script>

    <?php 
            $_GET = array();
        } 
    ?>
    
</main>