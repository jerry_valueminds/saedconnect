<style>
    .work-profile{
        display: none !important;
    }
</style>
<?php
    $base_cv_url = 'https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv';

    /* Variables */
    $current_cv_view_name = 'Summary';

    /* CV Array Links */
    $cv_links = array(
        array(
            'title' => 'Personal Information',
            'template' => 'personal-information',
            'gravityFormID' => 84,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Overview Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'About Me',
                    'field_id' => 2,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Summary',
            'template' => 'summary',
            'gravityFormID' => 27,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Headline',
                    'field_id' => 4,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Summary about yourself',
                    'field_id' => 5,
                    'type' => 'textarea'
                ),
            )
        ),
        
        /*array(
            'title' => 'Personal Information',
            'template' => 'personal-information',
            'gravityFormID' => 26,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Date of Birth',
                    'field_id' => 7,
                    'type' => 'date'
                ),
                
                
                 array(
                    'label' => 'Marital Status',
                    'field_id' => 8,
                    'type' => 'date'
                ),
                
                array(
                    'label' => 'Gender',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Nationality',
                    'field_id' => 10,
                    'type' => 'textarea'
                ),
            )
        ),*/

        array(
            'title' => 'Accomplishments',
            'template' => 'accomplishments',
            'gravityFormID' => 33,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 3,
                    'type' => 'textarea'
                ),
            )
        ),

        array(
            'title' => 'Additional Information',
            'template' => 'additional-information',
            'gravityFormID' => 42,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
        
        /*array(
            'title' => 'Affiliations',
            'template' => 'affiliations',
            'gravityFormID' => 36,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Your Achievements in the group',
                    'field_id' => 18,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Your Role in the Group',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Time Period',
                    'field_id' => 12,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Are you still a member of this group?',
                    'field_id' => 13,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'From',
                    'field_id' => 14,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'To',
                    'field_id' => 15,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 16,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Your Achievements in the group',
                    'field_id' => 17,
                    'type' => 'textarea'
                ),
            )
        ),*/
        
        /*array(
            'title' => 'Awards',
            'template' => 'awards',
            'gravityFormID' => 32,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Awarder/Awarder Website',
                    'field_id' => 6,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Month/Year',
                    'field_id' => 7,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 8,
                    'type' => 'textarea'
                ),
            )
        ),*/
        
        array(
            'title' => 'Certifications / Affiliations',
            'template' => 'certifications-affiliations',
            'gravityFormID' => 35,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Time Period',
                    'field_id' => 9,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Certification Expiration',
                    'field_id' => 10,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Valid from',
                    'field_id' => 11,
                    'type' => 'date'
                ),
                
                array(
                    'label' => 'To',
                    'field_id' => 12,
                    'type' => 'date'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Social Details',
            'template' => 'contact-details',
            'gravityFormID' => 28,
            'gravityViewID' => 1,
            'numberOfEntries' => 'single',
            'fields' => array(
                array(
                    'label' => 'Email',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Phone',
                    'field_id' => 12,
                    'type' => 'text'
                ),
                                
                array(
                    'label' => 'Address',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
                
                array(
                    'label' => 'Current State of Residence',
                    'field_id' => 14,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Personal Blog/Website',
                    'field_id' => 15,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Facebook',
                    'field_id' => 16,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Twitter',
                    'field_id' => 17,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Instagram',
                    'field_id' => 18,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Linkedin',
                    'field_id' => 19,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Education & Training',
            'template' => 'education-training',
            'gravityFormID' => 31,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Degree Attained (e.g BA, BS, JD, PhD)',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'School',
                    'field_id' => 8,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Start Month/Year',
                    'field_id' => 10,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'End Month/Year (Current students: Enter your expected graduation year)',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Achievements',
                    'field_id' => 12,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Experiences',
            'template' => 'experiences',
            'gravityFormID' => 29,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Position/Job Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Company/Company Website',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Location',
                    'field_id' => 10,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Start Month/Start Year',
                    'field_id' => 11,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'End Month/Year',
                    'field_id' => 12,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Describe what you did/Your Achievements',
                    'field_id' => 13,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Interests',
            'template' => 'Interests',
            'gravityFormID' => 39,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
        
        array(
            'title' => 'Languages',
            'template' => 'languages',
            'gravityFormID' => 40,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Language Name',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
                
        /*array(
            'title' => 'Patents',
            'template' => 'Patents',
            'gravityFormID' => 37,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Title',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 10,
                    'type' => 'textarea'
                ),
            )
        ),*/
                
        /*array(
            'title' => 'Personal Projects',
            'template' => 'personal-projects',
            'gravityFormID' => 30,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Project Name',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),*/
        
        /*array(
            'title' => 'Publications',
            'template' => 'publications',
            'gravityFormID' => 38,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Project Name',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'URL',
                    'field_id' => 6,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Publication Date',
                    'field_id' => 7,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 8,
                    'type' => 'textarea'
                ),
            )
        ),*/
        
        array(
            'title' => 'References',
            'template' => 'references',
            'gravityFormID' => 41,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Name of Referee',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Position',
                    'field_id' => 7,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Email Address',
                    'field_id' => 8,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Telephone Number',
                    'field_id' => 9,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Reference Available on Request?',
                    'field_id' => 10,
                    'type' => 'text'
                ),
            )
        ),
        
        array(
            'title' => 'Skills',
            'template' => 'skills',
            'gravityFormID' => 34,
            'gravityViewID' => 1,
            'numberOfEntries' => 'multiple',
            'fields' => array(
                array(
                    'label' => 'Skill Name',
                    'field_id' => 1,
                    'type' => 'text'
                ),
                
                array(
                    'label' => 'Description',
                    'field_id' => 4,
                    'type' => 'textarea'
                ),
            )
        ),
    );
    
    /* Get currently active CV from Query String  */
    $current_cv_view = $_REQUEST['cv-view'];
?>

<main class="main-content txt-color-light bg-white" style="margin-top: 70px">
    <header class="container-wrapper dashboard-multi-header bg-purple-career padding-tb-20">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h1 class="txt-xlg txt-medium txt-color-white">
                    My CV
                </h1>
            </div>
        </div>
    </header>

    <section class="container-wrapper" style="padding-top: 62px">
        <div class="row">
            <div class="col-md-2 dashboard-multi-main-menu">
                <div class="cv-menu-toggle">
                    CV Menu
                </div>
                <ul class="cv-menu-list">
                <?php 
                    $active = false;
                    
                    foreach( $cv_links as $link ){ 
                        
                        if( $current_cv_view == $link['template'] ){
                            $current_cv = $link;
                            $active = true;
                            $current_cv_view_name = $link['title'];
                        }
                            
                        
                ?>
                    <li>
                        <a
                            class="<?php echo $active ? 'active' : '' ?>"
                            href="<?php echo $base_cv_url.'&cv-view='.$link['template'] ?>"
                        >
                            <?php echo $link['title'] ?>
                        </a>
                    </li>
                <?php 
                        $active = false;
                    } 
                ?>
                </ul>
            </div>
            
            <div class="col-md-7 dashboard-multi-main-content">
                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php
                                /* Get View Title */
                                echo $current_cv_view_name;
                            ?>                
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/growth-programs/track/starter-track/entry/40/?edit=68fd6eebe5&amp;gvid=637" 
                               class="edit-btn"
                            >
                                Edit
                            </a>
                        </div>
                    </div>
                    <?php

                        $gf_id = $current_cv['gravityFormID']; //Form ID
                        $gv_id = $current_cv['gravityviewID']; //Gravity View ID

                        $entry_count = 0;

                        /* Get current User ID */
                        $current_user = wp_get_current_user();

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                          )
                        );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                        /* If no entry */
                        if(!$entry_count){
                    ?>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Empty! Start by adding your <?php echo $current_cv['title'] ?>.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-career-purple txt-xxs no-m-b" 
                                    data-toggle="collapse" 
                                    href="#form" role="button" 
                                    aria-expanded="false" 
                                    aria-controls="collapseExample"
                                >
                                    Add <?php echo $current_cv['title'] ?>
                                </a>
                            </div>
                            <div class="collapse" id="form">
                                <div class="">
                                    <?php 
                                        echo do_shortcode( "[gravityform id='".$gf_id."' title='false' description='false' ajax='false']"); 
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php

                        } else {

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            foreach( $entries as $entry ){
                    ?>
                        
                        <div class="entry">
                            <?php 

                                foreach( $current_cv['fields'] as $field ) { 

                                    if( true ){

                            ?>
                            
                            <div class="margin-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    <?php echo $field['label']; ?>                        
                                </p>
                                <p class="txt-sm">
                                    <?php echo rgar( $entry, $field['field_id'] ); ?>                       
                                </p>
                            </div>
                            
                            <?php 
                                    }
                                } 
                            ?>

                            <div class="text-right padding-b-20">
                                <a 
                                   href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="637" action="edit" return="url" /]');  ?>" 
                                   class="btn btn-blue txt-xs no-m-b"
                                >
                                    Edit
                                </a>
                            </div>
                        </div>

                    <?php
                            }


                        /* if Multiple Entries allowed */
                        if( $current_cv['numberOfEntries'] == 'multiple' ){


                            echo do_shortcode( "[gravityform id='".$gf_id."' title='false' description='false' ajax='false']");
                        }




                        }
                    ?>
                </div>
            </div>
            
            <div class="col-md-3 dashboard-multi-main-sidebar">
                <div class="side-bar-card">
                    <h4 class="txt-medium txt-normal-s txt-color-dark margin-b-20">
                        Print CV
                    </h4>
                    <p class="txt-xs">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ut, commodi, ea nisi voluptatibus error.
                    </p>
                    <div class="margin-t-20">
                        <a href="" class="btn btn-career-purple txt-xxs no-m-b">
                            <i class="fa fa-print"></i>
                            <span class="padding-l-5">
                                Print
                            </span>
                        </a>
                    </div>
                </div>
                <div class="side-bar-card">
                    <h4 class="txt-medium txt-color-dark margin-b-20">
                        Download CV
                    </h4>
                    <p class="txt-sm">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ut, commodi, ea nisi voluptatibus error.
                    </p>
                    <div class="margin-t-20">
                        <a href="" class="btn btn-career-purple txt-xxs no-m-b">
                            <i class="fa fa-download"></i>
                            <span class="padding-l-5">
                                Download
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<main class="main-content txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper">
        <div class="row align-items-center margin-b-80">
            <div class="col-md-6">
                <h1 class="txt-2em txt-bold">
                    My CV
                </h1>
            </div>
        </div>
    </section>
    
    <section class="container-wrapper margin-b-20">
        <div class="row">
            <div class="col-md-3 padding-r-20">
                <ul class="">
                <?php 
                    $active = false;
                    
                    foreach( $cv_links as $link ){ 
                        
                        if( $current_cv_view == $link['template'] ){
                            $current_cv = $link;
                            $active = true;
                            $current_cv_view_name = $link['title'];
                        }
                            
                        
                ?>
                    <li class="margin-b-20">
                        <a
                            class="txt-normal-s txt-medium txt-color-lighter <?php echo $active ? 'txt-color-blue' : '' ?>"
                            href="<?php echo $base_cv_url.'&cv-view='.$link['template'] ?>"
                        >
                            <?php echo $link['title'] ?>
                        </a>
                    </li>
                <?php 
                        $active = false;
                    } 
                ?>
                </ul>
            </div>
            <div class="col-md-7 bg-white padding-o-40">
                <h2 class="txt-lg txt-medium txt-color-white-off margin-b-40">
                    <?php
                        /* Get View Title */
                        echo $current_cv_view_name;
                    ?>
                </h2>
            <?php

                $gf_id = $current_cv['gravityFormID']; //Form ID
                $gv_id = $current_cv['gravityviewID']; //Gravity View ID

                $entry_count = 0;

                /* Get current User ID */
                $current_user = wp_get_current_user();
                
                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search

                    array(

                        'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                    )
                  )
                );

                /* Get GF Entry Count */
                $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                if(!$entry_count){
            ?>
                    <h3 class="txt-medium margin-b-40">
                        Empty! Start by adding your <?php echo $current_cv['title'] ?>.
                    </h3>
                    
                    <div>
                        <a 
                            class="btn btn-blue txt-xs" 
                            data-toggle="collapse" 
                            href="#form" role="button" 
                            aria-expanded="false" 
                            aria-controls="collapseExample"
                        >
                            Add <?php echo $current_cv['title'] ?>
                        </a>
                    </div>
                    <div class="collapse" id="form">
                        <div class="">
                            <?php 
                                echo do_shortcode( "[gravityform id='".$gf_id."' title='false' description='false' ajax='false']"); 
                            ?>
                        </div>
                    </div>
            <?php
                    
                } else {

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                
                    foreach( $entries as $entry ){
            ?>
                
                <div class="Entry margin-b-15 padding-b-15 border-b-1">
                    <?php 
                        
                        foreach( $current_cv['fields'] as $field ) { 
                            
                            if( true ){
                    
                    ?>
                    <div class="margin-b-20">
                        <p class="txt-sm txt-color-dark txt-medium padding-b-5">
                            <?php echo $field['label']; ?>
                        </p>
                        <p class="txt-normal-s">
                            <?php echo rgar( $entry, $field['field_id'] ); ?>
                        </p>
                    </div>
                    
                    
                    <?php 
                            }
                        } 
                    ?>

                    <div class="text-right">
                        <a 
                           href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="637" action="edit" return="url" /]');  ?>" 
                           class="btn btn-blue txt-xs no-m-b"
                        >
                            Edit
                        </a>
                    </div>
                </div>

            <?php
                    }
                    
                    
                /* if Multiple Entries allowed */
                if( $current_cv['numberOfEntries'] == 'multiple' ){


                    echo do_shortcode( "[gravityform id='".$gf_id."' title='false' description='false' ajax='false']");
                }
                    
                    
                    
                    
                }
            ?>
            </div>
            
            <div class="col-md-2 padding-l-40">
                <h4 class="txt-bold txt-color-dark margin-b-20">
                    Actions
                </h4>
                <ul class="">
                    <li class="margin-b-20">
                        <a
                            class="txt-normal-s txt-medium txt-color-lighter <?php echo $active ? 'txt-color-blue' : '' ?>"
                            href="<?php echo $base_cv_url.'&cv-view='.$link['template'] ?>"
                        >
                            Preview CV
                        </a>
                    </li>
                    <li class="margin-b-20">
                        <a
                            class="txt-normal-s txt-medium txt-color-lighter <?php echo $active ? 'txt-color-blue' : '' ?>"
                            href="<?php echo $base_cv_url.'&cv-view='.$link['template'] ?>"
                        >
                            Print CV
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
</main>


