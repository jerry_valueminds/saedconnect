<?php
    /* Get current user */
    $current_user = wp_get_current_user();

    /* Get Avatar */
    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
    $meta_key = 'user_avatar_url';
    $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

    if($get_avatar_url){
        $avatar_url = $get_avatar_url;
    }

    /* Get User Display Name */
    switch_to_blog(1);

    $gf_id = 4; //Form ID
    $gv_id = 1385; //Gravity View ID
    $title = 'Profile';

    $entry_count = 0;

    /* GF Search Criteria */
    $search_criteria = array(

    'field_filters' => array( //which fields to search

        array(

            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
            )
        )
    );

    /* Get Entries */
    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

    /* Get GF Entry Count */
    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

    if($entry_count){ //If no entry
        foreach( $entries as $entry ){          
            $displayname = rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' );
        }                
    }                    
?>

<div class="dashboard-multi-main-menu">
    <figure class="avatar">
        <div class="img" style="background-image: url('<?php echo $avatar_url ?>')">
            <a href="https://www.saedconnect.org/my-avatar/" class="edit">
                <i class="fa fa-camera"></i>
            </a>
        </div>
    </figure>
    <div class="user-info-wrapper">
        <div class="user-info">
            <div class="name">
                <?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?>

                <a 
                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=edit-profile&form-title='.$title ?>" 
                    class="edit"
                >
                    <i class="fa fa-pencil"></i>
                </a>
            </div>
        </div>
    </div>
    <?php restore_current_blog(); ?>
    <nav class="nav-wrapper">
        <ul class="cv-menu-list">
            <li>
                <a href="#collapseExample">
                    Welcome                 
                </a>
            </li>
            <li>
                <a href="#collapseExample">
                    My Profile                 
                </a>
            </li>
            <li>
                <a 
                    class="has-child" 
                    href="#collapseExample"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="collapseExample"
                    role="button"
                >
                    My Profile                    
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information">
                                Personal Profile                  
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/competency-profile/education-experience-profile/"
                        >
                                Education & Experience          
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/competency-profile/my-skills/"
                        >
                                Skills Profile          
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/service-marketplace/my-capabilities/">
                                Capability Profile                 
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/service-provider-directory/trainer-profile/"
                        >
                                Trainer Profile        
                            </a>
                        </li>
                        <li>
                            <a href="http://www.saedconnect.org/growth-programs/mobile-workforce/"
                        >
                                Mobile Workforce   
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/competency-profile/my-uploads/"
                        >
                                Uploads     
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="https://www.saedconnect.org/competency-profile/my-messages/">
                    Messages                        
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/ventures-directory/my-businesses/">
                    My Businesses                        
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/ventures-directory/my-projects/">
                    My Projects                   
                </a>
            </li>
            <li>
                <a href="https://www.saedconnect.org/opportunity-center/my-programs/">
                    My Programs                       
                </a>
            </li>
            <li>
                <a 
                    class="has-child"
                    href="#offers"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="offers"
                    role="button"
                >
                    My Training & Service Offers                    
                </a>
                <div class="collapse" id="offers">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/service-marketplace/my-service-offers/"
                        >
                                Service Offers                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/service-provider-directory/my-training-offers/"
                        >
                                Training Offers                       
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            
            <li>
                <a 
                    class="has-child"
                    href="#myJobs"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="myJobs"
                    role="button"
                >
                    My Jobs                       
                </a>
                <div class="collapse" id="myJobs">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/service-marketplace/my-jobs/"
                        >
                                My Job Posts                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/service-marketplace/job-interactions/"
                        >
                                My Job Applications                  
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a 
                    class="has-child"
                    href="#talentRequests"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="talentRequests"
                    role="button"
                >
                    My Requests                       
                </a>
                <div class="collapse" id="talentRequests">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/service-marketplace/my-tasks/"
                        >
                                My Task/gig Requests                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/service-provider-directory/my-training-requests/"
                        >
                                My Training Requests                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/helpsquare-requests/"
                        >
                                HelpSquare Requests                       
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <!--<li>
                <a 
                    class="has-child"
                    href="#helpRequests"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="helpRequests"
                    role="button"
                >
                    My Help Requests                    
                </a>
                <div class="collapse" id="helpRequests">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-partner-requests/"
                        >
                                Business Partner Requests                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-mentor-requests/"
                            >
                                Mentorship Requests                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-marketing-requests/"
                            >
                                Marketing/Publicity  Support Requests                      
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-tool-requests/"
                        >
                                Tool/Equipment Request                      
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-workspace-requests/"
                        >
                                Officespace/workspace request                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-land-requests/"
                        >
                                Land & Landed Property Request                      
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-funding-requests/"
                            >
                                Funding Request                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-volunteer-requests/"
                            >
                                Volunteer Requests                      
                            </a>
                        </li>
                    </ul>
                </div>
            </li>-->
            <!--<li>
                <a 
                    class="has-child"
                    href="#businesses"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="businesses"
                    role="button"
                >
                    My Interactions                        
                </a>
                <div class="collapse" id="businesses">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-business-interactions/"
                        >
                                Businesses I have engaged                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/my-project-interactions/"
                        >
                                Projects I have engaged                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/helpsquare-interactions/"
                        >
                                Help Square Requests I have engaged                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/opportunity-center/opportunity-interactions/"
                        >
                                Opportunity Applications                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/service-marketplace/service-offer-interactions/"
                        >
                                Service Offers I have engaged                  
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/service-provider-directory/training-offer-interactions/"
                        >
                                Trainings Offers I have engaged                       
                            </a>
                        </li>
                        
                        <li>
                            <a href="https://www.saedconnect.org/service-provider-directory/training-request-interactions/"
                        >
                                Training Requests I have engaged         
                            </a>
                        </li>
                    </ul>
                </div>
            </li>-->
            <!--<li>
                <a 
                    class="has-child"
                    href="#enquiries"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="enquiries"
                    role="button"
                >
                    Enquiries & Applications                   
                </a>
                <div class="collapse" id="enquiries">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/business-enquiry/"
                        >
                                Business Enquiries                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/ventures-directory/project-enquiries/"
                        >
                                Project Enquiries                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                        >
                                Program Applications                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                        >
                                Service offer enquiries                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                        >
                                Job Offer Applications                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                        >
                                Training Offer Enquiries                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                        >
                                Training request responses                       
                            </a>
                        </li>
                    </ul>
                </div>
            </li>-->
            <li>
                <a href="https://www.saedconnect.org/service-marketplace/micro-tasks/">
                    Micro Tasks               
                </a>
            </li>
            <li>
                <a 
                    class="has-child"
                    href="#settings"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="settings"
                    role="button"
                >
                    Settings                       
                </a>
                <div class="collapse" id="settings">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/password-reset/"
                        >
                                Reset Password                      
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/profile-visibility/"
                        >
                                Profile Visibility                      
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a 
                    class="has-child"
                    href="#nysc-saed"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="nysc-saed"
                    role="button"
                >
                    NYSC SAED                     
                </a>
                <div class="collapse" id="nysc-saed">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/nysc-saed-profile"
                        >
                                Register                     
                            </a>
                        </li>
                        <li>
                            <a href=""
                        >
                                SAED eLogbook                     
                            </a>
                        </li>
                        <li>
                            <a href=""
                        >
                                My SAED Certificate                     
                            </a>
                        </li>
                        <li>
                            <a href=""
                        >
                                Complaint / Feedback                   
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            
            
            <?php if( $current_user->ID == 1 ){ ?>
            
            <li>
                <a 
                    class="has-child"
                    href="#saed-monitoring"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="saed-monitoring"
                    role="button"
                >
                    SAED Monitoring                     
                </a>
                <div class="collapse" id="saed-monitoring">
                    <ul class="sub-list">
                        <li>
                            <a href=""
                        >
                                State Monitor                     
                            </a>
                        </li>
                        <li>
                            <a href=""
                        >
                                Complaint Manager                   
                            </a>
                        </li>
                        <li>
                            <a href=""
                        >
                                Partner Manager                    
                            </a>
                        </li>
                        <li>
                            <a href=""
                        >
                                Announcements                   
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            
            <li>
                <a href="https://www.saedconnect.org/validation/manage-validation-program/">
                    Manage Validation Programs               
                </a>
            </li>
            
            <li>
                <a href="https://www.saedconnect.org/validation/manage-program-bundles/">
                    Manage Program Bundles               
                </a>
            </li>
            
            <?php } ?>
            
            <?php
            
                switch_to_blog(110);
    
                $meta_key = 'assigned_users';
            
                // Create Query Argument
                $args = array(
                    'post_type' => 'validation-program',
                    'showposts' => -1,
                );


                $validation_program_query = new WP_Query($args);

                while ($validation_program_query->have_posts()) : $validation_program_query->the_post();

                /* Get Post ID */
                $post_id = $post->ID;

                /* Get Saved Users */
                $saved_users = get_post_meta($post_id, $meta_key);
        
            ?>
            
                <?php if( in_array($current_user->ID, $saved_users) ){ ?>
            
                <li>
                    <a 
                        class="has-child"
                        href="#<?php echo 'validation-program-'.$post_id; ?>"
                        data-toggle="collapse"
                        aria-expanded="false"
                        aria-controls="<?php echo 'validation-program-'.$post_id; ?>"
                        role="button"
                    >
                        <?php the_title(); ?>                    
                    </a>
                    <?php
                        /* Validation Array */
                        $validation_types = array(
                            'business' => 'Business',
                            'jobs' => 'Job Offers',
                            'programs' => 'Programs',
                            'projects' => 'Projects',
                            'service-offers' => 'Service Offers',
                            'training-offers' => 'Training Offers',
                        );

                        /* Get Saved Validation types */
                        $saved_validation_types = get_post_meta($post_id, $validation_meta_key);

                        $saved_validation_types = $saved_validation_types['validation_types'];

                        if( $saved_validation_types ){

                    ?>
                    <div class="collapse" id="<?php echo 'validation-program-'.$post_id; ?>">
                        <ul class="sub-list">
                            <?php
                                foreach( 
                                    $saved_validation_types as $key => $saved_validation_type 
                                ){
                            ?>
                            <li>
                                <a href="<?php echo get_permalink().'?validate='.$saved_validation_type; ?>"
                            >
                                    <?php echo $validation_types[ $saved_validation_type ];  ?>                  
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                </li>
            
                <?php } ?>
            
            <?php
                endwhile;
            
                restore_current_blog();
            ?>
            
            <!--<li>
                <a href="#businesses">
                    My Community Interactions                       
                </a>
            </li>
            <li>
                <a 
                    class="has-child"
                    href="#nysc"
                    data-toggle="collapse"
                    aria-expanded="false"
                    aria-controls="nysc"
                    role="button"
                >
                    NYSC SAED                      
                </a>
                <div class="collapse" id="nysc">
                    <ul class="sub-list">
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                        >
                                Verify that you have served/are Serving                       
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                        >
                                My SAED Experience                 
                            </a>
                        </li>
                        <li>
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&amp;cv-view=personal-information"
                        >
                                SAED Certificate                    
                            </a>
                        </li>
                    </ul>
                </div>
            </li>-->
        </ul>
    </nav>
</div>