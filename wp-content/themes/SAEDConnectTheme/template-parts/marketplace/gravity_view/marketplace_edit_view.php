<?php 

    $permalink_esc;

    if( $_REQUEST['status'] == 'deleted' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/service-marketplace/service-category/sub-category-one/?view=jobs' );
?>
   
    <script>
        //window.location.replace( "<?php echo $permalink_esc; ?>" );
    </script>
    
<?php } ?>

<?php get_header() ?>

<style>
    .work-profile{
        display: none !important;
    }
    
    .gform_wrapper .top_label .gfield_label {
        font-size: 0.8rem !important;
        font-weight: 500 !important;
    }
    
    .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
        font-size: 0.9rem !important;
        width: 100% !important;
    }
    
    .gform_wrapper .gform_button {
        background-color: #b55085 !important;
        font-size: 0.7rem !important;
        width: auto !important;
    }
    
    .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
        display: none !important;
    }
</style>

<main class="main-content txt-color-light" style="margin-top: 100px"> 

<?php
    /* Reset Global Queried Object  */
    wp_reset_postdata();
    wp_reset_query();
   
    while ( have_posts() ) : the_post();
?>
   
    <div class="container-wrapper bg-white padding-t-80">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="header">
                    <h2 class="section-wrapper-title">
                        Edit <?php echo $_REQUEST['form-title']; ?>          
                    </h2>
                    <div class="text-right">
                        <a 
                            href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv" 
                            class="btn-trans-bw txt-xs n0-m-b"
                        >
                            Cancel
                        </a>
                    </div>
                </div>

                <div class="entry">
                   <?php the_content() ?>
                </div>
            </div>
        </div>
    </div>
    
<?php
    endwhile; // end of the loop.
?>

</main>

<?php wp_footer(); ?>
