<?php
    switch_to_blog(20);
?>


<!-- SHC Modal -->
<div class="modal fade font-main shc-modal" id="comingSoonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="container-wrapper">
                <div class="col-md-10 mx-auto">
                    <div class="padding-tb-80">
                        <div class="margin-b-40">
                            <h2 class="txt-2em txt-bold txt-height-1-2">
                                What Side Hustle Community
                                <br>
                                are you interested in?
                            </h2>
                        </div>
                        <ul class="row row-20 shc-list txt-normal-s txt-medium">
                        <?php
                            wp_reset_postdata();
                            wp_reset_query();

                            $temp = $wp_query; $wp_query= null;
                            $wp_query = new WP_Query();
                            $wp_query->query(array('post_type' => 'information-session'));
                                while ($wp_query->have_posts()) : $wp_query->the_post();                
                        ?>

                            <div class="col-md-4 padding-lr-20">
                                <li>
                                    <a href="<?php the_permalink($post->id); ?>">
                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                        <span class="txt-color-dark">
                                            <?php the_title() ?>
                                        </span>
                                    </a>
                                </li>
                            </div>

                        <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
    //Revert to Previous Multisite
    restore_current_blog();
?>