<?php

    /*
    *==================================================================================
    *==================================================================================
    *   Get REQUEST Variables
    *==================================================================================
    *==================================================================================
    */

        //Get from REQUEST (From Previous Page, This Page Header and This Page Menu)
        $industry_id = esc_html($_REQUEST['industry-id']); //Get Industry ID
        $industry_child = esc_html($_REQUEST['industry-child']); //Get Industry Child

?>
   

<main class="main-content">
    <header class="course-directory-banner image sm" style="background-image:url('http://www.saedconnect.org/learn-a-skill/wp-content/themes/SAEDConnectTheme/images/grid/image (41).jpg');">
        <div class="content container-wrapper txt-color-white">
            <h1 class="title">
                Explore different business ideas.
            </h1>
            <h2>
                xPlore offers information to better understand how to start specific businesses across all industries.
            </h2>
        </div>
    </header>

    <section class="container-wrapper bg-purple">
        <div class="border-color-darkgrey">
            <div class="padding-tb-40">
                <p class="txt-medium txt-color-white margin-b-20">
                    Filter Business ideas by industry
                </p>
                <ul class="tax-list white">
                    <li>
                        <a
                            href="<?php echo site_url() ?>/?industry-id=&search-type=xplore-business&s="
                            class="<?php echo ($industry_id == ''  ? 'selected' : '') ?>"
                        >
                            All
                        </a>
                    </li>
                <!-- Get All Industry Terms -->
                <?php 
                    //Switch to Learn a Skill Multisite (id = 8)
                    //switch_to_blog(8);

                    //Get Terms
                    $terms = get_terms( 'industry', array('hide_empty' => false,)); //Get all the terms

                    foreach ($terms as $term) { //Cycle through terms, one at a time

                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        if ( $parent=='0' ) {

                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                            $term_url = get_term_link($term);
                ?>

                    <li>
                        <a
                           href="<?php echo site_url() ?>/?industry-id=<?php echo $term_id ?>&search-type=xplore-business&s="
                           class="<?php echo ($industry_id == $term_id  ? 'selected' : '') ?>"
                        >
                            <?php echo $term_name; ?>   
                        </a>
                    </li>
                <?php
                        } 
                    }

                    //Revert to Previous Multisite
                    //restore_current_blog();
                ?>
                </ul>
            </div>
        </div>
    </section>
    <?php
        $tax_array = array(
            //Relation Type
            'relation' => 'AND',
            //Industry
            array (
                'taxonomy' => 'xplore-directory-type',
                'field' => 'slug',
                'terms' => 'business-idea',
            ),

        );
    
        /*
        *
        *==================================================================================
        * Industry Tax Query
        *==================================================================================
        *
        */
        if($industry_id){
            $category_array = array(

                //Industry
                array (
                    'taxonomy' => 'industry',
                    'field' => 'id',
                    'terms' => $industry_id,
                ),

            );

            $tax_array = array_merge($tax_array, $category_array); 
        }
    
        //Pass Search Results Parameters to args variable 
        $args = array(
            'post_type' => 'guide',
            'showposts' => -1,
            'tax_query' => $tax_array,
        );

        $temp = $wp_query; $wp_query= null;
        $wp_query = new WP_Query($args);
    ?>
    <section class="container-wrapper padding-tb-40">
        <h2 class="txt-normal-s margin-b-20">
            <?php echo $wp_query->post_count ?> Results
        </h2>
        <ul class="row row-40 directory-list">
        <?php // Display posts

            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                <li class="col-md-3 padding-lr-40">
                    <a href="<?php the_permalink() ?>">
                        <span class="title">
                            <?php the_title() ?>
                        </span> 
                    </a>
                </li>
        <?php
            endwhile;
        ?>
        </ul>
    </section>
</main> 