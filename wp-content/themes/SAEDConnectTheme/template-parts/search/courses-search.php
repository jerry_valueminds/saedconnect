<?php
    /*
    *==================================================================================
    *==================================================================================
    *   Get REQUEST Variables
    *==================================================================================
    *==================================================================================
    */
        //Declare Empty
        $industry_child = ''; //Set to empty. No Industry Child Selected by default
        $generic_counter = 0; //Use this Counter to hold aside post numbers

        //Get from REQUEST (From Previous Page, This Page Header and This Page Menu)
        $industry_id = esc_html($_REQUEST['industry-id']); //Get Industry ID
        $industry_child = esc_html($_REQUEST['industry-child']); //Get Industry Child
        $post_type = esc_html($_REQUEST['post-type']); //Get Post Type: course or training
        
        //Get from REQUEST (Aside Filter)
        $level = esc_html($_REQUEST['level']);
        $model = esc_html($_REQUEST['model']);
        $coverage_nigeria = esc_html($_REQUEST['coverage-nigeria']);

    /*
    *==================================================================================
    *==================================================================================
    *   WP Query
    *==================================================================================
    *==================================================================================
    */
        // Define Tax Query
        $tax_array = array('relation' => 'AND');
        $tax_array_count = array('relation' => 'AND');

        /*
        *
        *==================================================================================
        * Industry Tax Query
        *==================================================================================
        *
        */
        if($industry_id){
            $category_array = array(

                //Industry
                array (
                    'taxonomy' => 'industry',
                    'field' => 'id',
                    'terms' => $industry_id,
                ),

            );

            $tax_array = array_merge($tax_array, $category_array); 
            $tax_array_count = array_merge($tax_array_count, $category_array); 
        }

        /*
        *
        *==================================================================================
        * Sub Industry Tax Query
        *==================================================================================
        *
        */
        if($industry_child){
            $category_array = array(

                //Industry
                array (
                    'taxonomy' => 'industry',
                    'field' => 'id',
                    'terms' => $industry_child
                ),

            );

            $tax_array = array_merge($tax_array, $category_array); 
            $tax_array_count = array_merge($tax_array_count, $category_array); 
        }

        /*
        *
        *==================================================================================
        * Level Tax Query
        *==================================================================================
        *
        */
        if($level){
            $level_array = array(

                //Sub-Categoty
                array (
                    'taxonomy' => 'level',
                    'field' => 'id',
                    'terms' => $level,
                ),
            );

            $tax_array = array_merge($tax_array, $level_array);
        }

        /*
        *
        *==================================================================================
        * Model Tax Query
        *==================================================================================
        *
        */
        if($model){
            $model_array = array(

                //Sub-Categoty
                array (
                    'taxonomy' => 'model',
                    'field' => 'id',
                    'terms' => $model,
                ),
            );

            $tax_array = array_merge($tax_array, $model_array);
        }

        /*
        *
        *==================================================================================
        * Model Tax Query
        *==================================================================================
        *
        */
        if($coverage_nigeria){
            $coverage_nigeria_array = array(

                //Sub-Categoty
                array (
                    'taxonomy' => 'coverage-nigeria',
                    'field' => 'id',
                    'terms' => $coverage_nigeria,
                ),
            );

            $tax_array = array_merge($tax_array, $coverage_nigeria_array);
        }

        /*
        *==================================================================================
        *==================================================================================
        * WP Query
        *==================================================================================
        *==================================================================================
        */
        
        //Pass Search Results Parameters to args variable 
        $args = array(
            'post_type' => $post_type,
            'showposts' => -1,
            'tax_query' => $tax_array,
        );

        //Pass Search Count Parameters to args variable 
        $args_count = array(
            'post_type' => $post_type,
            'showposts' => -1,
            'tax_query' => $tax_array_count,
        );

        // Set up a Query for SAERCH RESULTS
        $posts_collection = new WP_Query($args);

        // Set up a Query for SAERCH RESULTS
        $posts_count = new WP_Query($args_count);
    ?>
    
    
    <main class="main-content">
        <header class="course-directory-banner">
            <div class="content container-wrapper txt-color-white">
                <h1 class="title">
                    <?php
                        $industry = get_term_by( 'id', $industry_id, 'industry' );
                        echo $industry->name;
                    ?>
                </h1>
                <ul class="tax-list">
                    <li>
                        <a
                            class="<?php echo (esc_html($_REQUEST['industry-child']) == ''  ? 'selected' : '') ?>"
                            href="
                               <?php echo site_url(); ?>/?industry-id=<?php echo $industry->term_id ?>&industry-child=&post-type=<?php echo $post_type ?>&s=
                            ">
                            All
                        </a>
                    </li>
                <?php
                    $industry_children = get_term_children( $industry->term_id, 'industry' );

                    foreach ( $industry_children as $child ) {
                        $term = get_term_by( 'id', $child, 'industry' );
                ?>
                        
                        <li>
                            <a
                                class="<?php echo (esc_html($_REQUEST['industry-child']) == $term->term_id  ? 'selected' : '') ?>"
                                href="
                                   <?php echo site_url(); ?>/?industry-id=<?php echo $industry->term_id ?>&industry-child=<?php echo $term->term_id ?>&post-type=<?php echo $post_type ?>&s=
                                ">
                                <?php echo $term->name ?>
                            </a>
                        </li>
                <?php
                    } 
                ?>
                </ul>
            </div>
        </header>
        <section class="bg-grey">
            <ul class="skills-nav-bar">
                <li class="<?php echo ($post_type == 'course'  ? 'active' : '') ?>">
                    <a
                        href="
                            <?php echo site_url(); ?>/?industry-id=<?php echo $industry->term_id ?>&industry-child=<?php echo $industry_child ?>&post-type=course&search-type=course&s=
                        ">
                        Explore Courses
                    </a>
                </li>
                <li class="<?php echo ($post_type == 'training'  ? 'active' : '') ?>">
                    <a
                        href="
                            <?php echo site_url(); ?>/?industry-id=<?php echo $industry->term_id ?>&industry-child=<?php echo $industry_child ?>&post-type=training&search-type=course&s=
                        ">
                        Explore Trainers
                    </a>
                </li>
            </ul>
        </section>
        <section class="container-wrapper padding-tb-40 margin-b-40">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 filter-box">
                    <h2 class="txt-sm margin-b-40">Filter Results</h2>
                    <form action="<?php echo home_url('/') ?>" method="GET">
                        <!-- Filter by Level -->
                        <div class="filter-group">
                            <button class="title dropdown-toggle" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                LEVEL
                            </button>
                            <div class="collapse show" id="collapseExample">
                                <ul class="filter-list">
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="level" value="0" checked>
                                            <label for="">All</label>
                                        </span>
                                        <span class="number-of">
                                        <?php 
                                            while ($posts_count->have_posts()) : $posts_count->the_post();
                                                if ( has_term( '', 'level' ) ) {
                                                    $generic_counter++;
                                                }
                                                
                                            endwhile;
                                           
                                            //Print Generic Counter
                                            echo '('.$generic_counter.')';
                                                
                                            //Reset Generic Counter
                                            $generic_counter = 0;
                                            
                                        ?>
                                        </span>
                                    </li>
                                    
                                    <?php 
                                        $terms = get_terms( 'level', array('hide_empty' => false,)); //Get all the terms

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;

                                            if ( $parent=='0' ) {

                                                $term_id = $term->term_id; //Get the term ID
                                                $term_name = $term->name; //Get the term name
                                                $term_post_count = $term->count; //Get the term post-count
                                    ?>

                                    <li>
                                        <span class="input-area">
                                            <input
                                                type="radio"
                                                name="level"
                                                value="<?php echo $term->term_id ?>"
                                                <?php echo ($term->term_id == esc_html($_REQUEST['level'])  ? 'checked' : '') ?>
                                            >
                                            <label for=""><?php echo $term_name?></label>
                                        </span>
                                        <span class="number-of">
                                        <?php 
                                            while ($posts_count->have_posts()) : $posts_count->the_post();
                                                if ( has_term( $term->slug, 'level' ) ) {
                                                    $generic_counter++;
                                                }
                                                
                                            endwhile;
                                           
                                            //Print Generic Counter
                                            echo '('.$generic_counter.')';
                                                
                                            //Reset Generic Counter
                                            $generic_counter = 0;
                                            
                                        ?>
                                        </span>
                                    </li>    

                                    <?php
                                            } 
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        
                        <!-- Filter by Model -->
                        <div class="filter-group">
                            <button class="title dropdown-toggle" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                                Model
                            </button>
                            <div class="collapse show" id="collapseExample2">
                                <ul class="filter-list">
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="model" value="0" checked>
                                            <label for="">All</label>
                                        </span>
                                        <span class="number-of">
                                        <?php 
                                            while ($posts_count->have_posts()) : $posts_count->the_post();
                                                if ( has_term( '', 'model' ) ) {
                                                    $generic_counter++;
                                                }
                                                
                                            endwhile;
                                           
                                            //Print Generic Counter
                                            echo '('.$generic_counter.')';
                                                
                                            //Reset Generic Counter
                                            $generic_counter = 0;
                                            
                                        ?>
                                        </span>
                                    </li>
                                    
                                    <?php 
                                        $terms = get_terms( 'model', array('hide_empty' => false,)); //Get all the terms

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;

                                            if ( $parent=='0' ) {

                                                $term_id = $term->term_id; //Get the term ID
                                                $term_name = $term->name; //Get the term name
                                                $term_post_count = $term->count; //Get the term post-count
                                    ?>

                                    <li>
                                        <span class="input-area">
                                            <input
                                                type="radio"
                                                name="model"
                                                value="<?php echo $term->term_id ?>"
                                                <?php echo ($term->term_id == esc_html($_REQUEST['model'])  ? 'checked' : '') ?>    
                                            >
                                            <label for=""><?php echo $term_name?></label>
                                        </span>
                                        <span class="number-of">
                                        <?php 
                                            while ($posts_count->have_posts()) : $posts_count->the_post();
                                                if ( has_term( $term->slug, 'model' ) ) {
                                                    $generic_counter++;
                                                }
                                                
                                            endwhile;
                                           
                                            //Print Generic Counter
                                            echo '('.$generic_counter.')';
                                                
                                            //Reset Generic Counter
                                            $generic_counter = 0;
                                            
                                        ?>
                                        </span>
                                    </li>  

                                    <?php
                                            } 
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        
                        <!-- Filter by Nigeria -->
                        <div class="filter-group">
                            <button class="title dropdown-toggle" type="button" data-toggle="collapse" data-target="#collapseState" aria-expanded="false" aria-controls="collapseState">
                                Location
                            </button>
                            <div class="collapse show" id="collapseState">
                                <ul class="filter-list">
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="coverage-nigeria" value="0" checked>
                                            <label for="">All</label>
                                        </span>
                                        <span class="number-of">
                                        <?php 
                                            while ($posts_count->have_posts()) : $posts_count->the_post();
                                                if ( has_term( '', 'coverage-nigeria' ) ) {
                                                    $generic_counter++;
                                                }
                                                
                                            endwhile;
                                           
                                            //Print Generic Counter
                                            echo '('.$generic_counter.')';
                                                
                                            //Reset Generic Counter
                                            $generic_counter = 0;
                                            
                                        ?>
                                        </span>
                                    </li>
                                    
                                    <?php 
                                        $terms = get_terms( 'coverage-nigeria', array('hide_empty' => false,)); //Get all the terms

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;

                                            if ( $parent=='0' ) {

                                                $term_id = $term->term_id; //Get the term ID
                                                $term_name = $term->name; //Get the term name
                                                $term_post_count = $term->count; //Get the term post-count
                                    ?>

                                    <li>
                                        <span class="input-area">
                                            <input
                                                type="radio"
                                                name="coverage-nigeria"
                                                value="<?php echo $term->term_id ?>"
                                                <?php echo ($term->term_id == esc_html($_REQUEST['coverage-nigeria'])  ? 'checked' : '') ?>    
                                            >
                                            <label for=""><?php echo $term_name?></label>
                                        </span>
                                        <span class="number-of">
                                        <?php 
                                            while ($posts_count->have_posts()) : $posts_count->the_post();
                                                if ( has_term( $term->slug, 'coverage-nigeria' ) ) {
                                                    $generic_counter++;
                                                }
                                                
                                            endwhile;
                                           
                                            //Print Generic Counter
                                            echo '('.$generic_counter.')';
                                                
                                            //Reset Generic Counter
                                            $generic_counter = 0;
                                            
                                        ?>
                                        </span>
                                    </li>  

                                    <?php
                                            } 
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        
                        <!-- Pass Default Values: Industry, Sub-Industry & Post Type (Course or Training) -->
                        <input type="text" value="<?php echo $industry_id ?>" name="industry-id" hidden>
                        <input type="text" value="<?php echo $industry_child ?>" name="industry-child" hidden>
                        <input type="text" value="<?php echo $post_type ?>" name="post-type" hidden>
                        
                        <!-- Pass Mandatory Seach Field (Required by Wordpress) -->
                        <input type="text" value="" name="s" id="s" hidden>
                        
                        <!-- Submit Request -->
                        <input class="btn btn-blue txt-sm full-width" type="submit" value="Filter" id="searchsubmit">
                    </form>
                </div>
                <div class="col-md-9 padding-lr-40">
                    <!-- Post Count -->
                    <p class="txt-sm margin-b-20">
                        <?php
                            if($posts_collection->post_count == 1){
                                echo $posts_collection->post_count.' result';
                            }else{
                                echo $posts_collection->post_count.' results';
                            }
                        ?>
                    </p>
                <?php while ($posts_collection->have_posts()) : $posts_collection->the_post(); ?>    
                    <?php if($post_type == 'course'){ ?>
                    <!-- Course -->
                    <article class="row row-20 course-directory-card-spread">
                        <div class="col-md-3 padding-lr-20 margin-b-20">
                            <figure class="image-box">
                                <?php if (has_post_thumbnail()) { ?>

                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>" alt="">

                                <?php } else { ?>

                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.jpg" alt="">

                                <?php } ?>

                                <?php if ( has_term( 'virtual-class', 'model' ) ) {?>
                                    <h4 class="model">Virtual Class</h4>
                                <?php } elseif ( has_term( 'blended', 'model' ) ) {?>
                                    <h4 class="model">Blended Class</h4>
                                <?php } elseif ( has_term( 'classroom', 'model' ) ) {?>
                                    <h4 class="model">Classroom</h4>
                                <?php } elseif ( has_term( 'fully-online', 'model' ) ) {?>
                                    <h4 class="model">Fully Online Class</h4>
                                <?php } ?>
                            </figure>
                        </div>
                        <div class="col-md-9 padding-lr-20 margin-b-20">
                            <div class="course-info">
                                <div class="footer">
                                    <div class="level">
                                        <?php if ( has_term( 'beginner', 'level' ) ) {?>
                                            Advanced
                                        <?php } elseif ( has_term( 'intermediate', 'level' ) ) {?>
                                            Intermediate
                                        <?php } elseif ( has_term( 'advanced', 'level' ) ) {?>
                                            Advanced
                                        <?php } ?>
                                    </div>
                                    <div class="duration">
                                        6 Hours
                                    </div>
                                </div>
                                <h3 class="title"><?php the_title() ?></h3>
                                <h4 class="instructor">
                                    <span class="const">BY:</span>
                                    <span class="name">Maero Uwede</span>
                                </h4>
                                <p class="txt-sm">
                                    <strong class="txt-bold">Skills:</strong>
                                    Skill 1, Skill 2, Skill 3,
                                </p>
                            </div>
                        </div>
                    </article>
                    <?php }elseif($post_type == 'training'){ ?>
                    <!-- Trainer -->
                    <article class="trainer-card row margin-b-10">
                        <div class="col-md-8 padding-b-40 padding-t-20 padding-lr-20 bg-grey">
                            <div class="title-bar">
                                <div class="">
                                    <h4 class="subtitle">
                                        <?php the_title() ?>
                                    </h4>
                                    <p class="txt-sm margin-b-20">ID: 84554545</p>
                                </div>
                                <figure class="verification">
                                    <span class="caption">NYSCSAED Trainer</span>
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/nysc_saed%20_logo.png" alt="">
                                </figure>
                            </div>

                            <article class="">
                                <p class="txt-sm margin-b-20">
                                    <strong class="txt-bold">Trains on: </strong>
                                    Human Resources, Project & Event Management, Business Analysis, Administration
                                </p>
                                <p class="txt-sm margin-b-15">
                                    <strong>
                                        <i class="fa fa-map-marker"></i>
                                    </strong>
                                    Lagos, Abuja, Kano
                                </p>
                            </article>
                            <div class="reamore">
                                <div class="collapse" id="collapseTrainer1">
                                    <article class="margin-b-20">
                                        <h4 class="txt-sm txt-bold margin-b-10">About</h4>
                                        <p class="txt-sm">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                        </p>
                                    </article>
                                    <article class="margin-b-20">
                                        <h4 class="txt-sm txt-bold margin-b-10">Addresses</h4>
                                        <ul class="address-list txt-sm">
                                            <li>
                                                Lagos Badagry Expressway, okokomaiko
                                            </li>
                                            <li>
                                                3,Ibrahim Taiwo Road, Nasarawa, Kano
                                            </li>
                                            <li>
                                                Suite 2,Asokoro Shopping Mall T.Y Danjuma Street, Abuja
                                            </li>
                                        </ul>
                                    </article>
                                    <article>
                                        <h4 class="txt-sm txt-bold margin-b-15">Social Media</h4>
                                        <ul class="social-section row row-10">
                                            <li class="padding-lr-10">
                                                <a href="">
                                                    <i class="fa fa-facebook-square"></i>
                                                    <span class="">facebook.com/saedconnect</span>
                                                </a>
                                            </li>
                                            <li class="padding-lr-10">
                                                <a href="">
                                                    <i class="fa fa-linkedin"></i>
                                                    <span class="">linkedin.com/saedconnect</span>
                                                </a>
                                            </li>
                                            <li class="padding-lr-10">
                                                <a href="">
                                                    <i class="fa fa-twitter"></i>
                                                    <span class="">@saedconnect</span>
                                                </a>
                                            </li>
                                            <li class="padding-lr-10">
                                                <a href="">
                                                <i class="fa fa-globe"></i>
                                                    <span class="">www.saedconnect.com</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </article>
                                </div>
                            </div>
                            <div class="txt-sm text-right floating-footer">
                                <p>
                                    <a data-toggle="collapse" href="#collapseTrainer1" role="button" aria-expanded="false" aria-controls="collapseTrainer1">
                                        Read More
                                    </a>
                                </p>
                                <p>
                                    <a href="" class="padding-r-15">0 reviews</a>
                                    <a href="" class="txt-underline padding-r-15">
                                        <i class="fa fa-plus"></i>
                                        <span>Write a review</span>
                                    </a>
                                    <a href="" class="txt-underline">
                                        <i class="fa fa-plus"></i>
                                        <span>Report Trainer</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="padding-o-20 bg-ghostwhite">
                                <div class="trainer-rating margin-b-20 txt-color-yellow">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <h3 class="txt-bold txt-sm margin-b-20">Course Types Offered</h3>
                                <ul class="txt-sm course-type-list">
                                    <li class="margin-b-10">
                                        <i class="fa fa-building"></i>
                                        Classroom
                                    </li>
                                    <li class="margin-b-10">
                                        <i class="fa fa-tv"></i>
                                        Online/Virtual
                                    </li>
                                    <li class="margin-b-10">
                                        <i class="fa fa-window-restore"></i>
                                        Blended
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </article>
                    <?php } ?>
                <?php endwhile;
                    // Reset things, for good measure
                    $services = null;
                    wp_reset_postdata();
                ?>
                </div>
            </div>
        </section>
        
    </main>