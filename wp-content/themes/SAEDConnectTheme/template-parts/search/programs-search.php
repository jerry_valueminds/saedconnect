<?php
/*
    *==================================================================================
    *==================================================================================
    *   Get REQUEST Variables
    *==================================================================================
    *==================================================================================
    */
        //Declare Empty
        $industry_child = ''; //Set to empty. No Industry Child Selected by default
        $generic_counter = 0; //Use this Counter to hold aside post numbers

        //Get from REQUEST (From Previous Page, Used for filter text)
        $industry_id = esc_html($_REQUEST['industry']); //Get Industry ID
        $program_type_id = esc_html($_REQUEST['program-type']); //Get Program Type ID
        $coverage_nigeria_id = esc_html($_REQUEST['coverage-nigeria']); //Get Coverage- Nigeria ID
        $model_id = esc_html($_REQUEST['model']); //Get Model ID
        $level_id = esc_html($_REQUEST['level']); //Get Levvel ID

        /*
        *==================================================================================
        *==================================================================================
        *   WP Query
        *==================================================================================
        *==================================================================================
        */
        // Define Tax Query
        $tax_array = array('relation' => 'AND');

        // Add Arguements from _REQUEST
        foreach($_GET as $key => $value){
             
            /* Check & Exclude:
                    - S ( WP Search field )
                    - Search-type ( For selecting Search Template View to use )
                    - Empty values
            */
            if($key !== 's' && $key !== 'search-type' && !empty($value)){
                
                // Create Tax Array entry
                $category_array = array(

                    // Add Term Term Values
                    array (
                        'taxonomy' => trim($key), //Texanomy Type
                        'field' => 'id', //Search field
                        'terms' => trim($value), //Term ID
                    ),

                );

                // Add just created Array to Tax Array. 
                $tax_array = array_merge($tax_array, $category_array); 
            }
        }

        // Create Query Argument
        $args = array(
            'post_type' => 'opportunity',
            'showposts' => -1,
            'tax_query' => $tax_array,
        );
?>    

    
    <main class="main-content">
        <!--<header class="overview-header container-wrapper">
            <h1 class="title">Programs</h1>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <li class="active">
                        <a href="<?php get_site_url();  ?>">
                            All Opportunities 
                        </a>
                    </li>
                    <li>
                        <a href="">Partner Opportunities</a>
                    </li>
                    <li>
                        <a href="">Around the Web</a>
                    </li>
                    <li>
                </ul>
            </nav>
        </header>-->
        
        <header class="overview-header container-wrapper">
            <h1 class="title">
                <span class="txt-bold txt-height-1-2 padding-b-20">
                    Opportunity Center
                </span>
            </h1>
        </header>

        <section class="padding-t-40 padding-b-10">
            <form id="filter-accordion" class="filter-accordion" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
                <!-- Mandatory WordPress Search Field -->
                <input type="search" class="search-field" value="" name="s" hidden>
                <!-- Search Template Ttype to use -->
                <input type="text" name="search-type" value="search-programs" hidden>
                
                <?php
                    /* Filter Options Array */
                    $filter_items_array = array(
                        array(
                            'term_slug' =>'program-type',
                            'term_name' => 'Program Type'
                        ),
                        
                        array(
                            'term_slug' =>'industry',
                            'term_name' => 'Industry'
                        ),
                        
                        array(
                            'term_slug' =>'coverage-nigeria',
                            'term_name' => 'Location'
                        ),
                        
                        array(
                            'term_slug' =>'model',
                            'term_name' => 'Model'
                        ),
                        
                        array(
                            'term_slug' =>'level',
                            'term_name' => 'Level'
                        ),
                    );
                ?>
                
                <!-- Accordion Header -->
                <div class="container-wrapper">
                    <div class="row row-5">
                    <?php foreach($filter_items_array as $filter_item){ ?>
                        <div class="padding-lr-5 padding-b-10">
                            <button
                                id="heading-<?php echo $filter_item['term_slug']; ?>"
                                class="btn btn-trans-green dropdown-toggle txt-normal-s"
                                type="button" data-toggle="collapse"
                                data-target="#filter-content-<?php echo $filter_item['term_slug']; ?>"
                                aria-expanded="false"
                                aria-controls="filter-content-<?php echo $filter_item['term_slug']; ?>"
                            >
                                <?php echo $filter_item['term_name']; ?>
                            </button>
                        </div>
                    <?php } ?>
                        <div class="padding-lr-5 padding-b-10">
                            <input class="btn btn-green txt-normal-s" type="submit" value="GO">
                        </div>
                    </div>
                </div>
                
                <!-- Accordion Content -->
                <div class="container-wrapper bg-grey">
                <?php foreach($filter_items_array as $filter_item){ ?>
                    <div
                        id="filter-content-<?php echo $filter_item['term_slug']; ?>"
                        class="collapse"
                        aria-labelledby="heading-<?php echo $filter_item['term_slug']; ?>"
                        data-parent="#filter-accordion"
                    >
                        <div class="custom-radio txt-sm padding-t-40 padding-b-30">
                        <?php 

                            //Get Terms
                            $terms = get_terms( $filter_item['term_slug'], array('hide_empty' => false,)); //Get all the terms

                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                // Check and see if the term is a top-level parent. If so, display it.
                                $parent = $term->parent;

                                $term_id = $term->term_id; //Get the term ID
                                $term_slug = $filter_item['term_slug'];
                                $term_name = $term->name; //Get the term name
                                $term_url = get_term_link($term);
                        ?>
                            
                                <label class="radio-item <?php echo ($parent == '0'  ? 'parent-term' : '') ?>">
                                    <input
                                       type="radio"
                                       name="<?php echo $filter_item['term_slug']; ?>"
                                       value="<?php echo $term_id ?>"
                                       
                                       <?php 
                                            if($term_id == esc_html($_REQUEST[$term_slug])){
                                                echo 'checked';
                                            }
                                           ?>
                                    >
                                    <span class="text">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>
                        <?php

                            }

                        ?>
                        </div>
                    </div>
                <?php } ?>
                </div>
                
            </form>
        </section>

        <section>            
            <div class="row">
            <?php wp_reset_postdata();
                wp_reset_query();
                $temp = $wp_query; $wp_query= null;
                $wp_query = new WP_Query($args);
                
                if ( $wp_query->have_posts() ) {
                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Variables */
                    $program_id = $post->ID;    //Get Program ID

                    /* Get Opportunity Banner */
                    $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                    $image = reset( $images );                
            ?>
                <!-- CTA 2 -->
                <div class="col-md-4 flip-card">
                    <div
                        class="feature-image-block front"
                        style="background-image:url('<?php if($image){ echo $image['full_url']; } ?>')"
                    >
                        
                        
                        <div class="content txt-color-white">
                            <div class="event-title margin-b-20">
                                <?php
                                    //Get Event Date
                                    //$event_date = rwmb_meta( 'event-date' );

                                    //Event Expiry Date Calculation
                                    //$start = new DateTime();
                                    //$start->setTimestamp($event_date);
                                    //$end   = new DateTime(); // Current date time
                                    //$diff  = $start->diff($end);

                                   // echo  $diff->days . ' days, ';

                                ?>
                                <?php if($event_date){ ?>
                                    <div class="event-date-wrapper">
                                        <div class="play-btn">
                                            <i>
                                                <div class="month uppercase">
                                                    <?php echo date( 'M', $event_date ); ?>
                                                </div>
                                                <div class="day">
                                                   <?php echo date( 'j', $event_date ); ?> 
                                                </div>
                                            </i>
                                        </div>
                                        <!--<div class="play-btn">
                                            <i>
                                                <div class="date-label">
                                                    Starts
                                                </div>
                                                <div class="month">
                                                    <?php
                                                        //echo date( 'M', $event_date )." ".date( 'j', $event_date );
                                                    ?>
                                                </div>
                                            </i>
                                        </div>-->
                                    </div>
                                <?php } ?>
                                <h3 class="title txt-xlg txt-medium txt-height-1-2">
                                    <?php echo rwmb_get_value( 'event-title' ); ?>
                                </h3>
                            </div>
                        </div>

                        <div class="caption txt-color-white">
                            <h3 class="title txt-xlg txt-medium txt-height-1-2 margin-b-5">
                                <?php echo rwmb_get_value( 'opportunity-title' ); ?>
                            </h3>
                            <h3 class="subtitle">
                                <?php echo rwmb_get_value( 'opportunity-author' ); ?>
                            </h3>
                        </div>
                    </div>
                    <div class="feature-image-block back">
                        <div class="content txt-color-white">
                            <p class="txt-height-1-5 margin-b-30">
                                <?php echo rwmb_get_value( 'opportunity-short-description' ); ?>
                            </p>
                            <article class="btn-wrapper">
                                <a
                                    class="btn btn-trans-wb"
                                    href="<?php the_permalink(); ?>"
                                    <?php
                                        if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                    ?>   
                                >
                                    Learn More
                                </a>
                               
                                <?php $values = rwmb_meta('event-btns');
                                    if($values){
                                        foreach ( $values as $value ) {
                                            if($value['footer-btn-type'] == 1){ ?>

                                                <a
                                                    class="btn btn-trans-wb icon"
                                                    href="<?php echo $value['footer-btn-url']; ?>"
                                                    <?php
                                                        if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                    ?>   
                                                >
                                                    <?php echo $value['footer-btn-text']; ?>
                                                </a>

                                            <?php }elseif($value['footer-btn-type'] == 2){ ?>
                                                <a
                                                    class="btn btn-trans-wb"
                                                    href="<?php echo $value['footer-btn-url']; ?>"
                                                    <?php
                                                        if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                    ?>    
                                                >
                                                    <?php echo $value['footer-btn-text']; ?>
                                                </a>
                                            <?php }else{ ?>
                                                <a
                                                    class="btn btn-white"
                                                    href="<?php echo $value['footer-btn-url']; ?>"
                                                    <?php
                                                        if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                    ?>
                                                >
                                                    <?php echo $value['footer-btn-text']; ?>
                                                </a>
                                            <?php }
                                        }
                                    }
                                ?>
                            </article>
                        </div>
                    </div>
                </div>
           
            <?php
                    endwhile;
                
                }else{
            ?>
                <div class="col-12 padding-tb-40 border-t-1 border-color-darkgrey">
                    <div class="container-wrapper">
                        <h2 class="txt-lg txt-medium">
                            No Opportunities found.
                        </h2>
                    </div>
                </div>   
                
            <?php
                    
                }
            ?>
			</div>
        </section>
        <!--<section class="container-wrapper">
            <div class="padding-t-20 padding-b-10 text-center">
                <a class="btn btn-trans-green" href="">
                    Show More
                </a>
            </div>
        </section>-->
    </main>
   
