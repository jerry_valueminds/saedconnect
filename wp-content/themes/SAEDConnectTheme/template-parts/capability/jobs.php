
<?php
    /* Publication key for getting Admin Approved content */
    $publication_key = 'publication_status';
?>

<!-- Job offers -->
<section class="padding-t-20 margin-b-40 bg-grey">
    <form id="filter-accordion" class="filter-accordion" action="">
        <input type="text" hidden name="view" value="<?php echo $view; ?>">
        <?php
            /* Filter Options Array */
            $filter_items_array = array(
                array(
                    'term_slug' =>'program-type',
                    'term_name' => 'Program Type'
                ),

                array(
                    'term_slug' =>'coverage-nigeria',
                    'term_name' => 'Location'
                ),
            );
        ?>

        <!-- Accordion Header -->
        <div class="container-wrapper txt-xs">
            <div class="row row-5">
            <?php
                if($_GET['term']){
            ?>
                <div class="padding-lr-5 padding-b-10">
                    <a href="https://www.saedconnect.org/opportunity-center/" class="btn btn-dark txt-normal-s">
                        <i class="fa fa-times"></i>
                        Clear Filter
                    </a>
                </div>
            <?php } ?>

            <?php foreach($filter_items_array as $filter_item){ ?>
                <div class="padding-lr-5 padding-b-10">
                    <button
                        id="heading-<?php echo $filter_item['term_slug']; ?>"
                        class="btn btn-trans-bw dropdown-toggle txt-normal-s
                            <?php
                                if($_GET['s-'.$filter_item['term_slug']] != ''){
                                    echo 'bg-darkgrey';
                                }
                            ?>
                        "
                        type="button" data-toggle="collapse"
                        data-target="#filter-content-<?php echo $filter_item['term_slug']; ?>"
                        aria-expanded="false"
                        aria-controls="filter-content-<?php echo $filter_item['term_slug']; ?>"
                    >
                        <?php echo $filter_item['term_name']; ?>
                    </button>
                </div>
            <?php } ?>
                <div class="padding-lr-5 padding-b-10">
                    <input class="btn btn-green txt-normal-s" type="submit" value="GO">
                </div>
            </div>
        </div>

        <!-- Accordion Content -->
        <div class="container-wrapper bg-grey">
        <?php foreach($filter_items_array as $filter_item){ ?>
            <div
                id="filter-content-<?php echo $filter_item['term_slug']; ?>"
                class="collapse"
                aria-labelledby="heading-<?php echo $filter_item['term_slug']; ?>"
                data-parent="#filter-accordion"
            >
                <div class="custom-radio txt-sm padding-t-40 padding-b-30">
                    <label class="radio-item parent=term">
                        <input
                            type="radio"
                            name="s-<?php echo $filter_item['term_slug']; ?>"
                            value=""
                            <?php 
                                if('' == esc_html($_REQUEST['s-'.$term_slug])){
                                    echo 'checked';
                                }
                            ?>
                        >
                        <span class="text">
                            All
                        </span>
                    </label>
                <?php 

                    //Get Terms
                    $terms = get_terms( $filter_item['term_slug'], array('hide_empty' => false, 'parent' => 0)); //Get all the terms

                    foreach ($terms as $term) { //Cycle through terms, one at a time

                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        $term_id = $term->term_id; //Get the term ID
                        $term_slug = $filter_item['term_slug'];
                        $term_name = $term->name; //Get the term name
                        $term_url = get_term_link($term);
                ?>

                        <label class="radio-item <?php echo ($parent == '0'  ? 'parent-term' : '') ?>">
                            <input
                                type="radio"
                                name="s-<?php echo $filter_item['term_slug']; ?>"
                                value="<?php echo $term_id ?>"
                                <?php 
                                    if($term_id == esc_html($_REQUEST['s-'.$term_slug])){
                                        echo 'checked';
                                    }
                                ?>
                            >
                            <span class="text">
                                <?php echo $term_name; ?>
                            </span>
                        </label>
                <?php

                    }

                ?>
                </div>
            </div>
        <?php } ?>
        </div>

    </form>
</section>

<section class="container-wrapper">
    <header class="row align-items-center border-b-1 border-color-grey padding-b-20 margin-b-20">
        <div class="col-md-8">
            <h2 class="txt-xlg txt-medium">
                Job Offers
            </h2>
        </div>
        <div class="col-md-4 text-md-right">
            <a
                href="https://www.saedconnect.org/service-marketplace/marketplace-form/?gf-id=5&form-title=Post a Job" 
                class="btn btn-trans-bw txt-xs no-m-b"
            >
                Post a Job
            </a>
        </div>
    </header>
    <div class="padding-b-40">
        <ul class="row row-10 marketplace-job-cards">
        <?php
            /*
            *==================================================================================
            *==================================================================================
            *   WP Query
            *==================================================================================
            *==================================================================================
            */
            // Define Meta Args
            /*$meta_array = array(
                'relation' => 'AND',
                array (
                    'key' => 'request-what-need', //Meta Key
                    'value' => 'A skilled person for a Job', //Meta Value
                ),
            );

            // Add Arguements from _REQUEST
            foreach($_GET as $key => $value){

                /* Check & Exclude:
                        - S ( WP Search field )
                        - Search-type ( For selecting Search Template View to use )
                        - Empty values

                if($key !== 's' && $key !== 'search-type' && $key !== 'view' && !empty($value) && $value != 'all'){

                    // Create Tax Array entry
                    $category_array = array(

                        // Add Term Term Values
                        array (
                            'key' => trim($key), //Texanomy Type
                            'value' => $value, //Search field
                        ),

                    );

                    // Add just created Array to Tax Array. 
                    $meta_array = array_merge($meta_array, $category_array); 
                }
            }*/

            // Create Query Argument
            $args = array(
                'post_type' => 'job',
                'showposts' => -1,
                'meta_query' => array(
                    array(
                        'key' => $publication_key,
                        'value' => 'admin_published'
                    )
                ),
            );


            $wp_query = new WP_Query($args);

            while ($wp_query->have_posts()) : $wp_query->the_post();

            /* Get Post ID */
            $post_id = $post->ID 
        ?>
            <li class="col-sm-6 col-lg-3 padding-o-10 d-flex">
                <a href="<?php the_permalink() ?>" class="flex_1">
                    <div class="content">
                        <div class="title">
                            <?php the_title(); ?>
                        </div>
                        <div class="employer">
                            <?php
                                $field = 'job_post_organization_name';

                                $meta = get_post_meta($post->ID, $field, true);

                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </div>
                        <div class="location">
                            <?php
                                $field = 'job_post_locations';

                                $meta = get_post_meta($post->ID, $field, true);

                                if($meta){
                                    echo $meta;
                                }
                            ?> | Full Time
                        </div>
                        <div class="post-time">
                            Posted 2 days ago
                        </div>
                    </div>
                </a> 
            </li>

        <?php
            endwhile;
        ?>
        </ul>
    </div>
</section>
<!--<div class="padding-t-40 padding-b-80">
    <a href="" class="txt-normal-s txt-underline txt-color-dark">
        <i class="fa fa-plus"></i>
        Show more
    </a>
</div>-->