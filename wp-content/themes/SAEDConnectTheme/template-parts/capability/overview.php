<?php
    /* View URL */
    global $wp;
    $page_url = home_url( $wp->request );
?>
  
<?php
    /* Publication key for getting Admin Approved content */
    $publication_key = 'publication_status';
?>
   
<section class="container-wrapper margin-t-40">
    <!-- Service offers -->
    <header class="row align-items-center border-b-1 border-color-grey padding-b-20 margin-b-20">
        <div class="col-md-8">
            <h2 class="txt-xlg txt-medium">
                Service Offers
            </h2>
        </div>
        <div class="col-md-4 text-md-right">
            <a href="https://www.saedconnect.org/service-marketplace/marketplace-form/?gf-id=1&form-title=Offer%20a%20Service" class="btn btn-trans-bw txt-xs no-m-b">
                Offer a Service
            </a>
        </div>
    </header>
    <ul class="row row-10 marketplace-service-cards">
    <?php
        /*
        *==================================================================================
        *==================================================================================
        *   WP Query
        *==================================================================================
        *==================================================================================
        */
        // Define Tax Query
        $meta_array = array('relation' => 'AND');

        // Add Arguements from _REQUEST
        foreach($_GET as $key => $value){

            /* Check & Exclude:
                    - S ( WP Search field )
                    - Search-type ( For selecting Search Template View to use )
                    - Empty values
            */
            if($key !== 's' && $key !== 'search-type' && $key !== 'view' && !empty($value) && $value != 'all'){

                // Create Tax Array entry
                $category_array = array(

                    // Add Term Term Values
                    array (
                        'key' => trim($key), //Texanomy Type
                        'value' => $value, //Search field
                    ),

                );

                // Add just created Array to Tax Array. 
                $meta_array = array_merge($meta_array, $category_array); 
            }
        }

        // Create Query Argument
        $args = array(
            'post_type' => 'offer-a-service',
            'showposts' => 8,
            'meta_query' => array(
                array(
                    'key' => $publication_key,
                    'value' => 'admin_published'
                )
            ),
        );


        $wp_query = new WP_Query($args);

        while ($wp_query->have_posts()) : $wp_query->the_post();

        /* Get Post ID */
        $post_id = $post->ID;
        
        $gf_id = 1;
        $search_criteria = array(

            'field_filters' => array( //which fields to search

                array(

                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                    'key' => '23', 'value' => $post_id, //Current logged in user
                )
            )
        );

        /* Get GF Entry Count */
        $offer_entries = GFAPI::get_entries( $gf_id, $search_criteria );
        $offer_entry = $offer_entries[0];
    ?>

        <li class="col-sm-6 col-lg-3 padding-o-10">
            <a href="<?php the_permalink() ?>">
            <?php
                /* Image */
                $meta = rgar( $offer_entry, '5' );
                        
                if($meta){
                    $meta = str_ireplace( 'http:', 'https:', $meta );
            ?>

                <figure class="service-img" style="background-image:url('<?php echo $meta; ?>');">

                </figure>
            <?php } else{ ?>
                <figure class="service-img" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/table.jpg');">

                </figure>
            <?php } ?>
                <div class="content">
                    <div class="top">
                        <div class="avatar">
                            <?php

                            ?>
                            <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($post->post_author, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $post->post_author, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                /* Get GF Entry Count */
                                $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                                if($entry_count){ //If no entry
                                    foreach( $entries as $entry ){          
                                        $displayname = rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' );
                                    }                
                                }   

                                restore_current_blog();
                            ?>
                            <figure style="background-image:url('<?php echo $avatar_url; ?>');">

                            </figure>
                            <div class="name" style="text-transform:capitalize">
                                <?php echo ($post->post_author == 1)? 'SAEDConnect Admin' : $displayname; ?>
                            </div>
                        </div>
                        <div class="title">
                            <?php the_title(); ?>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="location">
                            Abuja, Lagos
                        </div>
                        <?php
                            $meta = rgar( $offer_entry, '10' );
                        
                            if($meta){
                        ?>
                        <div class="post-fee">
                            Starting at
                            <span class="txt-color-dark txt-bold">
                                ₦<?php echo $meta; ?>
                            </span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </a>   
        </li>
    <?php
        endwhile;
    ?>
    </ul>
    <div class="padding-t-40 padding-b-80">
        <a 
            href="<?php echo $page_url ?>/?view=offers" 
            class="txt-normal-s txt-underline txt-color-dark"
        >
            <i class="fa fa-plus"></i>
            Show more
        </a>
    </div>

    <!-- Freelancers -->
    <header class="row align-items-center border-b-1 border-color-grey padding-b-20 margin-b-20">
        <div class="col-md-8">
            <h2 class="txt-xlg txt-medium">
                Freelancer Directory
            </h2>
        </div>
        <div class="col-md-4 text-md-right">
            <a href="https://www.saedconnect.org/service-marketplace/freelancer-profile-form/" class="btn btn-trans-bw txt-xs no-m-b">
                Join
            </a>
        </div>
    </header>
    <ul class="row row-10 marketplace-freelancer-cards padding-b-40">
    <?php
        /*
        *==================================================================================
        *==================================================================================
        *   WP Query
        *==================================================================================
        *==================================================================================
        */
        // Define Tax Query
        $meta_array = array('relation' => 'AND');

        // Add Arguements from _REQUEST
        foreach($_GET as $key => $value){

            /* Check & Exclude:
                    - S ( WP Search field )
                    - Search-type ( For selecting Search Template View to use )
                    - Empty values
            */
            if($key !== 's' && $key !== 'search-type' && $key !== 'view' && !empty($value) && $value != 'all'){

                // Create Tax Array entry
                $category_array = array(

                    // Add Term Term Values
                    array (
                        'key' => trim($key), //Texanomy Type
                        'value' => $value, //Search field
                    ),

                );

                // Add just created Array to Tax Array. 
                $meta_array = array_merge($meta_array, $category_array); 
            }
        }

        // Create Query Argument
        $args = array(
            'post_type' => 'freelancer',
            'showposts' => -1,
            'meta_query' => $meta_array,
        );


        $wp_query = new WP_Query($args);

        while ($wp_query->have_posts()) : $wp_query->the_post();

        /* Get Post ID */
        $post_id = $post->ID 
    ?>
        <li class="col-sm-6 col-lg-3 padding-o-10">
            <a href="<?php the_permalink() ?>">
                <span class="wrapper">
                    <span class="image">
                        <?php
                            /* Get Avatar */
                            $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                            $meta_key = 'user_avatar_url';
                            $get_avatar_url = get_user_meta($post->post_author, $meta_key, true);

                            if($get_avatar_url){
                                $avatar_url = $get_avatar_url;
                            }
                        ?>
                            
                        <figure class="freelancer-img" style="background-image:url('<?php echo $avatar_url; ?>');">

                        </figure>                        
                    </span>
                    <div class="content">
                        <div class="name">
                            <?php the_title(); ?>
                        </div>
                        <div class="bottom">
                            <div class="location">
                                <i class="fa fa-map-marker"></i>
                                <?php
                                    $field = 'freelancer_locations';

                                    $meta = get_post_meta($post->ID, $field, false);
                                
                                    if($meta){
                                        foreach($meta as $key=>$value) {
                                            echo $value;
                                            if($key <= (count($meta) - 1 )){
                                                echo ', ';
                                            }
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </span>
                <div class="services">
                    Web developement, Digital Marketing, Content Writing
                </div>
            </a> 
        </li>
    <?php
        endwhile;
    ?>
    </ul>
    <div class="padding-t-40 padding-b-80">
        <a 
            href="<?php echo $page_url ?>/?view=freelancers" 
            class="txt-normal-s txt-underline txt-color-dark"
        >
            <i class="fa fa-plus"></i>
            Show more
        </a>
    </div>

    <!-- Job offers -->
    <header class="row align-items-center border-b-1 border-color-grey padding-b-20 margin-b-20">
        <div class="col-md-8">
            <h2 class="txt-xlg txt-medium">
                Job Offers
            </h2>
        </div>
        <div class="col-md-4 text-md-right">
            <a href="https://www.saedconnect.org/service-marketplace/marketplace-form/?gf-id=5&form-title=Post%20a%20Job" class="btn btn-trans-bw txt-xs no-m-b">
                Post a Job
            </a>
        </div>
    </header>
    <ul class="row row-10 marketplace-job-cards">
    <?php
        /*
        *==================================================================================
        *==================================================================================
        *   WP Query
        *==================================================================================
        *==================================================================================
        */
        // Define Meta Args
        $meta_array = array(
            'relation' => 'AND',
            array (
                'key' => 'request-what-need', //Meta Key
                'value' => 'A skilled person for a Job', //Meta Value
            ),
        );

        // Add Arguements from _REQUEST
        foreach($_GET as $key => $value){

            /* Check & Exclude:
                    - S ( WP Search field )
                    - Search-type ( For selecting Search Template View to use )
                    - Empty values
            */
            if($key !== 's' && $key !== 'search-type' && $key !== 'view' && !empty($value) && $value != 'all'){

                // Create Tax Array entry
                $category_array = array(

                    // Add Term Term Values
                    array (
                        'key' => trim($key), //Texanomy Type
                        'value' => $value, //Search field
                    ),

                );

                // Add just created Array to Tax Array. 
                $meta_array = array_merge($meta_array, $category_array); 
            }
        }

        // Create Query Argument
        $args = array(
            'post_type' => 'job',
            'showposts' => -1,
            'meta_query' => array(
                array(
                    'key' => $publication_key,
                    'value' => 'admin_published'
                )
            ),
        );


        $wp_query = new WP_Query($args);

        while ($wp_query->have_posts()) : $wp_query->the_post();

        /* Get Post ID */
        $post_id = $post->ID 
    ?>
        <li class="col-sm-6 col-lg-3 padding-o-10">
            <a href="<?php the_permalink() ?>" class="d-flex">
                <div class="content">
                    <div class="title">
                        <?php the_title(); ?>
                    </div>
                    <div class="employer">
                        Valueminds
                    </div>
                    <div class="location">
                        Abuja | Full Time
                    </div>
                    <div class="post-time">
                        Posted 2 days ago
                    </div>
                </div>
            </a> 
        </li>

    <?php
        endwhile;
    ?>
    </ul>
    <div class="padding-t-40 padding-b-80">
        <a 
            href="<?php echo $page_url ?>/?view=jobs" 
            class="txt-normal-s txt-underline txt-color-dark"
        >
            <i class="fa fa-plus"></i>
            Show more
        </a>
    </div>

    <!-- Tasks -->
    <header class="row align-items-center border-b-1 border-color-grey padding-b-20 margin-b-20">
        <div class="col-md-8">
            <h2 class="txt-xlg txt-medium">
                Task Requests
            </h2>
        </div>
        <div class="col-md-4 text-md-right">
            <a href="https://www.saedconnect.org/service-marketplace/post-a-task-request/" class="btn btn-trans-bw txt-xs no-m-b">
                Make a Request
            </a>
        </div>
    </header>
    <ul class="row row-10 marketplace-job-cards">
    <?php
        /*
        *==================================================================================
        *==================================================================================
        *   WP Query
        *==================================================================================
        *==================================================================================
        */
        // Define Meta Args
        $meta_array = array(
            'relation' => 'AND',
            array (
                'key' => 'request-what-need', //Meta Key
                'value' => 'Service', //Meta Value
            ),
        );

        // Add Arguements from _REQUEST
        foreach($_GET as $key => $value){

            /* Check & Exclude:
                    - S ( WP Search field )
                    - Search-type ( For selecting Search Template View to use )
                    - Empty values
            */
            if($key !== 's' && $key !== 'search-type' && $key !== 'view' && !empty($value) && $value != 'all'){

                // Create Tax Array entry
                $category_array = array(

                    // Add Term Term Values
                    array (
                        'key' => trim($key), //Texanomy Type
                        'value' => $value, //Search field
                    ),

                );

                // Add just created Array to Tax Array. 
                $meta_array = array_merge($meta_array, $category_array); 
            }
        }

        // Create Query Argument
        $args = array(
            'post_type' => 'request-a-need',
            'showposts' => -1,
            //'meta_query' => $meta_array,
        );


        $wp_query = new WP_Query($args);

        while ($wp_query->have_posts()) : $wp_query->the_post();

        /* Get Post ID */
        $post_id = $post->ID 
    ?>

        <li class="col-sm-6 col-lg-3 padding-o-10">
            <a href="<?php the_permalink() ?>" class="d-flex">
                <div class="content">
                    <div class="title">
                        <?php the_title(); ?>
                    </div>
                    <div class="location">
                        Abuja | Full Time
                    </div>
                    <div class="post-time">
                        Posted 2 days ago
                    </div>
                </div>
            </a> 
        </li>

    <?php
        endwhile;
    ?>
    </ul>
    <div class="padding-t-40 padding-b-80">
        <a 
            href="<?php echo $page_url ?>/?view=requests" 
            class="txt-normal-s txt-underline txt-color-dark"
        >
            <i class="fa fa-plus"></i>
            Show more
        </a>
    </div>
</section>