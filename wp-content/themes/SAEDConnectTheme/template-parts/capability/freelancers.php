<?php
    $view = $_GET['view'];
?>

<?php
    /* Publication key for getting Admin Approved content */
    $publication_key = 'publication_status';
?>

<!-- Freelancers -->
<section class="padding-t-20 margin-b-40 bg-grey">
    <form id="filter-accordion" class="filter-accordion" action="">
        <input type="text" hidden name="view" value="<?php echo $view; ?>">
        <?php
            /* Filter Options Array */
            $filter_items_array = array(
                /*array(
                    'term_slug' =>'program-type',
                    'term_name' => 'Program Type'
                ),

                array(
                    'term_slug' =>'coverage-nigeria',
                    'term_name' => 'Location'
                ),*/
            );
        ?>

        <!-- Accordion Header -->
        <div class="container-wrapper txt-xs">
            <div class="row row-5">
            <?php
                if($_GET['term']){
            ?>
                <div class="padding-lr-5 padding-b-10">
                    <a href="https://www.saedconnect.org/opportunity-center/" class="btn btn-dark txt-normal-s">
                        <i class="fa fa-times"></i>
                        Clear Filter
                    </a>
                </div>
            <?php } ?>

            <?php foreach($filter_items_array as $filter_item){ ?>
                <div class="padding-lr-5 padding-b-10">
                    <button
                        id="heading-<?php echo $filter_item['term_slug']; ?>"
                        class="btn btn-trans-bw dropdown-toggle txt-normal-s
                            <?php
                                if($_GET['s-'.$filter_item['term_slug']] != ''){
                                    echo 'bg-darkgrey';
                                }
                            ?>
                        "
                        type="button" data-toggle="collapse"
                        data-target="#filter-content-<?php echo $filter_item['term_slug']; ?>"
                        aria-expanded="false"
                        aria-controls="filter-content-<?php echo $filter_item['term_slug']; ?>"
                    >
                        <?php echo $filter_item['term_name']; ?>
                    </button>
                </div>
            <?php } ?>
                <div class="padding-lr-5 padding-b-10">
                    <button
                        id="heading-availability"
                        class="btn btn-trans-bw dropdown-toggle txt-normal-s
                            <?php
                                if($_GET['s-availability'] != ''){
                                    echo 'bg-darkgrey';
                                }
                            ?>
                        "
                        type="button" data-toggle="collapse"
                        data-target="#filter-content-availability"
                        aria-expanded="false"
                        aria-controls="filter-content-availability"
                    >
                        Availability
                    </button>
                </div>
                <div class="padding-lr-5 padding-b-10">
                    <input class="btn btn-green txt-normal-s" type="submit" value="GO">
                </div>
            </div>
        </div>

        <!-- Accordion Content -->
        <div class="container-wrapper bg-grey">
        <?php foreach($filter_items_array as $filter_item){ ?>
            <div
                id="filter-content-<?php echo $filter_item['term_slug']; ?>"
                class="collapse"
                aria-labelledby="heading-<?php echo $filter_item['term_slug']; ?>"
                data-parent="#filter-accordion"
            >
                <div class="custom-radio txt-sm padding-t-40 padding-b-30">
                    <label class="radio-item parent=term">
                        <input
                            type="radio"
                            name="s-<?php echo $filter_item['term_slug']; ?>"
                            value=""
                            <?php 
                                if('' == esc_html($_REQUEST['s-'.$term_slug])){
                                    echo 'checked';
                                }
                            ?>
                        >
                        <span class="text">
                            All
                        </span>
                    </label>
                <?php 

                    //Get Terms
                    $terms = get_terms( $filter_item['term_slug'], array('hide_empty' => false, 'parent' => 0)); //Get all the terms

                    foreach ($terms as $term) { //Cycle through terms, one at a time

                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        $term_id = $term->term_id; //Get the term ID
                        $term_slug = $filter_item['term_slug'];
                        $term_name = $term->name; //Get the term name
                        $term_url = get_term_link($term);
                ?>

                        <label class="radio-item <?php echo ($parent == '0'  ? 'parent-term' : '') ?>">
                            <input
                                type="radio"
                                name="s-<?php echo $filter_item['term_slug']; ?>"
                                value="<?php echo $term_id ?>"
                                <?php 
                                    if($term_id == esc_html($_REQUEST['s-'.$term_slug])){
                                        echo 'checked';
                                    }
                                ?>
                            >
                            <span class="text">
                                <?php echo $term_name; ?>
                            </span>
                        </label>
                <?php

                    }

                ?>
                </div>
            </div>
        <?php } ?>
            <div
                id="filter-content-availability"
                class="collapse"
                aria-labelledby="heading-availability"
                data-parent="#filter-accordion"
            >
                <div class="custom-radio txt-sm padding-t-20 padding-b-30">
                    <label class="radio-item parent=term">
                        <input
                            type="radio"
                            name="freelancer_availability"
                            value=""
                            <?php 
                                if('' == esc_html($_REQUEST['freelancer_availability'])){
                                    echo 'checked';
                                }
                            ?>
                        >
                        <span class="text">
                            All
                        </span>
                    </label>
                    
                    <?php
                        $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                    
                        for( $i = 0; $i < count($days); $i++ ){
                    ?>

                    <label class="radio-item">
                        <input
                            type="radio"
                            name="freelancer_availability"
                            value="<?php echo $days[$i] ?>"
                            <?php 
                                if($days[$i] == esc_html($_REQUEST['freelancer_availability'])){
                                    echo 'checked';
                                }
                            ?>
                        >
                        <span class="text">
                            <?php echo $days[$i] ?>
                        </span>
                    </label>
                    
                    <?php } ?>

                </div>
            </div>
        </div>

    </form>
</section>
   
<section class="container-wrapper">
    <header class="row align-items-center border-b-1 border-color-grey padding-b-20 margin-b-20">
        <div class="col-md-8">
            <h2 class="txt-xlg txt-medium">
                Freelancer Directory
            </h2>
        </div>
        <div class="col-md-4 text-md-right">
            <a href="https://www.saedconnect.org/service-marketplace/freelancer-profile-form/" class="btn btn-trans-bw txt-xs no-m-b">
                Join
            </a>
        </div>
    </header>
    <ul class="row row-10 marketplace-freelancer-cards padding-b-40">
    <?php
        /*
        *==================================================================================
        *==================================================================================
        *   WP Query
        *==================================================================================
        *==================================================================================
        */
        // Define Tax Query
        $meta_array = array('relation' => 'AND');

        // Add Arguements from _REQUEST
        foreach($_GET as $key => $value){

            /* Check & Exclude:
                    - S ( WP Search field )
                    - Search-type ( For selecting Search Template View to use )
                    - Empty values
            */
            if($key !== 's' && $key !== 'search-type' && $key !== 'view' && !empty($value) && $value != 'all'){

                // Create Tax Array entry
                $category_array = array(

                    // Add Term Term Values
                    array (
                        'key' => trim($key), //Texanomy Type
                        'value' => $value, //Search field
                    ),

                );

                // Add just created Array to Tax Array. 
                $meta_array = array_merge($meta_array, $category_array); 
            }
        }

        // Create Query Argument
        $args = array(
            'post_type' => 'freelancer',
            'showposts' => -1,
            'meta_query' => $meta_array,
        );


        $wp_query = new WP_Query($args);

        while ($wp_query->have_posts()) : $wp_query->the_post();

        /* Get Post ID */
        $post_id = $post->ID 
    ?>
        <li class="col-sm-6 col-lg-3 padding-o-10">
            <a href="<?php the_permalink() ?>">
                <span class="wrapper">
                    <span class="image">
                        <?php
                            /* Get Avatar */
                            $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                            $meta_key = 'user_avatar_url';
                            $get_avatar_url = get_user_meta($post->post_author, $meta_key, true);

                            if($get_avatar_url){
                                $avatar_url = $get_avatar_url;
                            }
                        ?>
                            
                        <figure class="freelancer-img" style="background-image:url('<?php echo $avatar_url; ?>');">

                        </figure>                        
                    </span>
                    <div class="content">
                        <div class="name">
                            <?php the_title(); ?>
                        </div>
                        <div class="bottom">
                            <div class="location">
                                <i class="fa fa-map-marker"></i>
                                <?php
                                    $field = 'freelancer_locations';

                                    $meta = get_post_meta($post->ID, $field, false);
                                
                                    if($meta){
                                        foreach($meta as $key=>$value) {
                                            echo $value;
                                            if($key <= (count($meta) - 1 )){
                                                echo ', ';
                                            }
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </span>
                <div class="services">
                    Web developement, Digital Marketing, Content Writing
                </div>
            </a> 
        </li>
    <?php
        endwhile;
    ?>
    </ul>
</section>
<!--<div class="padding-t-40 padding-b-80">
    <a href="" class="txt-normal-s txt-underline txt-color-dark">
        <i class="fa fa-plus"></i>
        Show more
    </a>
</div>-->