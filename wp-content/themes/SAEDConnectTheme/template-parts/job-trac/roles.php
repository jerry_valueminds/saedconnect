
<!-- Step 3 -->
<section class="padding-t-80 padding-b-40">
    <div class="container-wrapper">
        <div class="row">
            <div class="col-md-6">
                <h2 class="txt-2em txt-bold txt-height-1-1 margin-b-20">
                    Available Roles
                </h2>
                <p class="margin-b-40">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis quasi dolor alias recusandae accusamus voluptates.
                </p>
            </div>
        </div>
        <div class="row row-15">

            <div class="col-md-3 padding-lr-15 d-flex padding-b-20">
                <div class="flex full-width bg-white border-o-1 border-color-darkgrey">
                    <h3 class="txt-xlg txt-bold padding-lr-20 padding-t-20">
                        Social Media Manager
                    </h3>
                    <article class="padding-o-20">
                        <p class="txt-normal-s txt-height-1-7 padding-tb-10 border-tb-1 border-color-darkgrey">

                            <span class="txt-bold txt-color-blue">
                                Status:
                            </span>
                            <span class="">
                                Recruiting
                            </span>
                        </p>
                        <p class="txt-sm txt-height-1-5 padding-tb-10 border-b-0 border-color-darkgrey">
                            Candidate is required to complete 5 Tasks to become eligible
                        </p>
                    </article>
                    <article class="padding-lr-20 padding-b-20">
                        <a href="job_track_role.html" class="btn btn-blue txt-normal-s no-m-b">
                            View Tasks
                        </a>
                    </article>
                </div>
            </div>
            <div class="col-md-3 padding-lr-15 d-flex padding-b-20">
                <div class="flex full-width bg-white border-o-1 border-color-darkgrey">
                    <h3 class="txt-xlg txt-bold padding-lr-20 padding-t-20">
                        Research / Business Analyst
                    </h3>
                    <article class="padding-o-20">
                        <p class="txt-normal-s txt-height-1-7 padding-tb-10 border-tb-1 border-color-darkgrey">

                            <span class="txt-bold txt-color-blue">
                                Status:
                            </span>
                            <span class="">
                                Recruiting
                            </span>
                        </p>
                        <p class="txt-sm txt-height-1-5 padding-tb-10 border-b-0 border-color-darkgrey">
                            Candidate is required to complete 5 Tasks to become eligible
                        </p>
                    </article>
                    <article class="padding-lr-20 padding-b-20">
                        <a href="job_track_role.html" class="btn btn-blue txt-normal-s no-m-b">
                            View Tasks
                        </a>
                    </article>
                </div>
            </div>
            <div class="col-md-3 padding-lr-15 d-flex padding-b-20">
                <div class="flex full-width bg-white border-o-1 border-color-darkgrey">
                    <h3 class="txt-xlg txt-bold padding-lr-20 padding-t-20">
                        Sales / Marketing Officer
                    </h3>
                    <article class="padding-o-20">
                        <p class="txt-normal-s txt-height-1-7 padding-tb-10 border-tb-1 border-color-darkgrey">

                            <span class="txt-bold txt-color-blue">
                                Status:
                            </span>
                            <span class="">
                                Recruiting
                            </span>
                        </p>
                        <p class="txt-sm txt-height-1-5 padding-tb-10 border-b-0 border-color-darkgrey">
                            Candidate is required to complete 5 Tasks to become eligible
                        </p>
                    </article>
                    <article class="padding-lr-20 padding-b-20">
                        <a href="job_track_role.html" class="btn btn-blue txt-normal-s no-m-b">
                            View Tasks
                        </a>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-ocean-6 padding-t-80 padding-b-60">
    <div class="container-wrapper txt-color-white">
        <div class="row">
            <div class="col-md-6 padding-lr-20">
                <h1 class="txt-medium txt-2em margin-b-30">
                    Join the JobTrac Program
                </h1>
                <article class="description">
                    <p class="txt-normal-s">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus repudiandae sed ipsum id, praesentium. Maiores possimus facere, dolores ducimus doloribus similique voluptas. Dicta nobis adipisci libero iste, incidunt! Voluptatem, nam.
                    </p>
                </article>
                <div class="margin-t-40">
                    <a class="btn btn-trans-wb no-m-b">
                        Get Started
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
