
    <!-- Step 1 -->
    <section class="padding-t-80 padding-b-60 bg-ocean-1 txt-color-white">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 padding-b-20">
                    <h2 class=" txt-bold margin-b-20">
                        <span class="txt-normal-s d-inline padding-o-10 bg-yellow">
                            STEP 1
                        </span>
                    </h2>
                    <h1 class="txt-2em txt-bold txt-height-1-1">
                        Complete your online
                        Resume 
                    </h1>
                </div>

                <div class="col-md-9 padding-lr-40 padding-b-20">
                    <article>
                        <h3 class="txt-xlg txt-height-1-2 margin-b-10">
                           Register online & complete your Job Profile.
                        </h3>
                        <p class="txt-medium margin-b-30">
                            You will be able to auto-generate a CV when you complete your profile for free. 
                        </p>
                    </article>
                    <article class="margin-t-20">
                        <a href="" class="btn btn-trans-wb no-m-b txt-normal-s">
                            Get Started
                        </a>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <!-- Step 2 -->
    <section class="padding-t-80 bg-ocean-2 txt-color-white">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 padding-b-40">
                    <h2 class=" txt-bold margin-b-20">
                        <span class="txt-normal-s d-inline padding-o-10 bg-yellow">
                            STEP 2
                        </span>
                    </h2>
                    <h1 class="txt-2em txt-bold txt-height-1-1">
                        Join a role track
                    </h1>
                </div>

                <div class="col-md-9 padding-lr-40 padding-b-20">
                    <h3 class="txt-xlg txt-height-1-2 margin-b-30">
                       Every role has a couple of tasks attached to it. You will be required to demonstrate competency in each task.
                    </h3>
                    <h4 class="txt-bold margin-b-20">
                        Available Roles
                    </h4>
                    <div class="row row-10">
                        <div class="col-md-3 d-flex padding-lr-10 padding-b-30">
                            <article class="padding-o-10 border-o-1 border-color-white full-width">
                                <h5 class="txt-lg txt-medium margin-b-10">
                                    Entry Level Marketing/Sales
                                </h5>
                                <p class="txt-normal-s">
                                    3 Tasks
                                </p>
                            </article>
                        </div>
                        <div class="col-md-3 d-flex padding-lr-10 padding-b-30">
                            <article class="padding-o-10 border-o-1 border-color-white full-width">
                                <h5 class="txt-lg txt-medium margin-b-10">
                                    Business Analyst
                                </h5>
                                <p class="txt-normal-s">
                                    3 Tasks
                                </p>
                            </article>
                        </div>
                        <div class="col-md-3 d-flex padding-lr-10 padding-b-30">
                            <article class="padding-o-10 border-o-1 border-color-white full-width">
                                <h5 class="txt-lg txt-medium margin-b-10">
                                    Project Support Officer
                                </h5>
                                <p class="txt-normal-s">
                                    3 Tasks
                                </p>
                            </article>
                        </div>
                        <div class="col-md-3 d-flex padding-lr-10 padding-b-30">
                            <article class="padding-o-10 border-o-1 border-color-white full-width">
                                <h5 class="txt-lg txt-medium margin-b-10">
                                    HR/Admin Officer
                                </h5>
                                <p class="txt-normal-s">
                                    3 Tasks
                                </p>
                            </article>
                        </div>
                        <div class="col-md-3 d-flex padding-lr-10 padding-b-30">
                            <article class="padding-o-10 border-o-1 border-color-white full-width">
                                <h5 class="txt-lg txt-medium margin-b-10">
                                    Social Media Manager/Digital Marketer
                                </h5>
                                <p class="txt-normal-s">
                                    3 Tasks
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Step 3 -->
    <section class="padding-t-80 padding-b-40 bg-ocean-3 txt-color-white">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 padding-b-40">
                    <h2 class=" txt-bold margin-b-20">
                        <span class="txt-normal-s d-inline padding-o-10 bg-yellow">
                            STEP 3
                        </span>
                    </h2>
                    <h1 class="txt-2em txt-bold txt-height-1-1">
                        Showcase your Experience
                    </h1>
                </div>

                <div class="col-md-9 padding-lr-40 padding-b-20">
                    <h3 class="txt-xlg txt-height-1-2 margin-b-30">
                       Showcase your Experience gained while executing each task under the selected role: Find an avenue to engage in the tasks listed under a role a minimum of 3 times, and log your experience on your JobTrac dashboard.
                    </h3>
                    <div class="row row-20">
                        <div class="col-md-4 padding-lr-20 padding-b-20">
                            <article>
                                <h4 class="txt-bold margin-b-20 txt-height-1-2">
                                    If you have completed a listed task before:
                                </h4>
                                <p class="txt-normal-s">
                                    Log your past practical experience in completing that task 
                                </p>
                            </article>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-20">
                            <article>
                                <h4 class="txt-bold margin-b-20 txt-height-1-2">
                                    If haven’t had an opportunity to engage in a listed task
                                </h4>
                                <ul class="icon-list white txt-normal-s">
                                    <li>
                                        <a data-toggle="modal" href="#comingSoonModal">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Volunteer to participate in a project where you can gain practical experience on the task. We do our best to find volunteer project opportunities for you.
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" href="#comingSoonModal">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                OR  Create your own project that gives you the opportunity to engage in a task and share your experience
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </article>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-20">
                            <article>
                                <h4 class="txt-bold margin-b-20 txt-height-1-2">
                                    Need Help to complete a task/project?
                                </h4>
                                <p class="txt-normal-s">
                                    Get help from experts in the support community for that role
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Step 4 -->
    <section class="padding-tb-80 bg-ocean-4 txt-color-white">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 padding-b-40">
                    <h2 class=" txt-bold margin-b-20">
                        <span class="txt-normal-s d-inline padding-o-10 bg-yellow">
                            STEP 4
                        </span>
                    </h2>
                    <h1 class="txt-2em txt-bold txt-height-1-1">
                        Earn Softskill Points
                    </h1>
                </div>

                <div class="col-md-9 padding-lr-40 padding-b-20">
                    <article>
                        <h3 class="txt-xlg txt-height-1-2 margin-b-10">
                           Complete the Online Soft Skill Courses assigned to that role and earn the required Soft-Skill Points.
                        </h3>
                        <p class="txt-medium margin-b-30">
                            Soft skill courses prepares you to succeed on the job. 
                        </p>
                    </article>
                    <article class="margin-t-20">
                        <a href="" class="btn btn-trans-wb no-m-b txt-normal-s">
                            View Courses
                        </a>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <!-- Step 5 -->
    <section class="padding-t-80 padding-b-60 bg-ocean-5 txt-color-white">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 padding-b-40">
                    <h2 class=" txt-bold margin-b-20">
                        <span class="txt-normal-s d-inline padding-o-10 bg-yellow">
                            STEP 5
                        </span>
                    </h2>
                    <h1 class="txt-2em txt-bold txt-height-1-1">
                        Verify your Competency
                    </h1>
                </div>

                <div class="col-md-9 padding-lr-40 padding-b-20">
                    <div class="row row-20">
                        <div class="col-md-4 padding-lr-20 padding-b-30">
                            <div class="icon-line-box">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_test_your_ideas.png" alt="" width="50">
                                </figure>
                                <article>
                                    <h4 class="txt-bold margin-b-10">
                                        Request Expert Verification
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-7">
                                        When you have successfully demonstrated competency across the range of tasks required to participate in a role and logged your experience, request an expert verification to ascertain your competency claim. 
                                    </p>
                                </article>
                            </div>
                        </div>

                        <div class="col-md-4 padding-lr-20 padding-b-30">
                            <div class="icon-line-box">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_success_stories.png" alt="" width="50">
                                </figure>
                                <article>
                                    <h4 class="txt-bold margin-b-10">
                                        Get Certificate of Competence
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-7">
                                        Once you pass, you become a verified JobTrac candidate for that role. We give you a verifiable certificate of competence to prove to the world that you are competent in the listed skills.
                                    </p>
                                </article>
                            </div>
                        </div>

                        <div class="col-md-4 padding-lr-20 padding-b-30">
                            <div class="icon-line-box">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_real_world_experience.png" alt="" width="50">
                                </figure>
                                <article>
                                    <h4 class="txt-bold margin-b-10">
                                        Increase your Chances of Success
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-7">
                                        You can verify for more than one role. The more the roles you are verified for, the higher your chances of getting a Job. 
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Step 6 -->
    <section class="padding-t-80 padding-b-60 bg-ocean-6 txt-color-white">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 padding-b-40">
                    <h2 class=" txt-bold margin-b-20">
                        <span class="txt-normal-s d-inline padding-o-10 bg-yellow">
                            STEP 6
                        </span>
                    </h2>
                    <h1 class="txt-2em txt-bold txt-height-1-1">
                        Prepare for Interviews
                    </h1>
                </div>

                <div class="col-md-9 padding-lr-40 padding-b-20">
                    <div class="row row-20">
                        <div class="col-md-7 padding-lr-20 padding-b-30">
                            <h3 class="txt-xlg txt-height-1-2 margin-b-10">
                               Polish your resume & Prepare for Interviews: Access resources to help you update your resume and prepare to meet employers.
                            </h3>
                        </div>

                        <div class="col-md-5 padding-lr-20 padding-b-30">
                            <ul class="icon-list white txt-normal-s">
                                <li>
                                    <a data-toggle="modal" href="#comingSoonModal">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get Help from the Free Online Guide
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="modal" href="#comingSoonModal">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get Mentor Support in the Job Advisor Forum
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="modal" href="#comingSoonModal">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Let an expert do it for you.
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Step 7 -->
    <section class="padding-t-80 padding-b-60 bg-ocean-7 txt-color-white">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 padding-b-40">
                    <h2 class=" txt-bold margin-b-20">
                        <span class="txt-normal-s d-inline padding-o-10 bg-yellow">
                            STEP 7
                        </span>
                    </h2>
                    <h1 class="txt-2em txt-bold txt-height-1-1">
                        Earn your Job
                    </h1>
                </div>

                <div class="col-md-9 padding-lr-40 padding-b-20">
                    <h3 class="txt-xlg txt-height-1-2 margin-b-40">
                        Attend Interviews & Get Placed
                    </h3>
                    <div class="row row-20">
                        <div class="col-md-6 padding-lr-20 padding-b-30">
                            <div class="row row-10">
                                <figure class="col-2 padding-lr-10 margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="">
                                </figure>
                                <article class="col-10 padding-lr-10">
                                    <p class="txt-normal-s txt-height-1-7">
                                        Our recruitment team partners with you to schedule interviews with employers looking to hire JobTrac candidates for the roles you have been verified for.
                                    </p>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-6 padding-lr-20 padding-b-30">
                            <div class="row row-10">
                                <figure class="col-2 padding-lr-10 margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_amazing_support_network.png" alt="">
                                </figure>
                                <article class="col-10 padding-lr-10">
                                    <p class="txt-normal-s txt-height-1-7">
                                        You can also go ahead and attend other external interviews. For those interviews, we will give you a special reference letter to back up your application.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
