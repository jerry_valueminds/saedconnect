
    <!-- Step 1 -->
    <section class="txt-color-white">
        <div class="">
            <div class="row">

                <div class="col-md-8 d-none d-md-block feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image%20(3).png')">

                </div>
                <div class="col-md-4 bg-blue padding-tb-40 padding-lr-40">
                    <h2 class="txt-xlg txt-medium txt-height-1-5">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit fuga doloremque possimus laboriosam quod quia, libero nam odio eligendi nesciunt, ex atque corrupti vel sequi labore neque cupiditate culpa totam.
                    </h2>
                    <article class="margin-t-30">
                        <a href="" class="btn btn-trans-wb no-m-b txt-normal-s margin-r-10">
                            The JobTrac Concept
                        </a>
                        <a href="" class="btn btn-trans-wb no-m-b txt-normal-s">
                            Get Started
                        </a>
                    </article>
                </div>

            </div>
        </div>
    </section>

    <!-- Step 2 -->
    <section class="bg-grey padding-t-80 padding-b-50">
        <div class="container-wrapper">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="txt-2em txt-bold txt-height-1-1 margin-b-20">
                        6 Steps to go
                    </h2>
                    <p class="margin-b-40">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis quasi dolor alias recusandae accusamus voluptates.
                    </p>
                </div>
            </div>
            <div class="d-none d-md-flex row row-10">
                <div class="col-md-2 d-flex padding-lr-10 padding-b-30">
                    <article class="padding-o-20 border-o-1 border-color-darkgrey full-width">
                        <h6 class="txt-bold margin-b-10">
                            STEP 1
                        </h6>
                        <h5 class="txt-sm margin-b-10 txt-height-1-1">
                            Sign up on
                            <a href="https://www.saedconnec.org">
                                www.saedconnect.org
                            </a>
                        </h5>
                    </article>
                </div>
                <div class="col-md-2 d-flex padding-lr-10 padding-b-30">
                    <article class="padding-o-20 border-o-1 border-color-darkgrey full-width">
                        <h6 class="txt-bold margin-b-10">
                            STEP 2
                        </h6>
                        <h5 class="txt-sm margin-b-10 txt-height-1-1">
                            Complete your online resume on your career dashboard
                        </h5>
                    </article>
                </div>
                <div class="col-md-2 d-flex padding-lr-10 padding-b-30">
                    <article class="padding-o-20 border-o-1 border-color-darkgrey full-width">
                        <h6 class="txt-bold margin-b-10">
                            STEP 3
                        </h6>
                        <h5 class="txt-sm margin-b-10 txt-height-1-1">
                            Select a role you are interested in
                        </h5>
                    </article>
                </div>
                <div class="col-md-2 d-flex padding-lr-10 padding-b-30">
                    <article class="padding-o-20 border-o-1 border-color-darkgrey full-width">
                        <h6 class="txt-bold margin-b-10">
                            STEP 4
                        </h6>
                        <h5 class="txt-sm margin-b-10 txt-height-1-1">
                            Build & log your experience completing tasks under that job role
                        </h5>
                    </article>
                </div>
                <div class="col-md-2 d-flex padding-lr-10 padding-b-30">
                    <article class="padding-o-20 border-o-1 border-color-darkgrey full-width">
                        <h6 class="txt-bold margin-b-10">
                            STEP 5
                        </h6>
                        <h5 class="txt-sm margin-b-10 txt-height-1-1">
                            From your Career Dashboard, Request a review of your competency, and get verified
                        </h5>
                    </article>
                </div>
                <div class="col-md-2 d-flex padding-lr-10 padding-b-30">
                    <article class="padding-o-20 border-o-1 border-color-darkgrey full-width">
                        <h6 class="txt-bold margin-b-10">
                            STEP 6
                        </h6>
                        <h5 class="txt-sm margin-b-10 txt-height-1-1">
                            Get referred to employers and attend job interviews
                        </h5>
                    </article>
                </div>
            </div>
            <div class="article nargin-t-40">
                <a href="" class="btn btn-blue no-m-b txt-normal-s">
                    How it works
                </a>
            </div>
        </div>
    </section>

    <!-- Step 3 -->
    <section class="padding-t-80 padding-b-40">
        <div class="container-wrapper">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="txt-2em txt-bold txt-height-1-1 margin-b-20">
                        Available Roles
                    </h2>
                    <p class="margin-b-40">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis quasi dolor alias recusandae accusamus voluptates.
                    </p>
                </div>
            </div>
            <div class="row row-15">

                <div class="col-md-3 padding-lr-15 d-flex padding-b-20">
                    <div class="flex full-width bg-white border-o-1 border-color-darkgrey">
                        <h3 class="txt-xlg txt-bold padding-lr-20 padding-t-20">
                            Social Media Manager
                        </h3>
                        <article class="padding-o-20">
                            <p class="txt-normal-s txt-height-1-7 padding-tb-10 border-tb-1 border-color-darkgrey">

                                <span class="txt-bold txt-color-blue">
                                    Status:
                                </span>
                                <span class="">
                                    Recruiting
                                </span>
                            </p>
                            <p class="txt-sm txt-height-1-5 padding-tb-10 border-b-0 border-color-darkgrey">
                                Candidate is required to complete 5 Tasks to become eligible
                            </p>
                        </article>
                        <article class="padding-lr-20 padding-b-20">
                            <a href="job_track_role.html" class="btn btn-blue txt-normal-s no-m-b">
                                View Tasks
                            </a>
                        </article>
                    </div>
                </div>
                <div class="col-md-3 padding-lr-15 d-flex padding-b-20">
                    <div class="flex full-width bg-white border-o-1 border-color-darkgrey">
                        <h3 class="txt-xlg txt-bold padding-lr-20 padding-t-20">
                            Research / Business Analyst
                        </h3>
                        <article class="padding-o-20">
                            <p class="txt-normal-s txt-height-1-7 padding-tb-10 border-tb-1 border-color-darkgrey">

                                <span class="txt-bold txt-color-blue">
                                    Status:
                                </span>
                                <span class="">
                                    Recruiting
                                </span>
                            </p>
                            <p class="txt-sm txt-height-1-5 padding-tb-10 border-b-0 border-color-darkgrey">
                                Candidate is required to complete 5 Tasks to become eligible
                            </p>
                        </article>
                        <article class="padding-lr-20 padding-b-20">
                            <a href="job_track_role.html" class="btn btn-blue txt-normal-s no-m-b">
                                View Tasks
                            </a>
                        </article>
                    </div>
                </div>
                <div class="col-md-3 padding-lr-15 d-flex padding-b-20">
                    <div class="flex full-width bg-white border-o-1 border-color-darkgrey">
                        <h3 class="txt-xlg txt-bold padding-lr-20 padding-t-20">
                            Sales / Marketing Officer
                        </h3>
                        <article class="padding-o-20">
                            <p class="txt-normal-s txt-height-1-7 padding-tb-10 border-tb-1 border-color-darkgrey">

                                <span class="txt-bold txt-color-blue">
                                    Status:
                                </span>
                                <span class="">
                                    Recruiting
                                </span>
                            </p>
                            <p class="txt-sm txt-height-1-5 padding-tb-10 border-b-0 border-color-darkgrey">
                                Candidate is required to complete 5 Tasks to become eligible
                            </p>
                        </article>
                        <article class="padding-lr-20 padding-b-20">
                            <a href="job_track_role.html" class="btn btn-blue txt-normal-s no-m-b">
                                View Tasks
                            </a>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-ocean-6 padding-t-80 padding-b-60">
        <div class="container-wrapper txt-color-white">
            <div class="row">
                <div class="col-md-6 padding-lr-20">
                    <h1 class="txt-medium txt-2em margin-b-30">
                        Join the JobTrac Program
                    </h1>
                    <article class="description">
                        <p class="txt-normal-s">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus repudiandae sed ipsum id, praesentium. Maiores possimus facere, dolores ducimus doloribus similique voluptas. Dicta nobis adipisci libero iste, incidunt! Voluptatem, nam.
                        </p>
                    </article>
                    <div class="margin-t-40">
                        <a class="btn btn-trans-wb no-m-b">
                            Get Started
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
