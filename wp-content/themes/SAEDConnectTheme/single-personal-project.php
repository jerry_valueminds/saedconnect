<?php get_header() ?>
   
<?php
    $site_url = site_url();
    $current_user = wp_get_current_user();
?>
   
<?php
    switch_to_blog(110);
    $states = get_terms( 'state', array('hide_empty' => false));
    restore_current_blog();

    switch_to_blog(104);
    
    /* Get Thematic Areas */
    $thematic_areas_query = new WP_Query();
    $thematic_areas_query->query( 
        array(
            'post_type' => 'thematic-area',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        ) 
    );

    /* Get SDGs */
    $sdgs_query = new WP_Query();
    $sdgs_query->query( 
        array(
            'post_type' => 'sdg',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        ) 
    );

    restore_current_blog();
?>
   
<?php 
    /* Get Image Field */
    function get_the_image_url($post_id){
        $images = get_attached_media( 'image', $post_id );
        $previousImg_id = 0;
        if($images){
            foreach($images as $image) { 
                $previousImg_id = $image->ID;
            }
            return wp_get_attachment_url($previousImg_id,"full");
        } else {
            return 0;
        }
    }
?>
    
<?php while ( have_posts() ) : the_post(); ?>

<?php $project_id = get_the_ID(); ?>
  
<?php endwhile; // end of the loop. ?>
   
    <main class="main-content">
        <header class="container-wrapper padding-tb-60">
            <div class="row">
                <div class="col-lg-10 mx-auto border-b-1 border-color-lighter">
                    <div class="row row-40">
                        <div class="col-lg-5 padding-lr-40 padding-b-40">
            
                            <?php if( get_the_image_url ){ ?>
                                <img src="<?php echo get_the_image_url($project_id); ?>">
                            <?php } else { ?>
                                <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png">
                                </figure>
                            <?php } ?>
                        </div>
                        <div class="col-lg-7 padding-lr-40 padding-b-40">
                            <h1 class="txt-2em txt-medium txt-height-1-1 uppercase padding-b-30">
                                <?php the_title() ?>
                            </h1>
                            <p class="txt-normal-s txt-light txt-height-1-7">
                                <?php echo get_post_meta( $project_id, 'summary', true ); ?>
                            </p>
                            <div class="padding-t-40 d-flex align-items-center">
                                <p class="inline-block txt-medium padding-tb-10 padding-lr-15 border-o-1">
                                    <?php
                                        $list = get_post_meta( $project_id, 'thematic_areas', false );
                                            

                                        if ( $thematic_areas_query->have_posts() ) {

                                            while ($thematic_areas_query->have_posts()) : $thematic_areas_query->the_post();
                                                if( in_array($post->ID, $list) ){
                                    ?>
                                                <?php the_title(); echo ', '; ?>

                                            <?php } ?>
                                        <?php endwhile; ?>
                                    <?php } ?>
                                </p>
                            </div>
                            <div class="padding-t-30">
                                <?php
                                    $list = get_post_meta( $project_id, 'sdgs', true );
                                        switch_to_blog(104);
                                    if ( $sdgs_query->have_posts() ) {

                                        while ($sdgs_query->have_posts()) : $sdgs_query->the_post();
                                            if( in_array($post->ID, $list) ){
                                                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); // Featured Image
                                ?>
                                            <img class="d-inline-block margin-r-20" width="50" src="<?php echo $featured_img_url; ?>" alt="<?php the_title() ?>">

                                        <?php } ?>
                                    <?php endwhile; ?>
                                <?php } ?>
                                <?php restore_current_blog(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <!-- Snapshot --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium container-wrapper margin-b-30">
                            Project Snapshot
                        </h2>
                        <div class="bg-ghostwhite container-wrapper padding-t-40 padding-b-10">
                            <div class="row row-15">
                                <div class="col-lg-4 padding-lr-15 padding-b-30">
                                    <p class="txt-sm uppercase">Age Group</p>
                                    <p class="txt-xlg txt-bold txt-height-1 txt-color-yellow">
                                        <?php
                                            $list = get_post_meta( $project_id, 'target_age_group', true );
                                            $age_groups_array = array(
                                                '1 – 5', '6 – 12', '13 – 19', '20 – 25', '26 – 30', '30 – 40', '40 – 60', 'Above 60',   
                                            );
                                        ?>

                                        <?php foreach ( $age_groups_array as $age_group_item ) { ?>
                                            <?php foreach ($list as $list_item){ ?>
                                                <?php if ($age_group_item == $list_item){ ?>
                                                <?php echo $age_group_item; echo ', '; ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </p>
                                </div>
                                <div class="col-lg-4 padding-lr-15 padding-b-30">
                                    <p class="txt-sm uppercase">NO OF USERS/BENEFICIARIES</p>
                                    <p class="txt-xlg txt-bold txt-color-yellow">-</p>
                                </div>
                                <div class="col-lg-4 padding-lr-15 padding-b-30">
                                    <p class="txt-sm uppercase">STATES IN WHICH THIS PROJECT IS ACTIVE</p>
                                    <p class="txt-xlg txt-bold txt-height-1 txt-color-yellow">
                                        <?php $meta = get_post_meta( $project_id, 'state', true ); ?>
                                        <?php foreach ($states as $state) { ?>
                                            <?php foreach ($meta as $saved_state){ ?>
                                                <?php if ($state->term_id == $saved_state){ ?>
                                                    <?php echo $state->name; echo ', '; ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </p>
                                </div>
                                <div class="col-lg-4 padding-lr-15 padding-b-30">
                                    <p class="txt-sm uppercase">ESTABLISHED</p>
                                    <p class="txt-xlg txt-bold txt-color-yellow"><?php echo get_post_meta( $project_id, 'start_year', true ); ?></p>
                                </div>
                                <div class="col-lg-4 padding-lr-15 padding-b-30">
                                    <p class="txt-sm uppercase">ORGANIZATION CAPACITY</p>
                                    <p class="txt-xlg txt-bold txt-height-1 txt-color-yellow">
                                        <?php echo get_post_meta( $project_id, 'capacity', true ); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
          
        <!-- Descripion --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium container-wrapper margin-b-30">
                            Project Description
                        </h2>
                        <div class="bg-ghostwhite container-wrapper padding-t-40 padding-b-10">
                            <div class="row row-80">
                                <div class="col-lg-6 padding-lr-80 padding-b-30">
                                    <div class="d-flex">
                                        <figure class="col-auto">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/problem.png" alt="icon">
                                        </figure>
                                        <div class="col padding-l-20">
                                            <p class="txt-lg txt-bold uppercase padding-b-15">Problem</p>
                                            <p class="txt-normal-s txt-color-light">
                                                <?php echo get_post_meta( $project_id, 'problem_solved', true ); ?>
                                            </p>
                                        </div>
                                    </div>        
                                </div>
                                <div class="col-lg-6 padding-lr-80 padding-b-30">
                                    <div class="d-flex">
                                        <figure class="col-auto">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/problem.png" alt="icon">
                                        </figure>
                                        <div class="col padding-l-20">
                                            <p class="txt-lg txt-bold uppercase padding-b-15">Solution</p>
                                            <p class="txt-normal-s txt-color-light">
                                                <?php echo get_post_meta( $project_id, 'your_solution', true ); ?>
                                            </p>
                                        </div>
                                    </div>        
                                </div>
                                <div class="col-12 padding-lr-80 padding-tb-30">
                                    <div class="d-flex">
                                        <figure class="col-auto">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/tips.png" alt="icon">
                                        </figure>
                                        <div class="col padding-l-20">
                                            <p class="txt-lg txt-bold uppercase padding-b-15">TIPS FOR IMPLEMENTATION</p>
                                            <p class="txt-normal-s txt-color-light">
                                                <?php echo get_post_meta( $project_id, 'tips_for_implementation', true ); ?>
                                            </p>
                                        </div>
                                    </div>        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
          
        <!-- Updates --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium container-wrapper margin-b-30">
                            Project Updates
                        </h2>
                        <div class="bg-ghostwhite container-wrapper padding-t-40">
                            <div class="row row-30">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'project-meta',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'meta-type',
                                                'value' => 'project-update',
                                            ),
                                            array(
                                                'key' => 'parent',
                                                'value' => $project_id
                                            )
                                        ),
                                    ) 
                                );
                            ?>
                            
                            <?php if ( $profile_query->have_posts() ) { ?>

                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                    <?php 
                                        $post_id = $post->ID;
                                        $type = get_post_meta( $post_id, 'type', true );
                                    ?> 
                                    
                                    <div class="col-lg-4 padding-lr-30 padding-b-60">
                                        <a href="<?php echo ''.get_post_meta( $post_id, 'url', true ); ?>" class="project-update-card" style="background-image: url('<?php echo get_the_image_url($post_id); ?>')">
                                            <?php if( $type == 'Album' ){ ?>
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/audio.png" alt="">
                                            <?php }elseif( $type == 'Video' ){ ?>
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/play_video.png" alt="">
                                            <?php }elseif( $type == 'Article' ){ ?>
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/news.png" alt="">
                                            <?php } ?>
                                            <p class="txt-normal-s txt-color-white"><?php the_title() ?></p>
                                        </a>
                                    </div>


                                <?php endwhile; ?> 

                            <?php }else{ ?> 
                                <p class="col-12 txt-normal-s padding-lr-30 padding-b-60">
                                    You haven't any updates yet.
                                </p>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
          
        <!-- Milestones --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium container-wrapper margin-b-30">
                            Milestones
                        </h2>
                        <div class="bg-ghostwhite container-wrapper padding-t-40">
                            <div class="row row-30">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'project-meta',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'meta-type',
                                                'value' => 'project-milestone',
                                            ),
                                            array(
                                                'key' => 'parent',
                                                'value' => $project_id
                                            )
                                        ),
                                    ) 
                                );
                            ?>
                            
                            <?php if ( $profile_query->have_posts() ) { ?>

                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                    <?php 
                                        $post_id = $post->ID;
                                        $type = get_post_meta( $post_id, 'type', true );
                                    ?> 
                                    
                                    <div class="col-lg-4 padding-lr-30 padding-b-60">
                                        <span class="project-update-card rounded-0" style="background-image: url('<?php echo get_the_image_url($post_id); ?>')">
                                        </span>
                                        <div class="bg-white padding-o-20 text-center">
                                            <p class="txt-bold padding-b-10"><?php the_title() ?></p>
                                            <p class="txt-normal-s">
                                                <?php 
                                                    $date = strtotime( get_post_meta( $post_id, 'month_year', true ) );
                                                    echo date('F', $date);
                                                ?>
                                            </p>
                                            <p class="txt-normal-s txt-medium">
                                                <?php 
                                                    $date = strtotime( get_post_meta( $post_id, 'month_year', true ) );
                                                    echo date('Y', $date);
                                                ?>
                                            </p>
                                        </div>
                                    </div>

                                <?php endwhile; ?> 

                            <?php }else{ ?> 
                                <p class="col-12 txt-normal-s padding-lr-30 padding-b-60">
                                    You haven't any updates yet.
                                </p>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Resources --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium container-wrapper margin-b-30">
                            Project Resources
                        </h2>
                        <?php
                            $profile_query = new WP_Query();
                            $profile_query->query( 
                                array(
                                    'post_type' => 'project-meta',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'meta-type',
                                            'value' => 'project-resource',
                                        ),
                                        array(
                                            'key' => 'parent',
                                            'value' => $project_id
                                        )
                                    ),
                                ) 
                            );
                        ?>

                        <?php if ( $profile_query->have_posts() ) { ?>

                            <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                <?php 
                                    $post_id = $post->ID;
                                    $type = get_post_meta( $post_id, 'type', true );
                                ?> 

                                <div class="bg-ghostwhite container-wrapper padding-t-40 padding-b-10 margin-b-10">
                                    <div class="d-flex">
                                        <div class="col-auto">
                                            <figure class="d-inline-block rounded-circle bg-ash padding-o-10">
                                                <?php if( $type == 'Album' ){ ?>
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/audio.png" alt="icon" width="40">
                                                <?php }elseif( $type == 'Video' ){ ?>
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/play_video.png" alt="icon" width="40">
                                                <?php }elseif( $type == 'Article' ){ ?>
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/news.png" alt="icon" width="40">
                                                <?php } ?>
                                            </figure>
                                        </div>
                                        <div class="col padding-l-30">
                                            <div class="row">
                                                <div class="col padding-r-30 padding-b-30">
                                                    <p class="txt-bold padding-b-15"><?php the_title() ?></p>
                                                    <p class="txt-normal-s txt-color-light">
                                                        <?php echo ''.get_post_meta( $post_id, 'summary', true ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-12 col-lg-auto">
                                                    <a href="<?php echo ''.get_post_meta( $post_id, 'url', true ); ?>" class="btn btn-trans-bw txt-sm no-m-b">Read more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>       
                                </div>

                            <?php endwhile; ?> 

                        <?php }else{ ?> 
                            <p class="txt-normal-s padding-lr-30 padding-b-60">
                                You haven't added any resources yet.
                            </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
         
        <!-- Implement this Project --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium txt-height-1-2 container-wrapper margin-b-30">
                            Implement this Project in your local community by using these steps
                        </h2>
                        <div class="bg-ghostwhite container-wrapper padding-t-40 padding-b-10">
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'project-meta',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                        'orderby' => 'meta_value_num',
                                        'meta_key'  => 'step_number',
                                        'order' => 'ASC',
                                        'meta_query' => array(
                                            array(
                                                'key' => 'meta-type',
                                                'value' => 'project-step',
                                            ),
                                            array(
                                                'key' => 'parent',
                                                'value' => $project_id
                                            )
                                        ),
                                    ) 
                                );
                            ?>

                            <?php if ( $profile_query->have_posts() ) { ?>

                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                    <?php 
                                        $post_id = $post->ID;
                                        $type = get_post_meta( $post_id, 'type', true );
                                    ?> 
                                    
                                    <div class="overflow-hidden padding-b-10 margin-b-30 border-b-1 border-color-lighter">
                                        <div class="row row-20">
                                            <figure class="col-lg-2 padding-lr-20 padding-b-20">
                                                <?php if( get_the_image_url($post_id) ){ ?>
                                                    <img src="<?php echo get_the_image_url($post_id); ?>" alt="icon">
                                                <?php }else{ ?>
                                                    <figure class="d-flex justify-content-center align-items-center border-o-1 border-color-darkgrey">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png">
                                                    </figure>
                                                <?php } ?>
                                            </figure>
                                            <div class="col-lg-10 padding-lr-20 padding-b-20">
                                                <div class="d-flex align-items-center padding-b-15">
                                                    <p class="txt-2em txt-bold txt-color-yellow col-auto">
                                                        <?php
                                                            $num_padded = sprintf("%02d", get_post_meta( $post_id, 'step_number', true ));
                                                            echo $num_padded; // returns 04
                                                        ?>
                                                    </p>
                                                    <p class="txt-bold padding-l-15"><?php the_title() ?></p>
                                                </div>
                                                <p class="txt-sm txt-color-light">
                                                    <?php echo get_post_meta( $post_id, 'description', true ); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                <?php endwhile; ?> 

                            <?php }else{ ?> 
                                <p class="txt-normal-s padding-lr-30 padding-b-60">
                                    No requests available.
                                </p>
                            <?php } ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
         
        <!-- Be a part of this project --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium txt-height-1-2  container-wrapper margin-b-30">
                            You can also be a part of this project
                        </h2>
                        <?php
                            $profile_query = new WP_Query();
                            $profile_query->query( 
                                array(
                                    'post_type' => 'project-meta',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'meta-type',
                                            'value' => 'project-request',
                                        ),
                                        array(
                                            'key' => 'parent',
                                            'value' => $project_id
                                        )
                                    ),
                                ) 
                            );
                        ?>

                        <?php if ( $profile_query->have_posts() ) { ?>

                            <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                <?php 
                                    $post_id = $post->ID;
                                    $type = get_post_meta( $post_id, 'type', true );
                                ?> 
                                
                                <div class="bg-ghostwhite container-wrapper padding-tb-40 margin-b-10">
                                    <p class="txt-bold padding-b-15"><?php the_title() ?></p>
                                    <p class="txt-normal-s txt-color-light">
                                        <?php echo ''.get_post_meta( $post_id, 'description', true ); ?>
                                    </p>
                                </div>

                            <?php endwhile; ?> 

                        <?php }else{ ?> 
                            <p class="txt-normal-s padding-lr-30 padding-b-60">
                                No requests available.
                            </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Contact --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium txt-height-1-2  container-wrapper margin-b-30">
                            Reach out to us, we wanna hear from you
                        </h2>
                        <div class="row row-15 text-center txt-normal-s">
                            <a href="<?php echo get_post_meta( $project_id, 'contact_website', true ); ?>" class="col-md-6 col-lg-3 d-flex padding-lr-15 padding-b-20">
                                <div class="bg-ash flex_1 padding-o-30">
                                    <figure class="d-inline-block margin-b-15">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/website.png" alt="" width="70" height="70">
                                    </figure>
                                    <p class="txt-bold txt-color-white"><?php echo get_post_meta( $project_id, 'contact_website', true ); ?></p>
                                </div>
                            </a>
                            <a href="mailto:<?php echo get_post_meta( $project_id, 'contact_email', true ); ?>" class="col-md-6 col-lg-3 d-flex padding-lr-15 padding-b-20">
                                <div class="bg-ash flex_1 padding-o-30">
                                    <figure class="d-inline-block margin-b-15">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/mail.png" alt="" width="70" height="70">
                                    </figure>
                                    <p class="txt-bold txt-color-white"><?php echo get_post_meta( $project_id, 'contact_email', true ); ?></p>
                                </div>
                            </a>
                            <a href="tel:<?php echo get_post_meta( $project_id, 'contact_phone', true ); ?>" class="col-md-6 col-lg-3 d-flex padding-lr-15 padding-b-20">
                                <div class="bg-ash flex_1 padding-o-30">
                                    <figure class="d-inline-block margin-b-15">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/phone.png" alt="" width="70" height="70">
                                    </figure>
                                    <p class="txt-bold txt-color-white">+<?php echo get_post_meta( $project_id, 'contact_phone', true ); ?></p>
                                </div>
                            </a>
                            <div class="col-md-6 col-lg-3 d-flex padding-lr-15 padding-b-20">
                                <div class="bg-ash flex_1 padding-o-30">
                                    <figure class="d-inline-block margin-b-15">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/person.png" alt="" width="70" height="70">
                                    </figure>
                                    <p class="txt-bold txt-color-white"><?php echo get_post_meta( $project_id, 'contact_name', true ); ?> is our primary contact</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
         
        <!-- Meet the Team members --> 
        <section class="padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xxlg txt-medium txt-height-1-2  container-wrapper margin-b-30">
                            Meet the Team members
                        </h2>
                        <div class="row row-15 txt-normal-s">
                        <?php
                            $profile_query = new WP_Query();
                            $profile_query->query( 
                                array(
                                    'post_type' => 'project-meta',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'meta-type',
                                            'value' => 'project-team',
                                        ),
                                        array(
                                            'key' => 'parent',
                                            'value' => $project_id
                                        )
                                    ),
                                ) 
                            );
                        ?>

                        <?php if ( $profile_query->have_posts() ) { ?>

                            <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                <?php 
                                    $post_id = $post->ID;
                                    $type = get_post_meta( $post_id, 'type', true );
                                ?> 

                                <div class="col-lg-6 padding-lr-15 padding-b-30">
                                    <div class="bg-ash  padding-o-30" style="background-color: #CBCBCB">
                                        <div class="row margin-b-30">
                                            <div class="col-auto">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/team_email.png" alt="Icon" style="opacity: 0">
                                            </div>
                                            <div class="col padding-l-20">
                                                <p class="txt-lg txt-bold uppercase"><?php the_title() ?></p>
                                                <p class="txt-normal-s"><?php echo get_post_meta( $post_id, 'role', true ); ?></p>
                                            </div>
                                        </div>
                                        <a href="mailto:<?php echo get_post_meta( $post_id, 'email', true ); ?>" class="row align-items-center margin-b-10">
                                            <div class="col-auto">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/team_email.png" alt="Icon">
                                            </div>
                                            <div class="col padding-l-20">
                                                <p class="txt-normal-s txt-color-dark"><?php echo get_post_meta( $post_id, 'email', true ); ?></p>
                                            </div>
                                        </a>
                                        <div class="row align-items-center margin-b-10">
                                            <div class="col-auto">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/team_twiter.png" alt="Icon">
                                            </div>
                                            <div class="col padding-l-20">
                                                <p><?php echo get_post_meta( $post_id, 'twitter', true ); ?></p>
                                            </div>
                                        </div>
                                        <div class="row align-items-center margin-b-10">
                                            <div class="col-auto">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/team_instagram.png" alt="Icon">
                                            </div>
                                            <div class="col padding-l-20">
                                                <p><?php echo get_post_meta( $post_id, 'instagram', true ); ?></p>
                                            </div>
                                        </div>
                                        <a href="<?php echo get_post_meta( $post_id, 'linkedin', true ); ?>" class="row align-items-center">
                                            <div class="col-auto">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/dashboard/team_linkedin.png" alt="Icon">
                                            </div>
                                            <div class="col padding-l-20">
                                                <p class="txt-normal-s txt-color-dark"><?php echo get_post_meta( $post_id, 'linkedin', true ); ?></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            <?php endwhile; ?> 

                        <?php }else{ ?> 
                            <p class="col-12 txt-normal-s padding-lr-30 padding-b-60">
                                You haven't any team members yet.
                            </p>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>



<?php get_footer() ?>