

<?php
    /* 
        Establish connection to Applications DB:
            - Include WordPress Global DataBase Object
            - This Database contains the 'applications' table
            - The Applications table stores all application related
    */
    global $wpdb; //Include WP Global Object
    $table = "applications"; //Applications DB Table Name
    $application_db = new wpdb('root','umMv65ekyMRxfNfm','applications_db','localhost'); //DB COnnection

    /* Make variables available to template parts */
    set_query_var( 'table', $table );
    set_query_var( 'application_db', $application_db );


    /* Get Current User */
    $current_user = wp_get_current_user();
    set_query_var( 'current_user', $current_user ); //Make variables available to template parts

    /* Dahboard Manager Link */
    $dashboard_management_link = 'https://www.saedconnect.org/service-marketplace/my-jobs/';
    set_query_var( 'dashboard_management_link', $dashboard_management_link ); //Make variables available to template parts


    /*  Get Queried Page View */
    if ( $_GET['view'] ){
        $rendered_view = $_GET['view'];
        set_query_var( 'rendered_view', $rendered_view ); //Make variables available to template parts 
    }

    /*  Get Queried Manager Page View */
    if ( $_GET['manager-view'] ){
        $manager_view = $_GET['manager-view'];
    } else {
        $manager_view = 'unsorted';
    }
    set_query_var( 'manager_view', $manager_view ); //Make variables available to template parts 
    
    /* Manager view array */
    $manager_views_array = array(
        array(
            'slug' => 'unsorted',
            'name' => 'Unsorted'
        ),
        array(
            'slug' => 'rejected',
            'name' => 'Rejected'
        ),
        array(
            'slug' => 'in-review',
            'name' => 'In review'
        ),
        array(
            'slug' => 'shortlisted',
            'name' => 'Shortlisted'
        ),
        array(
            'slug' => 'offerred',
            'name' => 'Offerred'
        ),
        array(
            'slug' => 'hired',
            'name' => 'Hired'
        )
    );

    set_query_var( 'manager_views_array', $manager_views_array ); //Make variables available to template parts

    /* Get Queried Post Object */
    while ( have_posts() ) : the_post();

        /* Get Post Data */
        $post_id = get_the_ID(); //ID
        $post_type = get_post_type(); //Post Type
        $post_title = get_the_title(); //Title
        $post_link = get_permalink(); //Post Link
        $post_author_id = get_the_author_meta('ID'); //Author ID

        /* Open Graph */
        global $ogLink;
        global $ogTitle;
        global $ogDescription;
        global $ogImage;

        $ogLink = get_permalink();
        $ogTitle = $post_title;
        $ogDescription = get_post_meta( $post->ID, 'job-summary', true );
        $ogImage = 'https://www.saedconnect.org/service-marketplace/wp-content/themes/SAEDConnectTheme/images/icons/logo.png';

        /* Make variables available to template parts */
        set_query_var( 'post', $post );
        set_query_var( 'post_id', $post_id );
        set_query_var( 'post_type', $post_type );
        set_query_var( 'post_title', $post_title );
        set_query_var( 'post_link', $post_link );
        set_query_var( 'post_author_id', $post_author_id );

    endwhile; //resetting the page loop
?>

<?php get_header() ?>
    
    <main class="main-content">
        <?php get_template_part( 'template-parts/service-offer/applicant-move-action' ); ?>
        <?php get_template_part( 'template-parts/service-offer/search-bar' ); ?>
        <?php get_template_part( 'template-parts/service-offer/bundle-bar' ); ?>
        <?php get_template_part( 'template-parts/service-offer/author-menu' ); ?>
        <?php get_template_part( 'template-parts/service-offer/title-section' ); ?>
        <?php get_template_part( 'template-parts/service-offer/description' ); ?>
        <?php get_template_part( 'template-parts/service-offer/how-to-apply' ); ?>
        <?php get_template_part( 'template-parts/service-offer/application-manager-menu' ); ?>
        <?php get_template_part( 'template-parts/service-offer/application-manager' ); ?>
        <?php get_template_part( 'template-parts/service-offer/apply' ); ?>
        <?php get_template_part( 'template-parts/service-offer/application-confirmation' ); ?>
        <?php get_template_part( 'template-parts/service-offer/modals' ); ?>
        
        <?php if ( $_GET['view'] == 'applyx' ) { ?>
        
        
        <?php } elseif ( $_GET['view'] == 'application-confirmation' ) {  ?>
                
        <div class="container-wrapper padding-tb-40">
            <div class="row">
                <div class="col-md-8 mx-auto text-center">
                    <h1 class="txt-xlg margin-b-20">
                    <?php
                        /* Check if User is signed */
                        if ( is_user_logged_in() ) {

                            /* Get Applications DB */
                            $application_db = new wpdb('root','umMv65ekyMRxfNfm','applications_db','localhost'); //Server

                            /* Check Application Entry in DB */
                            $application_entry = $application_db->get_row("SELECT post_id, post_type FROM ".$table." WHERE user_id = '".$current_user->ID."' LIMIT 0,1");

                            /* Check if User has applied*/
                            if( $application_entry ){

                                echo "You have already applied.";

                            } else {

                                /* Create application status*/
                                $application_db->insert( 
                                    $table, 
                                    array( 
                                        "post_id" => $post_id,
                                        "post_type" => "job",
                                        "user_id" => $current_user->ID,
                                        "status" => "applied",
                                    ), 
                                    array( "%d", "%s", "%d", "%s" ) 
                                );

                                echo "Your Application for the ".get_the_title().' was succesfully submitted.';
                            }
                            
                        } else {
                            
                            echo "You must be logged in to continue";
                            
                        }
                    ?>
                    </h1>
                </div>
            </div>
        </div>
        
        <?php } ?>
    </main>
    
<?php
    
?>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<?php get_footer() ?>