<?php /*Template Name: Profile - NYSC National Validators*/ ?>
<?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Post Type */
        $postType = 'validation-program';

        /* Meta Key */
        $meta_key = 'nysc_assigned_state';
        $state_id = 'nationwide';

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Assign Users as NYSC National Validators
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/validation/manage-nysc-state-validators/" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>" method="post">
                            <?php
                                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if( $_POST ){                                    
                                    /* Delete existing meta */
                                    $saved_users = get_users( array('blog_id' => 1, 'orderby' => 'nicename', 'meta_key' => $meta_key, 'meta_value' => $state_id, 'fields' => 'ids' ) );
                                    foreach($saved_users as $saved_user){
                                        delete_user_meta( $saved_user, $meta_key );
                                    }

                                    /* Save Submitted User Selection */
                                    $submitted_users = $_POST['users'];

                                    foreach($submitted_users as $submitted_user){
                                        add_user_meta( $submitted_user, $meta_key, $state_id);
                                    }
                                                                        
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                }
                              
                                /* Get Saved Users */
                                $saved_users = get_users( array('blog_id' => 1, 'orderby' => 'nicename', 'meta_key' => $meta_key, 'meta_value' => $state_id, 'fields' => 'ids' ) );

                                /* Get All Users */
                                $blogusers = get_users( 'blog_id=1&orderby=nicename&role=subscriber' );
                            ?>
                            
                            <p class="txt-normal-s txt-medium margin-b-10">
                                Select users to assign
                            </p>
                            <select class="select-collaborators full-width" name="users[]" multiple="multiple">
                                <option 
                                    value="1"
                                    <?php echo ( in_array(1, $saved_users) ) ? "selected" : ""; ?>
                                >
                                    Super Admin
                                </option>
                            <?php foreach( $blogusers as $user ){ //delete_user_meta( $user->ID, $meta_key ); ?>

                                <option 
                                   value="<?php echo esc_html( $user->ID ); ?>"
                                   <?php echo ( in_array($user->ID, $saved_users) ) ? "selected" : ""; ?>
                                >
                                    <?php echo esc_html( $user->user_login ); ?>
                                </option>

                            <?php } ?>
                            </select>
                            <div class="text-right padding-t-20">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>
                </div>
               
            <?php } else { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        Manage NYSC National Validators
                    </h1>
                    <div class="text-right">
                        <a 
                            href="https://www.saedconnect.org/validation/manage-nysc-national-validators/?view=form" 
                            class="edit-btn"
                        >
                            Add/Edit
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Manage and assign NYSC Users to validation programs.
                    </p>
                </article>
                
                <div>
                   <style>
                       .author_avatar {
                            display: inline-block;
                            background-size: cover !important;
                            background-position: center !important;
                            background-repeat: no-repeat !important;
                            width: 1.5rem;
                            height: 1.5rem;
                            margin-right: 10px;
                            border-radius: 50%;
                        }
                    </style>
                    <?php 
                        /*
                        *
                        * Populate Form Data from Terms
                        *
                        */
                        //Get Terms
                        $saved_users = get_users( array('blog_id' => 1, 'orderby' => 'nicename', 'meta_key' => $meta_key, 'meta_value' => $state_id, 'fields' => 'ids' ) );
    
                        foreach ($saved_users as $saved_user) { //Cycle through terms, one at a time

                    ?>
                    
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-4 padding-lr-15">
                           <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($saved_user, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $saved_user, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );
                          
                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                          

                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                }   

                                restore_current_blog();
                            ?>
                            
                            
                            <a class="d-flex align-items-center" href="https://www.saedconnect.org/competency-profile/user-cv/?user-id=<?php echo $saved_user ?>">
                                <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                                </figure>
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php echo ($saved_user== 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </div>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                            
                        </div>                      
                    </div>
                    
                    <?php } ?>                  
                </div>
                <?php } ?>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>