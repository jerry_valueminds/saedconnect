    <?php get_header() ?>
    
    <main class="main-content">
        <h1 class="txt-xxlg txt-medium padding-o-80">
            Hello, This is a generic post page
        </h1>
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="container-wrapper">
            <?php the_content() ?>
            
            <div class="row row-40">
                <?php get_template_part( 'template-parts/content-block-template' ); ?>
            </div>
        </div>
        <?php endwhile; // end of the loop. ?>
    </main>

    <?php get_footer() ?>