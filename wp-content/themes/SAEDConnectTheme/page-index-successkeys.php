<?php /*Template Name: Homepage-SuccessKey*/ ?>
    
<?php get_header() ?>
   
<style>
    .course-image{
        height: 180px;
        background-position: center !important;
        background-size: cover !important;
        background-repeat: no-repeat !important;
    }
    
    .success-keys-card {
        padding: 20px 20px 40px;
    }
</style>
   
<main class="main-content">
    <header class="overview-header container-wrapper">
        <h1 class="txt-2em txt-color-dark txt-bold txt-height-1-2 padding-b-20">
            SuccessKeys
        </h1>
        <p class="txt-lg">
            Preparing you for the world of work. Completeing the Successkeys set of courses will help <br> you bridge the core skills gap required to get a great job.
        </p>
    </header>

    <!-- General Section -->
    <section class="padding-t-40 padding-b-40">
        <div class="container-wrapper">
            <div class="row row-15">
            <?php

                wp_reset_postdata();
                wp_reset_query();
                $temp = $wp_query; $wp_query= null;

                // Create Query Argument
                $args = array(
                    'post_type' => 'successkeys-course',
                    'showposts' => -1,
                );

                // Query
                $wp_query = new WP_Query($args);

                if ( $wp_query->have_posts() ) {

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Variables */
                    $post_id = $post->ID;    //Get Program ID
                    $images = rwmb_meta( 'successkeys-course-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                    $courseImage = $image['full_url'];

            ?>
                <div class="col-md-4 d-flex padding-lr-15 padding-b-30">
                    <a
                       class="card-shadow-5 success-keys-card d-block" href="#" style="border-color:<?php echo rwmb_meta( 'successkeys-course-theme-color' ); ?>"
                    >
                        <div class="content">
                            <figure
                                class="course-image margin-b-20"
                                style="background-image:url('<?php echo $courseImage; ?>')"   
                            >

                            </figure>
                            <header class="">
                                <div class="title">
                                    <?php echo rwmb_meta( 'successkeys-course-title' ); ?>
                                </div>
                            </header>
                            <article class="description">
                                <?php echo rwmb_meta( 'successkeys-course-description' ); ?>
                            </article>
                        </div>
                    </a>    
                </div>

            <?php
                endwhile;

                }else{
            ?>
                <div class="col-12">
                    <h2 class="txt-lg txt-medium">
                        No Courses found.
                    </h2>
                </div>   

            <?php

                }
            ?>
            </div>
        </div>
    </section>
</main>
    
<?php get_footer() ?>