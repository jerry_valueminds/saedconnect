    <?php /*Template Name: Homepage-Personal-Projects*/ ?>
    
    <?php get_header() ?>
    
    <?php
        /* Publication key for getting Admin Approved content */
        $publication_key = 'publication_status';
    ?>
   
    <main class="main-content">
        <header class="course-directory-banner">
            <div class="content container-wrapper txt-color-white">
                <h1 class="title">
                    Support a Project
                </h1>
            </div>
        </header>
        
        <section class="bg-grey padding-t-40 padding-b-10">
            <form id="filter-accordion" class="filter-accordion">                
                <?php
                    /* Filter Options Array */
                    $filter_items_array = array(
                        array(
                            'term_slug' =>'project-category',
                            'term_name' => 'Project Type'
                        ),
                    );
                ?>
                
                <!-- Accordion Header -->
                <div class="container-wrapper">
                    <div class="row row-5">
                    <?php
                        if($_GET){
                    ?>
                        <div class="padding-lr-5 padding-b-10">
                            <div class="txt-xs margin-b-5 txt-color-white">
                                Clear Filter
                            </div>
                            <a href="https://www.saedconnect.org/ventures-directory/personal-projects-directory/" class="btn btn-dark txt-normal-s">
                                <i class="fa fa-times"></i>
                                Clear Filter
                            </a>
                        </div>
                    <?php } ?>
                    
                    <?php foreach($filter_items_array as $filter_item){ ?>
                        <div class="text-center padding-lr-5 padding-b-10">
                            <div class="txt-xs txt-medium margin-b-5">
                                <?php echo $filter_item['term_name']; ?>
                            </div>
                            <button
                                id="heading-<?php echo $filter_item['term_slug']; ?>"
                                class="btn btn-trans-green dropdown-toggle txt-normal-s
                                    <?php
                                        if($_GET['s-'.$filter_item['term_slug']] != ''){
                                            echo 'bg-darkgrey';
                                        }
                                    ?>
                                "
                                type="button" data-toggle="collapse"
                                data-target="#filter-content-<?php echo $filter_item['term_slug']; ?>"
                                aria-expanded="false"
                                aria-controls="filter-content-<?php echo $filter_item['term_slug']; ?>"
                            >
                                <?php
                                    if($_GET[$filter_item['term_slug']] != ''){
                                        $term = get_term_by('id', $_GET[$filter_item['term_slug']], $filter_item['term_slug']);
                                        echo $term->name;
                                    } else {
                                        echo 'All';
                                    }
                                ?>
                            </button>
                        </div>
                    <?php } ?>
                        <div class="text-center padding-lr-5 padding-b-10">
                            <div class="txt-xs txt-medium margin-b-5">
                                Stage of Execution
                            </div>
                            <button
                                id="heading-execution-stage"
                                class="btn btn-trans-green dropdown-toggle txt-normal-s
                                    <?php
                                        if($_GET['stage'] != ''){
                                            echo 'bg-darkgrey';
                                        }
                                    ?>
                                "
                                type="button" data-toggle="collapse"
                                data-target="#filter-content-execution-stage"
                                aria-expanded="false"
                                aria-controls="filter-content-execution-stage"
                            >
                                <?php
                                    if($_GET['stage'] != ''){
                                        echo $_GET['stage'];
                                    } else {
                                        echo 'All';
                                    }
                                ?>
                            </button>
                        </div>
                        <div class="padding-lr-5 padding-b-10">
                            <div class="txt-xs margin-b-5 txt-color-white">
                                Go
                            </div>
                            <input class="btn btn-green txt-normal-s" type="submit" value="GO">
                        </div>
                    </div>
                </div>
                
                <!-- Accordion Content -->
                <div class="container-wrapper bg-grey">
                <?php foreach($filter_items_array as $filter_item){ ?>
                    <div
                        id="filter-content-<?php echo $filter_item['term_slug']; ?>"
                        class="collapse"
                        aria-labelledby="heading-<?php echo $filter_item['term_slug']; ?>"
                        data-parent="#filter-accordion"
                    >
                        <div class="custom-radio txt-sm padding-t-40 padding-b-30">
                            <label class="radio-item parent=term">
                                <input
                                    type="radio"
                                    name="<?php echo $filter_item['term_slug']; ?>"
                                    value=""
                                    <?php 
                                        if('' == esc_html($_GET[$term_slug])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    All
                                </span>
                            </label>
                        <?php 

                            //Get Terms
                            $terms = get_terms( $filter_item['term_slug'], array('hide_empty' => false, 'parent' => 0)); //Get all the terms

                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                // Check and see if the term is a top-level parent. If so, display it.
                                $parent = $term->parent;

                                $term_id = $term->term_id; //Get the term ID
                                $term_slug = $filter_item['term_slug'];
                                $term_name = $term->name; //Get the term name
                                $term_url = get_term_link($term);
                        ?>
                            
                                <label class="radio-item <?php echo ($parent == '0'  ? 'parent-term' : '') ?>">
                                    <input
                                        type="radio"
                                        name="<?php echo $filter_item['term_slug']; ?>"
                                        value="<?php echo $term_id ?>"
                                        <?php 
                                            if($term_id == esc_html($_GET[$term_slug])){
                                                echo 'checked';
                                            }
                                        ?>
                                    >
                                    <span class="text">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>
                        <?php

                            }

                        ?>
                        </div>
                    </div>
                <?php } ?>
                    <div
                        id="filter-content-execution-stage"
                        class="collapse"
                        aria-labelledby="heading-execution-stage"
                        data-parent="#filter-accordion"
                    >
                        <div class="custom-radio txt-sm padding-t-40 padding-b-30">
                            <label class="radio-item parent=term">
                                <input
                                    type="radio"
                                    name="stage"
                                    value=""
                                    <?php 
                                        if('' == esc_html($_GETT['stage'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    All
                                </span>
                            </label>
                            <label class="radio-item">
                                <input
                                    type="radio"
                                    name="stage"
                                    value="Idea"
                                    <?php 
                                        if('Idea' == esc_html($_GET['stage'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    Idea
                                </span>
                            </label>
                            <label class="radio-item">
                                <input
                                    type="radio"
                                    name="stage"
                                    value="I have a written plan"
                                    <?php 
                                        if('I have a written plan' == esc_html($_GET['stage'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    I have a written plan
                                </span>
                            </label>
                            <label class="radio-item">
                                <input
                                    type="radio"
                                    name="stage"
                                    value="I have started implementation"
                                    <?php 
                                        if('I have started implementation' == esc_html($_GET['stage'])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    I have started implementation
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                
            </form>
        </section>
        
        <!-- General Section -->
        <section class="container-wrapper padding-tb-40 margin-b-40">
            <div class="row row-20">
            <?php
                function truncate($string, $length){
                    if (strlen($string) > $length) {
                        $string = substr($string, 0, $length) . '...';
                    }

                    return $string;
                }
                
                if( $_GET['project-category'] ){
                    
                    $tax_array = array(
                        array (
                            'taxonomy' => 'project-category',
                            'field' => 'id',
                            'terms' => $_GET['project-category'],
                        )
                    );
                }
    
                $custom_query = new WP_Query();
                $custom_query->query( 
                    array(
                        'post_type' => 'personal-project',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'meta_query' => array(
                            array(
                                'key' => $publication_key,
                                'value' => 'admin_published'
                            )
                        ),
                        'tax_query' => $tax_array
                    ) 
                );

                if ( $custom_query->have_posts() ) {

                    while ($custom_query->have_posts()) : $custom_query->the_post();

                    /* Variables */
                    $post_id = $post->ID;    //Get Program ID

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            'key' => '2', 'value' => $post_id, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    $entry = $entries[0];
                    $entry_id = $entry['id'];

                    $project_stage = rgar( $entry, 6 );

                    if( $project_stage && $project_stage == $_GET['stage'] || !$_GET['stage'] ){
                            
            ?>
                
                <div class="col-md-3 padding-lr-10 padding-b-20 d-flex">
                    <a href="<?php the_permalink() ?>" class="personal-project-card">
                        <div class="top">
                            <div class="row row-5">
                                <div class="col-4">
                                    <?php
                                        $meta = rgar( $entry, '8' );
                                        $meta = str_ireplace( 'http:', 'https:', $meta );
                                    ?>
                                    <img src="<?php echo $meta; ?>" alt="">
                                </div>
                                <div class="col-8 padding-l-10">
                                    <div class="row">
                                        <div class="txt-color-blue txt-medium txt-height-1-1 margin-b-15">
                                            <?php the_title() ?>
                                        </div>
                                        <div class="txt-sm txt-height-1-4">
                                            <div class="txt-color-lighter">
                                                Project Stage
                                            </div>
                                           <div class="txt-medium">
                                                <?php echo $project_stage; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="txt-normal-s txt-color-lighter txt-height-1-7 margin-t-20">
                                <?php
                                    $meta = rgar( $entry, 4 );
                                    if($meta){
                                        echo truncate($meta, 70);
                                    }
                                ?>
                            </p>
                        </div>
                        <div class="bottom">
                            <ul class="category">
                            <?php
                                $meta = wp_get_post_terms($post_id, 'project-category', array("fields" => "names"));

                                if($meta){
                                    foreach($meta as $key=>$value) {
                            ?>
                                <li><?php echo $value; ?></li>
                            <?php
                                    }
                                }
                            ?>                                
                            </ul>
                        </div>
                    </a>
                </div>
                
                    <?php } ?>
                    
                <?php endwhile; ?>

            <?php } else { ?>
               
                <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                    <h2 class="txt-lg txt-medium">
                        No Projects found.
                    </h2>
                </div>   

            <?php } ?>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>