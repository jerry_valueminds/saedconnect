    <?php /*Template Name: Homepage - TEI Mentor*/ ?> 
       
    <?php get_header() ?>
    
    <style>
        .theme-bg{
            background-color: <?php echo $theme_color ?>;
        }
        
        .theme-alt{
            color: <?php echo $theme_alt_color ?>;
        }
        
        .how-it-works-container::before{
            background-color: <?php echo $theme_color ?>;
        }
    </style>

    <main class="main-content">
        <?php
            while ( have_posts() ) : the_post();
                /* Get Program Information */
                $main_program_id = $post->ID;
                $program_name = get_the_title();
                $program_url = get_the_permalink();
                $program_mentor_forum_url = rwmb_meta( 'skilli-forum-url' );

                $how_it_works_brief = rwmb_meta( 'skilli-program-hiw-brief', array(), $main_program_id );
                $how_it_works = rwmb_meta( 'skilli_program_hiw_group', array(), $program_id );
                $what_you_get = rwmb_meta( 'skilli_program_wyg_group', array(), $program_id );
                $program_faq = rwmb_meta( 'skilli-program-faq' );

                /* Include Skilli Navigation */
                get_template_part( 'template-parts/navigation-skilli-secondary' );

            endwhile; // end of the loop.
        ?>
        
        <?php 
        
        $blog_id = get_current_blog_id();
        
        /* How it Works */
        $how_it_works = array(
                /* TEI */
                '90' => array(
                    'summary' => 'The Entrepreneurship. Incubator utilizes an innovative model to deliver game changing support to help you start your own business from anywhere you are. Here is how you can plug in.',
                    'items' => array(
                        array(
                            'title' => 'Join a track of your choice',
                            'content' => 'Select the “Get started Button” under the track of your choice and complete the enrolment form. You will get an email within 24-48hrs to confirm your enrolment, based on space availability.'
                        ),
                        array(
                            'title' => 'Get added to your virtual class',
                            'content' => 'All classes are on WhatsApp and are coordinated by strict rules. Details of class rules will be sent to you'
                        ),
                        array(
                            'title' => 'Learn & Engage',
                            'content' => 'Learn from audio, video & text lessons, participate in quizzes & complete your action ta'
                        ),
                        array(
                            'title' => 'Join the Alumni Community',
                            'content' => 'Your learning journey doesnt end after class. We’ll give you 3 month access to our alumni community where you can meet and interact with mentors and other entrepreneurs.'
                        )
                    )
                ),

                /* JA */
                '88' => array(
                    'summary' => 'The Job Advisor utilizes an innovative model to deliver game changing support to help you get your dream job, from anywhere you are. Here is how you can plug in.',
                    'items' => array(
                        array(
                            'title' => 'Access the Knowledge Center',
                            'content' => 'Click the "Explore button" under the track to see all the topics covered in the knowledge center under that track.'
                        ),
                        array(
                            'title' => 'Learn from Mentors',
                            'content' => 'Select a topic of your interest to access & digest the wisdom contributed by all our mentors under that topic.'
                        ),
                        array(
                            'title' => 'Ask questions in the Mentor Hub',
                            'content' => 'Ask any questions bothering you. Seasoned experts are waiting to provide any clarifications you require to convert lessons learnt into action.'
                        ),
                        array(
                            'title' => 'Engage till you are satisfied',
                            'content' => 'Push your conversation with the experts until you are completely satisfied.'
                        )
                    )
                ),

                /* BC */
                '26' => array(
                    'summary' => 'The Business utilizes an innovative model to help you grow your business and get past any challenges, from anywhere you are. Here is how you can plug in.',
                    'items' => array(
                        array(
                            'title' => 'Subscribe to the an Expert Group',
                            'content' => 'Join the expert group of your interest.'
                        ),
                        array(
                            'title' => 'Ask your Question',
                            'content' => 'Ask any questions bothering you. Seasoned experts are waiting to hear from you and give you customized answers that meet your need.'
                        ),
                        array(
                            'title' => 'Engage till you are satisfied',
                            'content' => 'Push your conversation with the experts until you are completely satisfied.'
                        )
                    )
                ),
            );

            /* What You get */
            $what_you_get = array(
                /* TEI */
                '90' => array(
                    'summary' => 'We are going all out to ensure you get all the support & resources you require to step out, discover great ideas & build an amazing business.',
                    'items' => array(
                        array(
                            'title' => 'Tap into the wisdom & experience of a lot of mentors',
                            'content' => 'We get a lot of mentors to contribute their expert knowledge as well as their practical experience to each and every topic in the class, so that you can see that topic from different points of view.'
                        ),
                        array(
                            'title' => 'Get customised answers to your questions',
                            'content' => 'Ask as many questions as you have. The experts in the class will provide answers personalised to your specific questions. You can keep asking questions till you are satisfied, and if you get stuck again, they are always there to help.'
                        ),
                        array(
                            'title' => 'Get inspired by your peers',
                            'content' => 'Meet, learn from & get inspired by other focused peers in the class. When you are among a company of focused peers, your chances of success increases drastically.'
                        ),
                        array(
                            'title' => 'Get access to growth support tools, templates & special opportunities.',
                            'content' => 'We continuously seek out and share tools, resources, opportunities and programs for our alumni that would make your journey easier.'
                        )
                    )
                ),

                /* JA */
                '88' => array(
                    'summary' => 'We are going all out to ensure you get all the support & resources you require to stand out among your peers and get your dream jobs.',
                    'items' => array(
                        array(
                            'title' => 'Tap into the wisdom & experience of a lot of mentors',
                            'content' => 'We get a lot of mentors to contribute their expert knowledge as well as their practical experience to each and every topic in the knowledge center, so that you can see that topic from different points of view.'
                        ),
                        array(
                            'title' => 'Get customised answers to your questions',
                            'content' => 'The experts in the mentor hub will provide answers personalised to your specific questions. You can keep asking questions till you are satisfied, and if you get stuck again, they are always there to help.'
                        ),
                        array(
                            'title' => 'Get inspired by your peers',
                            'content' => 'Meet, learn from & get inspired by other focused peers in the mentor hub. When you are among a company of focused peers, your chances of success increases drastically.'
                        ),
                        array(
                            'title' => 'Get access to growth support tools, templates & special opportunities.',
                            'content' => 'We continuously seek out and share tools, resources and programs that would make your journey easier and even negotiate special concessions for members of the mentor Hub.'
                        )
                    )
                ),

                /* BC */
                '26' => array(
                    'summary' => 'We are going all out to ensure you get all the support & resources you require to get past any business challenges and grow',
                    'items' => array(
                        array(
                            'title' => 'Tap into the wisdom & experience of a lot of mentors',
                            'content' => 'We get a lot of mentors to contribute their expert knowledge as well as their practical experience to each and every topic in the knowledge center, so that you can see that topic from different points of view.'
                        ),
                        array(
                            'title' => 'Get customised answers to your questions',
                            'content' => 'The experts in the mentor hub will provide answers personalised to your specific questions. You can keep asking questions till you are satisfied, and if you get stuck again, they are always there to help.'
                        ),
                        array(
                            'title' => 'Get inspired by your peers',
                            'content' => 'Meet, learn from & get inspired by other focused peers in the mentor hub. When you are among a company of focused peers, your chances of success increases drastically.'
                        ),
                        array(
                            'title' => 'Get access to growth support tools, templates & special opportunities.',
                            'content' => 'We continuously seek out and share tools, resources and programs that would make your journey easier and even negotiate special concessions for members of the mentor Hub.'
                        )
                    )
                ),
            );
        ?>
        
        <!-- Intro -->
        <section class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h2 class="d-inline-block bg-orange-2 txt-color-white txt-xlg emtxt-bold padding-o-10 margin-b-20">
                        The Entrepreneurship Incubator
                    </h2>
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        Start your business from anywhere you are
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        The entrepreneurship incubator offers you step by step guidance on the process of discovering, building and launching your business & connects you to mentors, collaborators, investors and opportunities that increase your chances of success.
                    </h2>
                </div>
            </div>
        </section>

        <!-- Tracks -->
        <section class="bg-orange-2 padding-tb-40">
            <div class="container-wrapper">
                <header class="txt-light margin-b-30 txt-color-white">

                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Tracks
                    </h2>
                    <p class="txt-height-1-">
                        Join any of the tracks below that best aligns with where you are on your entrepreneurial Journey.
                        <br>
                        Each track is dedicated to helping you through a specific phase of your journey. 
                    </p>
                </header>
                <?php // Display posts
                    $program_query = new WP_Query( array(
                        'post_type' => 'track',
                        'posts_per_page' => -1
                    ) );
                    while ( $program_query->have_posts() ) : $program_query->the_post();
                
                            $program_id = $post->ID;    //Get Program ID
                            //  Get Program Featured Image
                            $program_images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ) );
                            $program_images = reset( $program_images );
                ?>
                           
                
                <div class="overflow-hidden card-shadow-2 margin-b-40">
                   
                    <div class="row">
                        <div class="col-md-8 order-2 order-md-1 bg-white">
                            <div class="padding-o-30">
                                <h2 class="txt-normal-s txt-mediym uppercase margin-b-10">
                                    <a href="<?php the_permalink() ?>">
                                        <span>
                                            <?php the_title() ?>
                                        </span> 
                                        <span class="padding-lr-5">|</span> 
                                        <span>
                                           <span class="txt-bold">Next Date:</span>
                                           <?php 
                                                $date = strtotime( rwmb_meta( 'program-next-start-date' ) );
                                                echo date('j F Y', $date);
                                            ?>
                                        </span> 
                                    </a>
                                </h2>
                                <h2 class="txt-xlg txt-medium txt-height-1-2 padding-b-20">
                                    <?php echo rwmb_meta( 'program-name' ); ?>
                                </h2>
                                <article class="text-box sm">
                                    <?php echo rwmb_meta( 'program-description' ); ?>
                                </article>
                                <!--<div class="margin-t-30">
                                    <a class="btn btn-trans-bw txt-normal-s" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                        Get started
                                    </a>
                                </div>-->
                                
                                <!-- Action Buttons for Web -->
                                <div 
                                   class="d-none d-md-block row justify-content-between margin-t-20 padding-t-20 border-t-1 border-color-darkgrey"
                                >
                                    <style>
                                        button[aria-expanded="false"]{
                                            background-color: transparent;
                                            border-color: black;
                                            color: black;
                                        }

                                        .btn-trans-bw:hover, button[aria-expanded="true"]{
                                            background-color: grey;
                                            border-color: grey;
                                            color: white;
                                        }
                                    </style>
                                   
                                    <div class="col-md-6 d-flex align-items-center">

                                        <?php
                                            /* Check if user is loggedin */

                                            if ( /*is_user_logged_in()*/ true ) {
                                        ?>

                                            <!--<a 
                                                class="btn btn-trans-wb txt-sm" 
                                                href="<?php echo get_the_permalink().'?view=form' ?>"
                                            >
                                                Continue
                                            </a>-->

                                            <a
                                               class="btn btn-trans-bw txt-sm margin-r-10 no-m-b"
                                               href="<?php the_permalink() ?>"
                                            >
                                                Get Started
                                            </a>
                                            <!--<p class="txt-medium padding-l-15">
                                                Duration: <?php echo rwmb_meta( 'program-duration' ); ?>
                                            </p>-->

                                        <?php
                                            } else {
                                        ?>

                                            <a
                                               class="btn btn-trans-bw txt-sm margin-r-10"
                                               href="https://www.saedconnect.org/login"
                                            >
                                                Get Started
                                            </a>

                                        <?php

                                            }
                                        ?>
                                       
                                        <!--<button 
                                            class="btn btn-trans-bw txt-sm" 
                                            type="button" 
                                            data-toggle="collapse" 
                                            data-target="#program-<?php echo $program_id ?>-2" 
                                            aria-expanded="false" 
                                            aria-controls="collapseExample"
                                        >
                                            Request Mentor
                                        </button>-->
                                       <!-- <button 
                                            class="btn btn-trans-bw txt-sm scroll-to" 
                                        >
                                            Talk to a Mentor
                                        </button>-->
                                    </div>
                                </div>
                                
                                <!-- Action Buttons Mobile -->
                                <div 
                                   class="d-md-none row justify-content-between margin-t-20 padding-t-20 border-t-1 border-color-darkgrey"
                                >
                                    <style>
                                        button[aria-expanded="false"]{
                                            background-color: transparent;
                                            border-color: black;
                                            color: black;
                                        }

                                        button[aria-expanded="true"]{
                                            background-color: grey;
                                            border-color: grey;
                                            color: white;
                                        }
                                    </style>
                                   
                                    <div class="col-md-6">
                                    <?php
                                        /* Check if user is loggedin */

                                        if ( /*is_user_logged_in()*/ true ) {
                                    ?>

                                        <!--<a 
                                            class="btn btn-trans-wb txt-sm" 
                                            href="<?php echo get_the_permalink().'?view=form' ?>"
                                        >
                                            Continue
                                        </a>-->

                                        <a
                                           class="btn btn-trans-bw txt-sm margin-r-10"
                                           href="<?php the_permalink() ?>"
                                        >
                                            Get Started
                                        </a>
                                        <!--<p>
                                            Duration 1 Week
                                        </p>-->

                                    <?php
                                        } else {
                                    ?>

                                        <a
                                           class="btn btn-trans-bw txt-sm margin-r-10"
                                           href="https://www.saedconnect.org/login"
                                        >
                                            Get Started
                                        </a>

                                    <?php

                                        }
                                    ?>
                                        <!--<button 
                                            class="btn btn-trans-bw txt-sm scroll-to" 
                                        >
                                            Talk to a Mentor
                                        </button>-->
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        

                        <div class="col-md-4 order-1 order-md-2 generic-card-bg" 
                           style="background-image:url('<?php echo $program_images['full_url']; ?>');"
                        >
                            <div class="cta text-center">
                                <!--<div>
                                    <button type="button" class="btn btn-blue" data-toggle="modal" data-target="#formModal">
                                        Enroll for free online
                                    </button>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-white" data-toggle="modal" data-target="#smsEnrollModal-<?php echo $program_id ?>">
                                        Enroll for free via SMS
                                    </button>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    
                    <div class="faq-collapse border-t-1 border-color-darkgrey" id="accordion-<?php echo $program_id ?>">
                        <div
                           class="faq-collapse collapse bg-white" 
                           id="program-<?php echo $program_id ?>" 
                           data-parent="#accordion-<?php echo $program_id ?>"
                        >
                            <div class="padding-lr-80">
                            <?php
                                /* Get Group Values */
                                $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                if ( ! empty( $group_values ) ) {
                            ?>
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Curriculum
                                </h2>
                                    
                            <?php
                                    foreach ( $group_values as $group_value ) {
                            ?>
                               
                                    <!-- Article Group -->
                                    <div class="padding-tb-20 border-t-1 border-color-darkgrey">
                                       
                                    <?php if( $group_value['group-title-url'] ){ ?>
                                        
                                        <h3>
                                            <a
                                               href="<?php echo $group_value['group-title-url']; ?>"
                                               class="txt-color-dark d-flex align-items-center"
                                            >
                                                <span class="txt-xs padding-r-5">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                </span>
                                                <?php echo $group_value['group-title']; ?>
                                            </a>
                                        </h3>
                                        
                                    <?php } else { ?>
                                       
                                        <h3>
                                            <a
                                               href="<?php echo $group_value['group-title-url']; ?>"
                                               class="txt-color-dark d-flex align-items-center"
                                            >
                                                <span class="txt-xs padding-r-5">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                </span>
                                                <?php echo $group_value['group-title']; ?>
                                            </a>
                                        </h3>

                                    <?php } ?>
                                        
                                    <?php 
                                        $values = $group_value['forum-topics-group']['forum-topic-item'];
                                            
                                            if($values){
                                    ?>
                                        <ul class="row row-10 icon-list black txt-normal-s margin-t-20 padding-l-20">
                                        <?php 
                                            foreach ( $values as $value ) { 
                                        ?>
                                           
                                            <li class="col-md-3 padding-lr-10">
                                                <a href="<?php echo $value['forum-topic-url']; ?>">
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        <?php echo $value['forum-topic-title']; ?>
                                                    </span>
                                                </a>
                                            </li>
                                        
                                        <?php } ?>
                                        
                                        </ul>
                                        
                                    <?php } ?>
                                        
                                    </div>

                                <?php
                                    }
                                }
                            ?>
                            </div>
                        </div>

                        <div 
                           class="faq-collapse collapse bg-white" 
                           id="program-<?php echo $program_id ?>-2" 
                           data-parent="#accordion-<?php echo $program_id ?>"
                        >
                            <div class="padding-lr-80">
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Subscribe to the Mentor forum
                                </h2>
                                <p class="margin-b-40">
                                    Lorem animi maxime saepe impedit, natus. Vitae magnam nisi nostrum quaerat non tenetur quis!
                                </p>

                                <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                <?php
                                    $group_values = rwmb_meta( 'subscription-block-group' );


                                    if ( ! empty( $group_values ) ) {

                                        foreach ( $group_values as $group_value ) {

                                        /* Get Content type */
                                        $content_type = $group_value['select-subscription-type'];

                                            /* Check Subscription Type */
                                            if($content_type == 'paid-subscription-meta-box'){
                                ?>
                                    <!-- Paid Subscription -->
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            <?php
                                                echo $group_value['subscription-title'];
                                            ?>
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            <?php
                                                echo $group_value['paid-subscription-meta-box']['paid-subscription-description'];
                                            ?>
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            <?php
                                                echo $group_value['paid-subscription-meta-box']['paid-subscription-fee'];
                                            ?>
                                        </div>
                                        <div>
                                            <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                                                <?php
                                                    echo $group_value['subscription-button-text'];
                                                ?>
                                            </a>
                                        </div>
                                    </div>

                                <?php } elseif ($content_type == 'image-subscription-meta-box') { ?>

                                <?php
                                    /* Get Image */
                                    //$images = rwmb_meta( 'mini_site_feature_image', array( 'limit' => 1 ) );
                                    $images = $group_value['image-subscription-meta-box']['image-subscription-image'];
                                    $image = $images[0];
                                ?>

                                    <!-- Image -->
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            <?php
                                                echo $group_value['subscription-title'];
                                            ?>
                                        </h3>
                                        <div class="margin-b-20">
                                            <img src="<?php echo  wp_get_attachment_image_url( $image, 'size' ); ?>" alt="">
                                        </div>
                                        <p class="txt-sm margin-b-20">
                                            <?php
                                                echo $group_value['paid-subscription-meta-box']['paid-subscription-description'];
                                            ?>
                                        </p>
                                        <div>
                                            <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                                                <?php
                                                    echo $group_value['subscription-button-text'];
                                                ?>
                                            </a>
                                        </div>
                                    </div>

                                <?php } elseif ($content_type == 'text-subscription-meta-box') { ?> 

                                    <!-- Text -->
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            <?php
                                                echo $group_value['subscription-title'];
                                            ?>
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            <?php
                                                echo $group_value['text-subscription-meta-box']['text-subscription-content'];
                                            ?>
                                        </p>
                                        <div>
                                            <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                                                <?php
                                                    echo $group_value['subscription-button-text'];
                                                ?>
                                            </a>
                                        </div>
                                    </div>

                                <?php
                                            }
                                        }
                                    }
                                ?>   
                                </div>

                                <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                    <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
                                        Are you a Corp Member?
                                    </h2>
                                    <p class="col-md-7 padding-lr-20 txt-sm">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                    </p>
                                    <div class="col-md-2 padding-lr-20">
                                        <a href="" class="btn btn-green txt-xs full-width no-m-b">
                                            Learn more
                                        </a>
                                    </div>
                                </div>
                                <div class="row row-20 padding-b-40">
                                    <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
                                        Already Subscribed?
                                    </h2>
                                    <p class="col-md-7 padding-lr-20 txt-sm">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                    </p>
                                    <div class="col-md-2 padding-lr-20">
                                        <a href="" class="btn btn-green txt-xs full-width no-m-b">
                                            Sign up
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- Curriculum Modal -->
                <div class="modal fade font-main shc-modal" id="curriculumModal-<?php echo $program_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="container-wrapper">
                                <div class="col-md-10 mx-auto">
                                <?php
                                    /* Get Group Values */
                                    $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                    if ( ! empty( $group_values ) ) {
                                ?>
                                    <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                        Curriculum
                                    </h2>

                                <?php
                                        foreach ( $group_values as $group_value ) {
                                ?>

                                        <!-- Article Group -->
                                        <div class="padding-tb-20 border-t-1 border-color-darkgrey">

                                        <?php if( $group_value['group-title-url'] ){ ?>

                                            <h3>
                                                <a
                                                   href="<?php echo $group_value['group-title-url']; ?>"
                                                   class="txt-color-dark d-flex align-items-center"
                                                >
                                                    <span class="txt-xs padding-r-5">
                                                        <i class="fa fa-long-arrow-right"></i>
                                                    </span>
                                                    <?php echo $group_value['group-title']; ?>
                                                </a>
                                            </h3>

                                        <?php } else { ?>

                                            <h3>
                                                <a
                                                   href="<?php echo $group_value['group-title-url']; ?>"
                                                   class="txt-color-dark d-flex align-items-center"
                                                >
                                                    <span class="txt-xs padding-r-5">
                                                        <i class="fa fa-long-arrow-right"></i>
                                                    </span>
                                                    <?php echo $group_value['group-title']; ?>
                                                </a>
                                            </h3>

                                        <?php } ?>

                                        <?php 
                                            $values = $group_value['forum-topics-group']['forum-topic-item'];

                                                if($values){
                                        ?>
                                            <ul class="row row-10 icon-list black txt-normal-s margin-t-20 padding-l-20">
                                            <?php 
                                                foreach ( $values as $value ) { 
                                            ?>

                                                <li class="col-md-3 padding-lr-10">
                                                    <a href="<?php echo $value['forum-topic-url']; ?>">
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            <?php echo $value['forum-topic-title']; ?>
                                                        </span>
                                                    </a>
                                                </li>

                                            <?php } ?>

                                            </ul>

                                        <?php } ?>

                                        </div>

                                    <?php
                                        }
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Mentor Modal -->
                <div class="modal fade font-main shc-modal" id="mentorModal-<?php echo $program_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="padding-lr-40 padding-t-40">
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Subscribe to the Mentor forum
                                </h2>
                                <p class="margin-b-40">
                                    Lorem animi maxime saepe impedit, natus. Vitae magnam nisi nostrum quaerat non tenetur quis!
                                </p>
                                
                                <div class="row row-20 padding-b-40">
                                    <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
                                        Already Subscribed?
                                    </h2>
                                    <p class="col-md-7 padding-lr-20 txt-sm padding-b-20">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                    </p>
                                    <div class="col-md-2 padding-lr-20">
                                        <a href="" class="btn btn-green txt-xs full-width no-m-b">
                                            Sign up
                                        </a>
                                    </div>
                                </div>
                            
                                <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            1 Month
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            N1000.00
                                        </div>
                                        <div>
                                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                                Subscribe
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            3 Months
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            N1000.00
                                        </div>
                                        <div>
                                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                                Subscribe
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            6 Months
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            N1000.00
                                        </div>
                                        <div>
                                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                                Subscribe
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            1 Year
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            N1000.00
                                        </div>
                                        <div>
                                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                                Subscribe
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                    <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
                                        Are you a Corp Member?
                                    </h2>
                                    <p class="col-md-7 padding-lr-20 txt-sm padding-b-20">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                    </p>
                                    <div class="col-md-2 padding-lr-20">
                                        <a href="" class="btn btn-green txt-xs full-width no-m-b">
                                            Learn more
                                        </a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </section>

        <!-- How it works -->
        <section class="padding-t-80 padding-b-60">
            <div class="container-wrapper">
                <div class="row text-center">
                    <div class="col-md-10 mx-auto">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-20 ">
                            How it Works
                        </h1>
                        <p class="txt-normal-s margin-b-60 text-center">
                            The Entrepreneurship. Incubator utilizes an innovative model to deliver game changing support to help you start your own business from anywhere you are. Here is how you can plug in. </p>
                    </div>
                </div>
            </div>
            <div class="container-wrapper how-it-works-container">
                <div class="row text-center">
                    <div class="col-md-10 mx-auto">
                        <div class="row row-20 ">

                            <div class="col-md padding-lr-20">
                                <div class="how-card">
                                    <div class="number">
                                        <span>1</span>
                                    </div>
                                    <div class="content">
                                        <h3 class="txt-medium txt-height-1-2 margin-b-5">Join a track of your choice</h3>
                                        <p class="txt-sm txt-height-1-7">Select the “Get started Button” under the track of your choice and complete the enrolment form. You will get an email within 24-48hrs to confirm your enrolment, based on space availability.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md padding-lr-20">
                                <div class="how-card">
                                    <div class="number">
                                        <span>2</span>
                                    </div>
                                    <div class="content">
                                        <h3 class="txt-medium txt-height-1-2 margin-b-5">Get added to your virtual class</h3>
                                        <p class="txt-sm txt-height-1-7">All classes are on WhatsApp and are coordinated by strict rules. Details of class rules will be sent to you</p>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md padding-lr-20">
                                <div class="how-card">
                                    <div class="number">
                                        <span>3</span>
                                    </div>
                                    <div class="content">
                                        <h3 class="txt-medium txt-height-1-2 margin-b-5">Learn & Engage</h3>
                                        <p class="txt-sm txt-height-1-7">Learn from audio, video & text lessons, participate in quizzes & complete your action ta</p>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md padding-lr-20">
                                <div class="how-card">
                                    <div class="number">
                                        <span>4</span>
                                    </div>
                                    <div class="content">
                                        <h3 class="txt-medium txt-height-1-2 margin-b-5">Join the Alumni Community</h3>
                                        <p class="txt-sm txt-height-1-7">Your learning journey doesnt end after class. We’ll give you 3 month access to our alumni community where you can meet and interact with mentors and other entrepreneurs.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- What you get -->
        <section class="bg-grey padding-t-80 padding-b-60">
            <div class="container-wrapper text-center">
                <div class="row"> 
                    <div class="col-md-8 mx-auto">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-20">
                            What you get
                        </h1>
                        <p class="txt-normal-s margin-b-60">
                            <?php echo $what_you_get[$blog_id]['summary']; ?>
                        </p>
                        <div class="row row-20">
                        <?php
                            $group_values = $what_you_get[$blog_id]['items'];

                            if ( ! empty( $group_values ) ) {
                                foreach ( $group_values as $group_value ) {
                        ?>
                            <div class="col-md-6 padding-lr-20 d-flex">
                                <div class="bg-white collapsible-card">
                                    <div class="header">
                                        <div class="cta"></div>
                                        <div class="title">
                                            <?php echo $group_value[ 'title' ]; ?>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="wrapper">
                                            <div class="margin-b-20">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_mentor.png" alt="" width="100">
                                            </div>
                                            <h3 class="txt-medium margin-b-20">
                                                <?php echo $group_value[ 'title' ]; ?>
                                            </h3>
                                            <p class="txt-sm txt-height-1-7">
                                                <?php echo $group_value[ 'content' ]; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                                }
                            }
                        ?>
                        </div>
                    </div>  
                </div>
                <?php if ( $program_id == 649 ) { ?>
                <div class="margin-t-20">
                    <h2 class="txt-normal-s txt-height-1-7">
                        Get started in the <?php echo $track_title ?> knowledge Center for free.
                    </h2>
                    <div class="margin-t-20">
                        <button class="btn btn-green txt-sm no-m-b scroll-to">
                            <?php echo $cta_message[$program_id]; ?>
                        </button>
                    </div>
                </div>
                <?php } ?>
            </div>
        </section>

        <!-- Meet Your mentors -->
        <!--<section class="padding-tb-80">
            <div class="container-wrapper text-center">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <h1 class="txt-xlg txt-height-1-1 margin-b-20">
                            Meet your Mentors
                        </h1>
                        <p class="txt-normal-s margin-b-60">
                            We are constantly reaching out to the best of the best professionals &amp; experts to help you make progress.
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <div class="row row-20">

                             Mentor 
                            <div class="col-md-2 padding-lr-20 padding-b-40 d-flex">
                                <div class="mentor-card">
                                    <div class="image-box">
                                        <img src="http://www.saedconnect.org/growth-programs/wp-content/uploads/sites/20/gravity_forms/57-91de47f32ee1c9b6601835326fc41527/2019/02/o_tj.jpg" alt="">
                                    </div>
                                    <div class="name">
                                        <a data-toggle="modal" href="#mentor-1030">
                                            Franklin </a>
                                    </div>
                                    <div class="position">
                                        Frankeinstein Designs </div>
                                </div>
                            </div>

                             Mentor Modal 
                            <div class="modal fade font-main coming-soon-modal" id="mentor-1030" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content padding-o-40">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <div class="text-center">
                                            <div class="margin-b-10">
                                                <img src="http://www.saedconnect.org/growth-programs/wp-content/uploads/sites/20/gravity_forms/57-91de47f32ee1c9b6601835326fc41527/2019/02/o_tj.jpg" width="120" class="profile-image">
                                            </div>
                                            <div class="txt-normal-s margin-b-10">
                                                Mentor
                                            </div>
                                            <h4 class="txt-xlg txt-medium margin-b-10">
                                                Franklin </h4>
                                            <h5 class="txt-medium uppercase txt-sm margin-b-20">
                                                Art Deirector @ Frankeinstein Designs </h5>
                                            <p class="txt-sm txt-height-1-7 margin-b-20">
                                                Loose checking returns some crazy, counter-intuitive results when used with certain arrays. It is completely correct behaviour, due to PHP's leniency on variable types, but in "real-life" is almost useless. </p>
                                            <div class="txt-xlg">
                                                <a href="" class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey" target="_blank">
                                                    <i class="fa fa-facebook fa-fw"></i>
                                                </a>

                                                <a href="" class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey" target="_blank">
                                                    <i class="fa fa-twitter fa-fw"></i>
                                                </a>

                                                <a href="" class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey" target="_blank">
                                                    <i class="fa fa-linkedin fa-fw"></i>
                                                </a>
                                            </div>
                                            <div class="padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Mentors on
                                                </div>
                                                <p class="txt-sm">
                                                    The Entrepreneurship Incubator, Job Advisor, </p>
                                            </div>
                                            <div class="border-t-1 border-color-darkgrey padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Small Business Mentorship area
                                                </div>
                                                <p class="txt-sm">
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <h2 class="txt-normal-s txt-height-1-7" id="scroll-to-me">
                            Have questions? Check out the
                            Help Center
                            .
                        </h2>
                        <div class="margin-t-20">
                            <a href="https://www.saedconnect.org/growth-programs/mentor-directory/" class="btn btn-green txt-sm no-m-b">
                                See Full List
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->

        <!-- Pay -->
        <!--<section class="bg-grey padding-t-80 padding-b-80" id="scroll-to-me">
            <div class="container-wrapper row">
                <div class="row"> 
                    <div class="col-md-8 mx-auto">
                        <h2 class="text-center margin-b-5">
                            Want to speak to someone? Need Special help?
                        </h2>
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-60 text-center">
                            Engage with Expert Mentors waiting to help you
                        </h1>
                        <p class="margin-b-60 text-center">
                            <?php echo rwmb_meta( 'skilli-program-subscription-brief', array(), $main_program_id ); ?>
                        </p>
                        <div class="row">
                            <div class="col-md-9 mx-auto bg-orange-2 txt-color-white text-center text-md-left">
                                <div class="padding-o-30">
                                    <div class="row row-10 overflow-hidden">
                                        <div class="col-md-6 padding-lr-20 payment-border border-color-darkgrey margin-b-10 padding-b-10">
                                            <h3 class="txt-xxlg txt-bold margin-b-5">
                                                Just N10,000 / Qtr
                                            </h3>
                                            <p class="txt-sm">
                                                To join <?php echo $program_name ?>
                                                <br>
                                                Mentor Hub
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-20">
                                            <p class="txt-normal-s">
                                                We charge a small fee to cover our costs, maintain expert team & sustain a high quality moderated Mentor Hub.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 mx-auto">
                                <ul class="con-list black txt-normal-s margin-t-40">
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Tap into the wisdom & experience of a lot of mentors
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get customised answers to your questions
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get inspired by your peers
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get access to growth support tools, templates & special opportunities.
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Engage with Experts and Mentors until you are satisfied.
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="text-center margin-t-40">
                            <a href="<?php echo $program_mentor_forum_url; ?>" class="btn btn-green txt-sm">
                                Subscribe
                            </a>
                        </div>
                      </div>  
                </div>
            </div>
        </section>-->
        
        <!-- FAQ -->
        <section class="container-wrapper padding-tb-60">
            <header class="margin-b-60 text-center">
                <h2 class="txt-xxlg txt-light padding-b-20">Frequently Asked Questions</h2>
                <p>
                    Got questions? Require clarifications? Find your answers here
                </p>
            </header>
            <div class="row row-10">
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-1" aria-expanded="false" aria-controls="collapseExample">
                            What is The entrepreneurship incubator? </button>
                        <div class="collapse" id="faq-1">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        The entrepreneurship incubator is a program with the goal of helping start-ups to grow their businesses by providing mentorship, guidance and opportunities. </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-2" aria-expanded="false" aria-controls="collapseExample">
                            When is the right time to join these programs? </button>
                        <div class="collapse" id="faq-2">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        You can join the entrepreneurship incubator at any time or stage you are in business. </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-3" aria-expanded="false" aria-controls="collapseExample">
                            Can I join the program from anywhere? </button>
                        <div class="collapse" id="faq-3">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        You can join The Entrepreneurship Incubator from anywhere. </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-4" aria-expanded="false" aria-controls="collapseExample">
                            Must I follow all the tracks in sequences? </button>
                        <div class="collapse" id="faq-4">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        The tracks are presented in sequence, however you are free to follow the tracks depending on what stage of entrepreurship you in </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-5" aria-expanded="false" aria-controls="collapseExample">
                            Is the program open to people with any types of business ideas? </button>
                        <div class="collapse" id="faq-5">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        There is no restriction to any type of business ideas or owners who can join The Entrepreneurship Incubator. </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-6" aria-expanded="false" aria-controls="collapseExample">
                            Am finding it difficult to choose the right business idea, can the incubator help? </button>
                        <div class="collapse" id="faq-6">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        The incubator provides guilds to help you analyze your ideas and determine which business venture to pursue. </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-7" aria-expanded="false" aria-controls="collapseExample">
                            Can the program help me refine my ideas? </button>
                        <div class="collapse" id="faq-7">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        The incubator has guilds and mentor support that will help you refine your business ideas. </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php
            /* Include Skilli Footer */
            get_template_part( 'template-parts/footer-skilli' );
        ?>
    </main>
    <script>
        /* Scroll to view */
        $(".scroll-to").click(function(){
            var elmnt = document.getElementById("scroll-to-me");
            elmnt.scrollIntoView({behavior: "smooth", block: "start", }); // Top
        });
    </script>

    <?php get_footer() ?>
    