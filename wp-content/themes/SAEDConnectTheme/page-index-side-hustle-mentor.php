    <?php /*Template Name: Homepage - Side Hustle Skills Mentor*/ ?>     
    
    <?php get_header() ?>
    
    <?php
        while ( have_posts() ) : the_post();
            /* Get Program Information */
            $main_program_id = $post->ID;
            $program_name = get_the_title();
            $program_url = get_the_permalink();

            $how_it_works_brief = rwmb_meta( 'skilli-program-hiw-brief', array(), $main_program_id );
            $how_it_works = rwmb_meta( 'skilli_program_hiw_group', array(), $program_id );
            $what_you_get = rwmb_meta( 'skilli_program_wyg_group', array(), $program_id );
            $program_faq = rwmb_meta( 'skilli-program-faq' );

            /* Include saedconnect Navigation */
            get_template_part( 'template-parts/navigation-saedconnect' );
        endwhile; // end of the loop.
    ?>
   
    <main class="main-content">
       
        <!-- Intro -->
        <section class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h2 class="d-inline-block bg-yellow-dark txt-color-white txt-xlg emtxt-bold padding-o-10 margin-b-20">
                        <?php the_title() ?>
                    </h2>
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        <?php echo rwmb_meta( 'skilli-program-name' ); ?>
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        <?php echo rwmb_meta( 'skilli-program-description' ); ?>
                    </h2>
                </div>
            </div>
        </section>
        
        <!-- Business Ideas -->
        <section class="container-wrapper">
            <div class="margin-b-40 text-center">
                <h2 class="txt-2em txt-light txt-height-1-1">
                    Get expert support to start / grow any of 40+ Businesses
                </h2>
            </div>
        </section>
        <div class="bg-grey padding-tb-40">
        <?php
            /* Query */
            wp_reset_postdata();
            $temp = $wp_query; $wp_query= null;
            $wp_query = new WP_Query();
            $wp_query->query(array('post_type' => 'information-session'));
        ?>
        
        <?php 

            /* Get Terms */
            $terms = get_terms( 'shc-category', array('hide_empty' => false,)); //Get all the terms

            foreach ($terms as $term) { //Cycle through terms, one at a time

                // Check and see if the term is a top-level parent. If so, display it.
                $parent = $term->parent;

                if ( $parent=='0' ) {

                    $term_id = $term->term_id; //Get the term ID
                    $term_name = $term->name; //Get the term name
                    $term_url = get_term_link($term);
        ?>
                    
            
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-yellow  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-<?php echo $term_id ?>" aria-expanded="false" aria-controls="main-<?php echo $term_id ?>">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                <?php echo $term_name; ?> 
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-<?php echo $term_id ?>" class="collapse bg-yellow" aria-labelledby="heading-<?php echo $term_id ?>" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                <?php
                                    /* Loop */
                                    while ($wp_query->have_posts()) : $wp_query->the_post();
                                        if ( has_term( $term_name, 'shc-category' ) ) {
                                ?>

                                        <li class="col-6 col-md-4 padding-lr-15">
                                            <a href="<?php the_permalink() ?>">
                                                <i class="fa fa-chevron-right txt-color-dark"></i>
                                                <span class="txt-color-dark">
                                                    <?php the_title() ?>
                                                </span>
                                            </a>
                                        </li>

                                <?php
                                        }
                                    endwhile;
                                ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php
                } 
            }
        ?>
        </div>
        
        <!-- How it works -->
        <?php if ( $how_it_works ) { ?>
        <section class="padding-t-80 padding-b-60">
            <div class="container-wrapper">
                <div class="row text-center"> 
                    <div class="col-md-10 mx-auto">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-20 ">
                            How it Works
                        </h1>
                        <p class="txt-normal-s margin-b-60 text-center">
                            <?php echo $how_it_works_brief ?>
                        </p>
                      </div>  
                </div>
            </div>
            <div class="container-wrapper how-it-works-container">
                <div class="row text-center"> 
                    <div class="col-md-10 mx-auto">
                        <div class="row row-20 ">
                        <?php
                            
                            $counter = 1;

                            if ( ! empty( $how_it_works ) ) {
                                foreach ( $how_it_works as $group_value ) {
                        ?>

                            <div class="col-md-4 padding-lr-20">
                                <div class="how-card">
                                    <div class="number">
                                        <span>
                                            <?php
                                                echo $counter;
                                                $counter++;
                                            ?>
                                        </span>
                                    </div>
                                    <div class="content">
                                        <h3 class="txt-medium txt-height-1-2 margin-b-5">
                                            <?php echo $group_value[ 'skilli-program-hiw-title' ]; ?>
                                        </h3>
                                        <p class="txt-sm txt-height-1-7">
                                            <?php echo $group_value[ 'skilli-program-hiw-content' ]; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        <?php
                                }
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        
        <!-- What you get -->
        <?php if ( $what_you_get ) { ?>
        <section class="bg-grey padding-t-80 padding-b-80">
            <div class="container-wrapper row text-center">
                <div class="row"> 
                    <div class="col-md-8 mx-auto">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-20">
                            What you get
                        </h1>
                        <p class="txt-normal-s margin-b-60">
                            <?php echo rwmb_meta( 'skilli-program-wyg-brief', array(), $main_program_id ); ?>
                        </p>
                        <div class="row row-20">
                        <?php
                            

                            if ( ! empty( $what_you_get ) ) {
                                foreach ( $what_you_get as $group_value ) {
                        ?>
                            <div class="col-md-6 padding-lr-20 d-flex">
                                <div class="bg-white collapsible-card">
                                    <div class="header">
                                        <div class="cta"></div>
                                        <div class="title">
                                            <?php echo $group_value[ 'skilli-program-wyg-title' ]; ?>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="wrapper">
                                            <div class="margin-b-20">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_mentor.png" alt="" width="100">
                                            </div>
                                            <h3 class="txt-medium margin-b-20">
                                                <?php echo $group_value[ 'skilli-program-wyg-title' ]; ?>
                                            </h3>
                                            <p class="txt-sm txt-height-1-7">
                                                <?php echo $group_value[ 'skilli-program-wyg-content' ]; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                                }
                            }
                        ?>
                        </div>
                    </div>  
                </div>
            </div>
        </section>
        <?php } ?>
        
        <!-- FAQ -->
        <section class="container-wrapper padding-tb-60">
            <header class="margin-b-60 text-center">
                <h2 class="txt-xxlg txt-light padding-b-20">Frequently Asked Questions</h2>
                <p>
                    Got questions? Require clarifications? Find your answers here
                </p> 
            </header>
            <div class="row row-10">
            <?php 
                $accordionCounter = 1;

                foreach ( $program_faq as $value ) { 
            ?>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-<?php echo $accordionCounter ?>" aria-expanded="false" aria-controls="collapseExample">
                            <?php echo $value['accordion-title']; ?>
                        </button>
                        <div class="collapse" id="faq-<?php echo $accordionCounter ?>">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        <?php echo $value['accordion-content']; ?>
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                    $accordionCounter++;
                }
            ?> 
            </div>
            <p class="txt-xlg txt-medium text-center margin-t-40 margin-b-20">
                Didn't find your answer?
            </p>
            <div class="text-center">
                <a href="https://www.saedconnect.org/help-center/forums/forum/the-entrepreneurship-incubator-help-center/" class="btn btn-green txt-normal-s">
                    The Side Hustle Mentor Hubs Help Center
                </a>
            </div>
        </section>
        
        
        
        
        
        
        
        
        
        
                
        
        <!--<section>
            <div class="row txt-color-white">
            <?php wp_reset_postdata(); ?>
                <?php wp_reset_query(); ?>
                <?php // Display posts
                    $temp = $wp_query; $wp_query= null;
                    $wp_query = new WP_Query();
                    $wp_query->query(array('post_type' => 'information-session'));
                        while ($wp_query->have_posts()) : $wp_query->the_post();                
            ?>
                <div class="col-md-3">
                    <?php

                        $images = rwmb_meta( 'info-session-feautured-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>
                    <a
                       href="<?php the_permalink() ?>"
                       class="generic-card-bg h-180-480 overlay overlay-5"
                       style="background-image:url('<?php echo $image['full_url']; ?>');"
                    >
                        <h2 class="txt-lg txt-medium txt-height-1-2 margin-b-10 txt-color-white text-center padding-o-20">
                            <?php the_title() ?>
                        </h2>
                    </a>
                </div>
            <?php endwhile; ?>
            </div>
        </section>
        
        <section class="padding-t-60 padding-b-20 bg-grey">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <header class="padding-b-40 text-center">
                            <h1 class="txt-2-2em txt-light txt-height-1-1 margin-b-20">
                                What you get
                            </h1>
                            <h2 class="txt-normal-s txt-height-1-7">
                                We are going all out to ensure you get all the support & resources you require to start and excel in business.
                            </h2>
                        </header>
                        <div class="row row-20">
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10 align-items-center">
                                    <div class="col-2 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_expert_instructions.png" alt="">
                                    </div>
                                    <div class="col-10 padding-lr-10">
                                        <p class="txt-normal-s txt-bold txt-height-1-1">
                                            Expert Resources, Instruction & Guidance
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10 align-items-center">
                                    <div class="col-md-2 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="">
                                    </div>
                                    <div class="col-md-10 padding-lr-10">
                                        <p class="txt-normal-s txt-bold txt-height-1-1">
                                            Real-Life Learning from wherever you are
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10 align-items-center">
                                    <div class="col-md-2 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_real_world_experience.png" alt="">
                                    </div>
                                    <div class="col-md-10 padding-lr-10">
                                        <p class="txt-normal-s txt-bold txt-height-1-1">
                                            Start your own business 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10 align-items-center">
                                    <div class="col-md-2 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_amazing_support_network.png" alt="">
                                    </div>
                                    <div class="col-md-10 padding-lr-10">
                                        <p class="txt-normal-s txt-bold txt-height-1-1">
                                            Thrive within a Powerful Support Network
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10 align-items-center">
                                    <div class="col-md-2 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_challenge_the_status_quo.png" alt="">
                                    </div>
                                    <div class="col-md-10 padding-lr-10">
                                        <p class="txt-normal-s txt-bold txt-height-1-1">
                                            Explore every aspect of the business
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10 align-items-center">
                                    <div class="col-md-2 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_test_your_ideas.png" alt="">
                                    </div>
                                    <div class="col-md-10 padding-lr-10">
                                        <p class="txt-normal-s txt-bold txt-height-1-1">
                                            Access to Growth Opportunities
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        
        <!--
        <section>
            <header class="container-wrapper padding-t-40 padding-b-20">
                <h2 class="article-header-lg txt-color-lighter">
                    A
                </h2>
            </header>
            <article class="container-wrapper">
                <ul class="row row-40 directory-list txt-xlg">
                <?php wp_reset_postdata(); ?>
                    <?php wp_reset_query(); ?>
                    <?php // Display posts
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query(array('post_type' => 'information-session', 'posts_per_page' => 1, 'nopaging' => true, ));
                            while ($wp_query->have_posts()) : $wp_query->the_post();                
                ?>
                    <li class="col-md-4 padding-lr-40">
                        <a href="<?php the_permalink() ?>">
                            <?php the_title() ?>
                        </a>
                    </li>
                <?php endwhile; ?>
                </ul>
            </article>
        </section>
        -->

        <?php
            $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

            if(!$incoming_from_saedconnect){

                /* Include Skilli Footer */
                get_template_part( 'template-parts/footer-skilli' );
            }
        ?>
    </main>
    
<?php
    $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

    if($incoming_from_saedconnect){

        /* Include Saedconnect Footer */
        get_template_part( 'template-parts/footer-saedconnect' );
    }
?>
    
<?php get_footer() ?>


<?php
    $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

    if($incoming_from_saedconnect){
?>

   <script>
        $(document).ready(function(){
            /*
            *   Auto populate form with Track ID & Name
            */
            $('#formModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var track_id = button.attr('track-id'); // Extract Track ID
                var track_name = button.attr('track-name'); // Extract Track Name

                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);

                //modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('#input_1_6').val(track_id);
                modal.find('#input_1_7').val(track_name);
            })


            //Confirmation Modal
            $('#formConfirmationModal').modal('show')

            //Clear URL
           // var uri = window.location.toString();
            //if (uri.indexOf("?") > 0) {
                //var clean_uri = uri.substring(0, uri.indexOf("?"));
                //window.history.replaceState({}, document.title, clean_uri);
            //}

            //Append Saedconnect Query string
            $('a').each(function(index, element) {
              var newAttr = $(element).attr('href') + '?saedconnect=true';
              $(element).attr('href', newAttr);
            });
        });
    </script>

<?php } ?>