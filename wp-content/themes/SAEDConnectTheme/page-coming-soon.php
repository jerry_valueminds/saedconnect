<?php /*Template Name: Comming Soon Page*/ ?>
   

<?php get_header() ?>
    <main class="main-content">
        <section class="container-wrapper bg-white text-center padding-tb-80">
            <div class="row">
                <div class="d-none d-md-block col-md-4">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/dance.png" alt="">
                </div>
                <div class="col-md-4">
                    <h1 class="txt-4em txt-bold margin-b-20" style="font-weight:900">
                        Coming Soon
                    </h1>
                    <p class="txt">
                        We are currently building this service
                    </p>
                    <div class="margin-t-40">
                        <a href="#" class="btn btn-trans-blue" onclick="goBack()">
                            <i class="fa fa-arrow-left"></i>
                            <span class="padding-l-10">
                                Go Back
                            </span>
                        </a>
                        <a href="<?php echo get_site_url() ?>" class="btn btn-blue">
                            <i class="fa fa-home"></i>
                            <span class="padding-l-10">
                                Go to Homepage
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    
<?php get_footer() ?>