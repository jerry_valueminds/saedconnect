<?php
/**
*
* Main Content Settings Meta Box
*
*/
include 'post_types_metaboxes/_content_main.php';

/**
*
* Side Content Settings Meta Box
*
*/
include 'post_types_metaboxes/_content_side.php';

/**
*
* Footer Content Settings Meta Box
*
*/
include 'post_types_metaboxes/_content_footer.php';

/**
*
* Content Meta Boxes
*
*/
include 'post_types_metaboxes/_content.php';

/**
*
* Content General Meta Boxes
*
*/
include 'post_types_metaboxes/_content_general.php';

/**
*
* Online Guide Meta Boxes
*
*/
include 'post_types_metaboxes/online_guide_metaboxes.php';

/**
*
* Mini-site Meta Boxes
*
*/
include 'post_types_metaboxes/mini_site.php';

/**
*
* Trainer Meta Boxes
*
*/
include 'post_types_metaboxes/trainer.php';

/**
*
* Page Meta Boxes
*
*/
include 'post_types_metaboxes/_pages.php';

/**
*
* CV Meta Boxes
*
*/
include 'post_types_metaboxes/_cv.php';

/**
*
* CV Meta Boxes
*
*/
include 'post_types_metaboxes/_info_session.php';

/**
*
* Opportunities Meta Boxes
*
*/
include 'post_types_metaboxes/_opportunity.php';

/**
*
* SuccessKeys Meta Boxes
*
*/
include 'post_types_metaboxes/_successkeys.php';


/**
*
* SuccessKeys Meta Boxes
*
*/
include 'post_types_metaboxes/_change_ambassador.php';

/**
*
* Problems Meta Boxes
*
*/
include 'post_types_metaboxes/_problems.php';

/**
*
* BBPress Meta Boxes
*
*/
include 'post_types_metaboxes/_bbpress.php';







