<?php
/* Change Program Information Block */
function problems_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'general-settings',
		'title' => esc_html__( 'Problem Information', 'metabox-online-generator' ),
		'post_types' => array( 'problem' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
                'id' => 'problem-summary',
                'type' => 'textarea',
                'name' => esc_html__( 'Program Summary', 'metabox-online-generator' ),
            ),

                    // Accordion Group
            array(
                'group_title' => array( 'field' => 'problem-answer-group-title'),
                'id'     => 'problem-answer-group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'save_state' => true,
                'collapsible' => true,
                'add_button' => 'Add Answer Group',
                'fields' => array(
                    // Normal field (cloned)
                    array(
                        'name'  => 'Title',
                        'id'    => 'problem-answer-group-title',
                        'type'  => 'textarea',
                    ),
                    
                    array(
                        'name'  => 'Summary',
                        'id'    => 'problem-answer-group-summary',
                        'type'  => 'textarea',
                    ),

                    array(
                        'group_title' => array( 'field' => 'problem-answer-group-answer'),
                        'id'     => 'problem-answer-group-answers',
                        'type'   => 'group',
                        'clone'  => true,
                        'sort_clone' => true,
                        'save_state' => true,
                        'collapsible' => true,
                        'add_button' => 'Add Answer',
                        'fields' => array(
          
                            array(
                                'name'  => 'Answer',
                                'id'    => 'problem-answer-group-answer',
                                'type'  => 'textarea',
                            ),

                        ),
                    ),
                ),
            ),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'problems_meta_box' );