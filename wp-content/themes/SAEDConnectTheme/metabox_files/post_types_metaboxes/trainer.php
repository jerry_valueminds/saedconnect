
<?php
/*
 * Mini Site Meta Box
 */
function multiitem_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'mini_site_metabox',
		'title' => esc_html__( 'ABOUT TRAINER', 'metabox-online-generator' ),
		'post_types' => array( 'training' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
                'type' => 'heading',
                'name' => 'General Information',
            ),
            array(
				'id' => 'trainer-name',
				'type' => 'text',
				'name' => esc_html__( 'Display Name', 'metabox-online-generator' ),
			),
            array(
                'id'   => 'trainer-certification',
                'name' => esc_html__( 'Certified NYSCSAED Trainer?', 'metabox-online-generator' ),
                'type'      => 'switch',

                // Style: rounded (default) or square
                'style'     => 'rounded',

                // On label: can be any HTML
                'on_label'  => 'Yes',

                // Off label
                'off_label' => 'No',
            ),
            array(
                'name' => esc_html__( 'Trainer Rating', 'metabox-online-generator' ),
                'id'   => 'trainer-rating',
                'type' => 'range',

                'min'  => 1,
                'max'  => 5,
                'step' => 1,
                
                'std' => 1,
            ),
            array(
				'id' => 'trainer-about',
				'type' => 'textarea',
				'name' => esc_html__( 'About Trainer', 'metabox-online-generator' ),
			),
            array(
				'id' => 'trainer-addresses',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Trainer Addresses', 'metabox-online-generator' ),
				'rows' => 1,
				'options' => array(
					'address-line' => 'Address Line',
				),
				'clone' => true,
				'add_address' => esc_html__( 'Add Addresses', 'metabox-online-generator' ),
			),
            array(
                'type' => 'heading',
                'name' => 'Social Media',
            ),
            array(
				'id' => 'trainer-facebook',
				'type' => 'url',
				'name' => esc_html__( 'Facebook link', 'metabox-online-generator' ),
			),
            array(
				'id' => 'trainer-linkedin',
				'type' => 'url',
				'name' => esc_html__( 'Linkedin link', 'metabox-online-generator' ),
			),
            array(
				'id' => 'trainer-twitter',
				'type' => 'url',
				'name' => esc_html__( 'Twitter link', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'multiitem_get_meta_box' );





