<?php
/*
*
*==========================================================================================================
* META BOX: Opportunity Metaboxe
*==========================================================================================================
*
*/
function opportunity_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'opportunity-meta-box',
		'title' => esc_html__( 'Opportunity Summary', 'metabox-online-generator' ),
		'post_types' => array( 'opportunity', 'money-opportunity', 'user', 'author' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
            array(
                'name' => 'Use User data',
                'id'   => 'user_data',
                'type' => 'checkbox',
                'std'  => 1, // 0 or 1
            ),

			array(
				'id' => 'opportunity-title',
				'type' => 'input',
				'name' => esc_html__( 'Opportunity Title', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-author',
				'type' => 'input',
				'name' => esc_html__( 'Powered by', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-tagline',
				'type' => 'input',
				'name' => esc_html__( 'Opportunity Tagline (If any)', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-fee',
				'type' => 'input',
				'name' => esc_html__( 'Fee', 'metabox-online-generator' ),
			),
            
            array(
                'id'        => 'opportunity-featured',
                'name'      => esc_html__( 'Featured Opportunity?', 'metabox-online-generator' ),
                'type'      => 'switch',

                // Style: rounded (default) or square
                'style'     => 'rounded',

                // On label: can be any HTML
                'on_label'  => 'Yes',

                // Off label
                'off_label' => 'No',
            ),
            
            array(
				'id' => 'opportunity-open-date',
				'type' => 'date',
				'name' => esc_html__( 'Opens', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
				'id' => 'opportunity-close-date',
				'type' => 'date',
				'name' => esc_html__( 'Expires', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
				'id' => 'opportunity-start-date',
				'type' => 'date',
				'name' => esc_html__( 'Event Start Date', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
				'id' => 'opportunity-end-date',
				'type' => 'date',
				'name' => esc_html__( 'Event End Date', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
				'id' => 'opportunity-theme',
				'name' => esc_html__( 'Theme Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            
            array(
				'id' => 'opportunity-theme-alt',
				'name' => esc_html__( 'Alt. Theme Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            
            array(
				'id' => 'opportunity-short-description',
				'type' => 'textarea',
				'name' => esc_html__( 'Short Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-upload-banner',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Image Banner', 'metabox-online-generator' ),
			),
            
            array(
                'id' => 'opportunity-upload-brochure',
                'type' => 'file',
                'name' => esc_html__( 'Brochure', 'metabox-online-generator' ),
                
                // Delete file from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same file for multiple posts
                'force_delete'     => false,

                // Maximum file uploads.
                'max_file_uploads' => 1,
            ),

            
			array(
				'id' => 'opportunity-website',
				'type' => 'url',
				'name' => esc_html__( 'Website Link (if any)', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-application-link',
				'type' => 'url',
				'name' => esc_html__( 'Application Link (if any)', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-venue',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Venue', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-time',
				'type' => 'input',
				'name' => esc_html__( 'Time', 'metabox-online-generator' ),
			),
            
            array(
                'type' => 'heading',
                'name' => 'Opportunity Type',
                'desc' => '(select all that apply)',
            ),
            
            array(
                'id' => 'opportunity-type-training',
                'type' => 'checkbox',
                'name' => esc_html__( 'Training/Workshop/Seminar', 'metabox-online-generator' ),
                'std' => 0, // 0 or 1
            ),
            
            array(
                'id' => 'opportunity-type-business-incubation',
                'type' => 'checkbox',
                'name' => esc_html__( 'Business Incubation', 'metabox-online-generator' ),
                'std' => 0, // 0 or 1
            ),
            
            array(
                'id' => 'opportunity-type-finance',
                'type' => 'checkbox',
                'name' => esc_html__( 'Access to Finance', 'metabox-online-generator' ),
                'std' => 0, // 0 or 1
            ),
            
            array(
                'id' => 'opportunity-type-business-opportunity',
                'type' => 'checkbox',
                'name' => esc_html__( 'Business Opportunity', 'metabox-online-generator' ),
                'std' => 0, // 0 or 1
            ),
            
            array(
                'id' => 'opportunity-type-competition',
                'type' => 'checkbox',
                'name' => esc_html__( 'Competition / Hackathon', 'metabox-online-generator' ),
                'std' => 0, // 0 or 1
            ),
            
            array(
                'id' => 'opportunity-type-scholarship',
                'type' => 'checkbox',
                'name' => esc_html__( 'Scholarship', 'metabox-online-generator' ),
                'std' => 0, // 0 or 1
            ),            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'opportunity_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Opportunity Informatio Metaboxe
*==========================================================================================================
*
*/
function opportunity_information_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'opportunity-info-meta-box',
		'title' => esc_html__( 'Opportunity Information', 'metabox-online-generator' ),
		'post_types' => array( 'opportunity', 'money-opportunity' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
			array(
				'id' => 'opportunity-info-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Full Opportunity Description', 'metabox-online-generator' ),
			),
            
            array(
                'id'        => 'opportunity-info-disclaimer',
                'name'      => esc_html__( 'Add Disclaimer?', 'metabox-online-generator' ),
                'type'      => 'switch',

                // Style: rounded (default) or square
                'style'     => 'rounded',

                // On label: can be any HTML
                'on_label'  => 'Yes',

                // Off label
                'off_label' => 'No',
            ),

            
            array(
				'id' => 'opportunity-faq',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'FAQ', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add FAQ' ),
				'options' => array(
					'accordion-title' => 'Title',
					'accordion-content' => 'Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'opportunity_information_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Opportunity Informatio Metaboxe
*==========================================================================================================
*
*/
function opportunity_organizer_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'opportunity-organizer-meta-box',
		'title' => esc_html__( 'Organizer Contact', 'metabox-online-generator' ),
		'post_types' => array( 'opportunity', 'money-opportunity' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
			array(
				'id' => 'opportunity-organizer-email',
				'type' => 'input',
				'name' => esc_html__( 'Email', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-organizer-phone',
				'type' => 'input',
				'name' => esc_html__( 'Telephone', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-organizer-whatsapp',
				'type' => 'input',
				'name' => esc_html__( 'WhatsApp', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-organizer-facebook',
				'type' => 'input',
				'name' => esc_html__( 'Facebook', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-organizer-twitter',
				'type' => 'input',
				'name' => esc_html__( 'Twitter', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'opportunity-organizer-instagram',
				'type' => 'input',
				'name' => esc_html__( 'Instagram', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'opportunity_organizer_meta_box' );