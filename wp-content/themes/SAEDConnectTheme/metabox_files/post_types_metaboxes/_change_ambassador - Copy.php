<?php
/* section Content Block */
function change_program_block_get_meta_box( $meta_boxes ) {
    
	$meta_boxes[] = array(
		'id' => 'change_program_block_metabox',
		'title' => esc_html__( 'Content Blocks', 'metabox-online-generator' ),
		'post_types' => array('project', 'information-page'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => false,
        
        /* Fields */
		'fields' => array(
            
            /* Content Block Group */
            array(
                'group_title' => array( 'field' => 'select-content-type'),
                'id'     => 'content-block-group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'collapsible' => true,
                'add_button' => 'Add Content Block',                
                'fields' => array(
                    array(
                        'name'            => 'Select Content Type',
                        'id'              => 'select-content-type',
                        'type'            => 'select',
                        'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
                        // Array of 'value' => 'Label' pairs
                        'options' => array(
                            
                            'cp-heading-meta-box' => 'Heading',
                            'cp-text-meta-box' => 'Text',
                            'cp-image-meta-box' => 'Image',
                            'cp-hero-meta-box' => 'Image Hero',
                            'cp-text-image-meta-box' => 'Image & Text',
                            'cp-icon-group-meta-box' => 'Icon Group',
                            'cp-timeline-group-meta-box' => 'Timeline Group',
                            'cp-information-card-meta-box' => 'Information Cards',
                            'cp-job-card-meta-box' => 'Job Cards',
                            'cp-opportunity-card-meta-box' => 'Opportunity Cards',
                            'cp-image-card-group-meta-box' => 'Image Cards',
                            'cp-accordion-meta-box' => 'Accordion',
                            'cp-faq-meta-box' => 'FAQ Section',
                            'cp-form-section-meta-box' => 'Form Section',
                        ),
                        'columns' => 4,
                    ),
                    
                    /* Background Color */
                    array(
                        'id' => 'bg-color',
                        'type' => 'color',
                        'name' => 'Background Color',
                        // Add alpha channel?
                        'alpha_channel' => true,
                        // Color picker options. See here: https://automattic.github.io/Iris/.
                        'js_options'    => array(
                            'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                        ),
                    ),
                    /* Alt. Color */
                    array(
                        'id' => 'alt-color',
                        'type' => 'color',
                        'name' => 'Alt. Color',
                        // Add alpha channel?
                        'alpha_channel' => true,
                        // Color picker options. See here: https://automattic.github.io/Iris/.
                        'js_options'    => array(
                            'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                        ),
                    ),
                    array(
                        'id' => 'bg-image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'name' => esc_html__( 'Background Image', 'metabox-online-generator' ),
                    ),
                    array(
                        'id'        => 'show-overlay',
                        'name'      => esc_html__( 'Image Overlay?', 'metabox-online-generator' ),
                        'type'      => 'switch',

                        // Style: rounded (default) or square
                        'style'     => 'rounded',

                        // On label: can be any HTML
                        'on_label'  => 'Yes',

                        // Off label
                        'off_label' => 'No',
                    ),
                    
                    
                    
                    
                    /* Field Group for Image Hero */
                    array(
                        'id'     => 'cp-hero-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-hero-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'cp-hero-title',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Title', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-hero-summary',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Summary', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-hero-padding',
                                'type' => 'select',
                                'name' => esc_html__( 'Top & Bottom Spacing', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'None',
                                    '40' => 'Small',
                                    '80' => 'Medium',
                                    '180' => 'Large',
                                ),
                            ),

                            array(
                                'id' => 'cp-hero-cta-text',
                                'type' => 'text',
                                'name' => esc_html__( 'CTA Text', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-hero-cta-url',
                                'type' => 'url',
                                'name' => esc_html__( 'CTA Link', 'metabox-online-generator' ),
                            ),
                            /* Text Alignment */
                            array(
                                'id' => 'cp-hero-text-align',
                                'name' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'options' => array(
                                    'left' => 'Left',
                                    'center' => 'Center',
                                    'right' => 'Right',
                                ),
                            ),
                            array(
                                'id'        => 'cp-hero-show-content',
                                'name'      => esc_html__( 'Show Content?', 'metabox-online-generator' ),
                                'type'      => 'switch',

                                // Style: rounded (default) or square
                                'style'     => 'rounded',

                                // On label: can be any HTML
                                'on_label'  => 'Yes',

                                // Off label
                                'off_label' => 'No',
                            ),
                        ),
                    ),
                    
                    /* Field Group for Image */
                    array(
                        'id'     => 'cp-image-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-image-meta-box' ),
                        'fields' => array(
                            
                            array(
                                'id' => 'cp-image-image',
                                'type' => 'image_advanced',
                                'max_file_uploads' => 1,
                                'name' => esc_html__( 'Select Image', 'metabox-online-generator' ),
                            ),

                            array(
                                'id' => 'cp-image-width',
                                'type' => 'select',
                                'name' => esc_html__( 'Image Width', 'metabox-online-generator' ),
                                'options' => array(
                                    'col-md-4' => 'Small',
                                    'col-md-6' => 'Medium',
                                    'col-md-12' => 'Large',
                                    '1' => 'End to End',
                                ),
                            ),
                            
                            /* Padding */  
                            array(
                                'id' => 'cp-image-padding-top',
                                'type' => 'select',
                                'name' => esc_html__( 'Top Padding', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'None',
                                    '40' => 'Small',
                                    '80' => 'Medium',
                                    '180' => 'Large',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-image-padding-bottom',
                                'type' => 'select',
                                'name' => esc_html__( 'Bottom Padding', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'None',
                                    '40' => 'Small',
                                    '80' => 'Medium',
                                    '180' => 'Large',
                                ),
                            ),

                            
                            /* Justify Content */
                            array(
                                'id' => 'cp-image-justify-content',
                                'name' => esc_html__( 'Justify Content', 'metabox-online-generator' ),
                                'type' => 'select',
                                'options' => array(
                                    'mr-auto' => 'Left',
                                    'mx-auto' => 'Center',
                                    'ml-auto' => 'Right',
                                ),
                            ),

                        ),
                    ),
                    
                    /* Field Group for Heading */
                    array(
                        'id'     => 'cp-heading-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-heading-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'cp-heading-title',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Title', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-heading-content',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Content', 'metabox-online-generator' ),
                            ),
                            /* Text Alignment */
                            array(
                                'id' => 'cp-heading-text-align',
                                'name' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'options' => array(
                                    'left' => 'Left',
                                    'center' => 'Center',
                                    'right' => 'Right',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-heading-padding',
                                'type' => 'select',
                                'name' => esc_html__( 'Top & Bottom Spacing', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'None',
                                    '40' => 'Small',
                                    '80' => 'Medium',
                                    '180' => 'Large',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-heading-cta-text',
                                'type' => 'text',
                                'name' => esc_html__( 'CTA Text', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-heading-cta-url',
                                'type' => 'url',
                                'name' => esc_html__( 'CTA Link', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Text */
                    array(
                        'id'     => 'cp-text-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-text-meta-box' ),
                        'fields' => array(

                            array(
                                'id' => 'cp-text-content',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Content', 'metabox-online-generator' ),
                            ),
                            
                            /* Text Size */
                            array(
                                'id' => 'cp-text-font-size',
                                'name' => esc_html__( 'Select Text Size', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Size', 'metabox-online-generator' ),
                                'options' => array(
                                    'normal' => 'Small',
                                    'xxlg' => 'Medium',
                                    '2em' => 'Large',
                                    '3em' => 'Extra Large',
                                ),
                            ),
                            
                            array(
                                'id'        => 'cp-text-bold',
                                'name'      => esc_html__( 'Make Bold?', 'metabox-online-generator' ),
                                'type'      => 'switch',

                                // Style: rounded (default) or square
                                'style'     => 'rounded',

                                // On label: can be any HTML
                                'on_label'  => 'Yes',

                                // Off label
                                'off_label' => 'No',
                            ),
                            
                            /* Text Alignment */
                            array(
                                'id' => 'cp-text-text-align',
                                'name' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'options' => array(
                                    'left' => 'Left',
                                    'center' => 'Center',
                                    'right' => 'Right',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-text-width',
                                'type' => 'select',
                                'name' => esc_html__( 'Text Width', 'metabox-online-generator' ),
                                'options' => array(
                                    'col-md-4' => 'Small',
                                    'col-md-6' => 'Medium',
                                    'col-md-8' => 'Large',
                                    '12' => 'End to End',
                                ),
                            ),
                            
                            /* Justify Content */
                            array(
                                'id' => 'cp-text-justify-content',
                                'name' => esc_html__( 'Justify Content', 'metabox-online-generator' ),
                                'type' => 'select',
                                'options' => array(
                                    'ml-auto' => 'Left',
                                    'mx-auto' => 'Center',
                                    'mr-auto' => 'Right',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-text-padding-top',
                                'type' => 'select',
                                'name' => esc_html__( 'Top Padding', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'None',
                                    '40' => 'Small',
                                    '80' => 'Medium',
                                    '180' => 'Large',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-text-padding-bottom',
                                'type' => 'select',
                                'name' => esc_html__( 'Bottom Padding', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'None',
                                    '40' => 'Small',
                                    '80' => 'Medium',
                                    '180' => 'Large',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-text-cta-text',
                                'type' => 'text',
                                'name' => esc_html__( 'CTA Text', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-text-cta-url',
                                'type' => 'url',
                                'name' => esc_html__( 'CTA Link', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Plain Text */
                    array(
                        'id'     => 'cp-featured-action-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-featured-action-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'cp-featured-action-title',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Title', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-featured-action-content',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Content', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-featured-action-image',
                                'type' => 'image_advanced',
                                'max_file_uploads' => 1,
                                'name' => esc_html__( 'Image', 'metabox-online-generator' ),
                            ),
                            /* Select Image Position */
                            array(
                                'id' => 'cp-featured-action-image-position',
                                'name' => esc_html__( 'Image Position', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Image Position', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'Left',
                                    '1' => 'Right'
                                ),
                            ),
                            array(
                                'id' => 'cp-featured-action-cta-text',
                                'type' => 'input',
                                'name' => esc_html__( 'CTA Text', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-featured-action-cta-link',
                                'type' => 'url',
                                'name' => esc_html__( 'CTA Url', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Text Image */
                    array(
                        'id'     => 'cp-text-image-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-text-image-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'cp-text-image-subtitle',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Subtitle', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-text-image-title',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Title', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-text-image-content',
                                'type' => 'wysiwyg',
                                'name' => esc_html__( 'Content', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-text-image-image',
                                'type' => 'image_advanced',
                                'max_file_uploads' => 1,
                                'name' => esc_html__( 'Image', 'metabox-online-generator' ),
                            ),
                            /* Select Image Position */
                            array(
                                'id' => 'cp-text-image-image-position',
                                'name' => esc_html__( 'Image Position', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Image Position', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'Left',
                                    '1' => 'Right'
                                ),
                            ),
                            array(
                                'id' => 'cp-text-image-image-size',
                                'name' => esc_html__( 'Image Size', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Image Size', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'Normal',
                                    '1' => 'Background',
                                    '2' => 'Compact',
                                    '3' => 'Small',
                                ),
                            ),
                            
                            array(
                                'id'        => 'cp-text-image-border-bottom',
                                'name'      => esc_html__( 'Add Bottom Border?', 'metabox-online-generator' ),
                                'type'      => 'switch',

                                // Style: rounded (default) or square
                                'style'     => 'rounded',

                                // On label: can be any HTML
                                'on_label'  => 'Yes',

                                // Off label
                                'off_label' => 'No',
                            ),
                            
                            // Custom List Group
                            array(
                                'group_title' => array( 'field' => 'cp-text-image-list-content'),
                                'id'     => 'cp-text-image-list',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add List Item',
                                'fields' => array(
                                    // Normal field (cloned)
                                    array(
                                        'id' => 'cp-text-image-list-image',
                                        'type' => 'image_advanced',
                                        'max_file_uploads' => 1,
                                        'name' => esc_html__( 'Image', 'metabox-online-generator' ),
                                    ),
                                    array(
                                        'id' => 'cp-text-image-list-content',
                                        'type' => 'textarea',
                                        'name' => esc_html__( 'Content', 'metabox-online-generator' ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Icon group */
                    array(
                        'id'     => 'cp-icon-group-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-icon-group-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Icon Group Title',
                                'id'    => 'cp-icon-group-title',
                                'type'  => 'text',
                            ),
                            array(
                                'name'  => 'Icon Group Content',
                                'id'    => 'cp-icon-group-content',
                                'type'  => 'textarea',
                            ),
                            
                            /* Text Alignment */
                            array(
                                'id' => 'cp-icon-group-text-align',
                                'name' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'options' => array(
                                    'left' => 'Left',
                                    'center' => 'Center',
                                    'right' => 'Right',
                                ),
                            ),
                            
                            /* Icon Position */
                            array(
                                'id' => 'cp-icon-group-icon-position',
                                'name' => esc_html__( 'Icon Position', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'options' => array(
                                    'left' => 'Left',
                                    'top' => 'Top',
                                ),
                            ),
                            
                            /* Number of Columns */
                            array(
                                'id' => 'cp-icon-group--columns',
                                'name' => esc_html__( 'Select Number of Columns', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Number of Columns', 'metabox-online-generator' ),
                                'options' => array(
                                    '12' => '1 Column (Full Width',
                                    '6' => '2 Columns (Half Width)',
                                    '4' => '3 Columns (A Third)',
                                    '3' => '4 Columns (A Quarter)',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-icon-group-padding-top',
                                'type' => 'select',
                                'name' => esc_html__( 'Top Padding', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'None',
                                    '40' => 'Small',
                                    '80' => 'Medium',
                                    '180' => 'Large',
                                ),
                            ),
                            
                            array(
                                'id' => 'cp-icon-group-padding-bottom',
                                'type' => 'select',
                                'name' => esc_html__( 'Bottom Padding', 'metabox-online-generator' ),
                                'options' => array(
                                    '-40' => 'None',
                                    '20' => 'Small',
                                    '60' => 'Medium',
                                    '160' => 'Large',
                                ),
                            ),
                            
                            // Icon Group
                            array(
                                'group_title' => array( 'field' => 'cp-icon-content'),
                                'id'     => 'cp-icon-group',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add Accordion Item',
                                'fields' => array(
                                    array(
                                        'id' => 'cp-icon-image',
                                        'type' => 'image_advanced',
                                        'max_file_uploads' => 1,
                                        'name' => esc_html__( 'Image', 'metabox-online-generator' ),
                                    ),
                                    array(
                                        'name'  => 'icon Content',
                                        'id'    => 'cp-icon-content',
                                        'type'  => 'textarea',
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Timeline group */
                    array(
                        'id'     => 'cp-timeline-group-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-timeline-group-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Timeline Group Title',
                                'id'    => 'cp-timeline-group-title',
                                'type'  => 'text',
                            ),
                            array(
                                'name'  => 'Timeline Group Content',
                                'id'    => 'cp-timeline-group-content',
                                'type'  => 'textarea',
                            ),
                            
                            array(
                                'id' => 'cp-timeline-group-padding',
                                'type' => 'select',
                                'name' => esc_html__( 'Top & Bottom Spacing', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'None',
                                    '40' => 'Small',
                                    '80' => 'Medium',
                                    '180' => 'Large',
                                ),
                            ),
                            
                            // Timeline Group
                            array(
                                'group_title' => array( 'field' => 'cp-timeline-title'),
                                'id'     => 'cp-timeline',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add Accordion Item',
                                'fields' => array(
                                    array(
                                        'name'  => 'Timeline Item Title',
                                        'id'    => 'cp-timeline-title',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'name'  => 'Timeline Item Content',
                                        'id'    => 'cp-timeline-content',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'name'  => 'CTA Text',
                                        'id'    => 'cp-timeline-cta-text',
                                        'type'  => 'text',
                                    ),
                                    array(
                                        'name'  => 'CTA Link',
                                        'id'    => 'cp-timeline-cta-link',
                                        'type'  => 'url',
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Information Cards */
                    array(
                        'id'     => 'cp-information-card-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-information-card-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Information Group Title',
                                'id'    => 'cp-information-card-group-title',
                                'type'  => 'text',
                            ),
                            array(
                                'name'  => 'Information Group Content',
                                'id'    => 'cp-information-card-group-content',
                                'type'  => 'textarea',
                            ),
                            
                            /* Select Width */
                            array(
                                'id' => 'cp-information-card-group-columns',
                                'name' => esc_html__( 'Select Number of Columns', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Content Width', 'metabox-online-generator' ),
                                'options' => array(
                                    '12' => '1 Column (Full Width',
                                    '6' => '2 Columns (Half Width)',
                                    '4' => '3 Columns (A Third)',
                                    '3' => '4 Columns (A Quarter)',
                                ),
                            ),
                            
                            /* Text Alignment */
                            array(
                                'id' => 'cp-information-card-group-text-align',
                                'name' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'options' => array(
                                    'left' => 'Left',
                                    'center' => 'Center',
                                    'right' => 'Right',
                                ),
                            ),
                            
                            /* Background Color */
                            array(
                                'id' => 'cp-information-card-group-bg-color',
                                'type' => 'color',
                                'name' => 'Background Color',
                                // Add alpha channel?
                                'alpha_channel' => true,
                                // Color picker options. See here: https://automattic.github.io/Iris/.
                                'js_options'    => array(
                                    'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                                ),
                            ),
                            
                            // Infomation Card Group
                            array(
                                'group_title' => array( 'field' => 'cp-information-card-title'),
                                'id'     => 'cp-information-card',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add Accordion Item',
                                'fields' => array(
                                    array(
                                        'name'  => 'Infomation Card Title',
                                        'id'    => 'cp-information-card-title',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'name'  => 'Infomation Card Content',
                                        'id'    => 'cp-information-card-content',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'id' => 'cp-information-card-image',
                                        'type' => 'image_advanced',
                                        'max_file_uploads' => 1,
                                        'name' => esc_html__( 'Image', 'metabox-online-generator' ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Job Cards */
                    array(
                        'id'     => 'cp-job-card-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-job-card-meta-box' ),
                        'fields' => array(
                            
                            /* Select Width */
                            array(
                                'id' => 'cp-job-card-group-columns',
                                'name' => esc_html__( 'Select Number of Columns', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Content Width', 'metabox-online-generator' ),
                                'options' => array(
                                    '12' => '1 Column (Full Width',
                                    '6' => '2 Columns (Half Width)',
                                    '4' => '3 Columns (A Third)',
                                    '3' => '4 Columns (A Quarter)',
                                ),
                            ),
                            
                            /* Text Alignment */
                            array(
                                'id' => 'cp-job-card-group-text-align',
                                'name' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'options' => array(
                                    'left' => 'Left',
                                    'center' => 'Center',
                                    'right' => 'Right',
                                ),
                            ),
                            
                            /* Justify Content */
                            array(
                                'id' => 'cp-job-card-justify-content',
                                'name' => esc_html__( 'Justify Content', 'metabox-online-generator' ),
                                'type' => 'select',
                                'options' => array(
                                    'mr-auto' => 'Left',
                                    'mx-auto' => 'Center',
                                    'ml-auto' => 'Right',
                                ),
                            ),
                            
                            /* Background Color */
                            array(
                                'id' => 'cp-job-card-group-bg-color',
                                'type' => 'color',
                                'name' => 'Background Color',
                                // Add alpha channel?
                                'alpha_channel' => true,
                                // Color picker options. See here: https://automattic.github.io/Iris/.
                                'js_options'    => array(
                                    'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                                ),
                            ),
                            
                            // Infomation Card Group
                            array(
                                'group_title' => array( 'field' => 'cp-job-card-title'),
                                'id'     => 'cp-job-card',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add Card',
                                'fields' => array(
                                    array(
                                        'name'  => 'Job Card Title',
                                        'id'    => 'cp-job-card-title',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'name'  => 'Job Card Status Label',
                                        'id'    => 'cp-job-card-status-label',
                                        'type'  => 'text',
                                    ),
                                    array(
                                        'name'  => 'Job Card Status Value',
                                        'id'    => 'cp-job-card-value-value',
                                        'type'  => 'text',
                                    ),
                                    array(
                                        'name'  => 'Job Card Content',
                                        'id'    => 'cp-job-card-content',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'name'  => 'CTA Text',
                                        'id'    => 'cp-job-card-cta-text',
                                        'type'  => 'text',
                                    ),
                                    array(
                                        'name'  => 'CTA Link',
                                        'id'    => 'cp-job-card-cta-link',
                                        'type'  => 'url',
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Opportunity Cards */
                    array(
                        'id'     => 'cp-opportunity-card-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-opportunity-card-meta-box' ),
                        'fields' => array(
                            
                            /* Select Width */
                            array(
                                'id' => 'cp-opportunity-card-group-columns',
                                'name' => esc_html__( 'Select Number of Columns', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Content Width', 'metabox-online-generator' ),
                                'options' => array(
                                    '12' => '1 Column (Full Width',
                                    '6' => '2 Columns (Half Width)',
                                    '4' => '3 Columns (A Third)',
                                    '3' => '4 Columns (A Quarter)',
                                ),
                            ),
                            
                            /* Text Alignment */
                            array(
                                'id' => 'cp-opportunity-card-group-text-align',
                                'name' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Text Alignment', 'metabox-online-generator' ),
                                'options' => array(
                                    'left' => 'Left',
                                    'center' => 'Center',
                                    'right' => 'Right',
                                ),
                            ),
                            
                            /* Background Color */
                            array(
                                'id' => 'cp-opportunity-card-group-bg-color',
                                'type' => 'color',
                                'name' => 'Background Color',
                                // Add alpha channel?
                                'alpha_channel' => true,
                                // Color picker options. See here: https://automattic.github.io/Iris/.
                                'js_options'    => array(
                                    'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                                ),
                            ),
                            
                            // Opportunity Card Group
                            array(
                                'group_title' => array( 'field' => 'cp-opportunity-card-title'),
                                'id'     => 'cp-opportunity-card',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add Item',
                                'fields' => array(
                                    array(
                                        'name'  => 'Title',
                                        'id'    => 'cp-opportunity-card-title',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'name'  => 'Content',
                                        'id'    => 'cp-opportunity-card-content',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'id' => 'cp-opportunity-card-image',
                                        'type' => 'image_advanced',
                                        'max_file_uploads' => 1,
                                        'name' => esc_html__( 'Image', 'metabox-online-generator' ),
                                    ),
                                    array(
                                        'name'  => 'URL',
                                        'id'    => 'cp-opportunity-card-link',
                                        'type'  => 'url',
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Image Card Group */
                    array(
                        'id'     => 'cp-image-card-group-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-image-card-group-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Image Card Group Title',
                                'id'    => 'cp-image-card-group-title',
                                'type'  => 'text',
                            ),
                            array(
                                'name'  => 'Image Card Group Content',
                                'id'    => 'cp-image-card-group-content',
                                'type'  => 'textarea',
                            ),
                            
                            
                            // Infomation Card Group
                            array(
                                'group_title' => array( 'field' => 'cp-image-card-title'),
                                'id'     => 'cp-image-card-group',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add Accordion Item',
                                'fields' => array(
                                    array(
                                        'name'  => 'image Card Title',
                                        'id'    => 'cp-image-card-title',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'name'  => 'image Card Content',
                                        'id'    => 'cp-image-card-content',
                                        'type'  => 'textarea',
                                    ),
                                    array(
                                        'id' => 'cp-image-card-image',
                                        'type' => 'image_advanced',
                                        'max_file_uploads' => 1,
                                        'name' => esc_html__( 'Image', 'metabox-online-generator' ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                                        
                    /* Field Group for FAQ Accordion */
                    array(
                        'id'     => 'cp-faq-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-faq-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Accordion Group Title',
                                'id'    => 'cp-faq-accordion-group-title',
                                'type'  => 'text',
                            ),
                            array(
                                'name'  => 'Accordion Group Content',
                                'id'    => 'cp-faq-accordion-group-content',
                                'type'  => 'textarea',
                            ),
                            
                            // Accordion Group
                            array(
                                'group_title' => array( 'field' => 'cp-faq-accordion-title'),
                                'id'     => 'cp-faq-accordion',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add Accordion Item',
                                'fields' => array(
                                    // Normal field (cloned)
                                    array(
                                        'name'  => 'Accordion Title',
                                        'id'    => 'cp-faq-accordion-title',
                                        'type'  => 'text',
                                    ),
                                    
                                    array(
                                        'id' => 'cp-faq-accordion-content',
                                        'type' => 'wysiwyg',
                                        'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Accordion */
                    array(
                        'id'     => 'cp-accordion-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-accordion-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Accordion Group Title',
                                'id'    => 'cp-accordion-group-title',
                                'type'  => 'text',
                            ),
                            array(
                                'name'  => 'Accordion Group Summary',
                                'id'    => 'cp-accordion-group-summary',
                                'type'  => 'textarea',
                            ),
                            array(
                                'name'  => 'Accordion Group Content',
                                'id'    => 'cp-accordion-group-content',
                                'type'  => 'textarea',
                            ),
                            
                            /* Background Color */
                            array(
                                'id' => 'cp-accordion-group-bg-color',
                                'type' => 'color',
                                'name' => 'Background Color',
                                // Add alpha channel?
                                'alpha_channel' => true,
                                // Color picker options. See here: https://automattic.github.io/Iris/.
                                'js_options'    => array(
                                    'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                                ),
                            ),
                            
                            // Accordion Group
                            array(
                                'group_title' => array( 'field' => 'cp-accordion-title'),
                                'id'     => 'cp-accordion',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => true,
                                'add_button' => 'Add Accordion Item',
                                'fields' => array(
                                    // Normal field (cloned)
                                    array(
                                        'name'  => 'Accordion Title',
                                        'id'    => 'cp-accordion-title',
                                        'type'  => 'text',
                                    ),
                                    
                                    array(
                                        'id' => 'cp-accordion-content',
                                        'type' => 'wysiwyg',
                                        'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group Form Section */
                    array(
                        'id'     => 'cp-form-section-meta-box',
                        'type'   => 'group',
                        'save_state' => true,
                        'hidden' => array( 'select-content-type', '!=', 'cp-form-section-meta-box' ),
                        'fields' => array(
                            /* Select Content Type */
                            array(
                                'id' => 'cp-form-section-content-type',
                                'name' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
                                'options' => array(
                                    'text' => 'Text Content',
                                    'image' => 'Image',
                                ),
                            ),
                            /* Select Content Position */
                            array(
                                'id' => 'cp-form-section-content-type-position',
                                'name' => esc_html__( 'Content Position', 'metabox-online-generator' ),
                                'type' => 'select',
                                'desc' => esc_html__( 'Content Position', 'metabox-online-generator' ),
                                'options' => array(
                                    '0' => 'Left',
                                    '1' => 'Right'
                                ),
                            ),
                            
                            
                            array(
                                'id' => 'cp-form-section-title',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Title', 'metabox-online-generator'),
                                'hidden' => array( 'cp-form-section-content-type', '!=', 'text' ),
                            ),
                            
                            array(
                                'id' => 'cp-form-section-content',
                                'type' => 'wysiwyg',
                                'name' => esc_html__( 'Content', 'metabox-online-generator' ),
                                'hidden' => array( 'cp-form-section-content-type', '!=', 'text' ),
                            ),
                            array(
                                'id' => 'cp-form-section-bg-image',
                                'type' => 'image_advanced',
                                'max_file_uploads' => 1,
                                'name' => esc_html__( 'Background Image', 'metabox-online-generator' ),
                                'hidden' => array( 'cp-form-section-content-type', '!=', 'image' ),
                            ),
                            array(
                                'id' => 'cp-form-section-form-title',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Form Title', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-form-section-form-summary',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Form Brief', 'metabox-online-generator' ),
                            ),
                            array(
                                'id' => 'cp-form-section-form-id',
                                'type' => 'number',
                                'name' => esc_html__( 'Gravity Form ID', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                    
                    
                ),
            ),
        
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'change_program_block_get_meta_box' );

/* Change Program Information Block */
function change_ambassador_info_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'general-settings',
		'title' => esc_html__( 'Project Information', 'metabox-online-generator' ),
		'post_types' => array( 'project' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
                'id' => 'change-program-name',
                'type' => 'input',
                'name' => esc_html__( 'Program Name', 'metabox-online-generator' ),
            ),
            array(
                'id' => 'change-program-summary',
                'type' => 'textarea',
                'name' => esc_html__( 'Program Summary', 'metabox-online-generator' ),
            ),
            array(
				'id' => 'change-program-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Program Image', 'metabox-online-generator' ),
			),
            array(
                'id' => 'change-program-theme-color',
                'type' => 'color',
                'name' => 'Program Theme Color',
                // Add alpha channel?
                'alpha_channel' => true,
                // Color picker options. See here: https://automattic.github.io/Iris/.
                'js_options'    => array(
                    'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                ),
            ),
            
            array(
                'id' => 'change-program-alt-theme-color',
                'type' => 'color',
                'name' => 'Program Alt Theme Color',
                // Add alpha channel?
                'alpha_channel' => true,
                // Color picker options. See here: https://automattic.github.io/Iris/.
                'js_options'    => array(
                    'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                ),
            ),
            
            array(
                'type' => 'heading',
                'name' => 'Program Action',
                'desc' => 'Add Actions and their corresponding forms',
            ),

                    // Accordion Group
            array(
                'group_title' => array( 'field' => 'cp-action-title'),
                'id'     => 'cp-action',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'save_state' => true,
                'collapsible' => true,
                'add_button' => 'Add Action',
                'fields' => array(
                    // Normal field (cloned)
                    array(
                        'name'  => 'Action Title',
                        'id'    => 'cp-action-title',
                        'type'  => 'text',
                    ),

                    array(
                        'id' => 'cp-action-content',
                        'type' => 'textarea',
                        'name' => esc_html__( 'Summary', 'metabox-online-generator' ),
                    ),

                    array(
                        'id' => 'cp-action-image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'name' => esc_html__( 'Image', 'metabox-online-generator' ),
                    ),

                    array(
                        'id' => 'cp-action-gf-id',
                        'type' => 'number',
                        'name' => esc_html__( 'Gravity Forms ID', 'metabox-online-generator' ),
                    ),
                    array(
                        'id' => 'cp-action-cta-text',
                        'type' => 'input',
                        'name' => esc_html__( 'CTA Text', 'metabox-online-generator' ),
                    ),
                ),
            ),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'change_ambassador_info_meta_box' );

/* Information Page Block */
function information_page_info_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'general-settings',
		'title' => esc_html__( 'Page Information', 'metabox-online-generator' ),
		'post_types' => array( 'information-page' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            

		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'information_page_info_meta_box' );

/* Field Group for Action */
            