<?php
/*
*
*==========================================================================================================
* META BOX: Information Session Metaboxes
*==========================================================================================================
*
*/
function info_session_general_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-general-meta-box',
		'title' => esc_html__( 'Session Information', 'metabox-online-generator' ),
		'post_types' => array( 'information-session' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'info-session-name',
				'type' => 'text',
				'name' => esc_html__( 'Session Name', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-session-feautured-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Feautured Image', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-session-form',
				'type' => 'text',
				'name' => esc_html__( 'Enrollment Form Shortcode', 'metabox-online-generator' ),
			),
            
            array(
                'type' => 'heading',
                'name' => 'Sessions Essentials Section',
                'desc' => 'Optional description',
            ),
            
            array(
				'id' => 'info-session-essentials',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Session Essentials', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add Session Essentials' ),
				'options' => array(
					'accordion-title' => 'Title',
					'accordion-content' => 'Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),
            
                        array(
                'type' => 'divider',
            ),

            array(
                'type' => 'heading',
                'name' => 'Community Perks Section',
                'desc' => 'Select as applicable to this session',
            ),
            
            array(
                'id'   => 'info-session-perk-1',
                'type' => 'checkbox',
                'name' => esc_html__( 'Community Perk 1', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'info-session-perk-2',
                'type' => 'checkbox',
                'name' => esc_html__( 'Community Perk 2', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'info-session-perk-3',
                'type' => 'checkbox',
                'name' => esc_html__( 'Community Perk 3', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'type' => 'heading',
                'name' => 'Schedule Sessions',
                'desc' => 'Optional description',
            ),
            
            array(
				'id' => 'info-session-start-date',
				'type' => 'date',
				'name' => esc_html__( 'Starting Out Date', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
				'id' => 'info-session-question-date',
				'type' => 'date',
				'name' => esc_html__( 'Need Help Date', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
                'type' => 'divider',
            ),

            array(
                'type' => 'heading',
                'name' => 'What am I getting Section',
                'desc' => 'Optional description',
            ),
            
            array(
                'id'   => 'info-session-help-1',
                'type' => 'checkbox',
                'name' => esc_html__( 'What you are getting 1', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'info-session-help-2',
                'type' => 'checkbox',
                'name' => esc_html__( 'What you are getting 2', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'info-session-help-3',
                'type' => 'checkbox',
                'name' => esc_html__( 'What you are getting 3', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),

            array(
                'type' => 'divider',
            ),
            
            array(
                'type' => 'heading',
                'name' => 'Micro Sessions',
                'desc' => 'Add Micro Sessions to this Information Session',
            ),
            
            array(
				'id' => 'info-session-micro-sessions',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Micro Session', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add a Micro Session' ),
				'options' => array(
					'micro-session-title' => 'Title',
					'micro-session-desc' => 'Content',
					'micro-session-form' => 'Micro Session Form Shortcode',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add session', 'metabox-online-generator' ),
			),
            
            array(
                'type' => 'divider',
            ),
            
            array(
                'type' => 'heading',
                'name' => 'FAQ Section',
                'desc' => 'Optional description',
            ),
            
            array(
				'id' => 'info-session-faq',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'FAQ', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add FAQ' ),
				'options' => array(
					'accordion-title' => 'Title',
					'accordion-content' => 'Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_session_general_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Information Session - Micro Course Metaboxes
*==========================================================================================================
*
*/
function info_sess_micro_course_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-micro-course-meta-box',
		'title' => esc_html__( 'Micro Course Information', 'metabox-online-generator' ),
		'post_types' => array( 'micro-course' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'micro-course-name',
				'type' => 'text',
				'name' => esc_html__( 'Course Name', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Short Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-goal',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'What will you learn?', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-objectives',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Outcome Objectives', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-project',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Project', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-Requirement',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Required Tools', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-pre-requisites',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Pre-Requisites', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-schedules',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Schedules', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-fee',
				'type' => 'text',
				'name' => esc_html__( 'Fee', 'metabox-online-generator' ),
			),
            
            array(
                'type' => 'heading',
                'name' => 'Learning Model',
                'desc' => 'Select applicable model',
            ),
            
            array(
                'id'   => 'micro-course-model-online',
                'type' => 'checkbox',
                'name' => esc_html__( 'Fully Online', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'micro-course-model-offline',
                'type' => 'checkbox',
                'name' => esc_html__( 'Offline', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'micro-course-model-blended',
                'type' => 'checkbox',
                'name' => esc_html__( 'Blended', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'type' => 'divider',
            ),

            
            array(
				'id' => 'micro-course-locations',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Locations Available', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_sess_micro_course_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Information Session - Mentor Metaboxes
*==========================================================================================================
*
*/
function info_sess_mentor_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-mentor-meta-box',
		'title' => esc_html__( 'Mentor Information', 'metabox-online-generator' ),
		'post_types' => array( 'info-session-mentor' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'info-sess-mentor-name',
				'type' => 'text',
				'name' => esc_html__( 'Mentor Name', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-sess-mentor-desc',
				'type' => 'textarea',
				'name' => esc_html__( 'Brief Information', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-sess-mentor-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Profile Image', 'metabox-online-generator' ),
			),
  
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_sess_mentor_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Information Session - Testimonial Metaboxes
*==========================================================================================================
*
*/
function info_sess_testimonial_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-testimonial-meta-box',
		'title' => esc_html__( 'Testimonial Information', 'metabox-online-generator' ),
		'post_types' => array( 'info-session-testi' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'info-sess-testimonial-name',
				'type' => 'text',
				'name' => esc_html__( 'Testimonial Author', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-sess-testimonial-desc',
				'type' => 'textarea',
				'name' => esc_html__( 'Author description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-sess-testimonial-quote',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Quote', 'metabox-online-generator' ),
			),
  
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_sess_testimonial_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Track Metaboxes
*==========================================================================================================
*
*/
function track_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-general-meta-box',
		'title' => esc_html__( 'Track Information', 'metabox-online-generator' ),
		'post_types' => array( 'track' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'program-name',
				'type' => 'text',
				'name' => esc_html__( 'Program Name', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-feautured-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Feautured Image', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-icon',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Track Icon', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-duration',
				'type' => 'text',
				'name' => esc_html__( 'Program Duration', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-sms-code',
				'type' => 'text',
				'name' => esc_html__( 'Enrollment SMS Code', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-form-shortcode',
				'type' => 'text',
				'name' => esc_html__( 'Enrollment Form Shortcode', 'metabox-online-generator' ),
			),
            
            
            array(
				'id' => 'program-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Program Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-prerequisites',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Program Pre-requisites', 'metabox-online-generator' ),
			),
            
            
            array(
                'type' => 'heading',
                'name' => 'Program Locations',
                'desc' => 'Select as many as are applicable.',
            ),
            
            array(
                  'id' => 'program-location-abia',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Abia', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-adamawa',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Adamawa', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-akwa-ibom',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Akwa Ibom', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-anambra',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Anambra', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-bauchi',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Bauchi', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),

             array(
                  'id' => 'program-location-bayelsa',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Bayelsa', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-benue',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Benue', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-borno',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Borno', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-cross-river',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Cross River', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-delta',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Delta', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),

             array(
                  'id' => 'program-location-ebonyi',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Ebonyi', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-edo',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Edo', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-ekiti',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Ekiti', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-enugu',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Enugu', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-gombe',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Gombe', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),

             array(
                  'id' => 'program-location-imo',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Imo', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-jigawa',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Jigawa', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-kaduna',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'kaduna', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-kano',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Kano', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-katsina',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Katsina', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),

             array(
                  'id' => 'program-location-kebbi',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Kebbi', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-kogi',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Kogi', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-kwara',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Kwara', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-lagos',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Lagos', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-nassarawa',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Nassarawa', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),

             array(
                  'id' => 'program-location-niger',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Niger', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-ogun',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Ogun', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-ondo',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Ondo', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-osun',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Osun', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-oyo',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Oyo', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),

             array(
                  'id' => 'program-location-plateau',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'plateau', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-rivers',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Rivers', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-sokoto',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Sokoto', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-taraba',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Taraba', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),


             array(
                  'id' => 'program-location-yobe',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Yobe', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),

             array(
                  'id' => 'program-location-zamfara',
                  'type' => 'checkbox',
                  'name' => esc_html__( 'Zamfara', 'metabox-online-generator' ),
                  'std' => 0, // 0 or 1
                ),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'track_meta_box' );