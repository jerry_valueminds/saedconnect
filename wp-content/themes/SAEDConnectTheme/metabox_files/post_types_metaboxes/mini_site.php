
<?php
/*
 * Mini Site Meta Box
 */
function mini_site_get_meta_box( $meta_boxes ) {
	$prefix = 'prefix-';

	$meta_boxes[] = array(
		'id' => 'mini-site-content-metabox',
		'title' => esc_html__( 'Information', 'metabox-online-generator' ),
		'post_types' => array( 'mini-site', 'guide-topic' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'mini-site-feature-image',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Mini Site Featured Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Mini Site's Featured Image", 'metabox-online-generator' ),
			),
            array(
				'id' => 'mini-site-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Mini Site Description', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Mini Site's Description, also Topic description when a mini site is used as a Guide Topic", 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'mini-site-sponsor-name',
				'type' => 'text',
				'name' => esc_html__( "Mini Site's Sponsor Name", 'metabox-online-generator' ),
			),
            array(
				'id' => 'mini-site-sponsor-image',
				'type' => 'image_advanced',
				'name' => esc_html__( "Mini Site's Sponsor Image", 'metabox-online-generator' ),
				'desc' => esc_html__( "This Mini Site's Sponsor Image", 'metabox-online-generator' ),
			),
            
			array(
				'id' => 'mini-site-theme-color',
				'name' => esc_html__( 'Theme Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            array(
				'id' => 'mini-site-link-color',
				'name' => esc_html__( 'Link Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_get_meta_box' );





