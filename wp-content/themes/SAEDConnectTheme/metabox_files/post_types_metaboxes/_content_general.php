<?php
/*
*
*==========================================================================================================
* META BOX: General Metaboxes
*==========================================================================================================
*
*/
function sponsor_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'sponsor-metabox',
		'title' => esc_html__( 'Sponsor Information', 'metabox-online-generator' ),
		'post_types' => array( 'guide-article-group', 'guide-collection' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'sponsor-name',
				'type' => 'text',
				'name' => esc_html__( 'Sponsor Name', 'metabox-online-generator' ),
			),
			array(
				'id' => 'sponsor-image',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Sponsor Image', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'sponsor_get_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Album Metaboxes
*==========================================================================================================
*
*/
function album_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'sponsor-metabox',
		'title' => esc_html__( 'Album Content Information', 'metabox-online-generator' ),
		'post_types' => array( 'album-content'),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'album-title',
				'type' => 'text',
				'name' => esc_html__( 'Album Title', 'metabox-online-generator' ),
			),
			array(
				'id' => 'album-subtitle',
				'type' => 'text',
				'name' => esc_html__( 'Album Subtitle', 'metabox-online-generator' ),
			),
            array(
				'id' => 'album-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
			),
            array(
				'id' => 'select-content-type',
				'name' => esc_html__( 'Select Media Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Media Type', 'metabox-online-generator' ),
				'placeholder' => esc_html__( 'Select Media Type', 'metabox-online-generator' ),
				'options' => array(
					'gi-image-meta-box' => 'Image',
                    'gi-video-meta-box' => 'Video'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'album_get_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Home Slides Metaboxes
*==========================================================================================================
*
*/
function homepage_slides_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'sponsor-metabox',
		'title' => esc_html__( 'Album Content Information', 'metabox-online-generator' ),
		'post_types' => array( 'homepage-slide'),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'slide-title',
				'type' => 'textarea',
				'name' => esc_html__( 'Slide Title', 'metabox-online-generator' ),
			),
            array(
				'id' => 'slide-subtitle',
				'type' => 'textarea',
				'name' => esc_html__( 'Slide Subtitle', 'metabox-online-generator' ),
			),
            array(
				'id' => 'slide-caption',
				'type' => 'textarea',
				'name' => esc_html__( 'Slide Content', 'metabox-online-generator' ),
			),
            array(
				'id' => 'slide-image',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Sponsor Image', 'metabox-online-generator' ),
			),
            array(
				'id' => 'slide-cta-text',
				'type' => 'text',
				'name' => esc_html__( 'Call To Action text', 'metabox-online-generator' ),
			),
            array(
				'id' => 'slide-cta-link',
				'type' => 'url',
				'name' => esc_html__( 'Call To Action URL', 'metabox-online-generator' ),
			),
            array(
				'id' => 'select-slide-theme',
				'name' => esc_html__( 'Select Slide Theme', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Slide Theme', 'metabox-online-generator' ),
				'placeholder' => esc_html__( 'Select Slide Theme', 'metabox-online-generator' ),
				'options' => array(
					'themeOne' => 'Theme one',
                    'themeTwo' => 'Theme Two'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'homepage_slides_get_meta_box' );