<?php
/*
*
*==========================================================================================================
* META BOX: Side COntent Settings
*==========================================================================================================
*
*/
function mini_site_side_content_general_meta_box( $meta_boxes ) {
	$prefix = 'general-item-content-';

	$meta_boxes[] = array(
		'id' => 'general-settings',
		'class' => 'general-item-content-',
		'title' => esc_html__( 'General Settings', 'metabox-online-generator' ),
		'post_types' => array( 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'select-content-type',
				'name' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'placeholder' => esc_html__( 'Select content Type', 'metabox-online-generator' ),
				'options' => array(
					'gi-text-meta-box' => 'Text',
					'gi-image-meta-box' => 'Image',
                    'gi-video-meta-box' => 'Video',
                    'gi-quicklinks-meta-box' => 'Quicklinks',
					'gi-quote-meta-box' => 'Quote',
                    'gi-event-meta-box' => 'Events',
					'gi-article-meta-box' => 'Article & Other General Links',
                    'gi-permalink-meta-box' => 'Post Permalink',
                    'gi-cta-1-meta-box' => 'Call to Action 1',
					'gi-cta-2-meta-box' => 'Call to Action 2',
                    'gi-image-gallery-meta-box' => 'Image Gallery',
                    'gi-text-gallery-meta-box' => 'Text Gallery',
					'gi-ftp-meta-box' => 'Faculty/Team/Product',
					'gi-link-meta-box' => 'Internal / External Link',
                    'gi-accordion-meta-box' => 'Accordion',
                    'gi-schedule-meta-box' => 'Schedule',
					'gi-pft-meta-box' => 'Profile/Faculty/Team',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_side_content_general_meta_box' );






