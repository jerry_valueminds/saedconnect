<?php
/*
*
*==========================================================================================================
* META BOX: ALl Metaboxes
*==========================================================================================================

/* Text Box */
function mini_site_general_item_text_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-text-meta-box',
		'title' => esc_html__( 'Text Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'text_content',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_text_meta_box' );

/* Plain Text Box */
function mini_site_general_item_plain_text_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-plain-text-meta-box',
		'title' => esc_html__( 'Text Content', 'metabox-online-generator' ),
		'post_types' => array( 'content-block', 'bottom-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'plain-text-content',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_plain_text_meta_box' );

/* Text Strip */
function mini_site_general_item_text_strip_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-text-strip-meta-box',
		'title' => esc_html__( 'Text Strip Content', 'metabox-online-generator' ),
		'post_types' => array( 'content-block', 'bottom-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'text-strip-content',
				'type' => 'textarea',
				'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
			),
            array(
				'id' => 'text-strip-background-color',
				'name' => esc_html__( 'Background Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            array(
				'id' => 'text-strip-text-color',
				'name' => esc_html__( 'Background Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_text_strip_meta_box' );

/* Text, Image & Button Box */
function text_image_button_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-text-image-button-meta-box',
		'title' => esc_html__( 'Text & Image Content', 'metabox-online-generator' ),
		'post_types' => array( 'guide-article-rccb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'tib-feature-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Content Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Content's Image", 'metabox-online-generator' ),
			),
			array(
				'id' => 'tib-text-content',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
			),
            array(
				'id' => 'tib-button-text',
				'type' => 'input',
				'name' => esc_html__( 'Button Text', 'metabox-online-generator' ),
			),

            array(
				'id' => 'tib-button-url',
				'type' => 'url',
				'name' => esc_html__( 'Button URL', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'tib-link-select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'text_image_button_meta_box' );

/* Text, Video & Button Box */
function text_video_button_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-text-video-button-meta-box',
		'title' => esc_html__( 'Text & Video Content', 'metabox-online-generator' ),
		'post_types' => array( 'guide-article-rccb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'tvb-feature-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Content Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Content's Image", 'metabox-online-generator' ),
			),
			array(
				'id' => 'tvb-text-content',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
			),
            array(
				'id' => 'tvb-video-url',
				'type' => 'url',
				'name' => esc_html__( 'Video URL', 'metabox-online-generator' ),
			),
            array(
				'id' => 'tvb-button-text',
				'type' => 'input',
				'name' => esc_html__( 'Button Text', 'metabox-online-generator' ),
			),
            array(
				'id' => 'tvb-button-url',
				'type' => 'url',
				'name' => esc_html__( 'Button URL', 'metabox-online-generator' ),
			),
            array(
				'id' => 'tvb-link-select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'text_video_button_meta_box' );

/* Highlight */
function highlight_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-highlight-meta-box',
		'title' => esc_html__( 'Highlight Content', 'metabox-online-generator' ),
		'post_types' => array( 'article-content', 'mp-article-content' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'highlight-content',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'highlight_meta_box' );

/* Image Box */
function mini_site_general_item_image_box_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-image-box-meta-box',
		'title' => esc_html__( 'Image Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb', 'album-content' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'image-box-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Content Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Content's Image", 'metabox-online-generator' ),
			)
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_image_box_meta_box' );

/* Background Image & Caption Box */
function mini_site_general_item_image_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-image-meta-box',
		'title' => esc_html__( 'Image Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb', 'album-content' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'mini_site_feature_image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Content Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Content's Image", 'metabox-online-generator' ),
			),
            array(
				'id' => 'image_text_content',
				'type' => 'textarea',
				'name' => esc_html__( 'Image Caption', 'metabox-online-generator' ),
				'desc' => esc_html__( "Text content to appear on image", 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_image_meta_box' );

/* Video Box */
function mini_site_general_item_video_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-video-meta-box',
		'title' => esc_html__( 'Video Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb', 'album-content' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
            array(
				'id' => 'video-feature-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Content Image', 'metabox-online-generator' ),
			),
			
            array(
				'id' => 'video-link',
				'type' => 'url',
				'name' => esc_html__( 'Video Link', 'metabox-online-generator' ),
                'placeholder' => ('http://www.saedconnect.com/'),
				'desc' => esc_html__( "Text content to appear on", 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_video_meta_box' );

/* Quicklinks Box */
function mini_site_general_item_quicklinks_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-quicklinks-meta-box',
		'title' => esc_html__( 'Quicklinks Content', 'metabox-online-generator' ),
		'post_types' => array( 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'quicklink-content',
				'type' => 'textarea',
				'name' => esc_html__( 'Intro Content', 'metabox-online-generator' ),
				'desc' => esc_html__( "Text content to appear on", 'metabox-online-generator' ),
			),
            array(
				'id' => 'quicklink-select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Content Type' ),
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
            array(
				'id' => 'quick-links',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Quick Links', 'metabox-online-generator' ),
				'options' => array(
					'link-text' => 'Quicklink Text',
					'quicklink-url' => 'Quicklink URL',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Quicklink', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_quicklinks_meta_box' );

/* Quote Box */
function mini_site_general_item_quote_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-quote-meta-box',
		'title' => esc_html__( 'Quote Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'quote-content',
				'type' => 'textarea',
				'name' => esc_html__( 'Quote Content', 'metabox-online-generator' ),
				'desc' => esc_html__( "Text content to appear on", 'metabox-online-generator' ),
			),
            array(
				'id' => 'name-of-quoter',
				'type' => 'input',
				'name' => esc_html__( 'Author', 'metabox-online-generator' ),
				'desc' => esc_html__( "Text content to appear on", 'metabox-online-generator' ),
			),
            array(
				'id' => 'description-of-quoter',
				'type' => 'textarea',
				'name' => esc_html__( 'Description Content', 'metabox-online-generator' ),
				'desc' => esc_html__( "Text content to appear on", 'metabox-online-generator' ),
			),
            array(
				'id' => 'quote-background-color',
				'name' => esc_html__( 'Background Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            array(
				'id' => 'quote-btn-text',
				'type' => 'text',
				'name' => esc_html__( 'Button Text', 'metabox-online-generator' ),
			),
            array(
				'id' => 'quote-btn-link',
				'type' => 'url',
				'name' => esc_html__( 'Button Link', 'metabox-online-generator' ),
			),
            array(
				'id' => 'quote-link-target',
				'name' => esc_html__( 'Where to open link', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_quote_meta_box' );

/* Event Box */
function mini_site_general_item_event_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-event-meta-box',
		'title' => esc_html__( 'Event Content', 'metabox-online-generator' ),
		'post_types' => array( 'content-block', 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'event-title',
				'type' => 'input',
				'name' => esc_html__( 'Title', 'metabox-online-generator' ),
			),
            array(
				'id' => 'event-date',
				'type' => 'date',
				'name' => esc_html__( 'Event Date', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
				'id' => 'event-description',
				'type' => 'textarea',
				'name' => esc_html__( 'Description', 'metabox-online-generator' ),
			),
            array(
				'id' => 'event-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Featured Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Content's Image", 'metabox-online-generator' ),
			),
            array(
				'id' => 'event-btns',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Buttons', 'metabox-online-generator' ),
				'rows' => 2,
				'options' => array(
					'footer-btn-text' => 'Button Text',
					'footer-btn-url' => 'Button URL',
					'footer-btn-type' => 'Button Type (1, 2, 3)',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Button', 'metabox-online-generator' ),
			),
            array(
				'id' => 'event-select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_event_meta_box' );

/* Article & Other General Lonks Box */
function mini_site_general_item_article_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-article-meta-box',
		'title' => esc_html__( 'Article & Other General Links Content', 'metabox-online-generator' ),
		'post_types' => array( 'content-block', 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(

            array(
				'id' => 'article-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Article Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Article's Featured Image", 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'select-article-type',
				'name' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'options' => array(
                    'general' => 'General',
					'podcast' => 'Podcast',
					'video' => 'Video',
				),
				'std' => 'text-content',
			),
            
            array(
				'id' => 'category-text',
				'type' => 'input',
				'name' => esc_html__( 'Content Category', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'article-publication-date',
				'type' => 'date',
				'name' => esc_html__( 'Event Date', 'metabox-online-generator' ),
                'timestamp' => false,
			),

            array(
				'id' => 'article-link',
				'type' => 'url',
				'name' => esc_html__( 'Artcle Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'artcle-select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_article_meta_box' );

/* Post Box */
function mini_site_general_item_post_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-permalink-meta-box',
		'title' => esc_html__( 'Post Content', 'metabox-online-generator' ),
		'post_types' => array( 'ms-general-item', 'section-footer', 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(

            array(
				'id' => 'post-feature-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Post Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Content's Image", 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'post-content',
				'type' => 'textarea',
				'name' => esc_html__( 'Post Description', 'metabox-online-generator' ),
			),

            array(
				'id' => 'artcle-link',
				'type' => 'url',
				'name' => esc_html__( 'Post Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'artcle-select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_post_meta_box' );

/* Call To Action Type I Box */
function mini_site_general_item_cta_1_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-cta-1-meta-box',
		'title' => esc_html__( 'Call To Action Type I', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
            array(
				'id' => 'cta-1--background-color',
				'name' => esc_html__( 'Background Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            
             array(
				'id' => 'cta-1--text-color',
				'name' => esc_html__( 'Text Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            
            array(
				'id' => 'cta-1-title',
				'type' => 'input',
				'name' => esc_html__( 'Title', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'cta-1-subtitle',
				'type' => 'textarea',
				'name' => esc_html__( 'Subtitle', 'metabox-online-generator' ),
			),

            array(
				'id' => 'cta-1-btns',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Buttons', 'metabox-online-generator' ),
				'rows' => 2,
				'options' => array(
					'footer-btn-text' => 'Button Text',
					'footer-btn-url' => 'Button URL',
					'footer-btn-type' => 'Button Type (1, 2, 3, 4)',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Button', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'cta-1-select-target',
				'name' => esc_html__( 'Links open in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_cta_1_meta_box' );

/* Call To Action Type II Box */
function mini_site_general_item_cta_2_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-cta-2-meta-box',
		'title' => esc_html__( 'Call To Action Type II', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
            array(
				'id' => 'cta-2-background-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Background Image', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'cta-2-title',
				'type' => 'input',
				'name' => esc_html__( 'Title', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'cta-2-subtitle',
				'type' => 'textarea',
				'name' => esc_html__( 'Subtitle', 'metabox-online-generator' ),
			),

            array(
				'id' => 'cta-2-btns',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Buttons', 'metabox-online-generator' ),
				'rows' => 2,
				'options' => array(
					'footer-btn-text' => 'Button Text',
					'footer-btn-url' => 'Button URL',
					'footer-btn-type' => 'Button Type (1, 2, 3)',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Button', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'cta-2-select-target',
				'name' => esc_html__( 'Links open in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_cta_2_meta_box' );

/* Image Gallery */
function mini_site_general_item_image_gallery_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-image-gallery-meta-box',
		'title' => esc_html__( 'Image Gallery Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'image-galley-subtitle',
				'type' => 'input',
				'name' => esc_html__( 'Subtitle', 'metabox-online-generator' ),
			),
            array(
				'id' => 'image_gallery_description',
				'type' => 'textarea',
				'name' => esc_html__( 'Gallery Description', 'metabox-online-generator' ),
			),
			array(
				'id' => 'image-gallery',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Image Gallery', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_image_gallery_meta_box' );

/* Text Gallery */
function mini_site_general_item_text_gallery_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-text-gallery-meta-box',
		'title' => esc_html__( 'Text Gallery Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'text-gallery',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Text Gallery', 'metabox-online-generator' ),
				'desc' => esc_html__( '0 = No icon on Button. 1 - Button with Icon', 'metabox-online-generator' ),
				'options' => array(
					'text-slide-title' => 'Slide Title',
					'text-slide-content' => 'Slide Content',
					'text-slide-btn-text' => 'Button Text',
					'text-slide-btn-url' => 'Button URL',
					'text-slide-btn-type' => 'Button with icon',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Slide', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_text_gallery_meta_box' );

/* Faculty/Team/Product Box */
function mini_site_general_item_ftp_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-ftp-meta-box',
		'title' => esc_html__( 'Faculty/Team/Product Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'fta_feature_image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Faculty/Team/Product Image', 'metabox-online-generator' ),
			),
            array(
				'id' => 'fta_text_description',
				'type' => 'textarea',
				'name' => esc_html__( 'Faculty/Team/Product Description', 'metabox-online-generator' ),
			),
            array(
				'id' => 'fta_text_url',
				'type' => 'url',
				'name' => esc_html__( 'Faculty/Team/Product URL', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_ftp_meta_box' );

/* Link Box */
function mini_site_general_item_internal_link_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-link-meta-box',
		'title' => esc_html__( 'Internal / External Content', 'metabox-online-generator' ),
		'post_types' => array( 'content-block', 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'link-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Link Featured Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Link's Featured Image", 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'link-description',
				'type' => 'textarea',
				'name' => esc_html__( 'Brief Description About Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'select-link-type',
				'name' => esc_html__( 'Select Link Type', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
                    'internal' => 'Internal Link',
					'external' => 'External Link',
				),
				'std' => 'text-content',
			),

            array(
				'id' => 'link-url',
				'type' => 'url',
				'name' => esc_html__( 'Link URL', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'internal-link-select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_internal_link_meta_box' );

/* Accordion Box */
function mini_site_general_item_accordion_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-accordion-meta-box',
		'title' => esc_html__( 'Accordion Content', 'metabox-online-generator' ),
		'post_types' => array( 'content-block', 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'accordion',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Accordion', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add Accordion items' ),
				'options' => array(
					'accordion-title' => 'Accordion Title',
					'accordion-content' => 'Accordion Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),   
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_accordion_meta_box' );

/* Accordion Box Unhidden */
function unhidden_accordion_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'accordion-meta-box',
		'title' => esc_html__( 'Accordion Content', 'metabox-online-generator' ),
		'post_types' => array( 'faq-article' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'accordion',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Accordion', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add Accordion items' ),
				'options' => array(
					'accordion-title' => 'Accordion Title',
					'accordion-content' => 'Accordion Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),   
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'unhidden_accordion_meta_box' );

/* Schedule Box */
function mini_site_general_item_schedule_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-schedule-meta-box',
		'title' => esc_html__( 'Schedule Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_schedule_meta_box' );

/* Profile/Faculty/Team Box */
function mini_site_general_item_pft_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-pft-meta-box',
		'title' => esc_html__( 'Profile / Faculty / Team Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'pft-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Profile Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Link's Featured Image", 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'pft-department',
				'type' => 'input',
				'name' => esc_html__( 'Department', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'pft-description',
				'type' => 'textarea',
				'name' => esc_html__( 'Brief Description About Profile', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'pft-button',
				'type' => 'input',
				'name' => esc_html__( 'Button Text', 'metabox-online-generator' ),
			),

            array(
				'id' => 'pft-url',
				'type' => 'url',
				'name' => esc_html__( 'Button URL', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'pft-link-select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),   
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_pft_meta_box' );

/* Get in Touch Box */
function mini_site_get_in_touch_block_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-get-in-touch-meta-box',
		'title' => esc_html__( 'Get in Touch Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'get-in-touch-title',
				'type' => 'input',
				'name' => esc_html__( 'Title', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'get-in-touch-background-color',
				'name' => esc_html__( 'Background Color', 'metabox-online-generator' ),
				'type' => 'color',
			),

            array(
				'id' => 'Contact-items',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Contact Persons', 'metabox-online-generator' ),
				'rows' => 2,
				'options' => array(
					'Contact-item-name' => 'Contact Person Name',
					'Contact-item-position' => 'Contact Person Position',
					'Contact-item-email' => 'Contact Person Email',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Contact Person', 'metabox-online-generator' ),
			),  
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_get_in_touch_block_meta_box' );

/* Footer Box */
function mini_site_general_item_footer_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-footer-meta-box',
		'title' => esc_html__( 'Footer Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'feature-background-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Background Image', 'metabox-online-generator' ),
			),

            array(
				'id' => 'footer-btns',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Footer Buttons', 'metabox-online-generator' ),
				'rows' => 2,
				'options' => array(
					'footer-btn-text' => 'Button Text',
					'footer-btn-url' => 'Button URL',
					'footer-btn-type' => 'Button Type (1, 2, 3)',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Button', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'artcle-select-target',
				'name' => esc_html__( 'Links open in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),   
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_footer_meta_box' );

/* Footer - Content Box */
function mini_site_footer_content_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-footer-contact-meta-box',
		'title' => esc_html__( 'Footer Content', 'metabox-online-generator' ),
		'post_types' => array( 'bottom-cb', 'right-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'footer-contact-title',
				'type' => 'input',
				'name' => esc_html__( 'Title', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'footer-contact-content',
				'type' => 'textarea',
				'name' => esc_html__( 'Content', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'footer-contact-background-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Background Image', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'footer-contact-facebook',
				'type' => 'url',
				'name' => esc_html__( 'Facebook link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'footer-contact-twitter',
				'type' => 'url',
				'name' => esc_html__( 'Twitter link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'footer-contact-telegram',
				'type' => 'url',
				'name' => esc_html__( 'Telegram link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'footer-contact-form',
				'type' => 'url',
				'name' => esc_html__( 'Contact Form Shortcode', 'metabox-online-generator' ),
			),
              
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_footer_content_meta_box' );

/* Button Group Box */
function mini_site_general_item_btn_group_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'gi-button-group-meta-box',
		'title' => esc_html__( 'Button Group', 'metabox-online-generator' ),
		'post_types' => array( 'content-block' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(

            array(
				'id' => 'btn-group',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Buttons', 'metabox-online-generator' ),
				'rows' => 2,
				'options' => array(
					'btn-text' => 'Button Text',
					'btn-url' => 'Button URL',
					'btn-type' => 'Button Type (1, 2, 3)',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Button', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'artcle-select-target',
				'name' => esc_html__( 'Links open in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),   
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_general_item_btn_group_meta_box' );



/*
*
*==========================================================================================================
* META BOX: ALl Metaboxes
*==========================================================================================================

/* Content Block */
function content_block_get_meta_box( $meta_boxes ) {
    
	$meta_boxes[] = array(
		'id' => 'course_metabox',
		'title' => esc_html__( 'Content Blocks', 'metabox-online-generator' ),
		'post_types' => array( 'discourse', 'topic-article', 'guide-article'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => false,
        
        /* Fields */
		'fields' => array(
            
            /* Content Block Group */
            array(
                'group_title' => array( 'field' => 'select-content-type'),
                'id'     => 'content-block-group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'collapsible' => true,
                'add_button' => 'Add Content Block',                
                'fields' => array(
                    array(
                        'name'            => 'Select Content Type',
                        'id'              => 'select-content-type',
                        'type'            => 'select',
                        'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
                        // Array of 'value' => 'Label' pairs
                        'options' => array(
                            'cb-heading-meta-box' => 'Heading',
                            'cb-plain-text-meta-box' => 'Text',
                            'cb-image-meta-box' => 'Image',
                            'cb-video-meta-box' => 'Video',
                            'cb-accordion-meta-box' => 'Accordion',
                            'cb-button-meta-box' => 'Buttons',
                            'cb-event-meta-box' => 'Events',
                            'cb-article-meta-box' => 'Article & Other General Links',
                            'cb-link-meta-box' => 'Internal / External Link',
                        ),
                        'columns' => 4,
                    ),
                    
                    /* Select Width */
                    array(
                        'id' => 'select-column-width',
                        'name' => esc_html__( 'Select Column Width', 'metabox-online-generator' ),
                        'type' => 'select',
                        'desc' => esc_html__( 'Select Content Width', 'metabox-online-generator' ),
                        'options' => array(
                            '12' => '1 (Full Width)',
                            '6' => '1/2 (Half Width)',
                            '4' => '1/3 (A Third)',
                        ),
                        'columns' => 4,
                    ),
                    
                    /* Field Group for Heading */
                    array(
                        'id'     => 'cb-heading-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-heading-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Heading',
                                'id'    => 'cb-heading-heading',
                                'type'  => 'text',
                                'columns' => 12,
                            ),
                        ),
                    ),
                    
                    /* Field Group for Image */
                    array(
                        'id'     => 'cb-image-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-image-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'cb-image-content',
                                'type' => 'image_advanced',
                                'max_file_uploads' => 1,
                                'name' => esc_html__( 'Image Content', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Video */
                    array(
                        'id'     => 'cb-video-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-video-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'YouTube Video ID',
                                'id'    => 'cb-video-id',
                                'type'  => 'input',
                                'columns' => 12,
                            ),
                        ),
                    ),
                    
                    /* Field Group for Plain Text */
                    array(
                        'id'     => 'cb-plain-text-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-plain-text-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'cb-plain-text-content',
                                'type' => 'wysiwyg',
                                'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                                        
                    /* Field Group for Accordion */
                    array(
                        'id'     => 'cb-accordion-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-accordion-meta-box' ),
                        'fields' => array(
                            // Accordion Group
                            array(
                                'group_title' => array( 'field' => 'cb-accordion-title'),
                                'id'     => 'cb-accordion',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'add_button' => 'Add Accordion Item',
                                'fields' => array(
                                    // Normal field (cloned)
                                    array(
                                        'name'  => 'Accordion Title',
                                        'id'    => 'cb-accordion-title',
                                        'type'  => 'text',
                                    ),
                                    
                                    array(
                                        'id' => 'cb-accordion-content',
                                        'type' => 'wysiwyg',
                                        'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Buttons */
                    array(
                        'id'     => 'cb-button-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-button-meta-box' ),
                        'fields' => array(
                            // Button Group
                            array(
                                'group_title' => array( 'field' => 'cb-btn-group'),
                                'id'     => 'cb-button',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'add_button' => 'Add Button',
                                'fields' => array(
                                    // Normal field (cloned)
                                    array(
                                        'name'  => 'Button Text',
                                        'id'    => 'cb-button-text',
                                        'type'  => 'text',
                                        'columns' => 4,
                                    ),
                                    
                                    array(
                                        'name'  => 'Button URL',
                                        'id'    => 'cb-button-url',
                                        'type'  => 'url',
                                        'columns' => 4,
                                    ),
                                    
                                    array(
                                        'name'            => 'Select Button Style',
                                        'id'              => 'cb-button-type',
                                        'type'            => 'select',
                                        'options' => array(
                                            '1' => 'Style 1',
                                            '2' => 'Style 2',
                                            '3' => 'Style 3',
                                        ),
                                        'columns' => 4,
                                    ),
                                    
                                    array(
                                        'id'        => 'cb-button-target',
                                        'name'      => 'Open link a in new tab?',
                                        'type'      => 'switch',

                                        // Style: rounded (default) or square
                                        'style'     => 'rounded',

                                        // On label: can be any HTML
                                        'on_label'  => 'Yes',

                                        // Off label
                                        'off_label' => 'No',
                                        'columns' => 4,
                                    ),

                                ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Events */
                    array(
                        'id'     => 'cb-event-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-event-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Events',
                                'id'    => 'cb-heading-heading',
                                'type'  => 'text',
                            ),
                        ),
                    ),
                    
                    /* Field Group for Article & Other General Links */
                    array(
                        'id'     => 'cb-article-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-article-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'cb-plain-text-content',
                                'type' => 'wysiwyg',
                                'name' => esc_html__( 'Article & Other General Links', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                    
                    /* Field Group for Internal / External Link */
                    array(
                        'id'     => 'cb-link-meta-box',
                        'type'   => 'group',
                        'hidden' => array( 'select-content-type', '!=', 'cb-link-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'cb-plain-text-content',
                                'type' => 'wysiwyg',
                                'name' => esc_html__( 'nternal / External Link', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                    
                    
                    
                ),
            ),
        
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'content_block_get_meta_box' );

/* Bottom Content Block */








