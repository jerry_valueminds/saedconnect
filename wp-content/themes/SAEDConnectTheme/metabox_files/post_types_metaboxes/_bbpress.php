<?php
/*
*
*==========================================================================================================
* META BOX: ALl Metaboxes
*==========================================================================================================

/* Text Box */
function bbpress_topic_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'bbp-topic-meta-box',
		'title' => esc_html__( 'Addional Information', 'metabox-online-generator' ),
		'post_types' => array( 'topic', 'forum' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'bbp-featured-image',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Featured Image', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'bbp-mentor-forum-title',
				'type' => 'text',
				'name' => esc_html__( 'Mentor Forum Name', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'bbp-mentor-forum-link',
				'type' => 'url',
				'name' => esc_html__( 'Mentor Forum Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'bbp-track-id',
				'type' => 'text',
				'name' => esc_html__( 'Track ID', 'metabox-online-generator' ),
				'decs' => esc_html__( 'This Topic/Forum will be featured on the Track with the referenced ID', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'bbpress_topic_meta_box' );