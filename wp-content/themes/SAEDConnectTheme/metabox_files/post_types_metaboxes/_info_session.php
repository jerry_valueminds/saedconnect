<?php
/*
*
*==========================================================================================================
* META BOX: Program Metaboxes
*==========================================================================================================
*
*/
function skilli_program_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-program-meta-box',
		'title' => esc_html__( 'Program Information', 'metabox-online-generator' ),
		'post_types' => array( 'program' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'skilli-program-name',
				'type' => 'text',
				'name' => esc_html__( 'Program Tagline', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'skilli-program-feautured-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( "Featured Image", "metabox-online-generator" ),
			),
            
            array(
                'id' => 'skilli-program-theme-color',
                'type' => 'color',
                'name' => 'Program Theme Color',
                // Add alpha channel?
                'alpha_channel' => true,
                // Color picker options. See here: https://automattic.github.io/Iris/.
                'js_options'    => array(
                    'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                ),
            ),
            
            array(
                'id' => 'skilli-program-alt-theme-color',
                'type' => 'color',
                'name' => 'Program Alt Theme Color',
                // Add alpha channel?
                'alpha_channel' => true,
                // Color picker options. See here: https://automattic.github.io/Iris/.
                'js_options'    => array(
                    'palettes' => array( '#d26819', '#3a4002', '#b55085', '#f4c026', '#ffffff', '#000' )
                ),
            ),
            
            array(
				'id' => 'skilli-program-template-type',
				'name' => esc_html__( 'Select Template Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Template Type', 'metabox-online-generator' ),
				'placeholder' => esc_html__( 'Select Template Type', 'metabox-online-generator' ),
				'options' => array(
					'program' => 'Program',
					'forum' => 'Forum',
				),
				'std' => 'text-content',
			),
            
            
            array(
				'id' => 'skilli-free-forum-url',
				'type' => 'url',
				'name' => esc_html__( 'Free Forum Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'skilli-forum-url',
				'type' => 'url',
				'name' => esc_html__( 'Mentor Forum Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'skilli-program-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Program Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'skilli-program-sponsor-images',
				'type' => 'image_advanced',
                //'max_file_uploads' => 1,
				'name' => esc_html__( "Suppporters' Images", "metabox-online-generator" ),
			),
            
            array(
                'type' => 'heading',
                'name' => 'Subscription',
            ),
            
            array(
				'id' => 'skilli-program-subscription-brief',
				'type' => 'textarea',
				'name' => esc_html__( 'Subscription Brief', 'metabox-online-generator' ),
			),
            
            array(
                'type' => 'heading',
                'name' => 'FAQ Section',
            ),
            
            array(
				'id' => 'skilli-program-faq',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'FAQ', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add FAQ' ),
				'options' => array(
					'accordion-title' => 'Title',
					'accordion-content' => 'Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'skilli_program_meta_box' );

/* What you get */
function skilli_program_what_you_get_meta_box( $meta_boxes ) {
    
	$meta_boxes[] = array(
		'id' => 'skilli_program_wyg_metabox',
		'title' => esc_html__( 'What you get', 'metabox-online-generator' ),
		'post_types' => array( 'program' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => false,
        
        /* Fields */
		'fields' => array(
            array(
                'name'            => 'Section Brief',
                'id'              => 'skilli-program-wyg-brief',
                'type'            => 'textarea',
            ),
            
            /* What You Get Group */
            array(
                'group_title' => array( 'field' => 'skilli-program-wyg-title'),
                'id'     => 'skilli_program_wyg_group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'collapsible' => true,
                'save_state' => true,
                'add_button' => 'Add Item',                
                'fields' => array(
                    array(
                        'name'            => 'Title',
                        'id'              => 'skilli-program-wyg-title',
                        'type'            => 'text',
                    ),
                    
                    array(
                        'id' => 'skilli-program-wyg-feautured-image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'name' => esc_html__( "Featured Image", "metabox-online-generator" ),
                    ),
                    
                    array(
                        'name'            => 'Content',
                        'id'              => 'skilli-program-wyg-content',
                        'type'            => 'textarea',
                    ),
                    
                    
                    
                ),
            ),
        
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'skilli_program_what_you_get_meta_box' );

/* How it works */
function skilli_program_how_it_works_meta_box( $meta_boxes ) {
    
	$meta_boxes[] = array(
		'id' => 'skilli_program_hiw_metabox',
		'title' => esc_html__( 'How it Works', 'metabox-online-generator' ),
		'post_types' => array( 'program' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => false,
        
        /* Fields */
		'fields' => array(
            array(
                'name'            => 'Section Brief',
                'id'              => 'skilli-program-hiw-brief',
                'type'            => 'textarea',
            ),
            
            /* How it Works Group */
            array(
                'group_title' => array( 'field' => 'skilli-program-hiw-title'),
                'id'     => 'skilli_program_hiw_group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'collapsible' => true,
                'save_state' => true,
                'add_button' => 'Add Item',                
                'fields' => array(
                    array(
                        'name'            => 'Title',
                        'id'              => 'skilli-program-hiw-title',
                        'type'            => 'text',
                    ),
                    
                    array(
                        'name'            => 'Content',
                        'id'              => 'skilli-program-hiw-content',
                        'type'            => 'textarea',
                    ),
                    
                    
                    
                ),
            ),
        
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'skilli_program_how_it_works_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Track Metaboxes
*==========================================================================================================
*
*/

/* Track Information */
function track_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-general-meta-box',
		'title' => esc_html__( 'Track Information', 'metabox-online-generator' ),
		'post_types' => array( 'track' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
            array(
				'id' => 'track-short-name',
				'type' => 'text',
				'name' => esc_html__( 'Track Short Name', 'metabox-online-generator' ),
			),
            
			array(
				'id' => 'program-name',
				'type' => 'text',
				'name' => esc_html__( 'Track Long Name', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-feautured-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Feautured Image', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-icon',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Track Icon', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-community-forum',
				'type' => 'url',
				'name' => esc_html__( 'Forum Community Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-mentor-forum',
				'type' => 'url',
				'name' => esc_html__( 'Mentor Forum Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-guide-url',
				'type' => 'url',
				'name' => esc_html__( 'Guide Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-duration',
				'type' => 'text',
				'name' => esc_html__( 'Program Duration', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-next-start-date',
				'type' => 'date',
				'name' => esc_html__( 'Next Start Date', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-fee',
				'type' => 'text',
				'name' => esc_html__( 'Program Fee', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-cta',
				'type' => 'url',
				'name' => esc_html__( 'Program CTA Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-sms-code',
				'type' => 'text',
				'name' => esc_html__( 'Enrollment SMS Code', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-form-shortcode',
				'type' => 'text',
				'name' => esc_html__( 'Enrollment Form Shortcode', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-mentor-ids',
				'type' => 'text',
				'name' => esc_html__( 'Mentor IDs', 'metabox-online-generator' ),
                'desc' => esc_html__( 'Enter a comma seperated list of User IDs', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Program Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-prerequisites',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Program Pre-requisites', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'program-faq',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'FAQ', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add FAQ' ),
				'options' => array(
					'accordion-title' => 'Title',
					'accordion-content' => 'Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),
            
            /* Field Group for Days */
            array(
                'id'     => 'program-course-days',
                'type'   => 'group',
                'name'   => 'Training Days',
                'group_title' => array( 'field' => 'program-course-day-title'),
                'save_state' => true,
                'clone'  => true,
                'sort_clone' => true,
                'collapsible' => true,
                'add_button' => 'Add Day',
                
                'fields' => array(
                    
                    array(
                        'id' => 'program-course-day-title',
                        'type' => 'text',
                        'name' => esc_html__( 'Day(Range)', 'metabox-online-generator' ),
                    ),

                     /* Field Group for Text */
                    array(
                        'id'     => 'program-courses',
                        'type'   => 'group',
                        'group_title' => array( 'field' => 'program-course-title'),
                        'save_state' => true,
                        'clone'  => true,
                        'sort_clone' => true,
                        'collapsible' => true,
                        'add_button' => 'Add Course', 
                        
                        'fields' => array(

                            array(
                                'id' => 'program-course-title',
                                'type' => 'text',
                                'name' => esc_html__( 'Course Title', 'metabox-online-generator' ),
                            ),

                            array(
                                'id' => 'program-course-content',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Content', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                ),
            ),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'track_meta_box' );

/* Connected Forums */
function info_session_get_meta_box( $meta_boxes ) {
    
	$meta_boxes[] = array(
		'id' => 'connected_topics_metabox',
		'title' => esc_html__( 'Connect Forum Topics', 'metabox-online-generator' ),
		'post_types' => array( 'track' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => false,
        
        /* Fields */
		'fields' => array(
            
            /* Content Block Group */
            array(
                'group_title' => array( 'field' => 'group-title'),
                'id'     => 'connected-forum-topics-group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'save_state' => 'true',
                'collapsible' => true,
                'add_button' => 'Add Topic Group Block',                
                'fields' => array(
                    
                    /* Group title */
                    array(
                        'name'            => 'Group title',
                        'id'              => 'group-title',
                        'type'            => 'text',
                    ),
                    
                    array(
                        'name'            => 'Group title URL',
                        'id'              => 'group-title-url',
                        'type'            => 'url',
                    ),
                                        
                    /* Field Group for Accordion */
                    array(
                        'id'     => 'forum-topics-group',
                        'type'   => 'group',
                        'fields' => array(
                            // Accordion Group
                            array(
                                'group_title' => array( 'field' => 'forum-topic-title'),
                                'id'     => 'forum-topic-item',
                                'type'   => 'group',
                                'clone'  => true,
                                'sort_clone' => true,
                                'collapsible' => true,
                                'save_state' => 'true',
                                'add_button' => 'Add Topic',
                                'fields' => array(
                                    // Normal field (cloned)
                                    array(
                                        'name'  => 'Topic Title',
                                        'id'    => 'forum-topic-title',
                                        'type'  => 'text',
                                    ),
                                    
                                    array(
                                        'id' => 'forum-topic-url',
                                        'type' => 'url',
                                        'name' => esc_html__( 'Topic URL', 'metabox-online-generator' ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                   

                    
                    
                    
                ),
            ),
        
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_session_get_meta_box' );

/* Track Subscription Content */
function track_subscription_block_get_meta_box( $meta_boxes ) {
    
	$meta_boxes[] = array(
		'id' => 'track_subscription_metabox',
		'title' => esc_html__( 'Subscription Blocks', 'metabox-online-generator' ),
		'post_types' => array( 'track' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => false,
        
        /* Fields */
		'fields' => array(
            
            /* Content Block Group */
            array(
                'group_title' => array( 'field' => 'select-subscription-type, subscription-title', 'separator' => ' - '),
                'id'     => 'subscription-block-group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'collapsible' => true,
                'save_state' => 'true',
                'add_button' => 'Add Subscription Block',                
                'fields' => array(
                    
                    /* Subscription Block Type */
                    array(
                        'name'            => 'Select Subscription Type',
                        'id'              => 'select-subscription-type',
                        'type'            => 'select',
                        // Array of 'value' => 'Label' pairs
                        'options' => array(
                            'paid-subscription-meta-box' => 'Paid Subscription',
                            'image-subscription-meta-box' => 'Image',
                            'text-subscription-meta-box' => 'Text-content',
                        ),
                        'columns' => 4,

                    ),
                    
                    /* Subscription Block Title*/
                    array(
                        'id' => 'subscription-title',
                        'name' => esc_html__( 'Subscription Title', 'metabox-online-generator' ),
                        'type' => 'text',
                        'columns' => 4,
                    ),
                    
                    /* Select Width */
                    array(
                        'id' => 'sb-select-column-width',
                        'name' => esc_html__( 'Select Column Width', 'metabox-online-generator' ),
                        'type' => 'select',
                        'options' => array(
                            '12' => '1 (Full Width)',
                            '6' => '1/2 (Half Width)',
                            '4' => '1/3 (A Third)',
                        ),
                        'columns' => 4,
                    ),
                    
                    /* Field Group for Paid Subscription */
                    array(
                        'id'     => 'paid-subscription-meta-box',
                        'type'   => 'group',
                        'save_state' => 'true',
                        'hidden' => array( 'select-subscription-type', '!=', 'paid-subscription-meta-box' ),
                        'fields' => array(
                            array(
                                'name'  => 'Fee',
                                'id'    => 'paid-subscription-fee',
                                'type'  => 'text',
                                'columns' => 12,
                            ),
                            
                            array(
                                'name'  => 'Description',
                                'id'    => 'paid-subscription-description',
                                'type'  => 'textarea',
                                'columns' => 12,
                            ),
                        ),
                    ),
                    
                    /* Field Group for Image Message */
                    array(
                        'id'     => 'image-subscription-meta-box',
                        'type'   => 'group',
                        'save_state' => 'true',
                        'hidden' => array( 'select-subscription-type', '!=', 'image-subscription-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'image-subscription-image',
                                'type' => 'image_advanced',
                                'max_file_uploads' => 1,
                                'name' => esc_html__( 'Featured Image', 'metabox-online-generator' ),
                            ),
                            
                            array(
                                'name'  => 'Description',
                                'id'    => 'image-subscription-description',
                                'type'  => 'textarea',
                                'columns' => 12,
                            ),
                        ),
                    ),
                    
                    /* Field Group for Text Message */
                    array(
                        'id'     => 'text-subscription-meta-box',
                        'type'   => 'group',
                        'save_state' => 'true',
                        'hidden' => array( 'select-subscription-type', '!=', 'text-subscription-meta-box' ),
                        'fields' => array(
                            array(
                                'id' => 'text-subscription-content',
                                'type' => 'textarea',
                                'name' => esc_html__( 'Text Content', 'metabox-online-generator' ),
                            ),
                        ),
                    ),
                    
                    array(
                        'name'  => 'Button Text',
                        'id'    => 'subscription-button-text',
                        'type'  => 'text',
                        'columns' => 4,
                    ),

                    array(
                        'name'  => 'Button Link',
                        'id'    => 'subscription-button-link',
                        'type'  => 'url',
                        'columns' => 4,
                    ),
                    
                    
                    
                ),
            ),
        
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'track_subscription_block_get_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Curriculum Metaboxes
*==========================================================================================================
*
*/
function program_curriculum_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-micro-course-meta-box',
		'title' => esc_html__( 'Curriculum Information', 'metabox-online-generator' ),
		'post_types' => array( 'curriculum' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
            array(
				'id' => 'curriculum-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Curriculum Description', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'program_curriculum_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Start Date Metaboxes
*==========================================================================================================
*
*/
function program_schedule_date_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-micro-course-meta-box',
		'title' => esc_html__( 'Schedule a Program', 'metabox-online-generator' ),
		'post_types' => array( 'program-schedule' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
            array(
				'id' => 'program-schedule-date',
				'type' => 'date',
				'name' => esc_html__( 'Select a Start Date', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'program_schedule_date_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Question Metaboxes
*==========================================================================================================
*
*/
function question_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'question-meta-box',
		'title' => esc_html__( 'Question Information', 'metabox-online-generator' ),
		'post_types' => array( 'question' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
            array(
				'id' => 'question-description',
				'type' => 'textarea',
				'name' => esc_html__( 'Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'question-featured-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Featured Image', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'question_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Answer Metaboxes
*==========================================================================================================
*
*/
function answer_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'question-meta-box',
		'title' => esc_html__( 'Answer Information', 'metabox-online-generator' ),
		'post_types' => array( 'answer' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'answer-type',
				'name' => esc_html__( 'Select Media Type', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'audio' => 'Audio',
					'video' => 'Video',
				),
				'std' => 'text-content',
			),
            
            array(
				'id' => 'answer-summary',
				'type' => 'textarea',
				'name' => esc_html__( 'Summary', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'answer-content',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'answer-audio-media',
				'type' => 'url',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Audio', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'answer-video-media',
				'type' => 'url',
				'name' => esc_html__( 'Video link', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'answer_meta_box' );

















/*
*
*==========================================================================================================
* META BOX: Information Session Metaboxes
*==========================================================================================================
*
*/
function info_session_general_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-general-meta-box',
		'title' => esc_html__( 'Session Information', 'metabox-online-generator' ),
		'post_types' => array( 'information-session' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'info-session-name',
				'type' => 'text',
				'name' => esc_html__( 'Session Name', 'metabox-online-generator' ),
                'desc' => esc_html__( 'Example: Poultry Farming Business', 'metabox-online-generator' )
			),
            
            array(
				'id' => 'info-session-practitioner',
				'type' => 'text',
				'name' => esc_html__( 'Practitioner', 'metabox-online-generator' ),
                'desc' => esc_html__( 'Example: Poultry Farmer', 'metabox-online-generator' )
			),
            
            array(
				'id' => 'info-session-feautured-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Feautured Image', 'metabox-online-generator' ),
			),
            
            array(
                'id' => 'info-session-theme-color',
                'type' => 'color',
                'name' => 'Program Theme Color',
                // Add alpha channel?
                'alpha_channel' => true,
                // Color picker options. See here: https://automattic.github.io/Iris/.
                'js_options'    => array(
                    'palettes' => array( '#125', '#459', '#78b', '#ab0', '#de3', '#f0f' )
                ),
            ),
            
            array(
                'id' => 'info-session-theme-alt-color',
                'type' => 'color',
                'name' => 'Program Alternate Theme Color',
                // Add alpha channel?
                'alpha_channel' => true,
                // Color picker options. See here: https://automattic.github.io/Iris/.
                'js_options'    => array(
                    'palettes' => array( '#125', '#459', '#78b', '#ab0', '#de3', '#f0f' )
                ),
            ),
            
            array(
				'id' => 'info-session-community-forum',
				'type' => 'url',
				'name' => esc_html__( 'Forum Community Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-session-mentor-forum',
				'type' => 'url',
				'name' => esc_html__( 'Mentor Forum Link', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-session-mentor-forum-fee',
				'type' => 'text',
				'name' => esc_html__( 'Mentor Forum Fee', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-session-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-session-form',
				'type' => 'text',
				'name' => esc_html__( 'Enrollment Form Shortcode', 'metabox-online-generator' ),
			),
            
            array(
                'type' => 'heading',
                'name' => 'Sessions Essentials Section',
                'desc' => 'Optional description',
            ),
            
            array(
				'id' => 'info-session-essentials',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Session Essentials', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add Session Essentials' ),
				'options' => array(
					'accordion-title' => 'Title',
					'accordion-content' => 'Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),
            
                        array(
                'type' => 'divider',
            ),

            array(
                'type' => 'heading',
                'name' => 'Community Perks Section',
                'desc' => 'Select as applicable to this session',
            ),
            
            array(
                'id'   => 'info-session-perk-1',
                'type' => 'checkbox',
                'name' => esc_html__( 'Community Perk 1', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'info-session-perk-2',
                'type' => 'checkbox',
                'name' => esc_html__( 'Community Perk 2', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'info-session-perk-3',
                'type' => 'checkbox',
                'name' => esc_html__( 'Community Perk 3', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'type' => 'heading',
                'name' => 'Schedule Sessions',
                'desc' => 'Optional description',
            ),
            
            array(
				'id' => 'info-session-start-date',
				'type' => 'date',
				'name' => esc_html__( 'Starting Out Date', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
				'id' => 'info-session-question-date',
				'type' => 'date',
				'name' => esc_html__( 'Need Help Date', 'metabox-online-generator' ),
                'timestamp' => true,
			),
            
            array(
                'type' => 'divider',
            ),

            array(
                'type' => 'heading',
                'name' => 'What am I getting Section',
                'desc' => 'Optional description',
            ),
            
            array(
                'id'   => 'info-session-help-1',
                'type' => 'checkbox',
                'name' => esc_html__( 'What you are getting 1', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'info-session-help-2',
                'type' => 'checkbox',
                'name' => esc_html__( 'What you are getting 2', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'info-session-help-3',
                'type' => 'checkbox',
                'name' => esc_html__( 'What you are getting 3', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),

            array(
                'type' => 'divider',
            ),
            
            array(
                'type' => 'heading',
                'name' => 'Micro Sessions',
                'desc' => 'Add Micro Sessions to this Information Session',
            ),
            
            array(
				'id' => 'info-session-micro-sessions',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Micro Session', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add a Micro Session' ),
				'options' => array(
					'micro-session-title' => 'Title',
					'micro-session-desc' => 'Content',
					'micro-session-form' => 'Micro Session Form Shortcode',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add session', 'metabox-online-generator' ),
			),
            
            array(
                'type' => 'divider',
            ),
            
            array(
                'type' => 'heading',
                'name' => 'FAQ Section',
                'desc' => 'Optional description',
            ),
            
            array(
				'id' => 'info-session-faq',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'FAQ', 'metabox-online-generator' ),
				'desc' => esc_html__( 'Add FAQ' ),
				'options' => array(
					'accordion-title' => 'Title',
					'accordion-content' => 'Content',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add item', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_session_general_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Information Session - Micro Course Metaboxes
*==========================================================================================================
*
*/
function info_sess_micro_course_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-micro-course-meta-box',
		'title' => esc_html__( 'Micro Course Information', 'metabox-online-generator' ),
		'post_types' => array( 'micro-course' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'micro-course-name',
				'type' => 'text',
				'name' => esc_html__( 'Course Name', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Short Description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-goal',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'What will you learn?', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-objectives',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Outcome Objectives', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-project',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Project', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-Requirement',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Required Tools', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-pre-requisites',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Pre-Requisites', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-schedules',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Schedules', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'micro-course-fee',
				'type' => 'text',
				'name' => esc_html__( 'Fee', 'metabox-online-generator' ),
			),
            
            array(
                'type' => 'heading',
                'name' => 'Learning Model',
                'desc' => 'Select applicable model',
            ),
            
            array(
                'id'   => 'micro-course-model-online',
                'type' => 'checkbox',
                'name' => esc_html__( 'Fully Online', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'micro-course-model-offline',
                'type' => 'checkbox',
                'name' => esc_html__( 'Offline', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'id'   => 'micro-course-model-blended',
                'type' => 'checkbox',
                'name' => esc_html__( 'Blended', 'metabox-online-generator' ),
                'std'  => 0, // 0 or 1
            ),
            
            array(
                'type' => 'divider',
            ),

            
            array(
				'id' => 'micro-course-locations',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Locations Available', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_sess_micro_course_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Information Session - Mentor Metaboxes
*==========================================================================================================
*
*/
function info_sess_mentor_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-mentor-meta-box',
		'title' => esc_html__( 'Mentor Information', 'metabox-online-generator' ),
		'post_types' => array( 'info-session-mentor' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'info-sess-mentor-name',
				'type' => 'text',
				'name' => esc_html__( 'Mentor Name', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-sess-mentor-desc',
				'type' => 'textarea',
				'name' => esc_html__( 'Brief Information', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-sess-mentor-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Profile Image', 'metabox-online-generator' ),
			),
  
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_sess_mentor_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Information Session - Testimonial Metaboxes
*==========================================================================================================
*
*/
function info_sess_testimonial_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'info-sess-testimonial-meta-box',
		'title' => esc_html__( 'Testimonial Information', 'metabox-online-generator' ),
		'post_types' => array( 'info-session-testi' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'info-sess-testimonial-name',
				'type' => 'text',
				'name' => esc_html__( 'Testimonial Author', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-sess-testimonial-desc',
				'type' => 'textarea',
				'name' => esc_html__( 'Author description', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'info-sess-testimonial-quote',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Quote', 'metabox-online-generator' ),
			),
  
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'info_sess_testimonial_meta_box' );