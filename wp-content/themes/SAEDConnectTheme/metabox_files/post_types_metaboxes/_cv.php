<?php
/*
*
*==========================================================================================================
* META BOX: Personal Information Metaboxes
*==========================================================================================================
*
*/
function cv_personal_information_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'cv-personal-info-meta-box',
		'title' => esc_html__( 'Personal Information', 'metabox-online-generator' ),
		'post_types' => array( 'cv-personal-info' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'cv-personal-info-dob',
				'type' => 'date',
				'name' => esc_html__( 'Date of Birth', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'cv-personal-info-marital',
				'name' => esc_html__( 'Marital Status', 'metabox-online-generator' ),
				'type' => 'select',
				'placeholder' => esc_html__( 'Marital Status', 'metabox-online-generator' ),
				'options' => array(
                    'marital-status-single' => 'Single',
					'marital-status-married' => 'Married',
                    'marital-status-divorced' => 'Divorced',
				),
				'std' => 'text-content',
			),
            
            array(
				'id' => 'cv-personal-info-gender',
				'name' => esc_html__( 'Gender', 'metabox-online-generator' ),
				'type' => 'select',
				'placeholder' => esc_html__( 'Gender', 'metabox-online-generator' ),
				'options' => array(
                    'marital-status-male' => 'Male',
					'marital-status-female' => 'Female',
				),
				'std' => 'text-content',
			),
            
            array(
				'id' => 'cv-personal-info-nationality',
				'type' => 'text',
				'name' => esc_html__( 'Nationality', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'cv_personal_information_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Summary Metaboxes
*==========================================================================================================
*
*/
function cv_summary_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'cv-personal-info-meta-box',
		'title' => esc_html__( 'Summary Information', 'metabox-online-generator' ),
		'post_types' => array( 'cv-summary' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'cv-summary-headline',
				'type' => 'text',
				'name' => esc_html__( 'Headline', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'cv-summary-about',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Summary about yourself', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'cv_summary_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Summary Metaboxes
*==========================================================================================================
*
*/
function cv_contact_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'cv-personal-info-meta-box',
		'title' => esc_html__( 'Contact Information', 'metabox-online-generator' ),
		'post_types' => array( 'cv-contact' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'cv-contact-email',
				'type' => 'email',
				'name' => esc_html__( 'Email', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-contact-phone',
				'type' => 'text',
				'name' => esc_html__( 'Telephone', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-contact-address',
				'type' => 'textarea',
				'name' => esc_html__( 'Address', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-contact-state',
				'type' => 'text',
				'name' => esc_html__( 'Current State of Residence', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-contact-website',
				'type' => 'url',
				'name' => esc_html__( 'Personal Blog/Website', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-contact-facebook',
				'type' => 'url',
				'name' => esc_html__( 'Facebook', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-contact-twitter',
				'type' => 'url',
				'name' => esc_html__( 'Twitter', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-contact-instagram',
				'type' => 'url',
				'name' => esc_html__( 'Instagram', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-contact-linkedin',
				'type' => 'url',
				'name' => esc_html__( 'Linkedin', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'cv_contact_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Experience Metaboxes
*==========================================================================================================
*
*/
function cv_experience_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'cv-experience-meta-box',
		'title' => esc_html__( 'Experience Information', 'metabox-online-generator' ),
		'post_types' => array( 'cv-experience' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'cv-experience-Position',
				'type' => 'text',
				'name' => esc_html__( 'Position/Job Title', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-experience-website',
				'type' => 'url',
				'name' => esc_html__( 'Company/Company Website', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-experience-location',
				'type' => 'text',
				'name' => esc_html__( 'Location', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-experience-start-date',
				'type' => 'date',
				'name' => esc_html__( 'Start Month/Start Year', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-experience-end-date',
				'type' => 'date',
				'name' => esc_html__( 'End Month/Year', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-experience-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Describe what you did/Your Achievements. ', 'metabox-online-generator' ),
			),
            
            

		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'cv_experience_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Personal Projects Metaboxes
*==========================================================================================================
*
*/
function cv_personal_projects_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'cv-experience-meta-box',
		'title' => esc_html__( 'Project Information', 'metabox-online-generator' ),
		'post_types' => array( 'cv-personal-projects' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'cv-personal-project-name',
				'type' => 'text',
				'name' => esc_html__( 'Project Name', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-personal-project-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Description', 'metabox-online-generator' ),
			),
            
            

		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'cv_personal_projects_meta_box' );

/*
*
*==========================================================================================================
* META BOX: Education & Training Metaboxes
*==========================================================================================================
*
*/
function cv_education_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'cv-experience-meta-box',
		'title' => esc_html__( 'Education/Training Information', 'metabox-online-generator' ),
		'post_types' => array( 'cv-education' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			
			array(
				'id' => 'cv-education-degree',
				'type' => 'text',
				'name' => esc_html__( 'Degree Attained (e.g BA, BS, JD, PhD)', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-education-school',
				'type' => 'text',
				'name' => esc_html__( 'School', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-education-field',
				'type' => 'text',
				'name' => esc_html__( 'Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-education-start-date',
				'type' => 'text',
				'name' => esc_html__( 'Start Month/Year', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-education-end-date',
				'type' => 'text',
				'name' => esc_html__( 'End Month/Year (Current students: Enter your expected graduation year)', 'metabox-online-generator' ),
			),
            array(
				'id' => 'cv-education-achievements',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Achievements', 'metabox-online-generator' ),
			),
            
            

		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'cv_education_meta_box' );


/*
*
*==========================================================================================================
* META BOX: Career Role Metaboxes
*==========================================================================================================
*
*/
function career_role_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'career-role-meta-box',
		'title' => esc_html__( 'Assign Tasks to this role', 'metabox-online-generator' ),
		'post_types' => array( 'career-role' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
            array(
                'name'        => 'Select Tasks',
                'id'          => 'career-role-tasks',
                'type'        => 'post',

                // Post type.
                'post_type'   => 'career-task',

                // Field type.
                'field_type'  => 'checkbox_list',

                // Placeholder, inherited from `select_advanced` field.
                'placeholder' => 'Select Tasks',

                // Query arguments. See https://codex.wordpress.org/Class_Reference/WP_Query
                'query_args'  => array(
                    'post_status'    => 'publish',
                    'posts_per_page' => - 1,
                ),
            ),
            
            

		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'career_role_meta_box' );