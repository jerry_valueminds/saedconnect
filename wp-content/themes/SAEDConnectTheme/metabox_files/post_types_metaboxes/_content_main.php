<?php
/*
*
*==========================================================================================================
* META BOX: Main-Content Settings
*==========================================================================================================
*
*/
function mini_site_main_content_general_meta_box( $meta_boxes ) {
	$prefix = 'general-item-content-';

	$meta_boxes[] = array(
		'id' => 'general-settings',
		'class' => 'general-item-content-',
		'title' => esc_html__( 'General Settings', 'metabox-online-generator' ),
		'post_types' => array( 'content-block' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'select-content-type',
				'name' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'placeholder' => esc_html__( 'Select content Type', 'metabox-online-generator' ),
				'options' => array(
                    'gi-heading-meta-box' => 'Heading',
					'gi-plain-text-meta-box' => 'Text',
					'gi-image-meta-box' => 'Image',
					'gi-video-meta-box' => 'Video',
                    'gi-accordion-meta-box' => 'Accordion',
                    'gi-button-group-meta-box' => 'Buttons',
                    'gi-event-meta-box' => 'Events',
					'gi-article-meta-box' => 'Article & Other General Links',
                    'gi-link-meta-box' => 'Internal / External Link',
				),
				'std' => 'text-content',
			),
            array(
				'id' => 'select-column-width',
				'name' => esc_html__( 'Select Column Width', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Content Width', 'metabox-online-generator' ),
				'options' => array(
					'12' => '1 (Full Width)',
					'6' => '1/2 (Half Width)',
					'4' => '1/3 (A Third)',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'mini_site_main_content_general_meta_box' );






