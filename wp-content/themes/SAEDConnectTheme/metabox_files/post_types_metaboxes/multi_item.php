
<?php
/*
 * Mini Site Meta Box
 */
function multiitem_get_meta_box( $meta_boxes ) {
	$prefix = 'prefix-';

	$meta_boxes[] = array(
		'id' => 'mini_site_metabox',
		'title' => esc_html__( 'Additional Fields', 'metabox-online-generator' ),
		'post_types' => array( 'multiitem' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'mini_site_feature_image',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Mini Site Featured Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Mini Site's Featured Image", 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'start_date',
				'type' => 'date',
				'name' => esc_html__( 'Start Date', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Mini Sites's Start Date", 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'duration',
				'type' => 'number',
				'name' => esc_html__( 'Duration', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Mini Site's Duration", 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'expiry_date',
				'type' => 'date',
				'name' => esc_html__( 'Expiry Date', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Mini Sites's Expiry Date", 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'end_date_copy_4',
				'type' => 'date',
				'name' => esc_html__( 'End Date', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Mini Sites's End Date", 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'convener',
				'type' => 'text',
				'name' => esc_html__( 'Convener', 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'convener_link',
				'type' => 'url',
				'name' => esc_html__( 'Convener Info Link', 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'source_site_name',
				'type' => 'text',
				'name' => esc_html__( 'Source Site Name', 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'source_site_info_link',
				'type' => 'url',
				'name' => esc_html__( 'Source Site Info Link', 'metabox-online-generator' ),
			),
			array(
				'id' => $prefix . 'link_color',
				'name' => esc_html__( 'Link Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
			array(
				'id' => $prefix . 'default_page_link',
				'type' => 'url',
				'name' => esc_html__( 'Default Page Link', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'multiitem_get_meta_box' );





