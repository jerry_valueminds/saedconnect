
<?php
/*
 * Multi Page Meta Box
 */
function multi_page_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'multi-page-metabox',
		'title' => esc_html__( 'Multi Page Settings', 'metabox-online-generator' ),
		'post_types' => array( 'multi-page' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'select-multi-page-type',
				'name' => esc_html__( 'Select Multi-Page Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Note: When a Single Page is selected, Only the First Article added below is visible on the Front-end', 'metabox-online-generator' ),
				'options' => array(
					'single-page' => 'Single Page',
					'multi-page' => 'Multi Pages',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'multi_page_get_meta_box' );

/*
 * Multi Page Article Meta Box
 */
function multi_page_article_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'multi-page-metabox',
		'title' => esc_html__( 'Article Content', 'metabox-online-generator' ),
		'post_types' => array( 'multi-page-article' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'multi-page-article-title',
				'type' => 'text',
				'name' => esc_html__( 'Caption', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'multi-page-article-content',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Brief Description.', 'metabox-online-generator' ),
                'desc' => esc_html__( 'Brief Description. This only displays on a Single Page' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'multi_page_article_get_meta_box' );


