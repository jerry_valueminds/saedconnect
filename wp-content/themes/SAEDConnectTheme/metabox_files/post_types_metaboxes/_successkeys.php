<?php
/*
*
*==========================================================================================================
* META BOX: Opportunity Metaboxe
*==========================================================================================================
*
*/
function success_keys_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'opportunity-meta-box',
		'title' => esc_html__( 'Course Summary', 'metabox-online-generator' ),
		'post_types' => array( 'successkeys-course'),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
			array(
				'id' => 'successkeys-course-title',
				'type' => 'text',
				'name' => esc_html__( 'Course Title', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'successkeys-course-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Image', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'successkeys-course-theme-color',
				'name' => esc_html__( 'Background Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            
            array(
				'id' => 'successkeys-course-description',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Course Description', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'success_keys_meta_box' );