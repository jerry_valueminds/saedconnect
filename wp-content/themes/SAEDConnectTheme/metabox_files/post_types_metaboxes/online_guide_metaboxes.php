
<?php
/*
 * Section Meta Box
 */
function section_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'section-metabox',
		'title' => esc_html__( 'Section Settings', 'metabox-online-generator' ),
		'post_types' => array( 'guide-section'),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'section-track-link',
				'name' => esc_html__( 'Track Link', 'metabox-online-generator' ),
				'type' => 'url',
			),
            array(
				'id' => 'section-fire-side-link',
				'name' => esc_html__( 'Fire Side Chat Link', 'metabox-online-generator' ),
				'type' => 'url',
			),
            array(
				'id' => 'section-mentor-hub-link',
				'name' => esc_html__( 'Mentor Hub Link', 'metabox-online-generator' ),
				'type' => 'url',
			),
            array(
				'id' => 'section-theme-color',
				'name' => esc_html__( 'Theme Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            array(
				'id' => 'section-link-hover-color',
				'name' => esc_html__( 'Link Hover Color', 'metabox-online-generator' ),
				'type' => 'color',
			),
            array(
				'id' => 'online-guide-featured-image',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Online Guide Featured Image', 'metabox-online-generator' ),
				'desc' => esc_html__( "This Online Guide's Featured Image", 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'section_get_meta_box' );

/*
 * Topic Area Meta Box
 */
function topic_area_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'article-metabox',
		'title' => esc_html__( 'Topic Settings', 'metabox-online-generator' ),
		'post_types' => array( 'topic-area' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'topic-area-type',
				'name' => esc_html__( 'Topic Area Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Topic Area Type', 'metabox-online-generator' ),
				'options' => array(
					'guide' => 'Guide',
					'advice' => 'Related Advice',
					'faq' => 'FAQ',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'topic_area_get_meta_box' );

/*
 * Related Advice Meta Box
 */
function related_advice_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'topic-metabox',
		'title' => esc_html__( 'Related Advice details', 'metabox-online-generator' ),
		'post_types' => array( 'ex-multi-item-cb' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'related-advice-image',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Featured Image', 'metabox-online-generator' ),
			),
            array(
				'id' => 'related-advice-source',
				'type' => 'text',
				'name' => esc_html__( 'Source Name', 'metabox-online-generator' ),
			),
            array(
				'id' => 'related-advice-source-url',
				'type' => 'text',
				'name' => esc_html__( 'Source URL', 'metabox-online-generator' ),
			),
			array(
				'id' => 'related-advice-content-type',
				'name' => esc_html__( 'Content Type ', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'options' => array(
					'article' => 'Article',
					'podcast' => 'Podcast',
					'video' => 'Video',
				),
				'std' => 'text-content',
			),
            array(
				'id' => 'select-target',
				'name' => esc_html__( 'Link opens in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			),  
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'related_advice_get_meta_box' );

/*
 * Article Meta Box
 */
function article_get_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'article-metabox',
		'title' => esc_html__( 'Article Hierarchy', 'metabox-online-generator' ),
		'post_types' => array( 'article', 'topic-article', 'guide-article' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            array(
				'id' => 'article-hierarchy-level',
				'name' => esc_html__( 'Article Hierarchy Level ', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Article Hierarchy Level', 'metabox-online-generator' ),
				'options' => array(
					'10' => '1',
					'40' => '2',
					'80' => '3',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'article_get_meta_box' );

/**
 * Article Content Meta Box
 */
function article_content_meta_box( $meta_boxes ) {
	$prefix = 'general-item-content-';

	$meta_boxes[] = array(
		'id' => 'general-settings',
		'class' => 'general-item-content-',
		'title' => esc_html__( 'General Settings', 'metabox-online-generator' ),
		'post_types' => array( 'article-content' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'select-content-type',
				'name' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'placeholder' => esc_html__( 'Select content Type', 'metabox-online-generator' ),
				'options' => array(
                    'gi-plain-text-meta-box' => 'Text box',
                    'gi-image-meta-box' => 'Image',
                    'gi-video-meta-box' => 'Video',
                    'gi-highlight-meta-box' => 'Highlight',
                    'gi-accordion-meta-box' => 'Accordion',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'article_content_meta_box' );

function article_resources_function_meta_box( $meta_boxes ) {
    
	$meta_boxes[] = array(
		'id' => 'article_resources_metabox',
		'title' => esc_html__( 'Tools & Resources', 'metabox-online-generator' ),
		'post_types' => array( 'mini-site', 'track', 'guide-topic' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => false,
        
        /* Fields */
		'fields' => array(
            
            /* Content Block Group */
            array(
                'group_title' => array( 'field' => 'resource-content-type, resource-title', 'separator' => ': '),
                'id'     => 'article-resources-group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'collapsible' => true,
                'save_state' => true,
                'add_button' => 'Add Tool / Resource',                
                'fields' => array(
                    array(
                        'name'            => 'Select Content Type',
                        'id'              => 'resource-content-type',
                        'type'            => 'select',
                        'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
                        // Array of 'value' => 'Label' pairs
                        'options' => array(
                            'video' => 'Video',
                            'podcast' => 'Podcast',
                            'article' => 'Article'
                        ),
                    ),
                    
                    array(
                        'name'  => 'Title',
                        'id'    => 'resource-title',
                        'type'  => 'input',
                    ),
                    
                    array(
                        'name'  => 'Summary',
                        'id'    => 'resource-summary',
                        'type'  => 'textarea',
                    ),
                    
                    array(
                        'name'  => 'Link',
                        'id'    => 'resource-link',
                        'type'  => 'url',
                    ),

                ),
            ),
        
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'article_resources_function_meta_box' );

/**
 * Side Content Meta Box
 */
function side_content_meta_box( $meta_boxes ) {
	$meta_boxes[] = array(
		'id' => 'general-settings',
		'class' => 'general-item-content-',
		'title' => esc_html__( 'General Settings', 'metabox-online-generator' ),
		'post_types' => array( 'guide-article-rccb'),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'select-content-type',
				'name' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'type' => 'select',
				'desc' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'placeholder' => esc_html__( 'Select content Type', 'metabox-online-generator' ),
				'options' => array(
                    'gi-text-image-button-meta-box' => 'Image & Text',
                    'gi-text-video-button-meta-box' => 'Video',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'side_content_meta_box' );







