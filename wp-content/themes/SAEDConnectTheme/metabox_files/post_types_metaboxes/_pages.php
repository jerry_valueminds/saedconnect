<?php
/*
*
*==========================================================================================================
* META BOX: Page Active Parent Link
*==========================================================================================================
*
*/
function page_active_main_menu_meta_box( $meta_boxes ) {
	$prefix = 'general-item-content-';

	$meta_boxes[] = array(
		'id' => 'general-settings',
		'class' => 'general-item-content-',
		'title' => esc_html__( 'Active Main Menu Link', 'metabox-online-generator' ),
		'post_types' => array( 'page' ),
		'context' => 'side',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'select-active-main-menu',
				'name' => esc_html__( 'Select Content Type', 'metabox-online-generator' ),
				'type' => 'select',
				'placeholder' => esc_html__( 'Select Active Main Menu', 'metabox-online-generator' ),
				'options' => array(
                    'find-your-path' => 'Find Your Path',
					'get-a-job' => 'Get a Job',
                    'do-business' => 'Do Business',
                    'learn-a-skill' => 'Learn a Skill',
                    'resources-and-Opportunities' => 'Resources & Opportunities',
					'contribute' => 'Contribute',
                    'about' => 'About',
                    'nysc-saed' => 'NYSC SAED',
				),
				'std' => 'text-content',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'page_active_main_menu_meta_box' );

function download_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'question-meta-box',
		'title' => esc_html__( 'Download Information', 'metabox-online-generator' ),
		'post_types' => array( 'page' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'download-file-type',
				'name' => esc_html__( 'Select Media Type', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
                    'image' => 'Image',
					'audio' => 'Audio',
					'video' => 'Video',
				),
				'std' => 'text-content',
			),
            
            array(
				'id' => 'download-summary',
				'type' => 'textarea',
				'name' => esc_html__( 'Summary', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'download-file',
				'type' => 'file',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'File', 'metabox-online-generator' ),
			),
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'download_meta_box' );

function marketplace_information_meta_box( $meta_boxes ) {

	$meta_boxes[] = array(
		'id' => 'marketplace-information-meta-box',
		'title' => esc_html__( 'Marketplace Information', 'metabox-online-generator' ),
		'post_types' => array( 'page' ),
		'context' => 'normal',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
            
            array(
				'id' => 'market-information-title',
				'type' => 'textarea',
				'name' => esc_html__( 'Title', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'market-information-summary',
				'type' => 'textarea',
				'name' => esc_html__( 'Summary', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'market-information-image',
				'type' => 'image_advanced',
                'max_file_uploads' => 1,
				'name' => esc_html__( 'Background Image', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'market-information-content',
				'type' => 'wysiwyg',
				'name' => esc_html__( 'Content', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'market-information-btn-group',
				'type' => 'fieldset_text',
				'name' => esc_html__( 'Buttons', 'metabox-online-generator' ),
				'rows' => 2,
				'options' => array(
					'btn-text' => 'Button Text',
					'btn-url' => 'Button URL',
					'btn-type' => 'Button Type (1, 2, 3)',
				),
				'clone' => true,
				'add_button' => esc_html__( 'Add Button', 'metabox-online-generator' ),
			),
            
            array(
				'id' => 'market-information-target',
				'name' => esc_html__( 'Links open in', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'current' => 'Current tab',
					'new' => 'New tab'
				),
				'std' => 'text-content',
			), 
            
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'marketplace_information_meta_box' );






