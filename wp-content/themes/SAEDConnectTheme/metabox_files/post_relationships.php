<?php
/*
*=================================================================================================================================
*=================================================================================================================================
*
* RELATIONSHIP & CUSTOM RELATIONSHIP METABOX SET-UP
*
*=================================================================================================================================
*=================================================================================================================================
*/

/*
*
*==========================================================================================================
* ALBUM
*==========================================================================================================
*
*/
/*
*
* Album to Album Content Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'album_to_album_content',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'album',
            'meta_box'    => array(
                'title'         => 'Album Content under this Album',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'album-content',
            'meta_box'    => array(
                'title'         => 'Album this Album Content belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Ext. Albums created',
            ),
        ),
    ) );
} );

/*
*
*==========================================================================================================
* EXTERNAL MULTI-ITEM
*==========================================================================================================
*
*/
/*
*
* External Multi-Items to External Multi-Item Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'ex_multi_item_to_ex_multi_item_cb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'collection',
            'meta_box'    => array(
                'title'         => 'Ext. Multi-item Content Blocks under this Ext. Multi-item',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'ex-multi-item-cb',
            'meta_box'    => array(
                'title'         => 'Ext. Multi-item this Ext. Multi-item Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Ext. Multi-item created',
            ),
        ),
    ) );
} );
/*
*
* External Multi-Items to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'ex_multi_item_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'collection',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Ext. Multi-item',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Ext. Multi-item this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Ext. Multi-item created',
            ),
        ),
    ) );
} );

/*
*
*==========================================================================================================
* MINI-SITE ARTICLE
*==========================================================================================================
*
*/
/*
*
* Mini-site Article to Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'minisite_article_to_cb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'information',
            'meta_box'    => array(
                'title'         => 'Content Blocks under this Mini-site Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'content-block',
            'meta_box'    => array(
                'title'         => 'Mini-site Article this Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-site Article created',
            ),
        ),
    ) );
} );
/*
*
* Mini-site Article to Side Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'minisite_article_to_rb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'information',
            'meta_box'    => array(
                'title'         => 'Right Content Blocks under this Mini-site Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'right-cb',
            'meta_box'    => array(
                'title'         => 'Mini-site Article this Right Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-site Article created',
            ),
        ),
    ) );
} );
/*
*
* Mini-site Article to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_site_article_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'information',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Mini-site Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Mini-site Article this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-site Article created',
            ),
        ),
    ) );
} );

/*
*
*==========================================================================================================
* THREE COLUMN MULTI-PAGE ARTICLE
*==========================================================================================================
*
*/
/*
*
* Multi-page Article to Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'multi_page_article_to_cb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'article',
            'meta_box'    => array(
                'title'         => 'Content Blocks under this Multi-page Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'content-block',
            'meta_box'    => array(
                'title'         => 'Multi-page Article this Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-page Article created',
            ),
        ),
    ) );
} );
/*
*
* Multi-page Article to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'multi_page_article_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'article',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Multi-page Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Multi-page Article this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-page Article created',
            ),
        ),
    ) );
} );

/*
*
*==========================================================================================================
* TWO COLUMN MULTI-PAGE ARTICLE
*==========================================================================================================
*
*/
/*
*
* Two Multi-page Article to Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'two_c_multi_to_cb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'discourse',
            'meta_box'    => array(
                'title'         => 'Content Blocks under this Two Column Multi-page Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'content-block',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-page Article this Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Two Column Multi-page Articles created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mp_article_to_cb_relationship_metabox' );

function mp_article_to_cb_relationship_metabox(){
    
    $ParentName = "Article";
    $parentType = "discourse";
    $thisParentPostId = get_the_ID();
    $ChildName = "Content Block";
    $ChildType = "content-block";
    $relationshipType = "two_c_multi_to_cb";
    
    $metaboxID = 'mp-article-to-cb-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
* Multi-page Article to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => '2_c_multi_page_article_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'discourse',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Two Column Multi-page Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-page Article this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Two Column Multi-page Article created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mp_article_to_bcb_relationship_metabox' );

function mp_article_to_bcb_relationship_metabox(){
    
    $ParentName = "Article";
    $parentType = "discourse";
    $thisParentPostId = get_the_ID();
    $ChildName = "Bottom Content Block";
    $ChildType = "bottom-cb";
    $relationshipType = "2_c_multi_page_article_to_bcb";
    
    $metaboxID = 'mp-article-to-bcb-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*==========================================================================================================
* MAGAZINE ARTICLE
*==========================================================================================================
*
*/
/*
*
* Magazine Article to Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'magazine_article_to_cb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'story',
            'meta_box'    => array(
                'title'         => 'Content Blocks under this Magazine Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'content-block',
            'meta_box'    => array(
                'title'         => 'Magazine Article this Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Magazine Article created',
            ),
        ),
    ) );
} );
/*
*
* Magazine Article to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'magazine_article_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'story',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Magazine Article',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Magazine Article this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Magazine Article created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mag_article_to_bcb_relationship_metabox' );

function mag_article_to_bcb_relationship_metabox(){
    
    $ParentName = "Magazine Article";
    $parentType = "story";
    $thisParentPostId = get_the_ID();
    $ChildName = "Content";
    $ChildType = "bottom-cb";
    $relationshipType = "magazine_article_to_bcb";
    
    $metaboxID = 'mag-article-to-bcb-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}


/*
*
*==========================================================================================================
* THREE COLUMN MULTI-PAGE
*==========================================================================================================
*
*/
/*
*
* Three Column Multi-page to Three Column Multi-page Article Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'three_c_m_page_to_m_page_article',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'guide',
            'meta_box'    => array(
                'title'         => 'Multi-page Articles under this Three Column Multi-page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'article',
            'meta_box'    => array(
                'title'         => 'Three Column Multi-page this Multi-page Article belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Magazine Three Column Multi-pages',
            ),
        ),
    ) );
} );
/*
*
* Three Column Multi-page to Right Column Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'three_c_m_page_to_rcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'guide',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Three Column Multi-page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'right-cb',
            'meta_box'    => array(
                'title'         => 'Three Column Multi-page this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Three Column Multi-pages created',
            ),
        ),
    ) );
} );
/*
*
* Three Column Multi-page to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'three_c_m_page_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'guide',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Three Column Multi-page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Three Column Multi-page this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Three Column Multi-pages created',
            ),
        ),
    ) );
} );

/*
*
*==========================================================================================================
* TWO COLUMN MULTI-PAGE
*==========================================================================================================
*
*/
/*
*
* Two Column Multi-page to Two Column Two Column Multi-page Article Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'two_c_m_page_to_m_page_article',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'reference',
            'meta_box'    => array(
                'title'         => 'Multi-page Articles under this Two Column Multi-page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'discourse',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-page this Multi-page Article belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Magazine Two Column Multi-pages',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'tc_mpage_tc_multi_page_article_relationship_metabox' );

function tc_mpage_tc_multi_page_article_relationship_metabox(){
    
    $ParentName = "Two Column Multi-page";
    $parentType = "reference";
    $thisParentPostId = get_the_ID();
    $ChildName = "Multi-page Article";
    $ChildType = "discourse";
    $relationshipType = "two_c_m_page_to_m_page_article";
    
    $metaboxID = 'two-c-m-page-to-m-page-article-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
* Two Column Multi-page to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'two_c_m_page_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'reference',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Two Column Multi-page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-page this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Two Column Multi-pages created',
            ),
        ),
    ) );
} );

/*
*
*==========================================================================================================
* MINI-SITE
*==========================================================================================================
*
*/
/*
*
* Mini-site to Right Column Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_site_to_rcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'right-cb',
            'meta_box'    => array(
                'title'         => 'Mini-site this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Three Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mini_site_to_rcb_relationship_metabox' );

function mini_site_to_rcb_relationship_metabox(){
    
    $ParentName = "Mini-site";
    $parentType = "mini-site";
    $thisParentPostId = get_the_ID();
    $ChildName = "Right Content Item";
    $ChildType = "right-cb";
    $relationshipType = "mini_site_to_rcb";
    
    $metaboxID = 'mini-site-to-rcb-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}
/*===================================================================================
/*===================================================================================
/*
*
* Mini-site to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_site_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Mini-site this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mini_site_to_bcb_relationship_metabox' );

function mini_site_to_bcb_relationship_metabox(){
    
    $ParentName = "Mini-site";
    $parentType = "mini-site";
    $thisParentPostId = get_the_ID();
    $ChildName = "Bottom Content Item";
    $ChildType = "bottom-cb";
    $relationshipType = "mini_site_to_bcb";
    
    $metaboxID = 'mini-site-to-bcb-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}
/*===================================================================================
/*===================================================================================
/*
*
* Mini-site to Mini-site Article Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_site_to_topic_article',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Topic Articles under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'topic-article',
            'meta_box'    => array(
                'title'         => 'Mini-site this Topic Article belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mini_site_to_m_s_article_relationship_metabox' );

function mini_site_to_m_s_article_relationship_metabox(){
    
    $ParentName = "Mini-site";
    $parentType = "mini-site";
    $thisParentPostId = get_the_ID();
    $ChildName = "Topic Article";
    $ChildType = "topic-article";
    $relationshipType = "mini_site_to_topic_article";
    
    $metaboxID = 'mini-site-to-topic-article-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}
/*===================================================================================
/*===================================================================================
/*
*
* Mini-site to Mini-site Album Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_site_to_m_site_album',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Mini-site Albums under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'gallery',
            'meta_box'    => array(
                'title'         => 'Mini-site this Mini-site Album belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mini_site_to_m_s_album_relationship_metabox' );

function mini_site_to_m_s_album_relationship_metabox(){
    
    $ParentName = "Mini-site";
    $parentType = "mini-site";
    $thisParentPostId = get_the_ID();
    $ChildName = "Mini-site Album";
    $ChildType = "gallery";
    $relationshipType = "mini_site_to_m_site_album";
    
    $metaboxID = 'mini-site-to-m-s-album-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}
/*===================================================================================
/*===================================================================================
/*
*
* Mini-site to Multi-item Page Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_site_to_m_item_page',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Multi-item Pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'synopsis',
            'meta_box'    => array(
                'title'         => 'Mini-site this Multi-item Page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mini_site_to_m_item_relationship_metabox' );

function mini_site_to_m_item_relationship_metabox(){
    
    $ParentName = "Mini-site";
    $parentType = "mini-site";
    $thisParentPostId = get_the_ID();
    $ChildName = "Multi-item Page";
    $ChildType = "synopsis";
    $relationshipType = "mini_site_to_m_item_page";
    
    $metaboxID = 'mini-site-to-m-item-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}
/*===================================================================================
/*===================================================================================
/*
*
* Mini-site to Three Column Multi-page Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_site_to_three_c_m_page',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Three Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'guide',
            'meta_box'    => array(
                'title'         => 'Mini-site this Three Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mini_site_to_three_c_m_relationship_metabox' );

function mini_site_to_three_c_m_relationship_metabox(){
    
    $ParentName = "Mini-site";
    $parentType = "mini-site";
    $thisParentPostId = get_the_ID();
    $ChildName = "Three Column Multi-page";
    $ChildType = "guide";
    $relationshipType = "mini_site_to_three_c_m_page";
    
    $metaboxID = 'mini-site-to-three-c-m-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}
/*===================================================================================
/*===================================================================================
/*
*
* Mini-site to Two Column Multi-page Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_site_to_two_c_m_page',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'reference',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'mini_site_to_two_c_m_relationship_metabox' );

function mini_site_to_two_c_m_relationship_metabox(){
    
    $ParentName = "Mini-site";
    $parentType = "mini-site";
    $thisParentPostId = get_the_ID();
    $ChildName = "Two Column Multi-page";
    $ChildType = "reference";
    $relationshipType = "mini_site_to_two_c_m_page";
    
    $metaboxID = 'mini-site-to-two-c-m-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}
/*===================================================================================
/*===================================================================================
/*
*
*==========================================================================================================
* DIRECTORY MINI-SITE
*==========================================================================================================
*
*/
/*
*
* Directory Mini-site to Right Column Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'directory_mini_site_to_rcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'directory-mini-site',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Directory Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'right-cb',
            'meta_box'    => array(
                'title'         => 'Directory Mini-site this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Directory Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* directory Mini-site to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'directory_mini_site_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'directory-mini-site',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Directory Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Directory Mini-site this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Directory Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* directory Mini-site to Mini-site Article Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'directory_mini_site_to_m_site_article',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'directory-mini-site',
            'meta_box'    => array(
                'title'         => 'Mini-site Articles under this Directory Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'information',
            'meta_box'    => array(
                'title'         => 'Directory Mini-site this Mini-site Article belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Directory Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* directory Mini-site to Mini-site Album Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'directory_mini_site_to_m_site_album',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'directory-mini-site',
            'meta_box'    => array(
                'title'         => 'Mini-site Albums under this Directory Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'gallery',
            'meta_box'    => array(
                'title'         => 'Directory Mini-site this Mini-site Album belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Directory Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* directory Mini-site to Multi-item Page Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'directory_mini_site_to_m_item_page',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'directory-mini-site',
            'meta_box'    => array(
                'title'         => 'Multi-item Pages under this Directory Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'synopsis',
            'meta_box'    => array(
                'title'         => 'Directory Mini-site this Multi-item Page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Directory Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* directory Mini-site to Three Column Multi-page Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'directory_mini_site_to_three_c_m_page',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'directory-mini-site',
            'meta_box'    => array(
                'title'         => 'Three Column Multi-pages under this Directory Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'guide',
            'meta_box'    => array(
                'title'         => 'Directory Mini-site this Three Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Directory Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
* directory Mini-site to Two Column Multi-page Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'directory_mini_site_to_two_c_m_page',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'directory-mini-site',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Directory Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'reference',
            'meta_box'    => array(
                'title'         => 'Directory Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Directory Mini-sites created',
            ),
        ),
    ) );
} );

/*
*
*==========================================================================================================
* MULTI-ITEM PAGE
*==========================================================================================================
*
*/
/*
*
* Multi-item Page to Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'm_item_page_to_cb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'synopsis',
            'meta_box'    => array(
                'title'         => 'Content Blocks under this Multi-item Page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'content-block',
            'meta_box'    => array(
                'title'         => 'Multi-item Page this Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-item Pages created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'm_item_to_cb_relationship_metabox' );

function m_item_to_cb_relationship_metabox(){
    
    $ParentName = "Multi-item Page";
    $parentType = "synopsis";
    $thisParentPostId = get_the_ID();
    $ChildName = "Content Block";
    $ChildType = "content-block";
    $relationshipType = "m_item_page_to_cb";
    
    $metaboxID = 'm-item-to-cb-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
* Multi-item Page to Right Column Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'm_item_page_to_rcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'synopsis',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Multi-item Page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'right-cb',
            'meta_box'    => array(
                'title'         => 'Multi-item Page this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-item Pages created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'm_item_to_rcb_relationship_metabox' );

function m_item_to_rcb_relationship_metabox(){
    
    $ParentName = "Multi-item Page";
    $parentType = "synopsis";
    $thisParentPostId = get_the_ID();
    $ChildName = "Right Content Block";
    $ChildType = "right-cb";
    $relationshipType = "m_item_page_to_rcb";
    
    $metaboxID = 'm-item-to-rcb-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
* Multi-item Page to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'm_item_page_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'synopsis',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Multi-item Page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Multi-item Page this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-item Pages created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'm_item_to_bcb_relationship_metabox' );

function m_item_to_bcb_relationship_metabox(){
    
    $ParentName = "Multi-item Page";
    $parentType = "synopsis";
    $thisParentPostId = get_the_ID();
    $ChildName = "Bottom Content Block";
    $ChildType = "bottom-cb";
    $relationshipType = "m_item_page_to_bcb";
    
    $metaboxID = 'm-item-to-bcb-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*==========================================================================================================
* MULTI-ITEM PAGE WITH HEADER
*==========================================================================================================
*
*/
/*
*
* Multi-item Page With Header to Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'm_item_page_h_to_cb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'overview',
            'meta_box'    => array(
                'title'         => 'Content Blocks under this Multi-item Page With Header',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'content-block',
            'meta_box'    => array(
                'title'         => 'Multi-item Page With Header this Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-item Page With Headers created',
            ),
        ),
    ) );
} );
/*
*
* Multi-item Page With Header to Right Column Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'm_item_page_h_to_rcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'overview',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Multi-item Page With Header',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'right-cb',
            'meta_box'    => array(
                'title'         => 'Multi-item Page With Header this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-item Page With Headers created',
            ),
        ),
    ) );
} );
/*
*
* Multi-item Page With Header to Bottom Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'm_item_page_h_to_bcb',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'overview',
            'meta_box'    => array(
                'title'         => 'Bottom Content Blocks under this Multi-item Page With Header',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'bottom-cb',
            'meta_box'    => array(
                'title'         => 'Multi-item Page With Header this Bottom Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-item Page With Headers created',
            ),
        ),
    ) );
} );




/*
*
*==========================================================================================================
* ONLINE GUIDE SECTION
*==========================================================================================================
*
*/

/*
*
* Online Guide Section to Mini-site Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'og_section_to_mini_site',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'guide-section',
            'meta_box'    => array(
                'title'         => 'Mini-sites under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Mini-site belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'guide_to_mini_site_relationship_metabox' );

function guide_to_mini_site_relationship_metabox(){
    
    $ParentName = "Online Guide";
    $thisParentPostId = get_the_ID();
    $ChildName = "Mini-site (Guide Topic)";
    $ChildType = "mini-site";
    $relationshipType = "og_section_to_mini_site";
    
    $metaboxID = 'og-guide-to-mini-site-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array('guide-section'),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
* Online Guide Section to Guide Topic Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'og_section_to_guide_topic',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'guide-section',
            'meta_box'    => array(
                'title'         => 'Guide Topics under this Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'guide-topic',
            'meta_box'    => array(
                'title'         => 'Guide Section this Guide Topic belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Guide Sections created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'guide_to_guide_topic_relationship_metabox' );

function guide_to_guide_topic_relationship_metabox(){
    
    $ParentName = "Guide";
    $thisParentPostId = get_the_ID();
    $ChildName = "Guide Topic";
    $ChildType = "guide-topic";
    $relationshipType = "og_section_to_guide_topic";
    
    $metaboxID = 'og-guide-to-guide-topic-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array('guide-section'),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*==========================================================================================================
*==========================================================================================================
*
* CV
*
*==========================================================================================================
*==========================================================================================================
*
*/
/*
*
* Online Guide Section to Right Column Content Block Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_school',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-school',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'cv_to_cv_school_relationship_metabox' );

function cv_to_cv_school_relationship_metabox(){
    
    $ParentName = "cv";
    $thisParentPostId = get_the_ID();
    $ChildName = "Right Content Block";
    $ChildType = "cv-school";
    $relationshipType = "cv_to_school";
    
    $metaboxID = 'cv-to-school-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array('cv'),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*============================================================================================
/*============================================================================================
/=============================================================================================

/*
*
* CV to Personal Information Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_personal_info',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-personal-info',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Summary Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_summary',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-summary',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Contact Details Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_contact',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-contact',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Experience Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_experience',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-experience',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Personal Project Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_personal_project',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-personal-project',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Education Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_education',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-education',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Award Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_award',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-award',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Accomplishments Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_accomplishment',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-accomplishment',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Skill Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_skill',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-skill',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Certifications Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_certification',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-certification',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Affiliations Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_affiliation',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-affiliation',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Patents Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_patent',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-patent',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Publications Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_publication',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-publication',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Interests Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_interest',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-interest',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Languages Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_language',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-language',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to Additional information Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_additional_info',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-additional-info',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* CV to References Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'cv_to_reference',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'cv',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'cv-reference',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );




/*
*
*==========================================================================================================
*==========================================================================================================
*
*   Mentor
*
*==========================================================================================================
*==========================================================================================================
*
*/


/*
*
* Mentor / Service Provider to Basic Information Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mentor_to_basic_info',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'service-prov-profile',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'mentor-basic-info',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* Mentor / Service Provider to Trainer Profile Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mentor_to_trainer_profile',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'service-prov-profile',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'mentor-trainer-prof',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* Mentor / Service Provider to Youth Development Support Service Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mentor_to_youth_service',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'service-prov-profile',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'mentor-youth-service',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );



/*
*
*==========================================================================================================
*==========================================================================================================
*
*   User Program
*
*==========================================================================================================
*==========================================================================================================
*
*/


/*
*
* User Program to Program Information Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_program_to_program_info',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program-info',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* User Program to Uploads Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_program_to_uploads',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program-upload',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* User Program to Frequently Asked Question Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_program_to_faq',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program-faq',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* User Program to Program Packages Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_program_to_program_packages',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program-package',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* User Program to Program Schedule Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_program_to_program_schedule',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-progr-schedule',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );




/*
*
*==========================================================================================================
*==========================================================================================================
*
*   User Course
*
*==========================================================================================================
*==========================================================================================================
*
*/


/*
*
* User Course to Course Information Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_course_to_course_info',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-course-info',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* User Course to Uploads Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_course_to_uploads',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-program',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-course-upload',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* User Course to Frequently Asked Question Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_course_to_faq',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-course',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-course-faq',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* User Course to Course Packages Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_course_to_course_packages',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-course',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-course-package',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );

/*
*
* User Course to Course Schedule Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'u_course_to_course_schedule',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'user-course',
            'meta_box'    => array(
                'title'         => 'Right Column Content Blocks under this Online Guide Section',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'user-course-schedule',
            'meta_box'    => array(
                'title'         => 'Online Guide Section this Right Column Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Online Guide Sections created',
            ),
        ),
    ) );
} );





/*
*
*==========================================================================================================
*   CAREER
*==========================================================================================================
*
*/
/*
*
*   Page to Page Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'page_to_page',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'page',
            'meta_box'    => array(
                'title'         => 'Pages under this Page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'page',
            'meta_box'    => array(
                'title'         => 'Page this Page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Pages created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'page_to_page_relationship_metabox' );

function page_to_page_relationship_metabox(){
    
    $ParentName = "Page";
    $parentType = "page";
    $thisParentPostId = get_the_ID();
    $ChildName = "Page";
    $ChildType = "page";
    $relationshipType = "page_to_page";
    
    $metaboxID = 'page-to-page-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}


/*
*
*==========================================================================================================
*   CAREER
*==========================================================================================================
*
*/
/*
*
* Career Role to Career Task Post Types
*
*/
/*add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'career_role_to_career_task',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'career-role',
            'meta_box'    => array(
                'title'         => 'Content Blocks under this Multi-item Page',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'career-task',
            'meta_box'    => array(
                'title'         => 'Multi-item Page this Content Block belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Multi-item Pages created',
            ),
        ),
    ) );
} );*/
/*
*
* Parent Relationship Metabox
*
*/
/*add_action( 'add_meta_boxes', 'career_role_to_career_task_metabox' );

function career_role_to_career_task_metabox(){
    
    $ParentName = "Career Role";
    $parentType = "career-role";
    $thisParentPostId = get_the_ID();
    $ChildName = "Task";
    $ChildType = "career-task";
    $relationshipType = "career_role_to_career_task";
    
    $metaboxID = 'career-to-task-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}*/




















/*
*==========================================================================================================
*==========================================================================================================
*
*   YNL
*
*==========================================================================================================
*==========================================================================================================
*/
/*
*
*   Thematic Area to Problem Area Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'thematic_area_to_problem_area',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'thematic-area',
            'meta_box'    => array(
                'title'         => 'Problem Areas under this Thematic Area',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'problem-area',
            'meta_box'    => array(
                'title'         => 'Thematic Area this Problem Area belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Thematic Area created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'thematic_area_to_problem_area_relationship_metabox' );

function thematic_area_to_problem_area_relationship_metabox(){
    
    $ParentName = "Thematic Area Session";
    $parentType = "thematic-area";
    $thisParentPostId = get_the_ID();
    $ChildName = "Problem Area";
    $ChildType = "problem-area";
    $relationshipType = "thematic_area_to_problem_area";
    
    $metaboxID = 'thematic-area-to-problem-area-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}






/*
*
*   Thematic Area to Theme Question Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'thematic_area_to_theme_question',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'thematic-area',
            'meta_box'    => array(
                'title'         => 'Questions under this Thematic Area',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'theme-question',
            'meta_box'    => array(
                'title'         => 'Thematic Area this Question belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Thematic Areas created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'thematic_area_to_theme_question_relationship_metabox' );

function thematic_area_to_theme_question_relationship_metabox(){
    
    $ParentName = "Thematic Area";
    $parentType = "thematic-area";
    $thisParentPostId = get_the_ID();
    $ChildName = "Question";
    $ChildType = "theme-question";
    $relationshipType = "thematic_area_to_theme_question";
    
    $metaboxID = 'thematic-area-to-thene-question-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}





/*
*
*   Thematic Area to Task Task Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'thematic_area_to_task',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'thematic-area',
            'meta_box'    => array(
                'title'         => 'Tasks under this Thematic Area',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'task',
            'meta_box'    => array(
                'title'         => 'Thematic Area this Task belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Thematic Areas created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'thematic_area_to_task_relationship_metabox' );

function thematic_area_to_task_relationship_metabox(){
    
    $ParentName = "Thematic Area";
    $parentType = "thematic-area";
    $thisParentPostId = get_the_ID();
    $ChildName = "Task";
    $ChildType = "task";
    $relationshipType = "thematic_area_to_task";
    
    $metaboxID = 'thematic-area-to-task-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}












/*
*
*   Thematic Area to Project Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'thematic_area_to_project',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'thematic-area',
            'meta_box'    => array(
                'title'         => 'Projects under this Thematic Area',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'project',
            'meta_box'    => array(
                'title'         => 'Thematic Area this Project belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Thematic Areas created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'thematic_area_to_project_relationship_metabox' );

function thematic_area_to_project_relationship_metabox(){
    
    $ParentName = "Thematic Area";
    $parentType = "thematic-area";
    $thisParentPostId = get_the_ID();
    $ChildName = "Project";
    $ChildType = "project";
    $relationshipType = "thematic_area_to_project";
    
    $metaboxID = 'thematic-area-to-project-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}











/*
*==========================================================================================================
*==========================================================================================================
*
*   Information Session
*
*==========================================================================================================
*==========================================================================================================
*/

include 'post_relationships_metaboxes/_skilli.php';










/*
*=================================================================================================================================
*=================================================================================================================================
*
*   CUSTOM RELATIONSHIP METABOX
*   - Adds a Custmon metabox on the Admin Edit page of PARENT POSTS.
*   - Process Ajax Requests: Add, Delete and Sort Child Post lists
*
*=================================================================================================================================
*=================================================================================================================================
*/

include 'post_relationships_metaboxes/_ajax_processing.php';
