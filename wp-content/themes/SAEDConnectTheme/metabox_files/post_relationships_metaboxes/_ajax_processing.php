<?php
/*
*
*==========================================================================================================
*
* PROCESS AJAX REQUEST - BEGIN:
* - This meta displays All related child posts, and adds an Edit & Delete Buttons to each post.
* - This metabox displays a field to Add a New Child post.
*   On button Click, an AJAX Request is sent to process the form.
*
*==========================================================================================================
*
*/
function child_post_meta_box_cb($post, $relationship_args)
{
    //wp_reset_postdata();
   // wp_reset_query();
    
    $thisParentPostId = get_the_ID();
    //echo $relationship_args ['args']['name'];
    ?>
    <article style="padding:20px;border: 2px solid gold;border-radius: 8px;">
        <p>
            Click on any <span style="font-weight:600">Post Name</span> (underlined) to view the Post in the front end. 
        </p>
        <p>
            <span style="font-weight:600">Edit Button</span> Takes you to the <span style="font-weight:600;">Post Edit Page</span>.
        </p>
        <p style="">
            <span style="font-weight:600;">Delete Button</span> Moves a Post to <span style="font-weight:600;">Trash</span> and redirects to the list of this post type.
        </p>
    </article>
    <ul class="child-list" style="margin-top: 0;">
        <!--<li style="border-top:1px solid gainsboro;padding-top: 15px;margin-bottom: 15px;">
            <div id="delete-action" style="font-weight:600;font-size:16px">
                All Assigned <?php //echo $relationship_args ['args']['ChildName'].'s' ?>   
            </div>
            <div id="publishing-action">
            </div>
            <div class="clear"></div>
        </li>-->
    <?php

    $original_post = $post;
        
    $getChild = new WP_Query( array(
        'relationship' => array(
            'id'   => $relationship_args ['args']['relationshipType'],
            'from' => $relationship_args ['args']['parentId'], // You can pass object ID or full object
        ),
        'nopaging' => true,
        'orderby' => 'title',
        'order'   => 'ASC',
    ) );
    
    while ( $getChild->have_posts() ) : $getChild->the_post(); ?>
           
        
            <li id="<?php echo get_the_ID(); ?>" class="ui-state-default" style="border: 0; background-color: transparent; border-top:1px solid gainsboro;padding-top: 15px;margin-bottom: 15px;">
                <div id="delete-action">
                    <i class="fa fa-arrows"></i>
                    <a id="post-status-display" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        
                </div>
                <div id="publishing-action">
                    <span class="spinner"></span>
                    <a href="<?php echo get_edit_post_link( $id, $context ); ?>" class="button button-primary">
                        Edit Post
                    </a>
                    <!--<a  href="<?php echo get_delete_post_link( $id, $context ); ?>" class="button button-primary delete-child-btn">
                        Delete Post
                    </a>-->
                    <button type="button" id="<?php echo get_the_ID(); ?>" class="delete-child-btn button button-primary">Delete Post</button>
                </div>
                <div class="clear"></div>
            </li>
            
    <?php
    endwhile;
    
    setup_postdata( $original_post );
    //wp_reset_postdata();
    //wp_reset_query();
    ?>
    <span hidden class="sort-parent-id"><?php echo $thisParentPostId ?></span>
    <span hidden class="sort-relationship-type"><?php echo $relationship_args ['args']['relationshipType'] ?></span>
    </ul>
    
    <article style="padding:20px 20px 40px 20px;background-color:ghostwhite">
        <h3 class="">
            <span>
                Add New <?php echo $relationship_args ['args']['ChildName'] ?>
            </span>
        </h3>
        <p>
            Type a post name and click the add post button. This creates a <?php echo $relationship_args ['args']['ChildName'] ?> post type, as a child of the current post you are on.
        </p>
        <form class="new-post-form">
            
            
            <input name="relationship_type" type="text" value="<?php echo $relationship_args ['args']['relationshipType'] ?>" hidden class="relationship-type">
            <input name="parent_id" type="text" value="<?php echo $relationship_args ['args']['parentId'] ?>" hidden class="parent-id">
            <input name="child_post_type" type="text" value="<?php echo $relationship_args ['args']['ChildType'] ?>" hidden class="child-post-type">
            
            <div class="add-post-input">
                <label for="meta-box-text">Post Name</label>
                <input name="post-name" type="text" value="" class="post-name">
            </div>
            <div class="add-post-btn">
                <a href="#" class="button button-primary ajax-submit" id="<?php echo $relationship_args ['args']['metaboxID'] ?>">
                    Add Post
                </a>
                <img class="loading-icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/spinner.gif" alt="loading-icon">
            </div>
        </form>
    </article>
    <?php
}


/*
*
*==========================================================================================================
*
* PROCESS AJAX REQUEST - BEGIN: This Handles CREATE NEW CHILD POST Ajax requests received.
*
*==========================================================================================================
*
*/
add_action('wp_ajax_add_post_form', 'add_post_form');
add_action('wp_ajax_nopriv_add_post_form', 'add_post_form');

function add_post_form()
{
    $postName = $_POST['postName'];
    $postSlug = $_POST['postSlug'];
    $childPostType = $_POST['childPostType'];
    $parentId = $_POST['parentId'];
    $relationshipType = $_POST['relationshipType'];
    
    $post_id = wp_insert_post(array (
        'post_type' => $childPostType,
        'post_title' => $postName,
        'post_content' => "",
        'post_status' => 'publish',
    ));
    
    
    
    /*
    *================================================
    *   MB Relationships
    *================================================
    */
    
    /* Get Current Multi-site ID */
    $blog_id = get_current_blog_id();
    
    /* MB Relationship Table */
    $mb_relationship_table = 'wp_mb_relationships';
    
    /* Relation Table to insert into on a multisite */
    if($blog_id != 1){
        $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
    }
    
    /* Get Clobal Relationship Table Object */
    global $wpdb;
    
    /* Insert into relationship table */
    $wpdb->insert( 
	$mb_relationship_table, 
        array( 
            'from' => $parentId, 
            'to' => $post_id,
            'type' => $relationshipType,
        ), 
        array( 
            '%d',
            '%d',
            '%s'
        ) 
    );
    
    echo 
    
        '<li class="ui-state-default" style="border: 0; background-color: transparent; border-top:1px solid gainsboro;padding-top: 15px;margin-bottom: 15px;">'.
            '<div id="delete-action">'.
                '<i class="fa fa-arrows"></i> '.
                '<a id="post-status-display" href="'.get_permalink($post_id).'">'.
                    get_the_title($post_id).
                '</a>'.  
            '</div>'.
            '<div id="publishing-action">'.
                '<span class="spinner"></span>'.
                '<a href="'.get_edit_post_link( $post_id, $context ).'" class="button button-primary">'.
                    'Edit Post'.
                '</a> '.
                '<a href="'.get_delete_post_link( $post_id, $context ).'" class="button button-primary">'.
                    'Delete Post'.
                '</a>'.
            '</div>'.
            '<div class="clear"></div>'.
        '</li>';
    
    wp_die();
}


/*
*
*==========================================================================================================
*
* PROCESS AJAX REQUEST - BEGIN: This DELETES A CHILD POST Ajax request received.
*
*==========================================================================================================
*
*/
add_action('wp_ajax_delete_form', 'delete_child_post');
add_action('wp_ajax_nopriv_delete_form', 'delete_child_post');

function delete_child_post()
{
    $postID = $_POST['postID'];
    
    wp_delete_post($postID);
    
    echo "Sucess: Deleted";
    
    wp_die();
}


/*
*
*==========================================================================================================
*
* PROCESS AJAX REQUEST - BEGIN: This Handles SORTING OF THE CHILD POST LIST
*
*==========================================================================================================
*
*/
add_action('wp_ajax_sort_children', 'sort_children');
add_action('wp_ajax_nopriv_sort_children', 'sort_children');

function sort_children()
{
    $i = 0;
    $orderList;
    
    /* Get Data from request */
    $parentId = $_POST['parentId'];
    $relationshipType = $_POST['relationshipType'];
    $process_list = $_POST['childrenOrder'];

    foreach ($process_list as $value) {
        // Execute statement:
        // UPDATE [Table] SET [Position] = $i WHERE [EntityId] = $value;
        
        /*$post = array(
            'ID' => $value,
            'post_modified_gmt' => date( 'Y:m:d H:i:s' ),
        );
        
        wp_update_post( $post );
        */
        
        if($value !== ''){
            /* Remove Relationship */
            MB_Relationships_API::delete( $parentId, $value, $relationshipType );

            /* Add Relationship */
            MB_Relationships_API::add( $parentId, $value, $relationshipType );
        }

        $i++;
        $orderList .= "Post " .$i. ": ".$value."\n";
        
        
    }
    

    
    echo "Returned Order List: \n\n".$orderList;
    
    wp_die();
}
