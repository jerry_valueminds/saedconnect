<?php
/*
*==========================================================================================================
*==========================================================================================================
*
*   Information Session (Business Ideas)
*
*==========================================================================================================
*==========================================================================================================
*/
/*
*
*   Information Session to Micro Course Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'info_session_to_micro_course',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'information-session',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'micro-course',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'info_sess_to_micro_course_relationship_metabox' );

function info_sess_to_micro_course_relationship_metabox(){
    
    $ParentName = "Information Session";
    $parentType = "information-session";
    $thisParentPostId = get_the_ID();
    $ChildName = "Micro Course";
    $ChildType = "micro-course";
    $relationshipType = "info_session_to_micro_course";
    
    $metaboxID = 'info-sess-to-micro-course-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*   Information Session to Mentor Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'info_session_to_mentor_course',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'information-session',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'info-session-mentor',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'info_sess_to_mentor_relationship_metabox' );

function info_sess_to_mentor_relationship_metabox(){
    
    $ParentName = "Information Session";
    $parentType = "information-session";
    $thisParentPostId = get_the_ID();
    $ChildName = "Mentor";
    $ChildType = "info-session-mentor";
    $relationshipType = "info_session_to_mentor_course";
    
    $metaboxID = 'info-sess-to-mentor-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*   Information Session to Testimonial Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'info_session_to_testimonial_course',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'information-session',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'info-session-testi',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'info_sess_to_testimonial_relationship_metabox' );

function info_sess_to_testimonial_relationship_metabox(){
    
    $ParentName = "Information Session";
    $parentType = "information-session";
    $thisParentPostId = get_the_ID();
    $ChildName = "Testimonial";
    $ChildType = "info-session-testi";
    $relationshipType = "info_session_to_testimonial_course";
    
    $metaboxID = 'info-sess-to-testimonial-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*   Information Session to Track Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'info_session_to_program',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'information-session',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'track',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'info_sess_to_program_relationship_metabox' );

function info_sess_to_program_relationship_metabox(){
    
    $ParentName = "Information Session";
    $parentType = "information-session";
    $thisParentPostId = get_the_ID();
    $ChildName = "Track";
    $ChildType = "track";
    $relationshipType = "info_session_to_program";
    
    $metaboxID = 'info-sess-to-program-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}


/*
*==========================================================================================================
*==========================================================================================================
*
*   SKilli Programs ( Programs )
*
*==========================================================================================================
*==========================================================================================================
*/

/*
*
*   Program Collection to Program Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'program_collection_to_program',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'program-collection',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'program',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'program_collection_to_program_relationship_metabox' );

function program_collection_to_program_relationship_metabox(){
    
    $ParentName = "Program Collection";
    $parentType = "program-collection";
    $thisParentPostId = get_the_ID();
    $ChildName = "Program";
    $ChildType = "program";
    $relationshipType = "program_collection_to_program";
    
    $metaboxID = 'program-collectiob-to-program-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*   Program to Track Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'program_to_track',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'program',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'track',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );
/*
*
*   Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'program_to_track_relationship_metabox' );

function program_to_track_relationship_metabox(){
    
    $ParentName = "Program";
    $parentType = "program";
    $thisParentPostId = get_the_ID();
    $ChildName = "Track";
    $ChildType = "track";
    $relationshipType = "program_to_track";
    
    $metaboxID = 'program-to-track-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*==========================================================================================================
*==========================================================================================================
*
*   Tracks
*
*==========================================================================================================
*==========================================================================================================
*/

/*
*
*   Track to Questions Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'track_to_question',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'track',
            'meta_box'    => array(
                'title'         => 'Questions under this Track',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'question',
            'meta_box'    => array(
                'title'         => 'Track this Question belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Tracks created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'track_to_question_relationship_metabox' );

function track_to_question_relationship_metabox(){
    
    $ParentName = "Track";
    $parentType = "track";
    $thisParentPostId = get_the_ID();
    $ChildName = "Question";
    $ChildType = "question";
    $relationshipType = "track_to_question";
    
    $metaboxID = 'track-to-question-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*   Track to Guide Topics Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'track_to_guide_topic',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'track',
            'meta_box'    => array(
                'title'         => 'Topics under this Track',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'guide-topic',
            'meta_box'    => array(
                'title'         => 'Track this Topic belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Tracks created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'track_to_guide_topic_relationship_metabox' );

function track_to_guide_topic_relationship_metabox(){
    
    $ParentName = "Track";
    $parentType = "track";
    $thisParentPostId = get_the_ID();
    $ChildName = "Guide Topic";
    $ChildType = "guide-topic";
    $relationshipType = "track_to_guide_topic";
    
    $metaboxID = 'track-to-guide-topic-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
*   Track to curriculum Post Types
*
*/
/*add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'track_to_curriculum',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'track',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'curriculum',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );*/
/*
*
* Parent Relationship Metabox
*
*/
/*add_action( 'add_meta_boxes', 'track_to_curriculum_relationship_metabox' );

function track_to_curriculum_relationship_metabox(){
    
    $ParentName = "Track";
    $parentType = "track";
    $thisParentPostId = get_the_ID();
    $ChildName = "Curriculum";
    $ChildType = "curriculum";
    $relationshipType = "track_to_curriculum";
    
    $metaboxID = 'track-to-curriculum-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}*/

/*
*
*   Track to Scheduled Dates Post Types
*
*/
/*add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'track_to_scheduled_date',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'track',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'program-schedule',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );*/
/*
*
* Parent Relationship Metabox
*
*/
/*add_action( 'add_meta_boxes', 'track_to_scheduled_date_relationship_metabox' );*/

/*function track_to_scheduled_date_relationship_metabox(){
    
    $ParentName = "Track";
    $parentType = "track";
    $thisParentPostId = get_the_ID();
    $ChildName = "Start Date";
    $ChildType = "program-schedule";
    $relationshipType = "track_to_scheduled_date";
    
    $metaboxID = 'track-to-scheduled-date-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}*/

/*
*
*   Curriclum to Project Post Types
*
*/
/*add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'curriculum_to_project',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'curriculum',
            'meta_box'    => array(
                'title'         => 'Two Column Multi-pages under this Mini-site',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'project',
            'meta_box'    => array(
                'title'         => 'Mini-site this Two Column Multi-page belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Mini-sites created',
            ),
        ),
    ) );
} );*/
/*
*
* Parent Relationship Metabox
*
*/
/*add_action( 'add_meta_boxes', 'curriculum_to_project_relationship_metabox' );

function curriculum_to_project_relationship_metabox(){
    
    $ParentName = "Curriculum";
    $parentType = "curriculum";
    $thisParentPostId = get_the_ID();
    $ChildName = "Project";
    $ChildType = "project";
    $relationshipType = "curriculum_to_project";
    
    $metaboxID = 'curriculum-to-project-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}*/

/*
*
*==========================================================================================================
* ONLINE GUIDE TOPIC
*==========================================================================================================
*
*/
/*
*
* Guide Topic to TOpic Article Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'guide_topic_to_guide_article',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'guide-topic',
            'meta_box'    => array(
                'title'         => 'Articles under this Guide Topic',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'right-cb',
            'meta_box'    => array(
                'title'         => 'Guide Topic this Articles belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Guide Topics created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'guide_topic_to_guide_article_relationship_metabox' );

function guide_topic_to_guide_article_relationship_metabox(){
    
    $ParentName = "Guide Topic";
    $parentType = "guide-topic";
    $thisParentPostId = get_the_ID();
    $ChildName = "Article";
    $ChildType = "guide-article";
    $relationshipType = "guide_topic_to_guide_article";
    
    $metaboxID = 'guide-topic-to-article-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}


/*
*
*==========================================================================================================
* QUESTIONS
*==========================================================================================================
*
*/
/*
*
* Question to Question Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'question_to_question',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'question',
            'meta_box'    => array(
                'title'         => 'Questions under this Question',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'question',
            'meta_box'    => array(
                'title'         => 'Question this Questions belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Questions created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'question_to_question_relationship_metabox' );

function question_to_question_relationship_metabox(){
    
    $ParentName = "Question";
    $parentType = "question";
    $thisParentPostId = get_the_ID();
    $ChildName = "Question";
    $ChildType = "question";
    $relationshipType = "question_to_question";
    
    $metaboxID = 'question-to-question-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}

/*
*
* Question to Answer Post Types
*
*/
add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'question_to_answer',
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'question',
            'meta_box'    => array(
                'title'         => 'Answers under this Question',
                'context'       => 'normal',
            ),
        ),
        
        
        'to' => array(
            'object_type' => 'post',
            'post_type'   => 'answer',
            'meta_box'    => array(
                'title'         => 'Question this Answer belongs to',
                'context'       => 'normal',
                'empty_message' => 'No Questions created',
            ),
        ),
    ) );
} );
/*
*
* Parent Relationship Metabox
*
*/
add_action( 'add_meta_boxes', 'question_to_answer_relationship_metabox' );

function question_to_answer_relationship_metabox(){
    
    $ParentName = "Question";
    $parentType = "question";
    $thisParentPostId = get_the_ID();
    $ChildName = "Answer";
    $ChildType = "answer";
    $relationshipType = "question_to_answer";
    
    $metaboxID = 'question-to-answer-metabox';

    add_meta_box( 
        $metaboxID,
        'Manage '.$ChildName.'s Assigned to this '.$ParentName,
        'child_post_meta_box_cb',
        array($parentType),
        'normal',
        'default',
        array(
            'parentName' => $ParentName,
            'parentId' => $thisParentPostId,
            'ChildName' => $ChildName,
            'ChildType' => $ChildType,
            'relationshipType' => $relationshipType,
            'metaboxID' => $metaboxID,
        ) 
    );
}