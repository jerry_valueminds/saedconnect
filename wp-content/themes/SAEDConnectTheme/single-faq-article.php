    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
   
    <main class="main-content">
        <!-- Section Header -->
        <header class="overview-header container-wrapper">
            <?php
                /* Get Content for this view */
                /* Variables to store data */
                $sectionName;
                $section_id;
                $section_theme_color;
                $section_hover_color;

                $topicName;
                $sponsorName;
                $sponsorImage;

                $topicArea;
                $articleTitle = get_the_title();
            ?>

            <?php
                /* First Query to get Topic Area Name */
                $topicAreaQuery = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'topic_area_to_faq_article',
                        'to' => get_the_ID(), // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $topicAreaQuery->have_posts() ) : $topicAreaQuery->the_post(); ?>
                    <?php $topicArea = get_the_title(); ?>

                    <?php 
                        /* Second Query to get Topic Name */
                        $topicNameQuery = new WP_Query( array(
                            'relationship' => array(
                                'id'   => 'topic_to_topic_area',
                                'to' => get_the_ID(), // You can pass object ID or full object
                            ),
                            'nopaging' => true,
                        ) );
                        while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post(); ?>   
                            <?php $topicName = rwmb_get_value( 'topic-page-name' ); ?>
                            <?php $sponsorName = rwmb_get_value( 'sponsor-name' ); ?>
                            <?php
                                $images = rwmb_meta( 'sponsor-image', array( 'limit' => 1 ) );
                                $image = reset( $images );
                                $sponsorImage = $image['full_url'];
                            ?>

                            <?php 
                            /* Third Query to get Section Name */
                            $sectionNameQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'section_to_topic',
                                    'to' => get_the_ID(), // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $sectionNameQuery->have_posts() ) : $sectionNameQuery->the_post(); ?>   
                                <?php 
                                    $sectionName = get_the_title();
                                    $section_id = get_the_ID();
                                    $section_theme_color = rwmb_meta( 'section-theme-color' );
                                    $section_hover_color = rwmb_meta( 'section-link-hover-color' );
                                ?>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* THird Query: END */
                        ?>

                    <?php
                        endwhile;
                        wp_reset_postdata();
                        /* Second Query: END */
                    ?>

            <?php
                endwhile;
                wp_reset_postdata();
                /* First Query: END */
            ?>
            <div class="info-box">
                <div class="info">
                    <h2 class="subtitle">
                        <?php echo $sectionName ?>
                    </h2>
                    <h1 class="title txt-color-lighter">
                        <?php echo $topicName ?>
                    </h1>
                </div>
                <div class="sponsor">
                    <div class="intro-text">
                        Content Sponsored by:
                    </div>
                    <img class="logo" src="<?php echo $sponsorImage ?>" alt="">
                </div>
            </div>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <?php // Display posts
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query(array('post_type' => 'topic-area'));
                            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                <li
                                   class="<?php echo ($topicArea == get_the_title()  ? 'active' : '') ?>">
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_title() ?>
                                    </a>
                                </li>  

                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php wp_reset_query(); ?>
                </ul>
            </nav>
        </header>
        <style>
            a{
                color: <?php echo $section_theme_color; ?>;
            }

            a:active {
                color: <?php echo $section_theme_color; ?>;
            }

            a:hover {
                color: <?php echo $section_hover_color; ?>;
            }

            .overview-header .header-nav li.active a {
                color: <?php echo $section_theme_color; ?>;
            }

            .overview-header .header-nav li.active a:before {
                background-color: <?php echo $section_theme_color; ?>;
            }
        </style>
        <section class="container-content-inverse">
            <div class="row">
                <div class="col-md-4 side-nav white-text bg-blue side-nav-container"
                    id="side-nav"
                    style="background-color:<?php echo $section_theme_color; ?>">
                    <article>
                        <h2 class="title">
                            Outline
                        </h2>
                        <ul class="list txt-normal-s">
                            <li class="active">
                                <a class="collapsed" href="">Academics</a>
                            </li>
                            <li>
                                <a href="">Admissions</a>
                            </li>
                            <li>
                                <a href="">Alumni</a>
                            </li>
                            <li>
                                <a href="">ArtCenter Facts</a>
                            </li>
                        </ul>
                        <div class="dropdown">
                            <button class="btn btn-trans-white dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                SECTIONS
                            </button>
                            <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                                <a class="dropdown-item" href="#">
                                    Select...
                                </a>
                                <a class="dropdown-item" href="#">
                                    Cover
                                </a>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-8 left-content-inverse padding-tb-40">
                    <!-- Accordion -->
                    <div class="faq-accordion margin-b-20">
                        <div id="accordion">

                            <?php 
                                $values = rwmb_meta( 'accordion' );
                                $accordionCounter = 1;

                                foreach ( $values as $value ) { ?>

                                    <div class="card">
                                        <div class="card-header" id="heading-<?php echo $accordionCounter; ?>">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $accordionCounter; ?>" aria-expanded="false" aria-controls="collapse<?php echo $accordionCounter; ?>">
                                                    Q: <?php echo $value['accordion-title']; ?>
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapse-<?php echo $accordionCounter; ?>" class="collapse" aria-labelledby="heading-<?php echo $accordionCounter; ?>" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                <article class="text-box sm">
                                                    <p class="">
                                                        <?php echo $value['accordion-content']; ?>
                                                    </p>
                                                </article>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $accordionCounter++ ?>
                                <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <?php endwhile; // end of the loop. ?>

    <?php get_footer() ?>