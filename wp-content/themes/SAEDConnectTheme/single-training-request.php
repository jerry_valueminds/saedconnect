<?php get_header() ?>
    
<?php
    $current_user = wp_get_current_user();
    $gv_id = 9;
    $entry_id = 0;
    
    // TO SHOW THE POST CONTENT
    while ( have_posts() ) : the_post();

        $post_id = get_the_ID();
        $post_author_id = get_the_author_meta('ID');

         /* Get current User ID */
        $current_user = wp_get_current_user();

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                'key' => '2', 'value' => $post_id, //Current logged in user
            )
          )
        );

        /* Get GF Entry Count */
        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

        foreach( $entries as $entry ){

            $entry_id = $entry['id'];
        }


        /*
        *
        *   Get Bid count
        *
        */
        /* Get current User ID */
        $current_user = wp_get_current_user();

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => '2', 'value' => $post_id->ID, //Current logged in user
            )
          )
        );

        /* Get GF Entry Count */
        $order_count = GFAPI::count_entries( 10, $search_criteria );
        $orders = GFAPI::get_entries( 10, $search_criteria );
    
?>

    <main class="main-content">
        <header class="container-wrapper bg-yellow padding-tb-15 margin-b-40">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="txt-sm">
                        <a href="marketplace_service_home.html" class="txt-color-dark padding-r-10">
                            Service Marketplace
                        </a>
                        <i class="fa fa-angle-right padding-r-10"></i>
                        <a href="marketplace_service_category.html" class="txt-color-dark">
                            Sell a Service
                        </a>
                    </h1>
                </div>
            </div>
        </header>
        
        <section class="container-wrapper">
            <div class="row">
                <div class="col-md-10 mx-auto">
                    <div class="row row-10">
                        <div class="col-md-8 padding-lr-10">
                            <h2 class="txt-2em txt-medium margin-b-20">
                                <?php the_title() ?>
                            </h2>
                            <div class="txt-normal-s txt-medium margin-b-20">
                                <i class="fa fa-clock-o padding-r-5"></i>
                                Posted <?php echo get_the_date(); ?>
                            </div>
                            <?php if( $post_author_id == $current_user->id ){ ?>
                                <p class="txt-sm margin-tb-15">
                                    <a
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry_id.'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=marketplace&form-title='.$form_title.'&post_id='.$post_id; ?>"
                                        class="txt-color-green"
                                    >
                                        Edit
                                    </a>
                                    <span class="padding-lr-5">|</span>
                                    <a
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=marketplace&return-view=offers';  ?>" 
                                        class="txt-color-red confirm-delete"
                                    >
                                        Delete
                                    </a>
                                </p>
                            <?php } ?>
                        </div>
                        <div class="col-md-4 text-right padding-lr-10">
                            <a href="" class="btn btn-blue no-m-b txt-xs">
                                Offer a Service
                            </a>
                        </div>
                    </div>
                    <div class="row row-15 padding-b-40">
                        <div class="col-md-7 padding-lr-15 padding-b-15">
                            <!--<div class="margin-b-40">
                                <img src="images/heroes/table.jpg" alt="">
                            </div>-->
                            <article class="margin-b-20">
                                <h3 class="txt-lg txt-medium txt-color-dark margin-b-20">
                                    Description
                                </h3>
                                <article class="text-box txt-normal-s txt-height-1-4">
                                    <?php echo rgar( $entry, 44 ) ?>
                                </article>
                            </article>
                            <?php if( $post_author_id == $current_user->id ){ ?>
                            <article>
                                <header class="margin-b-40">
                                    <h3 class="txt-lg txt-medium margin-b-20">
                                        Bids
                                    </h3>
                                </header>
                                <ul class="txt-normal-s">
                                    <li class="row txt-bold">
                                        <span class="col-4">
                                            Amount
                                        </span>
                                        <span class="col-4">
                                            Time
                                        </span>
                                    </li>
                                <?php foreach( $orders as $order ){ ?>
                                    <li class="row padding-y-10 margin-t-10 padding-t-10 border-t-1 border-color-darkgrey">
                                        <span class="col-4">
                                            <?php echo rgar( $order, '4' ) ?>
                                        </span>
                                        <span class="col-4">
                                            <?php echo human_time_diff( strtotime($order['date_created']), current_time('timestamp', 1) ) . ' ago'; ?>
                                        </span>
                                    </li>
                                
                                <?php } ?>
                                </ul>
                            </article>
                            <?php } ?>
                        </div>
                        <div class="col-md-5 padding-lr-15">                            
                            <div class="border-o-1 border-color-darkgrey padding-o-30 margin-b-20">
                                <h2 class=" margin-b-15">
                                    <span class="txt-xxlg txt-medium">
                                        <?php
                                            $field = 'task_budget';

                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                                echo $meta;
                                            }
                                        ?>
                                    </span> 
                                    Budget
                                </h2> 
                                <p class="txt-normal-s">
                                    Contact seller to negotiate a price point and subscribe for this service
                                </p>
                                <div class="padding-t-20 d-flex align-items-center">
                                    <a
                                        href="https://www.saedconnect.org/service-provider-directory/training-requests-response/?parent_id=<?php echo $post_id ?>"  
                                        class="btn btn-blue txt-sm no-m-b"
                                    >
                                        Make a Bid
                                    </a>
                                    <?php if( $order_count ){ ?>
                                        <span class="padding-l-10">
                                            (<?php echo $order_count; echo ($order_count > 1)? ' Bids' : ' Bid';  ?>)
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="border-o-1 border-color-darkgrey padding-o-30 margin-b-40">
                                <div class="border-b-1 border-color-darkgrey padding-b-15 margin-b-15">
                                    <h3 class="txt-sm txt-bold margin-b-10">
                                        Task Category
                                    </h3>
                                    <p class="txt-xs">
                                        <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                            Information Technology
                                        </a>
                                        <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                            Web Design
                                        </a>
                                    </p>
                                </div>
                                <div class="border-b-1 border-color-darkgrey padding-b-15 margin-b-15">
                                    <h3 class="txt-sm txt-bold margin-b-10">
                                        Skills Required
                                    </h3>
                                    <p class="txt-xs">
                                        <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                            Web Design
                                        </a>
                                        <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                            Graphics Design
                                        </a>
                                        <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                            Fashion Design
                                        </a>
                                    </p>
                                </div>
                                <div class="row border-b-1 border-color-darkgrey padding-b-15 margin-b-15">
                                    <h3 class="col-5 txt-sm txt-bold margin-b-10">
                                        Payment Method
                                    </h3>
                                    <p class="col-7 txt-sm">
                                        <?php
                                            $field = 'task_payment_type';

                                            $meta = get_post_meta($post->ID, $field, false);

                                            if($meta){
                                                foreach($meta as $key=>$value) {
                                                    echo $value;
                                                    if($key < count($meta) - 1 ){
                                                        echo ', ';
                                                    }
                                                }
                                            }
                                        ?>
                                    </p>
                                </div>
                                <div class="row border-b-1 border-color-darkgrey padding-b-15 margin-b-15">
                                    <h3 class="col-5 txt-sm txt-bold margin-b-10">
                                        Location
                                    </h3>
                                    <p class="col-7 txt-sm">
                                        <?php
                                            $field = 'task_locations';

                                            $meta = get_post_meta($post->ID, $field, false);

                                            if($meta){
                                                foreach($meta as $key=>$value) {
                                                    echo $value;
                                                    if($key < count($meta) - 1 ){
                                                        echo ', ';
                                                    }
                                                }
                                            }
                                        ?>
                                    </p>
                                </div>
                                <div class="row border-b-1 border-color-darkgrey padding-b-15 margin-b-15">
                                    <h3 class="col-5 txt-sm txt-bold margin-b-10">
                                        Deadline
                                    </h3>
                                    <p class="col-7 txt-sm">
                                        <?php
                                            $field = 'task_delivery_deadline';

                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                                echo $meta;
                                            }
                                        ?>
                                    </p>
                                </div>
                                <div class="row">
                                    <h3 class="col-5 txt-sm txt-bold margin-b-10">
                                        Expires
                                    </h3>
                                    <p class="col-7 txt-sm">
                                        <?php
                                            $field = 'task_expiry_date';

                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                                echo $meta;
                                            }
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
<?php
    endwhile; //resetting the page loop
?>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<?php get_footer() ?>