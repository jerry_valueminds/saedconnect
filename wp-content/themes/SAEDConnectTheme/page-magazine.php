    <?php /*Template Name: Magazine Home*/ ?>
    
    <?php get_header() ?>
   
    <main class="main-content">
       
    <header class="magazine-header row">
            <div class="cool-md-10">
                <h1 class="title">ACCELERATE BLOG</h1>
            </div>
            <div class="col-md-2">
                <div class="dropdown">
                    <button class="btn btn-trans-green dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        SECTIONS
                    </button>
                    <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                        <a class="dropdown-item" href="#">
                            Select...
                        </a>
                        <!-- Get All Top Magazine Terms -->
                        <?php

                            //Get Terms
                            $terms = get_terms( 'magazine', array('hide_empty' => false,)); //Get all the terms

                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                // Check and see if the term is a top-level parent. If so, display it.
                                $parent = $term->parent;

                                if ( $parent=='0' ) {

                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                                    $term_url = get_term_link($term);
                        ?>

                            <a class="dropdown-item" href="<?php echo $term_url ?>">
                                <?php echo $term_name; ?>  
                            </a>
                        <?php
                                } 
                            }

                            //Revert to Previous Multisite
                            //restore_current_blog();
                        ?>
                    </div>
                </div>
            </div>
        </header>
        
        <?php
            $services = new WP_Query(
                array(
                    'post_type' => 'story',
                    'showposts' => 1,
                )
            );

            while ($services->have_posts()) : $services->the_post();
        ?>
            <?php if (has_post_thumbnail()) { ?>

                <header class="article-banner" style="background-image:url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>');">

            <?php } else { ?>
                
                <header class="article-banner" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.jpg');">

            <?php } ?>
                <div class="content">
                    <span class="date">
                        <?php echo get_the_date( 'F j, Y' ); ?>
                    </span>
                    <h1 class="title">
                        <a href="">
                            <?php the_title(); ?>
                        </a>
                    </h1>
                    <div class="btn-wrapper">
                        <a class="btn btn-trans-wb" href="<?php the_permalink() ?>">Read Article</a>
                    </div>
                </div>
            </header>
        
        <?php endwhile;
            // Reset things, for good measure
            $services = null;
            wp_reset_postdata();
        ?>
            
        <?php
        // Get all the categories
        $categories = get_terms( 'magazine' );
        $category_counter = 1;

        // Loop through all the returned terms
        foreach ( $categories as $category ):
            
            // set up a new query for each category, pulling in related posts.
            $services = new WP_Query(
                array(
                    'post_type' => 'story',
                    'showposts' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy'  => 'magazine',
                            'terms'     => array( $category->slug ),
                            'field'     => 'slug'
                        )
                    )
                )
            );
        ?>
        <section>
            <header class="container-wrapper padding-t-40 padding-b-20">
                <h2 class="article-header-lg uppercase">
                    <?php echo $category->name; ?>
                </h2>
            </header>
            <div class="container-wrapper">
                <article class="row row-40">
                    <?php
                        $article_counter = 1;
                        while ($services->have_posts()) : $services->the_post();
                    ?>
                    <?php
                        if($article_counter == 1){
                            if($category_counter % 2 == 1){
                                $column_width = 12;
                            }else{
                                $column_width = 8;
                            }
                        }else{
                            $column_width = 4;
                        }
                    ?>
                    <div class="col-md-<?php echo $column_width ?> padding-lr-40 margin-b-30">
                        <a class="featured-article" href="<?php the_permalink() ?>">
                            <figure class="image-box">
                                <?php if (has_post_thumbnail()) { ?>

                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>" alt="">

                                <?php } else { ?>

                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.jpg" alt="">

                                <?php } ?>
                                
                                <?php if ( has_term( 'profile', 'content-type' ) ) {?>
                                    <figcaption class="caption-icon caption-video">
                                        PROFILE
                                    </figcaption>
                                <?php } elseif ( has_term( 'book-review', 'content-type' ) ) {?>
                                    <figcaption class="caption-icon caption-video">
                                        BOOK REVIEW
                                    </figcaption>
                                <?php } elseif ( has_term( 'feauture', 'content-type' ) ) {?>
                                    <figcaption class="caption-icon caption-video">
                                        FEATURE
                                    </figcaption>
                                <?php } elseif ( has_term( 'interview', 'content-type' ) ) {?>
                                    <figcaption class="caption-icon caption-video">
                                        INTERVIEW
                                    </figcaption>
                                <?php } ?>
                            </figure>
                            <h4 class="title<?php if($column_width > 4){echo '-lg';}?>">
                                <?php the_title(); ?>
                            </h4>
                            <p class="date">
                                <?php echo get_the_date( 'F j, Y' ); ?>
                            </p>
                        </a>
                    </div>
                    <?php $article_counter++; ?>
                    <?php endwhile;
                        // Reset things, for good measure
                        $services = null;
                        wp_reset_postdata();
                    ?>
                </article>
            </div>
            
        
            <footer class="container-wrapper padding-tb-40 border-b-1 border-color-darkgrey">
                <h4 class="text-right">
                    <a class="txt-bold txt-color-blue uppercase" href="<?php echo get_term_link($category) ?>">
                        MORE FROM <?php echo $category->name; ?>
                    </a>
                </h4>
            </footer>
        </section>
        <?php
        // end the loop
        $category_counter++;
        endforeach;
        ?>
        
        
        <section class="container-wrapper bg-grey padding-tb-60">
            <h4 class="txt-xxlg txt-bold margin-b-20">
                ACCELERATE BLOG
            </h4>
            <ul class="big-list uppercase">
                <!-- Get All Top Level Industry Terms from Learn a Skill Multisite -->
                <?php 
                    //Switch to Learn a Skill Multisite (id = 7)
                    //switch_to_blog(7);

                    //Get Terms
                    $terms = get_terms( 'magazine', array('hide_empty' => false,)); //Get all the terms

                    foreach ($terms as $term) { //Cycle through terms, one at a time

                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        if ( $parent=='0' ) {

                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                            $term_url = get_term_link($term);
                ?>

                    <li>
                        <a href="">
                            <?php echo $term_name; ?>   
                        </a>
                    </li>
                <?php
                        } 
                    }

                    //Revert to Previous Multisite
                    //restore_current_blog();
                ?>
            </ul>
        </section>
    </main>
    
    <?php get_footer() ?>