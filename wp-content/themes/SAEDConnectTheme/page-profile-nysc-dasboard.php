<?php /*Template Name: Profile - NYSC Dashboard*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Get Avatar */
        $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
        $meta_key = 'user_avatar_url';
        $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

        if($get_avatar_url){
            $avatar_url = $get_avatar_url;
        }
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header_nysc' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav-nysc' ); ?>
            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        Welcome to your NYSC admin dashboard
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Here, you can see all interventions running in your state, and those waiting for your approval. You can reach out to your partners via announcements, and see their complaints or questions feedbacks.
                    </p>
                </article>
                
                <article class="padding-b-40">
                    <h3 class="txt-normal-s txt-medium txt-color-dark margin-b-20">
                        Partner / Corps Member Summary
                    </h3>
                    <div class="row row-5">
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-lg txt-bold padding-b-10">
                                    25
                                </div>
                                <div class="txt-sm txt-height-1-2">
                                    Partner verification requests
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-lg txt-bold padding-b-10">
                                    9
                                </div>
                                <div class="txt-sm txt-height-1-2">
                                    Shown Interest in your state
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-lg txt-bold padding-b-10">
                                    39
                                </div>
                                <div class="txt-sm txt-height-1-2">
                                    Active Partners
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-lg txt-bold padding-b-10">
                                    2,450
                                </div>
                                <div class="txt-sm txt-height-1-2">
                                    Enrolled Corps Members
                                </div>
                            </a>
                        </div>
                    </div>
                </article>
                
                <article class="padding-b-20">
                    <h3 class="txt-normal-s txt-medium txt-color-dark margin-b-20">
                        Intervention Summary
                    </h3>
                    <div class="row row-5">
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-lg txt-bold padding-b-10">
                                    129
                                </div>
                                <div class="txt-sm txt-height-1-2">
                                    Active Interventions
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-lg txt-bold padding-b-10">
                                    45
                                </div>
                                <div class="txt-sm txt-height-1-2">
                                    Un-Approved Interventions
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-lg txt-bold padding-b-10">
                                    12
                                </div>
                                <div class="txt-sm txt-height-1-2">
                                    Un-Endorsed Interventions
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-lg txt-bold padding-b-10">
                                    19
                                </div>
                                <div class="txt-sm txt-height-1-2">
                                    Un-Recommended Interventions
                                </div>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>