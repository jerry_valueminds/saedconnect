    <?php /*Template Name: Hompage - Side Hustle Communities*/ ?>    
    
    <?php get_header() ?>
    
    <?php
        $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

        if($incoming_from_saedconnect){
            
            /* Include saedconnect Navigation */
            get_template_part( 'template-parts/navigation-saedconnect' );
        }
    ?>
   
    <main class="main-content">
        <?php
            $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

            if(!$incoming_from_saedconnect){

                /* Include Skilli Navigation */
                get_template_part( 'template-parts/navigation-skilli-secondary' );
            }
        ?>
       
        <section class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h2 class="d-inline-block bg-yellow-dark txt-color-white txt-xlg emtxt-bold padding-o-10 margin-b-20">
                        Side Hustle Communities                  
                    </h2>
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        What Business do You Want To Start?
                        <br>
                        We are Here to Help.
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        <p>
                            Side Hustle communities are exclusive, expert-led, highly moderated communities that provides access to all the ready-to-go learning, resources, mentorship & support you need to start specific small businesses and get it right the first time - opening new pathways to development, success and profits for you. Side Hustle or Main Hustle. Start earning from a Small Business of your choice.
                        </p>
                    </h2>
                    <!--<div class="margin-t-40">
                        <a href="#get-started" class="btn btn-trans-bw">
                            Get started
                        </a>
                    </div>-->
                </div>
            </div>
        </section>
        
        
        <section class="container-wrapper">
            <div class="margin-b-40 text-center">
                <h2 class="txt-2em txt-light txt-height-1-1">
                    Get Expert Support to Start any of 40+ Businesses
                </h2>
            </div>
        </section>
        
        <div class="bg-grey padding-tb-40">
        <?php
            /* Query */
            wp_reset_postdata();
            $temp = $wp_query; $wp_query= null;
            $wp_query = new WP_Query();
            $wp_query->query(array('post_type' => 'information-session'));
        ?>
        
        <?php 

            /* Get Terms */
            $terms = get_terms( 'shc-category', array('hide_empty' => false,)); //Get all the terms

            foreach ($terms as $term) { //Cycle through terms, one at a time

                // Check and see if the term is a top-level parent. If so, display it.
                $parent = $term->parent;

                if ( $parent=='0' ) {

                    $term_id = $term->term_id; //Get the term ID
                    $term_name = $term->name; //Get the term name
                    $term_url = get_term_link($term);
        ?>
                    
            
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-yellow  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-<?php echo $term_id ?>" aria-expanded="false" aria-controls="main-<?php echo $term_id ?>">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                <?php echo $term_name; ?> 
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-<?php echo $term_id ?>" class="collapse bg-yellow" aria-labelledby="heading-<?php echo $term_id ?>" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                <?php
                                    /* Loop */
                                    while ($wp_query->have_posts()) : $wp_query->the_post();
                                        if ( has_term( $term_name, 'shc-category' ) ) {
                                ?>

                                        <li class="col-6 col-md-4 padding-lr-15">
                                            <a href="<?php the_permalink() ?>">
                                                <i class="fa fa-chevron-right txt-color-dark"></i>
                                                <span class="txt-color-dark">
                                                    <?php the_title() ?>
                                                </span>
                                            </a>
                                        </li>

                                <?php
                                        }
                                    endwhile;
                                ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php
                } 
            }
        ?>
        </div>
        
        <section class="padding-tb-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <header class="padding-b-80 text-center">
                            <h1 class="txt-2-2em txt-light txt-height-1-1 margin-b-20">
                                What you get
                            </h1>
                            <h2 class="txt-normal-s txt-height-1-7">
                                We are going all out to ensure you get all the support & resources you require to start and excel in each business. 
                            </h2>
                        </header>
                        <div class="row row-20">
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10">
                                    <div class="col-3 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_expert_instructions.png" alt="">
                                    </div>
                                    <div class="col-9 padding-lr-10">
                                        <p class="txt-bold txt-color-light txt-height-1-1">
                                            Expert Resources, Instruction & Guidance
                                        </p>
                                        <div class="collapse" id="collapse-1">
                                            <div class="padding-tb-20">
                                                <ul class="icon-list txt-sm">
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Learn the skills & business acumen required to start & thrive in each business
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Access to experienced Mentors available to help you in real time
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Access useful Templates, Reference Guides & tailored advice from experts
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Access Tons of real-life questions & answers
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Connect directly with other practitioners who can handhold you to start and grow your own business
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <p>
                                            <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">
                                                Learn more
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10">
                                    <div class="col-3 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="">
                                    </div>
                                    <div class="col-9 padding-lr-10">
                                        <p class="txt-bold txt-color-light txt-height-1-1">
                                            Real-Life Learning from wherever you are
                                        </p>
                                        <div class="collapse" id="collapse-2">
                                            <div class="padding-tb-20">
                                                <ul class="icon-list txt-sm">
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Access real-live entrepreneur experiences that help you appreciate the practical application of lessons shared.
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Discover what works for other businesses. Success stories and pitfalls to avoid
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Get critical, tailored information relevant to your situation, not just Generic, one-size-fit all content
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Meet practitioners near you who can give you the opportunity to get practical experience before you start
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <p>
                                            <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-1">
                                                Learn more
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10">
                                    <div class="col-3 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_real_world_experience.png" alt="">
                                    </div>
                                    <div class="col-9 padding-lr-10">
                                        <p class="txt-bold txt-color-light txt-height-1-1">
                                            Start your own business 
                                        </p>
                                        <div class="collapse" id="collapse-3">
                                            <div class="padding-tb-20">
                                                <ul class="icon-list txt-sm">
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Learn new skills and develop fresh new ideas to use in your business  
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            You are challenged to apply the skills and support you get to start up your own business
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Hit the road running: You will learn things that you can use immediately after a session
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <p class="">
                                            <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-1">
                                                Learn more
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10">
                                    <div class="col-3 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_amazing_support_network.png" alt="">
                                    </div>
                                    <div class="col-9 padding-lr-10">
                                        <p class="txt-bold txt-color-light txt-height-1-1">
                                            Thrive within a Powerful Support Network
                                        </p>
                                        <div class="collapse" id="collapse-4">
                                            <div class="padding-tb-20">
                                                <ul class="icon-list txt-sm">
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Benefit from Peer-to-Peer Motivation 
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Test new ideas and strategies an get a wide range of opinions
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Connect directly with other practitioners who can handhold you to start and grow your own business
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            We occasionally run programs that would keep you accountable to start your business within a specified time frame
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <p class="">
                                            <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">
                                                Learn more
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10">
                                    <div class="col-3 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_challenge_the_status_quo.png" alt="">
                                    </div>
                                    <div class="col-9 padding-lr-10">
                                        <p class="txt-bold txt-color-light txt-height-1-1">
                                            Explore every aspect of the business
                                        </p>
                                        <div class="collapse" id="collapse-5">
                                            <div class="padding-tb-20">
                                                <p class="txt-sm">
                                                    You will find someone with experience in virtually every aspect of the business to help you through your challenges.
                                                </p>
                                            </div>
                                        </div>
                                        <p class="">
                                            <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">
                                                Learn more
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-20 padding-b-40">
                                <div class="row row-10">
                                    <div class="col-3 padding-lr-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_test_your_ideas.png" alt="">
                                    </div>
                                    <div class="col-9 padding-lr-10">
                                        <p class="txt-bold txt-color-light txt-height-1-1">
                                            Access to Growth Opportunities
                                        </p>
                                        <div class="collapse" id="collapse-6">
                                            <div class="padding-tb-20">
                                                <ul class="icon-list txt-sm">
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Experience personal development
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Challenge the status quo, get inspired with new strategies to grow your business 
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Access a Network of suppliers and partners
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            Get first-hand information about local Events and network meetings where you can meet potential partners, buyers, experts & investors
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <p class="">
                                            <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">
                                                Learn more
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="text-center margin-t-40">
                            <a href="#get-started" class="btn btn-trans-bw">
                                Get Started
                            </a>
                        </div>-->
                    </div>
                </div>
            </div>
        </section>
                
        <section class="margin-b-40">
            <div class="container-wrapper">
                <div class="bg-grey padding-lr-40 padding-t-80 padding-b-40 rounded-o-12">
                    <div class="row row-40">
                        <div class="col-md-4 padding-lr-40 padding-b-40 text-center text-md-left">
                            <header class="margin-b-30">
                                <h2 class="txt-2-6em txt-medium txt-height-1-2">
                                    Structured
                                    to help you start and thrive
                                </h2>    
                            </header>
                            <article>
                                <p class="txt-normal-s txt-height-1-7">
                                    Each business community is deliberately structured to ensure you get the best help irrespective of where you are in your business Journey.
                                </p>
                            </article>
                            <!--<div class="d-none d-md-block margin-t-40">
                                <a href="#get-started" class="btn btn-trans-bw">
                                    Get Started
                                </a>
                            </div>-->
                        </div>
                        
                        <div class="col-md-8 padding-lr-40">
                            <div class="row row-30 text-center">
                                <div class="col-md-6 padding-lr-40 padding-b-40">
                                    <figure class="margin-b-20">
                                        <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_amazing_support_network.png" alt="" width="100">
                                    </figure>
                                    <article>
                                        <h4 class="txt-bold margin-b-5 txt-height-1-2">
                                            Success Stories
                                        </h4>
                                        <h5 class="txt-xs txt-height-1-3">
                                             Share, learn and get inspired from real-life stories of how people started & grew their <?php echo rwmb_get_value( 'info-session-name' ); ?>, connect with them and get mentored.
                                        </h5>
                                    </article>
                                </div>
                                <div class="col-md-6 padding-lr-40 padding-b-40">
                                    <figure class="margin-b-20">
                                        <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_build.png" alt="" width="100">
                                    </figure>
                                    <article>
                                        <h4 class="txt-bold margin-b-5 txt-height-1-2">
                                            Key Information & Reference Materials
                                        </h4>
                                        <h5 class="txt-xs txt-height-1-3">
                                             Access a continuously updated repository solid expert-developed Articles, Videos and resources across every aspect of the <?php echo rwmb_get_value( 'info-session-name' ); ?>.
                                        </h5>
                                    </article>
                                </div>
                                <div class="col-md-6 padding-lr-40 padding-b-40">
                                    <figure class="margin-b-20">
                                        <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_challenge_the_status_quo.png" alt="" width="100">
                                    </figure>
                                    <article>
                                        <h4 class="txt-bold margin-b-5 txt-height-1-2">
                                            Q&As and Expert Conversations
                                        </h4>
                                        <h5 class="txt-xs txt-height-1-3">
                                             Engage in meaningful conversations, Ask your tailor-made questions and get rich answers and opinions from a wide pool of experts.
                                        </h5>
                                    </article>
                                </div>
                                <div class="col-md-6 padding-lr-40 padding-b-40">
                                    <figure class="margin-b-20">
                                        <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_connect.png" alt="" width="100">
                                    </figure>
                                    <article>
                                        <h4 class="txt-bold margin-b-5 txt-height-1-2">
                                            Offers & Business Opportunities
                                        </h4>
                                        <h5 class="txt-xs txt-height-1-3">
                                             Share and access partnership/business opportunities and offers within an exclusive group of serious minded business practitioners.
                                        </h5>
                                    </article>
                                </div>
                            </div>
                        </div>
                        
                        <!--<div class="d-md-none col-md-12 padding-lr-12">
                            <div class="margin-t-40 text-center">
                                <a href="#get-started" class="btn btn-trans-bw no-m-b">
                                    Get Started
                                </a>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </section>
        
        <style>
            
            .gform_wrapper .top_label .gfield_label {
                font-size: 0.8rem !important;
                margin-bottom: 0 !important;
            }
            
            .ginput_container input, .ginput_container select, .ginput_container textarea{
                width: 100% !important;
                font-size: 0.9rem !important;
                padding: 0.4rem 0.8rem !important;
            }
            
            .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
                margin-top: 0px !important;
            }
            
            .gform_button.button{
                margin: 0 auto !important;
                width: auto !important;
            }
        </style>
        
        <!--<section class="container-wrapper bg-green-lime padding-t-60 padding-b-40" id="get-started">
            <header class="padding-b-20 text-center">
                <h1 class="txt-2-2em txt-light txt-height-1-1 margin-b-20">
                    Let's do this Together
                </h1>
                <h2 class="txt-normal-s txt-height-1-7">
                    You don't have to walk alone. Tell us what small business you want to start to build & we will take it from there.
                </h2>
            </header>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <?php echo do_shortcode('[gravityform id="101" title="false" description="false"]'); ?>
                    <?php //echo do_shortcode('[gravityform id="21" title="false" description="false"]'); ?>
                </div>
            </div>
        </section>-->
        
        <!--
        <section>
            <header class="container-wrapper padding-t-40 padding-b-20">
                <h2 class="article-header-lg txt-color-lighter">
                    A
                </h2>
            </header>
            <article class="container-wrapper">
                <ul class="row row-40 directory-list txt-xlg">
                <?php wp_reset_postdata(); ?>
                    <?php wp_reset_query(); ?>
                    <?php // Display posts
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query(array('post_type' => 'information-session', 'posts_per_page' => 1, 'nopaging' => true, ));
                            while ($wp_query->have_posts()) : $wp_query->the_post();                
                ?>
                    <li class="col-md-4 padding-lr-40">
                        <a href="<?php the_permalink() ?>">
                            <?php the_title() ?>
                        </a>
                    </li>
                <?php endwhile; ?>
                </ul>
            </article>
        </section>
        -->

        <?php
            $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

            if(!$incoming_from_saedconnect){

                /* Include Skilli Footer */
                get_template_part( 'template-parts/footer-skilli' );
            }
        ?>
    </main>
    
<?php
    $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

    if($incoming_from_saedconnect){

        /* Include Saedconnect Footer */
        get_template_part( 'template-parts/footer-saedconnect' );
    }
?>
    
<?php get_footer() ?>


<?php
    $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

    if($incoming_from_saedconnect){
?>

   <script>
        $(document).ready(function(){
            $('form').attr('autocomplete', 'off');
            
            /*
            *   Auto populate form with Track ID & Name
            */
            $('#formModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var track_id = button.attr('track-id'); // Extract Track ID
                var track_name = button.attr('track-name'); // Extract Track Name

                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);

                //modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('#input_1_6').val(track_id);
                modal.find('#input_1_7').val(track_name);
            })


            //Confirmation Modal
            $('#formConfirmationModal').modal('show')

            //Clear URL
           // var uri = window.location.toString();
            //if (uri.indexOf("?") > 0) {
                //var clean_uri = uri.substring(0, uri.indexOf("?"));
                //window.history.replaceState({}, document.title, clean_uri);
            //}

            //Append Saedconnect Query string
            $('a').each(function(index, element) {
              var newAttr = $(element).attr('href') + '?saedconnect=true';
              $(element).attr('href', newAttr);
            });
        });
    </script>

<?php } ?>