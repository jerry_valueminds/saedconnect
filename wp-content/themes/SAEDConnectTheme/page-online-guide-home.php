    <?php /*Template Name: Online Guide Home*/ ?>
    
    <?php get_header() ?>

        <main class="main-content font-main">
        <?php
            // TO SHOW THE PAGE CONTENTS
            while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                <section class="container-wrapper txt-color-white bg-home" style="background-image:url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>');">
                    <div class="container content text-center">
                        <h2 class="txt-3em margin-b-20"><?php the_title(); ?></h2>
                        <div class="row">
                            <div class="col-md-8 mx-auto">
                                <article class="txt-sm">
                               <?php the_content(); ?>
                           </article>
                            </div>
                        </div>
                    </div>
                </section>
            <?php
            endwhile; //resetting the page loop
            wp_reset_query(); //resetting the page query
        ?>
        
       <!--<section class="courses-sub-menu">
           <div class="container">
               <ul class="list">
                   <li class="active"><a href="online_courses.html">Learn</a></li>
                   <li><a href="trainings.html">Tools & Resources</a></li>
                   <li><a href="trainings.html">Mentors</a></li>
                   <li><a href="discussions.html">Programs & Events</a></li>
               </ul>
           </div>
       </section>-->
       
       <section class="row">
            <!-- Main Content -->
            <div class="container padding-t-40">
                <?php wp_reset_postdata(); ?>
                <?php wp_reset_query(); ?>
                <?php // Display posts
                    $temp = $wp_query; $wp_query= null;
                    $wp_query = new WP_Query();
                    $wp_query->query(array('post_type' => 'guide-section'));
                        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                           <div class="row row-40 padding-tb-20 border-b-1 border-color-darkgrey">

                                <div class="row col-md-5 padding-lr-40 margin-b-20 margin-reset-lr-10 center-mobile">
                                   <div class="col-md-2 padding-lr-10 margin-b-20 txt-align-center">
                                       <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" width="80" alt="">
                                       <!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/empowerment_opp.png" width="80" alt="">-->
                                   </div>
                                   <div class="col-md-10 padding-lr-10">
                                       <h3 class="txt-lg txt-bold txt-height-1-4 margin-b-20">
                                           <a href="<?php the_permalink() ?>">
                                               <?php echo the_title(); ?>
                                           </a>
                                       </h3>
                                       <article class="txt-sm">
                                           <?php echo the_content(); ?>
                                       </article>
                                   </div>
                                </div>
                                <div class="col-md-7 padding-lr-40">
                                    <ul class="row row-40 course-list-disc txt-normal-s">
                                        <?php 

                                            $connected = new WP_Query( array(
                                                'relationship' => array(
                                                    'id'   => 'og_section_to_mini_site',
                                                    'from' => get_the_ID(), // You can pass object ID or full object
                                                ),
                                                'nopaging' => true,
                                            ) );
                                            while ( $connected->have_posts() ) : $connected->the_post(); ?>
                                                <div class="col-md-6 padding-lr-40">
                                                    <li class="margin-b-10" style="line-height:1.3em!important">
                                                       <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                                    </li>
                                                </div>
                                        <?php
                                            endwhile;
                                            wp_reset_postdata();
                                        ?>
                                    </ul>
                               </div>
                           </div>
               <?php endwhile; ?>               
            </div>
        </section>

       
       <!--<section>
            <div class="row">
                <div class="col-md-8">
                    <a class="feature-video-block" style="background-image:url('images/heroes/hero_8.jpg');">
                        <span class="caption">
                            <h4 class="subtitle txt-color-white">
                                Featured Courses
                            </h4>
                            <h3 class="title txt-color-white">
                                Advanced Cinematography
                            </h3>
                        </span>     
                    </a>
                </div>
                <div class="col-md-4 cta-block bg-black txt-color-white">
                    <article>
                        <div class="txt-3em txt-color-white padding-tb-20 border-t-4 border-color-white">
                            <i class="fa fa-quote-left"></i>
                        </div>
                        <h2 class="title">
                            The level of talent I was surrounded by when I started here was a bit overwhelming at first, until I realized these were people I’d be able to work with and learn from.
                        </h2>
                        <div class="txt-medium txt-height-1-2">
                            Anne Burdick
                            <br>
                            Chair, Media Design Practices
                        </div>
                    </article>
                </div>
            </div>
        </section>-->
        
    </main>
        
    <?php get_footer() ?>