<?php /*Template Name: Homepage-Trainer-directory*/ ?>
    
<?php get_header() ?>

<main class="font-main txt-color-light bg-white" style="margin-top: 100px">
    <header class="course-directory-banner image sm" style="background-image:url('http://www.saedconnect.org/learn-a-skill/wp-content/themes/SAEDConnectTheme/images/grid/image (41).jpg');">
        <div class="content container-wrapper txt-color-white text-center">
            <h1 class="title">
                Find Trainers
            </h1>
            <h2>
                What do you want to learn? Explore a nationwide pool of trainers accross different skills & subjects.
            </h2>
        </div>
    </header>

    <section class="bg-darkgrey padding-t-20">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-4 padding-lr-40 padding-b-20">
                    <div class="row">
                        <div>
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="" width="35">
                        </div>
                        <div class="flex_1 padding-l-15">
                            <h3 class="txt-medium txt-height-1-5 margin-b-5">
                                Post a Training Request
                            </h3>
                            <p class="txt-sm">
                                Want to learn something new? Make a request of what you want to learn and let trainers contact you.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-40 padding-b-20">
                    <div class="row">
                        <div>
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="" width="35">
                        </div>
                        <div class="flex_1 padding-l-15">
                            <h3 class="txt-medium txt-height-1-5 margin-b-5">
                                Become a Trainer
                            </h3>
                            <p class="txt-sm">
                                Monetize your knowledge. Teach whatever you know & earn by registering as a trainer.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-40 padding-b-20">
                    <div class="row">
                        <div>
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="" width="35">
                        </div>
                        <div class="flex_1 padding-l-15">
                            <h3 class="txt-medium txt-height-1-5 margin-b-5">
                                View Training Requests
                            </h3>
                            <p class="txt-sm">
                                Monetize your knowledge. Teach whatever you know & earn by registering as a trainer.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="margin-tb-50">
        <div class="container-wrapper">
            <div class="row row-15">
                <div class="col-md-4 padding-lr-15 d-flex padding-b-20">
                    <a
                        class="flex_1 dashboard-partner-card row align-items-center bg-grey"
                        href="<?php echo site_url().'/trainer-directory' ?>"
                    >
                        <div>
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="" width="35">
                        </div>
                        <div class="title">
                            All Trainers
                        </div>
                    </a>
                </div>
                <?php
                    /* Get Terms */
                    $terms = get_terms( 'training-category', array('hide_empty' => false, 'parent' => 0));

                    foreach($terms as $term){
                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        $tax_slug = $filter_item['term_slug'];

                        $term_id = $term->term_id; //Get the term ID
                        $term_slug = $term->slug; //Get the term ID
                        $term_name = $term->name; //Get the term name
                        $term_url = get_term_link($term);
                ?>
                   
                    <div class="col-md-4 padding-lr-15 d-flex padding-b-20">
                    
                        <a
                            class="flex_1 dashboard-partner-card row align-items-center"
                            href="<?php echo site_url().'/trainer-directory/?trainer_skill_areas='.$term_slug ?>"
                        >
                            <div>
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="" width="35">
                            </div>
                            <div class="title">
                                <?php echo $term_name ?>
                            </div>
                        </a>
                    </div>
                
                <?php } ?>
            </div>
        </div>
    </section>


    <!-- Enroll via SMS Modal -->
<div class="modal fade font-main coming-soon-modal" id="smsEnrollModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row content text-center">
                <div class="left gf-white">
                    <form>
                        <div class="gform_heading">
                            <h2 class="txt-4em text-center margin-b-10">
                                <i class="fa fa-mobile-phone "></i>
                            </h2>
                            <h3 class="txt-xlg txt-medium margin-b-30">
                                How to enroll via SMS
                            </h3>
                            <div class="text-center">
                                <p class="code-label txt-bold text-center margin-b-30">
                                    <span>
                                        Course Code:
                                    </span>
                                    TV024
                                </p>
                            </div>
                            <p class="txt-normal-s margin-b-20">
                                To enroll, send an SMS to 30448 in this format:
                            </p>
                            <p class="txt-normal-s margin-b-20">
                                [SAED], [Course Code], [Full Name], [State of Residence], [Contact Telephone Number], [WhatsApp Number if any]
                            </p>
                            <p class="txt-normal-s margin-b-20">
                                eg. 
                            </p>
                            <p class="txt-normal-s margin-b-20">
                                SAED, TV024, John Doe, Kebbi, 07031234567, 07031234567
                            </p>
                            <div class="text-center">
                                <p class="code-label txt-bold text-center margin-b-30">
                                    Send to 30448
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</main>
   
<?php get_footer() ?>