    <?php /*Template Name: Homepage-Programs Update*/ ?>
    
    <?php get_header() ?>
    
    <?php
        /* Publication key for getting Admin Approved content */
        $publication_key = 'publication_status';
    ?>
    
    <style>
        
        .overview-header .header-nav li.active a {
            color: green;
        }
        
        .overview-header .header-nav li.active a:before {
            background-color: green;
        }
    </style>
    <main class="main-content">
        <!--<header class="overview-header container-wrapper">
            <h1 class="title">Programs</h1>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <li class="active">
                        <a href="<?php get_site_url();  ?>">
                            All Opportunities 
                        </a>
                    </li>
                    <li>
                        <a href="">Partner Opportunities</a>
                    </li>
                    <li>
                        <a href="">Around the Web</a>
                    </li>
                    <li>
                </ul>
            </nav>
        </header>-->
        
        <header class="overview-header container-wrapper">
            <h1 class="title">
                <span class="txt-bold txt-height-1-2 padding-b-20">
                    Opportunity Center
                </span>
            </h1>
        </header>
        
        <section class="padding-t-40 padding-b-10">
            <form id="filter-accordion" class="filter-accordion">                
                <?php
                    /* Filter Options Array */
                    $filter_items_array = array(
                        array(
                            'term_slug' =>'program-type',
                            'term_name' => 'Program Type'
                        ),
                        
                        array(
                            'term_slug' =>'industry',
                            'term_name' => 'Industry'
                        ),
                        
                        array(
                            'term_slug' =>'coverage-nigeria',
                            'term_name' => 'Location'
                        ),
                    );
                ?>
                
                <!-- Accordion Header -->
                <div class="container-wrapper">
                    <div class="row row-5">
                    <?php
                        if($_GET){
                    ?>
                        <div class="padding-lr-5 padding-b-10">
                            <div class="txt-xs margin-b-5 txt-color-white">
                                Clear Filter
                            </div>
                            <a href="https://www.saedconnect.org/opportunity-center/" class="btn btn-dark txt-normal-s">
                                <i class="fa fa-times"></i>
                                Clear Filter
                            </a>
                        </div>
                    <?php } ?>
                    
                    <?php foreach($filter_items_array as $filter_item){ ?>
                        <div class="text-center padding-lr-5 padding-b-10">
                            <div class="txt-xs txt-medium margin-b-5">
                                <?php echo $filter_item['term_name']; ?>
                            </div>
                            <button
                                id="heading-<?php echo $filter_item['term_slug']; ?>"
                                class="btn btn-trans-green dropdown-toggle txt-normal-s
                                    <?php
                                        if($_GET['s-'.$filter_item['term_slug']] != ''){
                                            echo 'bg-darkgrey';
                                        }
                                    ?>
                                "
                                type="button" data-toggle="collapse"
                                data-target="#filter-content-<?php echo $filter_item['term_slug']; ?>"
                                aria-expanded="false"
                                aria-controls="filter-content-<?php echo $filter_item['term_slug']; ?>"
                            >
                                <?php
                                    if($_GET['s-'.$filter_item['term_slug']] != ''){
                                        $term = get_term_by('id', $_GET['s-'.$filter_item['term_slug']], $filter_item['term_slug']);
                                        echo $term->name;
                                    } else {
                                        echo 'All';
                                    }
                                ?>
                            </button>
                        </div>
                    <?php } ?>
                        <div class="padding-lr-5 padding-b-10">
                            <div class="txt-xs margin-b-5 txt-color-white">
                                Go
                            </div>
                            <input class="btn btn-green txt-normal-s" type="submit" value="GO">
                        </div>
                    </div>
                </div>
                
                <!-- Accordion Content -->
                <div class="container-wrapper bg-grey">
                <?php foreach($filter_items_array as $filter_item){ ?>
                    <div
                        id="filter-content-<?php echo $filter_item['term_slug']; ?>"
                        class="collapse"
                        aria-labelledby="heading-<?php echo $filter_item['term_slug']; ?>"
                        data-parent="#filter-accordion"
                    >
                        <div class="custom-radio txt-sm padding-t-40 padding-b-30">
                            <label class="radio-item parent=term">
                                <input
                                    type="radio"
                                    name="s-<?php echo $filter_item['term_slug']; ?>"
                                    value=""
                                    <?php 
                                        if('' == esc_html($_REQUEST['s-'.$term_slug])){
                                            echo 'checked';
                                        }
                                    ?>
                                >
                                <span class="text">
                                    All
                                </span>
                            </label>
                        <?php 

                            //Get Terms
                            $terms = get_terms( $filter_item['term_slug'], array('hide_empty' => false, 'parent' => 0)); //Get all the terms

                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                // Check and see if the term is a top-level parent. If so, display it.
                                $parent = $term->parent;

                                $term_id = $term->term_id; //Get the term ID
                                $term_slug = $filter_item['term_slug'];
                                $term_name = $term->name; //Get the term name
                                $term_url = get_term_link($term);
                        ?>
                            
                                <label class="radio-item <?php echo ($parent == '0'  ? 'parent-term' : '') ?>">
                                    <input
                                        type="radio"
                                        name="s-<?php echo $filter_item['term_slug']; ?>"
                                        value="<?php echo $term_id ?>"
                                        <?php 
                                            if($term_id == esc_html($_REQUEST['s-'.$term_slug])){
                                                echo 'checked';
                                            }
                                        ?>
                                    >
                                    <span class="text">
                                        <?php echo $term_name; ?>
                                    </span>
                                </label>
                        <?php

                            }

                        ?>
                        </div>
                    </div>
                <?php } ?>
                </div>
                
            </form>
        </section>
        
        <?php if(!$_GET){ ?>
        <!-- Featured Programs -->
        <section class="bg-grey padding-tb-20">
            <div class="container-wrapper">
                <h2 class="txt-lg txt-medium txt-color-dark">
                    Feautured Programs
                </h2>
            </div>
        </section>

        <section class="container-wrapper margin-b-80">
            <div class="program-card-wrapper">
                <div class="row row-20">
                <?php wp_reset_postdata();
                    wp_reset_query();
                    $temp = $wp_query; $wp_query= null;
                    $wp_query = new WP_Query();
                    $wp_query->query( 
                        array(
                            'post_type' => 'opportunity',
                            'post_status' => 'publish',
                            'posts_per_page' => 4,
                            'meta_query' => array(
                                array(
                                    'key' => 'featured',
                                    'value' => '1422',
                                ),
                                array(
                                    'key' => $publication_key,
                                    'value' => 'admin_published'
                                )
                            ),
                        ) 
                    );

                    if ( $wp_query->have_posts() ) {

                        while ($wp_query->have_posts()) : $wp_query->the_post();

                        /* Variables */
                        $program_id = $post->ID;    //Get Program ID

                        /* Get Entry */
                        /* GF Search Criteria */
                        $gf_id = 1;
                        $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    'key' => '31', 'value' => $program_id, //Current logged in user
                                )
                            )
                        );

                        /* Get GF Entry Count */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                        $entry = $entries[0];

                        /* Get Opportunity Banner */
                        $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                        $image = reset( $images );
                                    
                        /* Get expiry date */
                        $expiry_date = rwmb_meta( 'opportunity-close-date' );
                        $field = 'submit_opportunity_expires';
                        $exp = 2;
                        $td = 1;



                        if( rwmb_meta( 'opportunity-close-date' ) ){

                            $expiry_date = rwmb_meta( 'opportunity-close-date' );
                            $today_date = date('y-m-d');

                            $exp = strtotime( $expiry_date );
                            $td = strtotime( $today_date );

                        } else if( get_post_meta($post->ID, $field, true) ) {


                            $meta = get_post_meta($post->ID, $field, true);

                            if($meta){
                                $expiry_date = $meta;
                            }

                            $today_date = date('m/d/y');

                            $expiry_date = strtotime( $expiry_date );
                            $td = strtotime( $today_date );

                            /*echo "<br><br><br>back-today: ".$exp;
                            echo "<br>Expire: ".$td;*/

                        }
                        
                        /* Check if expired */
                        if( true ){
                        /*if( $td < $expiry_date ){*/
                        
                ?>
                    <div class="col-md-3 program-card">
                        <div class="content">
                            <figcaption>
                                <?php 
                                    $deadline = get_post_meta( $program_id, 'deadline', true );
                            
                                    if( $deadline ){
                                ?>
                                <h4 class="date">
                                    <span class="expires">
                                        Expires
                                    </span>
                                    <?php
                                        $field = 'submit_opportunity_expires';
                                        $date = strtotime( $deadline );

                                        echo date('j F Y', $date);
                                    ?>
                                </h4>
                                <?php } ?>
                                <div class="header">
                                    <h3 class="title">
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                                <p class="description">
                                    <?php echo get_post_meta( $program_id, 'summary', true ) ?>
                                </p>
                                <?php $meta = get_post_meta( 'author', $program_id ); ?>
                                <?php if($meta){ ?>
                                <h4 class="author">Powered by
                                    <?php echo $meta ?>
                                </h4>
                                <?php } ?>
                            </figcaption>
                            <div class="image-box">
                                <?php 
                                    $images = "";
                                    $images = get_attached_media( 'image', $program_id ); 
                                ?>
            
                                <?php if($images){ ?>

                                    <?php  
                                        foreach($images as $image) { //print_r( $image ); 
                                            $previousImg_id = $image->ID;
                                    ?>
                                        <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">

                                    <?php } ?>

                                <?php } else { ?>
                                   
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png">

                                <?php } ?>
                            </div>
                        </div>
                    </div>


                <?php
                    }
                        endwhile;

                    }else{
                ?>
                    <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                        <h2 class="txt-lg txt-medium">
                            No Opportunities found.
                        </h2>
                    </div>   

                <?php

                    }
                ?>
			    </div>
            </div>
        </section>
        <?php } ?>
        
        <!-- All Programs -->
        <section class="bg-grey padding-tb-20">
            <div class="container-wrapper">
                <h2 class="txt-lg txt-medium txt-color-dark">
                    <?php
                        if(!$_GET){ 
                            echo 'All Programs';
                        } else {
                            echo 'Filter Results';
                        }
                    ?>
                </h2>
            </div>
        </section>
        
        <section class="container-wrapper padding-b-40">
        <?php wp_reset_postdata();
            wp_reset_query();
            $temp = $wp_query; $wp_query= null;
            $wp_query = new WP_Query();
            $wp_query->query( 
                array(
                    'post_type' => 'opportunity',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'meta_query' => array(
                        array(
                            'key' => $publication_key,
                            'value' => 'admin_published'
                        )
                    ),
                ) 
            );

            if ( $wp_query->have_posts() ) {
                
                /* Get Post Count */
                $count = 1;
                $total_posts = $wp_query->found_posts;

                while ($wp_query->have_posts()) : $wp_query->the_post();

                /* Variables */
                $program_id = $post->ID;    //Get Program ID
                
                /* Get Entry */
                /* GF Search Criteria */
                $gf_id = 1;
                $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            'key' => '31', 'value' => $program_id, //Current logged in user
                        )
                    )
                );

                /* Get GF Entry Count */
                $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                $entry = $entries[0];

                /* Get Opportunity Banner */
                $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                $image = reset( $images );
                
                /* Get expiry date */
                $expiry_date = rwmb_meta( 'opportunity-close-date' );
                $field = 'submit_opportunity_expires';
                $exp = 2;
                $td = 1;
                
                
                
                if( rwmb_meta( 'opportunity-close-date' ) ){
                    
                    $expiry_date = rwmb_meta( 'opportunity-close-date' );
                    $today_date = date('y-m-d');

                    $exp = strtotime( $expiry_date );
                    $td = strtotime( $today_date );
                    
                } else if( get_post_meta($post->ID, $field, true) ) {
                    

                    $meta = get_post_meta($post->ID, $field, true);

                    if($meta){
                        $expiry_date = $meta;
                    }
                    
                    $today_date = date('m/d/y');

                    $expiry_date = strtotime( $expiry_date );
                    $td = strtotime( $today_date );
                    
                    /*echo "<br><br><br>back-today: ".$exp;
                    echo "<br>Expire: ".$td;*/
                    
                }
                
                
                                

                /* Check if expired */
                        if( true ){
                        /*if( $td < $expiry_date ){*/
                    
                //if( $td > $exp ){
        ?>
        
        <?php if($count == 1){  ?>
           
            <div class="program-card-wrapper">
                <div class="row row-20">
                
        <?php } ?>         
                    <div class="col-md-3 program-card">
                        <div class="content">
                            <figcaption>
                                <?php 
                                    $deadline = get_post_meta( $program_id, 'deadline', true );
                            
                                    if( $deadline ){
                                ?>
                                <h4 class="date">
                                    <span class="expires">
                                        Expires
                                    </span>
                                    <?php
                                        $field = 'submit_opportunity_expires';
                                        $date = strtotime( $deadline );

                                        echo date('j F Y', $date);
                                    ?>
                                </h4>
                                <?php } ?>
                                <div class="header">
                                    <h3 class="title">
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                                <p class="description">
                                    <?php echo get_post_meta( $program_id, 'summary', true ) ?>
                                </p>
                                <?php $meta = get_post_meta( 'author', $program_id ); ?>
                                <?php if($meta){ ?>
                                <h4 class="author">Powered by
                                    <?php echo $meta ?>
                                </h4>
                                <?php } ?>
                            </figcaption>
                            <div class="image-box">
                                <?php 
                                    $images = "";
                                    $images = get_attached_media( 'image', $program_id ); 
                                ?>
            
                                <?php if($images){ ?>

                                    <?php  
                                        foreach($images as $image) { //print_r( $image ); 
                                            $previousImg_id = $image->ID;
                                    ?>
                                        <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">

                                    <?php } ?>

                                <?php } else { ?>
                                   
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png">

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    
        <?php
            if( $count == 4 || $count == $total_posts ){ ?>
                          
                </div>
            </div>
            
        <?php
                $count = 0;
            }
            $count++;   
        ?> 

        <?php
                }
                endwhile;

            } else {
        ?>
            <div class="padding-t-20 padding-b-40">
                <h2 class="txt-lg txt-medium">
                    No Opportunities found.
                </h2>
            </div>   

        <?php

            }
        ?>
        </section>
        <!--<section class="container-wrapper">
            <div class="padding-t-20 padding-b-10 text-center">
                <a class="btn btn-trans-green" href="">
                    Show More
                </a>
            </div>
        </section>-->
    </main>
   
    <?php get_footer() ?>
