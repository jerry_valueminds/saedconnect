<?php /*Template Name: Profile - Competency - View*/ ?>
   
<?php

    if ( !is_user_logged_in() ) {
        // If User is Logged in, redirect to User Dashbord
        $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID

        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }

?>

<?php get_header() ?>

<?php
    /* Get current user */
    $current_user = wp_get_current_user();

    /* Get Avatar */
    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
    $meta_key = 'user_avatar_url';
    $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

    if($get_avatar_url){
        $avatar_url = $get_avatar_url;
    }

    /* Get User Display Name */
    switch_to_blog(1);

    $gf_id = 4; //Form ID
    $gv_id = 1385; //Gravity View ID
    $title = 'Profile';

    $entry_count = 0;

    /* GF Search Criteria */
    $search_criteria = array(

    'field_filters' => array( //which fields to search

        array(

            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
            )
        )
    );

    /* Get Entries */
    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

    /* Get GF Entry Count */
    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

    if($entry_count){ //If no entry
        foreach( $entries as $entry ){          
            $displayname = rgar( $entry, '4.3' );
        }                
    }   

    restore_current_blog();
?>

<?php
    /* Variable to check profile completion */
    $check = array();

    /* Template info */
    $pages_array = array(
        array(
            'name' => 'Contact Information',
            'template' => 'contact-information',
        ),
        array(
            'name' => 'Work Profile Snapshot',
            'template' => 'work-profile-snapshot',
        ),
        array(
            'name' => 'Education & Work Experience',
            'template' => 'education-and-work-experience',
        ),
        array(
            'name' => 'Skills and Capabilities',
            'template' => 'skills-and-capabilities',
        ),
        array(
            'name' => 'Languages and Interests',
            'template' => 'languages-and-interests',
        ),
        array(
            'name' => 'Certifications and Affiliations',
            'template' => 'certifications-and-Affiliations',
        ),
        array(
            'name' => 'Review your submission',
            'template' => 'review',
        ),
    );
    $max_page_step = count($pages_array);

    /* Select Page View Request */
    $get_page_step = intVal($_GET['step']);
    if( 
        $get_page_step 
        && ($get_page_step >= 1) 
        && ($get_page_step <= $max_page_step) 
    ){
        $page_step = $get_page_step; 
    } else {
        $page_step = 1;
    }

    /* Get User */
    $current_user = wp_get_current_user();
?>

<?php 
    /* Get Image Field */
    function get_the_image_url($post_id){
        $images = get_attached_media( 'image', $post_id );
        $previousImg_id = 0;
        if($images){
            foreach($images as $image) { 
                $previousImg_id = $image->ID;
            }
            return wp_get_attachment_url($previousImg_id,"full");
        } else {
            return 0;
        }
    }
?>

<main class="main-content" style="padding-top: 70px;margin-top:0;background-color: #e0e0e0">
    <section class="container-wrapper padding-t-40 padding-b-40">
        <div class="margin-b-20">
            <div class="flex_1 d-flex align-items-center padding-t-80">
                <div class="col-auto text-center">
                    <figure class="user-avatar" style="background-image: url('<?php echo $avatar_url ?>')"></figure>
                </div>
                <h2 class="col padding-tb-20 padding-lr-30">
                    <header class="col txt-xlg txt-height-1-2 txt-color-dark">
                        <h1 class="txt-light margin-b-5">Welcome, <?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?></h1>
                        <h2 class="txt-bold margin-b-5">View & Update Your Work Profile</h2>
                    </header>
                    <p>Add only accurate information about yourself. If any employer reports that you misrepresented your competence in your information, your account can be banned.</p>
                </h2>
            </div>
        </div>
    </section>

    <!-- General Information -->  
    <section class="container-wrapper padding-b-10">
        <?php include( locate_template( 'template-parts/competency/view.php', false, false ) ); ?>
    </section>

    <!-- Submit-->   
    <section class="container-wrapper padding-t-20 padding-b-40">
        <a href="https://www.saedconnect.org/competency-profile/profile/" class="btn btn-blue txt-normal-s">Back to Profile Manager</a>
    </section>
</main> 

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>


<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>