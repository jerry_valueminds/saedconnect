    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>

    <?php
        /* 
        *=============================================
        * Get Content for this view
        *=============================================
        */

        /* Variable to Check if Post has Mini-site Grand Parent */
        $has_parent = 'topic';
        
        /* Variables to store data */
        $miniSiteID;
        $miniSiteName;
    
        $sectionName;
        $section_id;
        $section_theme_color;
        $section_hover_color;

        $multiPageID;

        $topicName;
        $topicID;
        $sponsorName;
        $sponsorImage;

        $multiPageName;
        $articleTitle = get_the_title();

        /*
        *=============================================
        * First Query to get Multi-page Name 
        *=============================================
        */
        $multiPageQuery = new WP_Query( array(
            'relationship' => array(
                'id'   => 'three_c_m_page_to_m_page_article',
                'to' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
        while ( $multiPageQuery->have_posts() ) : $multiPageQuery->the_post();
            $multiPageName = get_the_title();
            $multiPageID = get_the_ID();
            $sponsorName = rwmb_get_value( 'sponsor-name' );

            /*
            *=============================================
            * Second Query to get Topic Name or Mini-site Name
            *=============================================
            */
            // Topic Name Query
            $topicNameQuery = new WP_Query( array(
                'relationship' => array(
                    'id'   => 'og_topic_to_three_c_m_page',
                    'to' => get_the_ID(), // You can pass object ID or full object
                ),
                'nopaging' => true,
            ) );

            // Minisite Query
            $miniSiteQuery = new WP_Query( array(
                'relationship' => array(
                    'id'   => 'mini_site_to_three_c_m_page',
                    'to' => get_the_ID(), // You can pass object ID or full object
                ),
                'nopaging' => true,
            ) );

            /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
            if ( $topicNameQuery->have_posts() ){
                while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post();  

                $topicName = rwmb_get_value( 'topic-page-name' );
                $topicID = get_the_ID();
                $images = rwmb_meta( 'sponsor-image', array( 'limit' => 1 ) );
                $image = reset( $images );
                $sponsorImage = $image['full_url'];

                    /*
                    *=============================================
                    * Third Query to get Section Name 
                    *=============================================
                    */
                    $sectionNameQuery = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'og_section_to_og_topic',
                            'to' => get_the_ID(), // You can pass object ID or full object
                        ),
                        'nopaging' => true,
                    ) );
                    while ( $sectionNameQuery->have_posts() ) : $sectionNameQuery->the_post();  
                            $sectionName = get_the_title();
                            $section_id = get_the_ID();
                            $section_theme_color = rwmb_meta( 'section-theme-color' );
                            $section_hover_color = rwmb_meta( 'section-link-hover-color' );

                    endwhile;
                    wp_reset_postdata();
                    /*
                    *=============================================
                    * Third Query: END
                    *=============================================
                    */

                endwhile;
                wp_reset_postdata();
            }elseif($miniSiteQuery->have_posts()){
            
                while ( $miniSiteQuery->have_posts() ) : $miniSiteQuery->the_post();  
                    $miniSiteName = get_the_title();
                    $miniSiteID = get_the_ID();
                    $has_parent = 'mini-site'; 

                endwhile;
                wp_reset_postdata();
            
            }else{
                /* Set to false if there is no Mini-site grand parent to this article  */
                $has_parent = false;
            }
            /*
            *=============================================
            * Second Query: END
            *=============================================
            */

        endwhile;
        wp_reset_postdata();
        /*
        *=============================================
        * First Query: END
        *=============================================
        */
    ?>
            
    <main class="main-content">
        <?php if($has_parent == 'topic') { ?>
            <header class="overview-header container-wrapper">
                <div class="info-box">
                    <div class="info">
                        <h2 class="subtitle">
                            <?php echo $sectionName ?>
                        </h2>
                        <h1 class="title txt-color-lighter">
                            <?php echo $topicName ?>
                        </h1>
                    </div>
                    <div class="sponsor">
                        <div class="intro-text">
                            Content Sponsored by:
                        </div>
                        <img class="logo" src="<?php echo $sponsorImage ?>" alt="">
                    </div>
                </div>
                <nav class="header-nav">
                    <div class="header-nav-title">
                        <span class="name">
                            Submenu
                        </span>
                        <span class="icon"></span>
                    </div>
                    <ul>
                        <!-- Get All Three column Posts related to the Parent Topic -->
                        <?php 
                            /* Third Query to get Section Name */
                            $subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'og_topic_to_three_c_m_page',
                                    'from' => $topicID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   

                            <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* THird Query: END */
                        ?>

                        <!-- Get All Three column Posts related to the Parent Topic -->
                        <?php 
                            /* Third Query to get Section Name */
                            $subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'og_topic_to_two_c_m_page',
                                    'from' => $topicID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   

                            <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* THird Query: END */
                        ?>

                        <!-- Get All External Multi-item Posts related to the Parent Topic -->
                        <?php 
                            /* Third Query to get Section Name */
                            $subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'og_topic_to_ex_multi_item',
                                    'from' => $topicID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   

                            <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* THird Query: END */
                        ?>
                    </ul>
                </nav>
            </header>
        <?php }elseif($has_parent == 'mini-site'){ ?>
             <!-- Section Header Mini-site-->
            <header class="overview-header container-wrapper">
                <div class="info-box">
                    <div class="info">
                        <h1 class="title txt-color-lighter">
                            <?php echo $miniSiteName ?>
                        </h1>
                    </div>
                    <div class="sponsor">
                        <div class="intro-text">
                            Content Sponsored by:
                        </div>
                        <img class="logo" src="<?php echo $sponsorImage ?>" alt="">
                    </div>
                </div>
                <nav class="header-nav">
                    <div class="header-nav-title">
                        <span class="name">
                            Submenu
                        </span>
                        <span class="icon"></span>
                    </div>
                    <ul>
                        <!-- Get All Three column Posts related to the Parent Topic -->
                        <?php 
                            /* Third Query to get Section Name */
                            $subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_three_c_m_page',
                                    'from' => $miniSiteID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   

                            <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* THird Query: END */
                        ?>

                        <!-- Get All Two column Posts related to the Parent Mini-site -->
                        <?php 
                            $subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_two_c_m_page',
                                    'from' => $miniSiteID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   

                            <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>

                        <!-- Get All External Multi-item Posts related to the Parent Mini-site -->
                        <?php 
                            $subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_m_item_page',
                                    'from' => $miniSiteID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   

                            <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>
                    </ul>
                </nav>
            </header>
        <?php }else{ ?>
            <!-- Section Header Standalone -->
            <header class="overview-header container-wrapper">
                <h1 class="title">
                    <?php echo the_title(); ?>
                </h1>
            </header>
        <?php } ?>
        <style>
            a{
                color: <?php echo $section_theme_color; ?>;
            }

            a:active {
                color: <?php echo $section_theme_color; ?>;
            }

            a:hover {
                color: <?php echo $section_hover_color; ?>;
            }

            .overview-header .header-nav li.active a {
                color: <?php echo $section_theme_color; ?>;
            }

            .overview-header .header-nav li.active a:before {
                background-color: <?php echo $section_theme_color; ?>;
            }
        </style>
        <section class="container-wrapper">
            <div class="row row-40">
                <!-- Left Side Menu -->               
                <div class="col-md-3 padding-lr-40">
                    <article class="side-nav white bg-white padding-tb-40" id="side-nav">
                        <h2 class="title">
                            Outline
                        </h2>
                        <ul class="list txt-normal-s">
                        <?php
                            /* 
                            *
                            *====================================================
                            * Nav Loop: Get ALl Sibling Posts from Multi-Page Parent
                            *====================================================
                            *
                            */
                            $multi_page_query = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'three_c_m_page_to_m_page_article',
                                    'from' => $multiPageID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $multi_page_query->have_posts() ) : $multi_page_query->the_post();
                        ?>

                            <?php $topicName = rwmb_get_value( 'article-hierarchy-level' ); ?>      
                            <li class="<?php echo ($articleTitle == get_the_title()  ? 'active' : '') ?>">
                                <a 
                                   class="padding-l-<?php echo rwmb_get_value( 'article-hierarchy-level' ); ?>"
                                   href="<?php the_permalink() ?>"
                                   style="background-color:<?php echo ($articleTitle == get_the_title()   ? $section_theme_color : '') ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* 
                            *
                            *====================================================
                            * Nav Loop: END
                            *====================================================
                            *
                            */
                        ?>
                        </ul>
                        <div class="dropdown">
                            <button class="btn btn-trans-white dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                TOPIC AREAS
                            </button>
                            <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                                <?php // Display posts
                                    $temp = $wp_query; $wp_query= null;
                                    $wp_query = new WP_Query();
                                    $wp_query->query(array('post_type' => 'article'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                                        <?php $topicName = rwmb_get_value( 'article-hierarchy-level' ); ?>      
                                            <a class="dropdown-item" href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Main Content -->
                <div class="col-12 col-md-6 padding-lr-40 padding-t-40 border-lr-1 border-color-darkgrey">
                                        
                    <?php 
                    
                        $connected = new WP_Query( array(
                            'relationship' => array(
                                'id'   => 'multi_page_article_to_cb',
                                'from' => get_the_ID(), // You can pass object ID or full object
                            ),
                            'nopaging'     => true,
                        ) );
                        while ( $connected->have_posts() ) : $connected->the_post();
                    
                    ?>
                        <?php /* Retrieve Content Type */ ?>  
                        <?php $content_type = rwmb_get_value( 'select-content-type' ); ?>
                        
                        <?php /* Check Content Type & Use Appropriate Markup to display content */ ?> 
                        <?php if($content_type == 'gi-plain-text-meta-box'){ ?>
                            
                            <!-- Text -->
                            <article class="text-box margin-b-10">
                                <?php echo rwmb_get_value( 'plain-text-content' ); ?>
                            </article>
                            
                        <?php }elseif($content_type == 'gi-highlight-meta-box'){ ?>
                            
                            <!-- Highlighted Text -->
                            <article class="text-box margin-b-20 padding-o-20 bg-blue txt-color-white">
                                <?php echo rwmb_get_value( 'highlight-content' ); ?>
                            </article>

                        <?php } elseif($content_type == 'gi-image-meta-box'){ ?> 
                            <!-- Image -->
                            <figure class="margin-b-20">
                                <?php

                                    $images = rwmb_meta( 'mini_site_feature_image', array( 'limit' => 1 ) );
                                    $image = reset( $images );
                                ?>

                                <img src="<?php echo $image['full_url']; ?>" alt="<?php the_title() ?>">
                            </figure>

                        <?php } elseif($content_type == 'gi-video-meta-box'){ ?> 
                            <!-- Video -->
                            <figure class="col-12 margin-b-10">
                                <?php

                                    $images = rwmb_meta( 'video-feature-image', array( 'limit' => 1 ) );
                                    $image = reset( $images );
                                ?>

                                <a class="feature-video-block popup-video" href="<?php echo rwmb_get_value( 'video-link' ); ?>" style="background-image:url('<?php echo $image['full_url']; ?>">
                                    <span class="caption">
                                        <h3 class="title txt-color-white">
                                            <?php the_title() ?>
                                        </h3>
                                    </span>
                                    <div class="play-btn">
                                        <i class="fa fa-play"></i>
                                    </div>      
                                </a>
                            </figure>

                        <?php } elseif($content_type == 'gi-accordion-meta-box'){ ?> 

                            <!-- Accordion -->
                            <div class="faq-accordion margin-b-20">
                                <div id="accordion">
                                    <?php 
                                        $values = rwmb_meta( 'accordion' );
                                        $accordionCounter = 1;

                                        foreach ( $values as $value ) { ?>

                                            <div class="card">
                                                <div class="card-header" id="heading-<?php echo $accordionCounter; ?>">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $accordionCounter; ?>" aria-expanded="false" aria-controls="collapse<?php echo $accordionCounter; ?>">
                                                            <?php echo $value['accordion-title']; ?>
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="collapse-<?php echo $accordionCounter; ?>" class="collapse" aria-labelledby="heading-<?php echo $accordionCounter; ?>" data-parent="#accordion" style="">
                                                    <div class="card-body">
                                                        <article class="text-box sm">
                                                            <p class="">
                                                                <?php echo $value['accordion-content']; ?>
                                                            </p>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $accordionCounter++ ?>
                                        <?php } ?>
                                </div>
                            </div>

                        <?php } ?>
                                
                                                                
                    <?php
                        endwhile;
                        wp_reset_postdata();
                    ?>
                    
                </div>
                <!-- Right Side Content -->
                <div class="col-md-3">                    
                    <?php
                        global $relashionship_id; 
                        global $relationship_parent_id;

                        $relashionship_id = 'three_c_m_page_to_rcb';
                        $relationship_parent_id = $multiPageID;
                    ?>
                    <?php get_template_part( 'template-parts/section-side-content' ); ?>
                </div>
            </div>
        </section>
        <!-- Get Section Footer Posts: Using Multi-Item General Section Template Part -->
        <section>
           <div class="row" style="align-items: flex-start;">
            <?php 
                global $relashionship_id; 
                global $relationship_parent_id;
            
                $relashionship_id = 'three_c_m_page_to_bcb';
                $relationship_parent_id = $multiPageID;
            ?>
            <?php get_template_part( 'template-parts/multi-item-general-content' ); ?>
            </div>
        </section>
    </main>
    
    <?php endwhile; // end of the loop. ?>

    <?php get_footer() ?>
    
