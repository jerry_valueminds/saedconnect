<?php /*Template Name: Profile*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>

            <div class="dashboard-multi-main-content">
                
            
            <?php if ( $_REQUEST['view'] == 'form' ){ ?>
               
                <?php if( $_GET['type'] == 'social-profile' ) { ?>
                    <div class="page-header">
                        <h1 class="page-title">
                            Edit My Social Details          
                        </h1>
                        <div class="cta">
                            <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                                Cancel
                            </a>
                        </div>
                    </div>
                    <article class="page-summary">
                        <p>
                            Give us information about your social accounts e.g. Facebook, Twitter and Instagram.
                        </p>
                    </article>
                    <form class="form" action="<?php echo currenturl(false) ?>" method="post">
                    <?php

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        /* Get Post */
                        switch_to_blog(109);
                        
                        $post_type = 'social-profile';
                        $profile_query = new WP_Query();
                        $profile_query->query( 
                            array(
                                'post_type' => $post_type,
                                'post_status' => 'publish',
                                'author' => $current_user->ID,
                                'posts_per_page' => 1,
                            ) 
                        );

                        if ( $profile_query->have_posts() ) {

                            while ($profile_query->have_posts()) : $profile_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;   //Get Program ID

                            endwhile;

                        }
                                                               
                        if($_POST){
                            
                            /* Get Post Name */
                            $postName = 'social-Profile-'.$current_user->ID;
                            
                            /* Meta */
                            $email = sanitize_text_field( $_POST['email'] ); 
                            $phone = sanitize_text_field( $_POST['phone'] ); 
                            $website = sanitize_text_field( $_POST['website'] ); 
                            $facebook = sanitize_text_field( $_POST['facebook'] ); 
                            $twitter = sanitize_text_field( $_POST['twitter'] ); 
                            $instagram = sanitize_text_field( $_POST['instagram'] ); 
                            $linkedin = sanitize_text_field( $_POST['linkedin'] ); 

                            /* Save Post to DB */
                            $post_id = wp_insert_post(array (
                                'ID' => $post_id,
                                'post_type' => $post_type,
                                'post_title' => $postName,
                                'post_content' => "",
                                'post_status' => 'publish',
                            ));

                            update_post_meta( $post_id, 'email', $email );
                            update_post_meta( $post_id, 'phone', $phone );
                            update_post_meta( $post_id, 'website', $website );
                            update_post_meta( $post_id, 'facebook', $facebook );
                            update_post_meta( $post_id, 'twitter', $twitter );
                            update_post_meta( $post_id, 'instagram', $instagram );
                            update_post_meta( $post_id, 'linkedin', $linkedin );
                            
                            restore_current_blog();

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                        }
                    ?>
                    <div class="form-item">
                        <label for="email">Your Email Address</label>
                        <input type="email" name="email" id="email" value="<?php echo get_post_meta( $post_id, 'email', true ); ?>">
                    </div>
                    <div class="form-item">
                        <label for="phone">Your Phone Number</label>
                        <input type="tel" name="phone" id="phone" value="<?php echo get_post_meta( $post_id, 'phone', true ); ?>">
                    </div>
                    <div class="form-item">
                        <label for="website">Personal Blog/Website</label>
                        <input type="url" name="website" id="website" value="<?php echo get_post_meta( $post_id, 'website', true ); ?>">
                    </div>
                    <div class="form-item">
                        <label for="facebook">Facebook</label>
                        <input type="url" name="facebook" id="facebook" value="<?php echo get_post_meta( $post_id, 'facebook', true ); ?>">
                    </div>
                    <div class="form-item">
                        <label for="twitter">Twitter</label>
                        <input type="url" name="twitter" id="twitter" value="<?php echo get_post_meta( $post_id, 'twitter', true ); ?>">
                    </div>
                    <div class="form-item">
                        <label for="instagram">Instagram</label>
                        <input type="url" name="instagram" id="instagram" value="<?php echo get_post_meta( $post_id, 'instagram', true ); ?>">
                    </div>
                    <div class="form-item">
                        <label for="linkedin">Linkedin</label>
                        <input type="url" name="linkedin" id="linkedin" value="<?php echo get_post_meta( $post_id, 'linkedin', true ); ?>">
                    </div>
                    
                    <div class="text-right">
                        <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
                    
                <?php } ?>

            <?php
                /* Else view template */    
                } else {

            ?>
            
            <div class="page-header">
                <h1 class="page-title">
                    My Profile     
                </h1>
            </div>
            <article class="page-summary">
                <p>
                    Input information about where you went to school, the trainings you have attended and details about what you did in your previous jobs. If you are a freelancer, include the names of clients you have worked for, and what you did for them.
                </p>
            </article>
                
                <!-- Basic Information -->
            <div class="section-wrapper">
            <?php
                switch_to_blog(1);
                
                $gf_id = 4; //Form ID
                $gv_id = 1445; //Gravity View ID
                $title = 'Basic Information';

                $entry_count = 0;

                /* GF Search Criteria */
                $search_criteria = array(

                'field_filters' => array( //which fields to search

                    array(

                        'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                    )
                );

                /* Get Entries */
                $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                /* Get GF Entry Count */
                $entry_count = GFAPI::count_entries( $gf_id, $search_criteria ); 
            ?>

                <?php if(!$entry_count){ //If no entry ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Input basic information about yourself like your name, gender, year of birth, etc.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-form=%s&cv-part=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                <?php } else { ?>
                    <?php foreach( $entries as $entry ){ ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=basic-information&cv-form-title='.$title ?>" 
                                    class="edit-btn"
                                >
                                    Edit
                                </a>
                            </div>
                        </div>
                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Name          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Username          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Email         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 2 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Phone         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 7 ); ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                <?php } ?>
                
                <?php restore_current_blog(); ?>
            </div>
            
            <!-- Social Details -->
            <div class="section-wrapper">
                <div class="header">
                    <h2 class="section-wrapper-title">
                        Social Profile                          
                    </h2>
                    <div class="text-right">
                        <a href="<?php echo currenturl(true); ?>?view=form&type=social-profile" class="edit-btn">
                            Add / Edit Profile
                        </a>
                    </div>
                </div>

                <div class="entry">
                <?php
                    switch_to_blog(109); 
                    $profile_query = new WP_Query();
                    $profile_query->query( 
                        array(
                            'post_type' => 'social-profile',
                            'post_status' => 'publish',
                            'author' => $current_user->ID,
                            'posts_per_page' => 1,
                        ) 
                    );

                    if ( $profile_query->have_posts() ) {

                        while ($profile_query->have_posts()) : $profile_query->the_post();

                        /* Variables */
                        $post_id = $post->ID;   //Get Program ID
                ?>
                    <div class="row row-10">
                        <div class="col-md-6 padding-lr-10 padding-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                Your Email Address     
                            </p>
                            <article class="text-box txt-sm">
                                <?php echo get_post_meta( $post_id, 'email', true ); ?>
                            </article>
                        </div>
                        <div class="col-md-6 padding-lr-10 padding-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                Your Phone Number      
                            </p>
                            <article class="text-box txt-sm">
                                <?php echo get_post_meta( $post_id, 'phone', true ); ?>
                            </article>
                        </div>
                        <div class="col-md-6 padding-lr-10 padding-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                Personal Blog/Website     
                            </p>
                            <article class="text-box txt-sm">
                                <?php echo get_post_meta( $post_id, 'website', true ); ?>
                            </article>
                        </div>
                        <div class="col-md-6 padding-lr-10 padding-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                Facebook      
                            </p>
                            <article class="text-box txt-sm">
                                <?php echo get_post_meta( $post_id, 'facebook', true ); ?>
                            </article>
                        </div>
                        <div class="col-md-6 padding-lr-10 padding-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                Twitter     
                            </p>
                            <p class="txt-sm">
                                <?php echo get_post_meta( $post_id, 'twitter', true ); ?>
                            </p>
                        </div>
                        <div class="col-md-6 padding-lr-10 padding-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                Instagram    
                            </p>
                            <p class="txt-sm">
                                <?php echo get_post_meta( $post_id, 'instagram', true ); ?>
                            </p>
                        </div>
                        <div class="col-md-6 padding-lr-10 padding-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                Linkedin    
                            </p>
                            <p class="txt-sm">
                                <?php echo get_post_meta( $post_id, 'linkedin', true ); ?>
                            </p>
                        </div>
                    </div>

                <?php
                        endwhile;

                    }else{
                ?>

                    <p class="txt-sm padding-b-15">You have not added your social profile information.</p>

                <?php } ?>
                <?php restore_current_blog(); ?>
                </div>
            </div>
            
            <?php } ?>
            
            
            </div>

        </section>
    </main>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>