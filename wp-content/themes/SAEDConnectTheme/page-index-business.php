    <?php /*Template Name: Homepage-Business*/ ?>
    
    <?php get_header() ?>
   
    <main class="main-content">
        <header class="overview-header container-wrapper">
            <h1 class="title">
                <span class="txt-bold txt-height-1-2 padding-b-20">
                    Start / Build a Business
                </span>
            </h1>
        </header>
        
        <!-- General Section -->
        <section>
            <div class="row">
                <article class="col-md-4 feature-image-block d-flex"
                    style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (30).png');">
                    <div class="content txt-color-white d-flex flex-column justify-content-between">
                        <div class="">
                            <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                                The Entrepreneurship Incubator
                            </h3>
                            <p class="txt-height-1-5 margin-b-30">
                                Build your entrepreneurship capacity, generate brilliant ideas & launch your own business at the end of the incubation period.
                            </p>
                        </div>
                        <div>
                            <a class="btn btn-trans-wb" href="http://www.saedconnect.org/skill-splash/program/the-entrepreneurship-incubator/">
                                Enroll
                            </a>
                        </div>
                    </div>   
                </article>
                <article class="col-md-4 feature-image-block d-flex"
                    style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (20).jpg');">
                    <div class="content txt-color-white d-flex flex-column justify-content-between">
                        <div class="">
                            <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                                Side Hustle Communities
                            </h3>
                            <p class="txt-height-1-5 margin-b-30">
                                Start making money by engaging in one of many easy-to-start small businesses near you. There are lots of exciting small business opportunities you can start on the Internet, in Finance, Agriculture, trading and a lot of other areas. Get ready-to-go training and support on how to engage in these businesses, and start benefitting from it almost immediately.
                            </p>
                        </div>
                        <div>
                            <a class="btn btn-trans-wb" href="http://www.saedconnect.org/skill-splash/program/side-hustle-skills/">
                                Explore
                            </a>
                        </div>
                    </div>   
                </article>
               
                <div class="col-md-4 cta-block bg-green-lime d-flex">
                    <article class="txt-color-dark align-self-end">
                        <h2 class="title txt-height-1-2">
                            Get access to information, resources, support, programs and people that will help you in your quest to start and grow your business.
                        </h2>
                    </article>
                </div>
                
                <div class="col-md-4 bg-green-army padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Getting Started
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                Should I start a business? What does it take to be a successful entrepreneur? What do I need to know about business?
                            </p>
                        </article>
                        <div class="margin-t-30">
                            <a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">Find Answers & More</a> 
                        </div>  
                    </div>
                </div>
                <div class="col-md-4 bg-green-army padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Crafting your Business
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                Find all you need to plan your business and get it off the ground.
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Finding your business Idea
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Developing your business idea
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Putting together your business plan
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Raising Cash for your business
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Hitting the Ground Running
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                       Business Registration & Legal Issues 
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 bg-green-army padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Becoming Successful
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                Discover Primary Skills for Business Success
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Record Keeping
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Savings Culture
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Cashflow Management
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Taxes & Statutory 
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Government Fees 
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="margin-t-30">
                            <a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">See all</a> 
                        </div>
                    </div>
                </div>
                
                <div class="col-md-8 cta-block">
                    <h4 class="txt-xlg margin-b-30">Get Help / Business Support Service</h4>
                    <div class="row row-20">
                        <div class="col-md-6 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15">
                                Business Counseling
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Need help with thinking through your business? Our consultants are available to support you
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/learn-a-skill/do-business/">
                                Learn more
                            </a> 
                        </div>
                        <div class="col-md-6 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15">
                                Business Plan Creation
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Lets help you create a great business plan to help you win that funding and/or help you stay on track.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/business-plan-creation/">
                                Get started
                            </a> 
                        </div>
                        <div class="col-md-6 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15">
                                Raise Finance for your Business
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Get a great logo, business card, brand collateral designs, business brochure and website for your new business.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/raise-finance-for-your-business/">
                                Learn more
                            </a> 
                        </div>
                        <div class="col-md-6 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15">
                                Business Registration & Other Legal Services
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Let our professionals help you register your business, handle standard compliance registrations and provide all your legal needs.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/business-registrations/">
                                Learn more
                            </a> 
                        </div>
                        <div class="col-md-6 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15">
                                Business Branding
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Get a great logo, business card, brand collateral designs, business brochure and website for your new business.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/business-branding/">
                                Learn more
                            </a> 
                        </div>
                    </div>
                </div>
                
                <article class="col-md-4 feature-image-block"
                    style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (25).jpg');">
                    <div class="content txt-color-white">
                        <!--<h4 class="margin-b-30">xPlore</h4>-->
                        <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                            Venture ToolPack
                        </h3>
                        <p class="txt-height-1-5 margin-b-30">
                            A suit of tools to structure and showcase your business to investors and potential partners
                        </p>
                        <!--<a
                            class="btn btn-trans-wb"
                            href="http://www.saedconnect.org/xplore-directory/?industry-id=&search-type=xplore-business&xplore-type=business&s="
                        >
                            Explore 
                        </a>-->
                    </div>   
                </article>
            </div>
        </section>
        
        <section class="pre-footer" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (10).jpg');">
            <div class="content">
                <h4 class="title">
                    Join the SAEDConnect Community
                </h4>
                <p class="txt-height-1-4 txt-color-white margin-b-40">
                    Be the first to know about trainings, events, programs, support and opportunities available to help you start and grow your business.
                </p>
                <article class="btn-wrapper margin-b-40">
                    <a class="btn btn-trans-wb-icon" href="https://www.facebook.com/saedconnect">
                        Connect on Facebook
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="btn btn-trans-wb-icon" href="https://www.twitter.com/saedconnect">
                        Follow us on Twitter
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="btn btn-trans-wb-icon" href="https://t.me/esaed">
                        Join the Telegram Channel
                        <i class="fa fa-telegram"></i>
                    </a>
                </article>
                <article>
                    <h4 class="txt-xlg txt-color-white margin-b-20">Join the Support Mailing List</h4>
                    <form action="" class="mailing-list-form">
                        <input class="mail-field" type="email" placeholder="Enter your email address">
                        <input class="submit-btn" type="submit" value="subscribe">
                    </form>
                </article>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>