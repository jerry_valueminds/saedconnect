<?php /*Template Name: User Courses Forms*/ ?>
    
<?php get_header() ?>
  
<?php
    // Get Base URL
    $base_url = get_site_url().'/my-courses-form';

    // Get Currently logged in User
    $current_user = wp_get_current_user();
?> 
  
<?php
    if ( !is_user_logged_in() ) {
        // If User is not Logged in, redirect to Login Page
        $dashboard_link = get_page_link(608); //Get Login Page Link by ID

        // Redirect to Login Page
        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }

    }
?>

<?php
    /* User is Logged in */

    /* Select Page View Request */
    if(isset($_GET['action'])){
        $page_type = $_GET['action']; 
    } else {
        $page_type = '';
    }
?>
   
<main class="main-content font-main">
    <section class="container">
        <div class="col-md-8 mx-auto esaed-form">
            <?php
                /* 
                *
                *   Select Page View
                *
                */
            
                if($page_type !== ''){
                    get_template_part( 'template-parts/user-course/'.$page_type );
                }
            
            ?>
        </div>  
    </section> 
</main>
        
<?php get_footer() ?>