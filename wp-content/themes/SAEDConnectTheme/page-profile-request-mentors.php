<?php /*Template Name: Profile - Requests: Mentors*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content">
                <div class="page-header">
                    <h1 class="page-title">
                        My Mentor Requests
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        in this section you could ask for mentors who would advise you on how to navigate difficult landscapes.
                    </p>
                </article>
                <div class="row row-15">
                <?php
                    /* GF Search Criteria */
                    $gf_id = 27;
                    $parent_post_id = 2;
                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                    foreach( $entries as $entry ){

                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $entry_post_id = $entry['post_id'];

                        $parent_post = get_post($parent_post_id);
                        $entry_post = get_post($entry_post_id);
                ?>
                    <div class="col-md-6 padding-lr-15 padding-b-30">
                        <div 
                            class="venture-request-card"
                            req-title="<?php echo $entry_post->post_title; ?>"
                            <?php
                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                if($meta){
                                    echo 'tool_type="'.$meta.'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_type_order', true);
                                if($meta){
                                    echo 'tool_type_order="'.$meta.'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_description', true);
                                if($meta){
                                    echo 'tool_description="'.$meta.'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_impact', true);
                                if($meta){
                                    echo 'tool_impact="'.$meta.'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                if($meta){
                                    echo 'tool_quantity="'.$meta.'"';
                                }

                                /* Locations */
                                $field = 'tool_location_needed';
                                $output_string;

                                $meta = get_post_meta($entry_post_id, $field, false);

                                if($meta){

                                    foreach($meta as $key=>$value) {
                                        $output_string .= $value;
                                        if($key < count($meta) - 1 ){
                                            $output_string .= ', ';
                                        }
                                    }

                                    echo 'tool_location_needed="'.$output_string.'"';
                                }


                                $meta = get_post_meta($entry_post_id, 'tool_when_needed', true);
                                if($meta){
                                    $date = strtotime($meta);
                                    echo 'tool_when_needed="'.date('j F Y',$date).'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                if($meta){
                                    echo 'tool_need_duration="'.$meta.'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_affordability', true);
                                if($meta){
                                    echo 'tool_affordability="'.$meta.'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_offererd_amount', true);
                                if($meta){
                                    echo 'tool_offererd_amount="'.$meta.'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_returns', true);
                                if($meta){
                                    echo 'tool_returns="'.$meta.'"';
                                }

                                $meta = get_post_meta($entry_post_id, 'tool_request_expiry_date', true);
                                if($meta){
                                    $date = strtotime($meta);
                                    echo 'tool_request_expiry_date="'.date('j F Y',$date).'"';
                                }
                            ?>
                        >
                            <div class="row row-10 margin-b-20">
                                <div class="col-8 padding-lr-10">
                                    <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                        <?php echo $entry_post->post_title; ?>
                                    </h3>
                                    <h3 class="txt-xs txt-color-light">
                                        by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                                    </h3>
                                </div>
                                <div class="col-4 txt-sm text-right padding-lr-10">
                                    <i class="fa fa-clock-o txt-color-red"></i>
                                    <span class="txt-color-lighter padding-l-5">
                                        Posted 9 minutes ago
                                    </span>
                                </div>
                            </div>
                            <div class="txt-sm margin-b-10">
                                <span class="padding-r-10 txt-color-lighter">
                                    Type:
                                </span>
                                <span class="txt-medium">
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                        if($meta){
                                            echo $meta;
                                        }
                                    ?>
                                </span>
                            </div>
                            <div class="txt-sm margin-b-10">
                                <span class="padding-r-10 txt-color-lighter">
                                    Quantity:
                                </span>
                                <span class="txt-medium">
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                        if($meta){
                                            echo $meta;
                                        }
                                    ?>
                                </span>
                            </div>
                            <div class="txt-sm margin-b-10">
                                <span class="padding-r-10 txt-color-lighter">
                                    Duration:
                                </span>
                                <span class="txt-medium">
                                    <?php
                                        $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                        if($meta){
                                            echo $meta; 
                                        }
                                    ?>
                                </span>
                            </div>
                            <div class="row row-10 margin-t-30">
                                <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                                    8 People responded
                                </h3>
                                <div class="col-4 txt-sm text-right padding-lr-10">
                                    <i class="fa fa-map-marker txt-color-red"></i>
                                    <span class="padding-l-5">
                                        Lagos
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>