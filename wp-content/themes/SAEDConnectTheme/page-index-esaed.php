    <?php /*Template Name: Homepage-eSAED*/ ?>
    
    <?php get_header() ?>
    
    <main class="main-content">
        <header class="overview-header container-wrapper">
            <div class="row padding-tb-20">
                <div class="col-md-8">
                    <h1 class="txt-3em txt-bold txt-height-1-2 padding-b-20">eSAED</h1>  
                </div>
            </div>
            <!--<nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <li>
                        <a href="overview_example.html">Overview</a>
                    </li>
                    <li>
                        <a href="minisite_singlepage.html">Single Page</a>
                    </li>
                    <li>
                        <a href="minisite-multipage.html">Multi-Page</a>
                    </li>
                    <li>
                        <a href="minisite_multipage_singlepage_information.html">
                            Multi-Page Single
                        </a>
                    </li>
                    <li>
                        <a href="minisite_multi_item.html">Multi-item</a>
                    </li>
                </ul>
            </nav>-->
        </header>
        
        <!-- General Section -->
        <section>
            <div class="row">

                
                 <!-- Main Content -->
                <div class="col-md-8 left-content padding-t-40">
                    <div class="row row-40">
                        <!-- Text content -->
                        <div class="col-md-12 padding-lr-40">
                            <div class="margin-b-40">
                                <h2 class="title txt-xlg txt-bold txt-height-1-2">
                                    eSAED leverages technology to enable skills Acquisition & Entrepreneurship Development solutions for youths across Nigeria. Get connected to a wide range of skills development, personal growth, business growth and earning opportunities when you subscribe.
                                </h2>
                            </div>
                        </div>

                        <!-- News Cards -->
                        <div class="col-md-6 padding-lr-40 margin-b-30">
                            <a class="featured-article" href="https://skilli.ng/program/entrepreneurship-incubator/?saedconnect=true">
                                <figure class="image-box border-o-1">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/entreprenuership_incubator.jpg" alt="">
                                </figure>
                                <h4 class="title">
                                    The Venture Oven
                                </h4>
                                <p class="txt-normal-s">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus asperiores rerum molestias impedit veritatis cum consequuntur quod repellat explicabot.
                                </p>
                            </a>
                        </div>
                        <div class="col-md-6 padding-lr-40 margin-b-30">
                            <a class="featured-article" href="https://skilli.ng/program/side-hustle-skills/?saedconnect=true">
                                <figure class="image-box">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/side_hustle.jpg" alt="">
                                </figure>
                                <h4 class="title">
                                    The Job Challenge
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-6 padding-lr-40 margin-b-30">
                            <a class="featured-article" href="https://skilli.ng/program/tech-career-mentorship/?saedconnect=true">
                                <figure class="image-box">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/tech_mentorship.jpg" alt="">
                                </figure>
                                <h4 class="title">
                                    Make Money
                                </h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 cta-block bg-yellow-dark">
                    <article class="txt-color-white">
                        <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Join the eSAED Program
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Unlock a whole new world of Opportunities from anywhere you are, when you join eSAED.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="">
				                Join 
							</a>
						</div>
                    </article>
                </div>
            </div>
            
            <!--<div class="row">
                <article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/cv_creator.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							CV Creator 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Build compelling multimedia CVs employers can’t ignore and download as pdf in multiple designs.    
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="https://www.saedconnect.org/esaed/mini-site/cv-creator">
								Learn more
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/business_plan_creator.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Interactive Business Plan Creator
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Create your solid business plan using the interactive Business Plan creator.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="https://www.saedconnect.org/esaed/mini-site/interactive-business-plan-creator/">
								Learn More 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/opportunity_centre.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Opportunity Center 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							The best growth opportunities from across the world, delivered to you in one place. You will find scholarships, funding, small business, access to market, and capacity development opportunities.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="https://www.saedconnect.org/esaed/mini-site/opportunity-center">
				                Learn more
							</a>
						</div>
					</div>
				</article>
                
                <article class="col-md-12 feature-image-block d-flex" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/Banners/02.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Skill Splash
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Build world-class skills from wherever you are – as long as you have a smart phone.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="https://www.saedconnect.org/skill-splash/">
								Learn 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/i_credential.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							iCredentials
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Earn badges that demonstrate your competence in a skill, and get a job guarantee.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="https://www.saedconnect.org/esaed/mini-site/icredentials/">
								Learn more
							</a>
						</div>
					</div>
				</article>

                <article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/talent_source.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							TalentSource
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Get matched to virtual tasks, remote jobs, short and full-time jobs that match your skill set & competence. 
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="https://www.saedconnect.org/esaed/mini-site/talentsource/">
								Learn more
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/influencer_network.png');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Influencer Network
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Make Money fro your blog or social media profile. Get paid for sharing advert campaigns and posts on your social media platforms.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="http://www.saedconnect.org/esaed/mini-site/influencer-network/">
								See How 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/tutor_link.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							TutorLink 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Get connected to private tutor jobs near you and earn extra income for yourself.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="http://www.saedconnect.org/esaed/mini-site/tutorlink/">
								Learn More
							</a>
						</div>
					</div>
				</article>
          
                <article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/business_community.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Business Communities
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Access a wealth of resources across a variety of small businesses, connect with mentors and peers and grow in your craft.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="https://www.saedconnect.org/esaed/mini-site/business-communities/">
								Learn more
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							eSAED Card 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Your access to local & international discount opportunities, seamless payments, and funding options.
						</p>
						<div class="btn-wrapper">
							<h3 class="class d-inline padding-lr-20 padding-tb-10 bg-blue">
				                Coming Soon
							</h3>
						</div>
					</div>
				</article>
                
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/opportunity.jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Opportunity Alert 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Never miss an opportunity to move to your next level. Get the latest opportunities delivered, fresh, to your mobile.
						</p>
						<div class="btn-wrapper">
							<h3 class="class d-inline padding-lr-20 padding-tb-10 bg-blue">
				                Coming Soon
							</h3>
						</div>
					</div>
				</article>
           
                <article class="col-md-8 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/esaed/job.png');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Job Sites Syndication 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Share your cv across several job sites at the click of a button.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" href="https://www.saedconnect.org/esaed/mini-site/job-sites-syndication">
								Learn More
							</a>
						</div>
					</div>
				</article>
            </div>-->
        </section>
    </main>
    
    <?php get_footer() ?>