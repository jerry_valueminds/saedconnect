<?php /*Template Name: Profile - Personal*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>

            <div class="dashboard-multi-main-content">
                <div class="page-header">
                    <h1 class="page-title">
                        My Profile          
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Your Personal Profile is the Key information about you which would be required by potential employers
                    </p>
                </article>
            
            <?php
                /* If Add form */
                if ( $_REQUEST['view'] == 'form' ){
            ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title']; ?>          
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>

                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                </div>

            <?php
                /* Else view template */    
                } else {

            ?>
                <!-- Personal Information -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 84; //Form ID
                    $gv_id = 1116; //Gravity View ID
                    $title = 'Personal Information';

                    $entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria ); 
                ?>

                    <?php if(!$entry_count){ //If no entry ?>

                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>
                            <div class="entry">
                                <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                    Input basic information about yourself like your name, gender, year of birth, etc.
                                </h3>
                                <div class="padding-b-20">
                                    <a 
                                        class="btn btn-ash txt-xxs no-m-b" 
                                        href="<?php printf("https://www.saedconnect.org/competency-profile/personal-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                    >
                                        Add <?php echo $title ?>
                                    </a>
                                </div>
                            </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                                <div class="text-right">
                                    <a 
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&cv-form-title='.$title ?>" 
                                        class="edit-btn"
                                    >
                                        Edit
                                    </a>
                                </div>
                            </div>
                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Name          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Gender          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 2 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Yeah of Birth          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 3 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Marital Status          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 21 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Nationality          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 4 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            State of Origin          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 5 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Location in Nigeria where you currently reside          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 6 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            States in Nigeria where you can work
                                        </p>
                                        <p class="txt-sm">
                                            <?php
                                                $field_id = 7; // Update this number to your field id number
                                                $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                echo $value;
                                            ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Preferred Contact Telephone Number (For Work)          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 8 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Preferred Contact Whatsapp Number (For Work)          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 9 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Preferred Contact Email Address (For Work)         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 10 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Highest Level of Education          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 11 ); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                    <?php } ?>
                </div>

                <!-- Social Details -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 28; //Form ID
                    $gv_id = 1123; //Gravity View ID
                    $title = 'Social Details';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>


                    <?php if(!$entry_count){ //If no entry ?>
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Give us information about your social accounts e.g. Facebook, Twitter and Instagram.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/competency-profile/personal-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&cv-form-title='.$title ?>" 
                                    class="edit-btn"
                                >
                                    Edit
                                </a>
                            </div>
                        </div>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Email          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 11 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Phone          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 12 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Address          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 13 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Current State of Residence
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 14 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Personal Blog/Website          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 15 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Facebook         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 16 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Twitter         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 17 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Instagram
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 18 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Linkedin          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 19 ); ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                    <?php } ?>
                </div>

                <!-- Interests -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 39; //Form ID
                    $gv_id = 1132; //Gravity View ID
                    $title = 'Interests';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $title ?>
                        </h2>
                    </div>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                What interests you? What do you like doing? List and describe what your interests are.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/competency-profile/personal-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Title        
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Description    
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 4 ); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&cv-form-title=Interest' ?>" 
                                   class="edit-btn"
                                >
                                    Edit
                                </a>
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=cv';  ?>" 
                                   class="delete-btn margin-l-5 confirm-delete"
                                >
                                    Delete
                                </a>
                            </div>
                        </div>

                        <?php } ?>

                        <div class="padding-o-20">
                            <a 
                                class="btn btn-ash txt-xxs no-m-b" 
                                href="<?php printf("https://www.saedconnect.org/competency-profile/personal-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                            >
                                Add <?php echo $title ?>
                            </a>
                        </div>

                    <?php } ?>
                </div>

                <!-- Languages -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 40; //Form ID
                    $gv_id = 1134; //Gravity View ID
                    $title = 'Languages';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $title ?>
                        </h2>
                    </div>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Inform us on the language(s) you speak along with your proficiency in the language(s).
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/competency-profile/personal-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Language Name        
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Description    
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 4 ); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=cv&cv-form-title=Language' ?>" 
                                   class="edit-btn"
                                >
                                    Edit
                                </a>
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=cv';  ?>" 
                                   class="delete-btn margin-l-5 confirm-delete"
                                >
                                    Delete
                                </a>
                            </div>
                        </div>

                        <?php } ?>

                        <div class="padding-o-20">
                            <a 
                                class="btn btn-ash txt-xxs no-m-b" 
                                href="<?php printf("https://www.saedconnect.org/competency-profile/personal-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                            >
                                Add <?php echo $title ?>
                            </a>
                        </div>

                    <?php } ?>
                </div>
            
            <?php } ?>
            </div>

            <div class="dashboard-multi-main-sidebar">
                <div class="side-bar-card">
                    <h4 class="txt-medium txt-normal-s txt-color-dark margin-b-20">
                        Print CV
                    </h4>
                    <p class="txt-xs">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ut, commodi, ea nisi voluptatibus error.
                    </p>
                    <div class="margin-t-20">
                        <a href="" class="btn btn-ash txt-xxs no-m-b">
                            <i class="fa fa-print"></i>
                            <span class="padding-l-5">
                                Print
                            </span>
                        </a>
                    </div>
                </div>
                <div class="side-bar-card">
                    <h4 class="txt-medium txt-color-dark margin-b-20">
                        Download CV
                    </h4>
                    <p class="txt-sm">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus ut, commodi, ea nisi voluptatibus error.
                    </p>
                    <div class="margin-t-20">
                        <a href="" class="btn btn-ash txt-xxs no-m-b">
                            <i class="fa fa-download"></i>
                            <span class="padding-l-5">
                                Download
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>