jQuery(document).ready(function( $ ){
/*
*
*=============================================================
* Toggle Hidden Metaboxes
*=============================================================
*
*/
  
	$('#' + $('#select-content-type').val()).show();
  
    $('#select-content-type').change(function(){
    	$('[id^="gi"]').hide();
    	$('#' + $(this).val()).show();
  	});

/*
*
*==============================================================================
* Remove All Metaboxes created by MB Relationships Plugin
*==============================================================================
*
*/
    /* On Parent Page */
    $("[id$='_to']").remove();
    
    /* On Child Page */
    $("[id$='_from']").remove();

/*
*
/*=============================================================================
* SEND AJAX REQUEST: Add New Child Post  
/*=============================================================================
*
*/
    
  $('.ajax-submit').click(function(e){
      e.preventDefault();
      
      var thisMetabox = "#" + $(this).attr('id');
      
      var postName = $(thisMetabox + " .post-name:first").val();
      var postSlug = $(thisMetabox + " .post-slug").val();
      var childPostType = $(thisMetabox + " .child-post-type:first").val();
      var parentId = $(thisMetabox + " .parent-id:first").val();
      var relationshipType = $(thisMetabox + " .relationship-type:first").val();
      
      $('.loading-icon').fadeIn();
      
    
      $.ajax({ 
         data: {action: 'add_post_form', postName:postName, childPostType:childPostType, parentId:parentId, relationshipType:relationshipType, },
         type: 'post',
         url: ajaxurl,
         success: function(data) {
            //alert(data); //should print out the name since you sent it along
            
            $(thisMetabox + " .child-list").append(data);
            $(thisMetabox + " .post-name").val("");
            $(thisMetabox + ' .loading-icon').hide();
        }
      });

  });
    
/*
*
/*=============================================================================
* SEND AJAX REQUEST: Delete Child Post  
/*=============================================================================
*
*/

    $('.delete-child-btn').on('click', function() {
        var postID = $(this).attr('id');
        var clickedButton  = $(this);
        
        $.alertable.confirm('Are you sure you want to delete this post?').then(function(clickedButton) {
            
            $.ajax({ 
                 data: {action: 'delete_form', postID:postID },
                 type: 'post',
                 url: ajaxurl,
                 success: function(data) {
                    $('li#'+postID).slideUp();
                }
            });
            
        }, function() {
            console.log('Confirmation canceled');
        });
        
    });
    
  /*$('.delete-child-btn').click(function(e){
  	e.preventDefault();
    
    var postID = $(this).attr('id');
      
      alert(postID);
      
    //$('.loading-icon').fadeIn();
    
  	$.ajax({ 
         data: {action: 'delete_form', postID:postID },
         type: 'post',
         url: ajaxurl,
         success: function(data) {
            $('li#'+postID).slideUp();
        }
    });

  });*/
    

    
/*
*
/*=============================================================================
* SEND AJAX REQUEST: Sort Child Post 
/*=============================================================================
*
*/
    
    $(".child-list").sortable({
        update : function(event, ui) {
            var parentId = $(this).find(".sort-parent-id").html();
            var relationshipType = $(this).find(".sort-relationship-type:first").html();
                        
            var order = $(this).sortable('toArray');
            //alert(order);
            $.ajax({ 
                 data: {action: 'sort_children', childrenOrder:order, parentId:parentId, relationshipType:relationshipType,},
                 type: 'post',
                 url: ajaxurl,
                 success: function(data) {
                    //alert(data); //should print out the name since you sent it along
                }
            });
        }
    });

    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
});
