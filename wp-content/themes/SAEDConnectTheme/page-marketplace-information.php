<?php /*Template Name: Marketplace Information*/ ?>

<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php
        /* 
        *=============================================
        * Get Content for this view
        *=============================================
        */

        $page_id = get_the_ID();
        $articleTitle = get_the_title();
        $btn_values = rwmb_meta('market-information-btn-group');
        
        $page_title = rwmb_get_value( 'market-information-title' );
        $page_summary = rwmb_get_value( 'market-information-summary' );
        $page_content = rwmb_get_value( 'market-information-content' );
        $images = rwmb_meta( 'market-information-image', array( 'limit' => 1 ) );
        $image = reset( $images );

        /*
        *=============================================
        * First Query to get Topic Name or Mini-site Name
        *=============================================
        */
        // Topic Name Query
        $parentQuery = new WP_Query( array(
            'relationship' => array(
                'id'   => 'page_to_page',
                'to' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );

        /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
        if ( $parentQuery->have_posts() ){
            while ( $parentQuery->have_posts() ) : $parentQuery->the_post();  
            
            $parent_id = get_the_ID();
            $parent_name = get_the_title();
            $parent_url = get_permalink();

            endwhile;
            wp_reset_postdata();
        }
        /*
        *=============================================
        * First Query: END
        *=============================================
        */
    ?>

    <?php endwhile; ?>
    
    <?php
        $childrenQuery = new WP_Query( array(
            'relationship' => array(
                'id'   => 'page_to_page',
                'from' => $parent_id, // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
    ?>
    
    <main class="font-main full-page-section">
        <section class="full-page-section-item">
            <div class="content">
                <div class="wrapper">
                    <h2 class="parent-name">
                        <a href="<?php echo $parent_link ?>">
                            <?php echo $parent_name ?>
                        </a>
                    </h2>
                    <h2 class="txt-2-6em txt-light txt-color-purple margin-b-40">
                         <?php echo $page_title ?>
                    </h2>
                    <h3 class="txt-lg txt-height-1-5 txt-color-light margin-b-20">
                        <?php echo $page_summary ?>
                    </h3>
                    <article class="text-box sm">
                         <?php echo $page_content; ?>
                    </article>
                    <div class="cta margin-t-40">
                        <?php foreach ( $btn_values as $value ) { ?>
                            <a class="btn btn-trans-bw txt-sm margin-r-5" href="<?php echo $value['btn-url']; ?>">
                                <?php echo $value['btn-text']; ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="image" style="background-image: url('<?php echo $image['full_url']?>')">
                
            </div>
        </section>
        <div class="next-label">
        <?php

            /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
            if ( $childrenQuery->have_posts() ){
                while ( $childrenQuery->have_posts() ) : $childrenQuery->the_post();
                
                    if( $page_id == get_the_ID() ){
                        
                        $next = true;
                        
                        if( $next ){
        ?>
            <a class="next row" href="<?php the_permalink() ?>">
                <div class="col-8">
                    <?php the_title(); ?>
                </div>
                <div class="col-4 text-right">
                    <i class="fa fa-long-arrow-right"></i>
                </div>
            </a>
        <?php
                            $next = false;
                        }
                        
                    }
                
                endwhile;
                wp_reset_postdata();
            }
        ?>
            
            <ul class="list">
            <?php

                /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
                if ( $childrenQuery->have_posts() ){
                    while ( $childrenQuery->have_posts() ) : $childrenQuery->the_post();  
            ?>
                    
                <li>
                    <a href="<?php the_permalink() ?>">
                        <?php the_title(); ?>
                    </a>
                </li>
            <?php
                    endwhile;
                    wp_reset_postdata();
                }
            ?>
            </ul>
        </div>
    </main>
