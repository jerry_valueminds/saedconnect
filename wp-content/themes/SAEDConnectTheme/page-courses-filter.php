    <?php /*Template Name: Courses Filtered*/ ?>
    
    <?php get_header() ?>
    
    <main class="main-content">
        <header class="course-directory-banner">
            <div class="content container-wrapper">
                <h1 class="title">
                    Learn from industry experts, connect with a global network of experience.
                </h1>
            </div>
        </header>
        <section class="container-wrapper padding-tb-40 margin-b-40">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 filter-box">
                    <h2 class="txt-sm margin-b-40">Filter Results</h2>
                    <form action="">
                        <div class="filter-group">
                            <button class="title dropdown-toggle" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                LEVEL
                            </button>
                            <div class="collapse show" id="collapseExample">
                                <ul class="filter-list">
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="level" value="beginner" checked>
                                            <label for="">All</label>
                                        </span>
                                        <span class="number-of">(136)</span>
                                    </li>
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="level" value="beginner">
                                            <label for="">Beginner</label>
                                        </span>
                                        <span class="number-of">(136)</span>
                                    </li>
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="level" value="beginner">
                                            <label for="">Intermediate</label>
                                        </span>
                                        <span class="number-of">(136)</span>
                                    </li>
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="level" value="beginner">
                                            <label for="">Advanced</label>
                                        </span>
                                        <span class="number-of">(136)</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="filter-group">
                            <button class="title dropdown-toggle" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                                Model
                            </button>
                            <div class="collapse show" id="collapseExample2">
                                <ul class="filter-list">
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="model" value="beginner" checked>
                                            <label for="">All</label>
                                        </span>
                                        <span class="number-of">(136)</span>
                                    </li>
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="model" value="beginner">
                                            <label for="">Classroom</label>
                                        </span>
                                        <span class="number-of">(136)</span>
                                    </li>
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="model" value="beginner">
                                            <label for="">Fully Online</label>
                                        </span>
                                        <span class="number-of">(136)</span>
                                    </li>
                                    <li>
                                        <span class="input-area">
                                            <input type="radio" name="model" value="beginner">
                                            <label for="">Virtual Class</label>
                                        </span>
                                        <span class="number-of">(136)</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-9 padding-lr-40">
                    <?php
                        // set up a new query for each category, pulling in related posts.
                        $services = new WP_Query(
                            array(
                                'post_type' => 'course',
                                'showposts' => -1,
                            )
                        );
                        while ($services->have_posts()) : $services->the_post();
                    ?>
                    <article class="row row-20 course-directory-card-spread">
                        <div class="col-md-3 padding-lr-20 margin-b-20">
                            <figure class="image-box">
                                <?php if (has_post_thumbnail()) { ?>

                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>" alt="">

                                <?php } else { ?>

                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.jpg" alt="">

                                <?php } ?>
                                
                                <?php if ( has_term( 'virtual-class', 'model' ) ) {?>
                                    <h4 class="model">Virtual Class</h4>
                                <?php } elseif ( has_term( 'blended', 'model' ) ) {?>
                                    <h4 class="model">Blended Class</h4>
                                <?php } elseif ( has_term( 'classroom', 'model' ) ) {?>
                                    <h4 class="model">Classroom</h4>
                                <?php } elseif ( has_term( 'fully-online', 'model' ) ) {?>
                                    <h4 class="model">Fully Online Class</h4>
                                <?php } ?>
                            </figure>
                        </div>
                        <div class="col-md-9 padding-lr-20 margin-b-20">
                            <div class="course-info">
                                <div class="skill-category">
                                    <span class="category">
                                        Category Name
                                    </span>
                                    <span class="sub-category">
                                        Sub-category Name
                                    </span>
                                </div>
                                <h3 class="title"><?php the_title() ?></h3>
                                <h4 class="instructor">
                                    <span class="const">BY:</span>
                                    <span class="name">Maero Uwede</span>
                                </h4>
                                <div class="footer">
                                    <div class="level">
                                        <?php if ( has_term( 'beginner', 'level' ) ) {?>
                                            Advanced
                                        <?php } elseif ( has_term( 'intermediate', 'level' ) ) {?>
                                            Intermediate
                                        <?php } elseif ( has_term( 'advanced', 'level' ) ) {?>
                                            Advanced
                                        <?php } ?>
                                    </div>
                                    <div class="duration">
                                        6 Hours
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <?php endwhile;
                        // Reset things, for good measure
                        $services = null;
                        wp_reset_postdata();
                    ?>
                </div>
            </div>
        </section>
        
    </main>
    
    <?php get_footer() ?>