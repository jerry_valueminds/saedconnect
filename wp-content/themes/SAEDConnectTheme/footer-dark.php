    
    <footer class="main-footer font-main">
        <div class="brand">
            <img class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
            <span class="name">SAEDConnect</span>
        </div>
        <div class="row row-20">
            <div class="col-12 col-md-3 padding-lr-20">
                <p class="address-line">
                    SAEDConnect exists to empower & activate Africa’s youth human Capital. We enable solutions that help people discover their potential, develop & activate their employability & entrepreneurship capacity to create value and derive economic benefits.
                </p>
            </div>
            <div class="col-12 d-none d-md-block col-md-1 padding-lr-20">
            </div>
            <div class="col-12 col-md-5 padding-lr-20">
                <h3 class="title">Follow SAEDConnect</h3>
                <ul class="social-media under-line-list">
                    <li>
                        <a href="https://www.facebook.com/saedconnect">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.twitter.com/saedconnect">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/saedconnect">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UCqYKoq2CQfDC7owMENESxOg">
                            <i class="fa fa-youtube"></i>
                        </a>
                    </li>
                </ul>
                <ul class="list under-line-list">
                    <li>
                        <a href="http://www.saedconnect.org/contribute/synopsis/trainings-overview/">
                            <i class="fa fa-chevron-right txt-sm"></i>
                            <span class="padding-l-10">
                                Offer Trainings & Youth Services
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.saedconnect.org/contribute/synopsis/support-youth-development-overview/">
                            <i class="fa fa-chevron-right txt-sm"></i>
                            <span class="padding-l-10">
                                Support Youth Development
                            </span>
                        </a>
                    </li>
                </ul>
                <ul class="list under-line-list">
                    <li>
                        <a href="https://www.saedconnect.org/help-center/forums/forum/get-help/make-a-suggestion/">
                            <i class="fa fa-chevron-right txt-sm"></i>
                            <span class="padding-l-10">
                                Make a Suggestion
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.saedconnect.org/help-center/forums/forum/get-help/report-an-issue/">
                            <i class="fa fa-chevron-right txt-sm"></i>
                            <span class="padding-l-10">
                                Report an Issue
                            </span>
                        </a>
                    </li>
                </ul>
                <ul class="list">
                    <li>
                        <a href="http://www.saedconnect.org/about/synopsis/terms-conditions/">
                            <i class="fa fa-chevron-right txt-sm"></i>
                            <span class="padding-l-10">
                                Terms & Conditions
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.saedconnect.org/about/synopsis/terms-conditions/">
                            <i class="fa fa-chevron-right txt-sm"></i>
                            <span class="padding-l-10">
                                Privacy Policy
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-3 padding-lr-20 txt-align-center">
                <h3 class="title">In partnership with</h3>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/nysc_saed.png" alt="" width="80">
            </div>
        </div>
    </footer>
    
    <!-- Coming Soon Modal -->
    <div class="modal fade font-main coming-soon-modal" id="comingSoonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row content">
                    <div class="col-md-6 left">
                        <h1 class="txt-xlg txt-bold margin-b-20">
                            This page is almost up.
                        </h1>
                        <p class="txt-normal-s txt-height-1-7 margin-b-20">
                            We are currently putting together all the pieces to make this page available to you. If you want us to notify you when its up, please leave your email below. Thank you.
                        </p>
                        <form action="" class="coming-soon-form">
                            <div class="form-item">
                                <input type="email" placeholder="mail@example.com">
                            </div>
                            <div class="form-submit">
                                <input type="submit" value="Notify me">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 right" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/dance.png');">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <!--Load Scripts-->
	<?php wp_footer(); ?>
    <script type="text/javascript">
        tinymce.init({
            selector: '.editor',
            menubar: false,
            plugins: "lists",
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright | link image | numlist bullist",
            mobile: {
                theme: 'silver',
                plugins: [ 'autosave', 'lists', 'autolink' ],
                height: 600
            }
        });
    </script>
    <script>
        $('.conditional').conditionize();
    </script>
    <script type="text/javascript">
        $('.confirmation').on('click', function () {
            return confirm('Are you sure?');
        });
    </script>
    
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5d8263509f6b7a4457e25de7/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    
    

</body>
</html>