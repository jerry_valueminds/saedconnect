    <?php get_header() ?>

    <main class="main-content">
        <?php
            /* Get program name */
            $program_name = get_the_title();
    
            /* Include Skilli Navigation */
            get_template_part( 'template-parts/navigation-skilli-secondary' );
        ?>

        <section class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h2 class="d-inline-block bg-orange-2 txt-color-white txt-xlg emtxt-bold padding-o-10 margin-b-20">
                        <?php the_title() ?>
                    </h2>
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        <?php echo rwmb_meta( 'skilli-program-name' ); ?>
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        <?php echo rwmb_meta( 'skilli-program-description' ); ?>
                    </h2>
                </div>
            </div>
        </section>

        <section class="bg-orange-2 padding-tb-40">
            <div class="container-wrapper">
                <header class="txt-light margin-b-30 txt-color-white">

                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Tracks
                    </h2>
                    <p class="txt-height-1-">
                        Join any of the tracks below that best aligns with where you are on your entrepreneurial Journey.
                        <br>
                        Each track is dedicated to helping you through a specific phase of your journey. 
                    </p>
                </header>
                <?php // Display posts
                    $program_query = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'program_to_track',
                            'from' => $post->ID, // You can pass object ID or full object
                        ),
                        'nopaging' => true,
                    ) );
                    while ( $program_query->have_posts() ) : $program_query->the_post();
                
                            $program_id = $post->ID;    //Get Program ID
                            //  Get Program Featured Image
                            $program_images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ) );
                            $program_images = reset( $program_images );
                ?>
                           
                
                <div class="overflow-hidden card-shadow-2 margin-b-40">
                   
                    <div class="row">
                        <div class="col-md-8 order-2 order-md-1 bg-white">
                            <div class="padding-o-30">
                                <h2 class="txt-normal-s txt-mediym uppercase margin-b-10">
                                    <?php the_title() ?>
                                </h2>
                                <h2 class="txt-xlg txt-medium txt-height-1-2 padding-b-20">
                                    <?php echo rwmb_meta( 'program-name' ); ?>
                                </h2>
                                <article class="text-box sm">
                                    <?php echo rwmb_meta( 'program-description' ); ?>
                                </article>
                                <!--<div class="margin-t-30">
                                    <a class="btn btn-trans-bw txt-normal-s" href="<?php echo rwmb_meta( 'program-community-forum' ); ?>">
                                        Get started
                                    </a>
                                </div>-->
                                
                                <!-- Action Buttons for Web -->
                                <div 
                                   class="d-none d-md-block row justify-content-between margin-t-20 padding-t-20 border-t-1 border-color-darkgrey"
                                >
                                    <style>
                                        button[aria-expanded="false"]{
                                            background-color: transparent;
                                            border-color: black;
                                            color: black;
                                        }

                                        button[aria-expanded="true"]{
                                            background-color: grey;
                                            border-color: grey;
                                            color: white;
                                        }
                                    </style>
                                   
                                    <div class="col-md-6">
                                    <?php
                                        /* Get Group Values */
                                        $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                        if ( ! empty( $group_values ) ) {
                                    ?>
                                        <button
                                           class="btn btn-trans-bw txt-sm margin-r-10"
                                           type="button" data-toggle="collapse" 
                                           data-target="#program-<?php echo $program_id ?>" 
                                           aria-expanded="false" 
                                           aria-controls="collapseExample"
                                        >
                                            Get Started
                                        </button>
                                    <?php
                                        }
                                    ?>
                                        <button 
                                            class="btn btn-trans-bw txt-sm" 
                                            type="button" 
                                            data-toggle="collapse" 
                                            data-target="#program-<?php echo $program_id ?>-2" 
                                            aria-expanded="false" 
                                            aria-controls="collapseExample"
                                        >
                                            Request Mentor
                                        </button>
                                    </div>
                                </div>
                                
                                <!-- Action Buttons Mobile -->
                                <div 
                                   class="d-md-none row justify-content-between margin-t-20 padding-t-20 border-t-1 border-color-darkgrey"
                                >
                                    <style>
                                        button[aria-expanded="false"]{
                                            background-color: transparent;
                                            border-color: black;
                                            color: black;
                                        }

                                        button[aria-expanded="true"]{
                                            background-color: grey;
                                            border-color: grey;
                                            color: white;
                                        }
                                    </style>
                                   
                                    <div class="col-md-6">
                                    <?php
                                        /* Get Group Values */
                                        $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                        if ( ! empty( $group_values ) ) {
                                    ?>
                                        <a class="btn btn-trans-bw txt-sm margin-r-10" data-toggle="modal" href="#curriculumModal-<?php echo $program_id ?>">
                                            Get Started
                                        </a>
                                    <?php
                                        }
                                    ?>
                                        <a class="btn btn-trans-bw txt-sm" data-toggle="modal" href="#mentorModal-<?php echo $program_id ?>">
                                            Request Mentor
                                        </a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        

                        <div class="col-md-4 order-1 order-md-2 generic-card-bg" 
                           style="background-image:url('<?php echo $program_images['full_url']; ?>');"
                        >
                            <div class="cta text-center">
                                <!--<div>
                                    <button type="button" class="btn btn-blue" data-toggle="modal" data-target="#formModal">
                                        Enroll for free online
                                    </button>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-white" data-toggle="modal" data-target="#smsEnrollModal-<?php echo $program_id ?>">
                                        Enroll for free via SMS
                                    </button>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    
                    <div class="faq-collapse border-t-1 border-color-darkgrey" id="accordion-<?php echo $program_id ?>">
                        <div
                           class="faq-collapse collapse bg-white" 
                           id="program-<?php echo $program_id ?>" 
                           data-parent="#accordion-<?php echo $program_id ?>"
                        >
                            <div class="padding-lr-80">
                            <?php
                                /* Get Group Values */
                                $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                if ( ! empty( $group_values ) ) {
                            ?>
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Curriculum
                                </h2>
                                    
                            <?php
                                    foreach ( $group_values as $group_value ) {
                            ?>
                               
                                    <!-- Article Group -->
                                    <div class="padding-tb-20 border-t-1 border-color-darkgrey">
                                       
                                    <?php if( $group_value['group-title-url'] ){ ?>
                                        
                                        <h3>
                                            <a
                                               href="<?php echo $group_value['group-title-url']; ?>"
                                               class="txt-color-dark d-flex align-items-center"
                                            >
                                                <span class="txt-xs padding-r-5">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                </span>
                                                <?php echo $group_value['group-title']; ?>
                                            </a>
                                        </h3>
                                        
                                    <?php } else { ?>
                                       
                                        <h3>
                                            <a
                                               href="<?php echo $group_value['group-title-url']; ?>"
                                               class="txt-color-dark d-flex align-items-center"
                                            >
                                                <span class="txt-xs padding-r-5">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                </span>
                                                <?php echo $group_value['group-title']; ?>
                                            </a>
                                        </h3>

                                    <?php } ?>
                                        
                                    <?php 
                                        $values = $group_value['forum-topics-group']['forum-topic-item'];
                                            
                                            if($values){
                                    ?>
                                        <ul class="row row-10 icon-list black txt-normal-s margin-t-20 padding-l-20">
                                        <?php 
                                            foreach ( $values as $value ) { 
                                        ?>
                                           
                                            <li class="col-md-3 padding-lr-10">
                                                <a href="<?php echo $value['forum-topic-url']; ?>">
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        <?php echo $value['forum-topic-title']; ?>
                                                    </span>
                                                </a>
                                            </li>
                                        
                                        <?php } ?>
                                        
                                        </ul>
                                        
                                    <?php } ?>
                                        
                                    </div>

                                <?php
                                    }
                                }
                            ?>
                            </div>
                        </div>

                        <div 
                           class="faq-collapse collapse bg-white" 
                           id="program-<?php echo $program_id ?>-2" 
                           data-parent="#accordion-<?php echo $program_id ?>"
                        >
                            <div class="padding-lr-80">
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Subscribe to the Mentor forum
                                </h2>
                                <p class="margin-b-40">
                                    Lorem animi maxime saepe impedit, natus. Vitae magnam nisi nostrum quaerat non tenetur quis!
                                </p>
                            
                                <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                <?php
                                    $group_values = rwmb_meta( 'subscription-block-group' );


                                    if ( ! empty( $group_values ) ) {
                                        
                                        foreach ( $group_values as $group_value ) {

                                        /* Get Content type */
                                        $content_type = $group_value['select-subscription-type'];
                                        
                                            /* Check Subscription Type */
                                            if($content_type == 'paid-subscription-meta-box'){
                                ?>
                                    <!-- Paid Subscription -->
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            <?php
                                                echo $group_value['subscription-title'];
                                            ?>
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            <?php
                                                echo $group_value['paid-subscription-meta-box']['paid-subscription-description'];
                                            ?>
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            <?php
                                                echo $group_value['paid-subscription-meta-box']['paid-subscription-fee'];
                                            ?>
                                        </div>
                                        <div>
                                            <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                                                <?php
                                                    echo $group_value['subscription-button-text'];
                                                ?>
                                            </a>
                                        </div>
                                    </div>
                                    
                                <?php } elseif ($content_type == 'image-subscription-meta-box') { ?>
                                 
                                <?php
                                    /* Get Image */
                                    //$images = rwmb_meta( 'mini_site_feature_image', array( 'limit' => 1 ) );
                                    $images = $group_value['image-subscription-meta-box']['image-subscription-image'];
                                    $image = $images[0];
                                ?>
                                  
                                    <!-- Image -->
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            <?php
                                                echo $group_value['subscription-title'];
                                            ?>
                                        </h3>
                                        <div class="margin-b-20">
                                            <img src="<?php echo  wp_get_attachment_image_url( $image, 'size' ); ?>" alt="">
                                        </div>
                                        <p class="txt-sm margin-b-20">
                                            <?php
                                                echo $group_value['paid-subscription-meta-box']['paid-subscription-description'];
                                            ?>
                                        </p>
                                        <div>
                                            <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                                                <?php
                                                    echo $group_value['subscription-button-text'];
                                                ?>
                                            </a>
                                        </div>
                                    </div>
                                   
                                <?php } elseif ($content_type == 'text-subscription-meta-box') { ?> 
                                   
                                    <!-- Text -->
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            <?php
                                                echo $group_value['subscription-title'];
                                            ?>
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            <?php
                                                echo $group_value['text-subscription-meta-box']['text-subscription-content'];
                                            ?>
                                        </p>
                                        <div>
                                            <a href="<?php echo $group_value['subscription-button-link']; ?>" class="btn btn-blue txt-xs no-m-b">
                                                <?php
                                                    echo $group_value['subscription-button-text'];
                                                ?>
                                            </a>
                                        </div>
                                    </div>
                                    
                                <?php
                                            }
                                        }
                                    }
                                ?>   
                                </div>

                                <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                    <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
                                        Are you a Corp Member?
                                    </h2>
                                    <p class="col-md-7 padding-lr-20 txt-sm">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                    </p>
                                    <div class="col-md-2 padding-lr-20">
                                        <a href="" class="btn btn-green txt-xs full-width no-m-b">
                                            Learn more
                                        </a>
                                    </div>
                                </div>
                                <div class="row row-20 padding-b-40">
                                    <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
                                        Already Subscribed?
                                    </h2>
                                    <p class="col-md-7 padding-lr-20 txt-sm">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                    </p>
                                    <div class="col-md-2 padding-lr-20">
                                        <a href="" class="btn btn-green txt-xs full-width no-m-b">
                                            Sign up
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- Curriculum Modal -->
                <div class="modal fade font-main shc-modal" id="curriculumModal-<?php echo $program_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="container-wrapper">
                                <div class="col-md-10 mx-auto">
                                <?php
                                    /* Get Group Values */
                                    $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                    if ( ! empty( $group_values ) ) {
                                ?>
                                    <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                        Curriculum
                                    </h2>

                                <?php
                                        foreach ( $group_values as $group_value ) {
                                ?>

                                        <!-- Article Group -->
                                        <div class="padding-tb-20 border-t-1 border-color-darkgrey">

                                        <?php if( $group_value['group-title-url'] ){ ?>

                                            <h3>
                                                <a
                                                   href="<?php echo $group_value['group-title-url']; ?>"
                                                   class="txt-color-dark d-flex align-items-center"
                                                >
                                                    <span class="txt-xs padding-r-5">
                                                        <i class="fa fa-long-arrow-right"></i>
                                                    </span>
                                                    <?php echo $group_value['group-title']; ?>
                                                </a>
                                            </h3>

                                        <?php } else { ?>

                                            <h3>
                                                <a
                                                   href="<?php echo $group_value['group-title-url']; ?>"
                                                   class="txt-color-dark d-flex align-items-center"
                                                >
                                                    <span class="txt-xs padding-r-5">
                                                        <i class="fa fa-long-arrow-right"></i>
                                                    </span>
                                                    <?php echo $group_value['group-title']; ?>
                                                </a>
                                            </h3>

                                        <?php } ?>

                                        <?php 
                                            $values = $group_value['forum-topics-group']['forum-topic-item'];

                                                if($values){
                                        ?>
                                            <ul class="row row-10 icon-list black txt-normal-s margin-t-20 padding-l-20">
                                            <?php 
                                                foreach ( $values as $value ) { 
                                            ?>

                                                <li class="col-md-3 padding-lr-10">
                                                    <a href="<?php echo $value['forum-topic-url']; ?>">
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            <?php echo $value['forum-topic-title']; ?>
                                                        </span>
                                                    </a>
                                                </li>

                                            <?php } ?>

                                            </ul>

                                        <?php } ?>

                                        </div>

                                    <?php
                                        }
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Mentor Modal -->
                <div class="modal fade font-main shc-modal" id="mentorModal-<?php echo $program_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="padding-lr-40 padding-t-40">
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Subscribe to the Mentor forum
                                </h2>
                                <p class="margin-b-40">
                                    Lorem animi maxime saepe impedit, natus. Vitae magnam nisi nostrum quaerat non tenetur quis!
                                </p>
                                
                                <div class="row row-20 padding-b-40">
                                    <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
                                        Already Subscribed?
                                    </h2>
                                    <p class="col-md-7 padding-lr-20 txt-sm padding-b-20">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                    </p>
                                    <div class="col-md-2 padding-lr-20">
                                        <a href="" class="btn btn-green txt-xs full-width no-m-b">
                                            Sign up
                                        </a>
                                    </div>
                                </div>
                            
                                <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            1 Month
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            N1000.00
                                        </div>
                                        <div>
                                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                                Subscribe
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            3 Months
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            N1000.00
                                        </div>
                                        <div>
                                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                                Subscribe
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            6 Months
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            N1000.00
                                        </div>
                                        <div>
                                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                                Subscribe
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 padding-lr-20 padding-b-20">
                                        <h3 class="txt-medium margin-b-10">
                                            1 Year
                                        </h3>
                                        <p class="txt-sm margin-b-20">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                        </p>
                                        <div class="bg-grey txt-medium d-inline-block margin-b-20 padding-o-15">
                                            N1000.00
                                        </div>
                                        <div>
                                            <a href="" class="btn btn-blue txt-xs no-m-b">
                                                Subscribe
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-20 padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                    <h2 class="col-md-3 padding-lr-20 txt-medium margin-b-20">
                                        Are you a Corp Member?
                                    </h2>
                                    <p class="col-md-7 padding-lr-20 txt-sm padding-b-20">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, iusto dolores eum dignissimos odit amet
                                    </p>
                                    <div class="col-md-2 padding-lr-20">
                                        <a href="" class="btn btn-green txt-xs full-width no-m-b">
                                            Learn more
                                        </a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </section>

        <section class="padding-t-80 padding-b-40">
            <div class="container-wrapper">
                <header class="padding-b-80 text-center">
                    <h1 class="txt-2-2em txt-light txt-height-1-1 margin-b-20">
                        What you get
                    </h1>
                    <h2 class="txt-height-1-5">
                        We are going all out to ensure you get all the support &amp; resources you require to start and excel in your tech career.
                    </h2>
                </header>
                <div class="row row-40">
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_contributor.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold txt-height-1-4">
                                    Expert Instruction &amp; Guidance
                                </p>
                                <div class="collapse" id="collapse-1">
                                    <div class="padding-tb-20">
                                        <ul class="icon-list txt-sm">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Learn with industry-vetted curriculum created by Subject-Matter Experts.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access to experienced Mentors available 12-14 hours/day during the week to help you in real time.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access useful Templates, Reference Guides &amp; tailored advice from experts.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p>
                                    <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_mentor.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10 txt-height-1-4">
                                <p class="txt-bold txt-height-1-4">
                                    Immersive Learning
                                </p>
                                <div class="collapse" id="collapse-2">
                                    <div class="padding-tb-20">
                                        <ul class="icon-list txt-sm">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Instructor &amp; Mentor Support.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access real-live entrepreneur interviews that help you appreciate the practical application of lessons shared.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Collaborative tasks.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p>
                                    <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_partner.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold txt-height-1-4">
                                    Get Real-World Experience
                                </p>
                                <div class="collapse" id="collapse-3">
                                    <div class="padding-tb-20">
                                        <ul class="icon-list txt-sm">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    You will work on challenging projects to solve real problems during the sessions, 
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    You are challenged to define and develop a personal project to apply the skills being learnt.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_trainer.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold txt-height-1-4">
                                    Amazing Support System.
                                </p>
                                <div class="collapse" id="collapse-4">
                                    <div class="padding-tb-20">
                                        <ul class="icon-list txt-sm">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Advisor Support beyond the end of the session
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Peer-to-Peer Motivation in the alumni community
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Tools, resources and programs that would make your tech career journey easier
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_partner.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold txt-height-1-4">
                                    Career Support
                                </p>
                                <div class="collapse" id="collapse-5">
                                    <div class="padding-tb-20">
                                        <ul class="icon-list txt-sm">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Get support to prepare you for the technical recruiting process
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Resume and portfolio critique and a review of your LinkedIn and GitHub profiles
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Mock Interviews
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Troubleshooting and work on weak areas
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_trainer.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold txt-height-1-4">
                                    Access to Growth Opportunities
                                </p>
                                <div class="collapse" id="collapse-6">
                                    <div class="padding-tb-20">
                                        <ul class="icon-list txt-sm">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Events and network meetings where you can meet other great tech gurus like you
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Discounts on online services like hosting, plugins, etc
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Be the first t know about internship and job openings for tech talent shared by partners and alumni
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--<section class="padding-t-80 bg-grey">
            <div class="container-wrapper">
                <header class="margin-b-100 text-center">
                    <h2 class="txt-2-2em txt-light margin-b-20">
                        Meet your Mentors
                    </h2>
                    <p class="txt-height-1-7">
                        The Tech Mentorship Scheme is led by actively-practicing,
                        <br>
                        world class programmers who actually make a living from coding.
                    </p>
                </header>
                <div class="row row-10 text-center">
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/onewoman.png" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Hussein Alayo
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/m_uwede.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Oguntade Samson
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/onewoman.png" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Tunde Omiwole
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Jerry Adaji
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Tayo Erubu
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Odugbemi
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        
        <section class="padding-tb-80 bg-grey">
            <div class="container-wrapper">
                <div class="row text-center">
                    <div class="col-md-10 mx-auto">
                        <h1 class="txt-xlg txt-height-1-1 margin-b-40">
                            Expert Mentors Ready to Help you
                        </h1> 

                        <div class="row row-30">
                        <?php
                             // Create Query Argument
                            $args = array(
                                'post_type' => 'mentor',
                                'showposts' => 6,
                                'meta_query' => $meta_array,
                            );


                            $mentor_query = new WP_Query($args);

                            while ($mentor_query->have_posts()) : $mentor_query->the_post();

                            /* Get Post ID */
                            $post_id = $post->ID;

                            /* Get Mentor mentorship areas */
                            $mentor_field = 'mentor-mentorship-areas';

                            $mentor_areas = get_post_meta($post->ID, $mentor_field, false);

                            /* Check if Menotor mentors on this Track's Program */
                            if ( in_array($program_name, $mentor_areas) ){

                        ?>

                            <!-- Mentor -->
                            <div class="col-md-2 padding-lr-30 padding-b-40 d-flex">
                                <div class="mentor-card">
                                    <div class="image-box">
                                        <?php
                                            $field = 'mentor-profile-image';

                                            $meta = get_post_meta($post->ID, $field, true);

                                        ?>
                                        <img src="<?php if($meta){  echo $meta; } ?>" alt="">
                                    </div>
                                    <div class="name">
                                        <a data-toggle="modal" href="#mentor-<?php echo $post_id ?>">
                                            <?php echo the_title(); ?>
                                        </a>
                                    </div>
                                    <div class="position">
                                        <?php
                                            $field_position = 'mentor-position';
                                            $field_company = 'mentor-company';

                                            $meta_position = get_post_meta($post->ID, $field_position, true);
                                            $meta_company = get_post_meta($post->ID, $field_company, true);

                                            echo $meta_company;
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <!-- Mentor Modal -->
                            <div class="modal fade font-main coming-soon-modal" id="mentor-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content padding-o-40">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <div class="text-center">
                                            <div class="margin-b-10">
                                                <?php
                                                    $field = 'mentor-profile-image';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <img src="<?php if($meta){  echo $meta; } ?>" width="120" class="profile-image">
                                            </div>
                                            <div class="txt-normal-s margin-b-10">
                                                Mentor
                                            </div>
                                            <h4 class="txt-xlg txt-medium margin-b-10">
                                                <?php echo the_title(); ?>
                                            </h4>
                                            <h5 class="txt-medium uppercase txt-sm margin-b-20">
                                                <?php
                                                    $field_position = 'mentor-position';
                                                    $field_company = 'mentor-company';

                                                    $meta_position = get_post_meta($post->ID, $field_position, true);
                                                    $meta_company = get_post_meta($post->ID, $field_company, true);

                                                    echo $meta_position.' @ '.$meta_company;
                                                ?>
                                            </h5>
                                            <p class="txt-sm txt-height-1-7 margin-b-20">
                                                <?php
                                                    $field = 'mentor-short-profile';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                    if($meta){
                                                        echo $meta;
                                                    }
                                                ?>
                                            </p>
                                            <div class="txt-xlg">
                                                <?php
                                                    $field = 'mentor-facebook';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-facebook fa-fw"></i>
                                                </a>

                                                <?php
                                                    $field = 'mentor-twitter';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a 
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-twitter fa-fw"></i>
                                                </a>

                                                <?php
                                                    $field = 'mentor-linkedin';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a 
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-linkedin fa-fw"></i>
                                                </a>
                                            </div>
                                            <div class="padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Mentors on
                                                </div>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field = 'mentor-mentorship-areas';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="border-t-1 border-color-darkgrey padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Small Business Mentorship area
                                                </div>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field = 'mentor-small-business-mentorship-interest';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                            }   // Check if Menotor mentors on this Track's Program: End

                            endwhile;
                        ?>  
                        </div>

                        <div>
                            <h2 class="txt-normal-s txt-height-1-7">
                                Have questions? Check out the
                                    Help Center
                                .
                            </h2>
                            <div class="margin-t-20">
                                <a href="https://www.saedconnect.org/growth-programs/mentor-directory/" class="btn btn-green txt-sm no-m-b">
                                    See Full List
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="container-wrapper padding-tb-60">
            <header class="margin-b-60 text-center">
                <h2 class="txt-xxlg txt-light padding-b-20">Frequently Asked Questions</h2>
                <p>
                    Got questions? Require clarifications? Find your answers here
                </p> 
            </header>
            <div class="row row-10">
            <?php 
                $values = rwmb_meta( 'skilli-program-faq' );
                $accordionCounter = 1;

                foreach ( $values as $value ) { 
            ?>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-<?php echo $accordionCounter ?>" aria-expanded="false" aria-controls="collapseExample">
                            <?php echo $value['accordion-title']; ?>
                        </button>
                        <div class="collapse" id="faq-<?php echo $accordionCounter ?>">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        <?php echo $value['accordion-content']; ?>
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                    $accordionCounter++;
                }
            ?> 
            </div>
            <p class="txt-xlg txt-medium text-center margin-t-40 margin-b-20">
                Didn't find your answer?
            </p>
            <div class="text-center">
                <a href="https://www.saedconnect.org/help-center/forums/forum/the-entrepreneurship-incubator-help-center/" class="btn btn-green txt-normal-s">
                    The Entreneurship Incubator Help Center
                </a>
            </div>
        </section>
        
        <?php
            /* Include Skilli Footer */
            get_template_part( 'template-parts/footer-skilli' );
        ?>
    </main>


    <?php get_footer() ?>