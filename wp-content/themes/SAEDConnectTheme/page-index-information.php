    <?php /*Template Name: Homepage - Information*/ ?>
    
    <?php get_header() ?>
    
    <main class="home-content">       
        
        <section class="container-wrapper header-section padding-tb-80">
            <h1 class="txt-3em txt-bold txt-height-1-1 margin-b-20">
                What help do you need now? 
            </h1>
            <p class="txt-lg">Select any of the options below, to see how we can help you.</p>
        </section>
        
        <div class="accordion-wrapper">
            <div class="faq-accordion container-wrapper padding-tb-30">
                <a class="d-flex row txt-xlg txt-color-dark" href="https://www.saedconnect.org/about/synopsis/find-your-path/">
                    <h4 class="col-4 btn-link col-8 txt-bold">
                         I don’t know what to do with myself
                    </h4>
                    <div class="col-4 text-right">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="faq-accordion bg-ghostwhite container-wrapper padding-tb-30">
                <a class="d-flex row txt-xlg txt-color-dark" href="https://www.saedconnect.org/information/start-your-biz/">
                    <h4 class="col-4 btn-link col-8 txt-bold">
                        I need help to start a business
                    </h4>
                    <div class="col-4 text-right">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            
            <div class="faq-accordion bg-darkgrey container-wrapper padding-tb-30">
                <a class="d-flex row txt-xlg txt-color-dark" href="https://www.saedconnect.org/information/grow-your-biz/">
                    <h4 class="col-4 btn-link col-8 txt-bold">
                        I need help with my existing business
                    </h4>
                    <div class="col-4 text-right">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            
            <div class="faq-accordion bg-yellow container-wrapper padding-tb-30">
                <a class="d-flex row txt-xlg txt-color-dark" href="https://www.saedconnect.org/information/get-a-job/">
                    <h4 class="col-4 btn-link col-8 txt-bold">
                        I need a Job
                    </h4>
                    <div class="col-4 text-right">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="faq-accordion bg-yellow-dark container-wrapper padding-tb-30">
                <a class="d-flex row txt-xlg txt-color-dark" href="https://www.saedconnect.org/information/make-money/">
                    <h4 class="col-4 btn-link col-8 txt-bold">
                        I need to make some extra income
                    </h4>
                    <div class="col-4 text-right">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="faq-accordion bg-yellow-darker container-wrapper padding-tb-30">
                <a class="d-flex row txt-xlg txt-color-dark" href="https://www.saedconnect.org/information/learn-a-skill/">
                    <h4 class="col-4 btn-link col-8 txt-bold">
                        I want to learn a skill
                    </h4>
                    <div class="col-4 text-right">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                </a>
            </div>
        </div>
    </main>
    
    <?php get_footer() ?>