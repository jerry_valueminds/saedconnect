    
<footer class="d-none d-md-block main-footer font-main">
    <div class="brand">
        <img class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
        <span class="name">SAEDConnect</span>
    </div>
    <div class="row row-20">
        <div class="col-12 col-md-3 padding-lr-20">
            <p class="address-line">
                SAEDConnect exists to empower & activate Africa’s youth human Capital. We enable solutions that help people discover their potential, develop & activate their employability & entrepreneurship capacity to create value and derive economic benefits.
            </p>
        </div>
        <div class="col-12 d-none d-md-block col-md-1 padding-lr-20">
        </div>
        <div class="col-12 col-md-5 padding-lr-20">
            <h3 class="title">Follow SAEDConnect</h3>
            <ul class="social-media under-line-list">
                <li>
                    <a href="https://www.facebook.com/saedconnect">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.twitter.com/saedconnect">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/saedconnect">
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.youtube.com/channel/UCqYKoq2CQfDC7owMENESxOg">
                        <i class="fa fa-youtube"></i>
                    </a>
                </li>
            </ul>
            <ul class="list under-line-list">
                <li>
                    <a href="http://www.saedconnect.org/contribute/synopsis/trainings-overview/">
                        <i class="fa fa-chevron-right txt-sm"></i>
                        <span class="padding-l-10">
                            Offer Trainings & Youth Services
                        </span>
                    </a>
                </li>
                <li>
                    <a href="http://www.saedconnect.org/contribute/synopsis/support-youth-development-overview/">
                        <i class="fa fa-chevron-right txt-sm"></i>
                        <span class="padding-l-10">
                            Support Youth Development
                        </span>
                    </a>
                </li>
            </ul>
            <ul class="list under-line-list">
                <li>
                    <a href="http://www.saedconnect.org/about/discourse/make-a-suggestion/">
                        <i class="fa fa-chevron-right txt-sm"></i>
                        <span class="padding-l-10">
                            Make a Suggestion
                        </span>
                    </a>
                </li>
                <li>
                    <a href="http://www.saedconnect.org/about/discourse/report-an-issue/">
                        <i class="fa fa-chevron-right txt-sm"></i>
                        <span class="padding-l-10">
                            Report an Issue
                        </span>
                    </a>
                </li>
            </ul>
            <ul class="list">
                <li>
                    <a href="http://www.saedconnect.org/about/synopsis/terms-conditions/">
                        <i class="fa fa-chevron-right txt-sm"></i>
                        <span class="padding-l-10">
                            Terms & Conditions
                        </span>
                    </a>
                </li>
                <li>
                    <a href="http://www.saedconnect.org/about/synopsis/terms-conditions/">
                        <i class="fa fa-chevron-right txt-sm"></i>
                        <span class="padding-l-10">
                            Privacy Policy
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-12 col-md-3 padding-lr-20 txt-align-center">
            <h3 class="title">In partnership with</h3>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/nysc_saed.png" alt="" width="80">
        </div>
    </div>
</footer>

<!--Load Scripts-->
<?php wp_footer(); ?>
