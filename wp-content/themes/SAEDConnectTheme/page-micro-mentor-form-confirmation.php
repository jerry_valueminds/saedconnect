<?php /*Template Name: Micro Mentor Form Confirmation*/ ?>
   

<?php get_header() ?>

<?php
    // TO SHOW THE PAGE CONTENTS
    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
?>
    
    <main class="main-content">
       
        <section class="container-wrapper padding-t-80 padding-b-40 text-center text-center">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        Thank you for your Submission.
                    </h1>
                    <h2 class="txt-lg txt-height-1-5 txt-light">
                        <p>
                            We will review your submission and come back to you shortly. If you feel you can be a mentor for a different small business, please complete this form again for that business.
                        </p>
                    </h2>
                    <div class="margin-t-40">
                        <a href="<?php echo get_home_url().'/micro-mentor-form' ?>" class="btn btn-trans-bw">
                            Back to Form
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
<?php
    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
?>

<?php get_footer() ?>