    <?php /*Template Name: Homepage-2*/ ?>
    
    <?php get_header() ?>
    
    <main class="main-content">
        <section class="container-wrapper bg-ash padding-tb-15">
            <div class="row">
                <div class="col-md-6">
                    <p class="txt-normal-s txt-color-white">
                        SAEDConnect is a <span class="txt-medium txt-color-yellow">Youth Empowerment Accelerator</span> dedicated to providing the resources, tools, opportunities and networks that young people need to make progress.
                    </p>
                </div>
            </div>
        </section>
        <section class="container-wrapper padding-t-10 padding-b-20">
            <div class="row">
                <section class="col-md-9 slippry-section">     
                    <ul id="thumbnails">
                    <?php
                        //WP Query
                        $args = array( 'post_type' => 'homepage-slide' );

                        $homeSlide_query = new WP_Query();
                        $homeSlide_query->query($args);

                        $counter = 1;

                        while ($homeSlide_query->have_posts()) : $homeSlide_query->the_post();

                            $images = rwmb_meta( 'slide-image', array( 'limit' => 1 ) );
                            $image = reset( $images ); 
                            $image_url = $image['full_url']; 
                    ?>
                        
                        <li>
                            <a
                                href="#slide<?php echo $counter ?>"
                                style="background-image: url('<?php echo $image_url; ?>')"
                            >
                                
                            </a>
                        </li>
                             
                    <?php 
                        $counter++; 
                        endwhile; 
                    ?>
                    </ul>
                    <div class="thumb-box">
                        <ul class="thumbs">
                        <?php 
                            function truncate($string, $length){
                                if (strlen($string) > $length) {
                                    $string = substr($string, 0, $length) . '...';
                                }

                                return $string;
                            }
                            
                            $counter = 1; 
                            while ($homeSlide_query->have_posts()) : $homeSlide_query->the_post(); 
                        ?>
                            
                            <li style="background-color: ghostwhite;">
                                <a href="#<?php echo $counter ?>" data-slide="<?php echo $counter ?>">
                                    <div class="content">
                                        <?php if( rwmb_get_value( 'slide-title' ) ){ ?>
                                        <h2 class="txt-lg txt-medium margin-b-10">
                                            <?php echo rwmb_get_value( 'slide-title' ); ?>
                                        </h2>
                                        <?php } ?>
                                        
                                        <?php if( rwmb_get_value( 'slide-subtitle' ) ){ ?>
                                        <h3 class="txt-normal-s txt-height-1-4 txt-medium margin-b-10">
                                            <?php echo rwmb_get_value( 'slide-subtitle' ); ?>
                                        </h3>
                                        <?php } ?>
                                        
                                        <?php if( rwmb_get_value( 'slide-caption' ) ){ ?>
                                        <p class="txt-xs">
                                            <?php 
                                                echo truncate(rwmb_get_value( 'slide-caption' ), 70);
                                            ?>
                                        </p>
                                        <?php } ?>
                                    </div>
                                </a>
                            </li>

                        <?php 
                            $counter++;
                            endwhile; 
                        ?>
                        </ul>
                    </div>
                </section>
                <div class="col-md-3 slide-sid-bar d-flex">
                    <div class="flex_1 d-flex flex-column">
                        <div class="flex_1 d-flex padding-b-20">
                            <div class="flex_1 padding-o-20 bg-grey">
                                <p class="txt-sm txt-medium  margin-b-10">
                                    Young Leaders Network
                                </p>
                                <p class="txt-sm margin-b-10 ">
                                    Join other young leaders to activate resources, networks & opportunities that will accelerate your progress
                                </p>
                                <a href="" class="btn btn-ash txt-medium txt-xxs no-m-b">
                                    Get Started
                                </a>
                            </div>
                        </div>                        
                        <div class="flex_1 d-flex padding-b-20">
                            <div class="flex_1 padding-o-20 bg-grey">
                                <p class="txt-sm txt-medium margin-b-20">
                                    Find help to accelerate your business or personal project
                                </p>
                                <div class="dropdown show">
                                    <a class="btn-pop-yellow txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        What do you need?
                                    </a>
                                    <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Volunteers?</a>
                                        <a class="dropdown-item" href="#">Funding?</a>
                                        <a class="dropdown-item" href="#">Partners?</a>
                                        <a class="dropdown-item" href="#">Tools?</a>
                                        <a class="dropdown-item" href="#">Workspace/office?</a>
                                        <a class="dropdown-item" href="#">Space?</a>
                                        <a class="dropdown-item" href="#">Land?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-md-12 d-flex padding-lr-5 padding-b-20">
                            <div class="flex_1 padding-o-20 bg-grey">
                                <p class="txt-sm txt-medium margin-b-20">
                                    For Corp Members!
                                </p>
                                <p class="txt-sm margin-b-15">
                                    Help other young people start your kind of business.
                                </p>
                                <a href="" class="btn btn-ash txt-medium txt-xxs no-m-b">
                                    Learn more
                                </a>
                            </div>
                        </div>-->
                        <div class="flex_1 d-flex">
                            <div class="flex_1 padding-o-20 bg-grey">
                                <p class="txt-sm txt-medium  margin-b-10">
                                    Make some extra Income for yourself
                                </p>
                                <p class="txt-sm margin-b-10 ">
                                    Explore various opportunities to earn some extra cash
                                </p>
                                <div class="dropdown show">
                                    <a class="btn-pop-yellow txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        See How
                                    </a>
                                    <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Become a Data Collection Agent</a>
                                        <a class="dropdown-item" href="#">Teach whatever you know & Earn</a>
                                        <a class="dropdown-item" href="#">Make money from your social media account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="container-wrapper">
            <!--<h1 class="txt-2em txt-bold txt-height-1-1 margin-b-40">
                Get a Job
            </h1>-->
            <div class="row row-5 text-center">
                <div class="col-md-3 d-flex padding-lr-5 padding-b-20">
                    <div class="flex_1 padding-lr-20 padding-tb-30 bg-ash txt-color-white">
                        <div class="txt-2em margin-b-20 txt-color-yellow">
                            <i class="fa fa-plane"></i>
                        </div>
                        <p class="txt-medium margin-b-10">
                            Make money <br> from your skill/hobby
                        </p>
                        <p class="txt-sm margin-b-20">
                            Somebody needs your skill, no matter how small. Set up your freelancer profile, create a work offer and start making money.
                        </p>
                        <a href="" class="btn btn-white txt-xs no-m-b">
                            Get started
                        </a>
                    </div>
                </div>
                <div class="col-md-3 d-flex padding-lr-5 padding-b-20">
                    <div class="flex_1 padding-lr-20 padding-tb-30 bg-ash txt-color-white" style="//background-color: #ec9bc6">
                        <div class="txt-2em margin-b-20 txt-color-yellow">
                            <i class="fa fa-home"></i>
                        </div>
                        <p class="txt-medium margin-b-10">
                            Get help to start over 50 different businesses
                        </p>
                        <p class="txt-sm margin-b-20">
                            No more trial & error. Access resources, guidance & support to start specific small businesses and get it right the first time. 
                        </p>
                        <a href="" class="btn btn-white txt-xs no-m-b">
                            Select a Business
                        </a>
                    </div>
                </div>
                <div class="col-md-3 d-flex padding-lr-5 padding-b-20">
                    <div class="flex_1 padding-lr-20 padding-tb-30 bg-ash txt-color-white" style="//background-color: #dc65a4">
                        <div class="txt-2em margin-b-20 txt-color-yellow">
                            <i class="fa fa-user"></i>
                        </div>
                        <p class="txt-medium margin-b-10">
                            Join the CVBank & be easily discovered by employers
                        </p>
                        <p class="txt-sm margin-b-20">
                            Join the CVBank and let opportunities that matches your skill & competence come to you.
                        </p>
                        <a href="" class="btn btn-white txt-xs no-m-b">
                            Get started
                        </a>
                    </div>
                </div>
                <div class="col-md-3 d-flex padding-lr-5 padding-b-20">
                    <div class="flex_1 padding-lr-20 padding-tb-30 bg-ash txt-color-white" style="//background-color: #ffd9ed">
                        <div class="txt-2em margin-b-20 txt-color-yellow">
                            <i class="fa fa-gears"></i>
                        </div>
                        <p class="txt-medium margin-b-10">
                            Talk to a Mentor
                        </p>
                        <p class="txt-sm margin-b-20">
                            What is your career or entrepreneurship challenge? Mentors are waiting to help you overcome it.
                        </p>
                        <a href="" class="btn btn-white txt-xs no-m-b">
                            Get started
                        </a>
                    </div>
                </div>
            </div>
            <div class="row row-10">
                <div class="col-md-8 d-flex padding-lr-10 margin-b-20 d-flex">
                    <div class="padding-o-20 border-o-1 border-color-darkgrey flex_1 bg-white">
                        <h2 class="txt-medium margin-b-30">
                            Featured Jobs & Freelancer Opportunities
                        </h2>
                        <div class="overflow-hidden">
                        <?php
                            //Switch to Learn a Skill Multisite (id = 3)
                            switch_to_blog(101);
                            
                            $blog_url = get_home_url();

                            //WP Query
                            $args = array(
                                'post_type' => array('task', 'job')
                            );
                            $job_query = new WP_Query();
                            $job_query->query($args);
                            
                            
                            
                            while ($job_query->have_posts()) : $job_query->the_post();
                                //print_r($post);
                                //echo '<br>';
                            
                                if( get_post_type() == 'job' ){
                                    
                        ?>
                        
                            <div class="row row-10 border-b-1 border-color-darkgrey padding-t-15 padding-b-5">
                                <div class="col-auto col-md-1 padding-lr-10 padding-b-10 txt-lg">
                                    <i class="fa fa-suitcase fa-fw job-icon"></i>
                                </div>
                                <div class="col-10 col-md-5 padding-lr-10 padding-b-10 txt-sm txt-medium">
                                    <?php the_title() ?>
                                </div>
                                <div class="col-auto col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light" style="text-transform:capitalize">
                                    <?php
                                        $field = 'job_post_locations';

                                        $meta = get_post_meta($post->ID, $field, true);

                                        if($meta){
                                            echo $meta;
                                        }
                                    ?>
                                </div>
                                <div class="col-auto col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light">
                                    Fulltime
                                </div>
                                <div class="col-12 col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light text-right text-md-left">
                                    <?php
                                        $permalink =  $blog_url.'/'.$post->post_type.'/'.$post->post_name;
                                    ?>
                                    <a href="<?php echo $permalink; ?>" class="txt-bold txt-underline">
                                        Apply
                                    </a>
                                </div>
                            </div>
                            
                        <?php } elseif( get_post_type() == 'task' ) { ?>
                            
                            <div class="row row-10 border-b-1 border-color-darkgrey padding-t-15 padding-b-5">
                                <div class="col-auto col-md-1 padding-lr-10 padding-b-10 txt-lg">
                                    <i class="fa fa-user fa-fw job-icon"></i>
                                </div>
                                <div class="col-10 col-md-5 padding-lr-10 padding-b-10 txt-sm txt-medium txt-height-1-2">
                                    <?php the_title() ?>
                                </div>
                                <div class="col-auto col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light" style="text-transform:capitalize">
                                    Lagos State
                                </div>
                                <div class="col-auto col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light">
                                    Freelance
                                </div>
                                <div class="col-12 col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light text-right text-md-left">
                                    <?php
                                        $permalink =  $blog_url.'/'.$post->post_type.'/'.$post->post_name;
                                    ?>
                                    <a href="<?php echo $permalink; ?>" class="txt-bold txt-underline">
                                        Bid
                                    </a>
                                </div>
                            </div>
                            
                        <?php
                                }
                            //Revert to Previous Multisite
                            
                            endwhile;
                            //wp_reset_postdata();
                            //wp_reset_query();
                            //$wp_query = $temp;
                            restore_current_blog();
                        ?>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="dropdown show margin-t-20">
                            <a class="btn-pop-ash txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Post a Job / Task
                            </a>
                            <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Post a Job</a>
                                <a class="dropdown-item" href="#">Post a Task</a>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex padding-lr-10 margin-b-20 d-flex">
                    <div 
                        class="flex_1 bg-grey"
                    >
                        <div class="content padding-o-30">
                            <div class="padding-b-20">
                                <p class="txt-normal-s txt-bold margin-b-10">
                                    <a class="txt-color-light txt-lg" href="https://www.saedconnect.org/service-marketplace/service-category/career-advice/?view=freelancers">
                                        Discuss, share ideas and make connections in special online communities
                                    </a>
                                </p>
                            </div>
                            
                            <div class="border-color-darkgrey padding-b-20">
                                <p class="txt-normal-s txt-bold margin-b-10">
                                    <a class="txt-color-yellow-dark" href="https://www.saedconnect.org/service-marketplace/service-category/career-advice/?view=jobs">
                                        Side Hustle Communities 
                                    </a>
                                </p>
                                <p class="txt-sm txt-height-1-1 padding-b-10">
                                    One community per business - for over 50 business types.
                                </p>
                                <div class="">
                                    <a
                                       class="btn-pop-ash txt-sm no-m-b"
                                       data-toggle="modal"
                                       href="#communityModal"
                                    >
                                        Visit a Community
                                    </a>
                                </div>
                            </div>
                            <div class="border-color-darkgrey padding-b-20">
                                <p class="txt-normal-s txt-bold margin-b-10">
                                    <a class="txt-color-yellow-dark" href="https://www.saedconnect.org/service-marketplace/service-category/career-advice/?view=freelancers">
                                        Career Communities
                                    </a>
                                </p>
                                <p class="txt-sm txt-height-1-1 padding-b-10">
                                    Discuss issues relating to CV writing, job search, interview prep, etc. 
                                </p>
                                <div class="dropdown show">
                                    <a class="btn-pop-ash txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Visit a Community
                                    </a>

                                    <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Starter Track</a>
                                        <a class="dropdown-item" href="#">Discover Track</a>
                                        <a class="dropdown-item" href="#">Validate + View Track</a>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <p class="txt-normal-s txt-bold margin-b-10">
                                    <a class="txt-color-yellow-dark" href="https://www.saedconnect.org/service-marketplace/service-category/career-advice/?view=freelancers">
                                        Entrepreneurship Communities
                                    </a>
                                </p>
                                <p class="txt-sm txt-height-1-1 padding-b-10">
                                    Discuss about starting a business, building a business plan, raising finance, overcoming marketing challenges, etc. 
                                </p>
                                <div class="dropdown show">
                                    <a class="btn-pop-ash txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Visit a Community
                                    </a>

                                    <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Get Branding, Marketing & Sales Right</a>
                                        <a class="dropdown-item" href="#">Raise Finance for your Business</a>
                                        <a class="dropdown-item" href="#">Get Business Registration & Legals Right</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="container-wrapper padding-b-20 overflow-hidden">
            <!--<h1 class="txt-2em txt-bold txt-height-1-1 margin-b-40">
                Start & Grow Your Business
            </h1>-->
            <div class="bg-yellow padding-lr-40 padding-t-40 margin-b-100">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="txt-lg txt-height-1-4 txt-bold padding-b-20">
                            Support / Collaborate with a young person to make progress
                        </h3>
                        <p class="txt-normal-s padding-b-40">
                            Get exclusive information, Connect with experts and other entrepreneurs.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="txt-normal-s txt-color-dark">
                            <i class="fa fa-question-circle padding-r-5"></i>
                            Need help to advance your business or project? <a href="" class="txt-color-dark txt-underline">Request here</a>
                        </p>
                    </div>
                </div>
                <div>
                <?php
                    switch_to_blog(103);
                    
                    $request_type_array = array(
                        array(
                            'gf_id' => 20,
                            'parent_post_field' => 15,
                            'gv_id' => 99,
                            'title' => 'Tool Request',
                        ),
                        array(
                            'gf_id' => 21,
                            'parent_post_field' => 12,
                            'gv_id' => 99,
                            'title' => 'Land Request',
                        ),
                        array(
                            'gf_id' => 22,
                            'parent_post_field' => 13,
                            'gv_id' => 20,
                            'title' => 'Request for Workspace/Office Space/Building/Factory Space',
                        ),
                        array(
                            'gf_id' => 23,
                            'parent_post_field' => 13,
                            'gv_id' => 20,
                            'title' => 'Request for Collaborator/Volunteer',
                        ),
                        array(
                            'gf_id' => 24,
                            'parent_post_field' => 9,
                            'gv_id' => 20,
                            'title' => 'Request for Funding',
                        ),
                        array(
                            'gf_id' => 25,
                            'parent_post_field' => 11,
                            'gv_id' => 20,
                            'title' => 'Request for Marketing/Publicity',
                        ),
                        array(
                            'gf_id' => 27,
                            'parent_post_field' => 2,
                            'gv_id' => 20,
                            'title' => 'Request for a Mentor',
                        ),
                    );
                    
                    /* GF Search Criteria */
                    $search_criteria = array();
                    
                    $order_count = GFAPI::count_entries( $request_item['gv_id'], $search_criteria );
                ?>
                   
                    <a href="" class="btn btn-white txt-xs featured-request">
                        <?php echo $request_item['title'] ?> 
                        <span class="value"><?php echo $order_count ?></span>
                    </a>
                    
                <?php } ?>
                   
                    <a href="" class="btn btn-white txt-xs featured-request">Tool Requests<span class="value">67</span></a>
                    <a href="" class="btn btn-white txt-xs featured-request">Workspace Requests<span class="value">67</span></a>
                    <a href="" class="btn btn-white txt-xs featured-request">Volunteer Requests<span class="value">67</span></a>
                    <a href="" class="btn btn-white txt-xs featured-request">Funding Requests<span class="value">67</span></a>
                    <a href="" class="btn btn-white txt-xs featured-request">Marketing Requests<span class="value">67</span></a>
                    <a href="" class="btn btn-white txt-xs featured-request">Mentorship Requests<span class="value">67</span></a>
                    <a href="" class="btn btn-blue txt-xs featured-request">Business Partner Requests<span class="value">67</span></a>
                </div>
                <div class="" style="position:relative;transform:translateY(80px)">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="txt-lg txt-height-1-4 txt-bold padding-b-20">
                                Featured Ventures
                            </h2>
                        </div>
                        <div class="col-md-4 text-md-right">
                            <a href="" class="txt-color-dark txt-sm txt-medium txt-underline">
                                Explore the Ventures Directory
                            </a>
                        </div>
                    </div>
                    <div class="row row-5">
                    <?php 
                        switch_to_blog(103);

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'business',
                                'post_status' => 'publish',
                                'posts_per_page' => 4,
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID

                            /* Get Opportunity Banner */
                            $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                            $image = reset( $images );
                    ?>
                        <div class="col-md-3 padding-lr-5 padding-b-20 d-flex">
                            <a href="<?php the_permalink() ?>" class="home-community-card">
                                <article class="content">
                                <?php
                                    $meta = get_post_meta($post_id, 'business_banner', true);
                                    if($meta){

                                    }
                                ?>
                                    <figure class="image-box" style="background-image: url('<?php echo $meta; ?>')">

                                    </figure>
                                    <div class="info bg-white">
                                        <h3 class=" txt-medium margin-b-10">
                                            <?php the_title() ?>
                                        </h3>
                                        <p class="txt-sm">
                                            <?php
                                                $meta = get_post_meta($post_id, 'business_description', true);
                                                if($meta){
                                                    echo truncate($meta, 70);
                                                }
                                            ?>
                                        </p>
                                    </div>
                                </article>  
                            </a>   
                        </div>
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                            <h2 class="txt-lg txt-medium">
                                No Businesses found.
                            </h2>
                        </div>   

                    <?php

                        }

                        restore_current_blog();
                    ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-r-md-30">
                    <!--<div class="row">
                        <div class="col-md-8">
                            <h2 class="txt-medium margin-b-20">
                                Featured Ventures
                            </h2>
                        </div>
                        <div class="col-md-4 text-md-right">
                            <a href="" class="txt-color-dark txt-sm txt-medium txt-underline">
                                Explore the Ventures Directory
                            </a>
                        </div>
                    </div>-->
                    <!--<ul class="row row-5 marketplace-freelancer-cards margin-b-20">
                        <li class="col-sm-6 col-lg-4 padding-lr-5">
                            <a href="marketplace_service_entry.html">
                                <span class="wrapper row">
                                    <span class="col-4 padding-r-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/Database of the Stars.jpg" alt="">
                                    </span>
                                    <div class="col-8 content">
                                        <div class="name">
                                            Jimlas Agric Pen
                                        </div>
                                        <p class="txt-xs">
                                            Feeding the Nation, one bag at a time.
                                        </p>
                                    </div>
                                </span>
                                <div class="services">
                                    Abakaliki, Agriculture
                                </div>
                            </a> 
                        </li>
                        <li class="col-sm-6 col-lg-4 padding-lr-5">
                            <a href="marketplace_service_entry.html">
                                <span class="wrapper row">
                                    <span class="col-4 padding-r-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/distribution_network.jpg" alt="">
                                    </span>
                                    <div class="col-8 content">
                                        <div class="name">
                                            Jimlas Agric Pen
                                        </div>
                                        <p class="txt-xs">
                                            Feeding the Nation, one bag at a time.
                                        </p>
                                    </div>
                                </span>
                                <div class="services">
                                    Abakaliki, Agriculture
                                </div>
                            </a> 
                        </li>
                        <li class="col-sm-6 col-lg-4 padding-lr-5">
                            <a href="marketplace_service_entry.html">
                                <span class="wrapper row">
                                    <span class="col-4 padding-r-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/Database of the Stars.jpg" alt="">
                                    </span>
                                    <div class="col-8 content">
                                        <div class="name">
                                            Jimlas Agric Pen
                                        </div>
                                        <p class="txt-xs">
                                            Feeding the Nation, one bag at a time.
                                        </p>
                                    </div>
                                </span>
                                <div class="services">
                                    Abakaliki, Agriculture
                                </div>
                            </a> 
                        </li>
                    </ul>-->
                    
                </div>
                <!--<div class="col-md-4 d-flex border-o-1 border-color-darkgrey">
                    <div class="flex_1">
                        <h3 class="txt-medium txt-xlg padding-lr-30 border-b-1 border-color-darkgrey padding-tb-20 margin-b-20">
                            Business Partner Connect
                        </h3>
                        <div class="padding-lr-30 border-b-1 border-color-darkgrey padding-b-20 margin-b-20">
                            <p class="txt-normal-s txt-bold margin-b-10">
                                Looking for partner to support with marketing
                            </p>
                            <p class="txt-sm margin-b-10">
                                Showcase your business venture & let investors, customers & collaborators find you.
                            </p>
                            <p class="txt-sm txt-medium">
                                Abakaliki, Agriculture
                            </p>
                        </div>
                        <div class="padding-lr-30 border-b-1 border-color-darkgrey padding-b-20 margin-b-20">
                            <p class="txt-normal-s txt-bold margin-b-10">
                                Looking for partner to support with marketing
                            </p>
                            <p class="txt-sm margin-b-10">
                                Showcase your business venture & let investors, customers & collaborators find you.
                            </p>
                            <p class="txt-sm txt-medium">
                                Abakaliki, Agriculture
                            </p>
                        </div>
                        <div class="padding-lr-30 border-b-1 border-color-darkgrey padding-b-20 margin-b-20">
                            <p class="txt-normal-s txt-bold margin-b-10">
                                Looking for partner to support with marketing
                            </p>
                            <p class="txt-sm margin-b-10">
                                Showcase your business venture & let investors, customers & collaborators find you.
                            </p>
                            <p class="txt-sm txt-medium">
                                Abakaliki, Agriculture
                            </p>
                        </div>
                        <div class="padding-lr-30 padding-b-20">
                            <p class="txt-normal-s txt-bold margin-b-10">
                                <a href="" class="txt-lg txt-bold txt-color-dark txt-underline">
                                    Find a Business Partner
                                </a>
                            </p>
                        </div>
                    </div>
                </div>-->
            </div>
        </section>
        
        <section class="container-wrapper padding-b-20">
            <!--<div class="row">
                <h1 class="col-md-8 txt-2em txt-bold txt-height-1-1 margin-b-40">
                    Open Growth Opportunities
                </h1>
                <p class="col-md-4 text-md-right txt-normal-s txt-bold margin-b-10">
                    <a href="https://www.saedconnect.org/opportunity-center/" class="txt-medium txt-color-dark txt-underline">
                        See all Open Opportunities
                        <i class="fa fa-arrow-right"></i>
                    </a>
                </p>
            </div>-->
            <div class="padding-t-20 padding-b-10 border-tb-1">
                <div class="row">
                    <div class="col-md-8 padding-b-10">
                        <h4 class="txt-lg txt-bold txt-height-1-4">
                            Open Growth Opportunities
                        </h4>
                    </div>
                    <div class="col-md-4 text-md-right padding-b-10">
                        <a href="" class="btn btn-trans-bw txt-xs no-m-b">
                            See All
                        </a>
                    </div>
                </div>
            </div>
            <div class="program-card-wrapper">
                <div class="row row-20">
                <?php 
                    switch_to_blog(12);
                    
                    wp_reset_postdata();
                    wp_reset_query();
                    $temp = $wp_query; $wp_query= null;
                    $wp_query = new WP_Query();
                    $wp_query->query( 
                        array(
                            'post_type' => 'opportunity',
                            'post_status' => 'publish',
                            'posts_per_page' => 4,
                            'meta_query' => array(
                                array(
                                    'key' => 'opportunity-featured',
                                    'value' => '1',
                                ),
                            ),
                        ) 
                    );

                    if ( $wp_query->have_posts() ) {

                        while ($wp_query->have_posts()) : $wp_query->the_post();

                        /* Variables */
                        $program_id = $post->ID;    //Get Program ID

                        /* Get Opportunity Banner */
                        $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                        $image = reset( $images );
                                    
                        /* Get expiry date */
                        $expiry_date = rwmb_meta( 'opportunity-close-date' );
                        $today_date = date('y-m-d');
                        
                        $td = strtotime( $today_date );
                        
                        //echo 'Today: '.$td.' Expire: '.$expiry_date;
                        
                        /* Check if expired */
                        if( $td < $expiry_date ){
                        
                ?>
                    <div class="col-md-3 program-card">
                        <div class="content">
                            <figcaption>
                                <?php
                                    if( rwmb_get_value( 'opportunity-close-date' ) ){
                                ?>
                                    
                                    <h4 class="date">
                                        <span class="expires">
                                            Expires
                                        </span>
                                        <?php rwmb_the_value( 'opportunity-close-date', array( 'format' => 'd\ M Y' ) ); ?>
                                    </h4>
                                    
                                <?php } ?>
                                
                                
                                <div class="header">
                                    <h3 class="title">
                                        <a href="<?php the_permalink() ?>">
                                            <?php echo rwmb_get_value( 'opportunity-title' ); ?>
                                        </a>
                                    </h3>
                                </div>
                                <p class="description">
                                    <?php echo rwmb_get_value( 'opportunity-short-description' ); ?>
                                </p>
                                <?php
                                    if( rwmb_get_value( 'opportunity-author' ) ){
                                ?>

                                    <h4 class="author">
                                        Powered by <?php echo rwmb_get_value( 'opportunity-author' ); ?>
                                    </h4>

                                <?php } ?>
                            </figcaption>
                            <div class="image-box">
                                <img src="<?php if($image){ echo $image['full_url']; } ?>" alt="">
                            </div>
                        </div>
                    </div>

                <?php
                    }
                        endwhile;

                    }else{
                ?>
                    <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                        <h2 class="txt-lg txt-medium">
                            No Opportunities found.
                        </h2>
                    </div>   

                <?php

                    }
                    
                    restore_current_blog();
                ?>
			    </div>
            </div>
            <div class="bg-grey padding-lr-20 padding-t-20 padding-b-10 margin-b-20">
                <div class="row">
                    <div class="col-md-8 padding-b-10">
                        <h4 class="txt-medium txt-height-1-4">
                            Have a program or initiative that can be beneficial to youths in any way? Share it here and get a lot of responses from interested young people.
                        </h4>
                    </div>
                    <div class="col-md-4 text-md-right padding-b-10">
                        <a href="" class="btn btn-trans-bw txt-sm no-m-b">
                            Get started
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <div class="pre-footer lg" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (22).jpg')">
            <div class="content">
                <h4 class="title">
                    Empower People, Transform Lives
                </h4>
                <p class="txt-height-1-4 txt-color-white margin-b-40">
                    The world needs more problem solvers. Your support will create possibilities and amplify impact.
                </p>
                <article class="btn-wrapper">
                    <a class="btn btn-trans-wb icon" href="">
                        Contribute
                    </a>
                    <a class="btn btn-trans-wb" href="">
                        Support Youth Development
                    </a>
                    <a class="btn btn-white" href="">
                        Donate Online
                    </a>
                </article>
            </div>
        </div>
    </main>
    
    <?php get_footer() ?>
    
    <script>
        var thumbs = jQuery('#thumbnails').slippry({
            // general elements & wrapper
            slippryWrapper: '<div class="slippry_box thumbnails" />',
            // options
            transition: 'fade',
            pager: false,
            auto: true,
            onSlideBefore: function (el, index_old, index_new) {
                jQuery('.thumbs a').removeClass('active');
                jQuery(jQuery('.thumbs a')[index_new]).addClass('active');
            }
        });

        jQuery('.thumbs a').click(function () {
            thumbs.goToSlide($(this).data('slide'));
            return false;
        });
        
        $('.thumbs a').hover(function() {
            //$( this ).trigger( "click" );
       });
    </script>

        
    <?php switch_to_blog(20); ?>

    <!-- SHC Modal -->
    <div class="modal fade font-main shc-modal" id="communityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="container-wrapper">
                    <div class="col-md-10 mx-auto">
                        <div class="padding-tb-80">
                            <div class="margin-b-40">
                                <h2 class="txt-2em txt-bold txt-height-1-2">
                                    What Side Hustle Community
                                    <br>
                                    are you interested in?
                                </h2>
                            </div>
                            <ul class="row row-20 shc-list txt-normal-s txt-medium">
                            <?php
                                wp_reset_postdata();
                                wp_reset_query();

                                $temp = $wp_query; $wp_query= null;
                                $wp_query = new WP_Query();
                                $wp_query->query(array('post_type' => 'information-session'));
                                    while ($wp_query->have_posts()) : $wp_query->the_post();                
                            ?>

                                <div class="col-md-4 padding-lr-20">
                                    <li>
                                        <a href="<?php the_permalink($post->id); ?>">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                <?php the_title() ?>
                                            </span>
                                        </a>
                                    </li>
                                </div>

                            <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php restore_current_blog(); ?>