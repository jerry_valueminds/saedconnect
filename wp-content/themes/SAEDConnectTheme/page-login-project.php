    <?php /*Template Name: Login - Project Page*/ ?>
    
    <?php
                    
        if ( is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            //$dashboard_link = get_site_url().'/my-dashboard'; //Get Daasboard Page Link by ID
            $dashboard_link = network_home_url().'/ventures-directory/create-project/'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header() ?>
    
    <style>
        .gfield_checkbox label, .login-form .gfield_label{
            display: block !important;
        }
        
        .gfield_checkbox li{
            display: flex;
            align-items: center;
        }
        
        .gfield_checkbox input{
            margin: 0 !important;
            margin-right: 10px !important;
        }
        
        .gform_wrapper label.gfield_label {
            font-size: 0.8em !important;
            font-weight: 500;
            font-size: inherit;
        }
        
        .gform_button{
            display: block !important;
            width: 100% !important;
            text-align: center !important;
            border-radius: 4px !important;
        }
    </style>
    
    <!-- // If User is not Logged in, Load Login Page -->
    <main class="main-content">
        <section>
            <div class="row row-40">
                <div class="col-lg-6 d-none d-lg-block padding-lr-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/project_login.jpg" alt="Page Hero" class="d-block">
                </div>
                <div class="col-lg-5 padding-lr-40">
                    <header class="container-wrapper text-center padding-t-80 margin-b-40">
                        <div class="margin-b-20">
                            <span class="d-inline-block rounded-circle bg-yellow" style="width:40px; height:40px"></span>
                        </div>
                        <h1 class="txt-2em txt-light txt-color-dark margin-b-10">
                            Welcome back
                        </h1>
                        <div class="margin-b-40">
                            <span class="d-inline-block bg-yellow rounded" style="width:80px; height:3px"></span>
                        </div> 
                        <div>
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                            <?php endif;?>
                        </div>
                        <div class="txt-normal-s padding-t-20">
                           Forgot Password?
                            <a class="txt-medium txt-color-green" href="https://www.saedconnect.org/password-reset/">
                                Click here to Reset your Password
                            </a>
                        </div>
                    </header>
                </div>
                <div class="col-md-1"></div>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>