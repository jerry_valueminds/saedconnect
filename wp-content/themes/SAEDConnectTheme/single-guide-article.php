    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>

    <?php
        /* 
        *=============================================
        * Get Content for this view
        *=============================================
        */

        $article_id = get_the_ID();
        $articleTitle = get_the_title();
        
        /* Variables to store data */
        $miniSiteID;
        $miniSiteName;

        $topicName;
        $topicID;
        $sponsorName;
        $sponsorImage;
    
        $sectionName;
        $section_id;
        $section_theme_color;
        $section_hover_color;

        /*
        *=============================================
        * First Query to get Topic Name or Mini-site Name
        *=============================================
        */
        // Topic Name Query
        $topicNameQuery = new WP_Query( array(
            'relationship' => array(
                'id'   => 'guide_topic_to_guide_article',
                'to' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );

        /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
        if ( $topicNameQuery->have_posts() ){
            while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post();  
            
            $topicID = get_the_ID();
            $topicName = get_the_title();
            $topic_url = get_permalink();
            $images = rwmb_meta( 'mini-site-sponsor-image', array( 'limit' => 1 ) );
            $image = reset( $images );
            $sponsorImage = $image['full_url'];

                /*
                *=============================================
                * Second Query to get Section Name 
                *=============================================
                */
                $sectionNameQuery = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'og_section_to_guide_topic',
                        'to' => get_the_ID(), // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $sectionNameQuery->have_posts() ) : $sectionNameQuery->the_post();  
                        $sectionName = get_the_title();
                        $section_id = get_the_ID();
                        $section_theme_color = rwmb_meta( 'section-theme-color' );
                        $section_hover_color = rwmb_meta( 'section-link-hover-color' );

                endwhile;
                wp_reset_postdata();
                /*
                *=============================================
                * Second Query: END
                *=============================================
                */

            endwhile;
            wp_reset_postdata();
        }
        /*
        *=============================================
        * First Query: END
        *=============================================
        */
    ?>
            
    <main class="main-content">
        <header class="overview-header container-wrapper">
            <div class="info-box">
                <div class="info">
                    <h2 class="subtitle">
                        <?php echo $sectionName ?>
                    </h2>
                    <h1 class="title txt-color-lighter">
                        <?php echo $topicName ?>
                    </h1>
                </div>
                <div class="sponsor">
                    <div class="intro-text">
                        Content Sponsored by:
                    </div>
                    <img class="logo" src="<?php echo $sponsorImage ?>" alt="">
                </div>
            </div>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <li class="active">
                        <a href="<?php echo $topic_url ?>">Your Guide</a>
                    </li>
                    <li>
                        <a href="<?php echo $topic_url ?>/?view=resources">Tools &amp; Resources</a>
                    </li>
                </ul>
            </nav>
        </header>

        <style>
            a{
                color: <?php echo $section_theme_color; ?>;
            }

            a:active {
                color: <?php echo $section_theme_color; ?>;
            }

            a:hover {
                color: <?php echo $section_hover_color; ?>;
            }

            .overview-header .header-nav li.active a {
                color: <?php echo $section_theme_color; ?>;
            }

            .overview-header .header-nav li.active a:before {
                background-color: <?php echo $section_theme_color; ?>;
            }
        </style>
        
        <section class="container-wrapper">
            <div class="row row-40">
                <!-- Left Side Menu -->               
                <div class="col-md-3 padding-lr-40">
                    <article class="side-nav white bg-white padding-tb-40" id="side-nav">
                        <h2 class="title">
                            Outline
                        </h2>
                        <ul class="list txt-normal-s">
                        <?php
                            /* 
                            *
                            *====================================================
                            * Nav Loop: Get ALl Sibling Posts from Multi-Page Parent
                            *====================================================
                            *
                            */
                            $multi_page_query = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'guide_topic_to_guide_article',
                                    'from' => $topicID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $multi_page_query->have_posts() ) : $multi_page_query->the_post();
                        ?>

                            <?php //$topicName = rwmb_get_value( 'article-hierarchy-level' ); ?>      
                            <li class="<?php echo ($articleTitle == get_the_title()  ? 'active' : '') ?>">
                                <a 
                                   class="padding-l-<?php echo rwmb_get_value( 'article-hierarchy-level' ); ?>"
                                   href="<?php the_permalink() ?>"
                                   style="background-color:<?php echo ($articleTitle == get_the_title()   ? $section_theme_color : '') ?>"
                                >
                                    <?php the_title() ?>
                                </a>
                            </li>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* 
                            *
                            *====================================================
                            * Nav Loop: END
                            *====================================================
                            *
                            */
                        ?>
                        </ul>
                    </article>
                </div>
                <!-- Main Content -->
                <div class="col-12 col-md-6 padding-t-40 border-lr-1 border-color-darkgrey">
                    <!-- Main Content -->
                    <?php get_template_part( 'template-parts/content-block-template' ); ?>
                </div>
                <!-- Right Side Content -->
                <div class="col-md-3">                    
                    <?php
                        global $relashionship_id; 
                        global $relationship_parent_id;

                        $relashionship_id = 'three_c_m_page_to_rcb';
                        $relationship_parent_id = $multiPageID;
                    ?>
                    <?php get_template_part( 'template-parts/section-side-content' ); ?>
                </div>
            </div>
        </section>
        
        <!-- Get Section Footer Posts: Using Multi-Item General Section Template Part -->
        <section>
           <div class="row" style="align-items: flex-start;">
            <?php 
                global $relashionship_id; 
                global $relationship_parent_id;
            
                $relashionship_id = 'three_c_m_page_to_bcb';
                $relationship_parent_id = $multiPageID;
            ?>
            <?php get_template_part( 'template-parts/multi-item-general-content' ); ?>
            </div>
        </section>
    </main>
    
    <?php endwhile; // end of the loop. ?>

    <?php get_footer() ?>
    
