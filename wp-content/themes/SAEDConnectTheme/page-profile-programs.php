<?php /*Template Name: Profile - Programs*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Publication */
        $publication_key = 'publication_status';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
            
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                <?php if( $_REQUEST['gf-id'] ){ ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                    
                <?php } elseif( $_REQUEST['form-view'] == 'collaborators' ) { //Collaborators ?>
                
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            $post_id = $_GET['post-id'];
                            $meta_key = 'collab_project';
                            $redirect_link = currentUrl(true);

                            if($_POST['users']){
                                /* Delete existing meta */
                                $saved_users = get_post_meta($post_id, $meta_key);

                                foreach($saved_users as $saved_user){
                                    delete_post_meta($post_id, $meta_key, $saved_user);
                                }

                                /* Save Submitted User Selection */
                                $submitted_users = $_POST['users'];

                                foreach($submitted_users as $submitted_user){
                                    add_post_meta( $post_id, $meta_key, $submitted_user );
                                }

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                            }

                            /* Get Saved Users */
                            $saved_users = get_post_meta($post_id, $meta_key);

                            /* Get All Users */
                            $blogusers = get_users( 'blog_id=1&orderby=nicename&role=subscriber' );
                        ?>
                        <form action="#" method="POST">
                            <p class="txt-sm txt-medium margin-b-10">
                                Search & assign Collaborators to this project
                            </p>
                            <select class="select-collaborators full-width" name="users[]" multiple="multiple">
                                <option value="1" >
                                    Super Admin
                                </option>
                            <?php foreach( $blogusers as $user ){ ?>

                                <option 
                                   value="<?php echo esc_html( $user->ID ); ?>"
                                   <?php echo ( in_array($user->ID, $saved_users) ) ? "selected" : ""; ?>
                                >
                                    <?php echo esc_html( $user->user_login ); ?>
                                </option>

                            <?php } ?>
                            </select>
                            <div class="padding-t-10 text-right">
                                <input type="submit" value="Save" class="btn btn-blue txt-xs">
                            </div>
                        </form>
                    </div>

                <?php } else { ?>
                    
                    <?php
                        $postType = 'program';
                             
                        /* Get Post ID */
                        if($_GET['post-id']){
                            $post_id = $_GET['post-id'];
                        }
      
                        /* Redirect Link */
                        $redirect_link = currentUrl(true);
                              
                        /* Delete & Return */
                        if($_GET['action'] == 'delete'){
                            /* Delete Post */
                            wp_delete_post($post_id);

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }

                        
                    ?>

                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post" class="form">
                            <?php
                                /* Meta Key */
                                $postType = 'opportunity';
                                $tax_types = array(
                                    array(
                                        'name' => 'Program Type',
                                        'slug' => 'program-type',
                                        'hierachical' => true,
                                    ),
                                    array(
                                        'name' => 'Industries',
                                        'slug' => 'industry',
                                        'hierachical' => false,
                                    ),
                                );
                                $redirect_link = currentUrl(true);
                              
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    $post = get_post($post_id);
                                    $postName = $post->post_title;
                                    $deadline = get_post_meta( $post->ID, 'deadline', true );
                                    
                                    /* Delete & Return */
                                    if($_GET['action'] == 'delete'){
                                        /* Delete Post */
                                        wp_delete_post($post_id);

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                    
                                    /* Publish / Unpublish & Return */
                                    if($_GET['action'] == 'publication'){
                                        /* Meta value to save */
                                        $value = "user_published";
                                        
                                        /* Get saved meta */
                                        $saved_meta = get_post_meta( $post_id, $publication_key, true );

                                        if ( $saved_meta ) //If published, Unpublish
                                            delete_post_meta( $post_id, $publication_key );
                                        else //If Unpublished, Publish
                                            update_post_meta( $post_id, $publication_key, $value );

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                }
                                                            
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if($_POST){
                                    
                                    /* Get Post Name */
                                    $postName = $_POST['post-name'];
                                    $deadline = sanitize_text_field( $_POST['deadline'] ); 
                                    
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));
                                    
                                    update_post_meta( $post_id, 'deadline', $deadline );

                                    /* Save terms to post */
                                    foreach($tax_types as $tax_type){
                                        wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug']);
                                    }
                                    
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }
                            ?>
                            
                            <!-- Title -->
                            <div class="form-item conditional" data-condition="['freelancer'].includes(job-type)">
                                <label for="deadline">
                                    Project / Goal Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post-name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $postName ?>"
                                >
                            </div>
                            
                            <!-- Terms -->
                            <div class="txt-normal-s">
                            <?php foreach($tax_types as $tax_type){ ?>
                            
                                <?php if( $tax_type['hierachical']){ ?>
                            
                                    <!-- Tax Title -->
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_type['name']; ?>
                                    </h2>

                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <?php if( $parent == 0 ){ ?>

                                            <div class="padding-b-20">
                                                <div class="txt-medium txt-color-dark margin-b-15">
                                                    <?php echo $term_name; ?>
                                                </div>
                                                <?php
                                                    foreach ($terms as $child_term) {
                                                        // Check and see if the term is a top-level parent. If so, display it.
                                                        $child_parent = $child_term->parent;
                                                        $child_term_id = $child_term->term_id; //Get the term ID
                                                        $child_term_name = $child_term->name; //Get the term name

                                                        if( $child_parent == $term_id ){
                                                ?>
                                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                            <input
                                                                class="margin-r-5"
                                                                type="checkbox" 
                                                                value="<?php echo $child_term_id ?>" 
                                                                name="<?php echo $tax_type['slug'] ?>[]" 
                                                                <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                            >
                                                            <span class="bg-label">
                                                                <?php echo $child_term_name; ?>
                                                            </span>
                                                        </label>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </div>

                                        <?php } ?>

                                    <?php } ?>
                                
                                <?php } else { ?>
                                
                                    <!-- Tax Title -->
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_type['name']; ?>
                                    </h2>
                                    
                                    <div class="padding-b-20">
                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                            <input
                                                class="margin-r-5"
                                                type="checkbox" 
                                                value="<?php echo $term_id ?>" 
                                                name="<?php echo $tax_type['slug'] ?>[]" 
                                                <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                            >
                                            <span class="bg-label">
                                                <?php echo $term_name; ?>
                                            </span>
                                        </label>

                                    <?php } ?>
                                    </div>
                                <?php } ?>
                                
                            <?php } ?>
                            </div>
                                                       
                            <!-- Deadline -->
                            <div class="form-item conditional" data-condition="['freelancer'].includes(job-type)">
                                <label for="deadline">
                                    Expiry Date
                                </label>
                                <input type="date" name="deadline" value="<?php echo $deadline ?>">
                            </div>
                                                        
                            <div class="text-right">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>

                <?php } ?>                    
                </div>
            
            <?php } elseif($_GET['view'] == 'interactions'){ ?>
            
                <div class="page-header">
                    <h1 class="page-title">
                        Opportunity Applications
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Here you can view the status of all the opportunities you have applied for.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] == '') ? 'active' : ''; ?>"
                            >
                                My Programs
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Opportunity Applications
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=collaborations'; ?>"
                               class="<?php echo ($_GET['view'] == 'collaborations') ? 'active' : ''; ?>"
                            >
                                Programs I can Collaborate on
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="row row-15">
                <?php
                    /* GF Search Criteria */
                    $gf_id = 2;
                    $parent_post_id = 2;
                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                    foreach( $entries as $entry ){

                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $parent_post = get_post($parent_post_id);
                ?>
                    <div class="col-md-6 padding-lr-15 padding-b-20 d-flex">
                        <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                            <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                <?php echo rgar( $entry, '1' ) ?>
                            </h3>
                            <div class="txt-normal-s margin-b-20">
                                <p>
                                    <?php echo rgar( $entry, '3' ) ?>
                                </p>
                            </div>
                            <h3 class="txt-sm txt-color-dark txt-medium">
                                For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                            </h3>
                        </div>
                    </div>
                <?php } ?>
                </div>    
                
            <?php } elseif($_GET['view'] == 'collaborations'){ ?>
            
            

            <?php } else { ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        My Programs
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form-title=Add an Opportunity" class="cta-btn">
                            Add Program
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        View all the opportunities that you have added.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] == '') ? 'active' : ''; ?>"
                            >
                                My Programs
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Opportunity Applications
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=collaborations'; ?>"
                               class="<?php echo ($_GET['view'] == 'collaborations') ? 'active' : ''; ?>"
                            >
                                Programs I can Collaborate on
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <style>
                    .publication-status{
                        display: flex;
                        align-items: center;
                        top: 0;
                        right: 0;
                        padding: 5px 10px;
                        color: black;
                        background-color: gainsboro;
                    }

                    .user_published{
                       background-color: #f4c026; 
                    }

                    .admin_published{
                       background-color: #00bfe7; 
                    }

                    .home-community-card{
                        overflow: visible;
                    }
                </style>
                
                <div class="">
                <?php wp_reset_postdata();
                    wp_reset_query();
                    $temp = $wp_query; $wp_query= null;
                    $wp_query = new WP_Query();
                    $wp_query->query( 
                        array(
                            'post_type' => 'opportunity',
                            'post_status' => 'publish',
                            'author' => $current_user->ID,
                            'posts_per_page' => -1
                        ) 
                    );

                    if ( $wp_query->have_posts() ) {

                        /* Get Post Count */
                        $count = 1;
                        $total_posts = $wp_query->found_posts;

                        while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $program_id = $post->ID;    //Get Program ID
                            $post_id = $post->ID;    //Get Program ID
                        
                            /* Publication status */      
                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                            /* Get Entry */
                            /* GF Search Criteria */
                            $gf_id = 1;
                            $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                        'key' => '31', 'value' => $program_id, //Current logged in user
                                    )
                                )
                            );

                            /* Get GF Entry Count */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $entry = $entries[0];

                            /* Get expiry date */
                            $expiry_date = rwmb_meta( 'opportunity-close-date' );
                            $field = 'submit_opportunity_expires';
                            $exp = 2;
                            $td = 1;

                        /* Check if expired */
                        //if( $td < $expiry_date ){
                        if( true ){

                        //if( $td > $exp ){
                ?>

                <?php if($count == 1){  ?>

                    <div class="program-card-wrapper">
                        <div class="row row-20">

                <?php } ?>         
                            <div class="col-md-4 program-card">
                                <div class="content">
                                    <figcaption>
                                        <?php 
                                            $deadline = get_post_meta( $program_id, 'deadline', true );

                                            if( $deadline ){
                                        ?>
                                        <h4 class="date">
                                            <span class="expires">
                                                Expires
                                            </span>
                                            <?php
                                                $field = 'submit_opportunity_expires';
                                                $date = strtotime( $deadline );

                                                echo date('j F Y', $date);
                                            ?>
                                        </h4>
                                        <?php } ?>
                                        <div class="header">
                                            <h3 class="title">
                                                <a href="<?php the_permalink() ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </h3>
                                        </div>
                                        <p class="description">
                                            <?php echo get_post_meta( $program_id, 'summary', true ) ?>
                                        </p>
                                        <?php $meta = get_post_meta( 'author', $program_id ); ?>
                                        <?php if($meta){ ?>
                                        <h4 class="author">Powered by
                                            <?php echo $meta ?>
                                        </h4>
                                        <?php } ?>
                                    </figcaption>
                                    <div class="image-box">
                                        <?php 
                                            $images = "";
                                            $images = get_attached_media( 'image', $program_id ); 
                                        ?>

                                        <?php if($images){ ?>

                                            <?php  
                                                foreach($images as $image) { //print_r( $image ); 
                                                    $previousImg_id = $image->ID;
                                            ?>
                                                <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">

                                            <?php } ?>

                                        <?php } else { ?>

                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png">

                                        <?php } ?>
                                    </div>
                                    <p class="txt-sm txt-color-dark margin-t-20">
                                        <?php 
                                            $response_gf_id = 2; //Form ID

                                            /* GF Search Criteria */
                                            $response_search_criteria = array(

                                            'field_filters' => array( //which fields to search

                                                array(

                                                    'key' => '2', 'value' => $program_id, //Current logged in user
                                                    )
                                                )
                                            );

                                            /* Get Entries */
                                            $response_entries = GFAPI::get_entries( $response_gf_id, $response_search_criteria );

                                            /* Get GF Entry Count */
                                            $response_entry_count = GFAPI::count_entries( $response_gf_id, $response_search_criteria );
                                        ?>
                                        <span class="txt-bold">
                                            <?php echo $response_entry_count ?>
                                        </span>
                                        Messages
                                    </p>
                                    <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                        <div>
                                            <a 
                                               href="<?php printf('%s/?view=form&post-id=%s&form-title=Edit %s', currentUrl(true), $program_id, get_the_title() ) ?>"
                                               class="txt-medium txt-color-green"
                                            >
                                                Edit
                                            </a>
                                            |
                                            <a 
                                               href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', currentUrl(true), $program_id ) ?>"
                                               class="confirm-delete txt-color-red"
                                            >
                                                Delete
                                            </a>
                                        </div>
                                        <div class="text-right">
                                            <div class="publication-status <?php echo $publication_meta ?>">
                                                <?php
                                                    $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                                    if($publication_meta == 'user_published'){
                                                        echo 'Undergoing review';
                                                    } elseif($publication_meta == 'admin_published') {
                                                        echo 'Published';
                                                    }else{
                                                        echo 'Unpublished';
                                                    }
                                                ?>
                                                <div class="dropdown padding-l-20">
                                                    <a class="" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="fa-stack">
                                                            <i class="fa fa-circle fa-stack-2x txt-color-white"></i>
                                                            <i class="fa fa-ellipsis-v fa-stack-1x"></i>
                                                        </span>
                                                    </a>

                                                    <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuLink">

                                                        <a 
                                                           href="<?php printf('%s/?view=form&form-view=collaborators&post-id=%s&form-title=%s Collaborators', currentUrl(true), $program_id, get_the_title() ) ?>" 
                                                           class="dropdown-item"
                                                        >
                                                            Collaborators
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                <?php
                    if( $count == 3 || $count == $total_posts ){ ?>

                        </div>
                    </div>

                <?php
                        $count = 0;
                    }
                    $count++;   
                ?> 

                <?php
                        }
                        endwhile;

                    } else {
                ?>
                    <div class="padding-t-20 padding-b-40">
                        <h2 class="txt-lg txt-medium">
                            No Opportunities found.
                        </h2>
                    </div>   

                <?php

                    }
                ?>
                </div>

            <?php } ?>   
            
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>