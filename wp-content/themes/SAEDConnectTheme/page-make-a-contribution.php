<?php /*Template Name: Make a Contribution*/ ?>
<?php
    $site_url = get_site_url();
    $question_id = $_GET['question-id'];
    $answer_id = $_GET['answer-id'];
    $permalink = get_permalink($question_id);

    /* Delete Action */
    if( $_GET['action'] == 'delete' ){
        /* Delete */
        $post_id = $_GET['answer-id'];

        wp_delete_post($post_id);

        if ( wp_redirect( $permalink ) ) {
            exit;
        }
        
    } elseif( $_GET['action'] == 'unpublish' ) {
        /* Edit Post */
        $post_data = array (
            'ID' => $_GET['answer-id'],
            'post_status' => 'draft'
        );
        
        $post_id = wp_update_post($post_data, true);

        if ( wp_redirect( $permalink ) ) {
            exit;
        }
    }


    if( $_POST['form_action'] == 'edit' ){
        
        /* Edit Post */
        // create post object with the form values
        $postName = $_POST['postName'];
        $childPostType = $_POST['childPostType'];

        /* Meta */
        $meta_content = array( 
            'answer-summary',
            'answer-content',
            'answer-video-media',
            'answer-audio-media',
        );

        $post_data = array (
            'ID' => $_POST['edit_id'],
            'post_title' => $postName,
            'post_content' => "",
        );
        
        $post_id = wp_update_post($post_data, true);

        /* Add Meta Values */
        foreach ($meta_content as $meta_key) {

            if( $_POST[$meta_key] ){
                update_post_meta($post_id, $meta_key, $_POST[$meta_key]);
            }
        }


        if ( wp_redirect( $permalink ) ) {
            exit;
        }
            
    
    }   elseif  ( $_POST['action'] == 'unpublish' )    {
        
        /* Publish / Unpublish */        
            

    }   else   {

        /* Insert Post */
        if ( $_POST['postName'] ) {

            // create post object with the form values
            $postName = $_POST['postName'];
            $childPostType = $_POST['childPostType'];
            $parentId = $_POST['parentId'];
            $relationshipType = $_POST['relationshipType'];

            /* Meta */
            $meta_content = array( 
                'answer-summary',
                'answer-content',
                'answer-video-media',
                'answer-audio-media',
            );



            $post_id = wp_insert_post(array (
                'post_type' => $childPostType,
                'post_title' => $postName,
                'post_content' => "",
                'post_status' => 'publish',
            ));

            // insert the post into the database
            //$cpt_id = wp_insert_post( $my_cptpost_args, $wp_error);
            echo $post_id;


            /* Add Meta Values */
            foreach ($meta_content as $meta_key) {

                if( $_POST[$meta_key] ){
                    add_post_meta($post_id, $meta_key, $_POST[$meta_key], true);
                }
            }


            /*
            *================================================
            *   MB Relationships
            *================================================
            */

            /* Get Current Multi-site ID */
            $blog_id = get_current_blog_id();

            /* MB Relationship Table */
            $mb_relationship_table = 'wp_mb_relationships';

            /* Relation Table to insert into on a multisite */
            if($blog_id != 1){
                $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
            }

            /* Get Clobal Relationship Table Object */
            global $wpdb;

            /* Insert into relationship table */
            $wpdb->insert( 
            $mb_relationship_table, 
                array( 
                    'from' => $parentId, 
                    'to' => $post_id,
                    'type' => $relationshipType,
                ), 
                array( 
                    '%d',
                    '%d',
                    '%s'
                ) 
            );

            if ( wp_redirect( $permalink ) ) {
                exit;
            }

        }
    }

?>   

    <style>
        label{
            font-size: 0.9rem !Important;
            margin-bottom: 0.5em !Important;
        }
    </style>
    <main class="main-content">
        <header class="role-header">
            <div class="container-wrapper padding-t-60 padding-b-50 bg-ocean-7 txt-color-white">
                <div class="row row-20">
                    <div class="col-md-2 padding-lr-20 padding-b-10">
                        <a href="<?php echo $permalink ?>" class="txt-color-white">
                            <i class="fa fa-angle-left padding-r-10"></i>
                            <span class="txt-bold txt-sm uppercase">
                                All Answers
                            </span>
                        </a>
                    </div>
                    <div class="col-md-8 padding-lr-20">
                        <h1 class="txt-xxlg">
                            Contributing to: <?php echo get_the_title( $question_id ); ?>
                        </h1>
                    </div>
                </div>
            </div>
        </header>
        
        <section class="padding-t-40 padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-md-8 col-md-8 mx-auto">
                        
                        <form class="needs-validation" novalidate method="post">
                            <div class=" mb-3">
                                <label for="postName">Title</label>
                                <input 
                                    type="text" 
                                    class="form-control" 
                                    id="postName" name="postName" 
                                    placeholder="" 
                                    required
                                    <?php if($_GET['action'] == 'edit'){ ?>
                                        value="<?php echo get_the_title($answer_id) ?>"
                                    <?php } ?>
                                >
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please enter a Title.
                                </div>
                            </div>
                            <div class=" mb-3">
                                <label for="answer-summary">Short Summary</label>
                                <textarea class="form-control" name="answer-summary" id="answer-summary" cols="30" rows="4"><?php echo rwmb_meta( 'answer-summary', array(), $answer_id ); ?></textarea>

                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please enter a Title.
                                </div>
                            </div>
                            <div class=" mb-3">
                                <label for="answer-content">Full Content</label>
                                <textarea class="form-control" name="answer-content" id="answer-content" cols="30" rows="4"><?php echo rwmb_meta( 'answer-content', array(), $answer_id ); ?></textarea>

                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please enter a Title.
                                </div>
                            </div>
                            <div class=" mb-3">
                                <label for="answer-video-media">Video Link</label>
                                <input type="text" class="form-control" id="answer-video-media" name="answer-video-media" placeholder="" 
                                value="<?php echo rwmb_meta( 'answer-video-media', array(), $answer_id ); ?>"
                                >
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please enter a Title.
                                </div>
                            </div>
                            <div class=" mb-3">
                                <label for="answer-audio-media">Audio Link</label>
                                <input type="text" class="form-control" id="answer-audio-media" name="answer-audio-media" placeholder="" 
                                value="<?php echo rwmb_meta( 'answer-audio-media', array(), $answer_id ); ?>"
                                >
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please enter a Title.
                                </div>
                            </div>
                            
                            <input hidden type="text" name="parentId" value="<?php echo $question_id ?>">
                            <input hidden type="text" name="childPostType" value="answer">
                            <input hidden type="text" name="relationshipType" value="question_to_answer">
                            
                            <?php if($_GET['action']){ ?>
                                <input hidden type="text" name="form_action" value="<?php echo $_GET['action'] ?>">
                                <input hidden type="text" name="edit_id" value="<?php echo $_GET['answer-id'] ?>">
                            <?php } ?>
                            
                            <button class="btn btn-blue txt-sm no-m-b" type="submit">Submit form</button>
                        </form>

                        <script>
                        // Example starter JavaScript for disabling form submissions if there are invalid fields
                        (function() {
                          'use strict';
                          window.addEventListener('load', function() {
                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                            var forms = document.getElementsByClassName('needs-validation');
                            // Loop over them and prevent submission
                            var validation = Array.prototype.filter.call(forms, function(form) {
                              form.addEventListener('submit', function(event) {
                                if (form.checkValidity() === false) {
                                  event.preventDefault();
                                  event.stopPropagation();
                                }
                                form.classList.add('was-validated');
                              }, false);
                            });
                          }, false);
                        })();
                        </script>
                    </div>
                </div>
            </div>
        </section>
    </main>

<?php get_header() ?>


<?php get_footer() ?>