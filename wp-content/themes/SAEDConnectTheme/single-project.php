<?php get_header() ?>
   
<?php
    $current_user = wp_get_current_user();
?>
    
<?php while ( have_posts() ) : the_post(); ?>

<?php
    $post_id = get_the_ID();
    
    /*Get Theme Color*/
    $theme_color = rwmb_meta( 'opportunity-theme' );

    /*Get Theme Alt Color*/
    $theme_alt_color = rwmb_meta( 'opportunity-theme-alt' );

    /* Use User data or admin data */
    $program_images = rwmb_meta( 'change-program-image', array( 'limit' => 1 ) );
    $program_images = reset( $program_images );

    
?>

<main class="main-content-home">
    <header class="bg-change-program" style="background-image: url('<?php echo $program_images['full_url']; ?>')">
        <div class="container-wrapper content">
            <div class="row txt-color-white padding-b-40">
                <div class="col-md-6 mx-auto text-center">
                    <h1 class="txt-xlg txt-height-1-2 margin-b-15 uppercase">
                        <?php echo the_title() ?>
                    </h1>
                    <h2 class="txt-3em txt-height-1-2 margin-b-30">
                        JOIN THE MOVEMENT
                    </h2>
                    <div>
                        <a href="" class="btn btn-blue">
                            Take Action
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--<section class="container-wrapper padding-tb-80">
        <div class="row text-center padding-b-80">
            <div class="col-md-9 mx-auto">
                <h2 class="txt-3em margin-b-20">
                    SKILL-UP CLIMB-UP<br>LEARNING TRACKS
                </h2>
                <p class="txt-color-light txt-height-1-7">
                    This projects involves empowering Nigerians with skills in the following tracks
                </p>
            </div>
        </div>
        <div class="row row-20">
            <a class="col-md-3 padding-lr-20 d-flex" href="">
                <div class="change-round-card">
                    <div class="image">
                        <figure style="background-image: url(images/nav/career_coaching_service.jpg)"></figure>
                    </div>
                    <div class="title">
                        Digital Skills
                    </div>
                </div>
            </a>
            <div class="col-md-3 padding-lr-20 d-flex">
                <div class="change-round-card">
                    <div class="image">
                        <figure style="background-image: url(images/nav/career_coaching_service.jpg)"></figure>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding-lr-20 d-flex">
                <div class="change-round-card">
                    <div class="image">
                        <figure style="background-image: url(images/nav/career_coaching_service.jpg)"></figure>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padding-lr-20 d-flex">
                <div class="change-round-card">
                    <div class="image">
                        <figure style="background-image: url(images/nav/career_coaching_service.jpg)"></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    
    
<?php
    
    $group_values = rwmb_meta( 'content-block-group' );

    /* Set Unique Accordion suffix */
    $accordion_suffix = 0;


    if ( ! empty( $group_values ) ) {
        foreach ( $group_values as $group_value ) {
            
            /* Increment Accordion Suffix */
            $accordion_suffix += 1;

            /* Get Content type */
            $content_type = $group_value['select-content-type'];

            
            if($content_type == 'cb-plain-text-meta-box'){ ?>

            <!-- Text content -->
            <div class="col-md-<?php echo $group_value[ 'select-column-width' ]; ?> padding-lr-40">
                <div class="margin-b-40">

                    <article class="text-box">
                        <?php
                                                          
                            echo $group_value['cb-plain-text-meta-box']['cb-plain-text-content'];
                        ?>
                        
                    </article>
                </div>
            </div>

        <?php } elseif($content_type == 'cp-featured-action-meta-box'){ ?>
        
        <section class="padding-tb-80">
            <div class="row">
                <div class="col-md-9 mx-auto">
                    <h2 class="txt-4em txt-medium txt-bold margin-b-40">
                        <?php echo $group_value['cp-featured-action-meta-box']['cp-featured-action-title']; ?>
                    </h2>
                    <div class="row row-30">
                        <?php
                            $image_ids = isset( $group_value['cp-featured-action-meta-box']['cp-featured-action-image'] ) ? $group_value['cp-featured-action-meta-box']['cp-featured-action-image'] : array();

                            foreach ( $image_ids as $image_id ) {
                                $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                                //echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '">';
                            }
                        ?>
                        <div class="col-md-6 padding-lr-30">
                            <img src="<?php echo $image['full_url']; ?>" alt="">
                        </div>
                        <div class="col-md-6 padding-lr-30">
                            <p class="txt-color-light txt-height-1-7">
                                <?php echo $group_value['cp-featured-action-meta-box']['cp-featured-action-content']; ?>
                            </p>
                            <div class="margin-t-40">
                                <a href="<?php echo $group_value['cp-featured-action-meta-box']['cp-featured-action-cta-link']; ?>" class="btn btn-blue">
                                    <?php echo $group_value['cp-featured-action-meta-box']['cp-featured-action-cta-text']; ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-text-image-meta-box'){ ?>
        
        <section class="full-page-section-change">
            <div class="full-page-section-item">
                <?php
                    $image_ids = isset( $group_value['cp-text-image-meta-box']['cp-text-image-image'] ) ? $group_value['cp-text-image-meta-box']['cp-text-image-image'] : array();
                                                                  
                    foreach ( $image_ids as $image_id ) {
                        $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                        //echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '">';
                    }
                ?>
                <div class="image" style="background-image: url('<?php echo $image['full_url']; ?>')">

                </div>
                <div class="content">
                    <div class="wrapper">
                        <!--<h2 class="parent-name">
                            <a href="">
                                WHY LITHUANIA
                            </a>
                        </h2>-->
                        <h2 class="txt-2em margin-b-40">
                            <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-title']; ?>
                        </h2>
                        <article class="text-box">
                            <?php echo $group_value['cp-text-image-meta-box']['cp-text-image-content']; ?>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } elseif($content_type == 'cp-accordion-meta-box'){ ?>
        
        <section class="row">
            <div class="col-md-6 txt-color-white">
                <div class="bg-blue container-wrapper padding-tb-80">
                    <h2 class="parent-name">
                        <a href="" class="d-block txt-color-white margin-b-15">
                            <?php echo $group_value['cp-accordion-meta-box']['cp-accordion-group-title']; ?>
                        </a>
                    </h2>
                    <h2 class="txt-2em margin-b-40">
                        <?php echo $group_value['cp-accordion-meta-box']['cp-accordion-group-summary']; ?>
                    </h2>
                    <article class="text-box">
                        <p>
                            <?php echo $group_value['cp-accordion-meta-box']['cp-accordion-group-content']; ?>
                        </p>
                    </article>
                </div>
            </div>
            <div class="col-md-6 bg-grey">
                <div class="container-wrapper padding-tb-80">
                    <div class="faq-accordion margin-b-20">
                        <div id="accordion-<?php echo $accordion_suffix; ?>">
                            <?php 
                                $values = $group_value['cp-accordion-meta-box']['cp-accordion'];
                                $accordionCounter = 1;

                                foreach ( $values as $value ) { ?>

                                    <div class="card">
                                        <div class="card-header" id="heading-<?php echo $post->ID.'-'.$accordionCounter; ?>">
                                            <h5 class="mb-0">
                                                <button 
                                                   class="btn btn-link collapsed" 
                                                   data-toggle="collapse" 
                                                   data-target="#collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>" 
                                                   aria-expanded="false" 
                                                   aria-controls="collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>"
                                                >
                                                    <?php echo $value['cp-accordion-title']; ?>
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapse-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>" class="collapse" aria-labelledby="heading-<?php echo $accordion_suffix.'-'.$accordionCounter; ?>>" data-parent="#accordion-<?php echo $accordion_suffix ?>" style="">
                                            <div class="card-body">
                                                <article class="text-box sm">
                                                    <div class="txt-height-1-5">
                                                        <?php echo $value['cp-accordion-content']; ?>
                                                    </div>
                                                </article>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php $accordionCounter++ ?>
                                    
                                <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } ?>
        
    <?php 
        }
    }
?>
        
    
    

    <section class="padding-tb-80">
        <div class="row text-center padding-b-80">
            <div class="col-md-9 mx-auto">
                <h2 class="txt-4em txt-bold txt-color-blue margin-b-20">
                    TAKE ACTION
                </h2>
                <p class="txt-color-light txt-height-1-7">
                    This projects involves empowering Nigerians with skills in the following tracks
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 mx-auto">
            <?php $group_values = rwmb_meta( 'cp-action' ); ?>

            <?php if ( count($group_values) == 1 ){ ?>
            
                <?php 
                    foreach($group_values as $group_value){
                        //print_r($group_value);
                        //echo '<br>';

                        /* Get Image */
                        $image_ids = isset( $group_value['cp-action-image'] ) ? $group_value['cp-action-image'] : array();

                        foreach ( $image_ids as $image_id ) {
                            $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                            //echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '">';
                        }
                ?>

                <!-- One -->
                <div class="row row-30 padding-b-80">
                    <div class="col-md-6 padding-lr-30">
                        <img class="image-shadow" src="<?php echo $image['full_url']; ?>" alt="">
                    </div>
                    <div class="col-md-6 padding-lr-30">
                        <h2 class="txt-2-6em txt-light margin-b-30">
                        <?php echo $group_value['cp-action-title']; ?>
                        </h2>
                        <p class="txt-color-light txt-height-1-7">
                            <?php echo $group_value['cp-action-content']; ?>
                        </p>
                        <div class="margin-t-40">
                            <a href="" class="btn btn-blue">
                               <?php echo $group_value['cp-action-cta-text']; ?>
                            </a>
                        </div>
                    </div>
                </div>
                
                <?php } ?>

            <?php }elseif( count($group_values) <= 2 ){ ?>
                <!-- Two -->
                <div class="row row-20 text-center">
                <?php 
                    foreach($group_values as $group_value){
                        //print_r($group_value);
                        //echo '<br>';

                        /* Get Image */
                        $image_ids = isset( $group_value['cp-action-image'] ) ? $group_value['cp-action-image'] : array();

                        foreach ( $image_ids as $image_id ) {
                            $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                            //echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '">';
                        }
                ?>
                
                    <div class="col-md-6 padding-lr-20">
                        <a href="" class="change-two-card" style="background-image: url('<?php echo $image['full_url']; ?>')">
                            <div class="content padding-o-20 txt-color-white">
                                <h3 class="txt-2em txt-height-1-2 txt-light">
                                    <?php echo $group_value['cp-action-content']; ?>
                                </h3>
                                <p class="txt-sm margin-t-20">
                                    <?php echo $group_value['cp-action-cta-text']; ?>
                                </p>
                            </div>
                        </a>
                    </div>
                
                
                <?php } ?>
                </div>
            <?php }elseif( count($group_values) >= 3 ){ ?>
            
                <?php 
                    foreach($group_values as $group_value){
                        //print_r($group_value);
                        //echo '<br>';

                        /* Get Image */
                        $image_ids = isset( $group_value['cp-action-image'] ) ? $group_value['cp-action-image'] : array();

                        foreach ( $image_ids as $image_id ) {
                            $image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
                            //echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '">';
                        }
                ?>
                
                <?php } ?>

            <?php } ?>
            </div>
        </div>
    </section>

    <div class="section">
        <div class="row padding-b-80">
            <div class="col-md-9 mx-auto bg-blue padding-o-40 txt-color-white">
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <h2 class="txt-2-6em text-center margin-b-80">
                            Support an existing project.
                        </h2>
                        <div class="row row-20">
                            <div class="col-md-5 padding-lr-20 padding-b-40">
                               <h3 class="txt-xxlg uppercase txt-bold margin-b-40">
                                    Fund
                               </h3>
                               <p class="txt-lg margin-b-40">
                                    Fund projects and actions from grassroot development champions taking for  this cause.
                               </p>
                               <div>
                                    <a href="" class="btn btn-trans-wb">
                                        Take Action
                                    </a>
                               </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-5 padding-lr-20 padding-b-40">
                               <h3 class="txt-xxlg uppercase txt-bold margin-b-40">
                                    Fund
                               </h3>
                               <p class="txt-lg margin-b-40">
                                    Fund projects and actions from grassroot development champions taking for  this cause.
                               </p>
                               <div>
                                    <a href="" class="btn btn-trans-wb">
                                        Take Action
                                    </a>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>

<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>