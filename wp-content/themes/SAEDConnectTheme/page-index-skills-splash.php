    <?php /*Template Name: Homepage-Skills-Splash*/ ?>
    
    <?php get_header() ?>
    
    <main class="main-content">
        <div class="splash-header row align-items-center " style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/Banners/02.jpg');">
            <div class="container-wrapper flex_1 txt-color-white">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="txt-3em txt-bold txt-height-1-4 margin-b-20">
                            Skills Splash
                        </h1>
                        <h2 class="txt-height-1-7">
                            Build world-class skills from wherever you are – as long as you have a smart phone.
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        
        <section>
            <div class="row">
                <!-- Main Content -->
                <div class="col-md-8 left-content padding-t-40">
                    <div class="row row-40">
                        <!-- Text content -->
                        <div class="col-md-12 padding-lr-40">
                            <div class="margin-b-40">
                                <h2 class="article-header margin-b-20">
                                    What the Skills Splash program is
                                </h2>
                                <article class="text-box">
                                    <p>
                                        The SAED SKills Splash program offers yuu access to the best skill acquisition & Entrepreneurship Development training, support & opportunities from experts nationwide, right from your phone irrespective of what state or local goverment you are.
                                    </p>
                                </article>
                            </div>
                        </div>

                        <!-- News Cards -->
                        <div class="col-md-6 padding-lr-40 margin-b-30">
                            <a class="featured-article" href="https://skilli.ng/program/entrepreneurship-incubator/?saedconnect=true">
                                <figure class="image-box border-o-1">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/entreprenuership_incubator.jpg" alt="">
                                </figure>
                                <h4 class="title">
                                    The Entrepreneurship Incubator Program
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-6 padding-lr-40 margin-b-30">
                            <a class="featured-article" href="https://skilli.ng/program/side-hustle-skills/?saedconnect=true">
                                <figure class="image-box">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/side_hustle.jpg" alt="">
                                </figure>
                                <h4 class="title">
                                    Side Hustle Skills
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-6 padding-lr-40 margin-b-30">
                            <a class="featured-article" href="https://skilli.ng/program/tech-career-mentorship/?saedconnect=true">
                                <figure class="image-box">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/tech_mentorship.jpg" alt="">
                                </figure>
                                <h4 class="title">
                                    Technology Mentorship Program
                                </h4>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- Right Side Content -->
                <div class="col-md-4">
                    <article class="feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (28).jpg');">
                        <div class="content txt-color-white">
                            <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                                Join the Skills Splash Program
                            </h3>
                            <p class="txt-height-1-5 margin-b-30">
                                Unlock a whole new world of Opportunities from anywhere you are, when you join the Skills Splash Program.
                            </p>
                            <div class="btn-wrapper">
                                <a class="btn btn-trans-wb" href="">
                                    Join
                                </a>
                            </div>
                        </div>
                    </article>
                    <div class="bg-black padding-tb-40">
                        <div class="right-content txt-color-white">
                            <h2 class="txt-xxlg txt-medium padding-b-30">
                                Quicklinks
                            </h2>
                            <article class="text-box sm txt-color-white">
                                <p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground.
                                </p>
                            </article>
                            <ul class="icon-list white">
                                <li>
                                    <a href="">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Programs
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            How the Program works
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            What do you get?
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            FAQs
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            How to join
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>