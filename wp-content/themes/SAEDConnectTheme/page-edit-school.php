    <?php /*Template Name: Show Content*/ ?>
    
    <?php get_header() ?>
    <style type="text/css">
        .um-button, .um-button.um-alt{
            position: relative !important;
            outline: 0 !important;
            font-weight: 500 !important;
            border-radius: 20px !important;
            transition: all 0.4s ease !important;
        }
    </style>

        <main class="main-content font-main">
            <section class="container text-center">
                <h1 class="txt-2em txt-medium margin-b-20">
                    Edit School
                </h1>
            </section>
        <?php
            // TO SHOW THE PAGE CONTENTS
            while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
            
            <section class="padding-tb-40">
                <div class="container">
                    <div class="col-md-8 mx-auto border-o-1 border-color-darkgrey padding-tb-20 padding-lr-30">
                    
                    <?php
                        the_content();
                        
                        /*//page template for "edit-page"
                          //grab entry id from query parameter in link above
                          $entry_id=$_GET['entry'];

                          //grab the entry values via the GF API
                          $entry = GFAPI::get_entry($entry_id);
                          if ( is_wp_error( $entry ) ) {
                               echo "Error"; 
                          } else {

                            //list field, example how to unserialize
                            $clist = maybe_unserialize($entry[2]);
                              var_dump($clist);
                            $cvalue = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($clist)), FALSE);

                            //embed new form and populate the form with normal field and list field
                            //gravity_form(1, false, false, false, array('normalfield1'=>$entry[1],'listfield2'=>$entry[2]), true);

                          } */
                       ?>
                   </div>
               </div>
            </section>
        <?php
            endwhile; //resetting the page loop
            wp_reset_query(); //resetting the page query
        ?>
       

        
    </main>
        
    <?php get_footer() ?>