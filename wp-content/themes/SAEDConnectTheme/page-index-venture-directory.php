<?php /*Template Name: Homepage-Venture Directory*/ ?>
    
<?php get_header() ?>

<main class="main-content">
    <section class="bg-venture-home-hero" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/heroes/table.jpg)">
        <div class="container-wrapper content padding-t-40 padding-b-20">
            <div class="row">
                <div class="col-md-5 txt-color-white">
                    <h1 class="txt-3em txt-height-1-2 txt-bold margin-b-20">
                        Hire freelancers. <br> Make things <br> happen.
                    </h1>
                    <h2 class="txt-lg txt-height-1-7">
                        Grow your business through the top freelancing website. Hire talent nearby or worldwide.
                    </h2>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-grey padding-t-40 margin-b-60">
        <div class="container-wrapper">
            <div class="row row-40">
                <div class="col-md-3 padding-lr-40 margin-b-40 border-r-1 border-color-blue">
                    <div class="txt-xlg txt-height-1-2 margin-b-15">
                        Hire for any scope of work:
                    </div>
                </div>
                <div class="col-md-3 txt-normal-s padding-lr-40 margin-b-40 border-r-1 border-color-blue">
                    <div class="txt-bold txt-color-dark margin-b-10">
                        Short-term tasks
                    </div>
                    <p class="txt-color-light">
                        Build a pool of diverse experts for one-off tasks
                    </p>
                </div>
                <div class="col-md-3 txt-normal-s padding-lr-40 margin-b-40 border-r-1 border-color-blue">
                    <div class="txt-bold txt-color-dark margin-b-10">
                        Recurring projects
                    </div>
                    <p class="txt-color-light">
                        Have a go-to team with specialized skills
                    </p>
                </div>
                <div class="col-md-3 txt-normal-s padding-lr-40 margin-b-40">
                    <div class="txt-bold txt-color-dark margin-b-10">
                        Full-time contract work
                    </div>
                    <p class="txt-color-light">
                        Expand your staff with a dedicated team
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="container-wrapper padding-b-80">
        <div class="row">
            <div class="col-md-8 mx-auto text-center">
                <h2 class="txt-2-4em txt-medium margin-b-60">
                    Build a pool of trusted experts for your team
                </h2>
            </div>
        </div>
        <div class="row row-15 justify-content-center">
        <?php
            $request_type_array = array(
                array(
                    'title' => 'Tool Requests',
                    'url' => 'https://www.saedconnect.org/ventures-directory/tool-requests/',
                    'image_url' => 'line_tool_equipment.png',
                ),
                array(
                    'title' => 'Land Requests',
                    'url' => 'https://www.saedconnect.org/ventures-directory/land-requests/',
                    'image_url' => 'line_land.png',
                ),
                array(
                    'title' => 'Workspace Requests',
                    'url' => 'https://www.saedconnect.org/ventures-directory/workspace-requests/',
                    'image_url' => 'line_workspace.png',
                ),
                array(
                    'title' => 'Volunteer Requests',
                    'url' => 'https://www.saedconnect.org/ventures-directory/volunteer-requests/',
                    'image_url' => 'line_collaborator_innovator.png',
                ),
                array(
                    'title' => 'Funding Requests',
                    'url' => 'https://www.saedconnect.org/ventures-directory/funding-requests/',
                    'image_url' => 'line_funding.png',
                ),
                array(
                    'title' => 'Marketing Requests',
                    'url' => 'https://www.saedconnect.org/ventures-directory/marketing-requests/',
                    'image_url' => 'line_marketing.png',
                ),
                array(
                    'title' => 'Business Partner Requests',
                    'url' => 'https://www.saedconnect.org/ventures-directory/business-partner-requests/',
                    'image_url' => 'line_business_partner.png',
                ),
                array(
                    'title' => 'Mentorship Requests',
                    'url' => 'https://www.saedconnect.org/ventures-directory/mentor-requests/',
                    'image_url' => 'line_stories.png',
                ),  
            );


            foreach( $request_type_array as $request_item ){ 
        ?>
            
            <div class="col-6 col-md-3 padding-lr-15 padding-b-30">
                <a class="venture-resource-card" href="<?php echo $request_item['url'] ?>">
                    <figure>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/requests/<?php echo $request_item['image_url'] ?>" alt="">
                    </figure>
                    <span class="title">
                        <?php echo $request_item['title'] ?>
                    </span>
                </a>
            </div>

        <?php } ?>
        </div>
    </section>

    <section class="container-wrapper bg-grey padding-t-80 padding-b-40 text-center">
        <h2 class="txt-2-4em txt-medium margin-b-60">
            How it works
        </h2>
        <div class="row row-20">
            <div class="col-md-3 padding-lr-40 padding-b-40">
                <div class="margin-b-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="" width="80">
                </div>
                <h3 class="txt-bold margin-b-10">
                    Post a job (it’s free) 
                </h3>
                <p class="txt-sm txt-height-1-7">
                    Tell us about your project. Upwork connects you with top talent around the world, or near you. 
                </p>
            </div>
            <div class="col-md-3 padding-lr-40 padding-b-40">
                <div class="margin-b-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_build.png" alt="" width="80">
                </div>
                <h3 class="txt-bold margin-b-10">
                    Freelancers come to you
                </h3>
                <p class="txt-sm txt-height-1-7">
                    Tell us about your project. Upwork connects you with top talent around the world, or near you. 
                </p>
            </div>
            <div class="col-md-3 padding-lr-40 padding-b-40">
                <div class="margin-b-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_amazing_support_network.png" alt="" width="80">
                </div>
                <h3 class="txt-bold margin-b-10">
                    Payment simplified
                </h3>
                <p class="txt-sm txt-height-1-7">
                    Tell us about your project. Upwork connects you with top talent around the world, or near you. 
                </p>
            </div>
            <div class="col-md-3 padding-lr-40 padding-b-40">
                <div class="margin-b-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_challenge_the_status_quo.png" alt="" width="80">
                </div>
                <h3 class="txt-bold margin-b-10">
                    Collaborate easily
                </h3>
                <p class="txt-sm txt-height-1-7">
                    Tell us about your project. Upwork connects you with top talent around the world, or near you. 
                </p>
            </div>
        </div>
    </section>

    <section class="container-wrapper padding-tb-80">
        <h3 class="txt-lg txt-medium margin-b-40">
            Top skills
        </h3>
        <div class="row row-20">
        <?php 
            /* Return Terms assigned to Post */
            $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

            /*
            *
            * Populate Form Data from Terms
            *
            */
            //Get Terms
            $terms = get_terms( 'service-category', array('hide_empty' => false));

            foreach ($terms as $term) { //Cycle through terms, one at a time

                // Check and see if the term is a top-level parent. If so, display it.
                $parent = $term->parent;
                $term_id = $term->term_id; //Get the term ID
                $term_name = $term->name; //Get the term name
        ?>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="<?php echo get_term_link($term_id); ?>">
                    <?php echo $term_name; ?>
                </a>
            </div>
            
        <?php } ?>
        </div>
    </section>
</main>
   
<?php get_footer() ?>