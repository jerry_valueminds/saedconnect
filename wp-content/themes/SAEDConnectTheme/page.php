<?php /*Template Name: General*/ ?>

<?php get_header() ?>
    
    <main class="main-content">
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="container-wrapper padding-tb-40">
            <?php the_content() ?>
        </div>
        <?php endwhile; // end of the loop. ?>
    </main>

<?php get_footer() ?>