    <?php /*Template Name: Homepage-Learn-a-Skill*/ ?>
    
    <?php get_header() ?>
   
    <main class="main-content">
        <header class="course-directory-banner image margin-b-40" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (41).jpg');">
            <div class="content container-wrapper">
                <h1 class="title">
                    Learn from industry experts, connect with a global network of experience.
                </h1>
            </div>
        </header>
        <section class="container-wrapper padding-b-40">
            <header class="margin-b-20">
                <h2 class="txt-medium uppercase">Top Classroom Courses</h2>    
            </header>
            <div class="row row-10">
            <?php // Display posts
                $temp = $wp_query; $wp_query= null;
                $wp_query = new WP_Query();
                $wp_query->query(array('post_type' => 'course'));
                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                    <div class="col-md-3 padding-lr-10 padding-b-20">
                    <a class="course-directory-card" href="<?php the_permalink() ?>">
                        <article class="content">
                            <figure class="image-box">
                                <img src="images/heroes/honda_1.jpg" alt="">
                                <?php if (has_post_thumbnail()) { ?>

                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>" alt="">

                                <?php } else { ?>

                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.jpg" alt="">

                                <?php } ?>
                                
                                <?php if ( has_term( 'virtual-class', 'model' ) ) {?>
                                    <h4 class="model">Virtual Class</h4>
                                <?php } elseif ( has_term( 'blended', 'model' ) ) {?>
                                    <h4 class="model">Blended Class</h4>
                                <?php } elseif ( has_term( 'classroom', 'model' ) ) {?>
                                    <h4 class="model">Classroom</h4>
                                <?php } elseif ( has_term( 'fully-online', 'model' ) ) {?>
                                    <h4 class="model">Fully Online Class</h4>
                                <?php } ?>
                            </figure>
                            <div class="course-info">
                                <h3 class="title"><?php the_title() ?></h3>
                                <h4 class="instructor">
                                    <span class="const">BY:</span>
                                    <span class="name">Maero Uwede</span>
                                </h4>
                                <p class="skills txt-sm">
                                    <strong class="txt-bold">Skills:</strong>
                                    Skill 1, Skill 2, Skill 3,
                                </p>
                                <div class="footer">
                                    <div class="level">
                                        <?php if ( has_term( 'beginner', 'level' ) ) {?>
                                            Advanced
                                        <?php } elseif ( has_term( 'intermediate', 'level' ) ) {?>
                                            Intermediate
                                        <?php } elseif ( has_term( 'advanced', 'level' ) ) {?>
                                            Advanced
                                        <?php } ?>
                                    </div>
                                    <div class="duration">
                                        6 Hours
                                    </div>
                                </div>
                            </div>
                        </article>  
                    </a>   
                </div>
            <?php
                endwhile; 
                wp_reset_postdata();
                wp_reset_query();
            ?>
            </div>
        </section>
        <section class="container-wrapper bg-ghostwhite padding-tb-40">
            <header class="margin-b-20">
                <h2 class="txt-medium uppercase">Browse Industry Categories</h2>    
            </header>
            <div class="row row-10 text-center">
                <?php 
                    $terms = get_terms( 'industry', array('hide_empty' => false,)); //Get all the terms

                    foreach ($terms as $term) { //Cycle through terms, one at a time

                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        if ( $parent=='0' ) {

                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                            $term_url = get_term_link($term);
                ?>
                
                    <div class="col-md-2 industry-card padding-lr-10 padding-b-20">
                        <a href="<?php echo site_url(); ?>/?industry-id=<?php echo $term_id ?>&post-type=course&search-type=course&s=">
                            <span>
                                <?php echo $term_name; ?>
                            </span>
                        </a>   
                    </div>

                <?php
                        } 
                    }
                ?>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>
    