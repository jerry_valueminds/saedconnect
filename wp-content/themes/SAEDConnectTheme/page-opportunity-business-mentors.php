    <?php /*Template Name: Business Mentor Programs */ ?>
        
    <?php get_header() ?>
    
    <style>
        .faq-accordion .btn-link:before, .faq-accordion .btn-link:after {
            background-color: white;
        }
        
        .faq-accordion .btn-link, .icon-list li a i, .icon-list li a span {
            color: white !important;
        }
    </style>
   
    <main class="main-content">
       
        <!-- Intro -->
        <section class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h2 class="d-inline-block bg-blue txt-color-white txt-xlg emtxt-bold padding-o-10 margin-b-20">
                        #MentorCall
                    </h2>
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                         Call for Business-Specific Mentors
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        We are looking for amazing young entrepreneurs, nationwide, who are engaged in any of the small Business listed below, and can mentor other aspiring entrepreneurs to start and grow the business.
                    </h2>
                </div>
            </div>
        </section>
        
        <!-- Business Ideas -->
        <section class="container-wrapper">
            <div class="margin-b-40 text-center">
                <h2 class="txt-2em txt-light txt-height-1-1">
                    Select a business below to apply to be a mentor for that business
                </h2>
            </div>
        </section>
        <div class="bg-grey padding-tb-40">
                <!-- Agro-related Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-1" aria-expanded="false" aria-controls="main-1">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Agro-related & Livestock Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-1" class="collapse bg-blue" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-bee-keeping-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Bee Keeping 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-fish-farming-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Fish Farming 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-forrabbit-farming-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Rabbit Farming  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-mentors-2/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Poultry Farming Mentors  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-piggery-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Piggery   
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-snail-farming-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Snail Farming    
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-commercial-farming-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Commercial Farming    
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-pet-breeding-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Pet Breeding  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-wholesale-retail-of-agro-production-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Wholesale & Retails Of Agro Produce
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Lifestyle, Textile, Fashion & Beauty Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-2" aria-expanded="false" aria-controls="main-2">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Lifestyle, Textile, Fashion & Beauty Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-2" class="collapse bg-blue" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-barbing-hairdressing-beauty-services-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Barbing, Hairdressing & Beauty Services 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-shoebag-making-leatherworks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Shoe/Bag Making & Leatherworks  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-makeup-artistry-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Makeup Artistry 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-tailoring-and-fashion-design-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Tailoring and Fashion Design 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-tye-dye-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Tie & Dye    
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-cosmetology-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Cosmetology    
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-fashion-accessories-jewelry-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Fashion Accessories & Jewelry    
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-clothing-wholesaleretail-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                Wholesale/Retail of Clothing (Boutique) 
                                            </span>
                                        </a>
                                    </li>
                                     <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-laundry-drycleaning-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Laundry & Drycleaning 
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Financial Services Busineses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-3" aria-expanded="false" aria-controls="main-3">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Financial Services Busineses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-3" class="collapse bg-blue" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-forex-crypto-currency-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Forex & Crypto Currency
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Building & Construction-related Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-4" aria-expanded="false" aria-controls="main-4">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Interior/Exterior Design, Building & Construction-related Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-4" class="collapse bg-blue" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-carpentry-and-furniture-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Carpentry and Furniture 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-paint-production-sales-services-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Paint Production, Sales & Services 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-building-materials-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Building Materials Wholesale/Retail
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-horticulture-landscaping-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Horticulture & Landscaping 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-interior-design-accessories-saleinstallation-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Interior Design & Accessories Sale/Installation
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Rentals & Logistics Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-5" aria-expanded="false" aria-controls="main-5">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Rentals & Logistics Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-5" class="collapse bg-blue" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-logistics-transportation-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Logistics & Transportation 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Food Delivery Business  
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Medical & Other Services -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-6" aria-expanded="false" aria-controls="main-6">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Services Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-6" class="collapse bg-blue" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-freelancing-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Freelancing
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-event-management-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Event Management 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-waste-management-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Waste Management  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-horticulture-landscaping-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Horticulture & Landscaping 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-pest-control-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Pest Control 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-education-training-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Education & Training 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-printing-publishing-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Printing & Publishing 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-laundry-drycleaning-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Laundry & Drycleaning 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-photography-cinematography-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Photography & Cinematography 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-writingblogging-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Writing/Blogging
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-interior-design-accessories-saleinstallation-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Interior Design & Accessories Sale/Installation
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-art-craft-metal-works-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Art, Craft & Metal Works 
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Technology & Entertainment Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-7" aria-expanded="false" aria-controls="main-7">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Technology & Entertainment Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-7" class="collapse bg-blue" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-music-dj-sound-engineering-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Music, DJ & Sound Engineering 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-technology-services-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Technology Services 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Online Selling 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-event-management-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Event Management 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-printing-publishing-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Printing & Publishing 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-photography-cinematography-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Photography & Cinematography 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-freelancing-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Freelancing
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-forex-crypto-currency-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Forex & Crypto Currency
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Trade, Wholesale / Retail Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-8" aria-expanded="false" aria-controls="main-8">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Trade, Wholesale / Retail Businesses (Online & Offline)
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-8" class="collapse bg-blue" aria-labelledby="heading-1" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-importexport-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Import/Export 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-building-materials-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Building Materials Wholesale/Retail
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-spare-parts-automobile-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Spare Parts & Automobile Business
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-books-stationery-office-supplies-shop-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Books, Stationery & Office Supplies Shop
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-supermarketshop-operation-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Supermarket/Shop Operation
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Online Selling 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-clothing-wholesaleretail-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                Wholesale/Retail of Clothing (Boutique) 
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-wholesale-retail-of-agro-production-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Wholesale & Retails Of Agro Produce
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <!-- Medical Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-9" aria-expanded="false" aria-controls="main-9">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Medical Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-9" class="collapse bg-blue" aria-labelledby="heading-9" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-pharmaceuticals-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Pharmacy Business 
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <!-- Food & Drinks -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-10" aria-expanded="false" aria-controls="main-10">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Food & Drinks Business
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-10" class="collapse bg-blue" aria-labelledby="heading-10" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Restaurant Business
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Bar Business
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Outdoor Catering
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Drinks & Confetionary Business  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Food Preservation Business  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Food Delivery Business  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Food Processing Business
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Wine & Drinks Reseller  
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-food-drinks-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Any other variant of the Food & Drinks Business
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <!-- Art & Crafts Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-11" aria-expanded="false" aria-controls="main-11">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Art & Crafts Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-11" class="collapse bg-blue" aria-labelledby="heading-11" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-art-craft-metal-works-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Drawing & Painting
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-art-craft-metal-works-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Sculpture
                                            </span>
                                        </a>
                                    </li>
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-art-craft-metal-works-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Ceramic & Pottery
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <!-- Power & Energy Businesses -->
            <section class="container-wrapper faq-accordion margin-b-10">
                <div class="container-wrapper-40 bg-ash  padding-tb-20 border-b-1 border-color-white">
                    <button class="btn-link collapsed txt-lg txt-medium" data-toggle="collapse" data-target="#main-12" aria-expanded="false" aria-controls="main-12">
                        <span class="row align-items-center">
                            <span class="padding-l-10">
                                Power & Energy Businesses
                            </span>
                        </span>
                    </button>
                </div>
                <div id="main-12" class="collapse bg-blue" aria-labelledby="heading-12" data-parent="#accordion">
                    <div class="container-wrapper-40 padding-tb-30">
                        <div class="row padding-l-40">
                            <div class="col-md-7">
                                <ul class="icon-list txt-sm row row-15">
                                    <li class="col-6 col-md-4 padding-lr-15">
                                        <a href="https://www.saedconnect.org/opportunity-center/opportunity/call-for-solar-inverter-business-mentors/">
                                            <i class="fa fa-chevron-right txt-color-dark"></i>
                                            <span class="txt-color-dark">
                                                 Solar & Inverter   
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        
        
        
        
    
    </main>
    
    
<?php get_footer() ?>


<?php
    $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

    if($incoming_from_saedconnect){
?>

   <script>
        $(document).ready(function(){
            /*
            *   Auto populate form with Track ID & Name
            */
            $('#formModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var track_id = button.attr('track-id'); // Extract Track ID
                var track_name = button.attr('track-name'); // Extract Track Name

                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);

                //modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('#input_1_6').val(track_id);
                modal.find('#input_1_7').val(track_name);
            })


            //Confirmation Modal
            $('#formConfirmationModal').modal('show')

            //Clear URL
           // var uri = window.location.toString();
            //if (uri.indexOf("?") > 0) {
                //var clean_uri = uri.substring(0, uri.indexOf("?"));
                //window.history.replaceState({}, document.title, clean_uri);
            //}

            //Append Saedconnect Query string
            $('a').each(function(index, element) {
              var newAttr = $(element).attr('href') + '?saedconnect=true';
              $(element).attr('href', newAttr);
            });
        });
    </script>

<?php } ?>