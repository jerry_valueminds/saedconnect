<?php /*Template Name: Profile - User CV*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* Publication */
        $publication_key   = 'publication_status';
        $accreditation_key   = 'nysc_accreditation_status';
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Get Avatar */
        $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
        $meta_key = 'user_avatar_url';
        $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

        if($get_avatar_url){
            $avatar_url = $get_avatar_url;
        }

        /* Get Trainer profile */
        $user_id = $_GET['user-id'];
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <?php get_header() ?>
    
    <main class="main-content txt-color-light bg-white">
        <?php get_template_part( 'template-parts/user-dashboard/_dashboard_strip' ); ?>
        <section class="row">
        <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav-user-profile' ); ?>
            <div class="dashboard-multi-main-content">
                <div class="page-header">
                    <h1 class="page-title">
                        My CV
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Your Personal Profile is the Key information about you which would be required by potential employers
                    </p>
                </article>
                <div>

                    <!-- Personal Information -->
                    <div class="section-wrapper">
                    <?php
                        $gf_id = 84; //Form ID
                        $gv_id = 1116; //Gravity View ID
                        $title = 'Personal Information';

                        $entry_count = 0;

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $user_id, //Current logged in user
                                )
                            )
                        );

                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria ); 
                    ?>

                        <?php if(!$entry_count){ //If no entry ?>

                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        <?php echo $title ?>
                                    </h2>
                                </div>
                                <div class="entry">
                                    <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                        No information available.
                                    </h3>
                                </div>

                        <?php } else { ?>
                            <?php foreach( $entries as $entry ){ ?>

                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        <?php echo $title ?>
                                    </h2>
                                </div>
                                <div class="entry">
                                    <div class="row row-10">
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Gender          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 2 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Yeah of Birth          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 3 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Marital Status          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 21 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Nationality          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 4 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                State of Origin          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 5 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Location in Nigeria where you currently reside          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 6 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                States in Nigeria where you can work
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 7; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Preferred Contact Telephone Number (For Work)          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 8 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Preferred Contact Whatsapp Number (For Work)          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 9 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Preferred Contact Email Address (For Work)         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 10 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Highest Level of Education          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 11 ); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                        <?php } ?>
                    </div>

                    <!-- Education & Training -->
                    <div class="section-wrapper">
                    <?php
                        $gf_id = 31; //Form ID
                        $gv_id = 1128; //Gravity View ID
                        $title = 'Education & Training';

                        $entry_count = 0;

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $user_id, //Current logged in user
                                )
                            )
                        );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );   

                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                    ?>
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>

                        <?php if(!$entry_count){ //If no entry ?>

                            <div class="entry">
                                <h3 class="txt-color-dark txt-sm margin-t-10 margin-b-20">
                                    No information available.
                                </h3>>
                            </div>

                        <?php } else { ?>
                            <?php foreach( $entries as $entry ){ ?>

                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Degree Attained (e.g BA, BS, JD, PhD)         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            School      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 8 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 9 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Start Month/Year      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 10 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            End Month/Year (Current students: Enter your expected graduation year)     
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 11 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Achievements      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 12 ); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <?php } ?>
                        <?php } ?>
                    </div>

                    <!-- Interests -->
                    <div class="section-wrapper">
                    <?php
                        $gf_id = 39; //Form ID
                        $gv_id = 1132; //Gravity View ID
                        $title = 'Interests';

                        $entry_count = 0;

                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $user_id, //Current logged in user
                                )
                            )
                        );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                    ?>
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>

                        <?php if(!$entry_count){ //If no entry ?>

                            <div class="entry">
                                <h3 class="txt-color-dark txt-sm margin-t-10 margin-b-20">
                                    No information available.
                                </h3>
                            </div>

                        <?php } else { ?>
                            <?php foreach( $entries as $entry ){ ?>

                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Title        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Description    
                                        </p>
                                        <article class="text-area txt-sm">
                                            <?php echo rgar( $entry, 4 ); ?>
                                        </article>
                                    </div>
                                </div>
                            </div>

                            <?php } ?>
                        <?php } ?>
                    </div>

                    <!-- Languages -->
                    <div class="section-wrapper">
                    <?php
                        $gf_id = 40; //Form ID
                        $gv_id = 1134; //Gravity View ID
                        $title = 'Languages';

                        $entry_count = 0;

                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $user_id, //Current logged in user
                                )
                            )
                        );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                    ?>
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>

                        <?php if(!$entry_count){ //If no entry ?>

                            <div class="entry">
                                <h3 class="txt-color-dark txt-sm margin-t-10 margin-b-20">
                                    No information available.
                                </h3>
                            </div>

                        <?php } else { ?>
                            <?php foreach( $entries as $entry ){ ?>

                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-4 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Language        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-4 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Spoken Level    
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 2 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-4 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Written Level    
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 3 ); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <?php } ?>
                        <?php } ?>
                    </div>

                </div>
            </div>
            <div class="dashboard-multi-main-sidebar">
               
                <?php if( is_user_logged_in() ){ ?>
                    <div class="margin-t-20">
                        <a data-toggle="modal" href="#messageModal" class="btn btn-blue w-100 txt-normal-s">
                           <span class="padding-r-5">Contact Me</span>
                           <i class="fa fa-envelope-o"></i>
                        </a>
                    </div>
                <?php } ?>
                
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>

<!-- iCheck -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/_all.css" integrity="sha256-808LC4rdK/cu4RspCXPGrLKH7mgCcuNspF46UfBSbNQ=" crossorigin="anonymous" />

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>