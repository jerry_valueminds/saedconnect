<?php /*Template Name: Profile - Competency - Create*/ ?>
   
<?php

    if ( !is_user_logged_in() ) {
        // If User is Logged in, redirect to User Dashbord
        $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID

        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }

?>

<?php get_header() ?>

<?php
    /* Variable to check profile completion */
    $check = array();

    /* Template info */
    $pages_array = array(
        array(
            'name' => 'Contact Information',
            'template' => 'contact-information',
        ),
        array(
            'name' => 'Work Profile Snapshot',
            'template' => 'work-profile-snapshot',
        ),
        array(
            'name' => 'Education & Work Experience',
            'template' => 'education-and-work-experience',
        ),
        array(
            'name' => 'Skills and Capabilities',
            'template' => 'skills-and-capabilities',
        ),
        array(
            'name' => 'Languages and Interests',
            'template' => 'languages-and-interests',
        ),
        array(
            'name' => 'Certifications and Affiliations',
            'template' => 'certifications-and-Affiliations',
        ),
        array(
            'name' => 'Review your submission',
            'template' => 'review',
        ),
    );
    $max_page_step = count($pages_array);

    /* Select Page View Request */
    $get_page_step = intVal($_GET['step']);
    if( 
        $get_page_step 
        && ($get_page_step >= 1) 
        && ($get_page_step <= $max_page_step) 
    ){
        $page_step = $get_page_step; 
    } else {
        $page_step = 1;
    }

    /* Get User */
    $current_user = wp_get_current_user();
?>

<?php 
    /* Get Image Field */
    function get_the_image_url($post_id){
        $images = get_attached_media( 'image', $post_id );
        $previousImg_id = 0;
        if($images){
            foreach($images as $image) { 
                $previousImg_id = $image->ID;
            }
            return wp_get_attachment_url($previousImg_id,"full");
        } else {
            return 0;
        }
    }
?>

<main class="main-content" style="padding-top: 70px;margin-top:0;background-color: #e0e0e0">
    <section class="container-wrapper padding-t-80 padding-b-40">
        <!-- Title / Image -->
        <header class="col txt-xlg txt-height-1-2 txt-color-dark">
            <h1 class="txt-light margin-b-5">Welcome, <?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?></h1>
            <h2 class="txt-bold margin-b-5">Complete your competency profile</h2>
        </header>
        <!--<p class="txt-normal-s">Deadline: 25th August 2019</p>-->
    </section>

    <!-- Page Info -->   
    <section class="container-wrapper padding-b-40">
        <div class="bg-ash padding-o-30">
            <h2 class="txt-bold txt-color-dark padding-b-15">Instruction</h2>
            <p class="txt-sm txt-color-white">Provide all the information requested below to complete your application for this Job. You will only be able to submit this application after you have provided all the requested information</p>
        </div>
    </section>
    
    <!-- Template Title -->
    <section class="container-wrapper padding-b-10">
        <h3 class="txt-lg txt-medium txt-color-light">
            <?php echo $pages_array[$page_step - 1]['name']; ?>
        </h3>
    </section>

    <!-- Steps -->
    <section class="container-wrapper padding-b-40">
        <ul class="steps-list">
        <?php for($i = 1; $i <= $max_page_step; $i++){ ?>
            <li class="<?php echo ($page_step >= $i ) ? 'complete' : ''; ?> <?php echo ($page_step == $i ) ? 'active' : ''; ?>">
               <a href="<?php echo currentUrl(true).'/?step='.$i ?>">
                    <span class="innertext">
                    <?php if( !($i == $max_page_step) ){ ?>
                        <span class="name">Step</span>
                        <span class="number"><?php echo $i ?></span>
                    <?php }else{ ?>
                        <span class="number">Review</span>
                    <?php } ?>
                    </span>
                </a>
            </li>
        <?php } ?>
        </ul>
    </section>

    <!-- General Information -->  
    <section class="container-wrapper padding-b-10">
        <?php include( locate_template( 'template-parts/competency/'.$pages_array[$page_step - 1]['template'].'.php', false, false ) ); ?>
    </section>

    <?php if (in_array("incomplete", $check)){ ?>
        <!-- Missing Content Info -->   
        <section class="container-wrapper padding-t-20 padding-b-40">
            <div class="row align-items-center padding-o-20 border-o-1 border-color-lighter">
                <div class="col-auto txt-lg">
                    <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x txt-color-white"></i>
                        <i class="fa fa-info fa-stack-1x txt-color-yellow"></i>
                    </span>
                </div>
                <p class="col txt-normal-s padding-l-15">
                    You need to complete your <?php echo $pages_array[ ($page_step - 1) ]['name'] ?> before you can proceed.
                </p>
            </div>
        </section>
    <?php } ?>

    <!-- Submit-->   
    <section class="container-wrapper padding-t-20 padding-b-40">
        <div class="d-flex justify-content-between">
            <div>
                <?php if( $page_step > 1 ){ ?>

                    <!--<span class="btn btn-disabled txt-normal-s">Previous</span>-->
                    <a href="<?php echo currentUrl(true).'/?step='.($page_step - 1) ?>" class="btn btn-blue txt-normal-s">Previous</a>

                <?php } ?>

                <?php if( $page_step < $max_page_step ){ ?>

                    <?php if (in_array("incomplete", $check)){ ?>
                        <span class="btn btn-disabled txt-normal-s">Next</span>
                    <?php }else{ ?>
                        <a href="<?php echo currentUrl(true).'/?step='.($page_step + 1) ?>" class="btn btn-blue txt-normal-s">Next</a>
                    <?php } ?>

                <?php } ?>
            </div>
            <div>
                <?php if( $page_step == $max_page_step ){ ?>
                    <a 
                        href="https://www.saedconnect.org/competency-profile/competency-profile-confirmation/" 
                        class="btn btn-ash txt-normal-s"
                    >Submit Profile</a>
                <?php } ?>
            </div>
        </div>
    </section>
</main> 

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>


<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>