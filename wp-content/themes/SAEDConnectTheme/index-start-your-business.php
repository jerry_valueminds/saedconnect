<?php /*Template Name: Homepage - Start your business*/ ?>
    

<?php get_header() ?>
   
    <main class="main-content">
        <div class="container-wrapper padding-t-40"> 
            <div class="color-hero bg-green txt-color-white">
                <div class="content">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="txt-4em txt-bold margin-b-20">
                                Start / Grow your business
                            </h1>
                            <h2 class="txt-height-1-7 txt-lg">
                                Make money on your own terms. Get help to overcome your fears, start any business of your choice, and grow your small business into an empire.
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="bg-green-lime container-wrapper padding-tb-20">
                <a class="d-flex row txt-xlg txt-color-dark" data-toggle="modal" href="#shcCommunities">
                    <h4 class="col-8 txt-bold">
                        Side Hustle Communities
                    </h4>
                    <div class="col-4 text-right">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                </a>
                <p class="txt-normal-s margin-t-10">
                    Meet with fellow Entrepreneurs accross over 40 businesses nationwide.
                </p>
            </div>

            <!-- General Section -->
            <section>
                <div class="row">
                    <div class="col-md-4 d-flex">
                        <article class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (10).jpg');">
                                <div class="content txt-color-white">
                                    <h3 class="txt-xxlg txt-medium margin-b-10">
                                        The Entrepreneurship Incubator
                                    </h3>
                                    <p class="txt-sm margin-b-30">
                                        Not sure if entrepreneurship is right for you? Looking for a great business idea? Want to turn your skill into money? Want to take your idea to the next level? Need to build a business plan? Get into the Entrepreneurship Incubator
                                    </p>
                                    <a class="btn btn-trans-wb" href="http://www.saedconnect.org/growth-programs/program/the-entrepreneurship-incubator/">
                                        Get in
                                    </a>
                                </div>   
                        </article>
                    </div>
                    <div class="col-md-4 d-flex">
                        <article class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (16).jpg');">
                            <div class="content txt-color-white">
                                <h3 class="txt-xxlg txt-medium margin-b-10">
                                    Business Clinic
                                </h3>
                                <p class="txt-sm margin-b-30">
                                    Want to raise finance for your business? Need help with business registration and legal? Need more customers for your business? Having issues with partnerships or team building? Join the business clinic.
                                </p>
                                <a class="btn btn-trans-wb" href="http://www.saedconnect.org/growth-programs/program/business-clinic/">
                                    Get in
                                </a>
                            </div>   
                        </article>
                    </div>
                    <!--<div class="col-md-4 d-flex">
                        <article class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (35).png');">
                            <div class="content txt-color-white">
                                <h3 class="txt-xxlg txt-medium margin-b-10">
                                    Small Business Mentor
                                </h3>
                                <p class="txt-sm margin-b-30">
                                    Get all the help you need to start and grow any of 40+ Businesses types. Ask questions from experts, meet potential partners, discover tips & tricks, and much more
                                </p>
                                <a class="btn btn-trans-wb" href="https://www.saedconnect.org/growth-programs/side-hustle-Skills/">
                                    Connect
                                </a>
                            </div>   
                        </article>
                    </div>-->
                    <div class="col-md-4 d-flex">
                        <article class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (35).png');">
                            <div class="content txt-color-white">
                                <h3 class="txt-xxlg txt-medium margin-b-10">
                                    Side Hustle Mentor Hub
                                </h3>
                                <p class="txt-sm margin-b-30">
                                    Connect with expert mentors to help you start and grow any of 40+ different side hustles.
                                </p>
                                <a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
                                    Connect
                                </a>
                            </div>   
                        </article>
                    </div>
                </div>
            </section>
        </div>
        <div class="container-wrapper">
            <div class="padding-tb-60">
                <h4 class="txt-xxlg txt-bold txt-height-1-2 margin-b-60">
                    Hire a Business Support Expert
                </h4>
                <div class="support-list">
                    <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-green" href="https://www.saedconnect.org/about/reference/business-entrepreneurship-support-services/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        Business Counseling
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Need help with thinking through your business? Our consultants are available to support you
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-green d-none d-md-inline" href="https://www.saedconnect.org/about/reference/business-entrepreneurship-support-services/">
                                    <i class="fa fa-long-arrow-right padding-r-5"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-green" href="https://www.saedconnect.org/about/discourse/business-plan-creation/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        Business Plan Creation
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Lets help you create a great business plan to help you win that funding and/or help you stay on track.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-green d-none d-md-inline" href="http://www.saedconnect.org/about/discourse/business-plan-creation/">
                                    <i class="fa fa-long-arrow-right padding-r-5"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-green" href="https://www.saedconnect.org/about/discourse/raise-finance-for-your-business/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        Raise Finance for your Business
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Get a great logo, business card, brand collateral designs, business brochure and website for your new business.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-green d-none d-md-inline" href="http://www.saedconnect.org/about/discourse/raise-finance-for-your-business/">
                                    <i class="fa fa-long-arrow-right padding-r-5"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-green" href="https://www.saedconnect.org/about/discourse/business-registrations/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        Business Registration & Other Legal Services
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Let our professionals help you register your business, handle standard compliance registrations and provide all your legal needs.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-green d-none d-md-inline" href="http://www.saedconnect.org/about/discourse/business-registrations/">
                                    <i class="fa fa-long-arrow-right padding-r-5"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-green" href="https://www.saedconnect.org/about/discourse/business-branding/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        Business Branding
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Get a great logo, business card, brand collateral designs, business brochure and website for your new business.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-green d-none d-md-inline" href="http://www.saedconnect.org/about/discourse/business-branding/">
                                    <i class="fa fa-long-arrow-right padding-r-5"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-wrapper text-center padding-b-20">
            <div class="bg-grey padding-tb-40 padding-lr-20">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <h3 class="txt-medium txt-xlg margin-b-15">
                            Interested in getting a job instead?
                        </h3>
                        <p class="txt-normal-s">
                            Build, showcase & verify your competency - and get connected to a large pool of employers looking to hire you.
                        </p>
                        <div class="margin-t-30">
                            <a class="btn btn-blue no-m-b" href="https://www.saedconnect.org/get-your-job">
                                Get your Job
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
<!-- SHC Modal -->
<?php get_template_part( 'template-parts/_shc_modal' ); ?>    

<!-- SHC Free Communuties Modal -->
<?php get_template_part( 'template-parts/_shc_free_modal' ); ?>
    
<?php get_footer() ?>