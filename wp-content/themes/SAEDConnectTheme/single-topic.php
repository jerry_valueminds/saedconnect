<?php get_header() ?>

<?php 
    while ( have_posts() ) : the_post(); 
        /* Get Current Topic URL */
        $topic_url = get_permalink();
?>

<main class="main-content bg-grey">
    <div class="container-wrapper">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <?php bbp_breadcrumb(); ?>
            </div>
        </div>
    </div>

    <?php do_action( 'bbp_template_before_single_topic' ); ?>

    <?php if ( post_password_required() ) : ?>

    <?php bbp_get_template_part( 'form', 'protected' ); ?>

<?php else : ?>
   
   <!-- Topic Header -->
    <?php if ( bbp_show_lead_topic() ) : ?>

        <?php bbp_get_template_part( 'content', 'single-topic-lead' ); ?>

    <?php endif; ?>

    
        <!-- Header -->
        <!--<div class="container-wrapper">
            <div class="row padding-tb-20">
                <div class="col-md-6 mx-auto">
                    <p class="txt-sm bg-white text-center txt-color-light padding-o-10">
                        You need to be
                        <a class="txt-color-blue" href="login.html">
                            signed in
                        </a>
                        to post comments
                    </p>
                </div>
            </div>
        </div>-->

        <!-- Comments -->
    <?php if ( bbp_has_replies() ) : ?>
    <section class="bg-grey padding-tb-40">
        <div class="container-wrapper">
            <div class="row">
                <div class="col-md-6 mx-auto">

                    <?php bbp_get_template_part( 'loop',       'replies' ); ?>
                    
                    <?php bbp_topic_tag_list(); ?>

                    <?php bbp_get_template_part( 'pagination', 'replies' ); ?>
                    
                    <?php bbp_get_template_part( 'form', 'reply' ); ?>

                </div>
            </div>
        </div>
        
        <style>
            .bbp-form legend{
                font-weight: 400;
                margin-bottom: 10px;
            }
            
            .bbp-form textarea{
                width: 100% !important;
            }
            
            .bbp-the-content-wrapper{
                margin-bottom: 20px;
            }
            
            .bbp-form .button{
                font-size: 0.9em;
                padding: 0.8em 2em;
                border: 0;
                outline: 0;
                background-color: gainsboro;
                border-radius: 20px;
                cursor: pointer;
            }
            
            .bbp-threaded-replies{
                padding-left: 40px;
            }
            
            li{
                list-style-type: none !important;
            }
            
            .reply-action{
                border: 0;
                outline: none !important;
                box-shadow: none;
                background-color: transparent;
                cursor: pointer;
            }
            
            .edit-links span{
                display: block;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            
            .edit-links a{
                display: block;
                min-width: 150px;
                padding-left: 20px;
                padding-right: 20px;
                
                font-size: 0.8rem !important;
                color: black !important;
                text-transform: capitalize !important;
            }
            
            input#bbp_topic_tags {
                display: none;
            }
            label[for=bbp_topic_tags] {
                display: none !important;
            }
        </style>
        
    </section>
    <?php endif; ?>

    
<?php endif; ?>

<?php do_action( 'bbp_template_after_single_topic' ); ?>



<?php
    /* Get Track ID from Query String */
    $track_id = $_REQUEST['track'];
    
    //Switch to Growth Programs Multisite (id = 20)
    switch_to_blog(20);
    
    /* Get Track Information */
    $track_url = get_permalink( $track_id );
    $track_name = get_the_title( $track_id );
    

    /* Related Topics Groups */
    $related_topics = rwmb_meta( 'connected-forum-topics-group', array(), $track_id );

    /* Created Topic */
    $related_topics_list = array();


    if ( ! empty( $related_topics ) ) {

        foreach ( $related_topics as $group_value ) {


            if( $group_value['group-title-url'] ){ 

                array_push( $related_topics_list, $group_value['group-title-url'] );
            } 


            $values = $group_value['forum-topics-group']['forum-topic-item'];

            if($values){
                foreach ( $values as $value ) { 
                    array_push( $related_topics_list, $value['forum-topic-url'] );
                }
            }

        }
    }

    /* Get Current Topic List from Array */
    $current_topic_key = array_search($topic_url, $related_topics_list);
    
    /* Get Last Index */
    $last_index = end( array_keys($related_topics_list) );
    
    /* Get Previous Topic link */
    if( $current_topic_key != 0 ){
        $previous_topic = $related_topics_list[$current_topic_key - 1].'?track='.$track_id;
    }

    /* Get Next Topic link */
    if( $current_topic_key != $last_index ){
        $next_topic = $related_topics_list[$current_topic_key + 1].'?track='.$track_id;
    }

    //Revert to Previous Multisite
    restore_current_blog();    
?>



<?php
    /* Get Forum Category */
    $forum_category = rwmb_meta( 'forum_category', array( 'object_type' => 'setting' ), 'my_options' );
    
    /* Forums to show Topic Footer On */
    $allowed_list = array('_tei_modal', '_business_clinic_modal', '_job_advisor_modal');
    
    $freeForum = rwmb_meta( 'forum_subcategory', array( 'object_type' => 'setting' ), 'my_options' );
    
    if( in_array($forum_category, $allowed_list) && $freeForum != 'mentor' ){
?>
        <!-- Topic Footer -->
        <div class="topic-footer">
            <div class="collapse" id="topic">
                <div class="padding-t-10 padding-b-20 text-center">

                    <a class="btn btn-green no-m-b txt-sm" data-toggle="modal" href="#mentorModal">
                        Curriculum
                    </a>
                    <a class="btn btn-green no-m-b txt-sm" data-toggle="modal" href="#subscriptionModal">
                        Talk to a Mentor
                    </a>

                </div>
            </div>
            <div class="wrapper">
                <?php if( $previous_topic ) { ?>
                    <a class="navigation-left" href="<?php echo $previous_topic ?>">
                        <span>
                            Previous Article
                        </span>
                    </a>
                <?php } else { ?>
                    <span class="navigation-left txt-color-lighter" style="cursor: default;">
                        <span>
                            Previous Article
                        </span>
                    </span>
                <?php } ?>

                <div class="d-md-none cta">
                    <button
                       class="btn btn-trans-bw no-m-b txt-sm"
                       type="button" data-toggle="collapse" 
                       data-target="#topic" 
                       aria-expanded="false" 
                       aria-controls="collapseExample"
                    >
                        Get more
                    </button>
                </div>
                
                <div class="d-none d-md-block cta">
                    <a class="btn btn-trans-bw no-m-b txt-sm" data-toggle="modal" href="#mentorModal">
                        Curriculum
                    </a>
                    <a class="btn btn-trans-bw no-m-b txt-sm" data-toggle="modal" href="#subscriptionModal">
                        Talk to a Mentor
                    </a>
                </div>

                <?php if( $next_topic ) { ?>
                    <a class="navigation-right" href="<?php echo $next_topic ?>">
                        <span>
                            Next Article
                        </span>
                    </a>
                <?php } else { ?>
                    <span class="navigation-right txt-color-lighter" style="cursor: default;">
                        <span>
                            Next Article
                        </span>
                    </span>
                <?php } ?>
            </div>
        </div>

        <!-- Request Mentor Modal -->
        <div class="modal fade font-main shc-modal" id="mentorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="padding-o-80">
                    <?php

                        if ( ! empty( $related_topics ) ) {
                    ?>
                        <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                            Curriculum
                        </h2>

                    <?php
                            foreach ( $related_topics as $group_value ) {
                    ?>

                            <!-- Article Group -->
                            <div class="padding-tb-20 border-t-1 border-color-darkgrey">

                            <?php if( $group_value['group-title-url'] ){ ?>

                                <h3>
                                    <a
                                       href="<?php echo $group_value['group-title-url']; ?>"
                                       class="txt-color-dark d-flex align-items-center"
                                    >
                                        <span class="txt-xs padding-r-5">
                                            <i class="fa fa-long-arrow-right"></i>
                                        </span>
                                        <?php echo $group_value['group-title']; ?>
                                    </a>
                                </h3>

                            <?php } else { ?>

                                <h3>
                                    <a
                                       href="<?php echo $group_value['group-title-url']; ?>"
                                       class="txt-color-dark d-flex align-items-center"
                                    >
                                        <span class="txt-xs padding-r-5">
                                            <i class="fa fa-long-arrow-right"></i>
                                        </span>
                                        <?php echo $group_value['group-title']; ?>
                                    </a>
                                </h3>

                            <?php } ?>

                            <?php 
                                $values = $group_value['forum-topics-group']['forum-topic-item'];

                                    if($values){
                            ?>
                                <ul class="row row-10 icon-list black txt-normal-s margin-t-20 padding-l-20">
                                <?php 
                                    foreach ( $values as $value ) { 
                                ?>

                                    <li class="col-md-3 padding-lr-10">
                                        <a href="<?php echo $value['forum-topic-url']; ?>">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                <?php echo $value['forum-topic-title']; ?>
                                            </span>
                                        </a>
                                    </li>

                                <?php } ?>

                                </ul>

                            <?php } ?>

                            </div>

                        <?php
                            }
                        }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Subscription Modal -->
        <div class="modal fade font-main shc-modal" id="subscriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <div class="container-wrapper padding-t-80">
                        <div class="row"> 
                            <div class="col-md-8 mx-auto">
                                <h2 class="text-center margin-b-5">
                                    Want to speak to someone? Need Special help?
                                </h2>
                                <h1 class="txt-xxlg txt-height-1-1 margin-b-60 text-center">
                                    Engage with Expert Mentors waiting to help you
                                </h1>
                                <p class="margin-b-60 text-center">
                                    <?php echo $subscription_message[$program_id]; ?>
                                </p>
                                <div class="row">
                                    <div class="col-md-9 mx-auto bg-ash txt-color-white text-center text-md-left">
                                        <div class="padding-o-30">
                                            <div class="row row-10 overflow-hidden">
                                                <div class="col-md-6 padding-lr-20 payment-border border-color-darkgrey margin-b-10 padding-b-10">
                                                    <h3 class="txt-xxlg txt-bold margin-b-5">
                                                        Just N10,000 / Qtr
                                                    </h3>
                                                    <p class="txt-sm">
                                                        To join <?php echo $track_name ?>
                                                        <br>
                                                        Mentor Hub
                                                    </p>
                                                </div>
                                                <div class="col-md-6 padding-lr-20">
                                                    <p class="txt-normal-s">
                                                        We charge a small fee to cover our costs and help maintain a high-quality moderated expert team.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="row">
                                    <div class="col-md-8 mx-auto">
                                        <ul class="con-list black txt-normal-s margin-t-40">
                                            <li class="padding-b-20">
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Tap into the wisdom & experience of a lot of mentors
                                                </span>
                                            </li>
                                            <li class="padding-b-20">
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Get customised answers to your questions
                                                </span>
                                            </li>
                                            <li class="padding-b-20">
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Get inspired by your peers
                                                </span>
                                            </li>
                                            <li class="padding-b-20">
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Get access to growth support tools, templates & special opportunities.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Engage with Experts and Mentors until you are satisfied.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="text-center margin-t-40">
                                    <a href="https://www.saedconnect.org/help-center/forums/forum/the-entrepreneurship-incubator-help-center/" class="btn btn-green txt-sm">
                                        Subscribe
                                    </a>
                                </div>
                              </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<?php
    }
?>

</main>
	

<?php endwhile; // end of the loop. ?>

<?php get_footer('user-dashboard') ?>