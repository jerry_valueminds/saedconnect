<?php /*Template Name: Profile - NYSC State Monitor*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Get Avatar */
        $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
        $meta_key = 'user_avatar_url';
        $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

        if($get_avatar_url){
            $avatar_url = $get_avatar_url;
        }
    
        /* Get State ID */
        $state_id = $_GET['state-id'];
        $state = get_term( $state_id );
        $nysc_state_code = rwmb_meta( 'nysc-state-code', array( 'object_type' => 'term' ), $state_id );
                        
        /* Meta Key */
        $meta_key = 'nysc_assigned_state';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php //get_template_part( 'template-parts/user-dashboard/_dashboard-nav-nysc' ); ?>
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            
            <?php if( $_GET['state-id'] && !$_GET['object-type'] ){ ?>
            
            <div class="dashboard-multi-main-content full">
                <div class="page-header justify-content-between">
                    <div>
                        <h1 class="page-title">
                            <?php echo $state->name; ?> State Monitor
                        </h1>
                    </div>
                    <div>
                        <a class="btn btn-trans txt-sm" href="<?php echo currenturl('true'); ?>">
                            <i class="fa fa-angle-left"></i>
                            All States
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Here, you can see all interventions running in your state, and those waiting for your approval. You can reach out to your partners via announcements, and see their complaints or questions feedbacks.
                    </p>
                </article>
                
                <article class="padding-b-40">
                    <h3 class="txt-normal-s txt-medium txt-color-dark margin-b-20">
                        Corp Members
                    </h3>
                    <div class="row row-5">
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=corp-member'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                    <?php
                                        global $wpdb; //Include WP Global Object
                                        $corp_member_db = new wpdb('root','umMv65ekyMRxfNfm','nysc_db','localhost');
                                                                   
                                        $user_count = $corp_member_db->get_var( "SELECT COUNT(*) FROM corp_member_profile WHERE state_code LIKE '%$nysc_state_code%' " );
                                        echo $user_count;
                                    ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Registered 
                                </div>
                            </a>
                        </div>
                    </div>
                </article>
                
                <article class="padding-b-40">
                    <h3 class="txt-normal-s txt-medium txt-color-dark margin-b-20">
                        Trainer Profiles
                    </h3>
                    <div class="row row-5">
                        <?php switch_to_blog(18); ?>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=trainer'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    // Create Query Argument
                                    $args = array(
                                        'post_type' => 'trainer-information',
                                        'showposts' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'nysc_accreditation_status_'.$state_id,
                                                'compare' => 'EXISTS'
                                            )
                                        )
                                    );

                                    $trainer_count_query = new WP_Query($args);
                                    echo $trainer_count_query->post_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Requests
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=trainer'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    // Create Query Argument
                                    $args = array(
                                        'post_type' => 'trainer-information',
                                        'showposts' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'nysc_accreditation_status_'.$state_id,
                                                'value' => 'user_published',
                                                'compare' => 'LIKE'
                                            )
                                        )
                                    );

                                    $trainer_count_query = new WP_Query($args);
                                    echo $trainer_count_query->post_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Pending
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=trainer'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    // Create Query Argument
                                    $args = array(
                                        'post_type' => 'trainer-information',
                                        'showposts' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'nysc_accreditation_status_'.$state_id,
                                                'value' => 'admin_published',
                                                'compare' => 'LIKE'
                                            )
                                        )
                                    );

                                    $trainer_count_query = new WP_Query($args);
                                    echo $trainer_count_query->post_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Approved
                                </div>
                            </a>
                        </div>
                        <?php restore_current_blog(); ?>
                    </div>
                </article>
                
                <article class="padding-b-40">
                    <h3 class="txt-normal-s txt-medium txt-color-dark margin-b-20">
                        Courses
                    </h3>
                    <div class="row row-5">
                        <?php switch_to_blog(18); ?>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=course'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    // Create Query Argument
                                    $args = array(
                                        'post_type' => 'training-offer',
                                        'showposts' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'nysc_accreditation_status_'.$state_id,
                                                'compare' => 'EXISTS'
                                            )
                                        )
                                    );

                                    $trainer_count_query = new WP_Query($args);
                                    echo $trainer_count_query->post_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Requests
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=course'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    // Create Query Argument
                                    $args = array(
                                        'post_type' => 'training-offer',
                                        'showposts' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'nysc_accreditation_status_'.$state_id,
                                                'value' => 'user_published',
                                                'compare' => 'LIKE'
                                            )
                                        )
                                    );

                                    $trainer_count_query = new WP_Query($args);
                                    echo $trainer_count_query->post_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Pending
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=course'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    // Create Query Argument
                                    $args = array(
                                        'post_type' => 'training-offer',
                                        'showposts' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'nysc_accreditation_status_'.$state_id,
                                                'value' => 'admin_published',
                                                'compare' => 'LIKE'
                                            )
                                        )
                                    );

                                    $trainer_count_query = new WP_Query($args);
                                    echo $trainer_count_query->post_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Approved
                                </div>
                            </a>
                        </div>
                        <?php restore_current_blog(); ?>
                    </div>
                </article>
            </div>
            
            <?php }elseif( $_GET['state-id'] && $_GET['object-type'] == 'trainer' ){ ?>
            
            <div class="dashboard-multi-main-content full">
                <div class="page-header justify-content-between">
                    <div>
                        <h1 class="page-title">
                            Trainers from <?php echo $state->name; ?> state
                        </h1>
                    </div>
                    <div>
                        <a class="btn btn-trans txt-sm" href="<?php echo currenturl('true').'?state-id='.$state_id; ?>">
                            <i class="fa fa-angle-left"></i>
                            Back to <?php echo $state->name; ?> State
                        </a>
                    </div>
                </div>
                <div class="">
                    <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                       <div class="col-5 padding-lr-15">
                            NAME
                       </div>
                       <div class="col-3 padding-lr-15">
                            AUTHOR
                       </div>
                    </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */
                    switch_to_blog(18);
                                                                       
                    // Create Query Argument
                    $args = array(
                        'post_type' => 'trainer-information',
                        'showposts' => -1,
                        'meta_query' => array(
                            array(
                                'key' => 'nysc_accreditation_status_'.$state_id,
                                'compare' => 'EXISTS'
                            )
                        )
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                    $post_author_id = get_the_author_meta('ID'); //Author ID
                ?>
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-5 padding-lr-15">
                            <a href="https://www.saedconnect.org/service-provider-directory/trainer-preview?user-id=<?php echo $post_author_id ?>&state-id=<?php echo $state_id ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                           <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($post->post_author, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $post->post_author, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );
                          
                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                          

                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                }   

                                restore_current_blog();
                            ?>
                            
                            <style>
                               .author_avatar {
                                    display: inline-block;
                                    background-size: cover !important;
                                    background-position: center !important;
                                    background-repeat: no-repeat !important;
                                    width: 1.5rem;
                                    height: 1.5rem;
                                    margin-right: 10px;
                                    border-radius: 50%;
                                }
                            </style>
                            <div class="d-flex align-items-center">
                                <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                                </figure>
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php echo ($post->post_author == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 padding-lr-15">
                            <a href="<?php printf("https://www.saedconnect.org/competency-profile/mail-user/?reference_post=%s&user_id=%s&blog_id=%s&post_id=%s", get_permalink(), get_the_author_meta('ID'), $template_information[ 'blog_id' ], $post_id ) ?>" target="_blank">
                                <i class="fa fa-envelope-o"></i>
                                Mail Author
                            </a>
                        </div>
                        <div class="col-2 padding-lr-15 text-right">
                            <div class="dropdown padding-l-20">
                                <a class="" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa-stack">
                                        <i class="fa fa-circle fa-stack-2x txt-grey"></i>
                                        <i class="fa fa-ellipsis-v fa-stack-1x txt-color-white"></i>
                                    </span>
                                </a>
                                <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuLink">
                                   
                                    <?php 
                                
                                        $actions = $page_view['actions'];

                                        for ($i = 0; $i < count( $actions ); $i++) {

                                    ?>

                                    <!--<a href="<?php printf('%s&view=form&action=publication&value=%s&redir=%s&post-id=%s', currentUrl(false), $actions[$i]['action'], $get_page_view, $post_id ) ?>" class="<?php echo $actions[$i]['btn_styles'] ?>">
                                        <?php echo $actions[$i]['cta_text'] ?>
                                    </a>-->
                                    
                                    <a 
                                       href="<?php printf('%s&view=form&action=publication&value=%s&redir=%s&post-id=%s', currentUrl(false), $actions[$i]['action'], $get_page_view, $post_id ) ?>"
                                       class="dropdown-item"
                                    >
                                        <?php echo $actions[$i]['cta_text'] ?>
                                    </a>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    endwhile;
                    restore_current_blog();
                ?>
                </div>
            </div>
            
            <?php }elseif( $_GET['state-id'] && $_GET['object-type'] == 'corp-member' ){ ?>
            
            <div class="dashboard-multi-main-content full">
                <div class="page-header justify-content-between">
                    <div>
                        <h1 class="page-title">
                            Registered Corp Members in <?php echo $state->name; ?> State
                        </h1>
                    </div>
                    <div>
                        <a class="btn btn-blue txt-sm" data-toggle="modal" href="#messageModal">Broadcast Messaage</a>
                        <a class="btn btn-trans txt-sm" href="<?php echo currenturl('true').'?state-id='.$state_id; ?>">
                            <i class="fa fa-angle-left"></i>
                            Back to <?php echo $state->name; ?> State
                        </a>
                    </div>
                </div>
                <div class="">
                    <div class="row row-15 txt-sm txt-medium uppercase padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                       <div class="col-4 padding-lr-15">
                            Name
                       </div>
                       <div class="col-3 padding-lr-15">
                            State Code
                       </div>
                       <div class="col-4 padding-lr-15">
                            Interests
                       </div>
                       
                    </div>
                    <style>
                       .author_avatar {
                            display: inline-block;
                            background-size: cover !important;
                            background-position: center !important;
                            background-repeat: no-repeat !important;
                            width: 1.5rem;
                            height: 1.5rem;
                            margin-right: 10px;
                            border-radius: 50%;
                        }
                    </style>
                <?php
                    /* Query */
                    global $wpdb; //Include WP Global Object
                    $corp_member_db = new wpdb('root','umMv65ekyMRxfNfm','nysc_db','localhost');
                    $corp_members = $corp_member_db->get_results( "SELECT * FROM corp_member_profile WHERE state_code LIKE '$nysc_state_code%'" );                                                                                        
                    /* Loop */                                                 
                    foreach($corp_members as $corp_member){

                    /* Get Post ID */
                    $post_id = $post->ID;
                    $post_author_id = get_the_author_meta('ID'); //Author ID
                ?>
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-4 padding-lr-15">
                           <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($corp_member->user_ID, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $corp_member->user_ID, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );
                          
                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                          

                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                }   

                                restore_current_blog();
                            ?>
                            
                            
                            <a class="d-flex align-items-center" href="https://www.saedconnect.org/competency-profile/user-cv/?user-id=<?php echo $corp_member->user_ID ?>&state-id=<?php echo $state_id ?>">
                                <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                                </figure>
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php echo ($corp_member->user_ID== 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </div>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                            <?php echo $corp_member->state_code ?>
                        </div>                      

                        <div class="col-5 padding-lr-15 txt-sm">
                            <?php
                                switch_to_blog(109);
                        
                                $gf_id = 39; //Form ID
                                $gv_id = 1132; //Gravity View ID
                                $title = 'Interests';

                                $entry_count = 0;

                                /* Get Entries */
                                $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                /* GF Search Criteria */
                                $search_criteria = array(
                                    'field_filters' => array( //which fields to search
                                        array( 'key' => 'created_by', 'value' => $current_user->ID )
                                    )
                                );

                                /* Get GF Entry Count */
                                $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    

                                if(!$entry_count){ //If no entry

                                } else {
                                    foreach( $entries as $key => $entry ){
                                        echo rgar( $entry, 1 );
                                        echo ( $key < $entry_count )? ', ' : '';
                                    }
                                }
                        
                                restore_current_blog(); 
                            ?>
                        </div>
                        <!--<div class="col-2 text-right padding-lr-15">
                            <a href="<?php printf("https://www.saedconnect.org/competency-profile/mail-user/?reference_post=%s&user_id=%s&blog_id=%s&post_id=%s", get_permalink(), get_the_author_meta('ID'), $template_information[ 'blog_id' ], $post_id ) ?>" target="_blank">
                                <i class="fa fa-envelope-o"></i>
                                Send Message
                            </a>
                        </div>-->
                    </div>
                <?php
                    }
                ?>
                </div>
            </div>
            
            <!-- Message Modal -->
            <div class="modal fade font-main filter-modal" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <form action="<?php echo currentUrl(false).'&view=form-message'; ?>" method="post">
                            <div class="modal-header padding-lr-30">
                                <h5 class="modal-title" id="exampleModalLabel">Send a Message</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body padding-o-30 form">
                                <?php
                                    /* Meta Key */
                                    $redirect_link = currentUrl(true).'?object-type='.$_GET['object-type'].'&state-id='.$_GET['state-id'];
                                    global $wpdb; //Include WP Global Object
                                    $message_db = new wpdb('root','umMv65ekyMRxfNfm','messages','localhost');

                                    /*
                                    *
                                    * Send Message
                                    *
                                    */
                                    if($_POST && $_GET['view'] == 'form-message'){

                                        /* Message Data */
                                        $sender = 0;
                                        $receiver = 0;
                                        $parent_message = 0;
                                        $type = 'nysc_broadcast';
                                        $subject = sanitize_text_field( $_POST['subject'] );
                                        $message = wp_kses_post( $_POST['message'] );
                                        $connected_post = 0;
                                        $connected_post_site = 0;
                                        $read_status_sender = 'unread';
                                        $read_status_receiver = 'unread';

                                        $message_db->insert( 
                                            'messages', 
                                            array( 
                                                "sender" => $sender,
                                                "receiver" => $receiver,
                                                "parent_message" => $parent_message,
                                                "nysc_broadcast_coverage" => $nysc_state_code,
                                                "type" => $type,
                                                "subject" => $subject,
                                                "content" => $message,
                                                "connected_post" => $connected_post,
                                                "connected_post_site" => $connected_post_site,
                                                "read_status_sender" => $read_status_sender,
                                                "read_status_receiver" => $read_status_receiver,
                                            ), 
                                            array( "%d", "%d", "%d", "%s", "%s", "%s", "%s", "%d", "%d", "%s", "%s" ) 
                                        );

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                ?>

                                <!-- Title -->
                                <div class="form-item">
                                    <label for="subject">
                                        Subject
                                    </label>
                                    <input 
                                        type="text" 
                                        name="subject"
                                        required
                                    >
                                </div>

                                <!-- Summary -->
                                <div class="form-item">
                                    <label for="message">
                                        Your Message
                                    </label>
                                    <textarea class="editor" name="message" cols="30" rows="4"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer padding-lr-30">
                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <?php }elseif( $_GET['state-id'] && $_GET['object-type'] == 'course' ){ ?>
            
            <div class="dashboard-multi-main-content full">
                <div class="page-header justify-content-between">
                    <div>
                        <h1 class="page-title">
                            Courses from <?php echo $state->name; ?> state
                        </h1>
                    </div>
                    <div>
                        <a class="btn btn-trans txt-sm" href="<?php echo currenturl('true').'?state-id='.$state_id; ?>">
                            <i class="fa fa-angle-left"></i>
                            Back to <?php echo $state->name; ?> State
                        </a>
                    </div>
                </div>
                <div class="">
                    <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                       <div class="col-5 padding-lr-15">
                            NAME
                       </div>
                       <div class="col-3 padding-lr-15">
                            AUTHOR
                       </div>
                    </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */
                    switch_to_blog(18);
                                                                       
                    // Create Query Argument
                    $args = array(
                        'post_type' => 'training-offer',
                        'showposts' => -1,
                        'meta_query' => array(
                            array(
                                'key' => 'nysc_accreditation_status_'.$state_id,
                            )
                        )
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                ?>
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-5 padding-lr-15">
                            <a href="<?php the_permalink() ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                           <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($post->post_author, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $post->post_author, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );
                          
                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                          

                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                }   

                                restore_current_blog();
                            ?>
                            
                            <style>
                               .author_avatar {
                                    display: inline-block;
                                    background-size: cover !important;
                                    background-position: center !important;
                                    background-repeat: no-repeat !important;
                                    width: 1.5rem;
                                    height: 1.5rem;
                                    margin-right: 10px;
                                    border-radius: 50%;
                                }
                            </style>
                            <div class="d-flex align-items-center">
                                <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                                </figure>
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php echo ($post->post_author == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 padding-lr-15">
                            <a href="<?php printf("https://www.saedconnect.org/competency-profile/mail-user/?reference_post=%s&user_id=%s&blog_id=%s&post_id=%s", get_permalink(), get_the_author_meta('ID'), $template_information[ 'blog_id' ], $post_id ) ?>" target="_blank">
                                <i class="fa fa-envelope-o"></i>
                                Mail Author
                            </a>
                        </div>
                        <div class="col-2 padding-lr-15 text-right">
                            <div class="dropdown padding-l-20">
                                <a class="" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa-stack">
                                        <i class="fa fa-circle fa-stack-2x txt-grey"></i>
                                        <i class="fa fa-ellipsis-v fa-stack-1x txt-color-white"></i>
                                    </span>
                                </a>
                                <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuLink">
                                   
                                    <?php 
                                
                                        $actions = $page_view['actions'];

                                        for ($i = 0; $i < count( $actions ); $i++) {

                                    ?>

                                    <!--<a href="<?php printf('%s&view=form&action=publication&value=%s&redir=%s&post-id=%s', currentUrl(false), $actions[$i]['action'], $get_page_view, $post_id ) ?>" class="<?php echo $actions[$i]['btn_styles'] ?>">
                                        <?php echo $actions[$i]['cta_text'] ?>
                                    </a>-->
                                    
                                    <a 
                                       href="<?php printf('%s&view=form&action=publication&value=%s&redir=%s&post-id=%s', currentUrl(false), $actions[$i]['action'], $get_page_view, $post_id ) ?>"
                                       class="dropdown-item"
                                    >
                                        <?php echo $actions[$i]['cta_text'] ?>
                                    </a>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    endwhile;
                    restore_current_blog();
                ?>
                </div>
            </div>
            
            <?php }else{ ?>
            
            <div class="dashboard-multi-main-content full">
               
                <div class="page-header">
                    <h1 class="page-title">
                        <?php echo ( $_GET['role'] = 'nationwide' ) ? 'NYSC Nationwide Monitor' : 'NYSC State Monitor'; ?>
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Manage and assign NYSC Users to validation programs.
                    </p>
                </article>
                
                <?php if( $_GET['role'] == 'nationwide' ){ ?>
                
                <article class="padding-b-40">
                    <h3 class="txt-normal-s txt-medium txt-color-dark margin-b-20">
                        Courses
                    </h3>
                    <div class="row row-5">
                        <?php switch_to_blog(18); ?>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=course'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    global $wpdb; //Include WP Global Object
                                    $corp_member_db = new wpdb('root','umMv65ekyMRxfNfm','nysc_db','localhost');

                                    $user_count = $corp_member_db->get_var( "SELECT COUNT(*) FROM corp_member_profile" );
                                    echo $user_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Corp Members Registered
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=course'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    // Create Query Argument
                                    $args = array(
                                        'post_type' => 'trainer-information',
                                        'showposts' => -1,
                                    );

                                    $trainer_count_query = new WP_Query($args);
                                    echo $trainer_count_query->post_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Trainers Registered
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-5 padding-b-10 d-flex">
                            <a href="<?php echo currenturl(false).'&object-type=course'; ?>" class="bg-grey flex_1 padding-o-20">
                                <div class="txt-3em margin-b-5">
                                <?php
                                    // Create Query Argument
                                    $args = array(
                                        'post_type' => 'training-offer',
                                        'showposts' => -1,
                                    );

                                    $trainer_count_query = new WP_Query($args);
                                    echo $trainer_count_query->post_count;
                                ?>
                                </div>
                                <div class="txt-medium txt-color-lighter">
                                    Courses Created
                                </div>
                            </a>
                        </div>
                        <?php restore_current_blog(); ?>
                    </div>
                </article>
                
                <?php } ?>
                
                <div class="row row-10">
                    <?php 
                        /*
                        *
                        * Populate Form Data from Terms
                        *
                        */
                        
                        /* Get Saved Users */
                        $saved_users = get_users( array('blog_id' => 1, 'orderby' => 'nicename', 'meta_key' => $meta_key, 'meta_value' => $state_id, 'fields' => 'ids' ) );
                        $nationwide_users = get_users( array('blog_id' => 1, 'orderby' => 'nicename', 'meta_key' => $meta_key, 'meta_value' => 'nationwide', 'fields' => 'ids' ) );
    
                        //Get Terms
                        $terms = get_terms( 'state', array('hide_empty' => false));

                        foreach ($terms as $term) { //Cycle through terms, one at a time

                            // Check and see if the term is a top-level parent. If so, display it.
                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                            //$saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term_id, true );
                            $saved_meta = get_user_meta($current_user->ID, $meta_key);
                            
                    ?>
                        <?php if( in_array($term_id, $saved_meta) || in_array('nationwide', $saved_meta) ){ ?>
                        <div class="col-md-6 col-lg-4 col-xl-3 d-flex padding-lr-10 padding-b-20">
                            <a 
                                href="<?php currenturl(false) ?>?&state-id=<?php echo $term_id; ?>"
                                class="col d-flex align-items-center justify-content-between txt-sm txt-medium padding-o-15 border-o-1 border-color-darkgrey"
                            >
                                <span class="txt-medium"><?php echo $term_name; ?></span>                
                            </a>
                        </div>
                        <?php } ?>
                    <?php } ?>                  
                </div>
            </div>
            
            <?php } ?>
            
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>