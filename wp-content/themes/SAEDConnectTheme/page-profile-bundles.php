<?php /*Template Name: Profile - Bundles*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Post Type */
        $postType = 'program-bundle';

        /* Publication */
        $publication_key   = 'publication_status';

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post">
                            <?php
                                /* Meta Key */
                                $meta_key = 'assigned_users';
                                $validation_meta_key = 'validation_types';
                                $redirect_link = currentUrl(true);
                              
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    $bundle_id = $_GET['post-id'];
                                    $post = get_post($post_id);
                                    $postName = $post->post_title;
                                    
                                    /* Delete & Return */
                                    if($_GET['action'] == 'delete'){
                                        /* Delete Post */
                                        wp_delete_post($post_id);

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                }
                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if( $_POST ){
                                    
                                    /* Get Post Name */
                                    $postName = $_POST['post-name'];
                                    
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));
                                    
                                    /* Get form data */
                                    $infoPageUrl = $_POST['info-page-url'];
                                    $summary = $_POST['summary'];
                                    $bgColor = $_POST['bg-color'];
                                    $altColor = $_POST['alt-color'];
                                    
                                    $submitted_jobs = $_POST['jobs'];
                                    $submitted_opportunities = $_POST['opportunities'];
                                    $submitted_courses = $_POST['courses'];
                                    
                                    
                                    /* Serialize Form Submission */
                                    $serialized_jobs = maybe_serialize( $submitted_jobs );
                                    $serialized_opportunities = maybe_serialize( $submitted_opportunities );
                                    $serialized_courses = maybe_serialize( $submitted_courses );

                                    
                                    /* Save data */
                                    update_post_meta($post_id, 'info-page-url', $infoPageUrl);
                                    update_post_meta($post_id, 'summary', $summary);
                                    update_post_meta($post_id, 'bg-color', $bgColor);
                                    update_post_meta($post_id, 'alt-color', $altColor);
                                    
                                    update_post_meta($post_id, 'jobs', $serialized_jobs);
                                    update_post_meta($post_id, 'opportunities', $serialized_opportunities);
                                    update_post_meta($post_id, 'courses', $serialized_courses);
                                }
                                              
                                              
                                /* Get Saved data */             
                                $infoPageUrl = get_post_meta($post_id, 'info-page-url', true);
                                $summary = get_post_meta($post_id, 'summary', true);
                                $bgColor = get_post_meta($post_id, 'bg-color', true);
                                $altColor = get_post_meta($post_id, 'alt-color', true);
                              
                                /* Get Saved Jobs */
                                $saved_jobs = maybe_unserialize( get_post_meta($post_id, 'jobs', true) );
                                              
                                /* Get Saved Opportunities */
                                $saved_opportunities = maybe_unserialize( get_post_meta($post_id, 'opportunities', true) );
                                              
                                /* Get Saved Opportunities */
                                $saved_courses = maybe_unserialize( get_post_meta($post_id, 'courses', true) );

                            ?>
                            
                            <!-- Title -->
                            <div class="margin-b-40">
                                <label for="post-name" class="d-block txt-normal-s txt-medium margin-b-10">
                                    Bundle Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post-name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $postName ?>"
                                >
                            </div>
                            
                            <!-- Information Page URL -->
                            <div class="margin-b-40">
                                <label for="info-page-url" class="d-block txt-normal-s txt-medium margin-b-10">
                                    Information Page URL
                                </label>
                                <input 
                                    type="url" 
                                    name="info-page-url" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $infoPageUrl ?>"
                                    placeholder="https://www.saedconnect.org/"
                                >
                            </div>
                            
                            <!-- Summary -->
                            <div class="margin-b-40">
                                <label for="summary" class="d-block txt-normal-s txt-medium margin-b-10">
                                    Summary
                                </label>
                                <textarea name="summary" id="summary" cols="30" rows="8" class="w-100"><?php echo $summary ?></textarea>
                            </div>
                            
                            <!-- BG Color -->
                            <div class="margin-b-40">
                                <label for="bg-color" class="d-block txt-normal-s txt-medium margin-b-10">
                                    Theme Color
                                </label>
                                <div>
                                    <input 
                                        type="color" 
                                        name="bg-color" 
                                        class=""
                                        value="<?php echo $bgColor ?>"
                                        placeholder="HEX/RGB/RGBA Color Code"
                                    >
                                </div>
                            </div>
                            
                            <!-- Alt. Color -->
                            <div class="margin-b-40">
                                <label for="bg-color" class="d-block txt-normal-s txt-medium margin-b-10">
                                    Text Color
                                </label>
                                <div>
                                    <input 
                                        type="color" 
                                        name="alt-color" 
                                        class=""
                                        value="<?php echo $altColor ?>"
                                        placeholder="HEX/RGB/RGBA Color Code"
                                    >
                                </div>
                            </div>
                            
                            
                            <!-- Jobs -->
                            <div class="padding-b-30">
                                <p class="txt-normal-s txt-medium margin-b-10">
                                    Assign Jobs to this Bundle
                                </p>
                                <?php switch_to_blog(101); ?>
                                <select class="select-collaborators full-width" name="jobs[]" multiple="multiple">
                                    <?php
                                        /*
                                        *==================================================================================
                                        *==================================================================================
                                        *   WP Query
                                        *==================================================================================
                                        *==================================================================================
                                        */
                                        // Create Query Argument
                                        $args = array(
                                            'post_type' => 'job',
                                            'showposts' => -1,
                                        );


                                        $wp_query = new WP_Query($args);

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Get Post ID */
                                        $post_id = $post->ID;
                                             
                                        if($_POST){
                                            delete_post_meta($post_id, 'bundle', $bundle_id);
                                            
                                            if( in_array($post_id, $saved_jobs) ){
                                                
                                                add_post_meta($post_id, 'bundle', $bundle_id, false);
                                            }
                                        }
                                    ?> 
                                        
                                        <option 
                                           value="<?php echo $post_id; ?>"
                                           <?php echo ( in_array($post_id, $saved_jobs) ) ? "selected" : ""; ?>
                                        >
                                            <?php echo '#'.$post_id.' - '.get_the_title(); ?>
                                        </option>

                                    <?php
                                        endwhile;
                                    ?>
                                </select>
                                <?php restore_current_blog(); ?>
                            </div>
                            
                            <!-- Opportunities -->
                            <div class="padding-b-30">
                                <p class="txt-normal-s txt-medium margin-b-10">
                                    Assign Opportunities to this Bundle
                                </p>
                                <?php switch_to_blog(12); ?>
                                
                                <select class="select-collaborators full-width" name="opportunities[]" multiple="multiple">
                                    <?php
                                        /*
                                        *==================================================================================
                                        *==================================================================================
                                        *   WP Query
                                        *==================================================================================
                                        *==================================================================================
                                        */
                                        // Create Query Argument
                                        $args = array(
                                            'post_type' => 'opportunity',
                                            'showposts' => -1,
                                        );


                                        $wp_query = new WP_Query($args);

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Get Post ID */
                                        $post_id = $post->ID;

                                    ?> 

                                        <option 
                                           value="<?php echo $post_id; ?>"
                                           <?php echo ( in_array($post_id, $saved_opportunities) ) ? "selected" : ""; ?>
                                        >
                                            <?php echo '#'.$post_id.' - '.get_the_title(); ?>
                                        </option>

                                    <?php
                                        endwhile;
                                    ?>
                                </select>
                                
                                <?php restore_current_blog(); ?>
                            </div>
                            
                            <!-- Training -->
                            <div class="padding-b-15">
                                <p class="txt-normal-s txt-medium margin-b-10">
                                    Assign Courses to this Bundle
                                </p>
                                <?php switch_to_blog(18); ?>
                                <select class="select-collaborators full-width" name="courses[]" multiple="multiple">
                                    <?php
                                        /*
                                        *==================================================================================
                                        *==================================================================================
                                        *   WP Query
                                        *==================================================================================
                                        *==================================================================================
                                        */
                                        // Create Query Argument
                                        $args = array(
                                            'post_type' => 'training-offer',
                                            'showposts' => -1,
                                        );


                                        $wp_query = new WP_Query($args);

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Get Post ID */
                                        $post_id = $post->ID;

                                    ?> 

                                        <option 
                                           value="<?php echo $post_id; ?>"
                                           <?php echo ( in_array($post_id, $saved_courses) ) ? "selected" : ""; ?>
                                        >
                                            <?php echo '#'.$post_id.' - '.get_the_title(); ?>
                                        </option>

                                    <?php
                                        endwhile;
                                    ?>
                                </select>
                                <?php restore_current_blog(); ?>
                            </div>
                            
                            <div class="text-right padding-t-20">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                        <?php 
                            if( $_POST )
                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        ?>
                    </div>
                </div>
               
            <?php } else { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        Program Bundles
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form-title=Create a Validation Program" class="cta-btn">
                            Add Bundle
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Create, manage and assign Jobs, Opportunities, offers to Program Bundles.
                    </p>
                </article>
                
               <!-- <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Service Offers
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Service Offers I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>-->
                <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                    <div class="col-4 padding-lr-15">
                        NAME
                    </div>
                    <div class="col-3 padding-lr-15">
                        ASSIGNED USERS
                    </div>
                    <div class="col-3 padding-lr-15">
                        VALIDATION TYPES
                   </div>                  
                </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */
                    // Create Query Argument
                    $args = array(
                        'post_type' => $postType,
                        'showposts' => -1,
                        'author' => $current_user->ID,
                        'meta_query' => $meta_array,
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                          
                ?>
                    
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-4 padding-lr-15">
                            <a href="<?php the_permalink() ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                            <div class="d-flex align-items-center">
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php
                                        /* Get Saved Users */
                                        $saved_users = get_post_meta($post_id, $meta_key);
                                        $saved_users = $saved_users['assigned_users'];
                          
                                        foreach( $saved_users as $key => $saved_user ){
                                            
                                            if( $saved_user == 1 ){
                                                
                                                echo 'SAEDConnect Admin'; 
                                                
                                            } else {
                                                
                                                $user_info = get_userdata($saved_user);
                                                echo $user_info->user_nicename;
                                                
                                            }
                                            
                                            if( $key + 1 < count($saved_users) ){
                                                echo ", ";
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 padding-lr-15">
                            <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                <?php 
                                    /* Get Saved Users */
                                    $saved_validation_types = get_post_meta($post_id, $validation_meta_key);
                                                    
                          
                                    $saved_validation_types = $saved_validation_types['validation_types'];

                                    foreach( $saved_validation_types as $key => $saved_validation_type ){

                                        echo $saved_validation_type; 

                                        if( $key + 1 < count($saved_validation_types) ){
                                            echo ", ";
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-2 padding-lr-15 text-right">
                            <a 
                               href="<?php printf('%s/?view=form&post-id=%s&form-title=Edit %s', currentUrl(true), $post_id, get_the_title() ) ?>"
                               class="txt-medium txt-color-green"
                            >
                                Edit
                            </a>
                            |
                            <a 
                               href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', currentUrl(true), $post_id ) ?>"
                               class="confirm-delete txt-color-red"
                            >
                                Delete
                            </a>
                        </div>
                    </div>
                    
                <?php
                    endwhile;
                ?>
                
            <?php } ?> 
                
            </div>
        </section>
    </main>
    
    <script>
        // Simple example, see optional options for more configuration.
        const pickr = Pickr.create({
            el: '.color-picker',
            theme: 'classic', // or 'monolith', or 'nano'

            swatches: [
                'rgba(244, 67, 54, 1)',
                'rgba(233, 30, 99, 0.95)',
                'rgba(156, 39, 176, 0.9)',
                'rgba(103, 58, 183, 0.85)',
                'rgba(63, 81, 181, 0.8)',
                'rgba(33, 150, 243, 0.75)',
                'rgba(3, 169, 244, 0.7)',
                'rgba(0, 188, 212, 0.7)',
                'rgba(0, 150, 136, 0.75)',
                'rgba(76, 175, 80, 0.8)',
                'rgba(139, 195, 74, 0.85)',
                'rgba(205, 220, 57, 0.9)',
                'rgba(255, 235, 59, 0.95)',
                'rgba(255, 193, 7, 1)'
            ],

            components: {

                // Main components
                preview: true,
                opacity: true,
                hue: true,

                // Input / output Options
                interaction: {
                    hex: true,
                    rgba: true,
                    hsla: true,
                    hsva: true,
                    cmyk: true,
                    input: true,
                    clear: true,
                    save: true
                }
            }
        });
    </script>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>