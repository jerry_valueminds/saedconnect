    <?php /*Template Name: Homepage-Partners*/ ?>
    
    <?php get_header() ?>

    <main class="main-content">
        <header class="overview-header container-wrapper">
            <h1 class="txt-2em txt-bold margin-b-20">
                Empower people, transform lives
            </h1>
            <h2 class="txt-lg txt-height-1-4 margin-b-80">
                Let us work together to create and enable solutions that help people discover their potential develop <br> & activate their employability & entrepreneurship capacity to create value and derive economic benefits.
            </h2>
        </header>
        <!-- General Section -->
        <section>
            <div class="row">
                <div class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (12).jpg')">
                    
                    <div class="content txt-color-white">
                        <div class="margin-b-20">
                            <h3 class="txt-xlg txt-medium txt-height-1-2">
                                Offer Trainings & Youth Services
                            </h3>
                        </div>
                        <p class="txt-normal-s txt-height-1-5 margin-b-20">
                            Hash Code is a team programming competition organized by Google for students and industry professionals across Europe, the Middle East and Africa.
                        </p>
                        <ul class="icon-list white">               
                        <?php
                            //Switch to Learn a Skill Multisite (id = 8)
                            switch_to_blog(11);

                            $connected = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_two_c_m_page',
                                    'from' => 608, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $connected->have_posts() ) : $connected->the_post(); ?>
                                <li>
                                   <a href="<?php the_permalink() ?>">
                                        
                                       <span>
                                           <?php the_title() ?>
                                       </span>
                                   </a>
                                </li>
                        <?php
                            endwhile;
                            wp_reset_postdata();

                            //Revert to Previous Multisite
                            restore_current_blog();
                        ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (35).png')">
                    
                    <div class="content txt-color-white">
                        <div class="margin-b-20">
                            <h3 class="txt-xlg txt-medium txt-height-1-2">
                                Support Youth Development
                            </h3>
                        </div>
                        <p class="txt-normal-s txt-height-1-5 margin-b-20">
                            Hash Code is a team programming competition organized by Google for students and industry professionals across Europe, the Middle East and Africa.
                        </p>
                        <ul class="icon-list white">
                            <li>
                                <a data-toggle="modal" href="http://www.saedconnect.org/prepare-yourself/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Discover Yourself
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Get Counseling
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/self-discovery-guide/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Self Discovery Guide
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <a href="#" class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (16).jpg')">
                        <div class="content txt-color-white">
                            <div class="margin-b-20">
                                <h3 class="txt-xlg txt-medium txt-height-1-2">
                                    Talent Source
                                </h3>
                            </div>
                            <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                Hash Code is a team programming competition organized by Google for students and industry professionals across Europe, the Middle East and Africa.
                            </p>
                        </div>
                    </a>
                    <a href="#" class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (18).jpg')">
                        <div class="content txt-color-white">
                            <div class="margin-b-20">
                                <h3 class="txt-xlg txt-medium txt-height-1-2">
                                    Advertise to Youths Nationwide
                                </h3>
                            </div>
                            <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                Hash Code is a team programming competition organized by Google for students and industry professionals across Europe, the Middle East and Africa.
                            </p>
                        </div>
                    </a>
                    <a href="#" class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (26).jpg')">
                        <div class="content txt-color-white">
                            <div class="margin-b-20">
                                <h3 class="txt-xlg txt-medium txt-height-1-2">
                                    Register with NYSC SAED
                                </h3>
                            </div>
                            <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                Hash Code is a team programming competition organized by Google for students and industry professionals across Europe, the Middle East and Africa.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </section>
        
        <section class="pre-footer" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/hero_7.jpg');">
            <div class="content">
                <h4 class="title">
                    Refine and redefine yourself as an artist or designer.
                </h4>
                <article class="btn-wrapper">
                    <a class="btn btn-trans-wb icon" href="">
                        Request More Information
                    </a>
                    <a class="btn btn-trans-wb" href="">
                        Schedule an Appointment
                    </a>
                    <a class="btn btn-white" href="">
                        Apply Now
                    </a>
                </article>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>
