<?php get_header() ?>
    
<?php
    
    // TO SHOW THE POST CONTENT
    while ( have_posts() ) : the_post();
    
?>

    <main class="main-content">
        <header class="container-wrapper dashboard-multi-header bg-yellow padding-tb-20">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="txt-xlg txt-medium ">
                        <a href="<?php echo get_site_url().'/products/' ?>" class="txt-color-dark">
                            <i class="fa fa-angle-left"></i>
                            Marketplace
                        </a>
                    </h1>
                </div>
            </div>
        </header>
        
        <section class="container-wrapper" style="padding-top: 62px">
            <div class="row row-15 padding-tb-40">
                <div class="col-md-7 padding-lr-15 padding-b-15">
                <?php
                    /* Twitter */
                    $field = 'sell-product-image';

                    $meta = get_post_meta($post->ID, $field, true);

                    if($meta){
                ?>
                    <img src="<?php echo $meta; ?>" alt="">

                <?php } else{ ?>
                   
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/table.jpg" alt="">
   
                <?php } ?>
                </div>
                <div class="col-md-5 padding-lr-15">
                    <header 
                       class="border-o-1 border-color-darkgrey padding-tb-20 padding-lr-15 margin-b-15"
                    >
                        <h2 class="txt-lg txt-medium margin-b-20">
                            <?php the_title() ?>
                        </h2> 
                        <div 
                           class="row row-10 justify-content-between txt-light txt-color-lighter txt-normal-s"
                        >
                            <div class="location padding-lr-10">
                                Abuja, Lagos
                            </div>
                            <div class="post-time padding-lr-10">
                                2 March 2019
                            </div>
                        </div>
                    </header>
                    <div class="border-o-1 border-color-darkgrey padding-tb-20 padding-lr-15 margin-b-40">
                        <h2 class="txt-lg txt-medium margin-b-15">
                            Seller Details 
                        </h2> 
                        <p class="txt-normal-s">
                            Browse through some of our most popular Products / Services.
                        </p>
                        <div class="padding-t-20">
                            <a href="" class="btn btn-trans-bw txt-xs no-m-b">
                                Contact Seller
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
<?php
    endwhile; //resetting the page loop
?>

<?php get_footer() ?>