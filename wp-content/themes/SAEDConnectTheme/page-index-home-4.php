    <?php /*Template Name: Homepage-4*/ ?>
    
    <?php get_header() ?>
    
    <?php
        /* Publication key for getting Admin Approved content */
        $publication_key = 'publication_status';
    ?>
    
    <main class="main-content">
        <!-- Top bar -->
        <section class="container-wrapper">
            <div class="bg-ash padding-o-15">
                <div class="row">
                    <div class="col-md-6">
                        <p class="txt-normal-s txt-color-white">
                            SAEDConnect is a <span class="txt-medium txt-color-yellow">Youth Empowerment Accelerator</span> dedicated to providing the solutions, resources, tools, opportunities and networks that young people need to make progress.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Hero Slider -->
        <section class="container-wrapper padding-t-10 padding-b-60">
            <div class="home-gallery-2">
                <section class="swiper-wrapper">
                <?php
                    //WP Query
                    $args = array( 'post_type' => 'homepage-slide' );

                    $homeSlide_query = new WP_Query();
                    $homeSlide_query->query($args);

                    $counter = 1;

                    while ($homeSlide_query->have_posts()) : $homeSlide_query->the_post();

                        $images = rwmb_meta( 'slide-image', array( 'limit' => 1 ) );
                        $image = reset( $images ); 
                        $image_url = $image['full_url']; 
                ?>
                    <div class="swiper-slide txt-color-white" style="background-image: url('<?php echo $image_url; ?>')">
                        <!--<div class="container-wrapper padding-tb-40 content">
                            <h1 class="title padding-b-60">
                                Young Leaders Network
                            </h1>
                            <a href="#" class="cta txt-color-white">
                                More Information
                            </a>
                        </div>-->
                    </div>
                <?php 
                    $counter++; 
                    endwhile; 
                ?>
                </section>
                <!-- Add Navigation -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                        
                <!-- Add Pagination -->
                <div class="swiper-pagination-home"></div>
            </div>
        </section>
        
        <!-- For Young People -->
        <section class="container-wrapper">
            <header class="container-wrapper margin-b-40">
                <h1 class="txt-xxlg txt-bold txt-height-1-1 uppercase margin-b-10">
                    For Young People
                </h1>
                <p>
                    If you have the skill for any of these roles, we will connect you to a job
                </p>
            </header>
            <div class="container-wrapper padding-t-40 padding-b-40 bg-yellow">
                <div class="border-b-1 border-color-white">
                    <div class="row row-30 padding-b-10">
                        <div class="col-md-3 padding-lr-30 padding-b-30">
                            <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/icon_1.png" alt="Icon" width="40"></figure>
                            <p class="txt-normal-s txt-bold uppercase margin-b-5">
                                find your path
                            </p>
                            <p class="txt-sm">
                                Dancing? Programming? whatever your skill, we can hook you up.
                            </p>
                        </div>
                        <div class="col-md-3 padding-lr-30 padding-b-30">
                            <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/get_a_job.png" alt="Icon" width="40"></figure>
                            <p class="txt-normal-s txt-bold uppercase margin-b-5">
                                Get A Job
                            </p>
                            <p class="txt-sm">
                                Lets connect you to employers looking for people like you.
                            </p>
                        </div>
                        <div class="col-md-3 padding-lr-30 padding-b-30">
                            <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/start_your_business.png" alt="Icon" width="40"></figure>
                            <p class="txt-normal-s txt-bold uppercase margin-b-5">
                                start your business
                            </p>
                            <p class="txt-sm">
                                Get hooked up to opportunities & work experience by doing good.
                            </p>
                        </div>
                        <div class="col-md-3 padding-lr-30 padding-b-30">
                            <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/find_a_trainer.png" alt="Icon" width="40"></figure>
                            <p class="txt-normal-s txt-bold uppercase margin-b-5">
                                Find a Trainer
                            </p>
                            <p class="txt-sm">
                                Explore a nationwide pool of trainers accross different skills & subjects.
                            </p>
                        </div>
                        <div class="col-md-3 padding-lr-30 padding-b-30">
                            <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/join_ynl.png" alt="Icon" width="40"></figure>
                            <p class="txt-normal-s txt-bold uppercase margin-b-5">
                                Join the Young Leaders Network
                            </p>
                            <p class="txt-sm">
                                Dancing? Programming? whatever your skill, we can hook you up.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="padding-t-40 padding-b-20 border-b-1 border-color-white">
                    <header class="margin-b-40">
                        <h1 class="txt-xxlg txt-bold txt-height-1-1 uppercase margin-b-10">
                            Join a Job Pool
                        </h1>
                        <p>
                            If you have the skill for any of these roles, we will connect you to a job
                        </p>
                    </header>
                    <div class="row row-10 text-center txt-normal-s txt-bold">
                        <div class="col-md-3 padding-lr-10 padding-b-20">
                            <a class="d-block bg-ash uppercase txt-color-white padding-o-15" href="">
                                JOB POOL CARD
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-10 padding-b-20">
                            <a class="d-block bg-ash uppercase txt-color-white padding-o-15" href="">
                                JOB POOL CARD
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-10 padding-b-20">
                            <a class="d-block bg-ash uppercase txt-color-white padding-o-15" href="">
                                JOB POOL CARD
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-10 padding-b-20">
                            <a class="d-block bg-ash uppercase txt-color-white padding-o-15" href="">
                                JOB POOL CARD
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-10 padding-b-20">
                            <a class="d-block bg-ash uppercase txt-color-white padding-o-15" href="">
                                JOB POOL CARD
                            </a>
                        </div>
                        <div class="col-md-3 padding-lr-10 padding-b-20">
                            <a class="d-block bg-ash uppercase txt-color-white padding-o-15" href="">
                                JOB POOL CARD
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Featured opportunities -->
        <section class="container-wrapper margin-b-60">
            <div class="container-wrapper half-bg-yellow">
                <header class="d-flex justify-content-between margin-b-30">
                    <h1 class="txt-xxlg txt-bold txt-height-1-1 txt-color-white uppercase margin-b-10">
                        Featured opportunities
                    </h1>
                    <p class="margin-b-10">
                        <a class="txt-bold txt-color-dark" href="https://www.saedconnect.org/opportunity-center/"><u>View All</u></a>
                    </p>
                </header>
                <div class="home-slide-section">
                    <div class="featured-opportunities">
                        <div class="swiper-wrapper">
                        <?php wp_reset_postdata();
                            switch_to_blog(12);

                            wp_reset_query();
                            $temp = $wp_query; $wp_query= null;
                            $wp_query = new WP_Query();
                            $wp_query->query( 
                                array(
                                    'post_type' => 'opportunity',
                                    'post_status' => 'publish',
                                    'posts_per_page' => 4,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'featured',
                                            'value' => '1422',
                                        ),
                                        array(
                                            'key' => $publication_key,
                                            'value' => 'admin_published'
                                        )
                                    ),
                                ) 
                            );

                            if ( $wp_query->have_posts() ) {

                                while ($wp_query->have_posts()) : $wp_query->the_post();

                                /* Variables */
                                $program_id = $post->ID;    //Get Program ID

                                /* Get Entry */
                                /* GF Search Criteria */


                                /* Get GF Entry Count */

                                /* Get expiry date */
                                $expiry_date = rwmb_meta( 'opportunity-close-date' );
                                $field = 'submit_opportunity_expires';
                                $exp = 2;
                                $td = 1;


                                if( rwmb_meta( 'opportunity-close-date' ) ){

                                    $expiry_date = rwmb_meta( 'opportunity-close-date' );
                                    $today_date = date('y-m-d');

                                    $exp = strtotime( $expiry_date );
                                    $td = strtotime( $today_date );

                                } else if( get_post_meta($post->ID, $field, true) ) {


                                    $meta = get_post_meta($post->ID, $field, true);

                                    if($meta){
                                        $expiry_date = $meta;
                                    }

                                    $today_date = date('m/d/y');

                                    $expiry_date = strtotime( $expiry_date );
                                    $td = strtotime( $today_date );

                                    /*echo "<br><br><br>back-today: ".$exp;
                                    echo "<br>Expire: ".$td;*/

                                }

                                /* Check if expired */
                                if( true ){
                                /*if( $td < $expiry_date ){*/

                        ?>
                            <a href="<?php the_permalink() ?>" class="swiper-slide">
                                <div class="content">
                                    <h4 class="txt-height-1-4 txt-bold uppercase margin-b-15"><?php 
                                            $meta = get_the_title();
                                            if($meta){
                                                echo truncate($meta, 40);
                                            }
                                        ?></h4>
                                    <p class="txt-sm txt-height-1-7">
                                        <?php 
                                            $meta = get_post_meta( $program_id, 'summary', true );
                                            if($meta){
                                                echo truncate($meta, 110);
                                            }
                                        
                                        ?>
                                    </p>
                                </div>
                                <?php 
                                    $images = "";
                                    $images = get_attached_media( 'image', $program_id ); 
                                ?>
            
                                <?php if($images){ ?>

                                    <?php  
                                        foreach($images as $image) { //print_r( $image ); 
                                            $previousImg_id = $image->ID;
                                    ?>
                                        <figure class="image margin-b-30" style="background-image: url('<?php echo wp_get_attachment_url($image->ID,'full'); ?>')"></figure>

                                    <?php } ?>

                                <?php } else { ?>
                                   
                                    <figure class="image margin-b-30" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png')"></figure>

                                <?php } ?>
                                
                                
                                <div class="footer d-flex justify-content-between">
                                    <p class="txt-bold">Job</p>
                                    <p>
                                        <?php 
                                            $deadline = get_post_meta( $program_id, 'deadline', true );

                                            if( $deadline ){
                                        ?>
                                                Expires
                                            <?php
                                                $field = 'submit_opportunity_expires';
                                                $date = strtotime( $deadline );

                                                echo date('j F Y', $date);
                                            ?>
                                        <?php } ?>
                                    </p>
                                </div>
                            </a>
                            
                        <?php
                                }
                                endwhile;

                            } else {
                        ?>
                            <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                                <h2 class="txt-lg txt-medium">
                                    No Opportunities found.
                                </h2>
                            </div>   

                        <?php

                            }

                            restore_current_blog();
                        ?>
                        </div>
                        <!-- Add Navigation -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </section> 
        
        <!-- Corporates & Youth Development Organizations -->
        <section class="container-wrapper padding-b-40">
            <header class="container-wrapper margin-b-40">
                <h1 class="txt-xxlg txt-bold txt-height-1-1 uppercase margin-b-10">
                    Corporates & Youth Development Organizations
                </h1>
                <p>
                    If you have the skill for any of these roles, we will connect you to a job
                </p>
            </header>
            <div class="container-wrapper bg-ash padding-t-40 padding-b-10">
                <div class="row row-30 txt-color-white">
                    <div class="col-md-3 padding-lr-30 padding-b-30">
                        <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/hire_talent.png" alt="Icon" width="35"></figure>
                        <p class="txt-normal-s txt-bold uppercase margin-b-5">
                            Hire Talent
                        </p>
                        <p class="txt-sm">
                            Dancing? Programming? whatever your skill, we can hook you up.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-30 padding-b-30">
                        <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/hire_freelancer.png" alt="Icon" width="35"></figure>
                        <p class="txt-normal-s txt-bold uppercase margin-b-5">
                            Hire Freelancers to get things done
                        </p>
                        <p class="txt-sm">
                            Dancing? Programming? whatever your skill, we can hook you up.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-30 padding-b-30">
                        <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/minds.png" alt="Icon" width="35"></figure>
                        <p class="txt-normal-s txt-bold uppercase margin-b-5">
                            Tap into the minds of young creative people
                        </p>
                        <p class="txt-sm">
                            Dancing? Programming? whatever your skill, we can hook you up.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-30 padding-b-30">
                        <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/campaign.png" alt="Icon" width="35"></figure>
                        <p class="txt-normal-s txt-bold uppercase margin-b-5">
                            Run Nationwide Campaigns
                        </p>
                        <p class="txt-sm">
                            Dancing? Programming? whatever your skill, we can hook you up.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-30 padding-b-30">
                        <figure class="margin-b-15"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/sell.png" alt="Icon" width="35"></figure>
                        <p class="txt-normal-s txt-bold uppercase margin-b-5">
                            sell your training
                        </p>
                        <p class="txt-sm">
                            Dancing? Programming? whatever your skill, we can hook you up.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        
        
        
        
        
        <!--<div class="pre-footer lg" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (22).jpg')">
            <div class="content">
                <h4 class="title">
                    Empower People, Transform Lives
                </h4>
                <p class="txt-height-1-4 txt-color-white margin-b-40">
                    The world needs more problem solvers. Your support will create possibilities and amplify impact.
                </p>
                <article class="btn-wrapper">
                    <a class="btn btn-trans-wb icon" href="">
                        Contribute
                    </a>
                    <a class="btn btn-trans-wb" href="">
                        Support Youth Development
                    </a>
                    <a class="btn btn-white" href="">
                        Donate Online
                    </a>
                </article>
            </div>
        </div>-->
    </main>
    
    <?php get_footer() ?>

