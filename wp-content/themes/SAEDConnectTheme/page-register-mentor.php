    <?php /*Template Name: Register - Mentor Page*/ ?>
    
    <?php
                    
        if ( is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            //$dashboard_link = get_site_url().'/my-dashboard'; //Get Daasboard Page Link by ID
            $dashboard_link = network_home_url().'/growth-programs/my-dashboard'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header() ?>
    
    <main class="main-content">
        <section class="container-wrapper padding-tb-80">
            <div class="row row-40">
                <div class="col-md-6 padding-lr-40">
                    <header class="margin-b-40">
                        <h1 class="txt-2em txt-bold margin-b-20">
                            Sign Up
                        </h1>
                        <h2>
                            Create your Mentor account on SAEDConnect.
                            <span class="txt-sm">
                                Already have an account?
                            </span>
                            <a class="txt-sm txt-medium txt-color-green" href="<?php echo network_home_url().'/login'; ?>">Click here to Login</a>
                        </h2>
                    </header>
                    <div class="login-form">
                    <?php
                        echo do_shortcode( "[gravityform id='4' title='false' description='false' ajax='false']"); 
                    ?>
                    </div>
                </div>
                <div class="col-md-6 padding-lr-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/art.png" alt="">
                </div>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>
    