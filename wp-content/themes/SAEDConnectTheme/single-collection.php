    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
   
    <main class="main-content">
        <!-- Section Header -->
        <header class="overview-header container-wrapper">
            <?php
                /* Get Content for this view */
                /* Variables to store data */
                $sectionName;
                $section_id;
                $section_theme_color;
                $section_hover_color;
            
                $multiPageID;

                $topicName;
                $topicID;
                $sponsorName;
                $sponsorImage;

                $multiItemName = get_the_title();
                $articleTitle = get_the_title();
                $articleID = get_the_ID();
            ?>

            <?php 
                /* Second Query to get Topic Name */
                $topicNameQuery = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'og_topic_to_ex_multi_item',
                        'to' => get_the_ID(), // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post(); ?>   
                    <?php
                        $topicName = rwmb_get_value( 'topic-page-name' );
                        $topicID = get_the_ID();
                        $images = rwmb_meta( 'sponsor-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                        $sponsorImage = $image['full_url'];
                    ?>

                    <?php 
                    /* Third Query to get Section Name */
                    $sectionNameQuery = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'og_section_to_og_topic',
                            'to' => get_the_ID(), // You can pass object ID or full object
                        ),
                        'nopaging' => true,
                    ) );
                    while ( $sectionNameQuery->have_posts() ) : $sectionNameQuery->the_post(); ?>   
                        <?php 
                            $sectionName = get_the_title();
                            $section_id = get_the_ID();
                            $section_theme_color = rwmb_meta( 'section-theme-color' );
                            $section_hover_color = rwmb_meta( 'section-link-hover-color' );
                        ?>

                <?php
                    endwhile;
                    wp_reset_postdata();
                    /* THird Query: END */
                ?>

            <?php
                endwhile;
                wp_reset_postdata();
                /* Second Query: END */
            ?>

            <div class="info-box">
                <div class="info">
                    <h2 class="subtitle">
                        <?php echo $sectionName ?>
                    </h2>
                    <h1 class="title txt-color-lighter">
                        <?php echo $topicName ?>
                    </h1>
                </div>
                <div class="sponsor">
                    <div class="intro-text">
                        Content Sponsored by:
                    </div>
                    <img class="logo" src="<?php echo $sponsorImage ?>" alt="">
                </div>
            </div>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <!-- Get All Three column Posts related to the Parent Topic -->
                    <?php 
                        /* Third Query to get Section Name */
                        $subNavQuery = new WP_Query( array(
                            'relationship' => array(
                                'id'   => 'og_topic_to_three_c_m_page',
                                'from' => $topicID, // You can pass object ID or full object
                            ),
                            'nopaging' => true,
                        ) );
                        while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   
                        
                        <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                            <a href="<?php the_permalink() ?>">
                                <?php the_title() ?>
                            </a>
                        </li>

                    <?php
                        endwhile;
                        wp_reset_postdata();
                        /* THird Query: END */
                    ?>
                    
                    <!-- Get All Three column Posts related to the Parent Topic -->
                    <?php 
                        /* Third Query to get Section Name */
                        $subNavQuery = new WP_Query( array(
                            'relationship' => array(
                                'id'   => 'og_topic_to_two_c_m_page',
                                'from' => $topicID, // You can pass object ID or full object
                            ),
                            'nopaging' => true,
                        ) );
                        while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   
                        
                        <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                            <a href="<?php the_permalink() ?>">
                                <?php the_title() ?>
                            </a>
                        </li>

                    <?php
                        endwhile;
                        wp_reset_postdata();
                        /* THird Query: END */
                    ?>
                    
                    <!-- Get All External Multi-item Posts related to the Parent Topic -->
                    <?php 
                        /* Third Query to get Section Name */
                        $subNavQuery = new WP_Query( array(
                            'relationship' => array(
                                'id'   => 'og_topic_to_ex_multi_item',
                                'from' => $topicID, // You can pass object ID or full object
                            ),
                            'nopaging' => true,
                        ) );
                        while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   
                        
                        <li class="<?php echo ($multiItemName == get_the_title()  ? 'active' : '') ?>">
                            <a href="<?php the_permalink() ?>">
                                <?php the_title() ?>
                            </a>
                        </li>

                    <?php
                        endwhile;
                        wp_reset_postdata();
                        /* THird Query: END */
                    ?>
                </ul>
            </nav>
        </header>
        <style>
            a{
                color: <?php echo $section_theme_color; ?>;
            }

            a:active {
                color: <?php echo $section_theme_color; ?>;
            }

            a:hover {
                color: <?php echo $section_hover_color; ?>;
            }

            .overview-header .header-nav li.active a {
                color: <?php echo $section_theme_color; ?>;
            }

            .overview-header .header-nav li.active a:before {
                background-color: <?php echo $section_theme_color; ?>;
            }
        </style>
        <section class="container-wrapper padding-t-40">
            <div class="row row-20">
                <?php 
                    
                    $connected = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'ex_multi_item_to_ex_multi_item_cb',
                            'from' => get_the_ID(), // You can pass object ID or full object
                        ),
                        'nopaging'     => true,
                    ) );
                    while ( $connected->have_posts() ) : $connected->the_post();

                ?>
                    <div class="col-md-3 advice-card padding-lr-20 margin-b-30">
                        <?php
                            $images = rwmb_meta( 'related-advice-image', array( 'limit' => 1 ) );
                            $image = reset( $images );
                        ?>
                        <figure>
                            <img class="rounded-corners" src="<?php echo $image['full_url']; ?>" alt="">
                        </figure>
                        <figcaption>
                            <h3 class="txt-medium padding-tb-10 margin-b-20">
                                <a class="" href="<?php echo rwmb_get_value( 'related-advice-source-url' ); ?>"><?php the_title() ?></a>
                            </h3>
                        </figcaption>
                        <p class="subtitle txt-normal-s txt-color-lighter">
                            <?php echo rwmb_get_value( 'related-advice-source' ); ?>
                        </p>
                    </div>
                
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </section>
        <!--<section class="container-wrapper padding-tb-40 border-tb-1 border-color-darkgrey">
            <div class="btn-wrapper text-center">
                <a class="btn btn-trans-green margin-r-40" href="">
                    Show More
                </a>
            </div>
        </section>-->
    </main>
    
    <?php endwhile; // end of the loop. ?>    

    <?php get_footer() ?>