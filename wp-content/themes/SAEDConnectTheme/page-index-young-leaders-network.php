<?php /*Template Name: Homepage-Young-Learders-Network*/ ?>
    
<?php get_header() ?>

<main class="font-main txt-color-light bg-white" style="margin-top: 100px">
    <header class="course-directory-banner image sm" style="background-image:url('http://www.saedconnect.org/learn-a-skill/wp-content/themes/SAEDConnectTheme/images/grid/image (41).jpg');">
        <div class="content container-wrapper txt-color-white text-center">
            <h1 class="title">
                Young Leaders Network
            </h1>
            <h2>
                Showcase Your Trainings & Youth Development Services on SAEDConnect
            </h2>
        </div>
    </header>

    <section class="container-wrapper padding-t-40">
        <div class="row row-5">
        <?php
            function truncate($string, $length){
                if (strlen($string) > $length) {
                    $string = substr($string, 0, $length) . '...';
                }

                return $string;
            }

            wp_reset_postdata();
            wp_reset_query();
            $temp = $wp_query; $wp_query= null;
            $wp_query = new WP_Query();
            $wp_query->query( 
                array(
                    'post_type' => 'project',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                ) 
            );

            if ( $wp_query->have_posts() ) {

                while ($wp_query->have_posts()) : $wp_query->the_post();

                /* Variables */
                $post_id = $post->ID;    //Get Program ID

                $program_images = rwmb_meta( 'change-program-image', array( 'limit' => 1 ) );
                $program_images = reset( $program_images );
        ?>
            <div class="col-md-3 padding-lr-5 padding-b-20 d-flex">
                <a href="<?php the_permalink() ?>" class="home-community-card">
                    <article class="content">
                        <figure class="image-box" style="background-image: url('<?php echo $program_images['full_url']; ?>')">

                        </figure>
                        <div class="info bg-white">
                            <h3 class=" txt-medium margin-b-10">
                                <?php the_title() ?>
                            </h3>
                            <p class="txt-sm">
                                <?php
                                    $meta = get_post_meta($post_id, 'business_description', true);
                                    if($meta){
                                        echo truncate($meta, 70);
                                    }
                                ?>
                            </p>
                        </div>
                    </article>  
                </a>   
            </div>
        <?php
                endwhile;

            }else{
        ?>
            <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                <h2 class="txt-lg txt-medium">
                    No Businesses found.
                </h2>
            </div>   

        <?php

            }

            restore_current_blog();
        ?>
        </div>
    </section>

</main>
   
<?php get_footer() ?>