<?php get_header() ?>

<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.main-content').fadeIn();
        $('.select-collaborators').select2({
            dropdownParent: $("#instuctorsModal")
        });
    });
</script>

<?php 
    /* Get Image Field */
    function get_the_image_url($post_id){
        $images = get_attached_media( 'image', $post_id );
        $previousImg_id = 0;
        if($images){
            foreach($images as $image) { 
                $previousImg_id = $image->ID;
            }
            return wp_get_attachment_url($previousImg_id,"full");
        } else {
            return 0;
        }
    }
?>

<?php
    /* Get Current User */
    $current_user = wp_get_current_user();
    
    /* Manager view array */
    $sdg_icons_array = array(
        'No Poverty' => '1.png',
        'Zero Hunger' => '2.jpg',
        'Good Health and Well-being' => '3.png',
        'Quality Education' => '4.png',
        'Gender Equality' => '5.png',
        'Clean Water and Sanitation' => '6.png',
        'Affordable and Clean Energy' => '7.png',
        'Decent Work and Economic Growth' => '8.png',
        'Industry, Innovation and Infrastructure' => '9.jpg',
        'Reduced Inequality' => '10.png',
        'Sustainable Cities and Communities' => '.png.jpg',
        'Responsible Consumption and Production' => '12.png',
        'Climate Action' => '13.jpg',
        'Life Below Water' => '14.jpg',
        'Life on Land' => '15.png',
        'Peace and Justice Strong Institutions' => '16.jpg',
        'Partnerships to achieve the Goal' => '17.png',
    );

    /* Get Queried Post Object */
    while ( have_posts() ) : the_post();

        /* Get Post Data */
        $post_id = get_the_ID(); //ID
        $theme_id = get_the_ID(); //ID
        $post_type = get_post_type(); //Post Type
        $post_title = get_the_title(); //Title
        $post_link = get_permalink(); //Post Link
        $post_author_id = get_the_author_meta('ID'); //Author ID

        $images = rwmb_meta( 'featured_image', array( 'limit' => 1 ) );
        $image = reset( $images );

    endwhile; //resetting the page loop

    /*$challenges_query = new WP_Query( array(
        'relationship' => array(
            'id'   => 'thematic_area_to_problem_area',
            'from' => $post_id, // You can pass object ID or full object
        ),
        'posts_per_page' => -1,
    ) );*/

    $challenges_query = new WP_Query( array(
        'post_type' => 'problem-area',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'parent_post',
                'value' => $post_id,
            ),
        ),
    ) );

      
?>

    <main class="main-content">
        <header class="position-relative">
            <section class="padding-tb-60">
                <div class="container-wrapper">
                    <div class="row">
                        <div class="col-lg-10 mx-auto position-static">
                            <div class="row row-40">
                                <div class="col-lg-6 padding-lr-40">
                                    <h1 class="txt-2em txt-height-1-2 txt-light uppercase padding-b-15">
                                        <?php echo $post_title; ?>
                                    </h1>
                                    <h2 class="txt-normal-s">
                                        <span class="txt-bold">Thematic Categories:</span>
                                        <?php 
                                            $term_list = wp_get_post_terms( $post->ID, 'thematic-category', array( 'fields' => 'names' ) );
                                            foreach ( $term_list as $key => $term_item ){
                                                echo $term_item;
                                                echo ( $key < count($term_list) - 1 ) ? ', ' : ''; 
                                            } 
                                        ?>
                                    </h2>
                                    <div class="d-inline-flex">
                                        <div class="d-inline-block padding-t-40 margin-r-10 align-items-center">
                                            <a class="btn btn-green txt-normal-s no-m-b" data-toggle="modal" href="#addIdeaModal">Add Your Voice</a>
                                            
                                        </div>
                                        <div class="padding-t-40">
                                            <a class="btn btn-trans-green txt-normal-s no-m-b" href="https://www.saedconnect.org/ventures-directory/create-project/">Submit a Project</a>
                                        </div>
                                        <!--<div class="padding-t-40">
                                            <a class="btn btn-trans-green txt-normal-s no-m-b" data-toggle="modal" href="#championProgramFormModal">Volunteer</a>
                                        </div>-->
                                    </div>
                                    <p>
                                        <a class="d-block txt-sm padding-t-20" data-toggle="modal" href="#aboutIdeasModal">
                                            <u>What we do with your contributions</u>
                                        </a>
                                    </p>
                                    <div class="modal fade font-main filter-modal" id="aboutIdeasModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog .modal-dialog-centered modal-lg" role="document">
                                            <div class="modal-content">
                                                <form action="<?php echo currentUrl(true).'?view=form-tasks'; ?>" method="post">
                                                    <div class="modal-header padding-lr-40">
                                                        <h5 class="modal-title" id="exampleModalLabel">From Ideas to Impact. How Ideas Become projects</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body padding-o-40">
                                                        <article>
                                                            <p class="txt-medium txt-height-1-7 padding-b-10">What we do with ideas you submit</p>
                                                            <p class="txt-sm txt-height-1-7 padding-b-30">Your submitted ideas can be voted on by the public. A professional review team also tracks every idea submitted and the comments made and combines these input with their professional  knowledge to decide whether or not to turn an idea into a project. </p>
                                                            
                                                            <p class="txt-medium txt-height-1-7 padding-b-10">After an idea has been selected to become a project</p>
                                                            <p class="txt-sm txt-height-1-7 padding-b-30">The selected idea or group of ideas can then be developed into actual project solutions (by our development partners, working in collaboration with idea champions). The final project blue print will be published as a guideline for anyone interested in implementing in their community.</p>
                                                            
                                                            <p class="txt-medium txt-height-1-7 padding-b-10">Project Implementation & Impact</p>
                                                            <p class="txt-sm txt-height-1-7 padding-b-30">We then mobilise our nationwide network of young leaders to implement the project in their local communities. As much as possible, we will do our best to provide our grass root implementing champions with all the tools and support required to deploy the solution where they are and provide feedback to the project coordinators.</p>
                                                        </article>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 padding-lr-40 ynl-thematic-header-image">
                                    <img src="<?php echo $image['full_url']; ?>" alt="Featured Image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="bg-green padding-tb-60">
                <div class="container-wrapper">
                    <div class="row">
                        <div class="col-lg-10 mx-auto overflow-hidden">
                            <div class="row">
                                <div class="col-lg-6">
                                    <p class="txt-xlg txt-color-white txt-light txt-height-1-7">
                                        <?php echo rwmb_meta( 'summary' ); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </header>
        
        <section class="bg-ash padding-t-20">
            <div class="container-wrapper">
                <div class="row">
                    <nav class="col-lg-10 mx-auto position-static">
                        <ul class="nav nav-pills ynl-thematic-tabs" id="pills-tab" role="tablist">
                            <li class="nav-item padding-b-20">
                                <a class="nav-link" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="false">About</a>
                            </li>
                            <!--<li class="nav-item padding-b-20">
                                <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="false">Volunteer</a>
                            </li>-->
                            <!--<li class="nav-item padding-b-20">
                                <a class="nav-link" id="pills-5-tab" data-toggle="pill" href="#pills-5" role="tab" aria-controls="pills-5" aria-selected="false">Add your voice</a>
                            </li>-->
                            <li class="nav-item padding-b-20">
                                <a class="nav-link active" id="pills-3-tab" data-toggle="pill" href="#pills-3" role="tab" aria-controls="pills-3" aria-selected="true">Voices</a>
                            </li>
                            <li class="nav-item padding-b-20">
                                <a class="nav-link" id="pills-4-tab" data-toggle="pill" href="#pills-4" role="tab" aria-controls="pills-4" aria-selected="false">Spotlight Projects</a>
                            </li>
                            <li class="nav-item padding-b-20">
                                <a class="nav-link" id="pills-6-tab" data-toggle="pill" href="#pills-6" role="tab" aria-controls="pills-6" aria-selected="false">Tasks</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </section>
            
        <div class="tab-content" id="pills-tabContent">
           
            <!-- Voices / Comments -->
            <div class="tab-pane fade" id="pills-5" role="tabpanel" aria-labelledby="pills-5-tab">
                <section class="container-wrapper padding-tb-80">
                    <div class="row">
                        <div class="col-lg-10 mx-auto overflow-hidden">
                            <h4 class="txt-xlg txt-light txt-height-1-4 padding-b-40">
                                Program Champions recognizes the urgency of a program area and are willing to actively participate to make things better in that area. If you want to work with other young leaders to [program name], please submit your name as a program champion and we will reach out to you whenever there are new activities and events under this program.
                            </h4>
                            <?php comments_template() ?>
                        </div>
                    </div>
                </section>
            </div>  
            
            <!-- About -->
            <div class="tab-pane fade" id="pills-1" role="tabpanel" aria-labelledby="pills-1-tab"> 
                <section class="bg-ghostwhite padding-tb-80">
                    <div class="container-wrapper">
                        <div class="row">
                            <div class="col-lg-10 mx-auto">
                                <h2 class="txt-xlg uppercase padding-b-30">
                                    THE NEEDS OF THIS PROJECT
                                </h2>
                                <article class="text-box txt-normal-s">
                                    <?php echo rwmb_meta( 'description' ); ?>
                                </article>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="padding-tb-60">
                    <div class="container-wrapper">
                        <div class="row">
                            <div class="col-lg-10 mx-auto">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h3 class="txt-lg txt-color-light txt-height-1-5 margin-b-30">Creating impact in this thematic area will contribute to the following SDGs</h3>
                                    </div>
                                </div>
                                <div>
                                    <?php 
                                        $sdgs = rwmb_meta( 'sdg' );
                                        foreach( $sdgs as $sdg ){
                                    ?>
                                    <img class="d-inline-block margin-r-30" width="60" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/sdgs/<?php echo $sdg_icons_array[$sdg] ?>" alt="<?php echo $sdg ?>">
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="bg-ghostwhite padding-tb-60">
                    <div class="container-wrapper">
                        <div class="row">
                            <div class="col-lg-10 mx-auto">
                                <h2 class="txt-xlg uppercase padding-b-20">
                                    Challenge Focus Areas
                                </h2>
                                <p class=" txt-color-white">Select any of the projects</p>
                                <?php 
                                    while ( $challenges_query->have_posts() ) : $challenges_query->the_post(); 
                                        $challenge_id = get_the_ID();
                                        $images = 0;
                                        $images = rwmb_meta( 'featured_image', array( 'limit' => 1 ) );
                                        $image = reset( $images );
                                ?>

                                    <div class="row row-20 padding-b-40 margin-b-40 border-b-1 border-color-lighter">
                                        <div class="col-lg-2 padding-lr-20">
                                            <img src="<?php echo $image['full_url']; ?>" alt="">
                                        </div>
                                        <div class="col-lg-10 padding-lr-20">
                                            <h3 class="txt-bold padding-b-20"><?php the_title() ?></h3>
                                            <article class="text-box txt-normal-s txt-color-light">
                                                <p><?php echo rwmb_meta( 'summary' ); ?></p>
                                            </article>
                                            <div class="collapse" id="challengCollapse-<?php echo $challenge_id; ?>">
                                                <article class="text-box txt-normal-s txt-color-light padding-tb-40 margin-t-40 border-t-1 border-color-lighter">
                                                    <?php echo rwmb_meta( 'description' ); ?>
                                                </article>
                                            </div>
                                            <div class="padding-t-20">
                                                <a class="btn btn-green txt-sm no-m-b" data-toggle="collapse" href="#challengCollapse-<?php echo $challenge_id; ?>" role="button" aria-expanded="false" aria-controls="challengCollapse-<?php echo $challenge_id; ?>">
                                                    Read More
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="bg-ghostwhite container-wrapper padding-t-40 padding-t-30">
                    <div class="row">
                        <div class="col-lg-10 mx-auto">
                            <header class="txt-center margin-b-20">
                                <h2 class="txt-2em uppercase text-center">Different Ways to Engage</h2>    
                            </header>
                        </div>
                    </div>
                </section>
            </div>
            
            <!-- Champion Form -->
            <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-3-tab">
                <div class="container-wrapper padding-tb-80">
                    <div class="row">
                        <div class="col-lg-10 mx-auto">
                            <div class="row">
                                <div class="col-lg-8">
                                    <form action="<?php echo currentUrl(true).'?view=form-champion-program'; ?>" method="post">
                                        <div class="">
                                            <?php
                                                if( $_POST && $_GET['view'] == 'form-champion-program' ){

                                                    /* Get Post Name */
                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                                    /* Meta */
                                                    $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                                    $email = sanitize_text_field( $_POST['email'] ); 
                                                    $location = sanitize_text_field( $_POST['location'] ); 
                                                    $implementation = sanitize_text_field( $_POST['implementation'] ); 
                                                    $educational_status = sanitize_text_field( $_POST['educational_status'] ); 
                                                    $contribution = wp_kses_post( $_POST['contribution'] );

                                                    /* Save Post to DB */
                                                    $champion_id = wp_insert_post(array (
                                                        'post_type' => 'idea-champion',
                                                        'post_title' => $post_name,
                                                        'post_content' => "",
                                                        'post_status' => 'publish',
                                                    ));

                                                    update_post_meta( $champion_id, 'thematic_area', $post_id );
                                                    //update_post_meta( $champion_id, 'problem_area', $post_id );

                                                    update_post_meta( $champion_id, 'telephone', $telephone );
                                                    update_post_meta( $champion_id, 'email', $email );
                                                    update_post_meta( $champion_id, 'location', $location );
                                                    update_post_meta( $champion_id, 'implementation', $implementation );
                                                    update_post_meta( $champion_id, 'educational_status', $educational_status );
                                                    update_post_meta( $champion_id, 'contribution', $contribution );
                                                    
                                                    /* Update Mailing List */
                                                    /* Data */
                                                    switch_to_blog(110);
                                                    $state = get_term( $location );
                                                    restore_current_blog();
                                                    
                                                    $api_key = '0fkxtL9crlJFX7cqCXUV';
                                                    $master_list = 'YdUPrXEfWxjULk9AsCRbPQ'; //Master

                                                    /* Endpoints */
                                                    $subscribe_url = 'http://maylbox.com/subscribe';

                                                    /* Subscribe Body */
                                                    $body = array(
                                                        'name' => $post_name,
                                                        'email' => $email,
                                                        'Phone' => $telephone,
                                                        'Location' => $state->name,
                                                        'PreviousImplementation' => $implementation,
                                                        'EducationalStatus' => $educational_status,
                                                        'contribution' => $contribution,
                                                        'ThematicArea' => $post_title,
                                                        'list' => $master_list,
                                                        'boolean' => 'true'
                                                    );

                                                    $args = array(
                                                        'body' => $body,
                                                        'timeout' => '5',
                                                        'redirection' => '5',
                                                        'httpversion' => '1.0',
                                                        'blocking' => true,
                                                        'headers' => array(),
                                                        'cookies' => array()
                                                    );

                                                    $response = wp_remote_post( $subscribe_url, $args );
                                                    $http_code = wp_remote_retrieve_response_code( $response );
                                                    $body = wp_remote_retrieve_body( $response );
                                                    /* Update Mailing List End */

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                                }
                                            ?>

                                            <h3 class="txt-2em txt-light uppercase padding-b-20">
                                                Volunteer
                                            </h3>

                                            <div class="margin-b-30 padding-tb-15 border-b-1 border-color-darkgrey">
                                                <p class="txt-normal-s txt-height-1-7">
                                                    Program volunteers recognizes the urgency of a program area and are willing to actively participate to make things better in that area. If you want to work with other young leaders to <span class="text-lowercase"><?php echo $post_title ?></span> , please submit your name as a program volunteer and we will reach out to you whenever there are new activities and events under this program.
                                                </p>
                                            </div>

                                            <div class="form margin-b-40">
                                                <div class="row row-10">
                                                    <!-- Name -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="post_name">Name</label>
                                                        <input 
                                                            type="text" 
                                                            name="post_name" 
                                                        >
                                                    </div>
                                                    <!-- Telephone -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="telephone">Telephone</label>
                                                        <input 
                                                            type="telephone" 
                                                            name="telephone" 
                                                        >
                                                    </div>
                                                    <!-- Email -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="email">Email</label>
                                                        <input 
                                                            type="email" 
                                                            name="email" 
                                                        >
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="location">Location</label>
                                                        <select name="location" id="location">
                                                        <?php 
                                                            switch_to_blog(110);
                                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                                            restore_current_blog();

                                                            foreach( $terms as $term ){
                                                        ?>
                                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                    <!-- Implementation -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="implementation">Have you implemented this program anywhere?</label>
                                                        <select name="implementation" id="implementation">
                                                            <option value="No, But i believe it will work">No</option>
                                                            <option value="I have started working on it, but haven't launched">Yes, I have led the implementation of this program in the past </option>
                                                            <option value="I have implemented the idea but at early stages">Yes, I have been part of a team that Implemented this program</option>
                                                        </select>
                                                    </div>
                                                    <!-- Educational Status -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="educational_status">Educational status</label>
                                                        <select name="educational_status" id="educational_status">
                                                            <option value="Secondary School Student">Secondary School Student</option>
                                                            <option value="Undergraduate">Undergraduate</option>
                                                            <option value="Graduate (Pre-NYSC)">Graduate (Pre-NYSC)</option>
                                                            <option value="NYSC Corps Member">NYSC Corps Member</option>
                                                            <option value="Graduate (Post NYSC)">Graduate (Post NYSC)</option>
                                                            <option value="Professional">Professional</option>
                                                        </select>
                                                    </div>
                                                    <!-- Contribution -->
                                                    <div class="col-lg-12 padding-lr-10 form-item">
                                                        <label for="contribution">How can you contribute to this program</label>
                                                        <textarea name="contribution" rows="8"></textarea>
                                                    </div>
                                                </div>

                                                <div class="padding-t-30">
                                                    <input type="submit" id="" class="btn btn-green txt-sm padding-lr-15">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Ideas -->
            <div class="tab-pane fade show active bg-grey-light" id="pills-3" role="tabpanel" aria-labelledby="pills-profile-2">
                <div class="container-wrapper padding-tb-80">
                    <header class="margin-b-40">
                        <h2 class="txt-xxlg txt-height-1-4 margin-b-20">Add your Voice to <?php echo $post_title ?></h2>
                        <p>Your ideas, suggestions and experiences are very critical to <?php echo $post_title ?>. It could be useful to facilitate new solutions or strengthen existing ones. Please add your voice to any of the questions below.</p>
                    </header>
                    <?php 
                        $question_query = new WP_Query( array(
                            'post_type' => 'theme-question',
                            'post_status' => 'publish',
                            'posts_per_page' => 5,
                            'meta_query' => array(
                                array(
                                    'key' => 'parent_post',
                                    'value' => $post_id,
                                ),
                            ),
                        ) ); 
                    ?>
                    
                    <?php if ( $question_query->have_posts() ) { ?>

                    <?php 
                        while ( $question_query->have_posts() ) : $question_query->the_post(); 
                            $question_id = get_the_ID();
                            $question_type = get_post_meta( $question_id, 'type', true );
    
                    ?>
                        
                        <!-- Question -->
                        <div class="bg-white padding-o-30 margin-b-20">
                            <h3 class="txt-lg">
                                <a class="d-flex align-items-center justify-content-between" data-toggle="collapse" href="#collapse-<?php echo $question_id ?>" role="button" aria-expanded="false" aria-controls="collapse-<?php echo $question_id ?>">
                                    <span class="txt-height-1-4 txt-medium txt-color-light ">
                                        <?php the_title(); ?>
                                    </span>
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                            </h3>
                            <div class="collapse" id="collapse-<?php echo $question_id ?>">
                                <div class="padding-lr-5 padding-t-40 margin-t-20 border-t-1 border-color-darkgrey">
                                <?php if( $question_type == 'Idea' ){ ?>
                        
                                    <!-- Idea -->
                                    <div class="row row-15">
                                        <div class="col-md-6 col-lg-3 col-xl-3 d-flex padding-lr-15 padding-b-30">
                                            <div class="ynl-theme-challenges-card" href="">
                                                <article class="content d-flex flex-column justify-content-between">
                                                    <h3 class="txt-xlg txt-height-1-4 txt-color-light padding-b-30">
                                                        <?php the_title(); ?>
                                                    </h3>
                                                    <div>
                                                        <a class="txt-sm d-block padding-b-15" data-toggle="modal" href="#guidelineModal">
                                                            <u>Review our idea submission guidelines</u>
                                                        </a>
                                                        <a 
                                                            class="btn btn-green txt-sm no-m-b answer-question" 
                                                            data-toggle="modal" 
                                                            href="#addIdeaModal"
                                                            data-question="<?php echo $question_id ?>"
                                                        >Submit an idea</a>
                                                    </div>
                                                </article>  
                                            </div>   
                                        </div>
                                        <?php 
                                            $idea_query = new WP_Query( array(
                                                'post_type' => 'idea',
                                                'post_status' => 'publish',
                                                'posts_per_page' => 3,
                                                'meta_query' => array(
                                                    array(
                                                        'key' => 'parent_post',
                                                        'value' => $question_id,
                                                    ),
                                                ),
                                            ) ); 
                                        ?>
                                        <?php if ( $idea_query->have_posts() ) { ?>

                                        <?php 
                                            while ( $idea_query->have_posts() ) : $idea_query->the_post(); 
                                                $idea_id = get_the_ID();
                                        ?>

                                        <div class="col-md-6 col-lg-3 col-xl-3 d-flex padding-lr-15 padding-b-30">
                                            <div class="ynl-theme-challenges-card" href="">
                                                <article class="content d-flex flex-column justify-content-between">
                                                    <div>
                                                        <h3 class="txt-lg txt-medium txt-color-green txt-height-1-4 txt-color-light padding-b-15">
                                                            <a class="txt-color-green" href="<?php the_permalink() ?>"><?php echo the_title() ?></a>
                                                        </h3>
                                                        <div class="txt-sm txt-height-1-7 padding-b-60">
                                                            <?php 
                                                                $meta = get_post_meta($idea_id, 'description', true);
                                                                if($meta){
                                                                    echo truncate($meta, 200);
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="padding-b-15">
                                                            <!--<a href="#" class="d-inline-flex padding-r-15 align-items-center txt-color-green txt-sm">
                                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/up_vote.png" alt="" height="14">
                                                                <span class="txt-bold padding-l-5">0</span>
                                                            </a>
                                                            <a href="#" class="d-inline-flex padding-r-15 align-items-center txt-color-lighter txt-sm">
                                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/down_vote.png" alt="" height="14">
                                                                <span class="txt-bold padding-l-5">0</span>
                                                            </a>-->
                                                            <a href="<?php the_permalink() ?>/?tab=2" class="d-inline-flex padding-r-15 align-items-center txt-color-green txt-sm">
                                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/voices.png" alt="" height="14">
                                                                <span class="txt-bold padding-l-5">
                                                                    <?php comments_number( '0', '1', '%' ); ?>
                                                                </span>
                                                            </a>
                                                            <a href="<?php the_permalink() ?>/?tab=2" class="d-inline-flex padding-r-15 align-items-center txt-color-green txt-sm">
                                                                <span class="txt-xlg" style="color:#00e65d"><i class="fa fa-flag-checkered"></i></span>
                                                                <span class="txt-bold padding-l-5">
                                                                <?php 
                                                                    $champion_query = new WP_Query( array(
                                                                        'post_type' => 'idea-champion',
                                                                        'post_status' => 'publish',
                                                                        'posts_per_page' => -1,
                                                                        'meta_query' => array(
                                                                            array(
                                                                                'key' => 'problem_area',
                                                                                'value' => $idea_id,
                                                                            ),
                                                                        ),
                                                                    ) );   

                                                                    echo $champion_query->found_posts;
                                                                ?>
                                                                </span>
                                                            </a>
                                                        </div>
                                                        <div class="d-flex align-items-center justify-content-between padding-t-15 border-t-1 border-color-darkgrey">
                                                            <!--<span class="d-inline-flex padding-r-10 align-items-center txt-color-green txt-sm">
                                                                <a data-toggle="modal" href="#championIdeaModal"><i class="fa  fa-info-circle"></i></a>
                                                                <a data-toggle="modal" href="#championIdeaFormModal-<?php echo $idea_id ?>"><u class="padding-l-5">Champion this idea</u></a>
                                                            </span>-->
                                                            <span class="d-inline-flex padding-r-10 align-items-center txt-color-green txt-sm">
                                                                <a data-toggle="modal" href="#addVoiceModal"><i class="fa  fa-info-circle"></i></a>
                                                                <a href="<?php the_permalink() ?>/?tab=2"><u class="padding-l-5">Add your view</u></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </article>  
                                            </div>   
                                        </div>

                                        <!-- Champion Idea -->
                                        <div class="modal fade font-main filter-modal" id="championIdeaFormModal-<?php echo $idea_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form action="<?php echo currentUrl(true).'?view=form-champion-idea-'.$idea_id; ?>" method="post">
                                                        <div class="modal-header padding-lr-30">
                                                            <h5 class="modal-title" id="exampleModalLabel">Champion <?php echo the_title() ?></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body padding-o-30">
                                                            <?php
                                                                if( $_POST && $_GET['view'] == 'form-champion-idea-'.$idea_id ){

                                                                    /* Get Post Name */
                                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                                                    /* Meta */
                                                                    $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                                                    $email = sanitize_text_field( $_POST['email'] ); 
                                                                    $location = sanitize_text_field( $_POST['location'] ); 
                                                                    $implementation = sanitize_text_field( $_POST['implementation'] ); 
                                                                    $educational_status = sanitize_text_field( $_POST['educational_status'] ); 
                                                                    $contribution = wp_kses_post( $_POST['contribution'] );

                                                                    /* Save Post to DB */
                                                                    $champion_id = wp_insert_post(array (
                                                                        'post_type' => 'idea-champion',
                                                                        'post_title' => $post_name,
                                                                        'post_content' => "",
                                                                        'post_status' => 'publish',
                                                                    ));

                                                                    update_post_meta( $champion_id, 'thematic_area', $post_id );
                                                                    update_post_meta( $champion_id, 'problem_area', $idea_id );

                                                                    update_post_meta( $champion_id, 'telephone', $telephone );
                                                                    update_post_meta( $champion_id, 'email', $email );
                                                                    update_post_meta( $champion_id, 'location', $location );
                                                                    update_post_meta( $champion_id, 'implementation', $implementation );
                                                                    update_post_meta( $champion_id, 'educational_status', $educational_status );
                                                                    update_post_meta( $champion_id, 'contribution', $contribution );

                                                                    /* Update Mailing List */
                                                                    /* Data */
                                                                    switch_to_blog(110);
                                                                    $state = get_term( $location );
                                                                    restore_current_blog();

                                                                    $api_key = '0fkxtL9crlJFX7cqCXUV';
                                                                    $master_list = 'YdUPrXEfWxjULk9AsCRbPQ'; //Master

                                                                    /* Endpoints */
                                                                    $subscribe_url = 'http://maylbox.com/subscribe';

                                                                    /* Subscribe Body */
                                                                    $body = array(
                                                                        'name' => $post_name,
                                                                        'email' => $email,
                                                                        'Phone' => $telephone,
                                                                        'Location' => $state->name,
                                                                        'PreviousImplementation' => $implementation,
                                                                        'EducationalStatus' => $educational_status,
                                                                        'contribution' => $contribution,
                                                                        'ThematicArea' => get_the_title($thematic_area_id),
                                                                        'ChallengeArea' => $post_title,
                                                                        'list' => $master_list,
                                                                        'boolean' => 'true'
                                                                    );

                                                                    $args = array(
                                                                        'body' => $body,
                                                                        'timeout' => '5',
                                                                        'redirection' => '5',
                                                                        'httpversion' => '1.0',
                                                                        'blocking' => true,
                                                                        'headers' => array(),
                                                                        'cookies' => array()
                                                                    );

                                                                    $response = wp_remote_post( $subscribe_url, $args );
                                                                    $http_code = wp_remote_retrieve_response_code( $response );
                                                                    $body = wp_remote_retrieve_body( $response );
                                                                    /* Update Mailing List End */

                                                                    /* Redirect */
                                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                                                }
                                                            ?>

                                                            <h3 class="txt-2em txt-light padding-b-20">
                                                                CHAMPION THIS IDEA
                                                            </h3>

                                                            <div class="margin-b-30 padding-tb-15 border-b-1 border-color-darkgrey">
                                                                <p class="txt-normal-s txt-height-1-7">
                                                                    Program Champions recognizes the urgency of a program area and are willing to actively participate to make things better in that area. If you want to work with other young leaders to <?php echo $post_title ?>, please submit your name as a program champion and we will reach out to you whenever there are new activities and events under this program.
                                                                </p>
                                                            </div>

                                                            <div class="form margin-b-40">
                                                                <div class="row row-10">
                                                                    <!-- Name -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="post_name">Your Name</label>
                                                                        <input 
                                                                            type="text" 
                                                                            name="post_name" 
                                                                        >
                                                                    </div>
                                                                    <!-- Telephone -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="telephone">Your Phone</label>
                                                                        <input 
                                                                            type="telephone" 
                                                                            name="telephone" 
                                                                        >
                                                                    </div>
                                                                    <!-- Email -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="email">Your Email</label>
                                                                        <input 
                                                                            type="email" 
                                                                            name="email" 
                                                                        >
                                                                    </div>
                                                                    <!-- Location -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="location">Where do you live?</label>
                                                                        <select name="location" id="location">
                                                                        <?php 
                                                                            switch_to_blog(110);
                                                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                                                            restore_current_blog();

                                                                            foreach( $terms as $term ){
                                                                        ?>
                                                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                                                        <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Implementation -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="implementation">Have you implemented this idea anywhere?</label>
                                                                        <select name="implementation" id="implementation">
                                                                            <option value="No, But i believe it will work">No</option>
                                                                            <option value="I have started working on it, but haven't launched">Yes, I have led the implementation of this idea in the past </option>
                                                                            <option value="I have implemented the idea but at early stages">Yes, I have been part of a team that Implemented this idea</option>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Idea Implementation -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="educational_status">Educational status</label>
                                                                        <select name="educational_status" id="educational_status">
                                                                            <option value="Secondary School Student">Secondary School Student</option>
                                                                            <option value="Undergraduate">Undergraduate</option>
                                                                            <option value="Graduate (Pre-NYSC)">Graduate (Pre-NYSC)</option>
                                                                            <option value="NYSC Corps Member">NYSC Corps Member</option>
                                                                            <option value="Graduate (Post NYSC)">Graduate (Post NYSC)</option>
                                                                            <option value="Professional">Professional</option>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Contribution -->
                                                                    <div class="col-lg-12 padding-lr-10 form-item">
                                                                        <label for="contribution">How can you contribute to this idea</label>
                                                                        <textarea name="contribution" rows="8"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer padding-lr-30">
                                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                            <input type="submit" class="btn btn-green txt-sm padding-lr-15">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <?php endwhile; ?>

                                        <?php } ?>
                                    </div>

                                <?php }elseif( $question_type == 'Experience'){ ?>

                                    <!-- Experience -->
                                    <div>
                                        <?php 
                                            $idea_query = new WP_Query( array(
                                                'post_type' => 'idea',
                                                'post_status' => 'publish',
                                                'posts_per_page' => 3,
                                                'meta_query' => array(
                                                    array(
                                                        'key' => 'parent_post',
                                                        'value' => $question_id,
                                                    ),
                                                ),
                                            ) ); 
                                        ?>
                                        <?php if ( $idea_query->have_posts() ) { ?>

                                        <?php 
                                            while ( $idea_query->have_posts() ) : $idea_query->the_post(); 
                                                $idea_id = get_the_ID();
                                        ?>

                                        <div class="padding-b-30 margin-b-30 border-b-1 border-color-darkgrey">
                                            <article class="text-box txt-sm txt-height-1-7">
                                                <p> <?php echo get_post_meta($idea_id, 'experience_description', true); ?> </p>
                                            </article>  
                                        </div>

                                        <!-- Champion Idea -->
                                        <div class="modal fade font-main filter-modal" id="championIdeaFormModal-<?php echo $idea_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form action="<?php echo currentUrl(true).'?view=form-champion-idea-'.$idea_id; ?>" method="post">
                                                        <div class="modal-header padding-lr-30">
                                                            <h5 class="modal-title" id="exampleModalLabel">Champion <?php echo the_title() ?></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body padding-o-30">
                                                            <?php
                                                                if( $_POST && $_GET['view'] == 'form-champion-idea-'.$idea_id ){

                                                                    /* Get Post Name */
                                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                                                    /* Meta */
                                                                    $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                                                    $email = sanitize_text_field( $_POST['email'] ); 
                                                                    $location = sanitize_text_field( $_POST['location'] ); 
                                                                    $implementation = sanitize_text_field( $_POST['implementation'] ); 
                                                                    $educational_status = sanitize_text_field( $_POST['educational_status'] ); 
                                                                    $contribution = wp_kses_post( $_POST['contribution'] );

                                                                    /* Save Post to DB */
                                                                    $champion_id = wp_insert_post(array (
                                                                        'post_type' => 'idea-champion',
                                                                        'post_title' => $post_name,
                                                                        'post_content' => "",
                                                                        'post_status' => 'publish',
                                                                    ));

                                                                    update_post_meta( $champion_id, 'thematic_area', $post_id );
                                                                    update_post_meta( $champion_id, 'problem_area', $idea_id );

                                                                    update_post_meta( $champion_id, 'telephone', $telephone );
                                                                    update_post_meta( $champion_id, 'email', $email );
                                                                    update_post_meta( $champion_id, 'location', $location );
                                                                    update_post_meta( $champion_id, 'implementation', $implementation );
                                                                    update_post_meta( $champion_id, 'educational_status', $educational_status );
                                                                    update_post_meta( $champion_id, 'contribution', $contribution );

                                                                    /* Update Mailing List */
                                                                    /* Data */
                                                                    switch_to_blog(110);
                                                                    $state = get_term( $location );
                                                                    restore_current_blog();

                                                                    $api_key = '0fkxtL9crlJFX7cqCXUV';
                                                                    $master_list = 'YdUPrXEfWxjULk9AsCRbPQ'; //Master

                                                                    /* Endpoints */
                                                                    $subscribe_url = 'http://maylbox.com/subscribe';

                                                                    /* Subscribe Body */
                                                                    $body = array(
                                                                        'name' => $post_name,
                                                                        'email' => $email,
                                                                        'Phone' => $telephone,
                                                                        'Location' => $state->name,
                                                                        'PreviousImplementation' => $implementation,
                                                                        'EducationalStatus' => $educational_status,
                                                                        'contribution' => $contribution,
                                                                        'ThematicArea' => get_the_title($thematic_area_id),
                                                                        'ChallengeArea' => $post_title,
                                                                        'list' => $master_list,
                                                                        'boolean' => 'true'
                                                                    );

                                                                    $args = array(
                                                                        'body' => $body,
                                                                        'timeout' => '5',
                                                                        'redirection' => '5',
                                                                        'httpversion' => '1.0',
                                                                        'blocking' => true,
                                                                        'headers' => array(),
                                                                        'cookies' => array()
                                                                    );

                                                                    $response = wp_remote_post( $subscribe_url, $args );
                                                                    $http_code = wp_remote_retrieve_response_code( $response );
                                                                    $body = wp_remote_retrieve_body( $response );
                                                                    /* Update Mailing List End */

                                                                    /* Redirect */
                                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                                                }
                                                            ?>

                                                            <h3 class="txt-2em txt-light padding-b-20">
                                                                CHAMPION THIS IDEA
                                                            </h3>

                                                            <div class="margin-b-30 padding-tb-15 border-b-1 border-color-darkgrey">
                                                                <p class="txt-normal-s txt-height-1-7">
                                                                    Program Champions recognizes the urgency of a program area and are willing to actively participate to make things better in that area. If you want to work with other young leaders to <?php echo $post_title ?>, please submit your name as a program champion and we will reach out to you whenever there are new activities and events under this program.
                                                                </p>
                                                            </div>

                                                            <div class="form margin-b-40">
                                                                <div class="row row-10">
                                                                    <!-- Name -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="post_name">Your Name</label>
                                                                        <input 
                                                                            type="text" 
                                                                            name="post_name" 
                                                                        >
                                                                    </div>
                                                                    <!-- Telephone -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="telephone">Your Phone</label>
                                                                        <input 
                                                                            type="telephone" 
                                                                            name="telephone" 
                                                                        >
                                                                    </div>
                                                                    <!-- Email -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="email">Your Email</label>
                                                                        <input 
                                                                            type="email" 
                                                                            name="email" 
                                                                        >
                                                                    </div>
                                                                    <!-- Location -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="location">Where do you live?</label>
                                                                        <select name="location" id="location">
                                                                        <?php 
                                                                            switch_to_blog(110);
                                                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                                                            restore_current_blog();

                                                                            foreach( $terms as $term ){
                                                                        ?>
                                                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                                                        <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Implementation -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="implementation">Have you implemented this idea anywhere?</label>
                                                                        <select name="implementation" id="implementation">
                                                                            <option value="No, But i believe it will work">No</option>
                                                                            <option value="I have started working on it, but haven't launched">Yes, I have led the implementation of this idea in the past </option>
                                                                            <option value="I have implemented the idea but at early stages">Yes, I have been part of a team that Implemented this idea</option>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Idea Implementation -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="educational_status">Educational status</label>
                                                                        <select name="educational_status" id="educational_status">
                                                                            <option value="Secondary School Student">Secondary School Student</option>
                                                                            <option value="Undergraduate">Undergraduate</option>
                                                                            <option value="Graduate (Pre-NYSC)">Graduate (Pre-NYSC)</option>
                                                                            <option value="NYSC Corps Member">NYSC Corps Member</option>
                                                                            <option value="Graduate (Post NYSC)">Graduate (Post NYSC)</option>
                                                                            <option value="Professional">Professional</option>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Contribution -->
                                                                    <div class="col-lg-12 padding-lr-10 form-item">
                                                                        <label for="contribution">How can you contribute to this idea</label>
                                                                        <textarea name="contribution" rows="8"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer padding-lr-30">
                                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                            <input type="submit" class="btn btn-green txt-sm padding-lr-15">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <?php endwhile; ?>

                                        <?php }else{ ?>
                                        
                                        <p class="padding-b-20">Be the first to share your experience.</p>
                                        
                                        <?php } ?>
                                    </div>
                                    <div class="padding-t-20">
                                        <a 
                                            class="btn btn-green txt-sm no-m-b margin-r-10 answer-question" 
                                            data-toggle="modal" 
                                            href="#addExperienceModal"
                                            data-question="<?php echo $question_id ?>"
                                        >Share your Experience</a>
                                        <a class="btn btn-trans-green txt-sm no-m-b" href="<?php get_the_permalink($question_id) ?>">
                                            View Stories
                                        </a>
                                    </div>
                                    
                                <?php }elseif( $question_type == 'Suggestion' ){ ?>  
                                    
                                     <!-- Suggestion -->
                                     <div class="row row-15">
                                        
                                        <div class="col-md-6 col-lg-3 col-xl-3 d-flex padding-lr-15 padding-b-30">
                                            <div class="ynl-theme-challenges-card" href="">
                                                <article class="content d-flex flex-column justify-content-between">
                                                    <h3 class="txt-xlg txt-height-1-4 txt-color-light padding-b-30">
                                                        <?php the_title(); ?>
                                                    </h3>
                                                    <div>
                                                        
                                                        <a 
                                                            class="btn btn-green txt-sm no-m-b answer-question" 
                                                            data-toggle="modal" 
                                                            href="#addSuggestionModal"
                                                            data-question="<?php echo $question_id ?>"
                                                        >Make a Suggestion</a>
                                                    </div>
                                                </article>  
                                            </div>   
                                        </div>
                                        
                                        <?php 
                                            $idea_query = new WP_Query( array(
                                                'post_type' => 'idea',
                                                'post_status' => 'publish',
                                                'posts_per_page' => 3,
                                                'meta_query' => array(
                                                    array(
                                                        'key' => 'parent_post',
                                                        'value' => $question_id,
                                                    ),
                                                ),
                                            ) ); 
                                        ?>
                                        <?php if ( $idea_query->have_posts() ) { ?>

                                        <?php 
                                            while ( $idea_query->have_posts() ) : $idea_query->the_post(); 
                                                $idea_id = get_the_ID();
                                        ?>

                                        <div class="col-md-6 col-lg-3 col-xl-3 d-flex padding-lr-15 padding-b-30">
                                            <div class="ynl-theme-challenges-card" href="">
                                                <article class="content d-flex flex-column justify-content-between">
                                                    <div>
                                                        <!--<h3 class="txt-lg txt-medium txt-color-green txt-height-1-4 txt-color-light padding-b-15">
                                                            <a class="txt-color-green" href="<?php the_permalink() ?>"><?php echo the_title() ?></a>
                                                        </h3>-->
                                                        <p class="txt-sm txt-height-1-7 padding-b-60">
                                                            <?php echo get_post_meta($idea_id, 'suggestion_description', true); ?>
                                                        </p>
                                                    </div>
                                                </article>  
                                            </div>   
                                        </div>

                                        <!-- Champion Idea -->
                                        <div class="modal fade font-main filter-modal" id="championIdeaFormModal-<?php echo $idea_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form action="<?php echo currentUrl(true).'?view=form-champion-idea-'.$idea_id; ?>" method="post">
                                                        <div class="modal-header padding-lr-30">
                                                            <h5 class="modal-title" id="exampleModalLabel">Champion <?php echo the_title() ?></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body padding-o-30">
                                                            <?php
                                                                if( $_POST && $_GET['view'] == 'form-champion-idea-'.$idea_id ){

                                                                    /* Get Post Name */
                                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                                                    /* Meta */
                                                                    $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                                                    $email = sanitize_text_field( $_POST['email'] ); 
                                                                    $location = sanitize_text_field( $_POST['location'] ); 
                                                                    $implementation = sanitize_text_field( $_POST['implementation'] ); 
                                                                    $educational_status = sanitize_text_field( $_POST['educational_status'] ); 
                                                                    $contribution = wp_kses_post( $_POST['contribution'] );

                                                                    /* Save Post to DB */
                                                                    $champion_id = wp_insert_post(array (
                                                                        'post_type' => 'idea-champion',
                                                                        'post_title' => $post_name,
                                                                        'post_content' => "",
                                                                        'post_status' => 'publish',
                                                                    ));

                                                                    update_post_meta( $champion_id, 'thematic_area', $post_id );
                                                                    update_post_meta( $champion_id, 'problem_area', $idea_id );

                                                                    update_post_meta( $champion_id, 'telephone', $telephone );
                                                                    update_post_meta( $champion_id, 'email', $email );
                                                                    update_post_meta( $champion_id, 'location', $location );
                                                                    update_post_meta( $champion_id, 'implementation', $implementation );
                                                                    update_post_meta( $champion_id, 'educational_status', $educational_status );
                                                                    update_post_meta( $champion_id, 'contribution', $contribution );

                                                                    /* Update Mailing List */
                                                                    /* Data */
                                                                    switch_to_blog(110);
                                                                    $state = get_term( $location );
                                                                    restore_current_blog();

                                                                    $api_key = '0fkxtL9crlJFX7cqCXUV';
                                                                    $master_list = 'YdUPrXEfWxjULk9AsCRbPQ'; //Master

                                                                    /* Endpoints */
                                                                    $subscribe_url = 'http://maylbox.com/subscribe';

                                                                    /* Subscribe Body */
                                                                    $body = array(
                                                                        'name' => $post_name,
                                                                        'email' => $email,
                                                                        'Phone' => $telephone,
                                                                        'Location' => $state->name,
                                                                        'PreviousImplementation' => $implementation,
                                                                        'EducationalStatus' => $educational_status,
                                                                        'contribution' => $contribution,
                                                                        'ThematicArea' => get_the_title($thematic_area_id),
                                                                        'ChallengeArea' => $post_title,
                                                                        'list' => $master_list,
                                                                        'boolean' => 'true'
                                                                    );

                                                                    $args = array(
                                                                        'body' => $body,
                                                                        'timeout' => '5',
                                                                        'redirection' => '5',
                                                                        'httpversion' => '1.0',
                                                                        'blocking' => true,
                                                                        'headers' => array(),
                                                                        'cookies' => array()
                                                                    );

                                                                    $response = wp_remote_post( $subscribe_url, $args );
                                                                    $http_code = wp_remote_retrieve_response_code( $response );
                                                                    $body = wp_remote_retrieve_body( $response );
                                                                    /* Update Mailing List End */

                                                                    /* Redirect */
                                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                                                }
                                                            ?>

                                                            <h3 class="txt-2em txt-light padding-b-20">
                                                                CHAMPION THIS IDEA
                                                            </h3>

                                                            <div class="margin-b-30 padding-tb-15 border-b-1 border-color-darkgrey">
                                                                <p class="txt-normal-s txt-height-1-7">
                                                                    Program Champions recognizes the urgency of a program area and are willing to actively participate to make things better in that area. If you want to work with other young leaders to <?php echo $post_title ?>, please submit your name as a program champion and we will reach out to you whenever there are new activities and events under this program.
                                                                </p>
                                                            </div>

                                                            <div class="form margin-b-40">
                                                                <div class="row row-10">
                                                                    <!-- Name -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="post_name">Your Name</label>
                                                                        <input 
                                                                            type="text" 
                                                                            name="post_name" 
                                                                        >
                                                                    </div>
                                                                    <!-- Telephone -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="telephone">Your Phone</label>
                                                                        <input 
                                                                            type="telephone" 
                                                                            name="telephone" 
                                                                        >
                                                                    </div>
                                                                    <!-- Email -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="email">Your Email</label>
                                                                        <input 
                                                                            type="email" 
                                                                            name="email" 
                                                                        >
                                                                    </div>
                                                                    <!-- Location -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="location">Where do you live?</label>
                                                                        <select name="location" id="location">
                                                                        <?php 
                                                                            switch_to_blog(110);
                                                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                                                            restore_current_blog();

                                                                            foreach( $terms as $term ){
                                                                        ?>
                                                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                                                        <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Implementation -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="implementation">Have you implemented this idea anywhere?</label>
                                                                        <select name="implementation" id="implementation">
                                                                            <option value="No, But i believe it will work">No</option>
                                                                            <option value="I have started working on it, but haven't launched">Yes, I have led the implementation of this idea in the past </option>
                                                                            <option value="I have implemented the idea but at early stages">Yes, I have been part of a team that Implemented this idea</option>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Idea Implementation -->
                                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                                        <label for="educational_status">Educational status</label>
                                                                        <select name="educational_status" id="educational_status">
                                                                            <option value="Secondary School Student">Secondary School Student</option>
                                                                            <option value="Undergraduate">Undergraduate</option>
                                                                            <option value="Graduate (Pre-NYSC)">Graduate (Pre-NYSC)</option>
                                                                            <option value="NYSC Corps Member">NYSC Corps Member</option>
                                                                            <option value="Graduate (Post NYSC)">Graduate (Post NYSC)</option>
                                                                            <option value="Professional">Professional</option>
                                                                        </select>
                                                                    </div>
                                                                    <!-- Contribution -->
                                                                    <div class="col-lg-12 padding-lr-10 form-item">
                                                                        <label for="contribution">How can you contribute to this idea</label>
                                                                        <textarea name="contribution" rows="8"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer padding-lr-30">
                                                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                            <input type="submit" class="btn btn-green txt-sm padding-lr-15">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <?php endwhile; ?>

                                        <?php } ?>
                                                                                
                                    </div> 
                                      
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                        
                    <?php endwhile; ?>

                    <?php } ?>
                </div> 
                <section class="container-wrapper padding-t-40 padding-t-30">
                    <header class="txt-center margin-b-20">
                        <h2 class="txt-2em uppercase text-center">Different Ways to Engage</h2>    
                    </header>
                </section>
            </div>
            
            <!-- Projects -->
            <div class="tab-pane fade" id="pills-4" role="tabpanel" aria-labelledby="pills-4-tab">
                <div class="container-wrapper padding-tb-80">
                    <header class="margin-b-40">
                        <h2 class="txt-xxlg txt-height-1-4 margin-b-20">Explore Ongoing Projects to <?php echo $post_title ?></h2>
                        <p>Are you doing anything noteworthy to <?php echo $post_title ?>?  Post your project here and we will showcase it to supporters, volunteers and partners who can accelerate your success.</p>
                    </header>
                    <div class="row">
                        <div class="col-lg-12 mx-auto">
                            <?php switch_to_blog(103); ?>
                          
                            <?php 
                                /* Get Thematic Areas */
                                $projects_query = new WP_Query();
                                $projects_query->query( 
                                    array(
                                        'post_type' => 'personal-project',
                                        'post_status' => 'publish',
                                        'posts_per_page' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'publication_status',
                                                'value' => 'user_published',
                                            ),
                                            array(
                                                'key' => 'thematic_areas',
                                                'value' => $theme_id,
                                            )
                                        ),
                                    ) 
                                );
                            ?>
                            <?php if ( $projects_query->have_posts() ) { ?>

                                <div class="row row-15">
                                    <div class="col-md-6 col-lg-3 col-xl-3 d-flex padding-lr-15 padding-b-30">
                                        <div class="ynl-theme-challenges-card" href="">
                                            <article class="content d-flex flex-column justify-content-between">
                                                <h3 class="txt-xlg txt-height-1-4 txt-color-light padding-b-30">
                                                    Submit a project to <?php echo $post_title; ?>
                                                </h3>
                                                <div>
                                                    <a class="txt-sm d-block padding-b-15" data-toggle="modal" href="#guidelineModal">
                                                        <u>Review our project submission guidelines</u>
                                                    </a>
                                                    <a 
                                                        class="btn btn-green txt-sm no-m-b answer-question" 
                                                        href="https://www.saedconnect.org/ventures-directory/create-project/"
                                                    >Submit a Project</a>
                                                </div>
                                            </article>  
                                        </div>   
                                    </div>
                                <?php while ($projects_query->have_posts()) : $projects_query->the_post(); ?>
                            
                                    <div class="col-md-6 col-lg-3 col-xl-3 d-flex padding-lr-15 padding-b-30">
                                        <div class="info-program-card">            
                                            <?php 
                                                if( get_the_image_url($post->ID) ){
                                                    $featured_image_url = get_the_image_url($post->ID);
                                                } else {
                                                    $featured_image_url = get_bloginfo('stylesheet_directory')."/images/heroes/noimage.png";
                                                }
                                            ?>
                                            
                                            <figure style="background-image: url('<?php echo $featured_image_url; ?>')"></figure>
                                            <div class="content padding-o-15">
                                                <h4 class="txt-bold txt-height-1-4 padding-b-15">
                                                    <a href="<?php the_permalink() ?>" class="txt-color-dark">
                                                        <?php the_title(); ?>
                                                    </a>
                                                </h4>
                                                <p class="txt-normal-s">
                                                    <?php 
                                                        $meta = get_post_meta($post->ID, 'summary', true);
                                                        if($meta){
                                                            echo truncate($meta, 140);
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                
                                <?php endwhile; ?>
                                </div>
                            
                            <?php }else{ ?>
                               
                                <h3 class="txt-height-1-4">Live projects are currently unavailable.</h3>
                                
                            <?php } ?>
                            
                            <?php restore_current_blog(); ?>
                        </div>
                    </div>
                </div>
                <section class="container-wrapper padding-t-40 padding-t-30">
                    <header class="txt-center margin-b-20">
                        <h2 class="txt-2em uppercase text-center">Different Ways to Engage</h2>    
                    </header>
                </section>
            </div>
            
            <!-- Tasks -->
            <div class="tab-pane fade" id="pills-6" role="tabpanel" aria-labelledby="pills-6-tab">
                <div class="container-wrapper padding-tb-80">
                    <div class="row">
                        <div class="col-lg-10 mx-auto">
                            <header class="margin-b-40">
                                <h2 class="txt-xxlg txt-height-1-4 margin-b-20">Here are something’s you can do to <?php echo $post_title ?></h2>
                                <p>We have compiled a series of activities you can do wherever you are to contribute your quota to <?php echo $post_title ?>. In some cases, there are incentives to be won.</p>
                            </header>
                            <?php 
                                $task_query = new WP_Query( array(
                                    'post_type' => 'theme-task',
                                    'post_status' => 'publish',
                                    'posts_per_page' => 5,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'parent_post',
                                            'value' => $post_id,
                                        ),
                                    ),
                                ) ); 
                            ?>
                            <?php if ( $task_query->have_posts() ) { ?>
                            <div class="row row-15">
                            <?php 
                                while ( $task_query->have_posts() ) : $task_query->the_post(); 
                                    $task_id = get_the_ID();
                                    $task_type = get_post_meta( $task_id, 'type', true );

                            ?>
  
                                <div class="col-md-6 col-lg-3 col-xl-4 d-flex padding-lr-15 padding-b-30">
                                    <div class="ynl-theme-challenges-card" href="">
                                        <article class="content d-flex flex-column justify-content-between" style="padding:20px">
                                            <div>
                                                <figure class="image-box" style="height:240px; background-image:url('https://www.saedconnect.org/young-leaders/wp-content/uploads/sites/104/2019/10/vawg.jpg')"></figure>
                                                <h3 class="txt-medium txt-color-green txt-height-1-4 uppercase padding-t-15 padding-b-30">
                                                    <a class="txt-color-green" href="<?php the_permalink() ?>"><?php echo the_title() ?></a>
                                                </h3>
                                                <div class="d-flex justify-content-between txt-normal-s">
                                                    <p class="txt-color-green"><?php echo $task_type ?></p>
                                                    <p class="txt-color-red">
                                                        <span class="txt-medium">Runs until</span>
                                                        <?php 
                                                            $date = strtotime(get_post_meta( $task_id, 'expiry_date', true ));
                                                            echo date('j F Y', $date);
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </article>  
                                    </div>   
                                </div>
                            
                            <?php endwhile; ?>
                            </div>
                            <?php } else { ?>
                            
                                <h3 class="txt-height-1-4">Tasks are currently unavailable.</h3>
                                
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Different ways to support -->
        <section class="bg-ash txt-color-white padding-tb-40">
            <div class="container-wrapper">
                <div class="row row-20">
                    <div class="col-lg-4 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/submit idea.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Join the Network</h2> 
                        <p class="txt-sm">Enroll as a young leader and automatically get first hand notifications about opportunities to collaborate with  strategic partners to co-create, shape and deploy creative solutions to development challenges, nationwide.</p>  
                    </div>
                    <div class="col-lg-4 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/submit an idea.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Add your Voice</h2> 
                        <p class="txt-sm">share your experiences, give your ideas and suggestions on ways we can address identified challenges. Development partners will use these inputs to shape projects and might be particularly interested in working with you to bring your ideas to life.</p>  
                    </div>
                    <div class="col-lg-4 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/participate in a project.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Participate in a Task or Project</h2>   
                        <p class="txt-sm">When informed about open opportunities, (as all registered young leaders will be) join other young leaders to attend events, complete tasks or contribute to project implementation in any way you feel you have the capacity to.</p> 
                    </div>
                    <!--<div class="col-lg-3 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Support a project.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Support a Project</h2>  
                        <p class="txt-sm">You can support the implementation of a project an an individual, Government, Private sector company, NGO or development agency by providing expertise, donating resources or funding to make change happen at the grass roots, nationwide.</p>  
                    </div>-->
                </div>
            </div>
        </section>
        
        <!-- View other impact programs -->
        <section class="padding-tb-40">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <span class="txt-xxlg txt-light txt-height-1-4 padding-r-20">View other impact programs</span>
                        <a class="btn btn-green txt-sm no-m-b d-block d-lg-inline" href="https://www.saedconnect.org/young-leaders">View</a>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Partner -->
        <section class="bg-green padding-t-80 padding-b-40">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <header class="text-center padding-b-60">
                            <h3 class="txt-lg uppercase txt-color-white padding-b-20">Become a Partner</h3>
                            <h4 class="txt-2em txt-bold txt-height-1-2 txt-color-white">Join forces with us to help make an impact in communities across Nigeria. </h4>
                        </header>
                        <div class="ynl-partner-cards row-20">
                            <div class="col-lg-6 padding-lr-20 padding-b-40 ">
                                <div class="ynl-partner-card">
                                    <div class="bg-white padding-o-40">
                                        <div class="text-center">
                                            <h4 class="txt-xxlg txt-bold txt-color-green padding-b-20">Support a program/<br>project</h4>
                                            <p class="txt-normal-s padding-b-20">
                                                We welcome well meaning individual experts, Government, Private sector companies, NGO, Civil society organisations and development agencies to join us in tackling the world’s toughest challenges. You can support the YLN in several ways including
                                            </p>
                                            <p>
                                                <a class="txt-color-green txt-medium" data-toggle="collapse" href="#collapseBecomePartner" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                    <u>See How</u>
                                                </a>
                                            </p>
                                        </div>
                                        <div class="collapse" id="collapseBecomePartner">
                                            <article class="text-box txt-sm padding-t-20">
                                                <ul>
                                                    <li>Providing program/project development expertise</li>
                                                    <li>Donating implementation resources or funding to execute and scale any of the existing projects</li>
                                                </ul>                                        
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 padding-lr-20 padding-b-40 ">
                                <div class="ynl-partner-card">
                                    <div class="bg-white padding-o-40">
                                        <div class="text-center">
                                            <h4 class="txt-xxlg txt-bold txt-color-green padding-b-20">Co-create/co-execute your impact programs with us</h4>
                                            <p class="txt-normal-s padding-b-20">
                                                If you already have existing impact programs or projects which you would love to scale nationwide, we are very happy to work with you to bring it to life and leverage our nationwide network to scale it down to the grassroots nationwide
                                            </p>
                                            <p style="opacity: 0;">
                                                <a class="txt-color-green txt-medium" data-toggle="collapse" href="#collapseBecomePartner" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                    <u>See How</u>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $('.ynl-partner-card').mouseenter(function(){
                    $('.ynl-partner-card').addClass('shrink');
                    $(this).addClass('grow');
                });

                $('.ynl-partner-card').mouseleave(function(){
                    $('.ynl-partner-card').removeClass('shrink');
                    $('.ynl-partner-card').removeClass('grow');
                });
            </script>
        </section>
        
        
        
        
        <!-- Add Idea Foem -->
        <div class="modal fade font-main filter-modal" id="addIdeaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-addIdea'; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            <?php
                                if( $_POST && $_GET['view'] == 'form-addIdea' ){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $description = wp_kses_post( $_POST['description'] ); 
                                    $challenge_areas = $_POST['challenge_areas'];
                                    $challenge_description = wp_kses_post( $_POST['challenge_description'] ); 
                                    $solution_description = wp_kses_post( $_POST['solution_description'] ); 
                                    $implementation = wp_kses_post( $_POST['implementation'] ); 
                                    $agreementCheckbox= wp_kses_post( $_POST['agreementCheckbox'] );
                                    $name = sanitize_text_field( $_POST['name'] );
                                    $telephone = sanitize_text_field( $_POST['telephone'] );
                                    $email = sanitize_text_field( $_POST['email'] );
                                    $question_id = sanitize_text_field( $_POST['question_id'] );

                                    /* Save Post to DB */
                                    $idea_id = wp_insert_post(array (
                                        'post_type' => 'idea',
                                        'post_title' => $post_name,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $idea_id, 'thematic_area', $post_id );
                                    update_post_meta( $idea_id, 'parent_post', $question_id );

                                    update_post_meta( $idea_id, 'description', $description );
                                    update_post_meta( $idea_id, 'challenge_areas', $challenge_areas );
                                    update_post_meta( $idea_id, 'challenge_description', $challenge_description );
                                    update_post_meta( $idea_id, 'solution_description', $solution_description );
                                    update_post_meta( $idea_id, 'implementation', $implementation );
                                    update_post_meta( $idea_id, 'agreementCheckbox', $agreementCheckbox );
                                    update_post_meta( $idea_id, 'name', $name );
                                    update_post_meta( $idea_id, 'telephone', $telephone );
                                    update_post_meta( $idea_id, 'email', $email );

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                }
                            ?>

                            <h3 class="txt-xxlg txt-light padding-b-30">
                                Add an idea to <?php echo $post_title ?>
                            </h3>

                            <div class="margin-b-30 padding-tb-15">
                                <div class="txt-normal-s txt-medium">
                                    <a 
                                       class="d-flex align-items-center justify-content-between txt-normal-s dropdown-toggle padding-o-15 bg-green txt-color-white" 
                                       data-toggle="collapse" 
                                       href="#ideaGuidelines" aria-expanded="false" aria-controls="collapseOne"
                                    >
                                        <span>
                                            <i class="fa fa-info-circle"></i>
                                            View idea guidelines
                                        </span>
                                    </a>
                                </div>
                                <div id="ideaGuidelines" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                    <div class="padding-t-15">
                                        <article class="text-box txt-sm">
                                            <p class="txt-normal-s txt-height-1-7">We are excited that you have an idea to solve this challenge . To help us act on your idea, please ensure that:</p>
                                            <ol>
                                                <li>The idea directly solves one of the identified challenge areas related to <?php echo $post_title ?></li>
                                                <li>The idea can be replicated nationwide by anybody who is willing to adopt it.</li>
                                                <li>Please make your idea topic as descriptive as possible. People will decide whether or not to engage with your idea depending on how clear your topic is.</li>
                                                <li>Every idea submitted here will be subject to public view and can be adopted/adapted by anyone without recourse to the poster. This is the very essence of this platform - to crowdsource ideas and inspire people to take action on any one they are excited about. Please do not submit any idea that you consider as proprietary. We will not be held liable if your idea is adopted/adapted and implemented by any visitor to this site.</li>
                                            </ol>
                                        </article>
                                    </div>
                                </div>
                            </div>

                            <div class="form margin-b-40">
                                <div class="form-item">
                                    <label for="post_name">Title of your idea</label>
                                    <input 
                                        type="text" 
                                        name="post_name" 
                                        required
                                    >
                                </div>
                                <!-- Description -->
                                <div class="form-item">
                                    <label for="post-name">
                                        Describe the idea
                                    </label>
                                    <textarea name="description" rows="8" required><?php echo $description ?></textarea>
                                </div>
                                <!-- Challenge Description -->
                                <div class="form-item">
                                    <label for="challenge_description">
                                        Describe the specific challenge this idea will solve 
                                    </label>
                                    <textarea name="challenge_description" rows="8" required><?php echo $challenge_description ?></textarea>
                                </div>
                                <!-- Challenges -->
                                <div class="form-item">
                                    <label class="d-block padding-b-10">Which of these challenge areas is this idea related to?</label>
                                    <?php 
                                        while ( $challenges_query->have_posts() ) : $challenges_query->the_post(); 
                                            $challenge_id = get_the_ID();
                                    ?>
                                    <label class="txt-sm d-flex align-items-center padding-b-10 padding-r-15">
                                        <input
                                            class="custom-check"
                                            type="checkbox" 
                                            value="<?php echo $challenge_id ?>" 
                                            name="challenge_areas[]" 
                                        >
                                        <span class="font-weight-normal padding-l-10">
                                            <?php the_title() ?>
                                        </span>
                                    </label>
                                    <?php endwhile; ?>
                                </div>
                                <!-- Solution Description -->
                                <div class="form-item">
                                    <label for="solution_description">
                                        How will this idea reduce or eliminate the problem?
                                    </label>
                                    <textarea name="solution_description" rows="8" required><?php echo $solution_description ?></textarea>
                                </div>
                                <!-- Implementation -->
                                <div class="form-item">
                                    <label for="implementation">Have you implemented this idea anywhere?</label>
                                    <select name="implementation" id="implementation">
                                        <option value="No, But i believe it will work">No, But i believe it will work</option>
                                        <option value="I have started working on it, but haven't launched">I have started working on it, but haven't launched</option>
                                        <option value="I have implemented the idea but at early stages">I have implemented the idea but at early stages</option>
                                        <option value="I have implemented and it is working">I have implemented and it is working</option>
                                    </select>
                                </div>
                                <!-- Name -->
                                <div class="form-item">
                                    <label for="name">Your Name</label>
                                    <input 
                                        type="text" 
                                        name="name" 
                                        required
                                    >
                                </div>
                                <!-- Telephone -->
                                <div class="form-item">
                                    <label for="telephone">Your Phone</label>
                                    <input 
                                        type="telephone" 
                                        name="telephone" 
                                        required
                                    >
                                </div>
                                <!-- Email -->
                                <div class="form-item">
                                    <label for="email">Your Contact Email</label>
                                    <input 
                                        type="email" 
                                        name="email" 
                                        required
                                    >
                                </div>
                                <!-- Location -->
                                <div class="form-item">
                                    <label for="location">Where do you live?</label>
                                    <select name="location" id="location">
                                    <?php 
                                        switch_to_blog(110);
                                        $terms = get_terms( 'state', array('hide_empty' => false));
                                        restore_current_blog();

                                        foreach( $terms as $term ){
                                    ?>
                                        <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <!-- Dynamically set Question ID -->
                                <input type="number" name="question_id" class="question_id" value="0" hidden >
                                <!-- Accept Terms -->
                                <div class="form-item">
                                    <label class="txt-sm d-flex align-items-start padding-b-10 padding-r-15">
                                        <span class="flex_1">
                                            <input
                                                class="custom-check agreementCheckbox"
                                                type="checkbox" 
                                                value="<?php echo $challenge_id ?>" 
                                                name="agreementCheckbox"
                                            >
                                        </span>
                                        <span class="txt-normal-s txt-color-light txt-height-1-7 padding-l-20">
                                            By submitting this idea, I agree that the SAEDConnect program staff and selection committee are able to review the information I have posted. I also agree that I have taken care not to provide proprietary information. By submitting this idea, I also agree to the terms and conditions.
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" class="addIdeaButton btn btn-green txt-sm padding-lr-15" disabled>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Add Experience Foem -->
        <div class="modal fade font-main filter-modal" id="addExperienceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-addExperience'; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            <?php
                                if( $_POST && $_GET['view'] == 'form-addExperience' ){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $telephone = sanitize_text_field( $_POST['telephone'] );
                                    $email = sanitize_text_field( $_POST['email'] );
                                    $experience_description = wp_kses_post( $_POST['experience_description'] ); 
                                    $challenge_areas = $_POST['challenge_areas'];
                                    $location = sanitize_text_field( $_POST['location'] );
                                    $agreementCheckbox= wp_kses_post( $_POST['agreementCheckbox'] );
                                    
                                    $question_id = sanitize_text_field( $_POST['question_id'] );

                                    /* Save Post to DB */
                                    $idea_id = wp_insert_post(array (
                                        'post_type' => 'idea',
                                        'post_title' => $post_name,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $idea_id, 'thematic_area', $post_id );
                                    update_post_meta( $idea_id, 'parent_post', $question_id );

                                    update_post_meta( $idea_id, 'telephone', $telephone );
                                    update_post_meta( $idea_id, 'email', $email );
                                    update_post_meta( $idea_id, 'experience_description', $experience_description );
                                    update_post_meta( $idea_id, 'challenge_areas', $challenge_areas );
                                    update_post_meta( $idea_id, 'location', $location );
                                    
                                    update_post_meta( $idea_id, 'agreementCheckbox', $agreementCheckbox );;

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                }
                            ?>

                            <h3 class="txt-xxlg txt-light padding-b-30">
                                Share your experience on <?php echo $post_title ?>
                            </h3>

                            <div class="margin-b-30 padding-tb-15">
                                <div class="txt-normal-s txt-medium">
                                    <a 
                                       class="d-flex align-items-center justify-content-between txt-normal-s dropdown-toggle padding-o-15 bg-green txt-color-white" 
                                       data-toggle="collapse" 
                                       href="#ideaGuidelines" aria-expanded="false" aria-controls="collapseOne"
                                    >
                                        <span>
                                            <i class="fa fa-info-circle"></i>
                                            View idea guidelines
                                        </span>
                                    </a>
                                </div>
                                <div id="ideaGuidelines" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                    <div class="padding-t-15">
                                        <article class="text-box txt-sm">
                                            <p class="txt-normal-s txt-height-1-7">We are excited that you have an idea to solve this challenge . To help us act on your idea, please ensure that:</p>
                                            <ol>
                                                <li>The idea directly solves one of the identified challenge areas related to <?php echo $post_title ?></li>
                                                <li>The idea can be replicated nationwide by anybody who is willing to adopt it.</li>
                                                <li>Please make your idea topic as descriptive as possible. People will decide whether or not to engage with your idea depending on how clear your topic is.</li>
                                                <li>Every idea submitted here will be subject to public view and can be adopted/adapted by anyone without recourse to the poster. This is the very essence of this platform - to crowdsource ideas and inspire people to take action on any one they are excited about. Please do not submit any idea that you consider as proprietary. We will not be held liable if your idea is adopted/adapted and implemented by any visitor to this site.</li>
                                            </ol>
                                        </article>
                                    </div>
                                </div>
                            </div>

                            <div class="form margin-b-40">

                                <!-- Name -->
                                <div class="form-item">
                                    <label for="post_name">Your Name</label>
                                    <input 
                                        type="text" 
                                        name="post_name" 
                                        required
                                    >
                                </div>
                                <!-- Telephone -->
                                <div class="form-item">
                                    <label for="telephone">Your Phone</label>
                                    <input 
                                        type="telephone" 
                                        name="telephone" 
                                        required
                                    >
                                </div>
                                <!-- Email -->
                                <div class="form-item">
                                    <label for="email">Your Contact Email</label>
                                    <input 
                                        type="email" 
                                        name="email" 
                                        required
                                    >
                                </div>
                                <!-- Share your experience -->
                                <div class="form-item">
                                    <label for="experience_description">
                                        Share your experience
                                    </label>
                                    <textarea name="experience_description" rows="8" required><?php echo $experience_description ?></textarea>
                                </div>
                                <!-- Which of these areas is this story related to? -->
                                <div class="form-item">
                                    <label class="d-block padding-b-10">Which of these areas is this story related to?</label>
                                    <?php 
                                        while ( $challenges_query->have_posts() ) : $challenges_query->the_post(); 
                                            $challenge_id = get_the_ID();
                                    ?>
                                    <label class="txt-sm d-flex align-items-center padding-b-10 padding-r-15">
                                        <input
                                            class="custom-check"
                                            type="checkbox" 
                                            value="<?php echo $challenge_id ?>" 
                                            name="challenge_areas[]" 
                                        >
                                        <span class="font-weight-normal padding-l-10">
                                            <?php the_title() ?>
                                        </span>
                                    </label>
                                    <?php endwhile; ?>
                                </div>
                                <!-- Location -->
                                <div class="form-item">
                                    <label for="location">What state are you based?</label>
                                    <select name="location" id="location">
                                    <?php 
                                        switch_to_blog(110);
                                        $terms = get_terms( 'state', array('hide_empty' => false));
                                        restore_current_blog();

                                        foreach( $terms as $term ){
                                    ?>
                                        <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <!-- Dynamically set Question ID -->
                                <input type="number" name="question_id" class="question_id" value="0" hidden >
                                <!-- Accept Terms -->
                                <div class="form-item">
                                    <label class="txt-sm d-flex align-items-start padding-b-10 padding-r-15">
                                        <span class="flex_1">
                                            <input
                                                class="custom-check agreementCheckbox"
                                                type="checkbox" 
                                                value="<?php echo $challenge_id ?>" 
                                                name="agreementCheckbox"
                                            >
                                        </span>
                                        <span class="txt-normal-s txt-color-light txt-height-1-7 padding-l-20">
                                            By submitting this idea, I agree that the SAEDConnect program staff and selection committee are able to review the information I have posted. I also agree that I have taken care not to provide proprietary information. By submitting this idea, I also agree to the terms and conditions.
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-green txt-sm padding-lr-15 addIdeaButton" disabled>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Add Suggestion Foem -->
        <div class="modal fade font-main filter-modal" id="addSuggestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-addSuggestion'; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            <?php
                                if( $_POST && $_GET['view'] == 'form-addSuggestion' ){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $telephone = sanitize_text_field( $_POST['telephone'] );
                                    $email = sanitize_text_field( $_POST['email'] );
                                    $suggestion_description = wp_kses_post( $_POST['suggestion_description'] ); 
                                    $challenge_areas = $_POST['challenge_areas'];
                                    $location = sanitize_text_field( $_POST['location'] );
                                    $agreementCheckbox= wp_kses_post( $_POST['agreementCheckbox'] );
                                    
                                    $question_id = sanitize_text_field( $_POST['question_id'] );

                                    /* Save Post to DB */
                                    $idea_id = wp_insert_post(array (
                                        'post_type' => 'idea',
                                        'post_title' => $post_name,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $idea_id, 'thematic_area', $post_id );
                                    update_post_meta( $idea_id, 'parent_post', $question_id );

                                    update_post_meta( $idea_id, 'telephone', $telephone );
                                    update_post_meta( $idea_id, 'email', $email );
                                    update_post_meta( $idea_id, 'suggestion_description', $suggestion_description );
                                    update_post_meta( $idea_id, 'challenge_areas', $challenge_areas );
                                    update_post_meta( $idea_id, 'location', $location );
                                    
                                    update_post_meta( $idea_id, 'agreementCheckbox', $agreementCheckbox );;

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s?confirmation-message=true")</script>', currenturl(true) );
                                }
                            ?>

                            <h3 class="txt-xxlg txt-light padding-b-30">
                                Make a suggestion to <?php echo $post_title ?>
                            </h3>

                            <div class="margin-b-30 padding-tb-15">
                                <div class="txt-normal-s txt-medium">
                                    <a 
                                       class="d-flex align-items-center justify-content-between txt-normal-s dropdown-toggle padding-o-15 bg-green txt-color-white" 
                                       data-toggle="collapse" 
                                       href="#ideaGuidelines" aria-expanded="false" aria-controls="collapseOne"
                                    >
                                        <span>
                                            <i class="fa fa-info-circle"></i>
                                            View idea guidelines
                                        </span>
                                    </a>
                                </div>
                                <div id="ideaGuidelines" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                    <div class="padding-t-15">
                                        <article class="text-box txt-sm">
                                            <p class="txt-normal-s txt-height-1-7">We are excited that you have an idea to solve this challenge . To help us act on your idea, please ensure that:</p>
                                            <ol>
                                                <li>The idea directly solves one of the identified challenge areas related to <?php echo $post_title ?></li>
                                                <li>The idea can be replicated nationwide by anybody who is willing to adopt it.</li>
                                                <li>Please make your idea topic as descriptive as possible. People will decide whether or not to engage with your idea depending on how clear your topic is.</li>
                                                <li>Every idea submitted here will be subject to public view and can be adopted/adapted by anyone without recourse to the poster. This is the very essence of this platform - to crowdsource ideas and inspire people to take action on any one they are excited about. Please do not submit any idea that you consider as proprietary. We will not be held liable if your idea is adopted/adapted and implemented by any visitor to this site.</li>
                                            </ol>
                                        </article>
                                    </div>
                                </div>
                            </div>

                            <div class="form margin-b-40">
                                <!-- Name -->
                                <div class="form-item">
                                    <label for="post_name">Your Name</label>
                                    <input 
                                        type="text" 
                                        name="post_name" 
                                        required
                                    >
                                </div>
                                <!-- Telephone -->
                                <div class="form-item">
                                    <label for="telephone">Your Phone</label>
                                    <input 
                                        type="telephone" 
                                        name="telephone" 
                                        required
                                    >
                                </div>
                                <!-- Email -->
                                <div class="form-item">
                                    <label for="email">Your Contact Email</label>
                                    <input 
                                        type="email" 
                                        name="email" 
                                        required
                                    >
                                </div>
                                <!-- Share your Suggestion -->
                                <div class="form-item">
                                    <label for="suggestion_description">
                                        Share your Suggestion
                                    </label>
                                    <textarea name="suggestion_description" rows="8" required><?php echo $suggestion_description ?></textarea>
                                </div>
                                <!-- Which of these areas is this suggestion related to? -->
                                <div class="form-item">
                                    <label class="d-block padding-b-10">Which of these areas is this suggestion related to?</label>
                                    <?php 
                                        while ( $challenges_query->have_posts() ) : $challenges_query->the_post(); 
                                            $challenge_id = get_the_ID();
                                    ?>
                                    <label class="txt-sm d-flex align-items-center padding-b-10 padding-r-15">
                                        <input
                                            class="custom-check"
                                            type="checkbox" 
                                            value="<?php echo $challenge_id ?>" 
                                            name="challenge_areas[]" 
                                        >
                                        <span class="font-weight-normal padding-l-10">
                                            <?php the_title() ?>
                                        </span>
                                    </label>
                                    <?php endwhile; ?>
                                </div>
                                <!-- Location -->
                                <div class="form-item">
                                    <label for="location">What state are you based?</label>
                                    <select name="location" id="location">
                                    <?php 
                                        switch_to_blog(110);
                                        $terms = get_terms( 'state', array('hide_empty' => false));
                                        restore_current_blog();

                                        foreach( $terms as $term ){
                                    ?>
                                        <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <!-- Dynamically set Question ID -->
                                <input type="number" name="question_id" class="question_id" value="0" hidden >
                                <!-- Accept Terms -->
                                <div class="form-item">
                                    <label class="txt-sm d-flex align-items-start padding-b-10 padding-r-15">
                                        <span class="flex_1">
                                            <input
                                                class="custom-check agreementCheckbox"
                                                type="checkbox" 
                                                value="<?php echo $challenge_id ?>" 
                                                name="agreementCheckbox"
                                            >
                                        </span>
                                        <span class="txt-normal-s txt-color-light txt-height-1-7 padding-l-20">
                                            By submitting this idea, I agree that the SAEDConnect program staff and selection committee are able to review the information I have posted. I also agree that I have taken care not to provide proprietary information. By submitting this idea, I also agree to the terms and conditions.
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-green txt-sm padding-lr-15 addIdeaButton" disabled>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        
        <!-- Champion Program -->
        <div class="modal fade font-main filter-modal" id="championProgramFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-champion-program'; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            
                            <h3 class="txt-xxlg txt-light padding-b-30">
                                Volunteer to <span class="text-lowercase"><?php echo $post_title ?></span>
                            </h3>
                            
                            <div class="margin-b-30 padding-tb-15">
                                <p class="txt-sm">Program volunteers recognizes the urgency of a program area and are willing to actively participate to make things better in that area. If you want to work with other young leaders to <span class="text-lowercase"><?php echo $post_title ?></span> , please submit your name as a program volunteer and we will reach out to you whenever there are new activities and events under this program.</p>
                            </div>

                            <div class="form margin-b-40">
                                <div class="row row-10">
                                    <!-- Name -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="post_name">Name</label>
                                        <input 
                                            type="text" 
                                            name="post_name" 
                                        >
                                    </div>
                                    <!-- Telephone -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="telephone">Telephone</label>
                                        <input 
                                            type="telephone" 
                                            name="telephone" 
                                        >
                                    </div>
                                    <!-- Email -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="email">Email</label>
                                        <input 
                                            type="email" 
                                            name="email" 
                                        >
                                    </div>
                                    <!-- Location -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="location">Location</label>
                                        <select name="location" id="location">
                                        <?php 
                                            switch_to_blog(110);
                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                            restore_current_blog();

                                            foreach( $terms as $term ){
                                        ?>
                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                    <!-- Implementation -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="implementation">Have you implemented this program anywhere?</label>
                                        <select name="implementation" id="implementation">
                                            <option value="No, But i believe it will work">No</option>
                                            <option value="I have started working on it, but haven't launched">Yes, I have led the implementation of this program in the past </option>
                                            <option value="I have implemented the idea but at early stages">Yes, I have been part of a team that Implemented this program</option>
                                        </select>
                                    </div>
                                    <!-- Educational Status -->
                                    <div class="col-lg-6 padding-lr-10 form-item">
                                        <label for="educational_status">Educational status</label>
                                        <select name="educational_status" id="educational_status">
                                            <option value="Secondary School Student">Secondary School Student</option>
                                            <option value="Undergraduate">Undergraduate</option>
                                            <option value="Graduate (Pre-NYSC)">Graduate (Pre-NYSC)</option>
                                            <option value="NYSC Corps Member">NYSC Corps Member</option>
                                            <option value="Graduate (Post NYSC)">Graduate (Post NYSC)</option>
                                            <option value="Professional">Professional</option>
                                        </select>
                                    </div>
                                    <!-- Contribution -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="contribution">How can you contribute to this program</label>
                                        <textarea name="contribution" rows="8"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" id="" class="btn btn-green txt-sm padding-lr-15">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Champion this idea -->
        <div class="modal fade font-main filter-modal" id="championIdeaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-addIdea'; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel">Champion this idea</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-40">
                            <article>

                                <p class="txt-normal-s txt-height-1-7">Idea Champions believe in the effectiveness of an idea and are willing to actively participate in making it a reality. If this idea resonates with you, please submit your name as an idea champion and we will reach out to you first, if we decide to convert this idea into a project.</p>
                            </article>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Guideline info -->
        <div class="modal fade font-main filter-modal" id="guidelineModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog .modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-addIdea'; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel">Review our idea submission guidelines</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-40">
                            <article class="text-box txt-sm">
                                <p class="txt-normal-s txt-height-1-7">We are excited that you have an idea to solve this challenge . To help us act on your idea, please ensure that:</p>
                                <ol>
                                    <li>The idea directly solves one of the identified challenge areas related to <?php echo $post_title ?></li>
                                    <li>The idea can be replicated nationwide by anybody who is willing to adopt it.</li>
                                    <li>Please make your idea topic as descriptive as possible. People will decide whether or not to engage with your idea depending on how clear your topic is.</li>
                                    <li>Every idea submitted here will be subject to public view and can be adopted/adapted by anyone without recourse to the poster. This is the very essence of this platform - to crowdsource ideas and inspire people to take action on any one they are excited about. Please do not submit any idea that you consider as proprietary. We will not be held liable if your idea is adopted/adapted and implemented by any visitor to this site.</li>
                                </ol>
                            </article>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Add Your Voice Info -->
        <div class="modal fade font-main filter-modal" id="addVoiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-addIdea'; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel">Add Your Voice</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-40">
                            <article>
                                <p class="txt-normal-s txt-height-1-7">Have a suggestion? Have a modified version of this idea? Or do you have experience doing something like this that you would love to share?  Your views are important in helping us determine whether or not we should convert this idea into a project. Follow the link to add your voice.</p>
                            </article>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        
        <?php if( $_GET['confirmation-message'] ){ ?>
        
        <!-- confirmation Message -->
        <div class="modal fade font-main filter-modal" id="confirmationMessageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-40">
                        <article class="text-center">
                            <p class="txt-xlg txt-light txt-height-1-5">Your idea was successfully submitted.</p>
                        </article>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-green txt-sm padding-lr-15" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </main>
    

<?php if( $_GET['confirmation-message'] ){ ?>
<!-- Confirmation Modal -->
<script type="text/javascript">
    $(window).on('load',function(){
        $('#confirmationMessageModal').modal('show');
        var url = window.location.href;
        var a = url.indexOf("?");
        var b =  url.substring(a);
        var c = url.replace(b,"");
        url = c;
        history.pushState(null, '', url);  
    });
</script>
<?php } ?>

<script type="text/javascript">
    /*Agreement Checkbox*/
    $(document).ready(function () {
        $(".agreementCheckbox").on("ifChanged", function() {
          $(this).closest('.modal-content').find(".addIdeaButton").attr("disabled", !this.checked);
        });
    });
    
    /* Set question ID */
    $('.answer-question').click(function(e){
        var question_id = $(this).attr('data-question');
        $('.question_id').val(question_id);    
        console.log($('#question_id'));
    });
</script>
   
<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('input.custom-check').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<?php get_footer() ?>