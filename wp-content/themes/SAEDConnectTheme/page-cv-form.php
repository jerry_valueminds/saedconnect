<?php /*Template Name: CV-forms*/ ?>
    
<?php get_header() ?>
  
<?php
    // Get Base URL
    $base_url = get_site_url().'/my-dashboard';

    // Get Currently logged in User
    $current_user = wp_get_current_user();
?> 
  
<?php
    if ( !is_user_logged_in() ) {
        // If User is not Logged in, redirect to Login Page
        $dashboard_link = get_page_link(608); //Get Login Page Link by ID

        // Redirect to Login Page
        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }
?>

<?php
    /* User is Logged in */

    /* Select Page View Request */
    if(isset($_GET['action'])){
        $page_type = $_GET['action']; 
    } else {
        $page_type = '';
    }
?>
   
<style>
    .gform_body{
        border: 0 !important;
    }

    .gform_wrapper .top_label .gfield_label {
        font-size: 0.8rem !important;
        margin-bottom: 0 !important;
    }

    .ginput_container input, .ginput_container select, .ginput_container textarea{
        width: 100% !important;
        font-size: 0.9rem !important;
        padding: 0.4rem 0.8rem !important;
    }

    .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
        margin-top: 0px !important;
    }

    .gform_button{
        text-align: center;
        margin: 0 !important;
        margin-bottom: 20px !important;
    }

    .gform_footer{
        display: flex !important;
        flex-direction: column;
        justify-content: center !important;
        align-items: center !important;
    }


</style>

<main class="main-content font-main">
<?php
    /*
    *
    *   Select CV Form to Display
    *
    */
    if($page_type !== ''){
        get_template_part( 'template-parts/cv-forms/'.$page_type );
    }

?>
</main>
        
<?php get_footer() ?>