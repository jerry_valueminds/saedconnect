<?php /*Template Name: Profile - Video CV*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full flex_1">
                <div class="page-header">
                    <h1 class="page-title">
                        My Video CV
                    </h1>
                    <div class="cta">
                        <a class="cta-btn" data-toggle="modal" href="#videoLinkModal" aria-expanded="false">
                            Add/Edit Video
                        </a>
                    </div>
                </div>
                <article class="page-summary">
                    <p>
                        Upload your video CV on YouTube and add the link on SAEDConnect.
                    </p>
                </article>
                <div class="section-wrapper">
                    <div class="entry">

                        <div id="video" class="padding-b-15">
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <!-- Details Modal -->
    <div class="modal fade font-main filter-modal" id="videoLinkModal" tabindex="-1" role="dialog" aria-labelledby="AddPaymentfaqModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <form action="<?php echo currentUrl(false); ?>" method="post">
                    <div class="modal-header padding-lr-30">
                        <h5 class="modal-title txt-medium" id="exampleModalLabel"><?php the_title() ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body padding-o-30 form">
                        <?php
                            $meta_key = 'video-cv-link';
                            /*
                            *
                            * Save / Retrieve Form Data
                            *
                            */
                            if( $_POST ){

                                /* Get form data */
                                $link = wp_kses_post( $_POST[$meta_key] );

                                /* Save data */
                                update_user_meta( $current_user->ID, $meta_key, $link);

                                printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                            }

                            /* Get Saved data */             
                            $link = get_user_meta($current_user->ID, $meta_key, true);
                        ?>

                        <p class="txt-sm txt-medium margin-b-5">
                            YouTube Link
                        </p>
                        <div class="d-flex m-0">
                            <div class="form-item flex_1 m-0">
                                <label for="post-name" class="sr-only">
                                    YouTube Link
                                </label>
                                <input 
                                    type="url" 
                                    name="<?php echo $meta_key ?>" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $link ?>"
                                    style="border-right:0;"
                                >
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer padding-lr-30">
                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form> 
            </div>
        </div>
    </div>
    
<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>


<script>
    $(document).ready(function(){

        function getId(url) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = url.match(regExp);

            if (match && match[2].length == 11) {
                return match[2];
            } else {
                return 'error';
            }
        }

        var myId;


        var myUrl = '<?php echo $link ?>';
        myId = getId(myUrl);

        $('#myId').html(myId);

        $('#video').html('<iframe width="100%" height="380" src="//www.youtube.com/embed/' + myId + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');        
    });
</script>