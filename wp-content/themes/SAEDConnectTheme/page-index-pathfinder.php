    <?php /*Template Name: Homepage-Pathfinder*/ ?>
    
    <?php get_header() ?>

	<main class="main-content">
		<header class="overview-header container-wrapper">
			<div class="row padding-tb-20">
				<div class="col-md-8">
                    <h1 class="txt-3em txt-bold txt-height-1-2 padding-b-20">
                        Find Your Path
                    </h1>   
                </div>
			</div>
		</header>
        <section class="bg-blue txt-color-white padding-tb-20">
            <div class="container-wrapper">
                <h2 class="txt-xlg txt-height-1-3">
                    It takes more than skills and hard-work to excel in career or business.
                    <br>
                    Find out what you are built to excel in, based on your unique personality.
                </h2>
            </div>
        </section>
		<!-- General Section -->
		<section>
			<div class="row">
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (40).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Discover Yourself
						</h3>
						<p class="margin-b-30">
						    Use our career guidance online tool to gain clarity about your strengths & weaknesses, and discover viable careers that match your talents.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-white" data-toggle="modal" href="#comingSoonModal">
								Get Started 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (41).jpg');">
					<div class="content txt-color-white">
                        <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Get Counseling
						</h3>
					    <p class="margin-b-30">
						    Let our counselors work with you to uncover your strengths, interpret your passions and provide objective and professional advice about optimal career paths to choose.
						</p>
						<div class="btn-wrapper">
							<!--<a class="btn btn-trans-wb" href="http://www.saedconnect.org/about/reference/purpose-discovery-services/">
								Sign me up 
							</a>-->
							<span class="btn btn-trans-wb">
								Coming soon
							</span>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (42).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Self Discovery Guide
						</h3>
						<p class="margin-b-30">
						    Dive into concepts & explanations that help you appreciate your uniqueness and guide you in making informed career decisions.
						</p>
						<div class="btn-wrapper">
							<!--<a class="btn btn-trans-wb" href="http://www.saedconnect.org/self-discovery-guide/">
								Explore 
							</a>-->
							<span class="btn btn-trans-wb">
								Coming soon
							</span>
						</div>
					</div>
				</article>
				<div class="col-md-12">
				    <div class="home-gallery">
                        <section class="swiper-wrapper" href="gallery">
                            <div class="swiper-slide bg-ash txt-color-white">
                                <div class="container-wrapper padding-tb-40 content">
                                    <h1 class="title">
                                        <span class="main">
                                            Fall in Love with the process
                                        </span>
                                        <span class="sub txt-color-dark">
                                            and the results will come  
                                        </span>
                                    </h1>
                                </div>
                            </div>
                            <div class="swiper-slide bg-ash txt-color-white">
                                <div class="container-wrapper padding-tb-40 content">
                                    <h1 class="title">
                                        <span class="main">
                                            Don’t be afraid to fail 
                                        </span>
                                        <span class="sub txt-color-dark">
                                            be afraid to not try    
                                        </span>
                                    </h1>
                                </div>
                            </div>
                            <div class="swiper-slide bg-ash txt-color-white">
                                <div class="container-wrapper padding-tb-40 content">
                                    <h1 class="title">
                                        <span class="main">
                                            Go where there is no path
                                        </span>
                                        <span class="sub txt-color-dark">
                                            and leave a trail  
                                        </span>
                                    </h1>
                                </div>
                            </div>
                            <div class="swiper-slide bg-ash txt-color-white">
                                <div class="container-wrapper padding-tb-40 content">
                                    <h1 class="title">
                                        <span class="main">
                                            You will never influence the world by being just like it
                                        </span>
                                        <span class="sub txt-color-dark">
                                            be different.
                                        </span>
                                    </h1>
                                </div>
                            </div>
                        </section>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination-home"></div>
                    </div>
				</div>
			</div>
		</section>
	</main>

    <?php get_footer() ?>