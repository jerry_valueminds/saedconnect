    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
  
    <?php

        // Get Title
        $session_title = get_the_title();

        //Get Session ID
        $session_id = $post->ID;

        // Session Fee
        $session_fee = '₦0.00';

        if( rwmb_meta( 'info-session-mentor-forum-fee' ) ){
            $session_fee = '₦'.rwmb_meta( 'info-session-mentor-forum-fee' );
        }
        /* Forum Community Link */
        $forum_community = rwmb_meta( 'info-session-community-forum' );
    
        /*Get Theme Color*/
        $theme_color = rwmb_meta( 'info-session-theme-color' );
    
        /*Get Theme Alt Color*/
        $theme_alt_color = rwmb_meta( 'info-session-theme-alt-color' );

        /* Declare Program Information Var */
        $program_id;
        $program_name;
        $program_url;

        /* Get Program Information */
        $program_id = 633;
        $program_name = get_the_title(633);
        $program_url = get_the_permalink(633);
    ?>
   
    <style>
        .theme-bg{
            background-color: <?php echo $theme_color ?>;
        }
        
        .theme-alt{
            color: <?php echo $theme_alt_color ?>;
        }
        
        .how-it-works-container::before{
            background-color: <?php echo $theme_color ?>;
        }
        
        .theme-btn{
            border-color: <?php echo $theme_alt_color ?>;
            color: <?php echo $theme_alt_color ?>;
        }
        
        .theme-btn:hover{
            background-color: <?php echo $theme_alt_color ?>;
            color: <?php echo $theme_color ?>;
        }
        
        .faq-collapse .faq-btn {
            font-size: 0.9rem;
            line-height: 1.4em;
        }
        
        .text-box.sm p {
            font-size: 0.8rem;
            line-height: 1.5em;
        }
    </style>
    
    <main class="main-content">
       
        <?php

            $images = rwmb_meta( 'info-session-feautured-image', array( 'limit' => 1 ) );
            $image = reset( $images );
        ?>
        
        <section class="container-wrapper padding-t-40">
            <header class="course-directory-banner image lg rounded-t-12 overflow-hidden" style="background-image:url('<?php echo $image['full_url']; ?>');">
                <div class="content container-wrapper text-center txt-color-white padding-tb-40">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <h1 class="title">
                                Connect with mentors to help you start & grow your
                                <?php echo rwmb_get_value( 'info-session-name' ); ?>
                                business
                            </h1>
                        </div>
                    </div>
                    <h2 class="txt-lg txt-height-1-4 txt-light margin-t-20">
                        Why go into the <?php echo rwmb_get_value( 'info-session-name' ); ?> business blindly, when you can
                        <br>
                        get help from experts and get it right the first time?
                    </h2>
                    <p class="margin-t-40">
                        <!--<button type="button" class="btn btn-trans-wb no-m-b" data-toggle="modal" data-target="#formModal">
                            Join the Community
                        </button>-->
                        <button class="btn btn-trans-wb no-m-b theme-btn scroll-to">
                            Join the Mentor Hub
                        </button>
                    </p>
                </div>
            </header>
        </section>
        
        <section class="container-wrapper padding-b-40">
            <div class="txt-color-white text-center rounded-b-12" style="background-color:<?php echo $theme_color; ?>">
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <div class="padding-lr-20 padding-tb-60">
                            <h2 class="txt-xxlg txt-medium txt-height-1-2 padding-b-30" style="color:<?php echo $theme_alt_color ?>">
                                About the
                                <?php echo rwmb_get_value( 'info-session-name' ); ?>
                                Business
                            </h2>
                            <article class="text-box txt-color-white" style="color:<?php echo $theme_alt_color ?>">
                                <?php echo rwmb_get_value( 'info-session-description' ); ?>
                            </article>
                            <p class="margin-t-30 margin-b-30">
                                <!--<button
                                    type="button"
                                    class="btn btn-trans-wb no-m-b"
                                    data-toggle="modal"
                                    data-target="#formModal"
                                    style="color:<?php echo $theme_alt_color ?>; border-color:<?php echo $theme_alt_color ?>;"
                                >
                                    Join the Community
                                </button>-->
                                
                                <button class="btn btn-trans-wb no-m-b theme-btn scroll-to">
                                    Join the Mentor Hub
                                </button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- How it works -->
        <section class="padding-t-80 padding-b-60">
            <div class="container-wrapper">
                <div class="row text-center"> 
                    <div class="col-md-10 mx-auto">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-20 ">
                            How it Works
                        </h1>
                        <p class="txt-normal-s margin-b-60 text-center">
                            <?php echo rwmb_meta( 'skilli-program-hiw-brief', array(), $program_id ); ?>
                        </p>
                      </div>  
                </div>
            </div>
            <div class="container-wrapper how-it-works-container">
                <div class="row text-center"> 
                    <div class="col-md-10 mx-auto">
                        <div class="row row-20 ">
                        <?php
                            $group_values = rwmb_meta( 'skilli_program_hiw_group', array(), $program_id );
                            $counter = 1;

                            if ( ! empty( $group_values ) ) {
                                foreach ( $group_values as $group_value ) {
                        ?>

                            <div class="col-md-4 padding-lr-20">
                                <div class="how-card">
                                    <div class="number">
                                        <span>
                                            <?php
                                                echo $counter;
                                                $counter++;
                                            ?>
                                        </span>
                                    </div>
                                    <div class="content">
                                        <h3 class="txt-medium txt-height-1-2 margin-b-5">
                                            <?php echo $group_value[ 'skilli-program-hiw-title' ]; ?>
                                        </h3>
                                        <p class="txt-sm txt-height-1-7">
                                            <?php echo $group_value[ 'skilli-program-hiw-content' ]; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        <?php
                                }
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- What you get -->
        <section class="container-wrapper padding-b-40">
            <div class="bg-grey text-center rounded-o-12">
                <div class="row"> 
                    <div class="col-md-8 mx-auto padding-t-80 padding-b-60">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-20">
                            What you get
                        </h1>
                        <p class="txt-normal-s margin-b-60">
                            <?php echo rwmb_meta( 'skilli-program-wyg-brief', array(), $program_id ); ?>
                        </p>
                        <div class="row row-20">
                        <?php
                            $group_values = rwmb_meta( 'skilli_program_wyg_group', array(), $program_id );

                            if ( ! empty( $group_values ) ) {
                                foreach ( $group_values as $group_value ) {
                        ?>
                            <div class="col-md-6 padding-lr-20 d-flex">
                                <div class="bg-white collapsible-card">
                                    <div class="header">
                                        <div class="cta"></div>
                                        <div class="title">
                                            <?php echo $group_value[ 'skilli-program-wyg-title' ]; ?>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="wrapper">
                                            <div class="margin-b-20">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_mentor.png" alt="" width="100">
                                            </div>
                                            <h3 class="txt-medium margin-b-20">
                                                <?php echo $group_value[ 'skilli-program-wyg-title' ]; ?>
                                            </h3>
                                            <p class="txt-sm txt-height-1-7">
                                                <?php echo $group_value[ 'skilli-program-wyg-content' ]; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                                }
                            }
                        ?>
                        </div>
                    </div>  
                </div>
            </div>
        </section>

        <!-- Meet Your mentors -->
        <section class="padding-tb-80">
            <div class="container-wrapper text-center">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <h1 class="txt-xlg txt-height-1-1 margin-b-20">
                            Meet your Mentors
                        </h1> 
                        <p class="txt-normal-s margin-b-60">
                            We are constantly reaching out to the best of the best professionals & experts to help you make progress.
                        </p>
                    </div>
                </div>

                <div class="row" id="scroll-to-me">
                    <div class="col-md-8 mx-auto">                
                        <div class="row row-20">
                        <?php
                             // Create Query Argument
                            $args = array(
                                'post_type' => 'mentor',
                                'showposts' => 6,
                                'meta_query' => $meta_array,
                            );


                            $mentor_query = new WP_Query($args);

                            while ($mentor_query->have_posts()) : $mentor_query->the_post();

                            /* Get Post ID */
                            $post_id = $post->ID;

                            /* Get Mentor mentorship areas */
                            $mentor_field = 'mentor-mentorship-areas';

                            $mentor_areas = get_post_meta($post->ID, $mentor_field, false);

                            /* Check if Menotor mentors on this Track's Program */
                            if ( in_array($program_name, $mentor_areas) ){

                        ?>

                            <!-- Mentor -->
                            <div class="col-md-2 padding-lr-20 padding-b-40 d-flex">
                                <div class="mentor-card">
                                    <div class="image-box">
                                        <?php
                                            $field = 'mentor-profile-image';

                                            $meta = get_post_meta($post->ID, $field, true);

                                        ?>
                                        <img src="<?php if($meta){  echo $meta; } ?>" alt="">
                                    </div>
                                    <div class="name">
                                        <a data-toggle="modal" href="#mentor-<?php echo $post_id ?>">
                                            <?php echo the_title(); ?>
                                        </a>
                                    </div>
                                    <div class="position">
                                        <?php
                                            $field_position = 'mentor-position';
                                            $field_company = 'mentor-company';

                                            $meta_position = get_post_meta($post->ID, $field_position, true);
                                            $meta_company = get_post_meta($post->ID, $field_company, true);

                                            echo $meta_company;
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <!-- Mentor Modal -->
                            <div class="modal fade font-main coming-soon-modal" id="mentor-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content padding-o-40">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <div class="text-center">
                                            <div class="margin-b-10">
                                                <?php
                                                    $field = 'mentor-profile-image';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <img src="<?php if($meta){  echo $meta; } ?>" width="120" class="profile-image">
                                            </div>
                                            <div class="txt-normal-s margin-b-10">
                                                Mentor
                                            </div>
                                            <h4 class="txt-xlg txt-medium margin-b-10">
                                                <?php echo the_title(); ?>
                                            </h4>
                                            <h5 class="txt-medium uppercase txt-sm margin-b-20">
                                                <?php
                                                    $field_position = 'mentor-position';
                                                    $field_company = 'mentor-company';

                                                    $meta_position = get_post_meta($post->ID, $field_position, true);
                                                    $meta_company = get_post_meta($post->ID, $field_company, true);

                                                    echo $meta_position.' @ '.$meta_company;
                                                ?>
                                            </h5>
                                            <p class="txt-sm txt-height-1-7 margin-b-20">
                                                <?php
                                                    $field = 'mentor-short-profile';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                    if($meta){
                                                        echo $meta;
                                                    }
                                                ?>
                                            </p>
                                            <div class="txt-xlg">
                                                <?php
                                                    $field = 'mentor-facebook';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-facebook fa-fw"></i>
                                                </a>

                                                <?php
                                                    $field = 'mentor-twitter';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a 
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-twitter fa-fw"></i>
                                                </a>

                                                <?php
                                                    $field = 'mentor-linkedin';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a 
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-linkedin fa-fw"></i>
                                                </a>
                                            </div>
                                            <div class="padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Mentors on
                                                </div>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field = 'mentor-mentorship-areas';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="border-t-1 border-color-darkgrey padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Small Business Mentorship area
                                                </div>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field = 'mentor-small-business-mentorship-interest';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                            }   // Check if Menotor mentors on this Track's Program: End

                            endwhile;
                        ?>  
                        </div>
                        <h2 class="txt-normal-s txt-height-1-7">
                            Have questions? Check out the
                                Help Center
                            .
                        </h2>
                        <div class="margin-t-20">
                            <a href="https://www.saedconnect.org/growth-programs/mentor-directory/" class="btn btn-green txt-sm no-m-b">
                                See Full List
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Pay -->
        <section class="container-wrapper padding-b-40">
            <div class="bg-grey rounded-o-12">
                <div class="row"> 
                    <div class="col-md-8 mx-auto padding-t-80 padding-b-60">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-60 text-center">
                            Engage with Expert Mentors waiting to help you
                        </h1>
                        <p class="margin-b-60 text-center">
                            Where do you go to get expert advice about starting or growing your <?php echo $session_title ?> business? The <?php echo $session_title ?> mentor hub is the community of <?php echo $session_title ?> experts you always wished you could reach out to when you needed some motivation, advice or expert experience.
                        </p>
                        <div class="row">
                            <div class="col-md-9 mx-auto theme-bg theme-alt text-center text-md-left">
                                <div class="padding-o-30">
                                    <div class="row row-10 overflow-hidden">
                                        <div class="col-md-6 padding-lr-20 payment-border border-color-darkgrey margin-b-10 padding-b-10">
                                            <h3 class="txt-xxlg txt-bold margin-b-5">
                                                Just <?php echo $session_fee ?> / Qtr
                                            </h3>
                                            <p class="txt-sm">
                                                To join <?php echo $program_name ?>
                                                <br>
                                                Mentor Hub
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-20">
                                            <p class="txt-normal-s">
                                                We charge a small fee to cover our costs, maintain expert team & sustain a high quality moderated Mentor Hub.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 mx-auto">
                                <ul class="con-list black txt-normal-s margin-t-40">
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Tap into the wisdom & experience of a lot of mentors
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get customised answers to your questions
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get inspired by your peers
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get access to growth support tools, templates & special opportunities.
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Engage with Experts and Mentors until you are satisfied.
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="text-center margin-t-40">
                            <a href="<?php echo $forum_community; ?>" class="btn btn-green txt-sm">
                                Subscribe
                            </a>
                        </div>
                      </div>  
                </div>
            </div>
        </section>
        
        <!-- FAQ -->
        <section class="container-wrapper padding-tb-80 margin-b-20">
            <header class="margin-b-40">
                <h2 class="txt-xxlg txt-light text-center">Frequently Asked Questions</h2>    
            </header>
            
            <div class="row row-10">
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-1" aria-expanded="false" aria-controls="collapseExample">
                            What is <?php echo $session_title ?> Side Hustle Mentor Hub?
                        </button>
                        <div class="collapse" id="faq-1">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        The <?php echo $session_title ?> Side Hustle Mentor Hub is an exclusive highly moderated community where you can meet and interact with expert mentors in the <?php echo $session_title ?> business who are always available to answer any questions you have as you try to start or grow your own <?php echo $session_title ?> business.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-2" aria-expanded="false" aria-controls="collapseExample">
                            Who can join <?php echo $session_title ?> Side Hustle Mentor Hub?
                        </button>
                        <div class="collapse" id="faq-2">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anyone interested in starting or growing a the <?php echo $session_title ?> business can join the side hustle Mentor Hub for that business.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-3" aria-expanded="false" aria-controls="collapseExample">
                            Can I access The <?php echo $session_title ?> Side Hustle Mentor Hub from anywhere in the country?
                        </button>
                        <div class="collapse" id="faq-3">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Yes. You can access the <?php echo $session_title ?> side hustle mentor hub from anywhere you are, as long as you have connectivity to the Internet.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-4" aria-expanded="false" aria-controls="collapseExample">
                            Can I join more than one Side Hustle Mentor Hub?
                        </button>
                        <div class="collapse" id="faq-4">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Yes, you can join the side hustle mentor hubs of as many businesses you are interested in.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-5" aria-expanded="false" aria-controls="collapseExample">
                            Can I advertise my product in the <?php echo $session_title ?> Side Hustle mentor hubs?
                        </button>
                        <div class="collapse" id="faq-5">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Yes. If you have a product that can be useful to practitioners of the <?php echo $session_title ?> side hustle, there is a "Business Offers and Opportunities" sections in the mentor hubs that allows you list offers and opportunities for members of that mentor hub to see.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-6" aria-expanded="false" aria-controls="collapseExample">
                            Can I meet with a <?php echo $session_title ?> side hustle mentor for a face-to-face training?
                        </button>
                        <div class="collapse" id="faq-6">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Yes. Check in the trainers directory to see all the trainers who can help you if you wish to have a face-to-face training on <?php echo $session_title ?>.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-7" aria-expanded="false" aria-controls="collapseExample">
                            Can I get connected to funding sources from the <?php echo $session_title ?> side hustle mentor hubs?
                        </button>
                        <div class="collapse" id="faq-7">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        When we come across funding ideas that might be of benefit to members of the <?php echo $session_title ?> side hustle mentor hub, we will share it with you and give you all the necessary support to apply and get it.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-8" aria-expanded="false" aria-controls="collapseExample">
                            I have a suggestion whom do I contact.
                        </button>
                        <div class="collapse" id="faq-8">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Send us an email at
                                        <a href="mailto:support@saedconnect.org">
                                            support@saedconnect.org
                                        </a>
                                        or share your suggestion with us in the
                                        <a href="https://www.saedconnect.org/help-center/forums/forum/get-help/make-a-suggestion/">
                                            Help Center
                                        </a>
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
        
        
        
        
        
    
        
        <?php
            /* Get all Accordion Data */
            $values = rwmb_meta( 'info-session-faq' );
        
            if($values){
        ?>
        <section class="container-wrapper padding-tb-80 margin-b-20">
            <header class="margin-b-40">
                <h2 class="txt-xxlg txt-light text-center">Frequently Asked Questions</h2>    
            </header>
            
            <div class="row row-10">
            <?php 
                /* Define Accordion Item Counter Variable */
                $accordionCounter = 1;

                foreach ( $values as $value ) { 
            ?>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-<?php echo $accordionCounter ?>" aria-expanded="false" aria-controls="collapseExample">
                            <?php echo $value['accordion-title']; ?>
                        </button>
                        <div class="collapse" id="faq-<?php echo $accordionCounter ?>">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        <?php echo $value['accordion-content']; ?>
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                    $accordionCounter++;
                }
            ?> 
            </div>
        <?php } ?>
        </section>
        
        <style>
            
            .gform_wrapper .top_label .gfield_label {
                font-size: 0.8rem !important;
                margin-bottom: 0 !important;
            }
            
            label{
                color:<?php echo $theme_alt_color ?> !important;
            }
            
            .ginput_container input, .ginput_container select, .ginput_container textarea{
                width: 100% !important;
                font-size: 0.9rem !important;
                padding: 0.4rem 0.8rem !important;
            }
            
            .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
                margin-top: 0px !important;
            }
            
            .gform_button.button{
                margin: 0 auto !important;
                width: auto !important;
            }
            
            .info-session-hidden{
                display: none !important;
            }
            
            
            .gf-white .gform_button {
                background-color: transparent;
                border: 2px solid <?php echo $theme_alt_color ?>;
                color: <?php echo $theme_alt_color ?>;
            }
            
            .gf-white .gform_button:hover {
                background-color: <?php echo $theme_alt_color ?>;
                color: <?php echo $theme_color ?>;
            }
        </style>
        
        <!-- Pre-registrstion Modal -->
        <div class="modal fade font-main coming-soon-modal" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="row content">
                        <div class="left gf-white full-width" style="background-color:<?php echo $theme_color ?>;">
                            <section id="get-started">
                                <header class="padding-b-20 text-center" style="color:<?php echo $theme_alt_color ?>;">
                                    <h1 class="txt-2-2em txt-light txt-height-1-1 margin-b-20">
                                        Start you journey to financial freedom through <?php echo rwmb_get_value( 'info-session-name' ); ?> business
                                    </h1>
                                    <h2 class="txt-normal-s txt-height-1-7">
                                        You don't have to walk alone. Tell us what small business you want to start to build & we will take it from there.
                                    </h2>
                                </header>
                                <div class="">
                                    <!-- Local -->
                                    <?php //echo do_shortcode('[gravityform id="101" title="false" description="false" field_values="business-name='.$session_title.'"]'); ?>
                                    
                                    <!--Online-->
                                    <?php echo do_shortcode('[gravityform id="21" title="false" description="false" field_values="business-name='.$session_title.'"]'); ?>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
            $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

            if(!$incoming_from_saedconnect){

                /* Include Skilli Footer */
                get_template_part( 'template-parts/footer-skilli' );
            }
        ?>
    </main>
    
    <script>
        /* Scroll to view */
        $(".scroll-to").click(function(){
            var elmnt = document.getElementById("scroll-to-me");
            elmnt.scrollIntoView({behavior: "smooth", block: "start", }); // Top
        });
    </script>
    
    <?php endwhile; // end of the loop. ?>
    
    <?php
        $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

        if($incoming_from_saedconnect){

            /* Include Saedconnect Footer */
            get_template_part( 'template-parts/footer-saedconnect' );
        }
    ?>
    
    <?php get_footer() ?>
    
    <?php
    $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

    if($incoming_from_saedconnect){
?>

   <script>
        $(document).ready(function(){
            /*
            *   Auto populate form with Track ID & Name
            */
            $('#formModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var track_id = button.attr('track-id'); // Extract Track ID
                var track_name = button.attr('track-name'); // Extract Track Name

                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);

                //modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('#input_1_6').val(track_id);
                modal.find('#input_1_7').val(track_name);
            })


            //Confirmation Modal
            $('#formConfirmationModal').modal('show')

            //Clear URL
           // var uri = window.location.toString();
            //if (uri.indexOf("?") > 0) {
                //var clean_uri = uri.substring(0, uri.indexOf("?"));
                //window.history.replaceState({}, document.title, clean_uri);
            //}

            //Append Saedconnect Query string
            $('a').each(function(index, element) {
              var newAttr = $(element).attr('href') + '?saedconnect=true';
              $(element).attr('href', newAttr);
            });
        });
    </script>

<?php } ?>