<?php /*Template Name: Profile - Mobile Workforce*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            
            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        Mobile Workforce
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        To become an influencer and make money while doing it, Register here.
                    </p>
                </article>
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true) ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                </div>      

            <?php } else { //Empty Profile Message ?>

                <!-- Influencer Information -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 87; //Form ID
                    $gv_id = 1171; //Gravity View ID
                    $title = 'Influencer Profile';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>

                    <?php if(!$entry_count){ //If no entry ?>
                        
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>
                            <div class="entry">
                                <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                    Empty! Start by creating your <?php echo $title ?>.
                                </h3>

                                <div class="padding-b-20">
                                    <a 
                                        class="btn btn-ash txt-xxs no-m-b"
                                        href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                    >
                                        Create Profile
                                    </a>
                                </div>
                            </div>      
                                               

                    <?php } else { ?>

                        <?php foreach( $entries as $entry ){ ?>
                           
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>          
                                </h2>
                                <div class="text-right">
                                    <a 
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=trainer-profile' ?>" 
                                        class="edit-btn"
                                    >
                                        Edit
                                    </a>
                                </div>
                            </div>
                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Name       
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            How Social Media Active are you?        
                                        </p>
                                        <p class="txt-sm">
                                            <?php
                                                $field_id = 2; // Update this number to your field id number
                                                $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                echo $value;
                                            ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Instagram Handle         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 3 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            No. of Instagram Followers as at Today          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 4 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Twitter Handle        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 6 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            No. of Twitter Followers as at Today          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 7 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Youtube Channel         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 8 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Number of Videos Uploaded till date        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 9 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Combined Number of video Views as at today          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 10 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Total Number of Subscribers to your channel         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 11 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Facebook Handle        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 12 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            No of Friends          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 13 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Facebook groups you own/admin      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 14 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Total No. of Subscribers across groups as at today         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 15 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Facebook Pages you own/Subscribe        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 16 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Total No. of Likes across all your pages as at today         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 17 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Blog URL       
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 18 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Average Number of Visits per month        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 19 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Niche Areas where you are known for on social media          
                                        </p>
                                        <p class="txt-sm">
                                            <?php
                                                $field_id = 20; // Update this number to your field id number
                                                $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                echo $value;
                                            ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Other Niche areas        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 21 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Are you open to receiving products as payment for posting?        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 22 ); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        
                        <?php } ?>

                    <?php } ?>
                </div>
                
                <!-- Data COllector Dashboard -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 85; //Form ID
                    $gv_id = 1183; //Gravity View ID
                    $title = 'Data Collector Profile';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>

                    <?php if(!$entry_count){ //If no entry ?>
                        
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                If you want to become a data collector, and make some extra money, register here.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b"
                                    href="<?php printf("%s/?view=form&gf-id=%s&form-title=%s", currentUrl(true), $gf_id, $title); ?>"
                                >
                                    Create Profile
                                </a>
                            </div>
                        </div>      

                    <?php } else { ?>

                        <?php foreach( $entries as $entry ){ ?>
                           
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>          
                                </h2>
                                <div class="text-right">
                                    <a 
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=trainer-profile' ?>" 
                                        class="edit-btn"
                                    >
                                        Edit
                                    </a>
                                </div>
                            </div>
                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Name       
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Do you have a smart phone?       
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 2 ); ?>
                                        </p>
                                    </div>
                                    
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Locations/State where you can collect data        
                                        </p>
                                        <p class="txt-sm">
                                            <?php
                                                $field_id = 3; // Update this number to your field id number
                                                $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                echo $value;
                                            ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            LGAs where you can collect data in that state        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 4 ); ?>
                                        </p>
                                    </div>
                                    
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            How regularly do you subscribe to the internet?         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 5 ); ?>
                                        </p>
                                    </div>

                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            How regularly do you check your email?        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 6 ); ?>
                                        </p>
                                    </div>
                                    
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Please share any previous experience of being a field worker/Data collector         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 7 ); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        
                        <?php } ?>

                    <?php } ?>
                </div>
            <?php } ?>   
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>