    <?php /*Template Name: Homepage-xPlore*/ ?>
    
    <?php get_header() ?>
   
    <main class="main-content">
        <header class="course-directory-banner image" style="background-image:url('http://www.saedconnect.org/learn-a-skill/wp-content/themes/SAEDConnectTheme/images/grid/image (41).jpg');">
            <div class="content container-wrapper text-center">
                <h1 class="title">
                    xPlore Business & Career Opportunities, learn from industry experts & connect with a global network of experience.
                </h1>
            </div>
        </header>
        <section class="container-wrapper padding-tb-40">
            <header class="margin-b-20">
                <h2 class="txt-medium uppercase">About the xplore directory</h2>    
            </header>
            <article class="text-box sm">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non eos earum, repellat consequatur, quos blanditiis quod asperiores cumque porro, molestiae reprehenderit sed saepe unde sunt debitis adipisci id corrupti nam.
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non eos earum, repellat consequatur, quos blanditiis quod asperiores cumque porro, molestiae reprehenderit sed saepe unde sunt debitis adipisci id corrupti nam.
                </p>
            </article>
        </section>
        <section class="container-wrapper padding-t-15 bg-grey">
            <div class="row row-10 align-items-center">
                <div class="padding-lr-10 padding-b-15">
                    <h2>
                        Filter by
                    </h2>
                </div>
                <div class="col-md-4 magazine-dropdown padding-lr-10 padding-b-15">
                    <div class="dropdown">
                        <button class="btn btn-trans-bw dropdown-toggle full-width" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        All                            </button>
                        <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 41px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <a class="dropdown-item selected" href="http://www.saedconnect.org/programs/program-type/access-to-market/">
                                All
                            </a>
                            <a class="dropdown-item selected" href="http://www.saedconnect.org/programs/program-type/access-to-market/">
                                Business Idea
                            </a>
                            <a class="dropdown-item selected" href="http://www.saedconnect.org/programs/program-type/access-to-market/">
                                Career Path
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-content-inverse">
            <div class="row">
                <div class="col-md-4 side-nav white-text bg-blue side-nav-container" id="side-nav">
                    <article>
                        <h2 class="title">
                            Industries
                        </h2>

                        <ul class="list txt-normal-s">
                            <!-- Get All Top Level Industry Terms from Learn a Skill Multisite -->
                            <li class="active">
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1720&amp;post-type=course&amp;s=">
                                    Agriculture                                            </a>
                            </li>
                             <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1721&amp;post-type=course&amp;s=">
                                    Art and Craft                                            </a>
                            </li>
                            <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1722&amp;post-type=course&amp;s=">
                                    Building Decoration and Services                                            </a>
                            </li>
                                                                <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1723&amp;post-type=course&amp;s=">
                                    Education                                            </a>
                            </li>
                                                                <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1724&amp;post-type=course&amp;s=">
                                    Engineering and Construction                                            </a>
                            </li>
                                                                <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1725&amp;post-type=course&amp;s=">
                                    Entertainment and Multimedia                                            </a>
                            </li>
                                                                <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1726&amp;post-type=course&amp;s=">
                                    Fashion and Beauty                                            </a>
                            </li>
                                                                <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1727&amp;post-type=course&amp;s=">
                                    Finance and Accounting                                            </a>
                            </li>
                                                                <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1728&amp;post-type=course&amp;s=">
                                    Food, Hospitality and Tourism                                            </a>
                            </li>
                                                                <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1729&amp;post-type=course&amp;s=">
                                    Health and Safety                                            </a>
                            </li>
                                                                <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1730&amp;post-type=course&amp;s=">
                                    Legal                                            </a>
                            </li>
                            <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1731&amp;post-type=course&amp;s=">
                                    Management and Business Administration                                            </a>
                            </li>
                            <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1732&amp;post-type=course&amp;s=">
                                    Marketing and Sales                                            </a>
                            </li>
                            <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1733&amp;post-type=course&amp;s=">
                                    Media and Journalism                                            </a>
                            </li>
                             <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1734&amp;post-type=course&amp;s=">
                                    Mobile and Information Technology                                            </a>
                            </li>
                             <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1735&amp;post-type=course&amp;s=">
                                    Online Business                                            </a>
                            </li>
                            <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1736&amp;post-type=course&amp;s=">
                                    Other Professional Careers                                            </a>
                            </li>
                            <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1737&amp;post-type=course&amp;s=">
                                    Renewable Energy                                            </a>
                            </li>
                            <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1738&amp;post-type=course&amp;s=">
                                    Textile                                            </a>
                            </li>
                            <li >
                                <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=1739&amp;post-type=course&amp;s=">
                                    Trade                                            </a>
                            </li>
                            </ul>
                        <div class="dropdown">
                            <button class="btn btn-trans-white dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                SECTIONS
                            </button>
                            <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                                <a class="dropdown-item" href="#">
                                    Select...
                                </a>
                                <a class="dropdown-item" href="#">
                                    Cover
                                </a>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-8 left-content-inverse padding-tb-40">
                    <h2 class="txt-normal-s margin-b-20">
                        25 Results
                    </h2>
                    <ul class="row row-40 directory-list txt-xlg">
                        <li class="col-md-6 padding-lr-40">
                            <a href="#">
                                Opportunity Name  
                            </a>
                        </li>
                        <li class="col-md-6 padding-lr-40">
                            <a href="#">
                                Opportunity Name 
                            </a>
                        </li>
                        <li class="col-md-6 padding-lr-40">
                            <a href="#">
                                Opportunity Name  
                            </a>
                        </li>
                        <li class="col-md-6 padding-lr-40">
                            <a href="#">
                                Opportunity Name  
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>