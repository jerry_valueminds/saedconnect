<?php /*Template Name: Profile - Competency*/ ?>
   
<?php

    if ( !is_user_logged_in() ) {
        // If User is Logged in, redirect to User Dashbord
        $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID

        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }

?>

<?php get_header() ?>

<?php
    /* Get current user */
    $current_user = wp_get_current_user();

    /* Get Avatar */
    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
    $meta_key = 'user_avatar_url';
    $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

    if($get_avatar_url){
        $avatar_url = $get_avatar_url;
    }

    /* Get User Display Name */
    switch_to_blog(1);

    $gf_id = 4; //Form ID
    $gv_id = 1385; //Gravity View ID
    $title = 'Profile';

    $entry_count = 0;

    /* GF Search Criteria */
    $search_criteria = array(

    'field_filters' => array( //which fields to search

        array(

            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
            )
        )
    );

    /* Get Entries */
    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

    /* Get GF Entry Count */
    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

    if($entry_count){ //If no entry
        foreach( $entries as $entry ){          
            $displayname = rgar( $entry, '4.3' );
        }                
    }     

    restore_current_blog();
?>

<?php
    /* Get User */
    $current_user = wp_get_current_user();
?>

<?php 
    /* Get Image Field */
    function get_the_image_url($post_id){
        $images = get_attached_media( 'image', $post_id );
        $previousImg_id = 0;
        if($images){
            foreach($images as $image) { 
                $previousImg_id = $image->ID;
            }
            return wp_get_attachment_url($previousImg_id,"full");
        } else {
            return 0;
        }
    }
?>

<main class="main-content" style="padding-top: 70px;margin-top:0;background-color: #e0e0e0">
    <div class="container-wrapper padding-b-60">

        <!-- Title / Image -->
        <div class="flex_1 d-flex align-items-center padding-t-80 padding-b-40">
            <div class="col-auto text-center">
                <figure class="user-avatar" style="background-image: url('<?php echo $avatar_url ?>)"></figure>
            </div>
            <header class="col txt-xlg txt-height-1-2 txt-color-dark padding-tb-20 padding-l-30">
                <h1 class="txt-light margin-b-5">Welcome, <?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?></h1>
                <h2 class="txt-bold">Update your Competency Profile</h2>
            </header>
        </div>

        <!-- Contact Information-->
        <div class="bg-white padding-t-60 padding-b-40 padding-lr-60 margin-b-20">
            <a 
                class="flex_1 row row-10 align-items-center"
                data-toggle="collapse" href="#dashboardCollapseSkills" role="button" aria-expanded="true" aria-controls="dashboardCollapseSkills"
            >
                <figure class="col-12 col-lg-auto text-center padding-lr-10 padding-b-10">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_picture.png" alt="Icon" height="25">
                </figure>
                <div class="col-12 col-lg txt-height-1-2 txt-color-dark padding-lr-10 padding-b-10">
                    <h3 class="txt-lg txt-bold txt-light text-center text-lg-left">Contact Information</h3>
                </div>
            </a>
            <div class="collapse show hideInMobile" id="dashboardCollapseSkills">
                <div class="margin-t-10">
                    <p class="txt-normal-s margin-b-20 text-center text-lg-left">
                        Your work profile is what we use to match you to jobs. You will not be connected to any freelance or full-time job if you dont complete your primary profile. Click on any of the profiles to update it.
                    </p>
                    
                        <?php
                            $profile_query = new WP_Query();
                            $profile_query->query( 
                                array(
                                    'post_type' => 'social-profile',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => 1,
                                ) 
                            );
                        ?>
                        <?php if ( $profile_query->have_posts() ) { ?>
                            
                            <?php $post_id = get_the_ID(); ?>

                            <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>
                                <div class="row padding-b-10">
                                    <div class="col-auto">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_phone.png" alt="Icon" height="15">
                                    </div>
                                    <div class="col padding-l-10">
                                        <?php echo get_post_meta( $post_id, 'phone', true ); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-auto">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_map_marker.png" alt="Icon" height="15">
                                    </div>
                                    <div class="col padding-l-10">
                                        <?php echo get_post_meta( $post_id, 'address', true ); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?> 

                        <?php }else{ ?> 

                            <p class="bg-grey txt-normal-s padding-o-20 margin-b-20">
                                You need to add your contact information to continue. Adding your contact information would enable us reach you with next steps. Click the add button to do so.
                            </p>

                        <?php } ?>
                </div>
            </div>
        </div>

        <!-- Work Profile -->
        <div class="bg-white padding-o-60 margin-b-20">
            <a 
                class="flex_1 row row-10 align-items-center"
                data-toggle="collapse" href="#dashboardCollapseJobs" role="button" aria-expanded="true" aria-controls="dashboardCollapseJobs"
            >
                <figure class="col-12 col-lg-auto text-center padding-lr-10 padding-b-10">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_work_profile.png" alt="Icon" height="25">
                </figure>
                <div class="col-12 col-lg txt-height-1-2 txt-color-dark padding-lr-10 padding-b-10">
                    <h3 class="txt-lg txt-bold txt-light text-center text-lg-left">Work Profile</h3>
                </div>
            </a>
            <div class="collapse show hideInMobile" id="dashboardCollapseJobs">
                <div class="margin-t-10">
                    <p class="txt-normal-s margin-b-40 text-center text-lg-left">
                        Your work profile is what we use to match you to jobs. You will not be connected to any freelance or full-time job if you dont complete your primary profile. Click on any of the profiles to update it.
                    </p>
                    <div class="row row-10 text-center">
                        <!-- Your Work profile Snapshot -->
                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20 d-flex">
                            <div class="d-flex flex-column justify-content-between padding-tb-30 padding-lr-20 border rounded">
                                <div class="margin-b-20">
                                    <figure class="padding-b-20">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_work_profile_snapshot.png" alt="Icon" height="40">
                                    </figure>
                                    <p class="txt-bold txt-color-dark padding-b-20">Your Work profile Snapshot</p>
                                    <p class="txt-normal-s">Your snapshot gives the employer get a quick glimpse on your growth journey.</p>
                                </div>
                                <?php
                                    $profile_query = new WP_Query();
                                    $profile_query->query( 
                                        array(
                                            'post_type' => 'snapshot',
                                            'post_status' => 'publish',
                                            'author' => $current_user->ID,
                                            'posts_per_page' => -1,
                                        ) 
                                    );

                                ?>
                                <?php if( $profile_query->found_posts ){ ?>
                                   
                                    <div>
                                        <p class="txt-xxlg txt-color-green padding-b-10">
                                            <i class="fa fa-check-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=2" class="txt-color-green"><u>Update</u></a>                                        
                                        </p>
                                    </div>
                                    
                                <?php }else{ ?>
                                
                                    <div>
                                        <p class="txt-xxlg txt-color-red padding-b-10">
                                            <i class="fa fa-times-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=2" class="txt-color-red"><u>Get started</u></a>
                                        </p>
                                    </div>
                                
                                <?php } ?>
                            </div>
                        </div>
                        
                        <!-- Education -->
                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20 d-flex">
                            <div class="d-flex flex-column justify-content-between padding-tb-30 padding-lr-20 border rounded">
                                <div class="margin-b-20">
                                    <figure class="padding-b-20">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_education.png" alt="Icon" height="40">
                                    </figure>
                                    <p class="txt-bold txt-color-dark padding-b-20">Your Education </p>
                                    <p class="txt-normal-s">Where did you school? Where have you worked, formally? What languages do you speak?</p>
                                </div>
                                <?php
                                    $profile_query = new WP_Query();
                                    $profile_query->query( 
                                        array(
                                            'post_type' => 'education',
                                            'post_status' => 'publish',
                                            'author' => $current_user->ID,
                                            'posts_per_page' => -1,
                                        ) 
                                    );

                                ?>
                                <?php if( $profile_query->found_posts ){ ?>
                                   
                                    <div>
                                        <p class="txt-xxlg txt-color-green padding-b-10">
                                            <i class="fa fa-check-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=3" class="txt-color-green"><u>Update</u></a>                                        
                                        </p>
                                    </div>
                                    
                                <?php }else{ ?>
                                
                                    <div>
                                        <p class="txt-xxlg txt-color-red padding-b-10">
                                            <i class="fa fa-times-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=3" class="txt-color-red"><u>Get started</u></a>
                                        </p>
                                    </div>
                                
                                <?php } ?>
                            </div>
                        </div>
                        
                        <!-- Work Experience -->
                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20 d-flex">
                            <div class="d-flex flex-column justify-content-between padding-tb-30 padding-lr-20 border rounded">
                                <div class="margin-b-20">
                                    <figure class="padding-b-20">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_work_experience.png" alt="Icon" height="40">
                                    </figure>
                                    <p class="txt-bold txt-color-dark padding-b-20">Your Work experience</p>
                                    <p class="txt-normal-s">What skills & capabilities do you have? How good are you at them?</p>
                                </div>
                                <?php
                                    $profile_query = new WP_Query();
                                    $profile_query->query( 
                                        array(
                                            'post_type' => 'work-experience',
                                            'post_status' => 'publish',
                                            'author' => $current_user->ID,
                                            'posts_per_page' => -1,
                                        ) 
                                    );

                                ?>
                                <?php if( $profile_query->found_posts ){ ?>
                                   
                                    <div>
                                        <p class="txt-xxlg txt-color-green padding-b-10">
                                            <i class="fa fa-check-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=3" class="txt-color-green"><u>Update</u></a>                                        
                                        </p>
                                    </div>
                                    
                                <?php }else{ ?>
                                
                                    <div>
                                        <p class="txt-xxlg txt-color-red padding-b-10">
                                            <i class="fa fa-times-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=3" class="txt-color-red"><u>Get started</u></a>
                                        </p>
                                    </div>
                                
                                <?php } ?>
                            </div>
                        </div>
                        
                        <!-- Skills -->
                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20 d-flex">
                            <div class="d-flex flex-column justify-content-between padding-tb-30 padding-lr-20 border rounded">
                                <div class="margin-b-20">
                                    <figure class="padding-b-20">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_picture.png" alt="Icon" height="40">
                                    </figure>
                                    <p class="txt-bold txt-color-dark padding-b-20">Your Skills and capabilities</p>
                                    <p class="txt-normal-s">Adding your profile picture help employers put a face to your profile</p>
                                </div>
                                <?php
                                    switch_to_blog(101);
                                
                                    $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, 'capability_profile', true) );
                                
                                    restore_current_blog();
                                ?>
                                <?php if( $unserialized_data ){ ?>
                                   
                                    <div>
                                        <p class="txt-xxlg txt-color-green padding-b-10">
                                            <i class="fa fa-check-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=4" class="txt-color-green"><u>Update</u></a>                                        
                                        </p>
                                    </div>
                                    
                                <?php }else{ ?>
                                
                                    <div>
                                        <p class="txt-xxlg txt-color-red padding-b-10">
                                            <i class="fa fa-times-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=4" class="txt-color-red"><u>Get started</u></a>
                                        </p>
                                    </div>
                                
                                <?php } ?>
                            </div>
                        </div>
                        
                        <!-- Languages -->
                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20 d-flex">
                            <div class="d-flex flex-column justify-content-between padding-tb-30 padding-lr-20 border rounded">
                                <div class="margin-b-20">
                                    <figure class="padding-b-20">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_languages.png" alt="Icon" height="40">
                                    </figure>
                                    <p class="txt-bold txt-color-dark padding-b-20">Languages you speak</p>
                                    <p class="txt-normal-s">Your snapshot gives the employer get a quick glimpse on your growth journey.</p>
                                </div>
                                <?php
                                    $profile_query = new WP_Query();
                                    $profile_query->query( 
                                        array(
                                            'post_type' => 'language',
                                            'post_status' => 'publish',
                                            'author' => $current_user->ID,
                                            'posts_per_page' => -1,
                                        ) 
                                    );

                                ?>
                                <?php if( $profile_query->found_posts ){ ?>
                                   
                                    <div>
                                        <p class="txt-xxlg txt-color-green padding-b-10">
                                            <i class="fa fa-check-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=5" class="txt-color-green"><u>Update</u></a>                                        
                                        </p>
                                    </div>
                                    
                                <?php }else{ ?>
                                
                                    <div>
                                        <p class="txt-xxlg txt-color-red padding-b-10">
                                            <i class="fa fa-times-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=5" class="txt-color-red"><u>Get started</u></a>
                                        </p>
                                    </div>
                                
                                <?php } ?>
                            </div>
                        </div>
                        
                        <!-- Interets & Hobbies -->
                        <div class="col-md-6 col-lg-3 padding-lr-10 padding-b-20 d-flex">
                            <div class="d-flex flex-column justify-content-between padding-tb-30 padding-lr-20 border rounded">
                                <div class="margin-b-20">
                                    <figure class="padding-b-20">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_hobbies.png" alt="Icon" height="40">
                                    </figure>
                                    <p class="txt-bold txt-color-dark padding-b-20">Hobbies and interests</p>
                                    <p class="txt-normal-s">Your snapshot gives the employer get a quick glimpse on your growth journey.</p>
                                </div>
                                <?php
                                    $profile_query = new WP_Query();
                                    $profile_query->query( 
                                        array(
                                            'post_type' => 'interest',
                                            'post_status' => 'publish',
                                            'author' => $current_user->ID,
                                            'posts_per_page' => -1,
                                        ) 
                                    );

                                ?>
                                <?php if( $profile_query->found_posts ){ ?>
                                   
                                    <div>
                                        <p class="txt-xxlg txt-color-green padding-b-10">
                                            <i class="fa fa-check-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=6" class="txt-color-green"><u>Update</u></a>                                        
                                        </p>
                                    </div>
                                    
                                <?php }else{ ?>
                                
                                    <div>
                                        <p class="txt-xxlg txt-color-red padding-b-10">
                                            <i class="fa fa-times-circle"></i>
                                        </p>
                                        <p class="txt-normal-s txt-medium">
                                            <a href="https://www.saedconnect.org/competency-profile/create-profile//?step=6" class="txt-color-red"><u>Get started</u></a>
                                        </p>
                                    </div>
                                
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="padding-t-20">
                        <a href="https://www.saedconnect.org/competency-profile/competency-profile-view/" class="btn btn-blue txt-medium txt-sm no-m-b">Manage your work profile</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Your Trainer profile -->
        <div class="bg-white padding-t-60 padding-b-40 padding-lr-60 margin-b-20">
            <a 
                class="flex_1 row row-10 align-items-center"
                data-toggle="collapse" href="#dashboardCollapseBusiness" role="button" aria-expanded="true" aria-controls="dashboardCollapseBusiness"
            >
                <figure class="col-12 col-lg-auto text-center padding-lr-10 padding-b-10">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_trainer_profile.png" alt="Icon" height="25">
                </figure>
                <div class="col-12 col-lg txt-height-1-2 txt-color-dark padding-lr-10 padding-b-10">
                    <h3 class="txt-lg txt-bold txt-light text-center text-lg-left">Your Trainer profile</h3>
                </div>
            </a>
            <div class="collapse show hideInMobile" id="dashboardCollapseBusiness">
                <div class="margin-t-5">
                    <p class="txt-normal-s margin-b-40 text-center text-lg-left">
                        Completing your trainer profile helps us connect you to training jobs. Posting your training courses enables students to find you quickly. Getting accredited boosts your profile and help students know that you are credible.
                    </p>
                    <div class="row row-10">
                        <div class="col-lg-6 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 row rounded border">
                                <div class="col-3 col-lg-2 txt-3em txt-medium txt-color-green padding-tb-15 padding-lr-20 d-flex align-items-center justify-content-center border-right">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="col-9 col-lg-10 padding-o-20 d-flex flex-column justify-content-between">
                                    <article class="flex_1">
                                        <p class="txt-sm txt-color-dark txt-height-1-4">You have setup a trainer's profile for</p>
                                        <p class="txt-normal-s txt-medium txt-color-dark padding-b-5">Company Name Heres</p>
                                    </article>
                                    <p class="txt-normal-s txt-medium">
                                        <a href="#" class="txt-color-green"><u>Update</u></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 row rounded border">
                                <div class="col-3 col-lg-2 txt-3em txt-medium txt-color-light padding-tb-15 padding-lr-20 d-flex align-items-center justify-content-center border-right">
                                    0
                                </div>

                                <div class="col-9 col-lg-10 padding-o-20 d-flex flex-column justify-content-between">
                                    <article class="flex_1">
                                        <p class="txt-normal-s txt-medium txt-color-dark padding-b-5">Skills you train on </p>
                                    </article>
                                    <p class="txt-normal-s txt-medium">
                                        <a href="#" class="txt-color-green"><u>Update</u></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 row rounded border">
                                <div class="col-3 col-lg-2 txt-3em txt-medium txt-color-light padding-tb-15 padding-lr-20 d-flex align-items-center justify-content-center border-right">
                                    0
                                </div>
                                <div class="col-9 col-lg-10 padding-o-20 d-flex flex-column justify-content-between">
                                    <article class="flex_1">
                                        <p class="txt-normal-s txt-medium txt-color-dark padding-b-5">Courses you have created</p>
                                    </article>
                                    <p class="txt-normal-s txt-medium">
                                        <a href="#" class="txt-color-green"><u>Update</u></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 row rounded border">
                                <div class="col-3 col-lg-2 txt-3em txt-medium txt-color-green padding-tb-15 padding-lr-20 d-flex align-items-center justify-content-center border-right">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_accreditation.png" alt="Icon">
                                </div>

                                <div class="col-9 col-lg-10 padding-o-20 d-flex flex-column justify-content-between">
                                    <article class="flex_1">
                                        <p class="txt-normal-s txt-medium txt-color-dark padding-b-5">Skills you train on </p>
                                    </article>
                                    <p class="txt-normal-s txt-medium">
                                        <a href="#" class="txt-color-green"><u>Update</u></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Your Influencer Profile -->
        <div class="bg-white padding-t-60 padding-b-40 padding-lr-60">      
            <a 
                class="flex_1 row row-10 align-items-center"
                data-toggle="collapse" href="#dashboardCollapseYLN" role="button" aria-expanded="true" aria-controls="dashboardCollapseYLN"
            >
                <figure class="col-12 col-lg-auto text-center padding-lr-10 padding-b-10">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_influencer_profile.png" alt="Icon" height="25">
                </figure>
                <div class="col-12 col-lg txt-height-1-2 txt-color-dark padding-lr-10 padding-b-10">
                    <h3 class="txt-lg txt-bold txt-light text-center text-lg-left">Your Influencer Profile</h3>
                </div>
            </a>
            <div class="collapse show hideInMobile" id="dashboardCollapseYLN">
                <div class="margin-t-5">
                    <p class="txt-normal-s margin-b-40 text-center text-lg-left">
                        Your competency profile is not complete yet. You need to complete your competency profile to get notified of Jobs that match your skills
                    </p>
                    <div class="row row-10 text-center">
                        <div class="col-4 col-sm-3 col-md-3 col-lg-3 col-xl-1 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 border rounded padding-tb-20 padding-lr-20">
                                <div class="txt-sm txt-medium padding-b-5">Blogs</div>
                                <div class="txt-xxlg txt-medium padding-b-5">0</div>
                                <div class="txt-normal-s txt-medium">
                                    <a href="#" class="txt-color-red"><u>Add</u></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-sm-3 col-md-3 col-lg-3 col-xl-1 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 border rounded padding-tb-20 padding-lr-20">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_twitter.png" alt="Icon" height="20">
                                </figure>
                                <p class="txt-normal-s txt-medium">
                                    <a href="#" class="txt-color-green"><u>Update</u></a>
                                </p>
                            </div>
                        </div>
                        <div class="col-4 col-sm-3 col-md-3 col-lg-3 col-xl-1 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 border rounded padding-tb-20 padding-lr-20">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_instagram.png" alt="Icon" height="20">
                                </figure>
                                <p class="txt-normal-s txt-medium">
                                    <a href="#" class="txt-color-red"><u>Setup</u></a>
                                </p>
                            </div>
                        </div>
                        <div class="col-4 col-sm-3 col-md-3 col-lg-3 col-xl-1 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 border rounded padding-tb-20 padding-lr-20">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_facebook.png" alt="Icon" height="20">
                                </figure>
                                <p class="txt-normal-s txt-medium">
                                    <a href="#" class="txt-color-green"><u>Update</u></a>
                                </p>
                            </div>
                        </div>
                        <div class="col-4 col-sm-3 col-md-3 col-lg-3 col-xl-1 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 border rounded padding-tb-20 padding-lr-20">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_youtube.png" alt="Icon" height="20">
                                </figure>
                                <p class="txt-normal-s txt-medium">
                                    <a href="#" class="txt-color-red"><u>Setup</u></a>
                                </p>
                            </div>
                        </div>
                        <div class="col-4 col-sm-3 col-md-3 col-lg-3 col-xl-1 padding-lr-10 padding-b-20 d-flex">
                            <div class="flex_1 border rounded padding-tb-20 padding-lr-20">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home_snapchat.png" alt="Icon" height="20">
                                </figure>
                                <p class="txt-normal-s txt-medium">
                                    <a href="#" class="txt-color-red"><u>Setup</u></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>


<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>