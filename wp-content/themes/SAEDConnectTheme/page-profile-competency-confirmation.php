<?php /*Template Name: Profile - Competency Profile Confirmation*/ ?> 
  

<?php get_header() ?>
   
<?php while ( have_posts() ) : the_post(); ?>
    <main class="main-content">
        <section class="container-wrapper bg-white padding-tb-80">
            <div class="row">
                <div class="col-md-7 mx-auto">
                    <h1 class="txt-xxlg txt-medium margin-b-40">
                        Your have successfully completed your competency profile
                    </h1>
                    <article class="text-box">
                        Click go back or go to your competency profile page.
                    </article>
                    <div class="margin-t-80">
                        <a href="#" class="btn btn-trans-blue" onclick="goBack()">
                            <i class="fa fa-arrow-left"></i>
                            <span class="padding-l-10">
                                Go Back
                            </span>
                        </a>
                        <a href="https://www.saedconnect.org/competency-profile/profile/" class="btn btn-blue">
                            <i class="fa fa-home"></i>
                            <span class="padding-l-10">
                                Go to My Profile
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    
<?php endwhile; ?>
    
<?php get_footer() ?>