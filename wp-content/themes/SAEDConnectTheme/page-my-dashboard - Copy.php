<?php /*Template Name: User Dashboard*/ ?>
   
    <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is not Logged in, redirect to Login Page
            $dashboard_link = get_page_link(608); //Get Login Page Link by ID
            
            // Redirect to Login Page
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
    
        }
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    ?>

    <main class="font-main">
        <div class="user-dashboard">
            <nav class="dashboard-nav">
                <div class="nav-header">
                    <a class="brand" href="index.html">
                        <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                        <span class="name">SAEDConnect</span>
                    </a>
                </div>
                <ul class="nav-list">
                    <?php
                        /* Get Base URL */
                        $base_url = get_site_url().'/my-dashboard';
                    ?>
                    <li>
                        <a
                            href="<?php echo $base_url ?>/?action=esaed"
                            class="
                                <?php 
                                    if( $page_type === 'esaed' || $page_type === 'esaed-form' || $page_type === '' ){
                                        echo 'active';
                                    }   
                                ?>
                            ">
                            eSAED
                        </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo $base_url ?>/?action=tools"
                            class="
                                <?php 
                                    if($page_type === 'tools'){
                                        echo 'active';
                                    }   
                                ?>
                            ">
                            Tools
                        </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo $base_url ?>/?action=contributor-profile"
                            class="
                                <?php 
                                    if($page_type === 'contributor-profile' || $page_type === 'share-a-story-form' || $page_type === 'support-a-project-form'){
                                        echo 'active';
                                    }   
                                ?>
                            ">
                            Contibutor Profile
                        </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo $base_url ?>/?action=personal-information"
                            class="
                                <?php 
                                    if($page_type === 'personal-information' || $page_type === 'personal-information-form'){
                                        echo 'active';
                                    }   
                                ?>
                            ">
                            Personal Information
                        </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo $base_url ?>/?action=bank-information"
                            class="
                                <?php 
                                    if($page_type === 'bank-information' || $page_type === 'bank-information-form' ){
                                        echo 'active';
                                    }   
                                ?>
                            ">
                            Bank Information
                        </a>
                    </li>
                </ul>
            </nav>
            <section class="dashboard-body">
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a class="active" href="">Sub-nav</a>
                        </li>
                        <li>
                            <a href="">Sub-nav-2</a>
                        </li>
                    </ul>
                </nav>
                
                <?php
                    /* 
                    *   Select Page View
                    *
                    */
                
                    if($page_type === 'esaed'){
                        
                        get_template_part( 'template-parts/user-dashboard/esaed' );    //Get eSAED View
                        
                    } elseif ($page_type === 'esaed-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/esaed-form' );    //Get eSAED Form View
                        
                    } elseif ($page_type === 'esaed-personal-profile-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/esaed-personal-profile-form' );    //Get eSAED Personal Profile Form View
                        
                    } elseif ($page_type === 'esaed-skills-profile-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/esaed-skills-profile-form' );    //Get eSAED Skills Profile Form View
                        
                    } elseif ($page_type === 'esaed-influencer-profile-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/esaed-influencer-profile-form' );    //Get eSAED Influencer Profile Form View
                        
                    } elseif ($page_type === 'esaed-entrepreneurship-profile-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/esaed-entrepreneurship-profile-form' );    //Get eSAED Entrepreneurship Profile Form View
                        
                    } elseif ($page_type === 'esaed-tutorlink-profile-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/esaed-tutorlink-profile-form' );    //Get eSAED Tutorlink Profile Form View
                        
                    } elseif ($page_type === 'tools'){
                    
                        get_template_part( 'template-parts/user-dashboard/tools' );    //Get Tools View
                        
                    } elseif ($page_type === 'contributor-profile'){
                    
                        get_template_part( 'template-parts/user-dashboard/contributor-profile' );    //Get Contributor Profile View
                        
                    } elseif ($page_type === 'personal-information'){
                    
                        get_template_part( 'template-parts/user-dashboard/personal-information' );    //Get Personal Information View
                        
                    } elseif ($page_type === 'personal-information-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/personal-information-form' );    //Get Personal Information View
                        
                    } elseif ($page_type === 'bank-information'){
                    
                        get_template_part( 'template-parts/user-dashboard/bank-information' );    //Get Bank Information View
                        
                    } elseif ($page_type === 'bank-information-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/bank-information-form' );    //Get Bank Information View
                        
                    } elseif ($page_type === 'support-a-project-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/support-a-project-form' );    //Get Bank Information View
                        
                    } elseif ($page_type === 'share-a-story-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/share-a-story-form' );    //Get Bank Information View
                        
                    } elseif ($page_type === 'co-create-a-program-form'){
                    
                        get_template_part( 'template-parts/user-dashboard/co-create-a-program-form' );    //Get Bank Information View
                        
                    } else {
                        get_template_part( 'template-parts/user-dashboard/esaed' );    //Get eSAED View
                    }
                ?>
                
            </section>
        </div>

    </main>
    
<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>