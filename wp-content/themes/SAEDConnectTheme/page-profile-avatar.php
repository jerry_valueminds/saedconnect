<?php /*Template Name: Profile - Avatar*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php
        //ssprint_r($_POST);
        $meta_key = 'user_avatar';
        $avatar_id = get_user_meta($current_user->ID, $meta_key, true);
        $avatar_display_url = get_user_meta($current_user->ID, 'user_avatar_url', true);


        // Check that the nonce is valid, and the user can edit this post.
        if ( 
            isset( $_POST['my_image_upload_nonce'] ) 
            && wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
        ) {
            // The nonce was valid and the user has the capabilities, it is safe to continue.

            // These files need to be included as dependencies when on the front end.
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            /* Delete prevoius image */
            if($avatar_id){
                wp_delete_attachment( $avatar_id );
            }
            
            // Let WordPress handle the upload.
            // Remember, 'my_image_upload' is the name of our file input in our form above.
            $attachment_id = media_handle_upload( 'my_image_upload', 0);

            if ( is_wp_error( $attachment_id ) ) {
                
                // There was an error uploading the image.
                echo 'An unknown Error occurred, Try again';
                
            } else {
                /* The image was uploaded successfully: Save Attachement ID as User Avatar Meta */
                $avatar_url = wp_get_attachment_url($attachment_id);
                
                update_user_meta( $current_user->ID, $meta_key, $attachment_id);
                update_user_meta( $current_user->ID, 'user_avatar_url', $avatar_url);
                
                $redirect_link = 'https://www.saedconnect.org/my-avatar/'; //Get Daasboard Page Link by ID
            
                /* Redirect */
                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                
            }

        } else {

            // The security check failed, maybe show the user an error.
            //echo '<br><br>Security fail';
        }
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content">
                <section class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Upload your Profile Image
                        </h2>
                    </div>
                    <div class="entry overflow-hidden">

                        <form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
                            <input type="file" name="my_image_upload" id="my_image_upload"  multiple="false" />

                            <?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
                            <input id="submit_my_image_upload" name="submit_my_image_upload" type="submit" value="Upload" />
                        </form>
                        <img src="<?php echo $attachment_url ?>" alt="">
                    </div>
                </section>   
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>