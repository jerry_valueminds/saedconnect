    <?php
        $site_url = get_site_url();
        $permalink = get_permalink();

        /* Insert Post */
        if ( $_POST['postName'] ) {
            echo 'Post exist';

            // create post object with the form values
            $postName = $_POST['postName'];
            $childPostType = $_POST['childPostType'];
            $parentId = $_POST['parentId'];
            $relationshipType = $_POST['relationshipType'];

            /* Meta */
            $meta_content = array( 
                'answer-summary',
                'answer-content',
                'answer-video-media',
                'answer-audio-media',
            );



            $post_id = wp_insert_post(array (
                'post_type' => $childPostType,
                'post_title' => $postName,
                'post_content' => "",
                'post_status' => 'publish',
            ));

            // insert the post into the database
            //$cpt_id = wp_insert_post( $my_cptpost_args, $wp_error);
            echo $post_id;


            /* Add Meta Values */
            foreach ($meta_content as $meta_key) {

                if( $_POST[$meta_key] ){
                    add_post_meta($post_id, $meta_key, $_POST[$meta_key], true);
                }
            }


            /*
            *================================================
            *   MB Relationships
            *================================================
            */

            /* Get Current Multi-site ID */
            $blog_id = get_current_blog_id();

            /* MB Relationship Table */
            $mb_relationship_table = 'wp_mb_relationships';

            /* Relation Table to insert into on a multisite */
            if($blog_id != 1){
                $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
            }

            /* Get Clobal Relationship Table Object */
            global $wpdb;

            /* Insert into relationship table */
            $wpdb->insert( 
            $mb_relationship_table, 
                array( 
                    'from' => $parentId, 
                    'to' => $post_id,
                    'type' => $relationshipType,
                ), 
                array( 
                    '%d',
                    '%d',
                    '%s'
                ) 
            );

            if ( wp_redirect( get_permalink() ) ) {
                exit;
            }

        }
        
    ?>    
    
    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>

    <?php
        /* 
        *=============================================
        * Get Content for this view
        *=============================================
        */

        $question_id = get_the_ID();
        $question_Title = get_the_title();
        $question_content = rwmb_get_value( 'question-description' );;

        $images = rwmb_meta( 'question-featured-image', array( 'limit' => 1 ) );
        $image = reset( $images );
        $questionImage = $image['full_url'];

        /*
        *=============================================
        * First Query to get Topic Name or Mini-site Name
        *=============================================
        */
        // Topic Name Query
        $topicNameQuery = new WP_Query( array(
            'relationship' => array(
                'id'   => 'track_to_question',
                'to' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );

        /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
        if ( $topicNameQuery->have_posts() ){
            while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post();  
            
            $track_id = get_the_ID();
            $track_name = get_the_title();
            $track_url = get_permalink();

            endwhile;
            wp_reset_postdata();
        }
        /*
        *=============================================
        * First Query: END
        *=============================================
        */
    ?>
            
    <main class="main-content">
        <header class="role-header text-center">
            <div class="top bg-ocean-7 txt-color-white">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <h2 class=" txt-bold margin-b-40">
                            <a 
                               class="txt-normal-s d-inline padding-o-10 bg-yellow txt-color-white" 
                               href="<?php echo $track_url ?>"
                            >
                                <?php echo $track_name ?>
                            </a>
                        </h2>
                        <h1 class=" txt-3em margin-b-30">
                            <?php echo $question_Title ?>
                        </h1>
                        <article class="description">
                            <p>
                                <?php echo $question_content ?>
                            </p>
                        </article>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <figure class="role-image" style="background-image: url(<?php echo $questionImage ?>)">
                    
                </figure>
            </div>
        </header>
        
        <section class="padding-t-80 padding-b-20">
            <div class="container-wrapper">
                <header class="padding-b-40 margin-b-40 border-b-1 border-color-darkgrey">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="txt-2em txt-bold margin-b-20">
                                Further Refine your Question
                            </h2>
                            <p class="txt-normal-s">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora rerum, asperiores aperiam laudantium.
                            </p>
                        </div>
                    </div>
                </header>
                <div class="row row-20">
                <?php
                    $topicNameQuery = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'question_to_question',
                            'from' => get_the_ID(), // You can pass object ID or full object
                        ),
                        'nopaging' => true,
                    ) );

                    /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
                    if ( $topicNameQuery->have_posts() ){
                        while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post();    
                ?>
                    <h3 class="col-4 padding-lr-20 padding-b-40 txt-medium txt-height-1-1">
                        <a href="<?php the_permalink(); ?>" class="txt-color-black">
                            <?php the_title(); ?>
                        </a>
                    </h3>
                <?php
                    endwhile;
                        wp_reset_postdata();
                    }    
                ?>
                </div>
            </div>
        </section>
        
        <section class="padding-t-80 padding-b-20">
            <div class="container-wrapper">
                <header class="margin-b-40">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="txt-2em txt-bold margin-b-20">
                                Contributions
                            </h2>
                            <p class="txt-normal-s">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora rerum, asperiores aperiam laudantium.
                            </p>
                        </div>
                        <div class="col-md-4 text-md-right">
                            <a 
                               href="<?php echo $site_url.'/make-a-contribution/?question-id='.$question_id; ?>" class="btn btn-trans-bw txt-sm no-m-b"
                               target="_blank"
                            >
                                Contribute
                            </a>
                        </div>
                    </div>
                </header>
                
                <div class="txt-sm uppercase txt-black padding-b-20">
                    <a
                        class="margin-r-10 <?php if( !$_GET['view'] == 'my-contributions' ){ echo 'txt-color-dark'; } ?>"
                        href="<?php echo $permalink ?>"
                    >
                        All Contributions
                    </a>
                    <a
                        class="txt-color-lighter <?php if( $_GET['view'] == 'my-contributions' ){ echo 'txt-color-lighter'; } ?>"
                        href="<?php echo $permalink.'/?view=my-contributions' ?>"
                    >
                        My Contributions
                    </a>
                </div>
                
                <?php if( !$_GET['view'] == 'my-contributions' ){ ?>
                <!-- All Contributions -->
                <div class="all-contributions">
                <?php
                    $topicNameQuery = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'question_to_answer',
                            'from' => get_the_ID(), // You can pass object ID or full object
                        ),
                        'nopaging' => true,
                        'post_status' => array('publish'),
                    ) );

                    /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
                    if ( $topicNameQuery->have_posts() ){
                        while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post();  
                        
                            $answer_id = get_the_ID();
                        
                            $files = rwmb_meta( 'answer-audio-media', array( 'limit' => 1 ) );
                            $file = reset( $files );
                            
                ?>
                
                <div class="answer-audio border-t-1 border-color-darkgrey padding-t-40">
                    <div class="row row-40">
                        <div class="col-md-2 padding-lr-40 padding-b-20">
                            <button class="play-button" id="controls-<?php echo $post_id ?>">
                                <i class="fa fa-play txt-2em"></i>
                            </button>
                            <div class="audio-wrapper bg-dark txt-color-white">
                                <div class="content">
                                    <div class="info">
                                        <h3 class="title">
                                            <?php 
                                                the_title();
                                            ?>
                                        </h3>
                                        <div class="author">
                                            Jerry Adaji
                                        </div>
                                    </div>
                                    <audio preload="none" controls id="<?php echo $post_id ?>">
                                        <source src="<?php echo $file['url']; ?>" type="audio/mp3">
                                    </audio>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 padding-lr-40 padding-b-20">
                            <article>
                                <h3 class="txt-lg txt-bold txt-height-1-1 margin-b-10">
                                    <a href="" class="<?php echo rwmb_get_value( 'answer-summary' ); ?>">
                                        <i class="fa fa-microphone  padding-r-5"></i>
                                        <?php 
                                            the_title();
                                            if ( get_post_status () == 'draft' ) {
                                                echo ' - Draft';
                                            }
                                        ?>
                                        
                                        <?php print_r( $audio ); ?>
                                    </a>
                                </h3>
                                <h4 class="txt-sm txt-medium margin-b-20">
                                    By
                                    <a href="">
                                        Jerry Adaji
                                    </a>
                                </h4>
                                <p class="txt-normal-s margin-b-20">
                                    <?php echo rwmb_get_value( 'answer-summary' ); ?>
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
                
                <?php
                    endwhile;
                        wp_reset_postdata();
                    }    
                ?>
                </div>
                
                <?php } else { ?>
                
                <!-- My COntributions -->
                <div class="my-contributions">
                    <div class="all-contributions">
                    <?php
                        $topicNameQuery = new WP_Query( array(
                            'relationship' => array(
                                'id'   => 'question_to_answer',
                                'from' => get_the_ID(), // You can pass object ID or full object
                            ),
                            'nopaging' => true,
                            'post_status' => array('publish', 'pending', 'draft'),
                            'author'=> get_the_author_meta('ID'),
                        ) );

                        /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
                        if ( $topicNameQuery->have_posts() ){
                            while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post();  

                                $answer_id = get_the_ID();

                                $files = rwmb_meta( 'answer-audio-media', array( 'limit' => 1 ) );
                                $file = reset( $files );

                    ?>

                    <div class="answer-audio border-t-1 border-color-darkgrey padding-t-40">
                        <div class="row row-40">
                            <div class="col-md-2 padding-lr-40 padding-b-20">
                                <button class="play-button" id="controls-<?php echo $post_id ?>">
                                    <i class="fa fa-play txt-2em"></i>
                                </button>
                                <div class="audio-wrapper bg-dark txt-color-white">
                                    <div class="content">
                                        <div class="info">
                                            <h3 class="title">
                                                <?php 
                                                    the_title();
                                                ?>
                                            </h3>
                                            <div class="author">
                                                Jerry Adaji
                                            </div>
                                        </div>
                                        <audio preload="none" controls id="<?php echo $post_id ?>">
                                            <source src="<?php echo $file['url']; ?>" type="audio/mp3">
                                        </audio>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 padding-lr-40 padding-b-20">
                                <article>
                                    <h3 class="txt-lg txt-bold txt-height-1-1 margin-b-10">
                                        <a href="" class="<?php echo rwmb_get_value( 'answer-summary' ); ?>">
                                            <i class="fa fa-microphone  padding-r-5"></i>
                                            <?php 
                                                the_title();
                                                if ( get_post_status () == 'draft' ) {
                                                    echo ' - Draft';
                                                }
                                            ?>

                                            <?php print_r( $audio ); ?>
                                        </a>
                                    </h3>
                                    <h4 class="txt-sm txt-medium margin-b-20">
                                        By
                                        <a href="">
                                            Jerry Adaji
                                        </a>
                                    </h4>
                                    <p class="txt-normal-s margin-b-20">
                                        <?php echo rwmb_get_value( 'answer-summary' ); ?>
                                    </p>
                                    <p class="txt-sm margin-b-30">
                                        <a 
                                            href="<?php echo $site_url.'/make-a-contribution/?action=edit&question-id='.$question_id.'&answer-id='.$answer_id; ?>" 
                                            class="d-inline-block txt-color-green margin-r-20"
                                        >
                                            <i class="fa fa-pencil"></i>
                                            Edit
                                        </a>
                                        <a
                                           href="<?php echo $site_url.'/make-a-contribution/?action=unpublish&question-id='.$question_id.'&answer-id='.$answer_id; ?>" 
                                           class="d-inline-block txt-color-blue margin-r-20 confirmation"
                                        >
                                            <i class="fa fa-undo"></i>
                                            Unpublish
                                        </a>
                                        <a
                                            href="<?php echo $site_url.'/make-a-contribution/?action=delete&question-id='.$question_id.'&answer-id='.$answer_id; ?>" 
                                            class="d-inline-block txt-color-red confirmation"
                                        >
                                            <i class="fa fa-trash"></i>
                                            Delete
                                        </a>
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>

                    <?php
                        endwhile;
                            wp_reset_postdata();
                        }    
                    ?>
                    </div>
                </div>
                
                <?php } ?>
                
                <!--<div class="answer-audio border-t-1 border-color-darkgrey padding-t-40">
                    <div class="row row-40">
                        <div class="col-md-2 padding-lr-40 padding-b-20">
                            <a class="play-button-video popup-video" href="https://www.youtube.com/watch?v=3qyhgV0Zew0">
                                <i class="fa fa-play"></i>
                            </a>
                            <div class="audio-wrapper bg-dark txt-color-white">
                                <div class="content">
                                    <div class="info">
                                        <h3 class="title">
                                            Build a Social Media Calender
                                        </h3>
                                        <div class="author">
                                            Jerry Adaji
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 padding-lr-40 padding-b-20">
                            <article>
                                <h3 class="txt-lg txt-bold txt-height-1-1 margin-b-10">
                                    <a href="" class="">
                                        <i class="fa fa-film  padding-r-5"></i>
                                        Build a Social Media Calender
                                    </a>
                                </h3>
                                <h4 class="txt-sm txt-medium margin-b-20">
                                    By
                                    <a href="">
                                        Jerry Adaji
                                    </a>
                                </h4>
                                <p class="txt-normal-s margin-b-30">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus repudiandae sed ipsum id, praesentium. Maiores possimus facere, dolores ducimus doloribus similique voluptas. Dicta nobis adipisci libero iste, incidunt! Voluptatem, nam.
                                </p>
                            </article>
                        </div>
                    </div>
                </div>-->
            </div>
        </section>
        
    </main>
    
    <?php endwhile; // end of the loop. ?>

    <?php get_footer() ?>
    
