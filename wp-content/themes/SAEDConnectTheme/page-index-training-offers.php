    <?php /*Template Name: Homepage-Training-Offers*/ ?>
    
    <?php get_header() ?>
    
    <?php
        /* Publication key for getting Admin Approved content */
        $publication_key = 'publication_status';
    ?>
   
    <main class="main-content">
        <header class="overview-header container-wrapper">
            <h1 class="txt-xlg txt-medium txt-height-1-2">
                Training Offers
            </h1>
        </header>
        
        <!-- General Section -->
        <section class="container-wrapper padding-t-40">
            <div class="row row-5">
            <?php
                function truncate($string, $length){
                    if (strlen($string) > $length) {
                        $string = substr($string, 0, $length) . '...';
                    }

                    return $string;
                }
    
                wp_reset_postdata();
                wp_reset_query();
                $temp = $wp_query; $wp_query= null;
                $wp_query = new WP_Query();
                $wp_query->query( 
                    array(
                        'post_type' => 'training-offer',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'meta_query' => array(
                            array(
                                'key' => $publication_key,
                                'value' => 'admin_published'
                            )
                        ),
                    ) 
                );

                if ( $wp_query->have_posts() ) {

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Variables */
                    $post_id = $post->ID;    //Get Program ID

                    /* Get Opportunity Banner */
                    $images = rwmb_meta( 'training-upload-banner', array( 'limit' => 1 ) );
                    $image = reset( $images );
            ?>
                <div class="col-md-3 padding-lr-5 padding-b-20 d-flex">
                    <a href="<?php the_permalink() ?>" class="home-community-card">
                        <article class="content">
                        <?php
                            $meta = get_post_meta($post_id, 'training_offer_image', true);
                            if($meta){

                            }
                        ?>
                            <figure class="image-box" style="background-image: url('<?php echo $meta; ?>')">

                            </figure>
                            <div class="info bg-white">
                                <h3 class=" txt-medium margin-b-10">
                                    <?php the_title() ?>
                                </h3>
                                <p class="txt-sm">
                                    <?php
                                        $meta = get_post_meta($post_id, 'business_description', true);
                                        if($meta){
                                            echo truncate($meta, 70);
                                        }
                                    ?>
                                </p>
                            </div>
                        </article>  
                    </a>   
                </div>
            <?php
                    endwhile;

                }else{
            ?>
                <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                    <h2 class="txt-lg txt-medium">
                        No Businesses found.
                    </h2>
                </div>   

            <?php

                }

                restore_current_blog();
            ?>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>