    <?php get_header() ?>
    
    <?php
        $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

        if($incoming_from_saedconnect){
            
            /* Include saedconnect Navigation */
            get_template_part( 'template-parts/navigation-saedconnect' );
        }
    ?>

    <main class="main-content">
        
        <?php
            $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

            if(!$incoming_from_saedconnect){

                /* Include Skilli Navigation */
                get_template_part( 'template-parts/navigation-skilli-secondary' );
            }
        ?>

        <section class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        <?php echo rwmb_meta( 'skilli-program-name' ); ?>
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        <?php echo rwmb_meta( 'skilli-program-description' ); ?>
                    </h2>
                </div>
            </div>
        </section>
        

        <section class="bg-yellow padding-tb-40">
            <div class="container-wrapper">
                <header class="txt-light margin-b-30">

                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Different Entry Points to a great career
                    </h2>
                    <p class="txt-height-1-">
                        You can start your tech journey either as a Wordpress developer, a frontend web developer,
                        <br>
                        a full stack developer, or a digital marketer. Which ever your choice, there is a track for you.
                    </p>
                </header>
                <?php // Display posts
                    $program_query = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'program_to_track',
                            'from' => $post->ID, // You can pass object ID or full object
                        ),
                        'nopaging' => true,
                    ) );
                    while ( $program_query->have_posts() ) : $program_query->the_post();
                
                        $program_id = $post->ID;    //Get Program ID
                        $program_name = $post->post_title;    //Get Program Name

                        //  Get Program Featured Image
                        $program_images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ) );
                        $program_images = reset( $program_images );

                        //  Get Program Duration
                        $program_duration = rwmb_meta( 'program-duration' );

                        //  Get Program Pre-requisites
                        $program_pre_requisites = rwmb_meta( 'program-prerequisites' );

                        //  Get Program Locations
                        $program_locations;
                        $prepend_text = ', ';
                
                        $all_states = array(
                            'abia',
                            'adamawa',
                            'akwa-ibom',
                            'anambra',
                            'bauchi',
                            'bayelsa',
                            'benue',
                            'borno',
                            'cross-river',
                            'delta',
                            'ebonyi',
                            'edo',
                            'ekiti',
                            'enugu',
                            'gombe',
                            'imo',
                            'jigawa',
                            'kaduna',
                            'kano',
                            'katsina',
                            'kebbi',
                            'kogi',
                            'kwara',
                            'lagos',
                            'nassarawa',
                            'niger',
                            'ogun',
                            'ondo',
                            'osun',
                            'oyo',
                            'plateau',
                            'rivers',
                            'sokoto',
                            'taraba',
                            'yobe',
                            'zamfara'
                        );

                        foreach($all_states as $state){
                            if(rwmb_meta( 'program-location-'.$state )){

                                $program_locations .= ucfirst($state) . $prepend_text;
                            }
                        }

                        if($program_locations){
                            $program_locations = chop($program_locations, $prepend_text);
                        }                
                ?>
                           
               
                <div class="overflow-hidden card-shadow-2 margin-b-40">
                    <div class="row row-40 ">
                        <div class="col-md-7 order-2 order-md-1 padding-lr-40 bg-white">
                            <div class="padding-o-40">
                                <h2 class="txt-normal-s txt-mediym uppercase margin-b-10">
                                    <?php the_title() ?>
                                </h2>
                                <h2 class="txt-xxlg txt-medium txt-height-1-2 padding-b-20">
                                    <?php echo rwmb_meta( 'program-name' ); ?>
                                </h2>
                                <article class="text-box sm">
                                    <?php echo rwmb_meta( 'program-description' ); ?>
                                </article>
                                <div class="row justify-content-between margin-t-20 padding-t-20 border-t-1 border-color-darkgrey">
                                    <div class="col-md-6">
                                        <div class="faq-collapse">
                                            <button class="faq-btn txt-normal-s" type="button" data-toggle="collapse" data-target="#program-<?php echo $program_id ?>" aria-expanded="false" aria-controls="collapseExample">
                                                Learn more
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-6 txt-sm text-right">
                                        <p>
                                            <span class="">Next Start Date: </span>
                                            <span class="txt-bold">
                                            <!-- Get All Child Scheduled Dates --> 
                                            <?php 
                                                $isFirst = true;

                                                $nav_3 = new WP_Query( array(
                                                    'relationship' => array(
                                                        'id'   => 'track_to_scheduled_date',
                                                        'from' => $program_id, // You can pass object ID or full object
                                                    ),
                                                    'nopaging' => true,
                                                    'posts_per_page' => '1'
                                                ) );
                                                while ( $nav_3->have_posts() ) : $nav_3->the_post();
                                            ?> 
                                                <?php
                                                    if($isFirst){

                                                        rwmb_the_value( 'program-schedule-date', array( 'format' => 'd' ) );
                                                        echo ' ';
                                                        rwmb_the_value( 'program-schedule-date', array( 'format' => 'F' ) );

                                                    }

                                                ?>
                                            <?php
                                                $isFirst = false;

                                                endwhile;
                                                wp_reset_postdata();
                                            ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 order-1 order-md-2 padding-lr-40 generic-card-bg" style="background-image:url('<?php echo $program_images['full_url']; ?>');">
                            <div class="cta text-center">
                                <div>
                                    <button
                                        type="button"
                                        class="btn btn-blue"
                                        data-toggle="modal"
                                        data-target="#formModal"
                                        track-id="<?php echo $program_id ?>"
                                        track-name="<?php echo $program_name ?>"
                                    >
                                        Enroll for free online
                                    </button>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-white" data-toggle="modal" data-target="#smsEnrollModal-<?php echo $program_id ?>">
                                        Enroll for free via SMS
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-40 ">
                        <div class="col overflow-hidden">
                            <div class="col-md-12 order-3 padding-lr-40 faq-collapse border-t-1 border-color-darkgrey">
                                <div class="faq-collapse collapse padding-o-40 bg-white" id="program-<?php echo $program_id ?>">
                                    <div class="card card-body no-padding">
                                        <h3 class="txt-xlg txt-medium margin-b-40">
                                            Curriculum
                                        </h3>
                                        <div class="row row=40">
                                            <ul class="col-md-8 padding-r-40 venture-list">
                                            <!-- Get All Child Curriculums --> 
                                            <?php 

                                                $curriculum_query = new WP_Query( array(
                                                    'relationship' => array(
                                                        'id'   => 'track_to_curriculum',
                                                        'from' => $program_id, // You can pass object ID or full object
                                                    ),
                                                    'nopaging' => true,
                                                ) );
                                                while ( $curriculum_query->have_posts() ) : $curriculum_query->the_post();
                                    
                                                $curriculum_id = $post->ID;
                                            ?>   
                                                <li>
                                                    <div class="title txt-normal-s">
                                                        <?php the_title(); ?>
                                                    </div>
                                                    <article class=" content text-box sm margin-b-20">
                                                        <?php echo rwmb_meta( 'curriculum-description' ); ?>
                                                        
                                                        <!-- Get All Child Projects --> 
                                                        <?php 

                                                            $nav_3 = new WP_Query( array(
                                                                'relationship' => array(
                                                                    'id'   => 'curriculum_to_project',
                                                                    'from' => $curriculum_id, // You can pass object ID or full object
                                                                ),
                                                                'nopaging' => true,
                                                            ) );
                                                            while ( $nav_3->have_posts() ) : $nav_3->the_post();
                                                        ?> 
                                                            <button type="button" class="project" data-toggle="modal" data-target="#projectModal">
                                                                <i class="fa fa-file-o"></i>
                                                                <span>
                                                                    <?php the_title() ?>
                                                                </span>
                                                            </button>
                                                        <?php
                                                            endwhile;
                                                            wp_reset_postdata();
                                                        ?>
                                                    </article>
                                                </li>

                                            <?php
                                                endwhile;
                                                wp_reset_postdata();
                                            ?>
                                            </ul>
                                            <div class="col-md-4 padding-o-40 bg-grey txt-normal-s">
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                                    <h3 class="">
                                                        <span class="txt-bold">
                                                            Duration:
                                                        </span>
                                                        <span>
                                                            <?php echo $program_duration ?>
                                                        </span>
                                                    </h3>
                                                </div>
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                                    <h3 class="margin-b-20">
                                                        <span class="txt-bold">
                                                            Next Start Date:
                                                        </span>
                                                        <span>
                                                        <!-- Get All Child Scheduled Dates --> 
                                                        <?php 
                                                            $isFirst = true;
                                                            
                                                            $nav_3 = new WP_Query( array(
                                                                'relationship' => array(
                                                                    'id'   => 'track_to_scheduled_date',
                                                                    'from' => $program_id, // You can pass object ID or full object
                                                                ),
                                                                'nopaging' => true,
                                                                'posts_per_page' => '1'
                                                            ) );
                                                            while ( $nav_3->have_posts() ) : $nav_3->the_post();
                                                        ?> 
                                                            <?php
                                                                if($isFirst){
                                                                    
                                                                    rwmb_the_value( 'program-schedule-date', array( 'format' => 'd' ) );
                                                                    echo ' ';
                                                                    rwmb_the_value( 'program-schedule-date', array( 'format' => 'F' ) );
                                                                    
                                                                }
                                                            
                                                            ?>
                                                        <?php
                                                            $isFirst = false;
                                                            
                                                            endwhile;
                                                            wp_reset_postdata();
                                                        ?>
                                                        </span>
                                                    </h3>
                                                </div>
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                                    <h3 class="margin-b-20">
                                                        <span class="txt-bold">
                                                            Enroll for free
                                                        </span>
                                                    </h3>
                                                    
                                                    <div class="cta">
                                                        <button type="button" class="btn btn-trans-bw txt-normal-s" data-toggle="modal" data-target="#formModal">
                                                            Online
                                                        </button>
                                                        <button type="button" class="btn btn-trans-bw txt-normal-s" data-toggle="modal" data-target="#smsEnrollModal-<?php echo $program_id ?>">
                                                            Via SMS
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">      
                                                    <div class="angle-collapse">
                                                        <button class="angle-btn txt-bold" type="button" data-toggle="collapse" data-target="#detail-1" aria-expanded="false" aria-controls="collapseExample">
                                                            Pre-requisites
                                                        </button>
                                                        <div class="collapse" id="detail-1">
                                                            <div class="card card-body bg-trans">
                                                                <article class="text-box sm">
                                                                    <?php echo $program_pre_requisites; ?>
                                                                </article>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">      
                                                    <div class="angle-collapse">
                                                        <button class="angle-btn txt-bold" type="button" data-toggle="collapse" data-target="#detail-2" aria-expanded="false" aria-controls="collapseExample">
                                                            Locations
                                                        </button>
                                                        <div class="collapse" id="detail-2">
                                                            <div class="card card-body bg-trans">
                                                                <article class="text-box sm">
                                                                    <p class="txt-normal-s txt-height-1-7">
                                                                        <?php echo $program_locations ?>
                                                                    </p>
                                                                </article>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </section>
        
        <section class="bg-ash padding-t-80 padding-b-60 txt-light txt-color-white">
            <div class="container-wrapper">
                <header class="txt-light margin-b-60">
                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Why Pursue a Career in Tech?
                    </h2>
                    <!--<p class="txt-medium">
                        Find all you need to plan your business and get it off the ground.
                        <br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                    </p>-->
                </header>
                <div class="row row-10">
                    <div class="col-md-3 padding-lr-10 padding-b-20">
                        <figure class="margin-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/make-more-money-white.png" alt="" width="50">
                        </figure>
                        <h3 class="txt-bold margin-b-15">
                            Make more money
                        </h3>
                        <p class="txt-normal-s txt-height-1-7">
                            Technology careers are one of the highest-paying careers – from entry level. Even better, they don’t require a college degree to start.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-10 padding-b-20">
                        <figure class="margin-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/high-demand-white.png" alt="" width="50">
                        </figure>
                        <h3 class="txt-bold margin-b-15">
                            High Demand
                        </h3>
                        <p class="txt-normal-s txt-height-1-7">
                            It is Projected that there will be1 million job shortfall for IT roles within the next 10 years. Tech roles are also projected to grow two-thirds faster than the average for all jobs. Secure your place in this future.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-10 padding-b-20">
                        <figure class="margin-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/career-flexibility-white.png" alt="" width="50">
                        </figure>
                        <h3 class="txt-bold margin-b-15">
                            Career Flexibility
                        </h3>
                        <p class="txt-normal-s txt-height-1-7">
                            Tech Careers give you the freedom and flexibility to work from anywhere in the world. You can be in a remote town in Nigeria and be working with a firm in China.
                        </p>
                    </div>
                    <div class="col-md-3 padding-lr-10 padding-b-20">
                        <figure class="margin-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/love-your-work-white.png" alt="" width="50">
                        </figure>
                        <h3 class="txt-bold margin-b-15">
                            Love Your Work
                        </h3>
                        <p class="txt-normal-s txt-height-1-7">
                            Tech work can be fun and stimulating. No 2 days are the same.  Wake up everyday and feel valuable, stimulated, and excited to open your laptop.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="padding-t-80 padding-b-40">
            <div class="container-wrapper">
                <header class="padding-b-80">
                    <h1 class="txt-xxlg txt-bold txt-height-1-1 margin-b-20">
                        What you get
                    </h1>
                    <h2 class="txt-height-1-5">
                        We are going all out to ensure you get all the support &amp; resources you require to start and excel in your tech career.
                    </h2>
                </header>
                <div class="row row-40">
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/expert-instruction-and-guidance.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Expert Instruction &amp; Guidance
                                </p>
                                <div class="collapse" id="collapse-1">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Learn with industry-vetted curriculum created by Subject-Matter Experts.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access to experienced Mentors available 12-14 hours/day during the week to help you in real time.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access useful Templates, Reference Guides &amp; tailored advice from experts.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p>
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/immersive-learning.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10 txt-height-1-4">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Immersive Learning
                                </p>
                                <div class="collapse" id="collapse-2">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Instructor &amp; Mentor Support.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Access real-live entrepreneur interviews that help you appreciate the practical application of lessons shared.
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Collaborative tasks.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p>
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/get-real-world-experience.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Get Real-World Experience
                                </p>
                                <div class="collapse" id="collapse-3">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    You will work on challenging projects to solve real problems during the sessions, 
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    You are challenged to define and develop a personal project to apply the skills being learnt.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-1">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/amazing-system-support.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Amazing Support System.
                                </p>
                                <div class="collapse" id="collapse-4">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Advisor Support beyond the end of the session
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Peer-to-Peer Motivation in the alumni community
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Tools, resources and programs that would make your tech career journey easier
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/career-support.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Career Support
                                </p>
                                <div class="collapse" id="collapse-5">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Get support to prepare you for the technical recruiting process
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Resume and portfolio critique and a review of your LinkedIn and GitHub profiles
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Mock Interviews
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Troubleshooting and work on weak areas
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <div class="row">
                            <div class="col-md-3 padding-lr-10">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/access-to-Growth-opportunities.png" alt="">
                            </div>
                            <div class="col-md-9 padding-lr-10">
                                <p class="txt-bold padding-b-5 txt-height-1-4">
                                    Access to Growth Opportunities
                                </p>
                                <div class="collapse" id="collapse-6">
                                    <div class="padding-b-20">
                                        <ul class="icon-list txt-normal-s">
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Events and network meetings where you can meet other great tech gurus like you
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Discounts on online services like hosting, plugins, etc
                                                </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-chevron-right"></i>
                                                <span>
                                                    Be the first t know about internship and job openings for tech talent shared by partners and alumni
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="">
                                    <a class="txt-color-green" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">
                                        Learn more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="padding-t-80 padding-b-40 bg-grey">
            <div class="container-wrapper">
                <header class="padding-b-40">
                    <h1 class="txt-xxlg txt-bold txt-height-1-1 margin-b-20">
                        How the Tech Career Mentorship scheme Works
                    </h1>
                    <h2 class="txt-height-1-5">
                        The Tech Career Mentorship Scheme utilizes an innovative model to deliver
                        <br>
                        game changing support to you anywhere you are.
                    </h2>
                </header>
                <div class="how-it-works-tab">
                    <ul class="nav nav-tabs" id="skillTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="location-1-skill-tab-1" data-toggle="tab" href="#location-1-skill-1" role="tab" aria-controls="location-1-skill-1" aria-selected="true">
                                Blended Session
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="location-1-skill-tab-2" data-toggle="tab" href="#location-1-skill-2" role="tab" aria-controls="location-1-skill-2" aria-selected="false">
                                Offline Session
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="skillTabContent text-center">
                        <div class="tab-pane fade show active" id="location-1-skill-1" role="tabpanel" aria-labelledby="location-tab-1">
                            <div class="row row-10">
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/join-a-track.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Join a track
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Join a track of your choice before the deadline date of a new session. There are limited spaces available per session. You will be assigned a personal mentor for the session (optional)
                                    </p>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/participate-in-virtual-learning.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Participate
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Participate in virtual learning sessions delivered through the Telegram Messaging App (How to download the telegram App), WhatsApp &amp; other third party Apps.
                                    </p>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/engage-in-all-the-session.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Engage
                                    </h3>
                                    <p class="txt-sm txt-height-1-7 margin-b-15">
                                        Engage in all the session activities and unlock your capacity 
                                    </p>
                                    <ul class="icon-list txt-sm">
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Complete all the assigned tasks and get feedback
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Participate in group collaborative sessions
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Get real-time advise and feedback from your assigned mentor
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/apply-the-lessons-learnt.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Apply the lessons learnt
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Apply the lessons learnt &amp; resources provided to complete your personal project.  
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="location-1-skill-2" role="tabpanel" aria-labelledby="location-tab-2">
                            <div class="row row-10">
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/join-a-track.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Join a track
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Join a track of your choice before the deadline date of a new session. There are limited spaces available per session. You will be assigned a personal mentor for the session (optional)
                                    </p>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/participate-in-virtual-learning.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Participate
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Participate in virtual learning sessions delivered through the Telegram Messaging App (How to download the telegram App), WhatsApp &amp; other third party Apps.
                                    </p>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/engage-in-all-the-session.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Engage
                                    </h3>
                                    <p class="txt-sm txt-height-1-7 margin-b-15">
                                        Engage in all the session activities and unlock your capacity 
                                    </p>
                                    <ul class="icon-list txt-sm">
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Complete all the assigned tasks and get feedback
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Participate in group collaborative sessions
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Get real-time advise and feedback from your assigned mentor
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <figure class="margin-b-30">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/skilli/apply-the-lessons-learnt.png" alt="" height="100">
                                    </figure>
                                    <h3 class="txt-medium margin-b-20">
                                        Apply the lessons learnt
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        Apply the lessons learnt &amp; resources provided to complete your personal project.  
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-grey padding-t-80">
            <div class="container-wrapper">
                <header class="margin-b-100 txt-light">
                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Meet your Mentors
                    </h2>
                    <p class="txt-height-1-7">
                        The Tech Mentorship Scheme is led by actively-practicing,
                        <br>
                        world class programmers who actually make a living from coding.
                    </p>
                </header>
                <div class="row row-10 text-center">
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/onewoman.png" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Hussein Alayo
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                                <!--<p class="txt-sm">
                                    Find all you need to plan your business and get it off the ground. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consecteturt.
                                </p>-->
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/m_uwede.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Oguntade Samson
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/onewoman.png" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Tunde Omiwole
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Jerry Adaji
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Tayo Erubu
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3 padding-lr-5 margin-b-80">
                        <div class="card-shadow-2 bg-white padding-b-40 padding-lr-20">
                            <figure class="margin-b-20">
                                <img class="profile-image offset-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/people/b_caleb.jpg" alt="" width="100">
                            </figure>
                            <article>
                                <h4 class="txt-bold margin-b-5">
                                    Odugbemi
                                </h4>
                                <h5 class="txt-normal-s margin-b-15">
                                    5 years+ Industry experience
                                </h5>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="container-wrapper bg-ghostwhite padding-tb-60">
            <header class="margin-b-40">
                <h2 class="txt-xxlg txt-medium">Frequently Asked Questions</h2>    
            </header>
            <div class="row row-10">
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-1" aria-expanded="false" aria-controls="collapseExample">
                            How much does it cost to join a session?
                        </button>
                        <div class="collapse" id="faq-1">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-2" aria-expanded="false" aria-controls="collapseExample">
                            How much does it cost to join a session?
                        </button>
                        <div class="collapse" id="faq-2">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-3" aria-expanded="false" aria-controls="collapseExample">
                            How much does it cost to join a session?
                        </button>
                        <div class="collapse" id="faq-3">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-5" aria-expanded="false" aria-controls="collapseExample">
                            How much does it cost to join a session?
                        </button>
                        <div class="collapse" id="faq-5">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                    <p class="txt-normal-s">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
            $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

            if(!$incoming_from_saedconnect){

                /* Include Skilli Footer */
                get_template_part( 'template-parts/footer-skilli' );
            }
        ?>
    </main>
    
    <?php
        $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

        if($incoming_from_saedconnect){

            /* Include Saedconnect Footer */
            get_template_part( 'template-parts/footer-saedconnect' );
        }
    ?>

    
    <!-- Enroll Online Modal -->
    <div class="modal fade font-main coming-soon-modal" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row content">
                    <div class="left gf-white">
                       <div class="gform_heading">
                            <h3 class="gform_title">
                                Give us your information & we will contact you.
                            </h3>
                            <span class="gform_description">
                                Tip: Enter outstanding achievements (not listed under your experiences) that show you can go above and beyond basic job expectations.)
                            </span>
                        </div>
                        
                        <?php
                            $track_id = 123;
                            $track_name = "Test Track Name";
                        ?>
                        
                        <!-- Gravity Form Begin -->
                        <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
                        <!-- Gravity Form End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php // Display posts
        $program_query = new WP_Query( array(
            'relationship' => array(
                'id'   => 'program_to_track',
                'from' => $post->ID, // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
        while ( $program_query->have_posts() ) : $program_query->the_post();

                $program_id = $post->ID;    //Get Program ID
                //  Get Program Featured Image
                $program_images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ) );
                $program_images = reset( $program_images );
    ?>
    
    <!-- Enroll via SMS Modal -->
    <div class="modal fade font-main coming-soon-modal" id="smsEnrollModal-<?php echo $program_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row content text-center">
                    <div class="left gf-white">
                        <form>
                            <div class="gform_heading">
                                <h2 class="txt-4em text-center margin-b-10">
                                    <i class="fa fa-mobile-phone "></i>
                                </h2>
                                <h3 class="txt-xlg txt-medium margin-b-30">
                                    How to enroll via SMS
                                </h3>
                                <div class="text-center">
                                    <p class="code-label txt-bold text-center margin-b-30">
                                        <span>
                                            Course Code:
                                        </span>
                                        <?php echo rwmb_meta( 'program-sms-code' ); ?>
                                    </p>
                                </div>
                                <p class="txt-normal-s margin-b-20">
                                    To enroll, send an SMS to 30448 in this format:
                                </p>
                                <p class="txt-normal-s margin-b-20">
                                    [SAED], [Course Code], [Full Name], [State of Residence], [Contact Telephone Number], [WhatsApp Number if any]
                                </p>
                                <p class="txt-normal-s margin-b-20">
                                    eg. 
                                </p>
                                <p class="txt-normal-s margin-b-20">
                                    SAED, <?php echo rwmb_meta( 'program-sms-code' ); ?>, John Doe, Kebbi, 07031234567, 07031234567
                                </p>
                                <div class="text-center">
                                    <p class="code-label txt-bold text-center margin-b-30">
                                        Send to 30448
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php
        endwhile;
        wp_reset_postdata();
    ?>
    
    
    
    <?php
        //  Get Confirmation Message    
        
        $confirmation_message = esc_html($_REQUEST['confirmation-message']);

        if($confirmation_message){
    ?>
        
        <div class="modal fade font-main coming-soon-modal" id="formConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="row content">
                        <div class="left gf-white">
                           <div class="gform_heading">
                                <h3 class="gform_title">
                                    <?php echo $confirmation_message ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php
        }
    ?>
        
        

    <?php get_footer() ?>
    
    <?php
        $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

        if($incoming_from_saedconnect){
    ?>
    
       <script>
            $(document).ready(function(){
                /*
                *   Auto populate form with Track ID & Name
                */
                $('#formModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget); // Button that triggered the modal
                    var track_id = button.attr('track-id'); // Extract Track ID
                    var track_name = button.attr('track-name'); // Extract Track Name

                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this);

                    //modal.find('.modal-title').text('New message to ' + recipient)
                    modal.find('#input_1_6').val(track_id);
                    modal.find('#input_1_7').val(track_name);
                })


                //Confirmation Modal
                $('#formConfirmationModal').modal('show')

                //Clear URL
               // var uri = window.location.toString();
                //if (uri.indexOf("?") > 0) {
                    //var clean_uri = uri.substring(0, uri.indexOf("?"));
                    //window.history.replaceState({}, document.title, clean_uri);
                //}

                //Append Saedconnect Query string
                $('a').each(function(index, element) {
                  var newAttr = $(element).attr('href') + '?saedconnect=true';
                  $(element).attr('href', newAttr);
                });
            });
        </script>
        
    <?php } ?>