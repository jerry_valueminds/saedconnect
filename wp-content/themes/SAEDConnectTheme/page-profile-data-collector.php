<?php /*Template Name: Profile - Data Collector*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        Data Collector Dashboard
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        If you want to become a data collector, and make some extra money, register here.
                    </p>
                </article>
                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Personal Information                
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/growth-programs/track/starter-track/entry/40/?edit=68fd6eebe5&amp;gvid=637" 
                               class="edit-btn"
                            >
                                Edit
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <div class="margin-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                Overview Title                        
                            </p>
                            <p class="txt-sm">
                                Jerry Adaji                        
                            </p>
                        </div>

                        <div class="margin-b-20">
                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                About Me                        
                            </p>
                            <p class="txt-sm">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora nam excepturi cumque! Nihil dolores, itaque maiores. Iusto officiis ad tenetur, at laborum minima ab libero, ex est incidunt eos magni.                      
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>