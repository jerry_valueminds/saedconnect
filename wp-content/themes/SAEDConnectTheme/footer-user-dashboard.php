<!--Load Scripts-->
<?php wp_footer(); ?>

<script type="text/javascript">
    $('.confirmation').on('click', function () {
        return confirm('Are you sure?');
    });
    
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure you want to delete this item? This action can not be undone.');
    });
</script>

<script type="text/javascript">
    tinymce.init({
        selector: '.editor',
        menubar: false,
        plugins: "lists",
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright | link image | numlist bullist"
    });
</script>

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('input.custom-check').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>