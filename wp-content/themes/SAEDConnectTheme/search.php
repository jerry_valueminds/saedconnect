    
    <?php get_header() ?>
    
    <?php
    
        /* Select Search form */
        if(isset($_GET['search-type'])){
            $type = $_GET['search-type'];
            
            /* Check for type of Template to use */
            if($type == 'course') {
                
                /* If Template for Courses */
                get_template_part( 'template-parts/search/courses-search' );
                
            } elseif($type == 'xplore-business') {
                
                /* Template for xPlore-Business */
                get_template_part( 'template-parts/search/xplore-business-search' );
                
            } elseif($type == 'xplore-career') {
                
                /* Template for xPlore-Career */
                get_template_part( 'template-parts/search/xplore-career-search' );
                
            } elseif($type == 'search-programs') {
                
                /* Template for xPlore-Career */
                get_template_part( 'template-parts/search/programs-search' );
                
            
            } elseif($type == 'search-make-money-opportunity') {
                
                /* Template for xPlore-Career */
                get_template_part( 'template-parts/search/make-money-search' );
                
            }
            
        }
    ?>
    
    <?php get_footer() ?>