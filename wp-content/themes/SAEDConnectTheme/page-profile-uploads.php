<?php /*Template Name: Profile - Uploads*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>

            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        My Upload        
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Upload key documents requested, here, to support your profile.
                    </p>
                </article>
                <p class="txt-lg">
                    Coming soon.
                </p>
            </div>

        </section>
    </main>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>