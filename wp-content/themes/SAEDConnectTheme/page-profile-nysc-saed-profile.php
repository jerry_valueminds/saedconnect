<?php /*Template Name: Profile - NYSC SAED*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <?php
        /* State Code Match */
        $state_match => {
            
        }
    ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        NYSC SAED
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Here you can view the status of all the opportunities you have applied for.
                    </p>
                </article>
                
                <div class="section-wrapper">
                    <?php
                        switch_to_blog(1);

                        $gf_id = 4; //Form ID
                        //$gv_id = 1385; //Gravity View ID
                        $gv_id = 1445; //Gravity View ID
                        $title = 'Corp Member Information';

                        $entry_count = 0;

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                            )
                        );

                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria ); 


                        $entry = $entries[0];
                    ?>

                    <?php if( rgar( $entry, '17' ) == 'Corp Member' ){ //If no entry ?>
                                   
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                        <?php
                            $call_up_number = rgar( $entry, 29 );
                                                                      
                            /* Get Call-Up Number User entered */  
                            $query_cu_number = $call_up_number;
                                                        
                            /* Check Call-Up number in from DB */
                            //$nysc_list_db = new wpdb('root','root','nysc_db','localhost'); //Local
                            $nysc_list_db = new wpdb('root','umMv65ekyMRxfNfm','nysc_db','localhost'); //Server
                            $corp_member = $nysc_list_db->get_row("SELECT call_up_number, verification_status, first_name, last_name, middle_name FROM corp_member_profile WHERE call_up_number = '".$query_cu_number."' LIMIT 0,1");
                        ?>
                        
                        <?php
                            /* If User is trying to verify Call-Up Number for the first time */
                            if ( !$corp_member->verification_status == 'verified' ){
                        ?>
                                    
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Name          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, '4.3' ).' '.rgar( $entry, '4.6' ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        State of Deployment         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 18 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        State Code
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 19 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Call Up Number
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo $call_up_number; ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        University Attended         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 26 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Course of Study         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 21 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Class of Degree        
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 23 ); ?>
                                    </p>
                                </div>
                            </div>
                            <p class="padding-t-20">
                                <a href="https://www.saedconnect.org/corp-member-verification/?call-up-number=<?php echo $call_up_number; ?>" class="btn btn-ash txt-sm">
                                    Verify Call Up Number
                                </a>
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=nysc-saed&form-title=Corp Member Information' ?>"
                                    class="btn btn-trans-bw txt-sm"
                                >
                                    Edit Corp Member Information
                                </a>
                            </p>
                            
                        <?php } else { ?>
                        
                            <p class="padding-b-15">
                                Your Corp Member Profile is Activated.
                            </p>
                        
                        <?php } ?>
                           
                        </div>
                                    
                    <?php } else { ?>
                           
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                Registration
                            </h2>
                        </div>
                        <div class="entry">
                            <p class="txt-normal-s padding-b-20">
                                You are not registered for the SAED Program yet. Complete your registration.
                            </p>
                            <p class="padding-t-20">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="1385" action="edit" return="url" /]').'&view=nysc-saed&form-title=Corp Member Information' ?>" 
                                    class="btn btn-trans-blue txt-sm"
                                >
                                    Add Corp Member Information
                                </a>
                            </p>
                        </div>
                            
                    <?php } ?>

                    <?php restore_current_blog(); ?>
                    
                </div>
            
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>