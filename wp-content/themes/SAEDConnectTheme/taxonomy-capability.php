<?php get_header() ?>
<?php
    /* Get view */
    $view = $_REQUEST['view'];
    
    /* View URL */
    global $wp;
    $page_url = home_url( $wp->request );

    $product_category = get_queried_object();
    $product_category_id = $product_category->term_id;
?>
    <main class="main-content">
        <header class="container-wrapper border-b-1 border-color-darkgrey">
            <div class="row align-items-center padding-t-20 padding-b-60">
                <div class="col-md-6">
                    <h1 class="txt-sm">
                        <a href="<?php echo get_site_url() ?>" class="txt-color-dark padding-r-10">
                            Sell a Service
                        </a>
                        <i class="fa fa-angle-right padding-r-10"></i>
                        <a href="<?php echo get_term_link($product_category_id); ?>" class="txt-color-dark">
                            <?php echo $product_category->name; ?>
                        </a>
                    </h1>
                </div>
            </div>
            <div class="row row-10 margin-b-40">
                <div class="col-md-8 padding-lr-10">
                    <h2 class="txt-xlg txt-medium margin-b-15">
                        <?php echo $product_category->name; ?>
                    </h2> 
                    <p class="txt-normal-s">
                        Browse through some of our most popular Products / Services.
                    </p>  
                </div>
                <div class="col-md-4 text-right padding-lr-10">
                    <a href="" class="btn btn-blue no-m-b txt-xs">
                        Offer a Service
                    </a>
                </div>
            </div>
        </header>
        
        <section class="container-wrapper bg-ash padding-t-10">
            <!-- Menu -->
            <ul class="service-offerings-tab">
                <li>
                    <a 
                       href="<?php echo $page_url ?>"
                       class="<?php echo ($view) ? '' : 'active' ?>" 
                    >
                        All
                    </a>
                </li>
                <li>
                    <a 
                       href="<?php echo $page_url.'?view=freelancers'; ?>"
                       class="<?php echo ($view == 'freelancers') ? 'active' : '' ?>" 
                    >
                        Freelancer Directory
                    </a>
                </li>
                <li>
                    <a 
                       href="<?php echo $page_url.'?view=offers'; ?>"
                       class="<?php echo ($view == 'offers') ? 'active' : '' ?>" 
                    >
                        Service Offers
                    </a>
                </li>
                <li>
                    <a 
                        href="<?php echo $page_url.'?view=requests'; ?>"
                        class="<?php echo ($view == 'requests') ? 'active' : '' ?>"   
                    >
                        Task Requests
                    </a>
                </li>
                <li>
                    <a 
                       href="<?php echo $page_url.'?view=jobs'; ?>"
                       class="<?php echo ($view == 'jobs') ? 'active' : '' ?>"
                    >
                        Job Offers
                    </a>
                </li>
            </ul>
        </section>
        
        <?php 
            if( $view ){
                
                get_template_part( 'template-parts/capability/'.$view );
            
            } else {    
                
                get_template_part( 'template-parts/capability/overview' );
            }
        ?>
    </main>

<?php get_footer() ?>