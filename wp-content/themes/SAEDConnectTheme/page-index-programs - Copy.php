    <?php /*Template Name: Homepage-Programs*/ ?>
    
    <?php get_header() ?>

	<main class="main-content">
		<header class="overview-header container-wrapper">
            <div class="info-box">
                <div class="info">
                    <h1 class="title txt-color-lighter">
                        Programs
                    </h1>
                </div>
            </div>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <li class="active">
                        <a href="<?php get_site_url();  ?>">
                            All Programs  
                        </a>
                    </li>
                <!-- Get All Magazine Terms -->
                <?php 

                    //Get Terms
                    $terms = get_terms( 'program-type', array('hide_empty' => false,)); //Get all the terms

                    foreach ($terms as $term) { //Cycle through terms, one at a time

                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        if ( $parent=='0' ) {

                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                            $term_url = get_term_link($term);
                ?>

                    <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                        <a href="<?php echo $term_url  ?>">
                            <?php echo $term_name; ?>   
                        </a>
                    </li>
                <?php
                        } 
                    }

                ?>
                </ul>
            </nav>
        </header>

		<!-- General Section -->
		<section>
			<div class="row">
            <?php wp_reset_postdata();
                wp_reset_query();
                $temp = $wp_query; $wp_query= null;
                $wp_query = new WP_Query();
                $wp_query->query(array('post_type' => 'bottom-cb'));
                while ($wp_query->have_posts()) : $wp_query->the_post();
                
                $images = rwmb_meta( 'post-feature-image', array( 'limit' => 1 ) );
                $image = reset( $images );
            ?>
           
                <!-- CTA 2 -->
                <?php
                    $images = rwmb_meta( 'event-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                ?>
                <div
                    class="col-md-4 feature-image-block"
                    style="background-image:url('<?php echo $image['full_url']; ?>"   
                >

                    <div class="content txt-color-white">
                        <div class="event-title margin-b-20">
                            <?php
                                //Get Event Date
                                $event_date = rwmb_meta( 'event-date' );
                            
                                //Event Expiry Date Calculation
                                $start = new DateTime();
                                $start->setTimestamp($event_date);
                                $end   = new DateTime(); // Current date time
                                $diff  = $start->diff($end);
                            
                               // echo  $diff->days . ' days, ';
                            
                            ?>
                            <?php if($event_date){ ?>
                                <div class="play-btn">
                                    <!--<i class="hidden">
                                        <div class="expired">
                                            Expired
                                        </div>
                                    </i>-->
                                    <i>
                                        <div class="month uppercase">
                                            <?php echo date( 'M', $event_date ); ?>
                                        </div>
                                        <div class="day">
                                           <?php echo date( 'j', $event_date ); ?> 
                                        </div>
                                    </i>
                                </div>
                            <?php } ?>
                            <h3 class="title txt-xlg txt-medium txt-height-1-2">
                                <?php echo rwmb_get_value( 'event-title' ); ?>
                            </h3>
                        </div>
						<p class="txt-height-1-5 margin-b-30">
							<?php echo rwmb_get_value( 'event-description' ); ?>
						</p>
                        <article class="btn-wrapper">
                            <?php $values = rwmb_meta('event-btns');
                                if($values){
                                    foreach ( $values as $value ) {
                                        if($value['footer-btn-type'] == 1){ ?>

                                            <a
                                                class="btn btn-trans-wb icon"
                                                href="<?php echo $value['footer-btn-url']; ?>"
                                                <?php
                                                    if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                ?>   
                                            >
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>

                                        <?php }elseif($value['footer-btn-type'] == 2){ ?>
                                            <a
                                                class="btn btn-trans-wb"
                                                href="<?php echo $value['footer-btn-url']; ?>"
                                                <?php
                                                    if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                ?>    
                                            >
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>
                                        <?php }else{ ?>
                                            <a
                                                class="btn btn-white"
                                                href="<?php echo $value['footer-btn-url']; ?>"
                                                <?php
                                                    if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                ?>
                                            >
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>
                                        <?php }
                                    }
                                }
                            ?>
                        </article>
					</div>
                </div>
           
            <?php endwhile; ?>
			</div>
		</section>
	</main>
   
    <?php get_footer() ?>
