<?php /*Template Name: Profile - Trainging Offers*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Publication */
        $publication_key = 'publication_status';
    ?>
    
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            
            <div class="dashboard-multi-main-content full">
               
             <?php if($_GET['view'] == 'form'){ //Display form  ?>
                <div class="page-header">
                    <h1 class="page-title">
                        My Courses
                    </h1>
                </div>

                <article class="page-summary">
                    <p>
                        Here you would find all the courses that you have requested for here.
                    </p>
                </article>
                <div class="section-wrapper">
                <?php if( $_REQUEST['gf-id'] ){ //GF View ?>
                   
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                    
                <?php } else { //Configuratin ?>
                
                
                    <?php
                        /* Check for training profile */
                        $gf_id = 1; //Form ID
                        $title = 'Trainer Profile';

                        $entry_count = 0;

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                            )
                        );

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                        /* Get Entries */
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                    ?>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                You do not have a <?php echo $title ?>! You must create one before you can post a course. Start by creating your <?php echo $title ?>.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b"
                                    href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Create Profile
                                </a>
                            </div>
                        </div>                            

                    <?php } else { ?>
                        
                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $_REQUEST['form-title'] ?>
                            </h2>
                            <div class="text-right">
                                <a 
                                    href="<?php echo currentUrl(true); ?>" 
                                    class="edit-btn"
                                >
                                    Cancel
                                </a>
                            </div>
                        </div>
                        <div class="entry">
                            <form action="<?php echo currentUrl(false); ?>?view=form" method="post" class="form">
                                <?php
                                    /* Meta Key */
                                    $postType = 'training-offer';
                                    $tax_types = array(
                                        array(
                                            'name' => 'Training Category',
                                            'slug' => 'training-category',
                                            'hierachical' => true,
                                        ),
                                    );
                                    $redirect_link = currentUrl(true);

                                    if($_GET['post-id']){
                                        $post_id = $_GET['post-id'];
                                        $post = get_post($post_id);
                                        $postName = $post->post_title;

                                        /* Delete & Return */
                                        if($_GET['action'] == 'delete'){
                                            /* Delete Post */
                                            wp_delete_post($post_id);

                                            /* Redirect */
                                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                        }

                                        /* Publish / Unpublish & Return */
                                        if($_GET['action'] == 'publication'){
                                            /* Meta value to save */
                                            $value = "user_published";

                                            /* Get saved meta */
                                            $saved_meta = get_post_meta( $post_id, $publication_key, true );

                                            if ( $saved_meta ) //If published, Unpublish
                                                delete_post_meta( $post_id, $publication_key );
                                            else //If Unpublished, Publish
                                                update_post_meta( $post_id, $publication_key, $value );

                                            /* Redirect */
                                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                        }
                                    }

                                    /*
                                    *
                                    * Save / Retrieve Form Data
                                    *
                                    */
                                    if($_POST){

                                        /* Get Post Name */
                                        $postName = $_POST['post-name'];
                                        $courseLevel = sanitize_text_field( $_POST['course-level'] );

                                        /* Save Post to DB */
                                        $post_id = wp_insert_post(array (
                                            'ID' => $post_id,
                                            'post_type' => $postType,
                                            'post_title' => $postName,
                                            'post_content' => "",
                                            'post_status' => 'publish',
                                        ));

                                        
                                        update_post_meta( $post_id, 'course-level', $courseLevel );

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                ?>
                                
                                <!-- Course Title -->
                                <div class="form-item">
                                    <label for="post-name">
                                        Course Title
                                    </label>
                                    <input 
                                        type="text" 
                                        name="post-name" 
                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                        value="<?php echo $postName ?>"
                                    >
                                </div>
                                
                                <!-- Course Level -->
                                <div class="form-item">
                                    <label for="course-level">
                                        Course Level
                                    </label>
                                    <select name="course-level" required>
                                        <option value="beginner" <?php echo ($courseLevel == 'beginner')? "selected" : "" ?>>
                                            Beginner
                                        </option>
                                        <option value="intermediate" <?php echo ($courseLevel == 'intermediate')? "selected" : "" ?>>
                                            Intermediate
                                        </option>
                                        <option value="advanced" <?php echo ($courseLevel == 'advanced')? "selected" : "" ?>>
                                            Advanced
                                        </option>
                                        <option value="open" <?php echo ($courseLevel == 'open')? "selected" : "" ?>>
                                            Open to All
                                        </option>
                                    </select>
                                </div>

                                <div class="text-right">
                                    <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                </div>
                            </form>
                        </div>
                        
                    <?php } ?>
                
                <?php } ?>  
                </div>
                
            <?php } elseif($_GET['view'] == 'interactions'){ ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        Courses I have engaged
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Here you would find all the courses that you have requested for here.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Courses
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Courses I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="row row-15">
                <?php
                    /* GF Search Criteria */
                    $gf_id = 7;
                    $parent_post_id = 2;
                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                    foreach( $entries as $entry ){

                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $parent_post = get_post($parent_post_id);
                ?>
                    <div class="col-md-4 padding-lr-15 padding-b-30 d-flex">
                        <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                            <h3 class="margin-b-10 txt-color-blue margin-b-10">
                                <?php echo rgar( $entry, '5' ) ?>
                            </h3>
                            <div class="txt-normal-s margin-b-30">
                                <p>
                                    <?php echo rgar( $entry, '6' ) ?>
                                </p>
                            </div>
                            <h3 class="txt-sm txt-color-dark txt-medium">
                                For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                            </h3>
                        </div>
                    </div>
                <?php } ?>
                </div>
                 
            <?php } else { ?>
                 
                <div class="page-header">
                    <h1 class="page-title">
                        My Courses
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form-title=Create a Course" class="cta-btn">
                            Add a Course
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Here you can request for a courses in any subject area here.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Courses
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Courses I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                                
                <div class="row row-5">
                   <style>
                        .publication-status{
                            position: absolute;
                            display: flex;
                            align-items: center;
                            font-weight: 500;
                            top: -0.1em;
                            right: 0.4em;
                            padding: 15px;
                            color: black;
                            background-color: gainsboro;
                        }

                        .user_published{
                           background-color: #f4c026; 
                        }

                        .admin_published{
                           background-color: #00bfe7; 
                        }

                        .home-community-card{
                            overflow: visible;
                        }
                    </style>
                    <?php
                        function truncate($string, $length){
                            if (strlen($string) > $length) {
                                $string = substr($string, 0, $length) . '...';
                            }

                            return $string;
                        }

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'training-offer',
                                'post_status' => 'publish',
                                'author' => $current_user->ID,
                                'posts_per_page' => -1,
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID
                            
                            /* Publication status */      
                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                            /* Get Opportunity Banner */
                            $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                            $image = reset( $images );
                    ?>
                        <div class="col-md-3 padding-lr-5 padding-b-20 d-flex">
                            <div class="home-community-card">
                                <article class="content">
                                    <figure>
                                        <?php $images = get_attached_media( 'image', $post_id ); ?>

                                        <?php if($images){ ?>

                                            <?php  
                                                foreach($images as $image) { //print_r( $image ); 
                                                    $previousImg_id = $image->ID;
                                            ?>
                                                <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">

                                            <?php } ?>

                                        <?php } else { ?>
                                            <figure class="d-flex justify-content-center align-items-center padding-t-15 padding-lr-15 border-o-1 border-color-darkgrey">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png" width="150" class="margin-b-60">
                                            </figure>

                                        <?php } ?>
                                    </figure>
                                    <div class="info bg-white">
                                        
                                        <h3 class=" txt-medium margin-b-10">
                                            <a href="<?php the_permalink() ?>" class="txt-bold">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <p class="txt-sm">
                                            <?php
                                                $meta = get_post_meta($post_id, 'summary', true);
                                                if($meta){
                                                    echo truncate($meta, 70);
                                                }
                                            ?>
                                        </p>
                                        <!--<p class="txt-sm txt-color-dark padding-t-20">
                                            Response(s)
                                        </p>-->
                                        <p class="txt-sm txt-color-dark padding-t-20">
                                            <a 
                                               href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', currentUrl(true), $post_id ) ?>"
                                               class="confirm-delete txt-color-red"
                                            >
                                                <i class="fa fa-trash"></i>
                                                Delete
                                            </a>
                                        </p>
  
                                        <div class="publication-status <?php echo $publication_meta ?>">
                                            <?php
                                                $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                                if($publication_meta == 'user_published'){
                                                    echo 'Undergoing review';
                                                } elseif($publication_meta == 'admin_published') {
                                                    echo 'Published';
                                                }else{
                                                    echo 'Unpublished';
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </article>  
                            </div>   
                        </div>
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                            <h2 class="txt-lg txt-medium">
                                No Courses found.
                            </h2>
                        </div>   

                    <?php } ?>
                </div>
                
            <?php } ?>
                
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>