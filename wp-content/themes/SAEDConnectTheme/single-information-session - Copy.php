    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
  
    <?php
        
        //Get Session ID
        $session_id = $post->ID;
    
        /*Get Theme Color*/
        $theme_color = rwmb_meta( 'info-session-theme-color' );
    
        /*Get Theme Alt Color*/
        $theme_alt_color = rwmb_meta( 'info-session-theme-alt-color' );
    ?>
   
    <?php
        $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

        if($incoming_from_saedconnect){
            
            /* Include saedconnect Navigation */
            get_template_part( 'template-parts/navigation-saedconnect' );
        }
    ?>
    
    <main class="main-content">
        <?php
            $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

            if(!$incoming_from_saedconnect){

                /* Include Skilli Navigation */
                get_template_part( 'template-parts/navigation-skilli-secondary' );
            }
        ?>
       
        <?php

            $images = rwmb_meta( 'info-session-feautured-image', array( 'limit' => 1 ) );
            $image = reset( $images );
        ?>
        <header class="course-directory-banner image lg" style="background-image:url('<?php echo $image['full_url']; ?>');">
            <div class="content container-wrapper text-center txt-color-white">
                <h1 class="title">
                    Start & Grow Your
                    <?php echo rwmb_get_value( 'info-session-name' ); ?>
                    Business
                </h1>
            </div>
        </header>
        <section class="padding-tb-40" style="background-color:<?php echo $theme_color; ?>">
            <div class="container-wrapper txt-color-white">
                <div class="row row-40">
                    <div class="col-md-6 padding-lr-40">
                        <h2 class="txt-xxlg txt-medium txt-height-1-2 padding-b-20">
                            Learn the essentials to begin a profitable
                            <?php echo rwmb_get_value( 'info-session-name' ); ?>
                            Business
                        </h2>
                        <article class="txt-color-white">
                            <p class="txt-normal-s txt-height-1-7">
                                Find all you need to plan your business and get it off the ground. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consectetur, magni deleniti. Accusantium magnam esse, ea maxime. Recusandae, tenetur rerum fugiat ipsum quia mollitia, libero dolore repellat.
                            </p>
                        </article>
                        <p class="margin-t-40">
                            <button type="button" class="btn btn-trans-wb" data-toggle="modal" data-target="#formModal">
                                Join Session
                            </button>
                        </p>
                    </div>
                    <div class="col-md-6 padding-lr-40">
                        <div class="faq-accordion white margin-b-20">
                            <div id="accordion">
                                <?php 
                                    $values = rwmb_meta( 'info-session-essentials' );
                                    $accordionCounter = 1;

                                    foreach ( $values as $value ) { 
                                ?>

                                        <div class="card">
                                            <div class="card-header" id="heading-<?php echo $accordionCounter; ?>">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $accordionCounter; ?>" aria-expanded="false" aria-controls="collapse<?php echo $accordionCounter; ?>">
                                                        <?php echo $value['accordion-title']; ?>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapse-<?php echo $accordionCounter; ?>" class="collapse" aria-labelledby="heading-<?php echo $accordionCounter; ?>" data-parent="#accordion" style="">
                                                <div class="card-body">
                                                    <article class="text-box sm">
                                                        <p class="">
                                                            <?php echo $value['accordion-content']; ?>
                                                        </p>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $accordionCounter++ ?>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="padding-t-80 padding-b-40 bg-grey">
            <div class="container-wrapper">
                <div class="row row-40">
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <header class="margin-b-30">
                            <h2 class="txt-xxlg txt-medium">
                                Ask an Expert
                            </h2>    
                        </header>
                        <article class="margin-b-30">
                            <p class="txt-normal-s txt-height-1-7">
                                Find all you need to plan your business and get it off the ground. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consectetur, magni deleniti. Accusantium magnam esse, ea maxime. Recusandae, tenetur rerum fugiat ipsum quia mollitia, libero dolore repellat.
                            </p>
                        </article>
                        <div class="padding-b-10 margin-b-20 ">
                            <h2 class="txt-lg txt-bold margin-b-20">
                               How to join
                            </h2>
                            <ul class="icon-list txt-normal-s">
                                <li>
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Provide your WhatsApp number.
                                    </span>
                                </li>
                                <li>
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        You will receive an invitation link the join a group
                                    </span>
                                </li>
                                <li>
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Follow the link to become the member of the group.
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <header class="margin-b-30">
                            <h2 class="txt-medium uppercase txt-height-1-4">
                                I am interested in starting a <?php echo rwmb_get_value( 'info-session-name' ); ?> business
                            </h2>    
                        </header>
                        <div class="padding-b-20">
                            <div class="content">
                                <div class="text-content">
                                    <h3 class="txt-bold txt-sm margin-b-5">
                                        Next Session Date
                                    </h3>
                                    <h2 class="txt-3em txt-bold padding-b-20 margin-b-20 border-b-1">
                                       <?php rwmb_the_value( 'info-session-start-date', array( 'format' => 'j' ) ); ?>
                                       <?php rwmb_the_value( 'info-session-start-date', array( 'format' => 'F' ) ); ?>
                                    </h2>
                                    <!--<h3 class="txt-3em txt-bold txt-height-1-2 margin-b-40">
                                        How to make your own Layers mash
                                    </h3>-->
                                    <div class="padding-b-20 margin-b-20 border-b-1">
                                        <p class="txt-normal-s txt-height-1-7">
                                            <span class="txt-bold">Venue: </span>
                                            WhatsApp
                                        </p>
                                    </div>
                                </div>
                                <div class="cta">
                                    <button type="button" class="btn btn-trans-bw" data-toggle="modal" data-target="#formModal">
                                        Join Session
                                    </button>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-4 padding-lr-40 padding-b-40">
                        <header class="margin-b-30">
                            <h2 class="txt-medium uppercase txt-height-1-4">
                                I already run a <?php echo rwmb_get_value( 'info-session-name' ); ?> business, but need some help.
                            </h2>    
                        </header>
                        <div class="padding-b-20">
                            <div class="content">
                                <div class="text-content">
                                    <h3 class="txt-bold txt-sm margin-b-5">
                                        Next Session Date
                                    </h3>
                                    <h2 class="txt-3em txt-bold padding-b-20 margin-b-20 border-b-1">
                                       <?php rwmb_the_value( 'info-session-question-date', array( 'format' => 'j' ) ); ?>
                                       <?php rwmb_the_value( 'info-session-question-date', array( 'format' => 'F' ) ); ?>
                                    </h2>
                                    <div class="padding-b-20 margin-b-20 border-b-1">
                                        <p class="txt-normal-s txt-height-1-7">
                                            <span class="txt-bold">Venue: </span>
                                            WhatsApp
                                        </p>
                                    </div>
                                </div>
                                <div class="cta">
                                    <button type="button" class="btn btn-trans-bw" data-toggle="modal" data-target="#formModal">
                                        Join Session
                                    </button>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </section>
                
        <section class="bg-yellow padding-tb-40" style="background-color:<?php echo $theme_color; ?>">
            <div class="container-wrapper">
                <header class="txt-light margin-b-30">

                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Different Entry Points to a great career
                    </h2>
                    <p class="txt-height-1-">
                        You can start your tech journey either as a Wordpress developer, a frontend web developer,
                        <br>
                        a full stack developer, or a digital marketer. Which ever your choice, there is a track for you.
                    </p>
                </header>
                <?php // Display posts
                    $program_query = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'info_session_to_program',
                            'from' => $post->ID, // You can pass object ID or full object
                        ),
                        'nopaging' => true,
                    ) );
                    while ( $program_query->have_posts() ) : $program_query->the_post();
                
                            $program_id = $post->ID;    //Get Program ID
                            $program_name = $post->post_title;    //Get Program Name
                
                            //  Get Program Featured Image
                            $program_images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ) );
                            $program_images = reset( $program_images );
                
                            //  Get Program Duration
                            $program_duration = rwmb_meta( 'program-duration' );
                
                            //  Get Program Pre-requisites
                            $program_pre_requisites = rwmb_meta( 'program-prerequisites' );
                
                            //  Get Program Locations
                            $program_locations;
                            $prepend_text = ', ';
                
                            $all_states = array(
                                'abia',
                                'adamawa',
                                'akwa-ibom',
                                'anambara',
                            );
                                
                            foreach($all_states as $state){
                                if(rwmb_meta( 'program-location-'.$state )){

                                    $program_locations .= ucfirst($state) . $prepend_text;
                                }
                            }
                
                            if($program_locations){
                                $program_locations = chop($program_locations, $prepend_text);
                            }                
                ?>
                           
               
                <div class="overflow-hidden card-shadow-2 margin-b-40">
                    <div class="row row-40 ">
                        <div class="col-md-7 order-2 order-md-1 padding-lr-40 bg-white">
                            <div class="padding-o-40">
                                <h2 class="txt-normal-s txt-mediym uppercase margin-b-10">
                                    <?php the_title() ?>
                                </h2>
                                <h2 class="txt-xxlg txt-medium txt-height-1-2 padding-b-20">
                                    <?php echo rwmb_meta( 'program-name' ); ?>
                                </h2>
                                <article class="text-box sm">
                                    <?php echo rwmb_meta( 'program-description' ); ?>
                                </article>
                                <div class="row justify-content-between margin-t-20 padding-t-20 border-t-1 border-color-darkgrey">
                                    <div class="col-md-6">
                                        <div class="faq-collapse">
                                            <button class="faq-btn txt-normal-s" type="button" data-toggle="collapse" data-target="#program-<?php echo $program_id ?>" aria-expanded="false" aria-controls="collapseExample">
                                                Learn more
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-6 txt-sm text-right">
                                        <p>
                                            <span class="">Next Start Date: </span>
                                            <span class="txt-bold">
                                            <!-- Get All Child Scheduled Dates --> 
                                            <?php 
                                                $isFirst = true;

                                                $nav_3 = new WP_Query( array(
                                                    'relationship' => array(
                                                        'id'   => 'track_to_scheduled_date',
                                                        'from' => $program_id, // You can pass object ID or full object
                                                    ),
                                                    'nopaging' => true,
                                                    'posts_per_page' => '1'
                                                ) );
                                                while ( $nav_3->have_posts() ) : $nav_3->the_post();
                                            ?> 
                                                <?php
                                                    if($isFirst){

                                                        rwmb_the_value( 'program-schedule-date', array( 'format' => 'd' ) );
                                                        echo ' ';
                                                        rwmb_the_value( 'program-schedule-date', array( 'format' => 'F' ) );

                                                    }

                                                ?>
                                            <?php
                                                $isFirst = false;

                                                endwhile;
                                                wp_reset_postdata();
                                            ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 order-1 order-md-2 padding-lr-40 generic-card-bg" style="background-image:url('<?php echo $program_images['full_url']; ?>');">
                            <div class="cta text-center">
                                <div>
                                    <button
                                        type="button"
                                        class="btn btn-blue"
                                        data-toggle="modal"
                                        data-target="#formModal"
                                        track-id="<?php echo $program_id ?>"
                                        track-name="<?php echo $program_name ?>"
                                    >
                                        Enroll for free online
                                    </button>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-white" data-toggle="modal" data-target="#smsEnrollModal-<?php echo $program_id ?>">
                                        Enroll for free via SMS
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-40 ">
                        <div class="col overflow-hidden">
                            <div class="col-md-12 order-3 padding-lr-40 faq-collapse border-t-1 border-color-darkgrey">
                                <div class="faq-collapse collapse padding-o-40 bg-white" id="program-<?php echo $program_id ?>">
                                    <div class="card card-body no-padding">
                                        <h3 class="txt-xlg txt-medium margin-b-40">
                                            Curriculum
                                        </h3>
                                        <div class="row row=40">
                                            <ul class="col-md-8 padding-r-40 venture-list">
                                            <!-- Get All Child Curriculums --> 
                                            <?php 

                                                $curriculum_query = new WP_Query( array(
                                                    'relationship' => array(
                                                        'id'   => 'track_to_curriculum',
                                                        'from' => $program_id, // You can pass object ID or full object
                                                    ),
                                                    'nopaging' => true,
                                                ) );
                                                while ( $curriculum_query->have_posts() ) : $curriculum_query->the_post();
                                    
                                                $curriculum_id = $post->ID;
                                            ?>   
                                                <li>
                                                    <div class="title txt-normal-s">
                                                        <?php the_title(); ?>
                                                    </div>
                                                    <article class=" content text-box sm margin-b-20">
                                                        <?php echo rwmb_meta( 'curriculum-description' ); ?>
                                                        
                                                        <!-- Get All Child Projects --> 
                                                        <?php 

                                                            $nav_3 = new WP_Query( array(
                                                                'relationship' => array(
                                                                    'id'   => 'curriculum_to_project',
                                                                    'from' => $curriculum_id, // You can pass object ID or full object
                                                                ),
                                                                'nopaging' => true,
                                                            ) );
                                                            while ( $nav_3->have_posts() ) : $nav_3->the_post();
                                                        ?> 
                                                            <button type="button" class="project" data-toggle="modal" data-target="#projectModal">
                                                                <i class="fa fa-file-o"></i>
                                                                <span>
                                                                    <?php the_title() ?>
                                                                </span>
                                                            </button>
                                                        <?php
                                                            endwhile;
                                                            wp_reset_postdata();
                                                        ?>
                                                    </article>
                                                </li>

                                            <?php
                                                endwhile;
                                                wp_reset_postdata();
                                            ?>
                                            </ul>
                                            <div class="col-md-4 padding-o-40 bg-grey txt-normal-s">
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                                    <h3 class="">
                                                        <span class="txt-bold">
                                                            Duration:
                                                        </span>
                                                        <span>
                                                            <?php echo $program_duration ?>
                                                        </span>
                                                    </h3>
                                                </div>
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                                    <h3 class="margin-b-20">
                                                        <span class="txt-bold">
                                                            Next Start Date:
                                                        </span>
                                                        <span>
                                                        <!-- Get All Child Scheduled Dates --> 
                                                        <?php 
                                                            $isFirst = true;
                                                            
                                                            $nav_3 = new WP_Query( array(
                                                                'relationship' => array(
                                                                    'id'   => 'track_to_scheduled_date',
                                                                    'from' => $program_id, // You can pass object ID or full object
                                                                ),
                                                                'nopaging' => true,
                                                                'posts_per_page' => '1'
                                                            ) );
                                                            while ( $nav_3->have_posts() ) : $nav_3->the_post();
                                                        ?> 
                                                            <?php
                                                                if($isFirst){
                                                                    
                                                                    rwmb_the_value( 'program-schedule-date', array( 'format' => 'd' ) );
                                                                    echo ' ';
                                                                    rwmb_the_value( 'program-schedule-date', array( 'format' => 'F' ) );
                                                                    
                                                                }
                                                            
                                                            ?>
                                                        <?php
                                                            $isFirst = false;
                                                            
                                                            endwhile;
                                                            wp_reset_postdata();
                                                        ?>
                                                        </span>
                                                    </h3>
                                                </div>
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                                    <h3 class="margin-b-20">
                                                        <span class="txt-bold">
                                                            Enroll for free
                                                        </span>
                                                    </h3>
                                                    
                                                    <div class="cta">
                                                        <button type="button" class="btn btn-trans-bw txt-normal-s" data-toggle="modal" data-target="#formModal">
                                                            Online
                                                        </button>
                                                        <button type="button" class="btn btn-trans-bw txt-normal-s" data-toggle="modal" data-target="#smsEnrollModal-<?php echo $program_id ?>">
                                                            Via SMS
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">      
                                                    <div class="angle-collapse">
                                                        <button class="angle-btn txt-bold" type="button" data-toggle="collapse" data-target="#detail-1" aria-expanded="false" aria-controls="collapseExample">
                                                            Pre-requisites
                                                        </button>
                                                        <div class="collapse" id="detail-1">
                                                            <div class="card card-body bg-trans">
                                                                <article class="text-box sm">
                                                                    <?php echo $program_pre_requisites; ?>
                                                                </article>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">      
                                                    <div class="angle-collapse">
                                                        <button class="angle-btn txt-bold" type="button" data-toggle="collapse" data-target="#detail-2" aria-expanded="false" aria-controls="collapseExample">
                                                            Locations
                                                        </button>
                                                        <div class="collapse" id="detail-2">
                                                            <div class="card card-body bg-trans">
                                                                <article class="text-box sm">
                                                                    <p class="txt-normal-s txt-height-1-7">
                                                                        <?php echo $program_locations ?>
                                                                    </p>
                                                                </article>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </section>
        
        <section class="padding-t-80">
            <div class="container-wrapper">
                <div class="row row-40">
                    <div class="col-md-5 padding-lr-40 padding-b-40">
                        <h2 class="txt-xlg txt-medium padding-b-20">
                            What am I getting?
                        </h2>
                        <p class="txt-height-1-7">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo dolores, dolore tenetur neque eligendi nisi perferendis quae itaque nemo nulla numquam.
                        </p>
                    </div>
                    <div class="col-md-7 padding-lr-40 row row-40">
                    <?php
                        
                        $terms = get_the_terms( $session_id, 'session-benefit' );
                             
                        if($terms){
                            foreach($terms as $term){
                                $term_id = $term->term_id;
                                $term_name = $term->name;
                                $term_desc = $term->description;

                                $term_image = get_wp_term_image($term_id); //Get Term Image
                    ?>
                            
                            <div class="col-md-4 padding-lr-40 padding-b-80">
                                <div class="margin-b-10">
                                    <img src="<?php echo $term_image; ?>" alt="" width="30">
                                </div>
                                <div class="">
                                    <h4 class="txt-normal-s txt-height-1-4">
                                        <?php echo $term_desc; ?>
                                    </h4>
                                </div>
                            </div>
                            
                    <?php
                            }
                        }
                    ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-wrapper bg-ghostwhite padding-tb-60">
            <header class="margin-b-40">
                <h2 class="txt-xxlg txt-medium margin-b-20">
                    Join a Micro Session
                    <br>
                    to learn more about
                    <?php echo rwmb_get_value( 'info-session-name' ); ?>
                </h2>  
                <article>
                    <p class="txt-normal-s txt-height-1-7">
                        Find all you need to plan your business and get it off the ground.
                    </p>
                </article>  
            </header>
            
            <div class="row row-10">
            
            <?php 
                $values = rwmb_meta( 'info-session-micro-sessions' );

                foreach ( $values as $value ) { 
            ?>
                <div class="col-md-3 info-session-card padding-lr-10 padding-b-20">
                    <div class="content">
                        <div class="text-content">
                            <h3 class="title txt-lg txt-medium txt-height-1-2 margin-b-5">
                                <?php echo $value['micro-session-title']; ?>
                            </h3>
                            <article class="description">
                                <p class="txt-normal-s txt-height-1-7">
                                    <?php echo $value['micro-session-desc']; ?>
                                </p>
                            </article>
                        </div>
                        <div class="cta">
                            <button type="button" class="btn btn-trans-bw" data-toggle="modal" data-target="#formModal">
                                Join Session
                            </button>
                        </div>
                    </div>  
                </div>
                
            <?php } ?>      
            </div>
        </section>
        
        <section class="padding-tb-80 txt-color-white" style="background-color:<?php echo $theme_color; ?>">
            <div class="container-wrapper">
                <div class="row">
                    <header class="col-md-6 margin-b-40">
                        <h2 class="txt-xxlg txt-medium uppercase margin-b-20">
                            Join the <?php echo rwmb_get_value( 'info-session-name' ); ?> community
                        </h2>  
                        <article>
                            <p class="txt-height-1-7">
                                Find all you need to plan your business and get it off the ground. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consectetur, magni deleniti.
                            </p>
                        </article>  
                    </header>
                </div>
            </div>
        </section>
        <section class="padding-b-40" style="margin-top: -4%;">
            <div class="container-wrapper">
                <div class="row row-10">
                <?php
                        
                    $terms = get_the_terms( $session_id, 'community-benefit' );
                                            
                    if($terms){
                        foreach($terms as $term){
                        $term_id = $term->term_id;
                        $term_name = $term->name;
                        $term_desc = $term->description;

                        $term_image = get_wp_term_image($term_id); //Get Term Image
                ?>
                            
                            
                    <div class="col-md-4 padding-lr-10 padding-b-40">
                        <div class="card-shadow">
                            <div class="row row-10">
                                <div class="col-3 padding-lr-10">
                                    <img src="<?php echo $term_image; ?>" alt="">
                                </div>
                                <div class="col-9 padding-lr-10">
                                    <h3 class="txt-bold margin-b-10">
                                        <?php echo $term_name; ?>
                                    </h3>
                                    <p class="txt-normal-s txt-height-1-7">
                                        <?php echo $term_desc; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                <?php
                        }
                    }
                ?>
                </div>
            </div>
        </section>
        
        <section class="padding-t-80 padding-b-40">
            <div class="container-wrapper">
                <div class="row row-40">
                    <div class="col-md-5 padding-lr-40 padding-b-40">
                        <header class="margin-b-30">
                            <h2 class="txt-xxlg txt-medium uppercase">
                                Meet your mentors
                            </h2>    
                        </header>
                        <article>
                            <p class="txt-normal-s txt-height-1-7">
                                Find all you need to plan your business and get it off the ground. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed laborum cupiditate facilis consectetur, magni deleniti. Accusantium magnam esse, ea maxime. Recusandae, tenetur rerum fugiat ipsum quia mollitia, libero dolore repellat.
                            </p>
                        </article>
                    </div>
                    <div class="col-md-7 padding-lr-40 padding-b-40">
                        <div class="row row-40 text-center">
                        <?php 

                            $connected = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'info_session_to_mentor_course',
                                    'from' => $post->ID, // You can pass object ID or full object
                                ),
                                'nopaging'     => true,
                            ) );
                            while ( $connected->have_posts() ) : $connected->the_post();

                                $images = rwmb_meta( 'info-sess-mentor-image', array( 'limit' => 1 ) );
                                $image = reset( $images );
                        ?>
                            <div class="col-md-4 padding-lr-40 padding-b-40">
                                <figure class="margin-b-20">
                                    <img class="profile-image" src="<?php echo $image['full_url']; ?>" alt="" width="150">
                                </figure>
                                <article>
                                    <h4 class="txt-bold margin-b-5">
                                        <?php echo rwmb_get_value( 'info-sess-mentor-name' ); ?>
                                    </h4>
                                    <h5 class="txt-sm">
                                        <?php echo rwmb_get_value( 'info-sess-mentor-desc' ); ?>
                                    </h5>
                                </article>
                            </div>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-wrapper bg-ghostwhite padding-tb-60">
            <header class="margin-b-40">
                <h2 class="txt-xxlg txt-medium">Frequently Asked Questions</h2>    
            </header>
            <div class="row row-10">
            <?php 
                $values = rwmb_meta( 'info-session-faq' );
                $accordionCounter = 1;

                foreach ( $values as $value ) { 
            ?>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-<?php echo $accordionCounter ?>" aria-expanded="false" aria-controls="collapseExample">
                            <?php echo $value['accordion-title']; ?>
                        </button>
                        <div class="collapse" id="faq-<?php echo $accordionCounter ?>">
                            <div class="card card-body">
                                <article class="text-box sm">
                                    <p class="txt-normal-s">
                                        <?php echo $value['accordion-content']; ?>
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                    $accordionCounter++;
                }
            ?> 
            </div>
        </section>
        <section class="padding-tb-60">
            <header class="container-wrapper margin-b-40">
                <h2 class="txt-xxlg txt-medium">Testimonials</h2>    
            </header>
            <div class="generic-gallery relative fluid">
                <div class="swiper-wrapper">
                <?php 

                    $connected = new WP_Query( array(
                        'relationship' => array(
                            'id'   => 'info_session_to_testimonial_course',
                            'from' => $post->ID, // You can pass object ID or full object
                        ),
                        'nopaging'     => true,
                    ) );
                    while ( $connected->have_posts() ) : $connected->the_post();
                ?>
                   
                    <div class="swiper-slide container-wrapper">
                        <div class="testimonial-slider-item">
                            <div class="content">
                                <h3 class="quote">
                                    <?php echo rwmb_get_value( 'info-sess-testimonial-quote' ); ?>
                                </h3>
                                <div class="author">
                                    <div class="name"><?php echo rwmb_get_value( 'info-sess-testimonial-name' ); ?></div>
                                    <div class="enrolled">
                                        <?php echo rwmb_get_value( 'info-sess-testimonial-desc' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>   
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </section>
        <section class="padding-t-40 bg-grey">
            <div class="container-wrapper">
                <header class="margin-b-40">
                    <h2 class="txt-xxlg txt-medium">
                        Business Insights
                    </h2>    
                </header>
                <div class="row row-40">
                    <div class="col-md-6 padding-lr-40 padding-b-40">
                        <div class="row row-20">
                            <div class="col-5 padding-lr-20">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/grid/image%20(35).png" alt="">
                            </div>
                            <div class="col-7 padding-lr-20">
                                <h3 class="txt-xxlg txt-bold margin-b-10">
                                    Not sure if this is the business idea to pursue?
                                </h3>
                                <p class="txt-normal-s txt-height-1-7 margin-b-20">
                                    Find all you need to plan your business and get it off. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora dicta.
                                </p>
                                <div class="cta">
                                    <button type="button" class="btn btn-trans-bw" data-toggle="modal" data-target="#formModal">
                                        Get Started
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 padding-lr-40 padding-b-40">
                        <div class="row row-20">
                            <div class="col-5 padding-lr-20">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/grid/image%20(40).jpg" alt="">
                            </div>
                            <div class="col-7 padding-lr-20">
                                <h3 class="txt-xxlg txt-bold margin-b-10">
                                    Discover your personality
                                </h3>
                                <p class="txt-normal-s txt-height-1-7 margin-b-20">
                                    Find all you need to plan your business and get it off. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora dicta quidem maiores aliquam hic.
                                </p>
                                <div class="cta">
                                    <button type="button" class="btn btn-trans-bw" data-toggle="modal" data-target="#formModal">
                                        Get Started
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

            if(!$incoming_from_saedconnect){

                /* Include Skilli Footer */
                get_template_part( 'template-parts/footer-skilli' );
            }
        ?>
    </main>
    
    <?php endwhile; // end of the loop. ?>
    
    <?php
        $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

        if($incoming_from_saedconnect){

            /* Include Saedconnect Footer */
            get_template_part( 'template-parts/footer-saedconnect' );
        }
    ?>
    
    <?php get_footer() ?>
    
    <?php
    $incoming_from_saedconnect = esc_html($_REQUEST['saedconnect']);

    if($incoming_from_saedconnect){
?>

   <script>
        $(document).ready(function(){
            /*
            *   Auto populate form with Track ID & Name
            */
            $('#formModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var track_id = button.attr('track-id'); // Extract Track ID
                var track_name = button.attr('track-name'); // Extract Track Name

                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);

                //modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('#input_1_6').val(track_id);
                modal.find('#input_1_7').val(track_name);
            })


            //Confirmation Modal
            $('#formConfirmationModal').modal('show')

            //Clear URL
           // var uri = window.location.toString();
            //if (uri.indexOf("?") > 0) {
                //var clean_uri = uri.substring(0, uri.indexOf("?"));
                //window.history.replaceState({}, document.title, clean_uri);
            //}

            //Append Saedconnect Query string
            $('a').each(function(index, element) {
              var newAttr = $(element).attr('href') + '?saedconnect=true';
              $(element).attr('href', newAttr);
            });
        });
    </script>

<?php } ?>