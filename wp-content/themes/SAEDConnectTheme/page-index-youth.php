    <?php /*Template Name: Homepage-Youth*/ ?>
    
    <?php get_header() ?>
   
    <main class="main-content">
        <header class="overview-header container-wrapper">
            
            <div class="row padding-tb-20">
                <div class="col-md-8">
                    <h1 class="txt-3em txt-bold txt-height-1-2 padding-b-20">Support Youth Development</h1>   
                </div>
                <div class="col-md-4">
                    <ul class="homepage-list">
						<li>
							<a href="<?php echo get_page_link(577); ?>">
                                Offer Trainings & Mentorship
                            </a>
						</li>
						<li class="active">
							<a href="<?php echo get_page_link(575); ?>">
                                Support Youth Development
                            </a>
						</li>
						<li>
							<a href="#">
                                Support a Project
                            </a>
						</li>
						<li>
							<a href="#">
                                Donate
                            </a>
						</li>
					</ul>
                </div>
            </div>
        </header>
        
        <!-- General Section -->
        <section>
            <div class="row">
                <div class="col-md-4">
                    
                    <div class="youth-dev-card">
                        <div class="content">
                            <header class="header bg-yellow-dark txt-color-white">
                                <div class="title">Corporates & Donor Agencies</div>
                                <p class="txt-normal-s txt-he-ght-1-5 margin-b-30">
                                    SAEDConnect gives you unrivalled access to the youth audience, nationwide. With over 1,000,000 youths spread across the country in our database, we are the right partners for your youth focused programs and impact projects.
                                </p>
                                <a class="btn btn-trans-wb" href="http://www.saedconnect.org/contribute/synopsis/corporate/">
									Lets work together  
								</a>
                            </header>
                        </div>
                    </div>    
                </div>
                <!--<div class="col-md-4">
                    <div class="youth-dev-card">
                        <div class="content">
                            <header class="header bg-yellow">
                                <div class="title">Government & Donor Agencies</div>
                                <p class="txt-normal-s txt-he-ght-1-5 margin-b-30">
                                    We are the right partners for your youth focused impact programs. Leverage our resources, network and experience to execute your youth focused impact programs and attain your youth development objectives across Nigeria. 
                                </p>
                                <a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
									How we help  
								</a>
                            </header>
                        </div>
                    </div>    
                </div>-->
                <div class="col-md-4">
                    <div class="youth-dev-card">
                        <div class="content">
                            <header class="header bg-yellow">
                                <div class="title">
                                    Individuals
                                </div>
                                <p class="txt-normal-s txt-he-ght-1-5 margin-b-30">
                                    We are the right partners for your youth focused impact programs. Leverage our resources, network and experience to execute your youth focused impact programs and attain your youth development objectives across Nigeria. 
                                </p>
                                <a class="btn btn-trans-wb" href="http://www.saedconnect.org/contribute/synopsis/individuals/">
									How we help  
								</a>
                            </header>
                        </div>
                    </div>    
                </div>
                <div class="col-md-4">
                    <div class="youth-dev-card">
                        <div class="content">
                            <header class="header bg-grey">
                                <div class="title">
                                    Schools, Youth Institutions & Religious Organizations
                                </div>
                                <p class="txt-normal-s txt-he-ght-1-5 margin-b-30">
                                    Leverage our resources, frameworks and network to develop & execute high-impact youth development initiatives for your students and youth audience.
                                </p>
                                <a class="btn btn-trans-bw" href="http://www.saedconnect.org/contribute/synopsis/schools-youth-development/">
									How we can help  
								</a>
                            </header>
                        </div>
                    </div>    
                </div>
            </div>
            
            <div class="row">
                <article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (30).png');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Lend your Expertise 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Your professional experience and expertise (or that of your team) are an awesome resource for good. Share your knowledge to strengthen programming, expand learning opportunities, and mentor youths.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn more
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (1).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Share your Story
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Share your wisdom and experience of success & failures to inspire and motivate others on their journey.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn More 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (28).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Donate 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							We welcome donations in cash, skill development tools & equipment, skill centers, airtime, media slots, etc that can boost program success.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Ways to Give
							</a>
						</div>
					</div>
				</article>

                <article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (6).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Support a Project
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							We execute change initiatives through streamlined projects and programs. You can lend your expertise, donate, volunteer, etc to any of our ongoing projects.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								View Projects 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (12).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Support with Sensitization
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							You can be our ambassador and help us reach out to more youths who can benefit from our programs and more stakeholders willing to support our mandate.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								See How 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (13).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Volunteer 
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Your time is valuable. Why not donate your energy and skills to help a great cause. From event support to community volunteering opportunities, helping hands are always appreciated.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn More
							</a>
						</div>
					</div>
				</article>

                
                <article class="col-md-8 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (16).jpg');">
                    <div class="content txt-color-white">
                        <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                        	Execute or co-Create a Youth Development Program 
                        </h3>
                        <p class="txt-height-1-5 margin-b-30">
                            We can work with you to develop and/or execute high-impact, win-win youth development initiatives that align with your organizational objectives. Programs could range from workshops & events to funding & business incubation.
                        </p>
                        <a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
                            Learn More 
                        </a>
                    </div>   
                </article>
                <div class="col-md-4 cta-block bg-blue txt-color-white ">
                    <article>
                        <div class="txt-3em txt-color-white padding-tb-20 border-t-4 border-color-white">
                            <i class="fa fa-quote-left"></i>
                        </div>
                        <h2 class="title">
                            Let's work together to ensure that every youth has the support they need to realize their potential. 
                        </h2>
                        <div class="txt-medium txt-height-1-2">
                            Anne Burdick
                            <br>
                            Chair, Media Design Practices
                        </div>
                    </article>
                </div>
                <div class="col-md-4 cta-block">
                    <article class="txt-color-dark">
                        <h4 class="margin-b-20">Corporate Sponsors</h4>
                        <h2 class="title txt-height-1-2">
                            We have built partnerships with companies across industries. Here are some of the partners that make our work possible.
                        </h2>
                    </article>
                </div>
                <div class="col-md-8 padding-o-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/saedconnect_partners.png" alt="">
                </div>
                <div class="col-md-12 pre-footer" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (22).jpg');">
                    <div class="content">
                        <h4 class="title">
                            Empower People, Transform Lives
                        </h4>
                        <p class="txt-height-1-4 txt-color-white margin-b-40">
                            The world needs more problem solvers. Your support will create possibilities and amplify impact.
                        </p>
                        <article class="btn-wrapper">
                            <a class="btn btn-white" data-toggle="modal" href="#comingSoonModal">
                                Ways to give
                            </a>
                            <a class="btn btn-trans-wb icon" data-toggle="modal" href="#comingSoonModal">
                                Donate Now
                            </a>
                        </article>
                    </div>
                </div>
                <div class="col-md-12 cta-block bg-grey">
                    <h4 class="txt-xlg margin-b-30">Contact Details</h4>
                    <div class="row row-20">
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
    </main>
    
    <?php get_footer() ?>