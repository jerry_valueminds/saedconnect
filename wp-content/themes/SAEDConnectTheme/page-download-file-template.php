<?php /*Template Name: Download Page*/ ?>
<?php get_header() ?>
   
    <main class="main-content">
        <section class="container-wrapper bg-white text-center padding-tb-80">
            <h1 class="txt-xxlg txt-bold margin-b-40">
                Downloading <?php echo get_the_title().'...' ?>
            </h1>
            <div class="row">
                <div class="col-md-4 mx-auto">
                    <?php
                        $files = rwmb_meta( 'download-file', array( 'limit' => 1 ) );
                        $file = reset( $files );
                    ?>
                    <p class="txt-normal-s">
                        You download will automatically begin in a few seconds. If the download does not begin,
                        <a 
                            href="<?php echo $file['url']; ?>" 
                            class="txt-color-blue"
                            id="download-link"
                            target="_blank"
                            download
                            data-auto-download
                        >
                            click here
                        </a> 
                    </p>
                </div>
            </div>
            <div class="margin-t-40">
                <a href="#" class="btn btn-trans-blue" onclick="goBack()">
                    <i class="fa fa-arrow-left"></i>
                    <span class="padding-l-10">
                        Go Back
                    </span>
                </a>
                <a href="https://www.saedconnect.org/" class="btn btn-blue">
                    <i class="fa fa-home"></i>
                    <span class="padding-l-10">
                        Go to Homepage
                    </span>
                </a>
            </div>
        </section>
    </main>
    
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    
<?php get_footer() ?>


<script>
    $(function() {
        setTimeout(function(){

            $("#download-link")[0].click();

        },1);
    });
</script>