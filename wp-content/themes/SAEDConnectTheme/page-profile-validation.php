<?php /*Template Name: Profile - Validation*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Post Type */
        $postType = 'validation-program';

        /* Publication */
        $publication_key   = 'publication_status';

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                <?php if( $_REQUEST['gf-id'] ){ //GF View ?>
                   
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                    
                <?php } else { //Offer Configuration ?>
                
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post">
                            <?php
                                /* Meta Key */
                                $meta_key = 'assigned_users';
                                $validation_meta_key = 'validation_types';
                                $redirect_link = currentUrl(true);
                              
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    $post = get_post($post_id);
                                    $postName = $post->post_title;
                                    
                                    /* Delete & Return */
                                    if($_GET['action'] == 'delete'){
                                        /* Delete Post */
                                        wp_delete_post($post_id);

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                }
                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if( $_POST ){
                                    
                                    /* Get Post Name */
                                    $postName = $_POST['post-name'];
                                    
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));
                                    
                                    
                                    /* Delete existing Validation meta */
                                    $saved_validation_types = get_post_meta($post_id, $validation_meta_key);

                                    foreach($saved_validation_types as $saved_validation_type){
                                        delete_post_meta($post_id, $validation_meta_key, $saved_validation_type);
                                    }

                                    /* Save Submitted Validation Types */
                                    $submitted_validation_types = $_POST['validation_types'];

                                    foreach($submitted_validation_types as $submitted_validation_type){
                                        add_post_meta( $post_id, $validation_meta_key, $submitted_validation_type );
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    /* Delete existing meta */
                                    $saved_users = get_post_meta($post_id, $meta_key);

                                    foreach($saved_users as $saved_user){
                                        delete_post_meta($post_id, $meta_key, $saved_user);
                                    }

                                    /* Save Submitted User Selection */
                                    $submitted_users = $_POST['users'];

                                    foreach($submitted_users as $submitted_user){
                                        add_post_meta( $post_id, $meta_key, $submitted_user );
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }
                              
                                /* Get Saved Users */
                                $saved_users = get_post_meta($post_id, $meta_key);
                              
                                /* Get Saved Validation */
                                $saved_validation_types = get_post_meta($post_id, $validation_meta_key);

                                /* Get All Users */
                                $blogusers = get_users( 'blog_id=1&orderby=nicename&role=subscriber' );
                            ?>
                            
                            <div class="margin-b-40">
                                <label for="post-name" class="d-block txt-normal-s txt-color-dark txt-medium margin-b-10">
                                    Program Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post-name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $postName ?>"
                                >
                            </div>
                            
                            <div class="margin-b-20">
                                <div class="txt-normal-s txt-medium txt-color-dark margin-b-15">
                                    Validation Type
                                </div>
                                <?php
                                    $validation_types = array(
                                        array(
                                            'name' => 'Business',
                                            'value' => 'business'
                                        ),
                                        array(
                                            'name' => 'Job Offers',
                                            'value' => 'jobs'
                                        ),
                                        array(
                                            'name' => 'Programs',
                                            'value' => 'programs'
                                        ),
                                        array(
                                            'name' => 'Projects',
                                            'value' => 'projects'
                                        ),
                                        array(
                                            'name' => 'Service Offers',
                                            'value' => 'service-offers'
                                        ),
                                        array(
                                            'name' => 'Trainer Profiles',
                                            'value' => 'trainer-information'
                                        ),
                                        array(
                                            'name' => 'Training Offers',
                                            'value' => 'training-offers'
                                        ),
                                    );
                                ?>
                                <?php foreach( $validation_types as $validation_type ){ ?>
                                
                                <label class="txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                    <input
                                        class="margin-r-5"
                                        type="checkbox" 
                                        value="<?php echo $validation_type['value']?>" 
                                        name="validation_types[]" 
                                        <?php echo in_array($validation_type['value'], $saved_validation_types) ? "checked" : "" ?>
                                    >
                                    
                                    <?php echo $validation_type['name']; ?>
                                    
                                </label>
                                
                                <?php } ?>
                            </div>
                            
                            <p class="txt-normal-s txt-medium margin-b-10">
                                Assign users to Program
                            </p>
                            <select class="select-collaborators full-width" name="users[]" multiple="multiple">
                                <option 
                                    value="1"
                                    <?php echo ( in_array(1, $saved_users) ) ? "selected" : ""; ?>
                                >
                                    Super Admin
                                </option>
                            <?php foreach( $blogusers as $user ){ ?>

                                <option 
                                   value="<?php echo esc_html( $user->ID ); ?>"
                                   <?php echo ( in_array($user->ID, $saved_users) ) ? "selected" : ""; ?>
                                >
                                    <?php echo esc_html( $user->user_login ); ?>
                                </option>

                            <?php } ?>
                            </select>
                            <div class="text-right padding-t-20">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>
                
                <?php } ?>  
                </div>
               
            <?php } else { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        Validation Programs
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form-title=Create a Validation Program" class="cta-btn">
                            Add Validation Program
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Create, manage and assign users to validation programs.
                    </p>
                </article>
                
               <!-- <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Service Offers
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Service Offers I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>-->
                <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                    <div class="col-4 padding-lr-15">
                        NAME
                    </div>
                    <div class="col-3 padding-lr-15">
                        ASSIGNED USERS
                    </div>
                    <div class="col-3 padding-lr-15">
                        VALIDATION TYPES
                   </div>                  
                </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */
                    // Create Query Argument
                    $args = array(
                        'post_type' => 'validation-program',
                        'showposts' => -1,
                        'author' => $current_user->ID,
                        'meta_query' => $meta_array,
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                          
                ?>
                    
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-4 padding-lr-15">
                            <a href="<?php the_permalink() ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                            <div class="d-flex align-items-center">
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php
                                        /* Get Saved Users */
                                        $saved_users = get_post_meta($post_id, $meta_key);
                                        $saved_users = $saved_users['assigned_users'];
                          
                                        foreach( $saved_users as $key => $saved_user ){
                                            
                                            if( $saved_user == 1 ){
                                                
                                                echo 'SAEDConnect Admin'; 
                                                
                                            } else {
                                                
                                                $user_info = get_userdata($saved_user);
                                                echo $user_info->user_nicename;
                                                
                                            }
                                            
                                            if( $key + 1 < count($saved_users) ){
                                                echo ", ";
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 padding-lr-15">
                            <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                <?php 
                                    /* Get Saved Users */
                                    $saved_validation_types = get_post_meta($post_id, $validation_meta_key);
                                                    
                          
                                    $saved_validation_types = $saved_validation_types['validation_types'];

                                    foreach( $saved_validation_types as $key => $saved_validation_type ){

                                        echo $saved_validation_type; 

                                        if( $key + 1 < count($saved_validation_types) ){
                                            echo ", ";
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-2 padding-lr-15 text-right">
                            <a 
                               href="<?php printf('%s/?view=form&post-id=%s&form-title=Edit %s', currentUrl(true), $post_id, get_the_title() ) ?>"
                               class="txt-medium txt-color-green"
                            >
                                Edit
                            </a>
                            |
                            <a 
                               href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', currentUrl(true), $post_id ) ?>"
                               class="confirm-delete txt-color-red"
                            >
                                Delete
                            </a>
                        </div>
                    </div>
                    
                <?php
                    endwhile;
                ?>
                
            <?php } ?> 
                
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>