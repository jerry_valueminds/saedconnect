<?php get_header() ?>

<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.main-content').fadeIn();
        $('.select-collaborators').select2({
            dropdownParent: $("#instuctorsModal")
        });
    });
</script>

<style>
    #wpcomm {
        max-width: 100vw;
        margin: 0;
    }
    
    #wc-comment-header, .wc_stick_btn, .wc_close_btn {
        display: none !important;
    }
    #wpcomm .wpdiscuz-form-bottom-bar {
        display: none;
    }
</style>

<?php
    /* Get Current User */
    $current_user = wp_get_current_user();
    
    /* Manager view array */
    $sdg_icons_array = array(
        'No Poverty' => '1.png',
        'Zero Hunger' => '2.jpg',
        'Good Health and Well-being' => '3.png',
        'Quality Education' => '4.png',
        'Gender Equality' => '5.png',
        'Clean Water and Sanitation' => '6.png',
        'Affordable and Clean Energy' => '7.png',
        'Decent Work and Economic Growth' => '8.png',
        'Industry, Innovation and Infrastructure' => '9.jpg',
        'Reduced Inequality' => '10.png',
        'Sustainable Cities and Communities' => '.png.jpg',
        'Responsible Consumption and Production' => '12.png',
        'Climate Action' => '13.jpg',
        'Life Below Water' => '14.jpg',
        'Life on Land' => '15.png',
        'Peace and Justice Strong Institutions' => '16.jpg',
        'Partnerships to achieve the Goal' => '17.png',
    );

    /* Get Queried Post Object */
    while ( have_posts() ) : the_post();

        /* Get Post Data */
        $post_id = get_the_ID(); //ID
        $post_type = get_post_type(); //Post Type
        $post_title = get_the_title(); //Title
        $post_link = get_permalink(); //Post Link
        $post_author_id = get_the_author_meta('ID'); //Author ID

        $images = rwmb_meta( 'featured_image', array( 'limit' => 1 ) );
        $image = reset( $images );

        $thematic_area_id = get_post_meta($post_id, 'thematic_area', true);

    endwhile; //resetting the page loop

    $challenges_query = new WP_Query( array(
        'relationship' => array(
            'id'   => 'thematic_area_to_problem_area',
            'from' => $post_id, // You can pass object ID or full object
        ),
        'posts_per_page' => -1,
    ) );

    $challenges_query = new WP_Query( array(
        'relationship' => array(
            'id'   => 'thematic_area_to_problem_area',
            'from' => $post_id, // You can pass object ID or full object
        ),
        'posts_per_page' => -1,
    ) );

    $idea_query = new WP_Query( array(
        'post_type' => 'idea',
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'meta_query' => array(
            array(
                'key' => 'thematic_area',
                'value' => $thematic_area_id,
            ),
        ),
    ) );

    $tab = $_GET['tab'];
  
?>

    <main class="main-content">
        <header class="position-relative">
            <section class="padding-t-60 padding-b-40">
                <div class="container-wrapper">
                    <div class="row row-10">
                        <div class="col-2 col-lg-1 padding-lr-10">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/light_bulb.png" alt="">
                        </div>
                        <div class="col-10 col-lg-10 padding-lr-10">
                            <div class="row row-40">
                                <div class="col-lg-6 padding-lr-40 padding-b-20">
                                    <h1 class="txt-2em txt-height-1-2 txt-light padding-b-40">
                                        <?php echo $post_title; ?>
                                    </h1>
                                    <h2 class="txt-xlg">
                                    <?php 
                                        $term_list = wp_get_post_terms( $post->ID, 'thematic-category', array( 'fields' => 'names' ) );
                                        foreach ( $term_list as $term_item ){
                                            echo $term_item;
                                        } 
                                    ?>
                                    </h2>
                                    <div class="d-flex align-items-center">
                                        <a href="<?php echo get_the_permalink($thematic_area_id) ?>" class="bg-green txt-color-white txt-normal-s padding-tb-10 padding-lr-20">
                                            <span class="txt-bold">Theme:</span> 
                                            <?php echo get_the_title($thematic_area_id); ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-6 padding-lr-40 padding-b-20 d-flex align-items-end justify-content-end">
                                    <div class="text-right">
                                        <a href="" class="d-inline-flex padding-r-10 align-items-center txt-color-green txt-sm">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/up_vote.png" alt="" height="14">
                                            <span class="txt-bold padding-l-5">776</span>
                                        </a>
                                        <a href="" class="d-inline-flex padding-r-10 align-items-center txt-color-lighter txt-sm">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/down_vote.png" alt="" height="14">
                                            <span class="txt-bold padding-l-5">776</span>
                                        </a>
                                        <a href="" class="d-inline-flex padding-r-10 align-items-center txt-color-green txt-sm">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/voices.png" alt="" height="14">
                                            <span class="txt-bold padding-l-5">776</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </header>
        
        <section class="bg-ash padding-t-20">
            <div class="container-wrapper">
                <div class="row">
                    <nav class="col-lg-10 mx-auto position-static">
                        <ul class="nav nav-pills ynl-thematic-tabs" id="pills-tab" role="tablist">
                            <li class="nav-item padding-b-20">
                                <a 
                                    class="nav-link <?php echo ( $tab ) ? '' : 'active' ; ?>" 
                                    id="pills-1-tab" data-toggle="pill" href="#pills-1" 
                                    role="tab" 
                                    aria-controls="pills-1" 
                                    aria-selected="<?php echo ( $tab ) ? 'false' : 'true' ; ?>"
                                >
                                    About
                                </a>
                            </li>
                            <li class="nav-item padding-b-20">
                                <a 
                                    class="nav-link <?php echo ( $tab == 2 ) ? 'active' : '' ; ?>" 
                                    id="pills-2-tab" 
                                    data-toggle="pill" 
                                    href="#pills-2" 
                                    role="tab" 
                                    aria-controls="pills-2" 
                                    aria-selected="<?php echo ( $tab == 2 ) ? 'true' : 'false' ; ?>"
                                >
                                    Add your voice
                                </a>
                            </li>
                            <li class="nav-item padding-b-20">
                                <a 
                                    class="nav-link <?php echo ( $tab == 3 ) ? 'active' : '' ; ?>" 
                                    id="pills-3-tab" 
                                    data-toggle="pill" 
                                    href="#pills-3" 
                                    role="tab" 
                                    aria-controls="pills-3" 
                                    aria-selected="<?php echo ( $tab == 3 ) ? 'true' : 'false' ; ?>"
                                >
                                    Champion this Idea
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </section>
            
        <div class="tab-content bg-ghostwhite" id="pills-tabContent">  
            <div class="tab-pane fade <?php echo ( $tab ) ? '' : 'show active' ; ?>" id="pills-1" role="tabpanel" aria-labelledby="pills-1-tab"> 
                <section class="padding-tb-80">
                    <div class="container-wrapper">
                        <div class="row">
                            <div class="col-lg-10 mx-auto">
                                <div class="margin-b-60">
                                    <h2 class="txt-lg txt-medium padding-b-30">
                                        The idea proposition
                                    </h2>
                                    <article class="txt-height-1-7 txt-normal-s">
                                        <?php echo get_post_meta($post_id, 'description', true); ?>
                                    </article>
                                </div>
                                <div class="margin-b-60">
                                    <h2 class="txt-lg txt-medium padding-b-30">
                                        Specific challenges this idea would address
                                    </h2>
                                    <article class="txt-height-1-7 txt-normal-s padding-b-40">
                                        <?php echo get_post_meta($post_id, 'challenge_description', true); ?>
                                    </article>
                                    <div class="row row-30">
                                    <?php 
                                        $challenge_areas = get_post_meta($post_id, 'challenge_areas', true);

                                        foreach( $challenge_areas as $challenge_id ){
                                            $images = 0;
                                            $images = rwmb_meta( 'featured_image', array( 'limit' => 1 ), $challenge_id );
                                            $image = reset( $images );
                                    ?>   
                                   
                                    <div class="col-md-6 col-lg-3 col-xl-2 padding-lr-30 padding-b-30">
                                        <a class="d-block" data-toggle="modal" href="#challengeModal-<?php echo $challenge_id; ?>">
                                            <div class="padding-b-5">
                                                <img src="<?php echo $image['full_url']; ?>" class="rounded-corners" alt="">
                                            </div>
                                            <div class="txt-normal-s txt-height-1-5"><?php echo get_the_title($challenge_id) ?></div>
                                        </a> 
                                        <div class="modal fade font-main filter-modal" id="challengeModal-<?php echo $challenge_id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header padding-lr-40">
                                                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body padding-o-40">
                                                        <div class="row row-20">
                                                            <div class="col-lg-3 padding-lr-20">
                                                                <img src="<?php echo $image['full_url']; ?>" alt="">
                                                                <div class="padding-t-40">
                                                                    <h4 class="txt-sm txt-medium txt-color-green margin-b-5">Thematic Area</h4>
                                                                    <p class="txt-xs txt-color-lighter"><?php echo get_the_title($thematic_area_id); ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-9 padding-lr-20">
                                                                <div class="padding-b-20">
                                                                    <span class="btn btn-green txt-xxs no-m-b">
                                                                        Challenge focus area
                                                                    </span>
                                                                </div>
                                                                <h3 class="txt-bold padding-b-20"><?php echo get_the_title($challenge_id) ?></h3>
                                                                <article class="text-box txt-normal-s txt-color-light">
                                                                     <p><?php echo rwmb_meta( 'summary', array(), $challenge_id ); ?></p>
                                                                </article>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div> 

                                    <?php
                                        }
                                    ?>  
                                    </div>
                                </div>
                                <div class="padding-b-30 margin-b-30 border-b-1 border-color-lighter">
                                    <h2 class="txt-lg txt-medium padding-b-30">
                                        Potential impact of this idea
                                    </h2>
                                    <article class="txt-height-1-7 txt-normal-s">
                                        <?php echo get_post_meta($post_id, 'solution_description', true); ?>
                                    </article>
                                </div>
                                <div>
                                    <div class="inline-block text-center margin-r-20">
                                        <a href="" class="d-inline-flex align-items-center btn btn-trans-green txt-sm d-flex">
                                            <span class="padding-r-20">Like</span>
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/thumbs_up.png" alt="" width="15">
                                        </a>
                                        <div class="txt-sm" style="opacity:0">
                                            <a data-toggle="modal" href="#addVoiceModal">
                                                <i class="fa  fa-info-circle"></i>
                                                What does this mean?
                                            </a>
                                        </div>
                                    </div>
                                    <div class="inline-block text-center margin-r-20">
                                        <a href="" class="d-inline-flex align-items-center btn btn-trans-green txt-sm d-flex">
                                            <span class="padding-r-20">Dislike</span>
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/thumbs_down.png" alt="" width="15">
                                        </a>
                                        <div class="txt-sm" style="opacity:0">
                                            <a data-toggle="modal" href="#addVoiceModal">
                                                <i class="fa  fa-info-circle"></i>
                                                What does this mean?
                                            </a>
                                        </div>
                                    </div>
                                    <div class="inline-block text-center margin-r-20">
                                        <a href="" class="d-inline-flex align-items-center btn btn-green txt-sm d-flex">
                                            Add your voice
                                        </a>
                                        <div class="txt-sm">
                                            <a data-toggle="modal" href="#addVoiceModal">
                                                <i class="fa  fa-info-circle"></i>
                                                What does this mean?
                                            </a>
                                        </div>
                                    </div>
                                    <div class="inline-block text-center margin-r-20">
                                        <a href="" class="d-inline-flex align-items-center btn btn-green txt-sm d-flex">
                                            Champion this idea
                                        </a>
                                        <div class="txt-sm">
                                            <a data-toggle="modal" href="#addVoiceModal">
                                                <i class="fa  fa-info-circle"></i>
                                                What does this mean?
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="tab-pane fade <?php echo ( $tab == 2 ) ? 'show active' : '' ; ?>" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
                <section class="container-wrapper padding-tb-80">
                    <div class="row">
                        <div class="col-lg-10 mx-auto overflow-hidden">
                            <h4 class="txt-xlg txt-light txt-height-1-4 padding-b-40">
                                Have a suggestion? Have a modified version of this idea? Or do you have experience doing something like this that you would love to share? Your views are important in helping us determine whether or not we should convert this idea into a project. Follow the link to add your voice.
                            </h4>
                            <?php comments_template() ?>
                        </div>
                    </div>
                </section>
            </div>
            <div class="tab-pane fade <?php echo ( $tab == 3 ) ? 'show active' : '' ; ?>" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">
                <div class="container-wrapper padding-tb-80">
                    <div class="row">
                        <div class="col-lg-10 mx-auto">
                            <div class="row">
                                <div class="col-lg-8">
                                    <form action="<?php echo currentUrl(true).'?view=form-champion-idea'; ?>" method="post">
                                        <div class="">
                                            <?php
                                                if( $_POST && $_GET['view'] == 'form-champion-idea' ){

                                                    /* Get Post Name */
                                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                                    /* Meta */
                                                    $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                                    $email = sanitize_text_field( $_POST['email'] ); 
                                                    $location = sanitize_text_field( $_POST['location'] ); 
                                                    $implementation = sanitize_text_field( $_POST['implementation'] ); 
                                                    $educational_status = sanitize_text_field( $_POST['educational_status'] ); 
                                                    $contribution = wp_kses_post( $_POST['contribution'] );

                                                    /* Save Post to DB */
                                                    $champion_id = wp_insert_post(array (
                                                        'post_type' => 'idea-champion',
                                                        'post_title' => $post_name,
                                                        'post_content' => "",
                                                        'post_status' => 'publish',
                                                    ));

                                                    update_post_meta( $champion_id, 'thematic_area', $thematic_area_id );
                                                    update_post_meta( $champion_id, 'problem_area', $post_id );

                                                    update_post_meta( $champion_id, 'telephone', $telephone );
                                                    update_post_meta( $champion_id, 'email', $email );
                                                    update_post_meta( $champion_id, 'location', $location );
                                                    update_post_meta( $champion_id, 'implementation', $implementation );
                                                    update_post_meta( $champion_id, 'educational_status', $educational_status );
                                                    update_post_meta( $champion_id, 'contribution', $contribution );
                                                    
                                                    /* Update Mailing List */
                                                    /* Data */
                                                    switch_to_blog(110);
                                                    $state = get_term( $location );
                                                    restore_current_blog();
                                                    
                                                    $api_key = '0fkxtL9crlJFX7cqCXUV';
                                                    $master_list = 'YdUPrXEfWxjULk9AsCRbPQ'; //Master

                                                    /* Endpoints */
                                                    $subscribe_url = 'http://maylbox.com/subscribe';

                                                    /* Subscribe Body */
                                                    $body = array(
                                                        'name' => $post_name,
                                                        'email' => $email,
                                                        'Phone' => $telephone,
                                                        'Location' => $state->name,
                                                        'PreviousImplementation' => $implementation,
                                                        'EducationalStatus' => $educational_status,
                                                        'contribution' => $contribution,
                                                        'ThematicArea' => get_the_title($thematic_area_id),
                                                        'ChallengeArea' => $post_title,
                                                        'list' => $master_list,
                                                        'boolean' => 'true'
                                                    );

                                                    $args = array(
                                                        'body' => $body,
                                                        'timeout' => '5',
                                                        'redirection' => '5',
                                                        'httpversion' => '1.0',
                                                        'blocking' => true,
                                                        'headers' => array(),
                                                        'cookies' => array()
                                                    );

                                                    $response = wp_remote_post( $subscribe_url, $args );
                                                    $http_code = wp_remote_retrieve_response_code( $response );
                                                    $body = wp_remote_retrieve_body( $response );
                                                    /* Update Mailing List End */

                                                    /* Redirect */
                                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                                }
                                            ?>

                                            <h3 class="txt-2em txt-light padding-b-20">
                                                CHAMPION THIS IDEA
                                            </h3>

                                            <div class="margin-b-30 padding-tb-15 border-b-1 border-color-darkgrey">
                                                <p class="txt-normal-s txt-height-1-7">
                                                    Idea Champions believe in the effectiveness of an idea and are willing to actively participate in making it a reality. If this idea resonates with you, please submit your name as an idea champion and we will reach out to you first, if we decide to convert this idea into a project.
                                                </p>
                                            </div>

                                            <div class="form margin-b-40">
                                                <div class="row row-10">
                                                    <!-- Name -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="post_name">Name</label>
                                                        <input 
                                                            type="text" 
                                                            name="post_name" 
                                                        >
                                                    </div>
                                                    <!-- Telephone -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="telephone">Telephone</label>
                                                        <input 
                                                            type="telephone" 
                                                            name="telephone" 
                                                        >
                                                    </div>
                                                    <!-- Email -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="email">Email</label>
                                                        <input 
                                                            type="email" 
                                                            name="email" 
                                                        >
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="location">Location</label>
                                                        <select name="location" id="location">
                                                        <?php 
                                                            switch_to_blog(110);
                                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                                            restore_current_blog();

                                                            foreach( $terms as $term ){
                                                        ?>
                                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                    <!-- Implementation -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="implementation">Have you implemented this idea anywhere?</label>
                                                        <select name="implementation" id="implementation">
                                                            <option value="No, But i believe it will work">No</option>
                                                            <option value="I have started working on it, but haven't launched">Yes, I have led the implementation of this idea in the past </option>
                                                            <option value="I have implemented the idea but at early stages">Yes, I have been part of a team that Implemented this idea</option>
                                                        </select>
                                                    </div>
                                                    <!-- Idea Implementation -->
                                                    <div class="col-lg-6 padding-lr-10 form-item">
                                                        <label for="educational_status">Educational status</label>
                                                        <select name="educational_status" id="educational_status">
                                                            <option value="Secondary School Student">Secondary School Student</option>
                                                            <option value="Undergraduate">Undergraduate</option>
                                                            <option value="Graduate (Pre-NYSC)">Graduate (Pre-NYSC)</option>
                                                            <option value="NYSC Corps Member">NYSC Corps Member</option>
                                                            <option value="Graduate (Post NYSC)">Graduate (Post NYSC)</option>
                                                            <option value="Professional">Professional</option>
                                                        </select>
                                                    </div>
                                                    <!-- Contribution -->
                                                    <div class="col-lg-12 padding-lr-10 form-item">
                                                        <label for="contribution">How can you contribute to this idea</label>
                                                        <textarea name="contribution" rows="8"></textarea>
                                                    </div>
                                                </div>

                                                <div class="padding-t-30">
                                                    <input type="submit" id="addIdeaButton" class="btn btn-green txt-sm padding-lr-15">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        
        <section class="bg-grey padding-tb-60">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="txt-xlg text-center padding-b-40">
                            Explore other ideas to <span class="text-lowercase"><?php echo get_the_title($thematic_area_id); ?></span>
                        </h2>
                        <div class="row row-15">
                        <?php if ( $idea_query->have_posts() ) { ?>

                        <?php 
                            while ( $idea_query->have_posts() ) : $idea_query->the_post(); 
                                $idea_id = get_the_ID();
                        ?>

                        
                        <div class="col-md-6 col-lg-4 col-xl-4 d-flex padding-lr-15 padding-b-30">
                            <div class="ynl-theme-challenges-card bg-white" href="">
                                <article class="content d-flex flex-column justify-content-between">
                                    <div>
                                        <h3 class="txt-lg txt-medium txt-color-green txt-height-1-4 txt-color-light padding-b-15">
                                            <a class="txt-color-green" href="<?php the_permalink() ?>"><?php echo the_title() ?></a>
                                        </h3>
                                        <div class="txt-sm txt-height-1-7 padding-b-60">
                                            <?php 
                                                $meta = get_post_meta($idea_id, 'description', true);
                                                if($meta){
                                                    echo truncate($meta, 200);
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="padding-b-15">
                                            <a href="#" class="d-inline-flex padding-r-10 align-items-center txt-color-green txt-sm">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/up_vote.png" alt="" height="14">
                                                <span class="txt-bold padding-l-5">0</span>
                                            </a>
                                            <a href="#" class="d-inline-flex padding-r-10 align-items-center txt-color-lighter txt-sm">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/down_vote.png" alt="" height="14">
                                                <span class="txt-bold padding-l-5">0</span>
                                            </a>
                                            <a href="<?php the_permalink() ?>/?tab=2" class="d-inline-flex padding-r-10 align-items-center txt-color-green txt-sm">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/voices.png" alt="" height="14">
                                                <span class="txt-bold padding-l-5">
                                                    <?php comments_number( '0', '1', '%' ); ?>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between padding-t-15 border-t-1 border-color-darkgrey">
                                            <span class="d-inline-flex padding-r-10 align-items-center txt-color-green txt-sm">
                                                <a data-toggle="modal" href="#championIdeaModal"><i class="fa  fa-info-circle"></i></a>
                                                <a data-toggle="modal" href="#championIdeaFormModal-<?php echo $idea_id ?>"><u class="padding-l-5">Champion this idea</u></a>
                                            </span>
                                            <span class="d-inline-flex padding-r-10 align-items-center txt-color-green txt-sm">
                                                <a data-toggle="modal" href="#addVoiceModal"><i class="fa  fa-info-circle"></i></a>
                                                <a href="<?php the_permalink() ?>/?tab=2"><u class="padding-l-5">Add your voice</u></a>
                                            </span>
                                        </div>
                                    </div>
                                </article>  
                            </div>   
                        </div>
                        
                        <!-- Champion Idea -->
                                <div class="modal fade font-main filter-modal" id="championIdeaFormModal-<?php echo $idea_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="<?php echo currentUrl(true).'?view=form-champion-idea-'.$idea_id; ?>" method="post">
                                                <div class="modal-header padding-lr-30">
                                                    <h5 class="modal-title" id="exampleModalLabel">Champion <?php echo the_title() ?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body padding-o-30">
                                                    <?php
                                                        if( $_POST && $_GET['view'] == 'form-champion-idea-'.$idea_id ){

                                                            /* Get Post Name */
                                                            $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                                            /* Meta */
                                                            $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                                            $email = sanitize_text_field( $_POST['email'] ); 
                                                            $location = sanitize_text_field( $_POST['location'] ); 
                                                            $implementation = sanitize_text_field( $_POST['implementation'] ); 
                                                            $educational_status = sanitize_text_field( $_POST['educational_status'] ); 
                                                            $contribution = wp_kses_post( $_POST['contribution'] );

                                                            /* Save Post to DB */
                                                            $champion_id = wp_insert_post(array (
                                                                'post_type' => 'idea-champion',
                                                                'post_title' => $post_name,
                                                                'post_content' => "",
                                                                'post_status' => 'publish',
                                                            ));

                                                            update_post_meta( $champion_id, 'thematic_area', $post_id );
                                                            update_post_meta( $champion_id, 'problem_area', $idea_id );

                                                            update_post_meta( $champion_id, 'telephone', $telephone );
                                                            update_post_meta( $champion_id, 'email', $email );
                                                            update_post_meta( $champion_id, 'location', $location );
                                                            update_post_meta( $champion_id, 'implementation', $implementation );
                                                            update_post_meta( $champion_id, 'educational_status', $educational_status );
                                                            update_post_meta( $champion_id, 'contribution', $contribution );

                                                            /* Update Mailing List */
                                                            /* Data */
                                                            switch_to_blog(110);
                                                            $state = get_term( $location );
                                                            restore_current_blog();

                                                            $api_key = '0fkxtL9crlJFX7cqCXUV';
                                                            $master_list = 'YdUPrXEfWxjULk9AsCRbPQ'; //Master

                                                            /* Endpoints */
                                                            $subscribe_url = 'http://maylbox.com/subscribe';

                                                            /* Subscribe Body */
                                                            $body = array(
                                                                'name' => $post_name,
                                                                'email' => $email,
                                                                'Phone' => $telephone,
                                                                'Location' => $state->name,
                                                                'PreviousImplementation' => $implementation,
                                                                'EducationalStatus' => $educational_status,
                                                                'contribution' => $contribution,
                                                                'ThematicArea' => get_the_title($thematic_area_id),
                                                                'ChallengeArea' => $post_title,
                                                                'list' => $master_list,
                                                                'boolean' => 'true'
                                                            );

                                                            $args = array(
                                                                'body' => $body,
                                                                'timeout' => '5',
                                                                'redirection' => '5',
                                                                'httpversion' => '1.0',
                                                                'blocking' => true,
                                                                'headers' => array(),
                                                                'cookies' => array()
                                                            );

                                                            $response = wp_remote_post( $subscribe_url, $args );
                                                            $http_code = wp_remote_retrieve_response_code( $response );
                                                            $body = wp_remote_retrieve_body( $response );
                                                            /* Update Mailing List End */

                                                            /* Redirect */
                                                            printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                                        }
                                                    ?>

                                                    <h3 class="txt-2em txt-light padding-b-20">
                                                        CHAMPION THIS IDEA
                                                    </h3>

                                                    <div class="margin-b-30 padding-tb-15 border-b-1 border-color-darkgrey">
                                                        <p class="txt-normal-s txt-height-1-7">
                                                            Idea Champions believe in the effectiveness of an idea and are willing to actively participate in making it a reality. If this idea resonates with you, please submit your name as an idea champion and we will reach out to you first, if we decide to convert this idea into a project.
                                                        </p>
                                                    </div>

                                                    <div class="form margin-b-40">
                                                        <div class="row row-10">
                                                            <!-- Name -->
                                                            <div class="col-lg-6 padding-lr-10 form-item">
                                                                <label for="post_name">Name</label>
                                                                <input 
                                                                    type="text" 
                                                                    name="post_name" 
                                                                >
                                                            </div>
                                                            <!-- Telephone -->
                                                            <div class="col-lg-6 padding-lr-10 form-item">
                                                                <label for="telephone">Telephone</label>
                                                                <input 
                                                                    type="telephone" 
                                                                    name="telephone" 
                                                                >
                                                            </div>
                                                            <!-- Email -->
                                                            <div class="col-lg-6 padding-lr-10 form-item">
                                                                <label for="email">Email</label>
                                                                <input 
                                                                    type="email" 
                                                                    name="email" 
                                                                >
                                                            </div>
                                                            <!-- Location -->
                                                            <div class="col-lg-6 padding-lr-10 form-item">
                                                                <label for="location">Location</label>
                                                                <select name="location" id="location">
                                                                <?php 
                                                                    switch_to_blog(110);
                                                                    $terms = get_terms( 'state', array('hide_empty' => false));
                                                                    restore_current_blog();

                                                                    foreach( $terms as $term ){
                                                                ?>
                                                                    <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                                                <?php } ?>
                                                                </select>
                                                            </div>
                                                            <!-- Implementation -->
                                                            <div class="col-lg-6 padding-lr-10 form-item">
                                                                <label for="implementation">Have you implemented this idea anywhere?</label>
                                                                <select name="implementation" id="implementation">
                                                                    <option value="No, But i believe it will work">No</option>
                                                                    <option value="I have started working on it, but haven't launched">Yes, I have led the implementation of this idea in the past </option>
                                                                    <option value="I have implemented the idea but at early stages">Yes, I have been part of a team that Implemented this idea</option>
                                                                </select>
                                                            </div>
                                                            <!-- Idea Implementation -->
                                                            <div class="col-lg-6 padding-lr-10 form-item">
                                                                <label for="educational_status">Educational status</label>
                                                                <select name="educational_status" id="educational_status">
                                                                    <option value="Secondary School Student">Secondary School Student</option>
                                                                    <option value="Undergraduate">Undergraduate</option>
                                                                    <option value="Graduate (Pre-NYSC)">Graduate (Pre-NYSC)</option>
                                                                    <option value="NYSC Corps Member">NYSC Corps Member</option>
                                                                    <option value="Graduate (Post NYSC)">Graduate (Post NYSC)</option>
                                                                    <option value="Professional">Professional</option>
                                                                </select>
                                                            </div>
                                                            <!-- Contribution -->
                                                            <div class="col-lg-12 padding-lr-10 form-item">
                                                                <label for="contribution">How can you contribute to this idea</label>
                                                                <textarea name="contribution" rows="8"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer padding-lr-30">
                                                    <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-green txt-sm padding-lr-15">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                        <?php endwhile; ?>

                        <?php } ?>
                    </div> 
                    </div>
                </div>
            </div>
        </section>
        
        <section class="padding-tb-40">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <span class="txt-xxlg txt-light txt-height-1-4 padding-r-20">View other impact programs</span>
                        <a class="btn btn-green txt-sm no-m-b d-block d-lg-inline" href="https://www.saedconnect.org/young-leaders">View</a>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Add Idea -->
        <div class="modal fade font-main filter-modal" id="addIdeaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-addIdea'; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            <?php
                                if( $_POST && $_GET['view'] == 'form-addIdea' ){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $description = wp_kses_post( $_POST['description'] ); 
                                    $challenge_areas = $_POST['challenge_areas'];
                                    $challenge_description = wp_kses_post( $_POST['challenge_description'] ); 
                                    $solution_description = wp_kses_post( $_POST['solution_description'] ); 
                                    $implementation = wp_kses_post( $_POST['implementation'] ); 
                                    $agreementCheckbox= wp_kses_post( $_POST['agreementCheckbox'] ); 

                                    /* Save Post to DB */
                                    $idea_id = wp_insert_post(array (
                                        'post_type' => 'idea',
                                        'post_title' => $post_name,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $idea_id, 'thematic_area', $post_id );
                                    
                                    update_post_meta( $idea_id, 'description', $description );
                                    update_post_meta( $idea_id, 'challenge_areas', $challenge_areas );
                                    update_post_meta( $idea_id, 'challenge_description', $challenge_description );
                                    update_post_meta( $idea_id, 'solution_description', $solution_description );
                                    update_post_meta( $idea_id, 'implementation', $implementation );
                                    update_post_meta( $idea_id, 'agreementCheckbox', $agreementCheckbox );

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                }
                            ?>
                            
                            <h3 class="txt-xxlg txt-light padding-b-30">
                                Add an idea to end violence against women and girls
                            </h3>

                            <div class="form margin-b-40">
                                <div class="form-item">
                                    <label for="post_name">Title</label>
                                    <input 
                                        type="text" 
                                        name="post_name" 
                                    >
                                </div>
                                <!-- Description -->
                                <div class="form-item">
                                    <label for="post-name">
                                        Describe the idea
                                    </label>
                                    <textarea name="description" rows="8"><?php echo $description ?></textarea>
                                </div>
                                <!-- Challenges -->
                                <div class="form-item">
                                    <label class="d-block padding-b-10">Challenge Areas</label>
                                    <?php 
                                        while ( $challenges_query->have_posts() ) : $challenges_query->the_post(); 
                                            $challenge_id = get_the_ID();
                                    ?>
                                    <label class="txt-sm d-flex align-items-center padding-b-10 padding-r-15">
                                        <input
                                            class="custom-check"
                                            type="checkbox" 
                                            value="<?php echo $challenge_id ?>" 
                                            name="challenge_areas[]" 
                                        >
                                        <span class="padding-l-10">
                                            <?php the_title() ?>
                                        </span>
                                    </label>
                                    <?php endwhile; ?>
                                </div>
                                <!-- Challenge Description -->
                                <div class="form-item">
                                    <label for="challenge_description">
                                        Describe the specific challenge this idea will solve 
                                    </label>
                                    <textarea name="challenge_description" rows="8"><?php echo $challenge_description ?></textarea>
                                </div>
                                <!-- Challenge Description -->
                                <div class="form-item">
                                    <label for="solution_description">
                                        How will this idea reduce or eliminate the problem?
                                    </label>
                                    <textarea name="solution_description" rows="8"><?php echo $solution_description ?></textarea>
                                </div>
                                <!-- Implementation -->
                                <div class="form-item">
                                    <label for="implementation">Have you implemented this idea anywhere?</label>
                                    <select name="implementation" id="implementation">
                                        <option value="No, But i believe it will work">No, But i believe it will work</option>
                                        <option value="I have started working on it, but haven't launched">I have started working on it, but haven't launched</option>
                                        <option value="I have implemented the idea but at early stages">I have implemented the idea but at early stages</option>
                                        <option value="I have implemented and it is working">I have implemented and it is working</option>
                                    </select>
                                </div>
                                <!-- Accept Terms -->
                                <div class="form-item">
                                    <label class="txt-sm d-flex align-items-start padding-b-10 padding-r-15">
                                        <span class="flex_1">
                                            <input
                                                class="custom-check"
                                                type="checkbox" 
                                                value="<?php echo $challenge_id ?>" 
                                                name="agreementCheckbox"
                                                id="agreementCheckbox"
                                            >
                                        </span>
                                        <span class="txt-normal-s txt-color-light txt-height-1-7 padding-l-20">
                                            By submitting this idea, I agree that the SAEDConnect program staff and selection committee are able to review the information I have posted. I also agree that I have taken care not to provide proprietary information. By submitting this idea, I also agree to the terms and conditions.
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" id="addIdeaButton" class="btn btn-green txt-sm padding-lr-15" disabled>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    
<?php
    
?>

<script type="text/javascript">
    $('.showChampionTab').on('click', function (e) {
        e.preventDefault();
        $('#pills-tab a[href="#pills-3"]').tab('show')
    });
</script>

   
<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('input.custom-check').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<?php get_footer() ?>