<?php /*Template Name: Profile - Trainging Requests*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            
            <div class="dashboard-multi-main-content full">
               
             <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                <?php if( $_REQUEST['gf-id'] ){ //GF View ?>
                   
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                    
                <?php } else { //Configuratin ?>
                
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post">
                            <?php
                                /* Meta Key */
                                $postType = 'training-request';
                                $tax_types = array(
                                    array(
                                        'name' => 'Training Category',
                                        'slug' => 'training-category',
                                        'hierachical' => true,
                                    ),
                                );
                                $redirect_link = currentUrl(true);
                              
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    $post = get_post($post_id);
                                    $postName = $post->post_title;
                                    
                                    /* Delete & Return */
                                    if($_GET['action'] == 'delete'){
                                        /* Delete Post */
                                        wp_delete_post($post_id);

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                }
                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if($_POST){
                                    
                                    /* Get Post Name */
                                    $postName = $_POST['post-name'];
                                    
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    /* Save terms to post */
                                    foreach($tax_types as $tax_type){
                                        wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug'] );
                                    }
                                    
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }
                            ?>
                            
                            <div class="margin-b-40">
                                <label for="post-name" class="d-block txt-normal-s txt-color-dark txt-medium margin-b-10">
                                    Training Request Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post-name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $postName ?>"
                                >
                            </div>
                            
                            
                            <?php foreach($tax_types as $tax_type){ ?>
                            
                                <?php if( $tax_type['hierachical']){ ?>
                            
                                    <!-- Tax Title -->
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_type['name']; ?>
                                    </h2>

                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <?php if( $parent == 0 ){ ?>

                                            <div class="padding-b-20">
                                                <div class="txt-medium txt-color-dark margin-b-15">
                                                    <?php echo $term_name; ?>
                                                </div>
                                                <?php
                                                    foreach ($terms as $child_term) {
                                                        // Check and see if the term is a top-level parent. If so, display it.
                                                        $child_parent = $child_term->parent;
                                                        $child_term_id = $child_term->term_id; //Get the term ID
                                                        $child_term_name = $child_term->name; //Get the term name

                                                        if( $child_parent == $term_id ){
                                                ?>
                                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                            <input
                                                                class="margin-r-5"
                                                                type="checkbox" 
                                                                value="<?php echo $child_term_id ?>" 
                                                                name="<?php echo $tax_type['slug'] ?>[]" 
                                                                <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                            >
                                                            <span class="bg-label">
                                                                <?php echo $child_term_name; ?>
                                                            </span>
                                                        </label>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </div>

                                        <?php } ?>

                                    <?php } ?>
                                
                                <?php } else { ?>
                                
                                    <!-- Tax Title -->
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_type['name']; ?>
                                    </h2>
                                    
                                    <div class="padding-b-20">
                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                            <input
                                                class="margin-r-5"
                                                type="checkbox" 
                                                value="<?php echo $term_id ?>" 
                                                name="<?php echo $tax_type['slug'] ?>[]" 
                                                <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                            >
                                            <span class="bg-label">
                                                <?php echo $term_name; ?>
                                            </span>
                                        </label>

                                    <?php } ?>
                                    </div>
                                <?php } ?>
                                
                            <?php } ?>
                                                        
                            <div class="text-right">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>
                
                <?php } ?>  
                </div>
                
            <?php } elseif($_GET['view'] == 'interactions'){ ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        Training Requests I have engaged
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Here you would find all the trainings that you have requested for here.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Training Requests
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Training Requests I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="row row-15">
                <?php
                    /* GF Search Criteria */
                    $gf_id = 7;
                    $parent_post_id = 2;
                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                    foreach( $entries as $entry ){

                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $parent_post = get_post($parent_post_id);
                ?>
                    <div class="col-md-4 padding-lr-15 padding-b-30 d-flex">
                        <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                            <h3 class="margin-b-10 txt-color-blue margin-b-10">
                                <?php echo rgar( $entry, '5' ) ?>
                            </h3>
                            <div class="txt-normal-s margin-b-30">
                                <p>
                                    <?php echo rgar( $entry, '6' ) ?>
                                </p>
                            </div>
                            <h3 class="txt-sm txt-color-dark txt-medium">
                                For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                            </h3>
                        </div>
                    </div>
                <?php } ?>
                </div>
                 
            <?php } else { ?>
                 
                <div class="page-header">
                    <h1 class="page-title">
                        My Training Requests
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form-title=Add Training Request" class="cta-btn">
                            Add Request
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Here you can request for a training in any subject area here.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Training requests
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Training Requests I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                                
                <div class="row row-5">
                    <?php
                        function truncate($string, $length){
                            if (strlen($string) > $length) {
                                $string = substr($string, 0, $length) . '...';
                            }

                            return $string;
                        }

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'training-request',
                                'post_status' => 'publish',
                                'author' => $current_user->ID,
                                'posts_per_page' => -1,
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID

                            /* Get Opportunity Banner */
                            $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                            $image = reset( $images );
                    ?>
                        <div class="col-md-4 padding-lr-5 padding-b-20 d-flex">
                            <div class="home-community-card">
                                <article class="content">
                                    <div class="info bg-white">
                                        <h3 class=" txt-medium margin-b-10">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <p class="txt-sm">
                                            <?php
                                                $meta = get_post_meta($post_id, 'training_request_summary', true);
                                                if($meta){
                                                    echo truncate($meta, 70);
                                                }
                                            ?>
                                        </p>
                                        <p class="txt-sm txt-color-dark padding-t-20">
                                            <?php 
                                                $response_gf_id = 7; //Form ID

                                                /* GF Search Criteria */
                                                $response_search_criteria = array(

                                                'field_filters' => array( //which fields to search

                                                    array(

                                                        'key' => '2', 'value' => $post_id, //Current logged in user
                                                        )
                                                    )
                                                );

                                                /* Get Entries */
                                                $response_entries = GFAPI::get_entries( $response_gf_id, $response_search_criteria );

                                                /* Get GF Entry Count */
                                                $response_entry_count = GFAPI::count_entries( $response_gf_id, $response_search_criteria );
                                            ?>
                                            <span class="txt-bold">
                                                <?php echo $response_entry_count ?>
                                            </span>
                                            Response(s)
                                        </p>
                                        <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                            <div>
                                                <a 
                                                   href="<?php printf('%s/?view=form&post-id=%s&form-title=Edit %s', currentUrl(true), $post_id, get_the_title() ) ?>"
                                                   class="txt-medium txt-color-green"
                                                >
                                                    Edit
                                                </a>
                                                |
                                                <a 
                                                   href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', currentUrl(true), $post_id ) ?>"
                                                   class="confirm-delete txt-color-red"
                                                >
                                                    Delete
                                                </a>
                                            </div>
                                            <div class="text-right">
                                                <?php
                                                    $request_desc_gf_id = 8; //Form ID

                                                    /* GF Search Criteria */
                                                    $request_desc_search_criteria = array(

                                                    'field_filters' => array( //which fields to search

                                                        array(

                                                            'key' => '2', 'value' => $post_id, //Current logged in user
                                                            )
                                                        )
                                                    );

                                                    /* Get Entries */
                                                    $request_desc_entries = GFAPI::get_entries( $request_desc_gf_id, $request_desc_search_criteria );

                                                    /* Get GF Entry Count */
                                                    $request_desc_entry_count = GFAPI::count_entries( $request_desc_gf_id, $request_desc_search_criteria );

                                                ?>

                                                <?php if (!$request_desc_entry_count){ ?>

                                                <a 
                                                   href="<?php echo currentUrl(true); ?>?view=form&gf-id=8&parent_id=<?php echo $post_id ?>&form-title=Describe <?php the_title(); ?>" 
                                                   class="txt-medium txt-color-blue"
                                                >
                                                    Add Description
                                                </a>

                                                <?php } else { ?>

                                                <a 
                                                   href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$request_desc_entries[0]['id'].'" view_id="752" action="edit" return="url" /]').'&view=training-request&form-title=Edit '.get_the_title().' Description' ?>" 
                                                   class="txt-medium txt-color-blue"
                                                >
                                                    Edit Description
                                                </a>

                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </article>  
                            </div>   
                        </div>
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                            <h2 class="txt-lg txt-medium">
                                No Training Requests found.
                            </h2>
                        </div>   

                    <?php } ?>
                </div>
                
            <?php } ?>
                
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>