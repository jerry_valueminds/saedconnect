<?php while ( have_posts() ) : the_post(); ?>
                
    <!-- Loop: Find First Multi-Page-Article related to Multi-Page-->
    <?php 

        $nav = new WP_Query( array(
            'relationship' => array(
                'id'   => 'multi_page_to_multi_page_article',
                'from' => get_the_ID(), // You can pass object ID or full object
            ),
            'posts_per_page' => 1,
        ) );
        while ( $nav->have_posts() ) : $nav->the_post(); ?>   

            <?php
                if ( wp_redirect( get_permalink()) ) {
                    exit;
                }
            ?>
    <?php
        endwhile;
        wp_reset_postdata();
    ?>
    <!-- Loop: END -->

<?php endwhile; // end of the loop. ?>