<?php /*Template Name: Profile - Interaction: Jobs*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        Internship & Job Applications
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        This is where you would interact with potential employers for all the Jobs you have applied for.
                    </p>
                </article>
                <div class="row row-15">
                <?php
                    /* GF Search Criteria */
                    $gf_id = 13;
                    $parent_post_id = 2;
                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                    foreach( $entries as $entry ){

                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $parent_post = get_post($parent_post_id);
                ?>
                    <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                        <div class="flex_1 padding-o-30 border-o-1 border-color-darkgrey">
                            <h3 class="margin-b-10 txt-lg txt txt-color-blue margin-b-10">
                                <?php echo rgar( $entry, '1' ) ?>
                            </h3>
                            <div class="txt-normal-s margin-b-30">
                                <p>
                                    <?php echo rgar( $entry, '6' ) ?>
                                </p>
                            </div>
                            <h3 class="txt-sm txt-color-dark txt-medium">
                                For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                            </h3>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>