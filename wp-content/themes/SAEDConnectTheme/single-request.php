<?php get_header() ?>
   
<?php
    $site_url = site_url();
    $current_user = wp_get_current_user();

    $gf_id = 12;
    $gv_id = 89;

?>
    
<?php while ( have_posts() ) : the_post(); ?>

<?php
    $post_id = get_the_ID();
    $form_title = get_the_title();

    /* GF Search Criteria */
    $search_criteria = array(

    'field_filters' => array( //which fields to search

        array(

            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
            'key' => 'post_id', 'value' => $post_id, //Current logged in user
        )
      )
    );

    /* Get GF Entry Count */
    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

    foreach( $entries as $entry ){
        $form_entry = $entry;
        $entry_id = $entry['id'];
    }
    
    /*Get Theme Color*/
    $theme_color = rwmb_meta( 'opportunity-theme' );

    /*Get Theme Alt Color*/
    $theme_alt_color = rwmb_meta( 'opportunity-theme-alt' );

    /* Use User data or admin data */
    $user_data = rwmb_get_value( 'user_data' );

    $program_images = rwmb_meta( 'change-program-image', array( 'limit' => 1 ) );
    $program_images = reset( $program_images );

    
?>

    <main class="main-content">
        <section class="container-wrapper bg-grey padding-t-40 padding-b-40 margin-b-40">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="txt-2em txt-medium margin-b-20">
                        <?php the_title() ?>
                    </h1>
                </div>
            </div>
        </section>
        
        <div class="section margin-b-40">
            <div class="container-wrapper">
                <div class="row row-20">
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            Tool Type
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_type', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </p>
                    </div>
                    <?php
                        $meta = get_post_meta($post_id, 'tool_type_order', true);
                        if($meta){
                    ?>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <p class="txt-normal-s txt-medium margin-b-10">
                                Tool Type (Other Description)
                            </p>
                            <p class="txt-sm">
                                <?php echo $meta; ?>
                            </p>
                        </div>
                    <?php } ?>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            Tool Description
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_description', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </p>
                    </div>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            How this tool will help me achieve this goal
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_impact', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </p>
                    </div>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            Quantity I need
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_quantity', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </p>
                    </div>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            Locations where I need this tool
                        </p>
                        <p class="txt-sm" style="text-transform:capitalize">
                            <?php
                                $field = 'tool_location_needed';

                                $meta = get_post_meta($post->ID, $field, false);

                                if($meta){
                                    foreach($meta as $key=>$value) {
                                        echo $value;
                                        if($key < count($meta) - 1 ){
                                            echo ', ';
                                        }
                                    }
                                }

                            ?>
                        </p>
                    </div>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            When I need this Tool
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_when_needed', true);
                                if($meta){
                                    $date = strtotime($meta);

                                    echo date('j F Y',$date);

                                }
                            ?>
                        </p>
                    </div>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            How long I need this tool
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_need_duration', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </p>
                    </div>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            Can I pay for this tool
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_affordability', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </p>
                    </div>
                    <?php
                        $meta = get_post_meta($post_id, 'tool_offererd_amount', true);
                        if($meta){
                    ?>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            I am willing to offer
                        </p>
                        <p class="txt-sm">
                            <?php echo $meta; ?>
                        </p>
                    </div>
                    <?php } ?>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            Other things I can offer to anyone willing to help
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_returns', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </p>
                    </div>
                    <?php
                        $meta = get_post_meta($post_id, 'tool_request_expiry', true);
                        if($meta){
                    ?>
                    <div class="col-md-4 padding-lr-20 padding-b-40">
                        <p class="txt-normal-s txt-medium margin-b-10">
                            Request Expires
                        </p>
                        <p class="txt-sm">
                            <?php
                                $meta = get_post_meta($post_id, 'tool_request_expiry_date', true);
                                if($meta){
                                    $date = strtotime($meta);

                                    echo date('j F Y',$date);

                                }
                            ?>
                        </p>
                    </div>
                    <?php } ?>
                </div>
                <div class="txt-xxlg text-center margin-b-40">
                    Responses
                </div>
                <div class="margin-b-10">
                    <?php
                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 2, 'value' => $post_id, //Parent Post
                            )
                          )
                        );

                        /* Get GF Entry Count */
                        $order_count = GFAPI::count_entries( $request_item['gv_id'], $search_criteria );
                        $orders = GFAPI::get_entries( $request_item['gf_id'], $search_criteria );

                        foreach( $orders as $order ){
                    ?>
                    
                    <div class="txt-normal-s margin-b-10">
                        Name: <?php echo rgar( $order, '1' ); ?>
                    </div>
                    <div class="txt-normal-s margin-b-10">
                        Phone: <?php echo rgar( $order, '3' ); ?>
                    </div>
                    <div class="txt-normal-s margin-b-10">
                        Email: <?php echo rgar( $order, '4' ); ?>
                    </div>
                    <div class="txt-normal-s margin-b-10">
                        <?php echo rgar( $order, '6' ); ?>
                    </div>
                    <div class="txt-normal-s margin-b-10">
                        <?php echo rgar( $order, '7' ); ?>
                    </div>
                    
                    <?php } ?>
                </div>
                <div class="text-center">
                    <a
                        href="<?php printf('%s/respond-to-request/?form-title=Respond To Request&gf-id=26&parent_id=%s', $site_url, $post_id); ?>" 
                        class="btn btn-blue txt-sm no-m-b"
                    >
                        Respond to Request
                    </a>
                </div>
            </div>
        </div>
    </main>

<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>