<?php /*Template Name: Marketplace Form*/ ?>

<?php get_header() ?>

<style>
    .work-profile{
        display: none !important;
    }
    
    .gform_wrapper .top_label .gfield_label {
        font-size: 0.8rem !important;
        font-weight: 500 !important;
    }
    
    .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
        font-size: 0.9rem !important;
        width: 100% !important;
    }
    
    .gform_wrapper .gform_button {
        background-color: #b55085 !important;
        font-size: 0.7rem !important;
        width: auto !important;
    }
    
    .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
        display: none !important;
    }
</style>

<main class="main-content txt-color-light" style="margin-top: 100px"> 

<?php
    /* Reset Global Queried Object  */
    wp_reset_postdata();
    wp_reset_query();
   
    while ( have_posts() ) : the_post();
?>
   
    <div class="container-wrapper bg-white padding-t-80">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="header text-center">
                    <h2 class="txt-xxlg txt-height-1-2 txt-light txt-color-dark section-wrapper-title">
                        <?php echo $_GET['form-title']; ?>          
                    </h2>
                </div>
                
                <?php
                    /*
                    *
                    *   Check if user has Freelancer Profile
                    *
                    */
                    /* Get current User ID */
                    $current_user = wp_get_current_user();

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $profile_count = GFAPI::count_entries( 8, $search_criteria );
                                
                    //if( !$profile_count ){
                    if( false ){
                ?>
                
                <article class="text-center">
                    <p class="padding-t-40">
                        You do not have a Freelancer profile. You must complete your freelancer profile before you can continue.<br>
                        <a href="" class="txt-color-blue">Click here to create your Freelance Profile</a>
                    </p>
                </article>
                
                <?php } else { ?>

                <div class="entry">
                    <?php
                        /* Get Form ID from request */
                        $gf_id = $_GET['gf-id'];

                        /* Get Form */
                        echo do_shortcode('[gravityform id="'.$gf_id.'" title="false" description="false"]');
                    ?>
                </div>
                
                <?php } ?>
            </div>
        </div>
    </div>
    
<?php
    endwhile; // end of the loop.
?>

</main>

<?php wp_footer(); ?>
