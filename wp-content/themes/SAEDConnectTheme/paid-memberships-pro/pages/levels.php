<header class="margin-b-40">
    <div class="row">
        <div class="col-md-6">
            <h1 class="txt-2em txt-bold margin-b-20">
                Levels
            </h1>
            <article class="">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam fuga similique iure in quis dignissimos aliquam, praesentium, libero a illum magni molestiae autem at aperiam, illo blanditiis sequi aut quos.
                </p>
            </article>
        </div>
    </div>
</header>
                   


<?php 
global $wpdb, $pmpro_msg, $pmpro_msgt, $current_user;

$pmpro_levels = pmpro_getAllLevels(false, true);
$pmpro_level_order = pmpro_getOption('level_order');

if(!empty($pmpro_level_order))
{
	$order = explode(',',$pmpro_level_order);

	//reorder array
	$reordered_levels = array();
	foreach($order as $level_id) {
		foreach($pmpro_levels as $key=>$level) {
			if($level_id == $level->id)
				$reordered_levels[] = $pmpro_levels[$key];
		}
	}

	$pmpro_levels = $reordered_levels;
}

$pmpro_levels = apply_filters("pmpro_levels_array", $pmpro_levels);

if($pmpro_msg)
{
?>
<div class="pmpro_message <?php echo $pmpro_msgt?>"><?php echo $pmpro_msg?></div>
<?php
}
?>
<section id="pmpro_levels_table" class="pmpro_checkout full-width">
    <div class="txt-bold padding-b-20 margin-b-20 border-b-1 border-color-darkgrey d-none d-md-block">
        <div class="row">
            <div class="col-md-6"><?php _e('Level', 'paid-memberships-pro' );?></div>
            <div class="col-md-4"><?php _e('Price', 'paid-memberships-pro' );?></div>	
            <div class="col-md-2">&nbsp;</div>
        </div>
    </div>
    <?php	
        $count = 0;
        foreach($pmpro_levels as $level)
        {
          if(isset($current_user->membership_level->ID))
              $current_level = ($current_user->membership_level->ID == $level->id);
          else
              $current_level = false;
        ?>
           
        <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
            <div class="row <?php if($count++ % 2 == 0) { ?>odd<?php } ?><?php if($current_level == $level) { ?> active<?php } ?>">
                <div class="col-md-6 txt-medium padding-b-10">
                    <?php echo $current_level ? "<strong class='txt-color-green'>{$level->name}</strong>" : $level->name?>
                </div>
                <div class="col-md-4 padding-b-10">
                    <?php 
                        if(pmpro_isLevelFree($level))
                            $cost_text = "<strong class='txt-color-green'>" . __("Free", 'paid-memberships-pro' ) . "</strong>";
                        else
                            $cost_text = pmpro_getLevelCost($level, true, true); 
                        $expiration_text = pmpro_getLevelExpiration($level);
                        if(!empty($cost_text) && !empty($expiration_text))
                            echo $cost_text . "<br />" . $expiration_text;
                        elseif(!empty($cost_text))
                            echo $cost_text;
                        elseif(!empty($expiration_text))
                            echo $expiration_text;
                    ?>
                </div>
                <div class="col-md-2 text-md-right padding-b-10">
                <?php if(empty($current_user->membership_level->ID)) { ?>
                    <a class="pmpro_btn-select btn btn-blue txt-xs no-m-b" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https")?>">
                        <?php _e('Pay', 'paid-memberships-pro' );?>
                    </a>
                <?php } elseif ( !$current_level ) { ?>                	
                    <a class="pmpro_btn-select btn btn-blue txt-xs no-m-b" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https")?>">
                        <?php _e('Pay', 'paid-memberships-pro' );?>
                    </a>
                <?php } elseif($current_level) { ?>      

                    <?php
                        //if it's a one-time-payment level, offer a link to renew				
                        if( pmpro_isLevelExpiringSoon( $current_user->membership_level) && $current_user->membership_level->allow_signups ) {
                            ?>
                                <a class="pmpro_btn-select btn btn-blue txt-xs no-m-b" href="
                                    <?php echo pmpro_url("checkout", "?level=" . $level->id, "https")?>"><?php _e('Renew', 'paid-memberships-pro' );?>
                                </a>
                            <?php
                        } else {
                            ?>
                                <a class="pmpro_btn disabled btn btn-trans-blue txt-xs no-m-b" href="<?php echo pmpro_url("account")?>">
                                    <?php _e('Your&nbsp;Level', 'paid-memberships-pro' );?>
                                </a>
                            <?php
                        }
                    ?>

                <?php } ?>
                </div>
            </div>
        </div>
    <?php
        }
    ?>
</section>

<nav id="nav-below" class="navigation margin-t-40" role="navigation">
	<div class="nav-previous alignleft">
		<?php if(!empty($current_user->membership_level->ID)) { ?>
			<a href="<?php echo pmpro_url("account")?>" id="pmpro_levels-return-account" class="btn btn-blue txt-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="padding-l-10">
                    <?php _e('Return to Your Account', 'paid-memberships-pro' );?>
                </span>
            </a>
		<?php } else { ?>
			<a href="<?php echo home_url()?>" id="pmpro_levels-return-home" class="btn btn-blue txt-sm">
			    
                <i class="fa fa-arrow-left"></i>
                <span class="padding-l-10">
                    <?php _e('Return to Home', 'paid-memberships-pro' );?>
                </span>
            </a>
		<?php } ?>
	</div>
</nav>
