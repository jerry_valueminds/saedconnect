<?php /*Template Name: User Reg. Successful*/ ?>

<?php get_header() ?>
    
    <main class="main-content">
       <script>
            /* Get Previous Page Coookie */
            /*var prevPage = Cookies.get('previous-page');*/

            /* Clear Cookie */
            /*Cookies.remove('previous-page');*/

            /* If Cookie exists, Goto URL Pointed by Cookie */
            /*if(prevPage){
                prevPage += '?confirmation=Your account has been successfully registered.<br>Check your inbox for an activation email to activate your account.<br>If you did not receive the email in your inbox, kindly check your Spam/Junk folder.'
                window.location.href = prevPage;
            }*/
        </script>
       
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="container-wrapper padding-o-80 text-center">
            <!--<article>
                <?php the_content() ?>
            </article>-->
            <h1 class="margin-b-20 margin-b-30 txt-2em txt-medium">
                Your account has been successfully registered.
            </h1>
            <p>
                Check your <span class="txt-bold">Inbox</span> for an activation email and follow the instructions to activate your account.
            </p>
            <p>
                If you did not receive the email in your inbox, kindly check your <span class="txt-bold">Spam/Junk folder</span>.
            </p>
            <div class="margin-t-40">
                <a href="#" class="btn btn-trans-blue" onclick="goBack()">
                    <i class="fa fa-arrow-left"></i>
                    <span class="padding-l-10">
                        Go Back
                    </span>
                </a>
                <a href="<?php echo get_site_url() ?>" class="btn btn-blue">
                    <i class="fa fa-home"></i>
                    <span class="padding-l-10">
                        Go to Homepage
                    </span>
                </a>
            </div>
        </div>
        <?php endwhile; // end of the loop. ?>
    </main>

<?php get_footer() ?>