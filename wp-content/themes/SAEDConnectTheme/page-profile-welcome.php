<?php /*Template Name: Profile - Welcome*/ ?>
   
<?php

    if ( !is_user_logged_in() ) {
        // If User is Logged in, redirect to User Dashbord
        $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID

        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }

?>

<?php get_header() ?>


<?php
    /* User is Logged in */

    /* Select Page View Request */
    if(isset($_GET['action'])){
        $page_type = $_GET['action']; 
    } else {
        $page_type = '';
    }

    /* Get Base URL */
    $base_url = get_site_url().'/my-dashboard';

    /* Get User */
    $current_user = wp_get_current_user();

    /* Post Type */
    $postType = 'validation-program';

    /* Publication */
    $publication_key   = 'publication_status';

?>

<?php //get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
   
   
<?php
    /* Get current user */
    $current_user = wp_get_current_user();

    /* Get Avatar */
    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
    $meta_key = 'user_avatar_url';
    $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

    if($get_avatar_url){
        $avatar_url = $get_avatar_url;
    }

    /* Get User Display Name */
    switch_to_blog(1);

    $gf_id = 4; //Form ID
    $gv_id = 1385; //Gravity View ID
    $title = 'Profile';

    $entry_count = 0;

    /* GF Search Criteria */
    $search_criteria = array(

    'field_filters' => array( //which fields to search

        array(

            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
            )
        )
    );

    /* Get Entries */
    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

    /* Get GF Entry Count */
    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

    if($entry_count){ //If no entry
        foreach( $entries as $entry ){          
            $displayname = rgar( $entry, '4.3' );
        }                
    }                    
?>
<?php 
    /* Get Image Field */
    function get_the_image_url($post_id){
        $images = get_attached_media( 'image', $post_id );
        $previousImg_id = 0;
        if($images){
            foreach($images as $image) { 
                $previousImg_id = $image->ID;
            }
            return wp_get_attachment_url($previousImg_id,"full");
        } else {
            return 0;
        }
    }
?>
<main class="main-content bg-ghostwhite txt-color-light" style="margin-top: 70px;background-color: #e0e0e0">
    <div class="container-wrapper padding-b-60">
        <div class="padding-lr-40 margin-b-40">
            <div class="flex_1 d-flex align-items-center padding-t-80">
                <div class="col-auto text-center">
                    <figure class="user-avatar" style="background-image: url('<?php echo $avatar_url ?>')"></figure>
                </div>
                <h2 class="col txt-xlg txt-height-1-2 txt-color-dark padding-tb-20 padding-lr-30">
                    <span class="txt-light">Welcome,</span>
                    <span class="txt-bold"><?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?></span>
                </h2>
            </div>
        </div>
        <?php $check = array(); ?>
        <?php switch_to_blog(109); ?>
        
        <?php
            $profiles_array = array('personal-info', 'cover-letter');
        
            foreach( $profiles_array as $profile_type ){
                /* Cover Letter */
                $profile_query = new WP_Query();
                $profile_query->query( 
                    array(
                        'post_type' => $profile_type,
                        'post_status' => 'publish',
                        'author' => $current_user->ID,
                        'posts_per_page' => -1,
                    ) 
                );

                if ( $profile_query->have_posts() )
                    $check[] = 'complete';
                else
                    $check[] = 'incomplete';
            }
        ?>
        
        
        <?php if (in_array("incomplete", $check)){ ?>
        
        <!-- Alert -->
        
        
        <?php } ?>
        <?php restore_current_blog(); ?>
        
        <div class="bg-yellow padding-t-30 padding-lr-40 margin-b-20">
            <div class="flex_1 row row-10 align-items-center">
                <figure class="col-12 col-lg-auto padding-lr-10 padding-b-30 text-center">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/warning.png" alt="">
                </figure>
                <div class="col-12 col-lg padding-lr-10 padding-b-30 txt-height-1-2 txt-color-dark padding-l-20 text-center text-lg-left ">
                    <h3 class="txt-lg txt-bold txt-light">You have not completed your competency profile.</h3>
                    <p>You need to complete your competency profile to get notified of Jobs that match your skills</p>
                </div>
                <figure class="col-12 col-lg-auto padding-lr-10 padding-b-30 text-center">
                    <a href="https://www.saedconnect.org/competency-profile/create-profile/" class="btn btn-blue txt-medium txt-sm no-m-b">Get Started</a>
                </figure>
            </div>
        </div>

        <!-- Updates -->
        <div class="margin-b-20">
            <div class="flex_1 row">
                <h3 class="bg-ash col-12 col-lg-1 txt-lg txt-medium txt-color-white text-center padding-tb-20 padding-lr-30">
                    Updates
                </h3>
                <div class="bg-white col-12 col-lg-11 txt-color-dark padding-tb-20 padding-tb-20 padding-lr-30">
                    <div class="dashboard-updates-slider overflow-hidden">
                        <section class="swiper-wrapper">
                            <span class="swiper-slide">
                                <p class="txt-normal-s">There are currently no updates</p>
                            </span>
                        </section>
                    </div>
                </div>
            </div>
        </div>

        <!-- Skill/work experience -->
        <div class="bg-white padding-t-30 padding-b-15 padding-lr-30 margin-b-20">
            <a 
                class="flex_1 row row-10 align-items-center"
                data-toggle="collapse" href="#dashboardCollapseSkills" role="button" aria-expanded="true" aria-controls="dashboardCollapseSkills"
            >
                <figure class="col-12 col-lg-auto text-center padding-lr-10 padding-b-10">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home-tools.png" alt="Icon" width="25">
                </figure>
                <div class="col-12 col-lg txt-height-1-2 txt-color-dark padding-lr-10 padding-b-10">
                    <h3 class="txt-lg txt-bold txt-light text-center text-lg-left">Build your portfolio</h3>
                </div>
            </a>
            <div class="collapse show hideInMobile" id="dashboardCollapseSkills">
                <div class="margin-t-10">
                    <p class="txt-normal-s margin-b-20 text-center text-lg-left">
                        Employers use your work experience to determine whether or not to hire you. Every experience counts - formal or informal. Add any new experience you just got, now, to increase your chance of being spotted by an employer.
                    </p>
                    <div class="text-center text-lg-left">
                        <span class="btn btn-blue txt-medium txt-sm margin-r-10">Coming soon</span>
                        <!--<a href="https://www.saedconnect.org/competency-profile/create-profile/" class="btn btn-blue txt-medium txt-sm margin-r-10">Add Experience</a>-->
                        <!--<a href="https://www.saedconnect.org/competency-profile/create-profile/" class="btn btn-blue txt-medium txt-sm">View all your experience</a>-->
                    </div>
                </div>
            </div>
        </div>

        <!-- Jobs -->
        <div class="bg-white padding-t-30 padding-b-10 padding-lr-30 margin-b-20">
            <a 
                class="flex_1 row row-10 align-items-center"
                data-toggle="collapse" href="#dashboardCollapseJobs" role="button" aria-expanded="true" aria-controls="dashboardCollapseJobs"
            >
                <figure class="col-12 col-lg-auto text-center padding-lr-10 padding-b-10">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home-work.png" alt="Icon" width="25">
                </figure>
                <div class="col-12 col-lg txt-height-1-2 txt-color-dark padding-lr-10 padding-b-10">
                    <h3 class="txt-lg txt-bold txt-light text-center text-lg-left">My Jobs</h3>
                </div>
            </a>
            <div class="collapse show hideInMobile" id="dashboardCollapseJobs">
                <div class="margin-t-10">
                    <div class="row row-15">
                        <?php switch_to_blog(101); ?>
                        <div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex">
                            <a href="https://www.saedconnect.org/competency-profile/profile/" class="dashboard-grey-card bg-grey d-flex flex-column justify-content-between padding-o-20">
                                <div class="d-flex padding-b-20 margin-b-60 border-b-2 border-color-white">
                                    <div class="col-3 txt-xlg txt-medium txt-color-white padding-tb-20 padding-lr-10 rounded text-center" style="background-color: #FF5E00">
                                        <?php 
                                            /*global $wpdb; //Include WP Global Object
                                            $application_db = new wpdb('root','umMv65ekyMRxfNfm','applications_db','localhost');
                                        
                                            $applicant_count = $application_db->get_var( "SELECT COUNT(*) FROM applications WHERE user_id=".$current_user->ID." AND (post_type='job' AND status='unsorted') LIMIT 0,10" );
                                        
                                            echo $applicant_count;*/
                                        ?>
                                        0
                                    </div>
                                    <div class="col-9 padding-l-20">
                                        <p class="txt-normal-s txt-bold txt-color-dark">Ongoing Job Applications</p>
                                    </div>
                                </div>
                                <p class="txt-xs txt-color-dark txt-italics">
                                    Have some new skill, education or experience? Update your Competency Profile so you never miss out on the best jobs
                                </p>
                            </a>
                        </div>
                        <!--<div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex">
                            <div class="dashboard-grey-card bg-grey d-flex flex-column justify-content-between padding-o-20">
                                <div class="d-flex padding-b-20 margin-b-60 border-b-2 border-color-white">
                                    <div class="col-3 txt-xxlg txt-medium txt-color-white padding-o-20 rounded text-center" style="background-color: #FFAA00">
                                        4
                                    </div>
                                    <div class="col-9 padding-l-20">
                                        <p class="txt-normal-s txt-bold txt-color-dark">Job pools you are registered with</p>
                                    </div>
                                </div>
                                <div>
                                    <p class="txt-sm txt-color-dark padding-b-15 txt-italics">
                                        Job pools are a faster way to get jobs in specific roles.
                                    </p>
                                    <p class="txt-sm txt-medium">
                                        <a href="" class="txt-color-dark"><u>Learn about Job Pools</u></a>
                                    </p>
                                </div>
                            </div>
                        </div>-->
                        <div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex flex-column">
                            <a href="#" class="dashboard-grey-card bg-grey margin-b-20 d-flex">
                                <div class="d-flex">
                                    <div class="col-3 txt-xlg txt-medium txt-color-white padding-tb-10 padding-lr-15 d-flex align-items-center justify-content-center" style="background-color: #17C4E9">
                                        <?php
                                            
                                            /*$custom_query = new WP_Query();
                                            $custom_query->query( 
                                                array(
                                                    'post_type' => 'job',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                    array(
                                                        'key' => 'job-type',
                                                        'value' => 'fullTime',
                                                    )
                                                ),
                                                ) 
                                            );

                                            if ( $custom_query->have_posts() ) {
                                                echo $custom_query->found_posts;
                                            } else {
                                                echo 0;
                                            }*/
                                        
                                        ?>
                                        0
                                    </div>
                                            
                                    <div class="col-9 padding-o-15 d-flex align-items-center">
                                        <article class="flex_1">
                                            <p class="txt-sm txt-medium txt-color-dark padding-b-5">Full-time Jobs</p>
                                            <p class="txt-xs txt-color-dark txt-height-1-4">Full-time Jobs that match your competencies</p>
                                        </article>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="dashboard-grey-card bg-grey margin-b-20 d-flex">
                                <div class="d-flex">
                                    <div class="col-3 txt-xlg txt-medium txt-color-white padding-tb-10 padding-lr-15 d-flex align-items-center justify-content-center" style="background-color: #17E9C5">
                                        <?php
                                            
                                            /*$custom_query = new WP_Query();
                                            $custom_query->query( 
                                                array(
                                                    'post_type' => 'job',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                    array(
                                                        'key' => 'job-type',
                                                        'value' => 'freelancer',
                                                    )
                                                ),
                                                ) 
                                            );

                                            if ( $custom_query->have_posts() ) {
                                                echo $custom_query->found_posts;
                                            } else {
                                                echo 0;
                                            }*/
                                        
                                        ?>
                                        0
                                    </div>
                                    <div class="col-9 padding-o-15 d-flex align-items-center">
                                        <article class="flex_1">
                                            <p class="txt-sm txt-medium txt-color-dark padding-b-5">Freelance Jobs</p>
                                            <p class="txt-xs txt-color-dark txt-height-1-4">Freelance Jobs that match your competencies</p>
                                        </article>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="dashboard-grey-card bg-grey d-flex">
                                <div class="d-flex">
                                    <div class="col-3 txt-xlg txt-medium txt-color-white padding-tb-10 padding-lr-15 d-flex align-items-center justify-content-center" style="background-color: #17E972">
                                        <?php
                                            
                                            /*$custom_query = new WP_Query();
                                            $custom_query->query( 
                                                array(
                                                    'post_type' => 'job',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                    array(
                                                        'key' => 'job-type',
                                                        'value' => 'trainingJob',
                                                    )
                                                ),
                                                ) 
                                            );

                                            if ( $custom_query->have_posts() ) {
                                                echo $custom_query->found_posts;
                                            } else {
                                                echo 0;
                                            }*/
                                        
                                        ?>
                                        0
                                    </div>
                                    <div class="col-9 padding-o-15 d-flex align-items-center">
                                        <article class="flex_1">
                                            <p class="txt-sm txt-medium txt-color-dark padding-b-5">Training Jobs</p>
                                            <p class="txt-xs txt-color-dark txt-height-1-4">Training Jobs that match your competencies</p>
                                        </article>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex flex-column">
                           
                            <!--<a href="#" class="dashboard-grey-card bg-grey margin-b-20 d-flex">
                                <div class="d-flex">
                                    <div class="col-3 txt-xxlg txt-medium txt-color-white padding-tb-15 padding-lr-20 d-flex align-items-center justify-content-center" style="background-color: #F49B26">
                                        0
                                    </div>
                                    <div class="col-9 padding-o-20 d-flex align-items-center">
                                        <article class="flex_1">
                                            <p class="txt-normal-s txt-medium txt-color-dark padding-b-5">Job pools</p>
                                            <p class="txt-sm txt-color-dark txt-height-1-4">Influencer Jobs that match your profile</p>
                                        </article>
                                    </div>
                                </div>
                            </a>-->
                            <a href="#" class="dashboard-grey-card bg-grey margin-b-20 d-flex">
                                <div class="d-flex">
                                    <div class="col-3 txt-xlg txt-medium txt-color-white padding-tb-10 padding-lr-15 d-flex align-items-center justify-content-center" style="background-color: #F4C026">
                                        <?php
                                            
                                            /*$custom_query = new WP_Query();
                                            $custom_query->query( 
                                                array(
                                                    'post_type' => 'job',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                    array(
                                                        'key' => 'job-type',
                                                        'value' => 'distributorOpportunity',
                                                    )
                                                ),
                                                ) 
                                            );

                                            if ( $custom_query->have_posts() ) {
                                                echo $custom_query->found_posts;
                                            } else {
                                                echo 0;
                                            }*/
                                        
                                        ?>
                                        0
                                    </div>
                                    <div class="col-9 padding-o-15 d-flex align-items-center">
                                        <article class="flex_1">
                                            <p class="txt-sm txt-medium txt-color-dark padding-b-5">Distributor Opportunities</p>
                                            <p class="txt-xs txt-color-dark txt-height-1-4">Distributor Opportunities that match your preference</p>
                                        </article>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="dashboard-grey-card bg-grey margin-b-20 d-flex">
                                <div class="d-flex">
                                    <div class="col-3 txt-xlg txt-medium txt-color-white padding-tb-10 padding-lr-15 d-flex align-items-center justify-content-center" style="background-color: #F4DF26">
                                        <?php
                                            
                                            /*$custom_query = new WP_Query();
                                            $custom_query->query( 
                                                array(
                                                    'post_type' => 'job',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                    array(
                                                        'key' => 'job-type',
                                                        'value' => 'dataCollection',
                                                    )
                                                ),
                                                ) 
                                            );

                                            if ( $custom_query->have_posts() ) {
                                                echo $custom_query->found_posts;
                                            } else {
                                                echo 0;
                                            }*/
                                        
                                        ?>
                                        0
                                    </div>
                                    <div class="col-9 padding-o-15 d-flex align-items-center">
                                        <article class="flex_1">
                                            <p class="txt-sm txt-medium txt-color-dark padding-b-5">Data Collection Opportunities</p>
                                            <p class="txt-xs txt-color-dark txt-height-1-4">Data Collection Opportunities that match your preference</p>
                                        </article>
                                    </div>
                                </div>
                            </a>
                            <div class="flex_1"></div>
                        </div>
                        <?php restore_current_blog(); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- My Business -->
        <div class="bg-white padding-t-30 padding-b-10 padding-lr-30 margin-b-20">
            <a 
                class="flex_1 row row-10 align-items-center"
                data-toggle="collapse" href="#dashboardCollapseBusiness" role="button" aria-expanded="true" aria-controls="dashboardCollapseBusiness"
            >
                <figure class="col-12 col-lg-auto text-center padding-lr-10 padding-b-10">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home-warehouse.png" alt="Icon" width="25">
                </figure>
                <div class="col-12 col-lg txt-height-1-2 txt-color-dark padding-lr-10 padding-b-10">
                    <h3 class="txt-lg txt-bold txt-light text-center text-lg-left">My Business</h3>
                </div>
            </a>
            <div class="collapse show hideInMobile" id="dashboardCollapseBusiness">
                <div class="margin-t-5">
                    <p class="txt-normal-s margin-b-20 text-center text-lg-left">
                        Your competency profile is not complete yet. You need to complete your competency profile to get notified of Jobs that match your skills
                    </p>
                    <div class="row row-15">
                        <div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex">
                            <a href="https://www.saedconnect.org/ventures-directory/my-businesses/" class="dashboard-grey-card bg-grey d-flex flex-column justify-content-between padding-o-20">
                                <div class="d-flex padding-b-20 margin-b-60 border-b-2 border-color-white">
                                    <div class="col-3 txt-xlg txt-medium txt-color-white text-center padding-o-20 rounded" style="background-color: #FF5E00">
                                        <?php
                                            switch_to_blog(103);
                                        
                                            $custom_query = new WP_Query();
                                            $custom_query->query( 
                                                array(
                                                    'post_type' => 'business',
                                                    'post_status' => 'publish',
                                                    'author' => $current_user->ID,
                                                    'posts_per_page' => -1,
                                                ) 
                                            );

                                            if ( $custom_query->have_posts() ) {
                                                echo $custom_query->found_posts;
                                            } else {
                                                echo 0;
                                            }
                                        
                                            restore_current_blog();
                                        ?>
                                    </div>
                                    <div class="col-9 padding-l-20">
                                        <p class="txt-normal-s txt-bold txt-color-dark">Number of businesses you have set up</p>
                                    </div>
                                </div>
                                <p class="txt-xs txt-color-dark txt-italics">
                                    Adding your business to the venture directory enables us to connect you to customers, partners & investors interested in businesses like yours.
                                </p>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex">
                            <a href="https://www.saedconnect.org/opportunity-center/my-programs/" class="dashboard-grey-card bg-grey padding-o-20">
                                <div class="d-flex padding-b-20 margin-b-60 border-b-2 border-color-white">
                                    <div class="col-3 txt-xlg txt-medium txt-color-white text-center padding-o-20 rounded" style="background-color: #FFAA00">
                                        <?php
                                            switch_to_blog(12);
                                        
                                            $custom_query = new WP_Query();
                                            $custom_query->query( 
                                                array(
                                                    'post_type' => 'opportunity',
                                                    'post_status' => 'publish',
                                                    'author' => $current_user->ID,
                                                    'posts_per_page' => 3,
                                                    'meta_query' => array(
                                                        array(
                                                            'key' => 'publication_status',
                                                            'value' => 'user_publishedx',
                                                            'compare' => 'NOT'
                                                        )
                                                    ),
                                                ) 
                                            );

                                            if ( $custom_query->have_posts() ) {
                                                echo $custom_query->found_posts;
                                            } else {
                                                echo 0;
                                            }
                                        
                                            restore_current_blog();
                                        ?>
                                    </div>
                                    <div class="col-9 padding-l-20">
                                        <p class="txt-normal-s txt-bold txt-color-dark">Opportunities that your business can benefit from</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex">
                            <div class="dashboard-grey-card bg-grey padding-o-20">
                                <h4 class="txt-medium txt-height-1-2 txt-color-dark text-center margin-b-30">The Entrepreneurship Incubator</h4>
                                <a href="https://www.saedconnect.org/tei-mentor-hub/track/track-one/" class="d-block dashboard-grey-card bg-white padding-o-15 margin-b-20">
                                    <h4 class="txt-normal-s txt-height-1-2 txt-color-dark text-center">Access the Starter Track</h4>
                                </a>
                                <a href="https://www.saedconnect.org/tei-mentor-hub/track/discover-track/" class="d-block dashboard-grey-card bg-white padding-o-15 margin-b-20">
                                    <h4 class="txt-normal-s txt-height-1-2 txt-color-dark text-center">Access the Discovery Track</h4>
                                </a>
                                <a href="https://www.saedconnect.org/tei-mentor-hub/track/validate-build-track/" class="d-block dashboard-grey-card bg-white padding-o-15 margin-b-20">
                                    <h4 class="txt-normal-s txt-height-1-2 txt-color-dark text-center">Access the Validate + Build Track</h4>
                                </a>
                            </div>
                            <?php restore_current_blog(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- My Projects -->
        <div class="bg-white padding-t-30 padding-b-10 padding-lr-30">      
            <a 
                class="flex_1 row row-10 align-items-center"
                data-toggle="collapse" href="#dashboardCollapseYLN" role="button" aria-expanded="true" aria-controls="dashboardCollapseYLN"
            >
                <figure class="col-12 col-lg-auto text-center padding-lr-10 padding-b-10">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/home-people.png" alt="Icon" width="25">
                </figure>
                <div class="col-12 col-lg txt-height-1-2 txt-color-dark padding-lr-10 padding-b-10">
                    <h3 class="txt-lg txt-bold txt-light text-center text-lg-left">My Young Leaders Network Projects</h3>
                </div>
            </a>
            <div class="collapse show hideInMobile" id="dashboardCollapseYLN">
                <div class="margin-t-5">
                    <p class="txt-normal-s margin-b-20 text-center text-lg-left">
                        Your competency profile is not complete yet. You need to complete your competency profile to get notified of Jobs that match your skills
                    </p>
                    <div class="row row-15">
                        <div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex">
                            <a href="https://www.saedconnect.org/ventures-directory/create-project/" class="dashboard-grey-card bg-grey padding-tb-30 padding-lr-20 text-center">
                                <figure class="margin-b-20">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/home/plus_blue.png" alt="">
                                </figure>
                                <p class="txt-lg txt-bold txt-color-blue margin-b-60">Add a Project</p>
                                <p class="txt-normal-s txt-color-dark">
                                    We promote projects that align with any of these xxx causes.
                                </p>
                            </a>
                        </div>
                        <?php switch_to_blog(103); ?>
                        
                        <?php
                            $custom_query = new WP_Query();
                            $custom_query->query( 
                                array(
                                    'post_type' => 'personal-project',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'publication_status',
                                            'value' => 'unpublished',
                                        )
                                    ),
                                ) 
                            );

                            if ( $custom_query->have_posts() ) {

                                while ($custom_query->have_posts()) : $custom_query->the_post();

                                /* Variables */
                                $post_id = $post->ID;    //Get Program ID

                                /* Publication status */      
                                $publication_meta = get_post_meta( $post_id, $publication_key, true );
                        ?>
                            
                            <div class="col-md-6 col-lg-3 padding-lr-15 padding-b-20 d-flex">
                                <a href="https://www.saedconnect.org/ventures-directory/create-project/?project-id=<?php echo $post_id ?>" class="dashboard-grey-card bg-grey padding-tb-30 padding-lr-20">
                                    <p class="txt-sm margin-b-5">
                                        Unpublished
                                    </p>
                                    <p class="txt-lg txt-medium txt-color-dark margin-b-60">
                                       <?php the_title() ?>
                                    </p>
                                    <figure 
                                        class="background-image bg-grey" 
                                        style="background-image: url('<?php echo get_the_image_url($post_id); ?>');height: 120px;"
                                    >

                                    </figure>
                                </a>
                            </div>
                        <?php endwhile; ?>
 
                        <?php } ?>
                        
                        <?php restore_current_blog(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>  

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>