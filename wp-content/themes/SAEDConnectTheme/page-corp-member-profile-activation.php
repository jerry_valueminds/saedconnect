    <?php /*Template Name: Corp Member Profile Activation Page*/ ?>
    
    <?php
                    
        if( !$_REQUEST['activation-code'] ) {
            global $wp_query;
            $wp_query->set_404();
            status_header( 404 );
            get_template_part( 404 ); exit();
        }

        $activation_code = $_REQUEST['activation-code'];

        $token_status = '';

        $nysc_list_db = new wpdb('root','umMv65ekyMRxfNfm','nysc_db','localhost');
        $corp_member = $nysc_list_db->get_row("SELECT call_up_number, state_code, email, verification_status FROM corp_member_profile WHERE verification_token = '".$activation_code."' LIMIT 0,1");

        if( $corp_member->verification_status == '' ){
            
            global $wp_query;
            $wp_query->set_404();
            status_header( 404 );
            get_template_part( 404 ); exit();
            $token_status = 'no_request';
            
        }
        elseif( $corp_member->verification_status == 'pending' ){
            
            /* Update Verification status to Verified */
            $nysc_list_db->update( 
                'corp_member_profile', //Table
                array("verification_status" => "verified"), //Data
                array("call_up_number" => $corp_member->call_up_number), //Where?
                array("%s"), //Data format
                array("%s") //Where format
            );
            
            $token_status = 'verified';
            
            /* Update Mailing List */
            /* Data */
            $api_key = '0fkxtL9crlJFX7cqCXUV';
            $master_list = 'py763E5sO36V5fa9WGjoUK4Q'; //Master
            $email = $corp_member->email;                                   

            /* Endpoints */
            $subscribe_url = 'http://maylbox.com/subscribe';

            /* Subscribe Body */
            $body = array(
                'Call-upNumber' => $corp_member->call_up_number,
                'Statecode' => $corp_member->state_code,
                'email' => $email,
                'list' => $master_list,
                'boolean' => 'true'
            );

            $args = array(
                'body' => $body,
                'timeout' => '5',
                'redirection' => '5',
                'httpversion' => '1.0',
                'blocking' => true,
                'headers' => array(),
                'cookies' => array()
            );

            $response = wp_remote_post( $subscribe_url, $args );
            $http_code = wp_remote_retrieve_response_code( $response );
            $body = wp_remote_retrieve_body( $response );
            /* Update Mailing List End */
            
        }
        elseif( $corp_member->verification_status == 'verified' ){
            
            $token_status = 'previously_verified';
            
        }
    
    ?>
    
    <?php get_header() ?>
    
    <!-- // If User is not Logged in, Load Login Page -->
    <main class="main-content">
        <section class="container-wrapper padding-t-40 padding-b-80">
            <div class="row">
                <div class="col-md-6 mx-auto text-center txt-height-1-5">                   
                    <?php if ($token_status == 'no_request'){ ?>
                       
                        <header class="margin-b-40">
                            <h1 class="txt-xlg txt-bold">
                                Your Corp Member Profile has been successfully Activated
                            </h1>
                        </header> 
                         
                    <?php } elseif ($token_status == 'verified'){ ?>
                    
                        <header class="margin-b-40">
                            <h1 class="txt-xlg txt-bold">
                                Your Corp Member Profile has been successfully Activated
                            </h1>
                        </header>
                    
                    <?php } elseif ($token_status == 'previously_verified'){ ?>
                       
                       <header class="margin-b-40">
                            <h1 class="txt-xlg txt-bold padding-b-20">
                                Your Corp Member Profile has already been Activated
                            </h1>
                            <p class="txt-normal-s txt-medium">
                                If this is your Call-up Number and you are not the one who requested the pending verification request, kindly contact the SAEDConnect
                                <a class="txt-color-blue" href="">Help Center</a>
                            </p>
                        </header>
                        
                    <?php } ?>
                    
                    <div class="margin-t-40">
                        <a href="https://www.saedconnect.org/nysc-saed-profile/" class="btn btn-blue">
                            <i class="fa fa-home"></i>
                            <span class="padding-l-10">
                                Go to Your Dashboard
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    
    <?php get_footer() ?>