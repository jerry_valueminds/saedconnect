<?php get_header() ?>
    
<?php
    $current_user = wp_get_current_user();
    $gv_id = 6;
    $entry_id = 0;
    
    // TO SHOW THE POST CONTENT
    while ( have_posts() ) : the_post();

        $post_id = get_the_ID();
        $post_author_id = get_the_author_meta('ID');

         /* Get current User ID */
        $current_user = wp_get_current_user();

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                'key' => '2', 'value' => $post_id, //Current logged in user
            )
          )
        );

        /* Get GF Entry Count */
        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

        foreach( $entries as $entry ){

            $entry_id = $entry['id'];
        }


        /*
        *
        *   Get Order count
        *
        */
        /* Get current User ID */
        $current_user = wp_get_current_user();

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => '2', 'value' => $post_id->ID, //Current logged in user
            )
          )
        );

        /* Get GF Entry Count */
        $order_count = GFAPI::count_entries( 9, $search_criteria );
        $orders = GFAPI::get_entries( 9, $search_criteria );
    
?>

    <main class="main-content">
        <header class="container-wrapper bg-yellow padding-tb-15 margin-b-40">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="txt-sm">
                        <a href="marketplace_service_home.html" class="txt-color-dark padding-r-10">
                            Service Directory
                        </a>
                        <i class="fa fa-angle-right padding-r-10"></i>
                        <a href="marketplace_service_category.html" class="txt-color-dark">
                            Training Offers
                        </a>
                    </h1>
                </div>
            </div>
        </header>
        
        <section class="container-wrapper">
            <div class="col-md-10 mx-auto">
                <div class="row row-10 margin-b-30">
                    <div class="col-md-8 padding-lr-10">
                        <h2 class="txt-2em txt-medium">
                            <?php the_title() ?>
                        </h2>
                        <?php if( $post_author_id == $current_user->id ){ ?>
                            <p class="txt-sm margin-tb-15">
                                <a
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry_id.'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=marketplace&form-title='.$form_title.'&post_id='.$post_id; ?>"
                                    class="txt-color-green"
                                >
                                    Edit
                                </a>
                                <span class="padding-lr-5">|</span>
                                <a
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=marketplace&return-view=offers';  ?>" 
                                    class="txt-color-red confirm-delete"
                                >
                                    Delete
                                </a>
                            </p>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 text-md-right padding-lr-10">
                        <a href="https://www.saedconnect.org/service-provider-directory/my-training-offers/?view=form&form-title=Add Training Offer"  class="btn btn-blue no-m-b txt-xs">
                            Create Training Offer
                        </a>
                    </div>
                </div>
                <div class="row row-15 padding-b-40 margin-b-40 border-b-1 border-color-darkgrey">
                    <div class="col-md-7 padding-lr-15 padding-b-15">
                        <?php
                            $meta = rgar( $entry, '25' );

                            if($meta){
                                $meta = str_ireplace( 'http:', 'https:', $meta );
                        ?>
                        <div class="margin-b-40">
                            <img src="<?php echo $meta; ?>" alt="">
                        </div>

                        <?php } ?>
                        <article class="margin-b-20">
                            <h3 class="txt-lg txt-medium txt-color-dark margin-b-20">
                                Details
                            </h3>
                            <article class="text-box txt-normal-s txt-height-1-4">
                                <?php echo rgar( $entry, 19 ) ?>
                            </article>
                        </article>
                        <article class="margin-b-20">
                            <h3 class="txt-lg txt-medium txt-color-dark margin-b-20">
                                Curriculum
                            </h3>
                            <article class="text-box txt-normal-s txt-height-1-4">
                                <?php echo rgar( $entry, 20 ) ?>
                            </article>
                        </article>
                        <article class="">
                            <h3 class="txt-lg txt-medium txt-color-dark margin-b-20">
                                Perks
                            </h3>
                            <article class="text-box txt-normal-s txt-height-1-4">
                                <?php echo rgar( $entry, 21 ) ?>
                            </article>
                        </article>
                    </div>
                    <div class="col-md-5 padding-lr-15">
                        <div class="border-o-1 border-color-darkgrey padding-o-30 margin-b-20">
                            <h2 class=" margin-b-15">
                                <span class="txt-xxlg txt-medium">
                                    <?php echo rgar( $entry, 27 ) ?>
                                </span> 
                                per order
                            </h2> 
                            <div class="txt-normal-s txt-medium margin-b-20">
                                <i class="fa fa-clock-o padding-r-5"></i>
                                <?php

                                    $meta_1 = rgar( $entry, 23 );
                                    $meta_2 = rgar( $entry, 24 );

                                    if($meta_1 && $meta_2){
                                        echo $meta_1.' '.$meta_2.' Training';
                                    }
                                ?>
                            </div>
                            <p class="txt-normal-s">
                                Contact seller to negotiate a price point and subscribe for this service
                            </p>
                            <div class="padding-t-20 d-flex align-items-center">
                                <a
                                    href="https://www.saedconnect.org/service-provider-directory/training-offer-response/?parent_id=<?php echo $post_id ?>"  
                                    class="btn btn-blue txt-sm no-m-b"
                                >
                                    Order Now
                                </a>
                                <?php if( $order_count ){ ?>
                                    <span class="padding-l-10">
                                        (<?php echo $order_count; echo ($order_count > 1)? ' Orders' : ' Order';  ?>)
                                    </span>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                            // Create Query Argument
                            $args = array(
                                'author' => $post_author_id,
                                'post_type' => 'freelancer',
                                'showposts' => 1,
                                'meta_query' => $meta_array,
                            );


                            $freelancer_query = new WP_Query($args);

                            while ($freelancer_query->have_posts()) : $freelancer_query->the_post();

                            /* Get Post ID */
                            $post_id = $post->ID 
                        ?>
                            <div class="border-o-1 border-color-darkgrey padding-o-30 margin-b-40">
                                <div class="d-flex">
                                    <figure class="margin-b-20 padding-r-20">
                                        <?php
                                            $field = 'freelancer_image';

                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                        ?>

                                            <img class="profile-image" src="<?php echo $meta; ?>" alt="" width="80"> 

                                        <?php } else { ?>
                                           
                                            <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="" width="80">
                                            
                                        <?php } ?>
                                    </figure>
                                    
                                    <div class="flex_1">
                                        <h2 class="txt-medium margin-b-10">
                                            <a href="marketplace_single_freelancer.html">
                                                <?php the_title() ?>
                                            </a> 
                                        </h2>
                                        <p class="txt-sm txt-height-1-7">
                                            <?php
                                                $field = 'freelancer_summary';

                                                $meta = get_post_meta($post->ID, $field, true);

                                                if($meta){
                                                    echo $meta;
                                                }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="border-b-1 border-color-darkgrey padding-b-15 margin-b-15">
                                    <h3 class="txt-sm txt-bold margin-b-10">
                                        Skills
                                    </h3>
                                    <p class="txt-xs">
                                        <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                            Web Design
                                        </a>
                                        <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                            Graphics Design
                                        </a>
                                        <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                            Fashion Design
                                        </a>
                                    </p>
                                </div>
                                <div class="">
                                    <h3 class="txt-sm txt-bold margin-b-10">
                                        About me
                                    </h3>
                                    <p class="txt-sm txt-height-1-7">
                                        <?php
                                            $field = 'freelancer_about';

                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                                echo $meta;
                                            }
                                        ?>
                                    </p>
                                </div>
                            </div>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <!--<article class="margin-b-80">
                    <header class="margin-b-40">
                        <h3 class="txt-lg txt-medium margin-b-20">
                            Frequentky Asked Questions
                        </h3>
                    </header>
                    <div class="d-none d-md-flex row row-10">
                        <?php
                            
                            for( $i = 1; $i <=8; $i++ ) {
                                
                                $workplan_title = get_post_meta($post->ID, 'offer_workplan_title_'.$i, true);
                                $workplan_content = get_post_meta($post->ID, 'offer_workplan_content_'.$i, true);
                                $workplan_duration = get_post_meta($post->ID, 'offer_workplan_duration_'.$i, true);
                                
                                if( $workplan_title || $workplan_content || $workplan_duration ){
                        ?>
                        
                        <div class="col-md-3 d-flex padding-lr-10 padding-b-30">
                            <article class="padding-o-20 border-o-1 border-color-darkgrey full-width">
                                <h6 class="txt-bold margin-b-10">
                                    <?php echo $workplan_title ?>
                                </h6>
                                <h5 class="txt-sm margin-b-10 txt-height-1-1">
                                    <?php echo $workplan_content ?>
                                </h5>
                                <div class="txt-normal-s txt-medium">
                                    <i class="fa fa-clock-o"></i>
                                    <?php echo $workplan_duration ?>
                                </div>
                            </article>
                        </div>
                        
                        <?php 
                                }
                            } 
                        ?>
                    </div>
                </article>-->
                
                <?php if( $post_author_id == $current_user->id ){ ?>
                <!--<article class="margin-b-80">
                    <header class="margin-b-40">
                        <h3 class="txt-lg txt-medium margin-b-20">
                            Orders
                        </h3>
                    </header>
                    <?php foreach( $orders as $order ){ ?>

                    <div class="margin-b-20 padding-b-20 border-b-1 border-color-darkgrey">
                        <div class="txt-normal-s txt-medium margin-b-10">
                            <?php echo rgar( $order, '1' ); ?>
                        </div>
                        <div class="txt-sm txt-color-lighter margin-b-10">
                            by <?php echo get_the_author_meta('nickname', $order['created_by']); ?> -
                            <?php echo human_time_diff( strtotime($order['date_created']), current_time('timestamp', 1) ) . ' ago'; ?>
                        </div>
                        <p class="txt-sm txt-height-1-7">
                            <?php echo rgar( $order, '3' ) ?>
                        </p>
                    </div>
                    
                    <?php } ?>
                </article>-->
                <?php } ?>
            </div>
        </section>
    </main>
    
<?php
    endwhile; //resetting the page loop
?>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<?php get_footer() ?>