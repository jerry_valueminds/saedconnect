    <?php /*Template Name: Homepage-YNL*/ ?>
    
    <?php get_header() ?>
    
    <style>
        .ynl-home-banner {
            display: flex;
            align-items: flex-start;
            padding-top: 60px;
            position: relative;
            background-position: bottom;
            background-repeat: no-repeat;
            background-size: contain;
            min-height: 480px !important;
        }
        
        .ynl-home-banner.image:after {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255, 255, 255, 0);
            z-index: 0;
            transition: all 0.4s ease;
        }
        
        .ynl-home-small-bg:before {
            height: 180px;
        }
        
        /*GF*/
        #field_4_32>.gfield_label{
            display: none !important;
        }
        
        .gfield_label {
            font-size: 0.8em !important;
            font-weight: 400 !important;
            margin-bottom: 0.4em;
            font-weight: 400 !important;
        }
    </style>
    
    <main class="main-content">
        <header class="ynl-home-banner lg image" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/hero_ynl.png');">
            <div class="content container-wrapper">
                <div class="row">
                    <div class="col-lg-6 mx-auto text-center">
                        <div class="text-center padding-b-20">
                            <span class="bg-grey txt-normal-s padding-tb-10 padding-lr-20">
                                JOIN THE YOUNG LEADERS NETWORK                                      
                            </span>
                        </div>
                        <h1 class="txt-3em txt-bold padding-b-20">
                            Make Tomorrow Better
                        </h1>
                        <h2 class="txt-color-light txt-height-1-7">
                            Join a nationwide network of passionate young people to co-create, shape and deploy creative solutions to development challenges
                        </h2>
                        <div class="padding-t-40">
                            <!--<a class="btn btn-green txt-normal-s margin-r-10" data-toggle="modal" href="#championProgramFormModal">Enroll as a young leader</a>-->
                            <a class="btn btn-ash txt-normal-s" data-toggle="modal" href="#aboutIdeasModal">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="bg-green padding-t-80 padding-b-80">
            <div class="container-wrapper text-center">
                <h2 class="txt-2em uppercase txt-color-white padding-b-20">
                    What are you passionate about?
                </h2>
                <p class=" txt-color-white">Explore available focus programs below and add your voice and complete available tasks/projects to make change happen in that area. </p>
            </div>
        </section>
        <section class="ynl-home-small-bg padding-b-40">
            <div class="container-wrapper">
                <nav class="padding-b-40">
                    <ul class="ynl-home-category-filter">
                        <?php 
                            $filter_term = $_GET['category'];
                            
                            /* Tax Query Array */
                            if($filter_term){
                                $tax_query = array(
                                    array(
                                        'taxonomy' => 'thematic-category',
                                        'terms' => $filter_term,
                                    )
                                );
                            }
                        
                            /* Get terms */
                            $terms = get_terms( 'thematic-category', array('hide_empty' => false, 'parent' => 0) ); 
                            $counter = 1;
                        ?>
                        <li>
                            <a class="<?php echo ( empty( $filter_term ) ) ? 'active' : ''; ?>" href="<?php echo currenturl(true); ?>">All</a>
                        </li>
                        <?php foreach( $terms as $term ){ ?>
                            <?php if( $counter <= 5 ){ ?>
                            <li>
                                <a 
                                   href="<?php echo currenturl(true).'?category='.$term->term_id; ?>" 
                                   class="<?php echo ( $filter_term == $term->term_id ) ? 'active' : ''; ?>"
                                >
                                    <?php echo $term->name ?>
                                </a>
                            </li>
                            <?php } ?>
                            <?php $counter++; ?>
                        <?php } ?>
                        <li class="dropdown show">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMoreCategoriesLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                More
                            </a>
                            <div class="dropdown-menu padding-tb-15" aria-labelledby="dropdownMoreCategoriesLink">
                            <?php foreach( $terms as $term ){ ?>
                                <a class="dropdown-item padding-tb-10" href="<?php echo currenturl(true).'?category='.$term->term_id; ?>">
                                    <span class="txt-color-green txt-light"><?php echo $term->name ?></span>
                                </a>
                            <?php } ?>
                            </div>
                        </li>
                    </ul>
                </nav>
                
                <!--<div>   
                    <div class="txt-normal-s txt-medium txt-color-white padding-o-15 border-0-1 border-color-white-trans">
                       Showing
                        <?php 
                            $current_term = get_term($filter_term);
                            echo ( $filter_term ) ? $current_term->name : 'All';
                        ?>
                    </div>
                </div>-->
                
                <?php  ?>
                <div class="row row-15">
                <?php
                    $custom_query = new WP_Query();
                    $custom_query->query( 
                        array(
                            'post_type' => 'thematic-area',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'tax_query' => $tax_query
                        ) 
                    );

                    if ( $custom_query->have_posts() ) {

                        while ($custom_query->have_posts()) : $custom_query->the_post();

                        /* Variables */
                        $post_id = $post->ID;    //Get Program ID

                        $images = rwmb_meta( 'featured_image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                ?>

                    <div class="col-md-6 col-lg-4 col-xl-3 padding-lr-15 padding-b-30 d-flex">
                        <div class="ynl-home-card col p-0" style="overflow:visible">
                            <article class="content d-flex flex-column">
                                <figure class="image-box" style="background-image: url('<?php echo $image['full_url']; ?>')">
                                    <div class="category txt-xxs dropdown show">
                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMoreCategoriesLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php 
                                            $term_list = wp_get_post_terms( $post->ID, 'thematic-category', array( 'fields' => 'names' ) );
                                            $counter = 1;
                                            foreach ( $term_list as $term_item ){
                                                if( $counter < 2 ){
                                                    echo $term_item;
                                                }

                                                $counter++;
                                            } 
                                            echo '  ';
                                            echo '  +';
                                            echo count($term_list) - 1; 
                                        ?>
                                        </a>
                                        <div class="dropdown-menu padding-tb-15 txt-normal-s" aria-labelledby="dropdownMoreCategoriesLink">
                                        <?php $counter = 1; ?>
                                        <?php foreach( $term_list as $term_item ){ ?>
                                            <?php if( $counter > 1 ){ ?>
                                                <a class="dropdown-item padding-tb-10" href="">
                                                    <span class="txt-color-green"><?php echo $term_item; ?></span>
                                                </a>
                                            <?php } ?>
                                            <?php $counter++ ?>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </figure>
                                <div class="course-info flex_1 d-flex flex-column justify-content-between">
                                    <h3 class="title txt-height-1-2 padding-b-40 <?php echo ( rwmb_meta( 'status' ) == 'Published' ) ? '' : 'txt-color-lighter' ?>">
                                        <a 
                                           class="" 
                                           <?php if( rwmb_meta( 'status' ) == 'Published' ){ ?>
                                           href="<?php the_permalink() ?>"
                                           <?php } ?>
                                        >
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                    <div class="row row-10">
                                    <?php if( rwmb_meta( 'status' ) == 'Published' ){ ?>
                                        <div class="col-4 padding-lr-10">
                                            <p class="txt-sm txt-color-lighter padding-b-5">Challenges</p>
                                            <p class="txt-lg txt-color-green">
                                            <?php 
                                                $challenges_query = new WP_Query( array(
                                                    'post_type' => 'problem-area',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                        array(
                                                            'key' => 'parent_post',
                                                            'value' => $post_id,
                                                        ),
                                                    ),
                                                ) );    

                                                echo $challenges_query->found_posts;
                                            ?>
                                            </p>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <p class="txt-sm txt-color-lighter padding-b-5">Ideas</p>
                                            <p class="txt-lg txt-color-green">
                                            <?php 
                                                $idea_query = new WP_Query( array(
                                                    'post_type' => 'idea',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => -1,
                                                    'meta_query' => array(
                                                        array(
                                                            'key' => 'thematic_area',
                                                            'value' => $post_id,
                                                        ),
                                                    ),
                                                ) );   

                                                echo $idea_query->found_posts;
                                            ?>
                                            </p>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <p class="txt-sm txt-color-lighter padding-b-5">Live Projects</p>
                                            <p class="txt-lg txt-color-green">0</p>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-6 padding-lr-10 padding-b-5 txt-normal-s txt-medium txt-color-lighter">
                                            Coming soon
                                        </div>
                                        <!--<div class="col-6 text-right padding-lr-10 padding-b-5 txt-normal-s txt-medium n">
                                            <a class="txt-color-gree" data-toggle="modal" href="#iAmInterestedModal-<?php echo $post_id ?>">
                                                <u>I am interested</u>
                                            </a>
                                        </div>-->
                                    <?php } ?>
                                    </div>
                                </div>
                            </article>  
                        </div>   
                    </div>

                <?php endwhile; ?>

                <?php }else{ ?>
                    <div class="col-12 padding-lr-15 padding-t-20 padding-b-40">
                        <h2 class="txt-lg txt-medium txt-color-white text-center">
                            No Projects available.
                        </h2>
                    </div>   
                <?php } ?>
                </div>
            </div>
        </section>
        
        
        <?php


            if ( $custom_query->have_posts() ) {

                while ($custom_query->have_posts()) : $custom_query->the_post();

                /* Variables */
                $post_id = $post->ID;    //Get Program ID

                $images = rwmb_meta( 'featured_image', array( 'limit' => 1 ) );
                $image = reset( $images );
        ?>
        
        <!-- Interest Modal -->
        <div class="modal fade font-main filter-modal" id="iAmInterestedModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-interested-in-idea-'.$idea_id; ?>" method="post">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel">Interested in <?php echo the_title() ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            <?php
                                if( $_POST && $_GET['view'] == 'form-interested-in-idea-'.$idea_id ){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $telephone = sanitize_text_field( $_POST['telephone'] ); 
                                    $email = sanitize_text_field( $_POST['email'] ); 
                                    $location = sanitize_text_field( $_POST['location'] ); 

                                    /* Update Mailing List */
                                    /* Data */
                                    switch_to_blog(110);
                                    $state = get_term( $location );
                                    restore_current_blog();

                                    $api_key = '0fkxtL9crlJFX7cqCXUV';
                                    $master_list = 'CoLqq7CTYUZYKBnJx5u7633g'; //Master

                                    /* Endpoints */
                                    $subscribe_url = 'http://maylbox.com/subscribe';

                                    /* Subscribe Body */
                                    $body = array(
                                        'name' => $post_name,
                                        'email' => $email,
                                        'Phone' => $telephone,
                                        'Location' => $state->name,
                                        'ThematicArea' => get_the_title(),
                                        'list' => $master_list,
                                        'boolean' => 'true'
                                    );

                                    $args = array(
                                        'body' => $body,
                                        'timeout' => '5',
                                        'redirection' => '5',
                                        'httpversion' => '1.0',
                                        'blocking' => true,
                                        'headers' => array(),
                                        'cookies' => array()
                                    );

                                    $response = wp_remote_post( $subscribe_url, $args );
                                    $http_code = wp_remote_retrieve_response_code( $response );
                                    $body = wp_remote_retrieve_body( $response );
                                    /* Update Mailing List End */

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                }
                            ?>


                            <div class="form margin-b-40">
                                <div class="row row-10">
                                    <!-- Name -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="post_name">Name</label>
                                        <input 
                                            type="text" 
                                            name="post_name" 
                                        >
                                    </div>
                                    <!-- Telephone -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="telephone">Telephone</label>
                                        <input 
                                            type="telephone" 
                                            name="telephone" 
                                        >
                                    </div>
                                    <!-- Email -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="email">Email</label>
                                        <input 
                                            type="email" 
                                            name="email" 
                                        >
                                    </div>
                                    <!-- Location -->
                                    <div class="col-lg-12 padding-lr-10 form-item">
                                        <label for="location">Location</label>
                                        <select name="location" id="location">
                                        <?php 
                                            switch_to_blog(110);
                                            $terms = get_terms( 'state', array('hide_empty' => false));
                                            restore_current_blog();

                                            foreach( $terms as $term ){
                                        ?>
                                            <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-green txt-sm padding-lr-15">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <?php endwhile; ?>
        
         <?php } ?>
        
        
        
        <section class="container-wrapper padding-t-40 padding-t-30">
            <header class="txt-center margin-b-20">
                <h2 class="txt-2em uppercase text-center">Different Ways to engage</h2>    
            </header>
        </section>
        <section class="bg-ash txt-color-white padding-tb-40">
            <div class="container-wrapper">
                <div class="row row-20">
                    <div class="col-lg-4 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/submit idea.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Join the Network</h2> 
                        <p class="txt-sm">Enroll as a young leader and automatically get first hand notifications about opportunities to collaborate with  strategic partners to co-create, shape and deploy creative solutions to development challenges, nationwide.</p>  
                    </div>
                    <div class="col-lg-4 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/submit an idea.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Add your Voice</h2> 
                        <p class="txt-sm">share your experiences, give your ideas and suggestions on ways we can address identified challenges. Development partners will use these inputs to shape projects and might be particularly interested in working with you to bring your ideas to life.</p>  
                    </div>
                    <div class="col-lg-4 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/participate in a project.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Participate in a Task or Project</h2>   
                        <p class="txt-sm">When informed about open opportunities, (as all registered young leaders will be) join other young leaders to attend events, complete tasks or contribute to project implementation in any way you feel you have the capacity to.</p> 
                    </div>
                    <!--<div class="col-lg-3 padding-o-40 padding-b-20">
                        <figure class="padding-b-30">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Support a project.png" alt="" height="80">
                        </figure>
                        <h2 class="txt-medium padding-b-20">Support a Project</h2>  
                        <p class="txt-sm">You can support the implementation of a project an an individual, Government, Private sector company, NGO or development agency by providing expertise, donating resources or funding to make change happen at the grass roots, nationwide.</p>  
                    </div>-->
                </div>
            </div>
        </section>
        
        <!-- Partner -->
        <section class="bg-green padding-t-80 padding-b-40">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <header class="text-center padding-b-60">
                            <h3 class="txt-lg uppercase txt-color-white padding-b-20">Become a Partner</h3>
                            <h4 class="txt-2em txt-bold txt-height-1-2 txt-color-white">Join forces with us to help make an impact in communities across Nigeria. </h4>
                        </header>
                        <div class="ynl-partner-cards row-20">
                            <div class="col-lg-6 padding-lr-20 padding-b-40 ">
                                <div class="ynl-partner-card">
                                    <div class="bg-white padding-o-40">
                                        <div class="text-center">
                                            <h4 class="txt-xxlg txt-bold txt-color-green padding-b-20">Support a program/<br>project</h4>
                                            <p class="txt-normal-s padding-b-20">
                                                We welcome well meaning individual experts, Government, Private sector companies, NGO, Civil society organisations and development agencies to join us in tackling the world’s toughest challenges. You can support the YLN in several ways including
                                            </p>
                                            <p>
                                                <a class="txt-color-green txt-medium" data-toggle="collapse" href="#collapseBecomePartner" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                    <u>See How</u>
                                                </a>
                                            </p>
                                        </div>
                                        <div class="collapse" id="collapseBecomePartner">
                                            <article class="text-box txt-sm padding-t-20">
                                                <ul>
                                                    <li>Providing program/project development expertise</li>
                                                    <li>Donating implementation resources or funding to execute and scale any of the existing projects</li>
                                                </ul>                                        
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 padding-lr-20 padding-b-40 ">
                                <div class="ynl-partner-card">
                                    <div class="bg-white padding-o-40">
                                        <div class="text-center">
                                            <h4 class="txt-xxlg txt-bold txt-color-green padding-b-20">Co-create/co-execute your impact programs with us</h4>
                                            <p class="txt-normal-s padding-b-20">
                                                If you already have existing impact programs or projects which you would love to scale nationwide, we are very happy to work with you to bring it to life and leverage our nationwide network to scale it down to the grassroots nationwide
                                            </p>
                                            <p style="opacity: 0;">
                                                <a class="txt-color-green txt-medium" data-toggle="collapse" href="#collapseBecomePartner" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                    <u>See How</u>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $('.ynl-partner-card').mouseenter(function(){
                    $('.ynl-partner-card').addClass('shrink');
                    $(this).addClass('grow');
                });

                $('.ynl-partner-card').mouseleave(function(){
                    $('.ynl-partner-card').removeClass('shrink');
                    $('.ynl-partner-card').removeClass('grow');
                });
            </script>
        </section>
        
        <!-- Learn more -->
        <div class="modal fade font-main filter-modal" id="aboutIdeasModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-tasks'; ?>" method="post">
                        <div class="modal-header padding-lr-40">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-40">
                            <div class="row">
                                <div class="col-lg-10 mx-auto">                                
                                    <h3 class="txt-xxlg txt-bold txt-height-1-1 txt-color-green uppercase padding-b-60">
                                        CO-CREATE IMPACT AT SCALE 
                                    </h3>
                                   <figure class="d-flex align-items-center justify-content-between txt-2em padding-b-60">
                                        <div class="">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Idea.png" alt="" width="160">
                                        </div>
                                        <i class="fa fa-long-arrow-right txt-color-orange padding-lr-10"></i>
                                        <div class="">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Project.png" alt="" width="160">
                                        </div>
                                        <i class="fa fa-long-arrow-right txt-color-orange padding-lr-10"></i>
                                        <div class="">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/ynl/Impact.png" alt="" width="160">
                                        </div>
                                    </figure>
                                    <h5 class="txt-xlg txt-height-1-4 padding-b-60">
                                        We connect young leaders like you to governments, development agencies and corporate organisations to co-create, shape and deploy creative solutions to development challenges, nationwide.
                                    </h5>

                                    <article class="padding-b-40">
                                        <h5 class="txt-lg txt-bold txt-height-1-4 padding-b-20">
                                            3 steps to impact
                                        </h5>
                                        <p class="txt-normal-s txt-height-1-7">Join the brightest and the best to tackle the worlds toughest challenges in 3 steps</p>
                                    </article>

                                    <div class="padding-b-40">
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-1" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#00752F"
                                                >
                                                    <span class="txt-bold txt-color-dark">01: Join the YLN</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-1" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>This gets you on the database of young leaders and enables us reach you with opportunities to collaborate with strategic partners to build innovative solutions.</p>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-2" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#029E41"
                                                >
                                                    <span class="txt-bold txt-color-dark">02: Add your voice to a program of interest</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-2" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>Explore available programs and contribute your ideas, share your experiences and give ideas and suggestions to create change in program areas you are interested in. Development partners will use these inputs to shape projects and might be particularly interested in working with you to bring your ideas to life.</p>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-3" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#00BC4C"
                                                >
                                                    <span class="txt-bold txt-color-dark">03: Participate in crafting a solution.</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-3" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>Participate in program events and projects</p>
                                                        <ul>
                                                            <li>Writing & editing</li>
                                                            <li>Translation</li>
                                                            <li>Research & Data Collection</li>
                                                            <li>Art & design</li>
                                                            <li>Technology Development</li>
                                                            <li>Teaching & Training</li>
                                                            <li>Project development & management</li>
                                                            <li>Leadership & strategy</li>
                                                            <li>Community organising</li>
                                                            <li>Event Organization</li>
                                                            <li>Healthcare services</li>
                                                            <li>Manual Labor & Skilled Trade</li>
                                                            <li>Outreach & advocacy</li>
                                                            <li>Administration</li>
                                                            <li>Volunteer management</li>
                                                        </ul>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <article class="padding-b-40">
                                        <h5 class="txt-lg txt-bold txt-height-1-4 padding-b-20">
                                            Whats in it for you?
                                        </h5>
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-4" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#929292"
                                                >
                                                    <span class="txt-bold txt-color-dark">01: When you add your voice to programs</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-4" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>Adding your voice to focus programs gives you a platform for the world to hear your views, share your experiences and engage with your ideas. It opens you up to a world of opportunities. For example, If any corporate or development partner is interested in your story or idea and would love to work with you, they get to contact you directly. You also get to inspire other people to take action and make the world a better place.</p>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="txt-normal-s txt-medium">
                                                <a 
                                                   class="d-flex align-items-center justify-content-between bg-green padding-tb-20 padding-lr-30" 
                                                   data-toggle="collapse" 
                                                   href="#impact-5" aria-expanded="false" aria-controls="collapseOne"
                                                   style="background-color:#afabab"
                                                >
                                                    <span class="txt-bold txt-color-dark">02: When you participate in tasks & projects</span>
                                                    <i class="fa fa-angle-down txt-color-white"></i>
                                                </a>
                                            </div>
                                            <div id="impact-5" class="collapse" aria-labelledby="headingOne" data-parent="#jobRequiremenetAccordion">
                                                <div class="border-o-1 border-color-darkgrey padding-o-15">
                                                    <article class="text-box txt-normal-s">
                                                        <p>Completing  tasks and participating in  projects comes with many perks. Some example are:</p>
                                                        <ul>
                                                            <li>Use your experience on these projects as work experience on your CV</li>
                                                            <li>Get access to special trainings and learn a lot of new skills from peers and coordinating partners</li>
                                                            <li>Build long lasting valuable network of friends and influencers</li>
                                                            <li>Be recommended for Jobs with our partners and other corporates based on your performance on a project.</li>
                                                            <li>Enjoy the feeling of fulfilment that comes with contributing to making the world a better place.</li>
                                                        </ul>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-green txt-xs" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Champion form -->
        <div class="modal fade font-main filter-modal" id="championProgramFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?view=form-champion-program'; ?>" method="post">
                            
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30">
                            
                            <h3 class="txt-xxlg txt-light padding-b-30">
                                Join the Young Leaders Network
                            </h3>
                            
                            <div class="margin-b-30 padding-tb-15">
                                <p class="txt-sm">You will be part of an elite group of people working with governments, development agencies and corporate organisations to co-create, shape and deploy creative solutions to development challenges, nationwide.</p>
                            </div>

                            <div class="form margin-b-40">
                                <?php
                                    switch_to_blog(1);
                                    echo do_shortcode( "[gravityform id='4' title='false' description='false' ajax='false']"); 
                                    restore_current_blog();
                                ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
   
    <?php get_footer() ?>
