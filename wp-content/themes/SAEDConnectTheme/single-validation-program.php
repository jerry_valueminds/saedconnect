    <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php

        /* Get User */
        $current_user = wp_get_current_user();
    
        // TO SHOW THE POST CONTENT
        while ( have_posts() ) : the_post();

            $meta_key = 'assigned_users';

            /* Get Post ID */
            $post_id = get_the_ID();

            /* Get Saved Users */
            $saved_users = get_post_meta($post_id, $meta_key);

            if( !in_array($current_user->ID, $saved_users) ){
                // If User is Logged in, redirect to User Dashbord
                $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID

                if ( wp_redirect( $dashboard_link ) ) {
                    exit;
                }
            }

        endwhile; //resetting the page loop
        wp_reset_query(); //resetting the page query
    ?>
    
    
    <?php get_header('user-dashboard') ?>

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* BLog ID */
        //$blog_id = 103;

        /* Post Type */
        //$postType = 'business';
        /* Template View Array */
        $get_page_view = $_GET['view'];
        $page_view_array = array(
            'pending' => array(
                'name' => 'Pending Approval',
                'meta_key' => 'user_published',
                'page_summary' => 'These posts have not been reviewed.',
                'actions' => array(
                    array(
                        'cta_text' => 'Publish',
                        'action' => 'admin_published',
                        'btn_styles' => 'btn btn-trans-bw txt-sm no-m-b'
                    ),
                    array(
                        'cta_text' => 'Move to Review',
                        'action' => 'under_review',
                        'btn_styles' => 'btn btn-trans-bw txt-sm no-m-b'
                    )
                )
            ),
            
            'approved' => array(
                'name' => 'Approved',
                'meta_key' => 'admin_published',
                'page_summary' => 'These posts have been reviewed and approved.',
                'actions' => array(
                    array(
                        'cta_text' => 'Unpublish',
                        'action' => 'user_published',
                        'btn_styles' => 'btn btn-blue txt-sm no-m-b'
                    ),
                    array(
                        'cta_text' => 'Move to Review',
                        'action' => 'under_review',
                        'btn_styles' => 'btn btn-trans-bw txt-sm no-m-b'
                    )
                )
            ),
            
            'reviewing' => array(
                'name' => 'Under Review',
                'meta_key' => 'under_review',
                'page_summary' => 'These posts are under review.',
                'actions' => array(
                    array(
                        'cta_text' => 'Publish',
                        'action' => 'admin_published',
                        'btn_styles' => 'btn btn-trans-bw txt-sm no-m-b'
                    ),
                    array(
                        'cta_text' => 'Move to Unpublished',
                        'action' => 'user_published',
                        'btn_styles' => 'btn btn-blue txt-sm no-m-b'
                    ),
                    
                )
            ),            
        );

        /* Template View Array */
        $template_view = $_GET['validate'];

        /* All template information */
        $templates_information = array(
            'business' => array(
                'blog_id' => 103,           
                'post_type' => 'business',
                'display_name' => 'Business'
            ),
            'jobs' => array(
                'blog_id' => 101,           
                'post_type' => 'job',
                'display_name' => 'Job Offers'
            ),
            'programs' => array(
                'blog_id' => 12,           
                'post_type' => 'opportunity',
                'display_name' => 'Programs'
            ),
            'projects' => array(
                'blog_id' => 103,           
                'post_type' => 'personal-project',
                'display_name' => 'Projects'
            ),
            'service-offers' => array(
                'blog_id' => 101,           
                'post_type' => 'offer-a-service',
                'display_name' => 'Service Offers'
            ),
            'training-offers' => array(
                'blog_id' => 18,           
                'post_type' => 'training-offer',
                'display_name' => 'Training Offers'
            ),
            'trainer-information' => array(
                'blog_id' => 18,           
                'post_type' => 'trainer-information',
                'display_name' => 'Trainer Information'
            ),
        );
            


        /* Select Template Information */
        $template_information = $templates_information[ $template_view ];

        /* Publication */
        $publication_key = 'publication_status';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <?php switch_to_blog($template_information[ 'blog_id' ]); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if( $_GET['view'] == 'form' ){ //Display form  ?>

                <?php
                    if($_GET['post-id']){
                        $post_id = $_GET['post-id'];
                        $post = get_post($post_id);
                        $postName = $post->post_title;
                        $meta_value = $_GET['value'];
                        $redir_view = $_GET['redir'];

                        /* Publish / Unpublish & Return */
                        if($_GET['action'] == 'publication'){

                            /* Get saved meta */
                            $saved_meta = get_post_meta( $post_id, $publication_key, true );

                            /* Published, Unpublish, Move to review */
                            update_post_meta( $post_id, $publication_key, $meta_value );
                            
                            $redirect_link = currentUrl(true).'/?validate='.$template_view.'&view='.$redir_view;
                                
                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    }
                ?>
            
            <?php } else { ?>
                <?php
                    /* Get Page View */
                    $page_view = $page_view_array[ $get_page_view ]
                ?>
                <div class="page-header">
                    <h1 class="page-title">
                        Publish <?php echo $template_information['display_name'] ?>
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        <?php echo $page_view['page_summary'] ?>
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                    <?php foreach( $page_view_array as $key => $page_view_item ){ ?>
                        
                        <li>
                            <a 
                               
                               href="<?php printf('%s/?view=%s&validate=%s', currentUrl(true), $key, $template_view ) ?>"
                               class="<?php echo ($_GET['view'] == $key) ? 'active' : ''; ?>"
                            >
                                <?php echo $page_view_item['name'] ?>
                            </a>
                        </li>
                    <?php } ?> 
                    </ul>
                </nav>
                
                <div class="">
                    <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                       <div class="col-5 padding-lr-15">
                            NAME
                       </div>
                       <div class="col-3 padding-lr-15">
                            AUTHOR
                       </div>
                    </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */

                    // Create Query Argument
                    $args = array(
                        'post_type' => $template_information['post_type'],
                        'showposts' => -1,
                        'meta_query' => array(
                            array(
                                'key' => $publication_key,
                                'value' => $page_view['meta_key']
                            )
                        )
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                ?>
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-5 padding-lr-15">
                            <a href="<?php the_permalink() ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                           <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($post->post_author, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $post->post_author, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );
                          
                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                          

                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                }   

                                restore_current_blog();
                            ?>
                            
                            <style>
                               .author_avatar {
                                    display: inline-block;
                                    background-size: cover !important;
                                    background-position: center !important;
                                    background-repeat: no-repeat !important;
                                    width: 1.5rem;
                                    height: 1.5rem;
                                    margin-right: 10px;
                                    border-radius: 50%;
                                }
                            </style>
                            <div class="d-flex align-items-center">
                                <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                                </figure>
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php echo ($post->post_author == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 padding-lr-15">
                            <a href="<?php printf("https://www.saedconnect.org/competency-profile/mail-user/?reference_post=%s&user_id=%s&blog_id=%s&post_id=%s", get_permalink(), get_the_author_meta('ID'), $template_information[ 'blog_id' ], $post_id ) ?>" target="_blank">
                                <i class="fa fa-envelope-o"></i>
                                Mail Author
                            </a>
                        </div>
                        <div class="col-2 padding-lr-15 text-right">
                            <div class="dropdown padding-l-20">
                                <a class="" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa-stack">
                                        <i class="fa fa-circle fa-stack-2x txt-grey"></i>
                                        <i class="fa fa-ellipsis-v fa-stack-1x txt-color-white"></i>
                                    </span>
                                </a>
                                <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuLink">
                                   
                                    <?php 
                                
                                        $actions = $page_view['actions'];

                                        for ($i = 0; $i < count( $actions ); $i++) {

                                    ?>

                                    <!--<a href="<?php printf('%s&view=form&action=publication&value=%s&redir=%s&post-id=%s', currentUrl(false), $actions[$i]['action'], $get_page_view, $post_id ) ?>" class="<?php echo $actions[$i]['btn_styles'] ?>">
                                        <?php echo $actions[$i]['cta_text'] ?>
                                    </a>-->
                                    
                                    <a 
                                       href="<?php printf('%s&view=form&action=publication&value=%s&redir=%s&post-id=%s', currentUrl(false), $actions[$i]['action'], $get_page_view, $post_id ) ?>"
                                       class="dropdown-item"
                                    >
                                        <?php echo $actions[$i]['cta_text'] ?>
                                    </a>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    endwhile;
                ?>
                </div>
            <?php } ?> 
                
            </div>
        </section>
    </main>

    <?php restore_current_blog(); ?>
            
<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>