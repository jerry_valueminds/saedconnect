<?php /*Template Name: Profile - Cover Letters*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Post Type */
        $postType = 'cover-letter';

        /* Publication */
        $publication_key   = 'publication_status';

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post" class="form">
                            <?php
                                /* Meta Key */
                                $meta_key = 'assigned_users';
                                $validation_meta_key = 'validation_types';
                                $redirect_link = currentUrl(true);
                              
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    $post = get_post($post_id);
                                    $postName = $post->post_title;
                                    
                                    /* Delete & Return */
                                    if($_GET['action'] == 'delete'){
                                        /* Delete Post */
                                        wp_delete_post($post_id);

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                }
                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if( $_POST ){
                                    
                                    /* Get Post Name */
                                    $postName = $_POST['post_name'];
                                    
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));
                                    
                                    /* Get form data */
                                    $summary = wp_kses_post( $_POST['summary'] );
                                    
                                    /* Save data */
                                    update_post_meta($post_id, 'summary', $summary);
                                }
                                              
                                              
                                /* Get Saved data */             
                                $summary = get_post_meta($post_id, 'summary', true);

                            ?>
                            
                            <!-- Title -->
                            <div class="form-item">
                                <label for="post-name">
                                    Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post_name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $postName ?>"
                                >
                            </div>

                            <!-- Summary -->
                            <div class="form-item">
                                <label for="body">
                                    Body
                                </label>
                                <textarea name="body" id="body" cols="30" rows="15" class="editor"><?php echo $summary ?></textarea>
                            </div>
                            
                            <div class="text-right padding-t-20">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                        <?php 
                            if( $_POST )
                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        ?>
                    </div>
                </div>
               
            <?php } else { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        My Cover Letters
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form-title=Create a Cover Letter" class="cta-btn">
                            Add Letter
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Create and Manage your Cover Letters.
                    </p>
                </article>
                
                <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                    <div class="col-12 padding-lr-15">
                        NAME
                    </div>                 
                </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */
                    // Create Query Argument
                    $args = array(
                        'post_type' => $postType,
                        'showposts' => -1,
                        'author' => $current_user->ID,
                        'meta_query' => $meta_array,
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                          
                ?>
                    
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-9 padding-lr-15">
                            <a href="<?php the_permalink() ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15 text-right">
                            <a 
                               href="<?php printf('%s/?view=form&post-id=%s&form-title=Edit %s', currentUrl(true), $post_id, get_the_title() ) ?>"
                               class="txt-medium txt-color-green"
                            >
                                Edit
                            </a>
                            |
                            <a 
                               href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', currentUrl(true), $post_id ) ?>"
                               class="confirm-delete txt-color-red"
                            >
                                Delete
                            </a>
                        </div>
                    </div>
                    
                <?php
                    endwhile;
                ?>
                
            <?php } ?> 
                
            </div>
        </section>
    </main>
<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>