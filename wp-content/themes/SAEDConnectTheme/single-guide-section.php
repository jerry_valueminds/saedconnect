    <?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
    
    <?php
        /* Guide Information */
        $track_url = rwmb_meta( 'section-track-link' );
        $track_fire_side_chat = rwmb_meta( 'section-fire-side-link' );
        $track_mentor_hub = rwmb_meta( 'section-mentor-hub-link' );
    ?>
   
    <main class="main-content">
       <style>
            .theme-bg{
                background-color: <?php echo $program_theme_color ?>;
            }

            .theme-alt{
                color: <?php echo $program_theme_color_alt ?>;
            }

            .how-it-works-container::before{
                background-color: <?php echo $program_theme_color ?>;
            }


            .menu li{
                display: inline;

            }

            .menu li a{
                display: inline-block;
                font-size: 0.9rem;
                padding: 20px 20px;
                color: white;
            }

            .menu li a.active{
                background-color: white;
                color: black;
            }
        </style>
        <div class="padding-t-10">
            <div class="container-wrapper bg-ash">
                <ul class="menu">
                    <li>
                        <a href="<?php echo $track_url ?>">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $track_fire_side_chat ?>">
                            Fire Side Chat
                        </a>
                    </li>
                    <li>
                        <a href="<?php the_permalink() ?>" class="active">
                            Guides
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $track_mentor_hub ?>">
                            Mentor Hub
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <section class="container-wrapper padding-tb-40 border-b-1 border-color-darkgrey">
            <div class="row row-20">
                <div class="col-md-10 padding-lr-20">
                    <a class="txt-sm txt-medium inline-block bg-grey padding-tb-10 padding-lr-20" href="<?php echo site_url() ?>">
                        <?php echo get_bloginfo() ?>
                    </a>
                    <h1 class="txt-3em margin-b-20"><?php the_title() ?></h1>
                    <article class="text-box">
                       <?php the_content() ?>
                    </article>
                </div>
                <div class="col-md-2 padding-lr-20">
                    <?php

                        $images = rwmb_meta( 'online-guide-featured-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>
                    <img src="<?php echo $image['full_url']; ?>" alt="">
                </div>
            </div>        
        </section>
        <section class="container-wrapper padding-tb-20 bg-ash d-none">
            <div class="row align-items-center">
                <p class="txt-color-white padding-r-20">
                    Explore other sections
                </p>
                <div class="col-md-4 magazine-dropdown">
                    <div class="dropdown">
                        <button class="btn btn-trans-wb txt-normal-s full-width dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php the_title() ?>
                        </button>
                        <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                        
                        <?php // Display posts
                            $temp = $wp_query; $wp_query= null;
                            $wp_query = new WP_Query();
                            $wp_query->query(array('post_type' => 'guide-section'));
                                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                            <a class="dropdown-item" href="<?php the_permalink() ?>">
                                <?php the_title() ?>
                            </a>
                        <?php
                            endwhile;
                               
                            //Reset Query
                            wp_reset_postdata();
                            wp_reset_query();
                            $wp_query = $temp;
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-wrapper padding-tb-40 bg-yellow">
            <div class="row row-10">
            <?php 

                $connected = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'og_section_to_mini_site',
                        'from' => get_the_ID(), // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $connected->have_posts() ) : $connected->the_post();
            ?>

                    <div class="col-md-3 padding-lr-10 padding-b-20 d-flex">
                        <a class="topics-card full-width" href="<?php the_permalink() ?>">
                            <article class="content full-width">
                                <div class="course-info">
                                    <h3 class="title"><?php the_title() ?></h3>
                                    <h4 class="txt-sm margin-b-80">
                                        <span class="txt-bold">Sposored By:</span>
                                        <span class="name">
                                            <?php echo rwmb_get_value( 'mini-site-sponsor-name' ); ?>
                                        </span>
                                    </h4>
                                    <article class="text-box sm">
                                        <?php echo rwmb_get_value( 'mini-site-description' ); ?>
                                    </article>
                                </div>
                            </article>  
                        </a>   
                    </div>
            <?php
                endwhile;
                wp_reset_postdata();
            ?>
            </div>
        </section>
        <?php the_content() ?>
        <!--<section>
            <div class="row">
                <div class="col-md-8">
                    <a class="feature-video-block" style="background-image:url('images/heroes/hero_8.jpg');">
                        <span class="caption">
                            <h4 class="subtitle txt-color-white">
                                Featured Courses
                            </h4>
                            <h3 class="title txt-color-white">
                                Advanced Cinematography
                            </h3>
                        </span>     
                    </a>
                </div>
                <div class="col-md-4 cta-block bg-black txt-color-white">
                    <article>
                        <div class="txt-3em txt-color-white padding-tb-20 border-t-4 border-color-white">
                            <i class="fa fa-quote-left"></i>
                        </div>
                        <h2 class="title">
                            The level of talent I was surrounded by when I started here was a bit overwhelming at first, until I realized these were people I’d be able to work with and learn from.
                        </h2>
                        <div class="txt-medium txt-height-1-2">
                            Anne Burdick
                            <br>
                            Chair, Media Design Practices
                        </div>
                    </article>
                </div>
            </div>
        </section>-->
    </main>
    
    <?php endwhile; // end of the loop. ?>

    <?php get_footer() ?>