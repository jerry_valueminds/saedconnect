<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    <meta name="description" content="">
    <meta name="author" content="saedconnect.com">
	<title>SAEDConnect</title>
	
	<!--Site icon-->
	<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" type="image/x-icon">
	
    <!--Load Styles-->
	<?php wp_head(); ?>
	
	<!--FontAwesome CDN-->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
</head>
<body>

<?php if ( get_post_type( get_the_ID() ) !== 'album' &&  get_post_type( get_the_ID() ) !== 'gallery' ) { ?>
	
	<header class="main-navigation font-main" id="myHeader">
        <div class="navigation-wrapper">
            <a class="brand" href="http://www.saedconnect.org/">
                <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                <span class="name">
                    SAEDConnect
                </span>
            </a>
            <div class="action-btn-wrapper">
                
                <div class="search">
                    <div class="search-btn">
                        <i class="fa fa-search"></i>
                    </div>
                    <form action="" class="search-form">
                        <div class="wrapper">
                            <input class="search-box" type="search" placeholder="Type to Search">
                            <div class="close-btn">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-times fa-stack-1x"></i>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="user-account">
                    
                    <?php
                    
                        if ( is_user_logged_in() ) {
                         $display_name = um_user('display_name');
                    ?>
                        
                        <div class="dropdown">
                            <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="account-info">
                                    <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="">
                                    <span class="profile-name">
                                        <?php echo $display_name; // prints the user's display name ?>
                                    </span>
                                </span>
                            </button>
                            <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                                <a class="dropdown-item" href="<?php echo network_home_url() ?>user">
                                    View Profile
                                </a>
                                <a class="dropdown-item" href="<?php echo network_home_url() ?>logout">
                                    Logout
                                </a>
                            </div>
                        </div>
                         
                         
                    <?php } else { ?>
                           
                        <a class="login" href="<?php echo network_home_url() ?>login">Login</a>
                        <a class="signup" href="<?php echo network_home_url() ?>register">Sign up</a>
                            
                    <?php } ?>
                </div>
                <button class="menu-btn hamburger hamburger--spring d-block d-sm-none">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    <?php 
        //global $wp;
        if(home_url( $wp->request ) != 'http://www.saedconnect.org/partner' ){ 
    ?>
        <!-- For Corp Members -->
        <nav class="navigation-list-wrapper left">
            <ul class="navigation-list">
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'find-your-path')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(573); ?>">
                        Find Your Path
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Discover Yourself
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Use our career guidance online tool to gain clarity about your strengths & weaknesses, and discover viable careers that match your talents.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" data-toggle="modal" href="#comingSoonModal">Start Here</a>  
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Get Counseling
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Let our counselors work with you to uncover your strengths, interpret your passions and provide objective and professional advice about optimal career paths to choose.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/about/reference/purpose-discovery-services/">Start Here</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Self Discovery Guide
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Dive into concepts & explanations that help you appreciate your uniqueness and guide you in making informed career decisions.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/self-discovery-guide/">Start Here</a>  
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'get-a-job')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(575); ?>">
                        Get a Job
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Are you ready?
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        You need to develop the right mindset to get a great job and keep it. Explore the key attitudes & principles that guarentee career success. 
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/prepare-yourself/">Start Here</a>  
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/career-guide">
                                            Job Search & Career Development Guide
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 3)
                                        switch_to_blog(3);
                                                                                                        
                                        //WP Query to get Guides
                                        
                                        $temp = $wp_query; $wp_query= null;
                                        $wp_query = new WP_Query();
                                        $wp_query->query(array('post_type' => 'guide-section'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php echo the_permalink() ?>">
                                                <?php echo the_title() ?>
                                            </a>
                                        </li>
                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                        endwhile;
                                        wp_reset_postdata();
                                        wp_reset_query();
                                        $wp_query = $temp;
                                                                                                        
                                    ?>
                                    </ul>
                                </div>
                                
                                <div class="col-md-6 row row-20 padding-lr-20">
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/career_coaching_service.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/about/discourse/career-coaching/">Career Coaching Services</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/cv_cover_letter_service.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/about/discourse/cv-cover-letter-prep/">CV & Cover Letter Services</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/job_interview_preparation.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/about/discourse/interview-prep/">Interview Prep Services</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/professional_exam.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/professional-association/">Register for a Professional Exam</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/school_oversea.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/about/discourse/school-overseas/">School Overseas</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/work%20abroad_2.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/about/discourse/work-overseas/">Work Overseas</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(577); ?>">
                        Do Business
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Are you ready?
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        You need to develop the right mindset to start & build a great business. Explore the key attitudes & principles to guarentee success in entreprenuership. 
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/prepare-yourself/">Start Here</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/entrepreneurship-guide/">
                                            Doing Business Guide
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 2)
                                        switch_to_blog(2);
                                                                                                        
                                        //WP Query to get Guides
                                        $temp = $wp_query; $wp_query= null;
                                        $wp_query = new WP_Query();
                                        $wp_query->query(array('post_type' => 'guide-section'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php echo the_permalink() ?>">
                                                <?php echo the_title() ?>
                                            </a>
                                        </li>
                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                        endwhile;
                                        wp_reset_postdata();
                                        wp_reset_query();
                                        $wp_query = $temp;
                                    ?>
                                    </ul>
                                </div>
                                <div class="col-md-6 row row-20 padding-lr-20">
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/xplore_biz_idea_direcrtory.png" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/xplore-directory/?industry-id=&search-type=xplore-business&xplore-type=business&s=">
                                                        Xplore Business Idea Directory
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/join_the_venture_ven_virtual_Incubator.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="">Join the Venture Oven Virtual Incubator</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/get_business_counseling.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="">Get Business Counseling</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/create_a_business_plan.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/about/discourse/business-plan-creation/">Create a Business Plan</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/get_complete_business_branding.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/about/discourse/business-branding/">Get Complete Business Branding</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/business_registration_other_legal_services.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/about/discourse/business-registrations/">Business Registration & Other Legal Services</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'learn-a-skill')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/learn-a-skill">
                        Learn a Skill
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <h4 class="txt-xlg txt-bold txt-height-1-4">
                                        Find trainers near you or check out available courses across a wide variety of subject areas.
                                    </h4>
                                </div>
                                <div class="col-md-9 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        I want to learn a skill in:
                                    </h4>
                                    <ul class="row sub-menu-list">
                                    <!-- Get All Top Level Industry Terms from Learn a Skill Multisite -->
                                    <?php 
                                        //Switch to Learn a Skill Multisite (id = 7)
                                        switch_to_blog(7);
                                         
                                        //Get Terms
                                        $terms = get_terms( 'industry', array('hide_empty' => false,)); //Get all the terms

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;

                                            if ( $parent=='0' ) {

                                                $term_id = $term->term_id; //Get the term ID
                                                $term_name = $term->name; //Get the term name
                                                $term_url = get_term_link($term);
                                    ?>
                                        <li class="col-md-4">
                                            <a href="http://www.saedconnect.org/learn-a-skill/?industry-id=<?php echo $term_id ?>&post-type=course&search-type=course&s=">
                                                <?php echo $term_name; ?>
                                            </a>
                                        </li>
                                    <?php
                                            } 
                                        }
                                                                                                        
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'resources-and-Opportunities')
                                echo 'active';
                            }
                        ?>"
                        href="#">
                        Get Help
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/accelerate-blog/">
                                            Get Help with Your Career
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/career-coaching/">Career Coaching Services</a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/cv-cover-letter-prep/">CV & Cover Letter Services</a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/interview-prep/">Interview Prep Services</a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/professional-association/">Register for a Professional Exam</a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/professional-association/">Register for a Professional Exam</a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/work-overseas/">Work Overseas</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/programs">Get Help with Your Business</a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <!-- Get All Magazine Terms -->
                                        <li>
                                            <a href="http://www.saedconnect.org/xplore-directory/?industry-id=&search-type=xplore-business&xplore-type=business&s=">
                                                Xplore Business Idea Directory
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a href="">Join the Venture Oven Virtual Incubator</a>
                                        </li>
                                    
                                        <li>
                                            <a href="">Get Business Counseling</a>
                                        </li>
                                    
                                        <li>
                                            <a href="">Get Business Counseling</a>
                                        </li>
                                    
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-branding/">Get Complete Business Branding</a>
                                        </li>
                                    
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-registrations/">Business Registration & Other Legal Services</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Tools
                                    </h4>
                                    <div class="row row-5 sub-menu-image-box padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/join_a_professional_association.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="http://www.saedconnect.org/professional-association">CV Creator</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row row-5 sub-menu-image-box padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/service_provider_directory.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="http://www.saedconnect.org/about/blog/mini-site/growth-support-services/">
                                                    Business Plan Creator
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="navigation-list">
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="http://www.saedconnect.org/programs">
                        Opportunities
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/accelerate-blog/">
                        Accelerate Blog
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'about')
                                echo 'active';
                            }
                        ?>"
                        data-toggle="modal" href="#comingSoonModal">
                        NYSC SAED
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        About SAEDConnect
                                    </h4>
                                    <figure class="image-item">
                                        <img class="border-o-1 border-color-darkgrey" src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/saedconnect.png" alt="">
                                        <figcation>
                                            <a class="link-btn bg-blue" href="">Discover</a>  
                                        </figcation>
                                    </figure>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Work With us
                                    </h4>
                                    <figure class="image-item">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/work_with_us.jpg" alt="">
                                        <figcation>
                                            <a class="link-btn bg-blue" href="">Discover</a>  
                                        </figcation>
                                    </figure>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Site Support
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="about.html">
                                                Report an issue
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </nav>
    <?php }else{ ?>
        <!-- For Corp Partners -->
        <nav class="navigation-list-wrapper left">
            <ul class="navigation-list">
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'find-your-path')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(573); ?>">
                        Support Youth Development
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'get-a-job')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(575); ?>">
                        Share Your Programs
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(577); ?>">
                        Talent Source
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
            </ul>
            <ul class="navigation-list">
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="http://www.saedconnect.org/programs">
                        Partner
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/accelerate-blog/">
                        NYSC SAED
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'about')
                                echo 'active';
                            }
                        ?>"
                        data-toggle="modal" href="#comingSoonModal">
                        Youth Portal
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
            </ul>
        </nav>
    <?php } ?>
	</header>
	
<?php } ?>