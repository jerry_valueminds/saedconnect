    <?php /*Template Name: Trainer Directory*/ ?>
    
    <?php get_header() ?>
    
    <?php
        /* Filter Request */
        $model = $_GET['model'];
    ?>
    
    <style>
        .gform_wrapper .gf_invisible, .gform_wrapper .gfield_visibility_hidden {
            visibility: hidden;
            position: absolute;
            left: -9999px;
        }
    </style>
   
    <main class="main-content">
        <header class="course-directory-banner bg-grey">
            <div class="content container-wrapper">
                <h1 class="title">
                    Trainer Directory
                </h1>
                <!--<ul class="tax-list">
                    <li>
                        <a class="selected" href="">Sub-category</a>
                    </li>
                    <li>
                        <a href="">Sub-category</a>
                    </li>
                    <li>
                        <a href="">Sub-category</a>
                    </li>
                    <li>
                        <a href="">Sub-category</a>
                    </li>
                    <li>
                        <a href="">Sub-category</a>
                    </li>
                </ul>-->
            </div>
        </header>
        <!--<section class=" bg-grey">
            <ul class="skills-nav-bar">
                <li>
                    <a href="course_directory_category.html">Explore Courses</a>
                </li>
                <li class="active">
                    <a href="trainer_directory_category.html">Explore Trainers</a>
                </li>
            </ul>
        </section>-->
        <section class="container-wrapper padding-tb-40 margin-b-40">
            <div class="row row-20">
                <div class="col-md-3 padding-lr-20 filter-box">
                    <h2 class="txt-sm margin-b-40">Filter</h2>
                    <form method="get">
                    <?php
                        /* Filter Options Array */
                        $filter_items_array = array(
                            array(
                                'term_slug' =>'coverage-nigeria',
                                'term_name' => 'Location',
                                'meta_key' => 'trainer_locations',
                                'show_default' => false
                            ),

                            array(
                                'term_slug' =>'model',
                                'term_name' => 'Model',
                                'meta_key' => 'trainer_type_of_training',
                                'show_default' => true
                            ),
                            
                            array(
                                'term_slug' =>'skill',
                                'term_name' => 'Skill',
                                'meta_key' => 'trainer_skill_areas',
                                'show_default' => true
                            ),
                        );
                    ?>
                       
                    <?php foreach($filter_items_array as $filter_item){ ?>
                       
                        <div class="filter-group">
                            <button
                               class="title dropdown-toggle"
                               type="button"
                               data-toggle="collapse"
                               data-target="#filter-content-<?php echo $filter_item['term_slug']; ?>"
                               aria-expanded="false"
                               aria-controls="filter-content-<?php echo $filter_item['term_slug']; ?>"
                            >
                                <?php echo $filter_item['term_name']; ?>
                            </button>
                            <div class="collapse show" id="filter-content-<?php echo $filter_item['term_slug']; ?>">
                                <ul class="filter-list">
                                    <li>
                                        <span class="input-area">
                                            <input
                                                type="radio"
                                                name="<?php echo $filter_item['meta_key']; ?>"
                                                value="all" 
                                            <?php
                                                if ( esc_html($_REQUEST[$tax_slug]) == 'all' || esc_html($_REQUEST[$tax_slug]) == '' ) {
                                                    echo 'checked';
                                                } 
                                            ?>
                                            
                                            >
                                            <label for="">All</label>
                                        </span>
                                    </li>
                                <?php 

                                    //Get Terms
                                    $terms = get_terms( $filter_item['term_slug'], array('hide_empty' => false, 'parent' => 0)); //Get all the terms

                                    foreach ($terms as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $parent = $term->parent;
                                        
                                        $tax_slug = $filter_item['term_slug'];

                                        $term_id = $term->term_id; //Get the term ID
                                        $term_slug = $term->slug; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                        $term_url = get_term_link($term);
                                ?>
                                    <li>
                                        <span class="input-area">
                                            <input
                                                type="radio"
                                                name="<?php echo $filter_item['meta_key']; ?>"
                                                value="<?php echo $term_slug ?>"
                                                <?php echo esc_html($_REQUEST[$tax_slug]) == $term_slug  ? 'checked' : '' ?>
                                            >
                                            <label for=""><?php echo $term_name; ?></label>
                                        </span>
                                    </li>
                                <?php } ?>
                                </ul>
                            </div>
                        </div>
                        
                    <?php } ?>
                        <input class="btn btn-blue txt-xs full-width" type="submit" value="Filter" id="searchsubmit">
                    </form>
                </div>
                <div class="col-md-9 loader-container padding-lr-20">
                    <div class="trainer-card">
                    <?php
                        /*
                        *==================================================================================
                        *==================================================================================
                        *   WP Query
                        *==================================================================================
                        *==================================================================================
                        */
                        // Define Tax Query
                        $meta_array = array('relation' => 'AND');

                        // Add Arguements from _REQUEST
                        foreach($_GET as $key => $value){

                            /* Check & Exclude:
                                    - S ( WP Search field )
                                    - Search-type ( For selecting Search Template View to use )
                                    - Empty values
                            */
                            if($key !== 's' && $key !== 'search-type' && !empty($value) && $value != 'all'){

                                // Create Tax Array entry
                                $category_array = array(

                                    // Add Term Term Values
                                    array (
                                        'key' => trim($key), //Texanomy Type
                                        'value' => $value, //Search field
                                    ),

                                );

                                // Add just created Array to Tax Array. 
                                $meta_array = array_merge($meta_array, $category_array); 
                            }
                        }

                        /* GF Search Criteria */
                        $gf_id = 1;
                        $search_criteria;
                        
                        /* GF Search Criteria */
                        /*$search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                          )
                        );*/

                        /* Get GF Entry Count */
                        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                        $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                        foreach( $entries as $entry ){

                            $post_id = $entry['post_id'];
                            $post = get_post($post_id);

                    ?>
                        <!-- Trainer -->
                        <a 
                           class="row margin-b-10"
                           data-toggle="modal" 
                           href="#trainerModal-<?php echo $entry['post_id'] ?>"
                           data-parentid="<?php echo $entry['post_id'] ?>"
                           data-name="<?php echo $post->post_title ?>"
                           data-description="<?php echo rgar( $entry, 5 ); ?>"
                           data-contactperson="<?php echo rgar( $entry, 8 ); ?>"
                           data-contactaddress="<?php echo rgar( $entry, 9 ); ?>"
                           data-contactemail="<?php echo rgar( $entry, 10 ); ?>"
                           data-contactphone="<?php echo rgar( $entry, 11 ); ?>"
                           data-contactwhatsapp="<?php echo rgar( $entry, 12 ); ?>"
                        >
                            <div class="col-md-8 padding-b-40 padding-t-20 padding-lr-20 bg-grey">
                                <div class="title-bar margin-b-20">
                                    <div class="">
                                        <h4 class="subtitle">
                                            <?php echo truncate( $post->post_title, 70 );?>
                                        </h4>
                                        <!--<p class="txt-sm margin-b-20">ID: 84554545</p>-->
                                    </div>
                                    <figure class="verification">
                                        <span class="caption">NYSCSAED Trainer</span>
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/nysc_saed%20_logo.png" alt="">
                                    </figure>
                                </div>
                                
                                <article class="">
                                    <p class="txt-sm margin-b-20">
                                        <strong class="txt-bold">Trains on: </strong>
                                        <?php
                                            $field_id = 2; // Update this number to your field id number
                                            $field = RGFormsModel::get_field( $gf_id, $field_id );
                                            $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                            echo $value;
                                        ?>
                                    </p>
                                    <p class="txt-sm margin-b-15">
                                        <strong>
                                            <i class="fa fa-map-marker"></i>
                                        </strong>
                                        <?php
                                            $field_id = 7; // Update this number to your field id number
                                            $field = RGFormsModel::get_field( $gf_id, $field_id );
                                            $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                            echo $value;
                                        ?>
                                    </p>
                                </article>
                                
                                <!--<div class="reamore">
                                    <div class="collapse" id="collapseTrainer-<?php the_ID(); ?>">
                                        <article class="margin-b-20">
                                            <h4 class="txt-sm txt-bold margin-b-10">About</h4>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 5 ); ?>
                                            </p>
                                        </article>
                                        <article class="margin-b-20">
                                            <h4 class="txt-sm txt-bold margin-b-10">
                                                Have you ever Trained before?
                                            </h4>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 17 ); ?>
                                            </p>
                                        </article>
                                        <article class="margin-b-20">
                                            <h4 class="txt-sm txt-bold margin-b-10">
                                                Name of Contact person for any enquiries
                                            </h4>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 8 ); ?>
                                            </p>
                                        </article>
                                        <article class="margin-b-20">
                                            <h4 class="txt-sm txt-bold margin-b-10">
                                                Primary Contact Address
                                            </h4>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 9 ); ?>
                                            </p>
                                        </article>
                                        <article class="margin-b-20">
                                            <h4 class="txt-sm txt-bold margin-b-10">
                                                Contact Email address
                                            </h4>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 10 ); ?>
                                            </p>
                                        </article>
                                        <article class="margin-b-20">
                                            <h4 class="txt-sm txt-bold margin-b-10">
                                                Contact Phone Number
                                            </h4>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 11 ); ?>
                                            </p>
                                        </article>
                                        <article class="margin-b-20">
                                            <h4 class="txt-sm txt-bold margin-b-10">
                                                Contact WhatsApp Number
                                            </h4>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 12 ); ?>
                                            </p>
                                        </article>
                                        
                                        <article>
                                            <h4 class="txt-sm txt-bold margin-b-15">Social Media</h4>
                                            <ul class="social-section row row-10">
                                            <?php
                                                /* facebook */
                                                $field = 'trainer_facebook';

                                                $meta = get_post_meta($post->ID, $field, true);

                                                if($meta){
                                            ?>
                                                <li class="padding-lr-10">
                                                    
                                                    <a href="<?php echo $meta; ?>">
                                                        <i class="fa fa-facebook-square"></i>
                                                        <span class=""><?php echo $meta; ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                              
                                            <?php
                                                /* Linkedin */
                                                $field = 'trainer_linkedin';

                                                $meta = get_post_meta($post->ID, $field, true);

                                                if($meta){
                                            ?>
                                                <li class="padding-lr-10">
                                                    
                                                    <a href="<?php echo $meta; ?>">
                                                        <i class="fa fa-linkedin"></i>
                                                        <span class=""><?php echo $meta; ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                              
                                            <?php
                                                /* Twitter */
                                                $field = 'trainer_twitter';

                                                $meta = get_post_meta($post->ID, $field, true);

                                                if($meta){
                                            ?>
                                                <li class="padding-lr-10">
                                                    
                                                    <a href="<?php echo $meta; ?>">
                                                        <i class="fa fa-twitter"></i>
                                                        <span class=""><?php echo $meta; ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            
                                            <?php
                                                /* Wrbsite */
                                                $field = 'trainer_website';

                                                $meta = get_post_meta($post->ID, $field, true);

                                                if($meta){
                                            ?>
                                                <li class="padding-lr-10">
                                                    
                                                    <a href="<?php echo $meta; ?>">
                                                        <i class="fa fa-globe"></i>
                                                        <span class=""><?php echo $meta; ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            </ul>
                                        </article>
                                    </div>
                                </div>-->
                                
                                <!--<div class="txt-sm text-right floating-footer">
                                    <p>
                                        
                                    </p>
                                    <p>
                                        <a href="" class="padding-r-15">0 reviews</a>
                                        <a href="" class="txt-underline padding-r-15">
                                            <i class="fa fa-plus"></i>
                                            <span>Write a review</span>
                                        </a>
                                        <a href="" class="txt-underline">
                                            <i class="fa fa-plus"></i>
                                            <span>Report Trainer</span>
                                        </a>
                                    </p>
                                </div>-->
                            </div>
                            <div class="col-md-4 padding-o-20 bg-ghostwhite">
                                <div class="trainer-rating margin-b-20 txt-color-yellow">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <h3 class="txt-bold txt-sm margin-b-20">Course Types Offered</h3>
                                <ul class="txt-sm course-type-list">
                                <?php
                                    $field = 'trainer_type_of_training';

                                    $meta = get_post_meta($post->ID, $field, false);

                                    if($meta){
                                        foreach($meta as $key=>$value) {

                                            if($value == 'Class room Training (Your training requires the students to come to a physical location)' ){
                                    ?>

                                        <li class="margin-b-10">
                                            <i class="fa fa-building"></i>
                                            Class room Training
                                        </li>

                                    <?php
                                            } else {
                                    ?>

                                        <li class="margin-b-10">
                                            <i class="fa fa-tv"></i>
                                            Online/Virtual
                                        </li>

                                    <?php    
                                            }
                                        }
                                    }
                                ?>                                        
                                </ul>
                            </div>
                        </a>
                        
                        <div class="modal fade font-main shc-modal" id="trainerModal-<?php echo $entry['post_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="container-wrapper">
                                        <div class="col-md-10 mx-auto">
                                            <div class="padding-tb-80">
                                                <div class="d-flex justify-content-between justify-content-space margin-b-40">
                                                    <h2 class="trainer-name txt-2em txt-bold txt-height-1-2">
                                                        <?php echo $post->post_title ?>
                                                    </h2>
                                                    <p>
                                                        <a class="btn btn-trans-bw txt-xs no-m-b" data-toggle="collapse" href="#contactForm" role="button" aria-expanded="false" aria-controls="contactForm">
                                                            Contact Trainer
                                                        </a>
                                                    </p>
                                                </div>
                                                <article class="text-box">
                                                    <p id="description">

                                                    </p>
                                                </article>
                                                <article>
                                                    <div class="collapse" id="contactForm">
                                                        <h2 class="txt-bold padding-b-20 padding-t-20">
                                                            Leave a message for <span class="trainer-name"></span>
                                                        </h2>
                                                        <div class="form">
                                                            <?php 
                                                                echo do_shortcode( "[gravityform id='4' title='false' description='false' ajax='false']"); 
                                                            ?>
                                                        </div>
                                                    </div>
                                                </article>
                                                <div class="row row-20 padding-t-20">
                                                    <div class="col-md-6 padding-lr-20 padding-b-40">
                                                        <h3 class="txt-bold margin-b-20">
                                                            Contact Information
                                                        </h3>
                                                        <div class="padding-o-30 border-o-1 border-color-darkgrey">
                                                            <article class="margin-b-20">
                                                                <h3 class="txt-sm txt-bold margin-b-5">
                                                                    Contact Person
                                                                </h3>
                                                                <p class="txt-normal-s" id="contactPerson">
                                                                    <?php echo rgar( $entry, 8 ); ?>
                                                                </p>
                                                            </article>
                                                            <article class="margin-b-20">
                                                                <h3 class="txt-sm txt-bold margin-b-10">
                                                                    Address
                                                                </h3>
                                                                <p class="txt-normal-s" id="contactAddress">
                                                                    <?php echo rgar( $entry, 9 ); ?>
                                                                </p>
                                                            </article>
                                                            <article class="margin-b-20">
                                                                <h3 class="txt-sm txt-bold margin-b-10">
                                                                    Contact Email
                                                                </h3>
                                                                <p class="txt-normal-s" id="contactEmail">
                                                                    <?php echo rgar( $entry, 10 ); ?>
                                                                </p>
                                                            </article>
                                                            <article class="margin-b-20">
                                                               <h3 class="txt-sm txt-bold margin-b-10">
                                                                    Contact Phone
                                                                </h3>
                                                                <p class="txt-normal-s" id="contactPhone">
                                                                    <?php echo rgar( $entry, 11 ); ?>
                                                                </p>
                                                            </article>
                                                            <article class="">
                                                                <h3 class="txt-sm txt-bold margin-b-10">
                                                                    Whatsapp
                                                                </h3>
                                                                <p class="txt-normal-s" id="contactWhatsapp">
                                                                    <?php echo rgar( $entry, 12 ); ?>
                                                                </p>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 padding-lr-20 padding-b-40" id="offers">

                                                        <div class="row row-20 align-items-center margin-b-20">
                                                            <div class="col-md-8 padding-lr-20">
                                                                <h3 class="txt-bold">
                                                                    Training Offers
                                                                </h3>
                                                            </div>
                                                        </div>

                                                        <div class="overflow-hidden txt-normal-s txt-height-1-5">
                                                        <?php
                                                            $custom_query = new WP_Query();
                                                            $custom_query->query( 
                                                                array(
                                                                    'post_type' => 'training-offer',
                                                                    'post_status' => 'publish',
                                                                    'author' => $entry['created_by'],
                                                                    'posts_per_page' => -1,
                                                                ) 
                                                            );

                                                            if ( $custom_query->have_posts() ) {

                                                                while ($custom_query->have_posts()) : $custom_query->the_post();

                                                                /* Variables */
                                                                $post_id = $post->ID;    //Get Program ID

                                                                /* Publication status */      
                                                                $publication_meta = get_post_meta( $post_id, $publication_key, true );
                                                        ?>

                                                            <div class="row padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                                                                <div class="col-md-8 txt-color-lighter">
                                                                    <h4 class="txt-medium txt-color-dark">
                                                                        <?php the_title(); ?>
                                                                    </h4>
                                                                </div>
                                                                <div class="col-md-4 txt-medium txt-color-dark text-right">
                                                                    <a href="<?php the_permalink($child_post_id); ?>" class="txt-sm txt-underline txt-color-dark">
                                                                        View Offer
                                                                    </a>
                                                                </div>
                                                            </div>

                                                        <?php
                                                                endwhile;

                                                            }else{
                                                        ?>
                                                            <div class="padding-b-40">
                                                                <h2 class="txt-medium">
                                                                    This Trainer has not created any Training Offers.
                                                                </h2>
                                                            </div>   

                                                        <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>
    
   <!-- Coming Soon Modal -->
    <div class="modal fade font-main coming-soon-modal" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="content">
                    <div class="padding-tb-20 padding-lr-30">
                        <h1 class="txt-xlg txt-bold margin-b-20">
                            Contact Trainer
                        </h1>
                        <p class="txt-normal-s txt-height-1-7 margin-b-20">
                            Leave a message for this trainer.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
        /*$('#contactFormModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('#input_4_2').val(button.data('parentid')) //Parent ID
            modal.find('.trainer-name').text(button.data('name')) //Entry Name
            modal.find('#description').text(button.data('description')) //Entry Name
            modal.find('#contactPerson').text(button.data('contactperson')) //Entry Name
            modal.find('#contactAddress').text(button.data('contactaddress')) //Entry Name
            modal.find('#contactEmail').text(button.data('contactemail')) //Entry Name
            modal.find('#contactPhone').text(button.data('contactphone')) //Entry Name
            modal.find('#contactWhatsapp').text(button.data('contactwhatsapp')) //Entry Name
        })*/
    </script>