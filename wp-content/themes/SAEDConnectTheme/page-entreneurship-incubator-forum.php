<?php /*Template Name: Entrepreneurship Incubator Forum*/ ?>


<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main-content">
    <section class="container-wrapper padding-tb-40 border-b-1 border-color-darkgrey">
        <div class="row row-20">
            <div class="col-md-10 padding-lr-20">
                <h1 class="txt-3em margin-b-20">
                    <?php the_title() ?>
                </h1>
                <article class="">
                   <?php echo the_content(); ?>
                </article>
            </div>
            <div class="col-md-2 padding-lr-20">
                <img src="images/heroes/dance.png" alt="">
            </div>
        </div>        
   </section>
<?php endwhile; // end of the loop. ?>
   
   <section class="container-wrapper padding-tb-40 bg-yellow">
        <div class="row row-10">
               <?php
                    bbp_list_forums(array (
                        'before'              => '',
                        'after'               => '',
                        'link_before'         => '<div class="col-md-3 d-flex margin-b-80 padding-lr-10 padding-b-20 d-flex">
                <div class="topics-card ">
                    <article class="content full-width">
                        <div class="course-info">
                            <h3 class="title">',
                        'link_after'          => '</h3>
                            
                        </div>
                    </article>  
                </div>   
            </div>',
                        'count_before'        => '',
                        'count_after'         => '',
                        'count_sep'           => '',
                        'separator'           => '',
                        'forum_id'            => '1119',
                        'show_topic_count'    => false,
                        'show_reply_count'    => false,
                    ));    
                ?>
           
           
            <!--<div class="col-md-3 padding-lr-10 padding-b-20 d-flex">
                <a class="topics-card " href="">
                    <article class="content full-width">
                        <div class="course-info">
                            <h3 class="title">C# Essential Training</h3>
                            <h4 class="txt-sm margin-b-80">
                                <span class="txt-bold">Sposored By:</span>
                                <span class="name">Maero Uwede</span>
                            </h4>
                            <p class="txt-sm txt-height-1-5">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eos recusandae ea aspernatur illum unde sapiente esse aliquid eveniet omnis.
                            </p>
                        </div>
                    </article>  
                </a>   
            </div>-->
        </div>
    </section>
</main>

<?php get_footer() ?>
