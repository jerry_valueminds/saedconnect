<?php /*Template Name: Profile - NYSC State Validators*/ ?>
<?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Post Type */
        $postType = 'validation-program';

        /* Meta Key */
        $meta_key = 'nysc_assigned_state';

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Assign Users to <?php echo $_GET['form-title'] ?> state
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/validation/manage-nysc-state-validators/" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>" method="post">
                            <?php
                                $state_id = sanitize_text_field( $_GET['state-id'] );
                                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if( $_POST ){                                    
                                    /* Delete existing meta */
                                    $saved_users = get_users( array('blog_id' => 1, 'orderby' => 'nicename', 'meta_key' => $meta_key, 'meta_value' => $state_id, 'fields' => 'ids' ) );
                                    foreach($saved_users as $saved_user){
                                        delete_user_meta( $saved_user, $meta_key );
                                    }

                                    /* Save Submitted User Selection */
                                    $submitted_users = $_POST['users'];

                                    foreach($submitted_users as $submitted_user){
                                        add_user_meta( $submitted_user, $meta_key, $state_id);
                                    }
                                                                        
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                                }
                              
                                /* Get Saved Users */
                                $saved_users = get_users( array('blog_id' => 1, 'orderby' => 'nicename', 'meta_key' => $meta_key, 'meta_value' => $state_id, 'fields' => 'ids' ) );

                                /* Get All Users */
                                $blogusers = get_users( 'blog_id=1&orderby=nicename&role=subscriber' );
                            ?>
                            
                            <p class="txt-normal-s txt-medium margin-b-10">
                                Select users to assign
                            </p>
                            <select class="select-collaborators full-width" name="users[]" multiple="multiple">
                                <option 
                                    value="1"
                                    <?php echo ( in_array(1, $saved_users) ) ? "selected" : ""; ?>
                                >
                                    Super Admin
                                </option>
                            <?php foreach( $blogusers as $user ){ //delete_user_meta( $user->ID, $meta_key ); ?>

                                <option 
                                   value="<?php echo esc_html( $user->ID ); ?>"
                                   <?php echo ( in_array($user->ID, $saved_users) ) ? "selected" : ""; ?>
                                >
                                    <?php echo esc_html( $user->user_login ); ?>
                                </option>

                            <?php } ?>
                            </select>
                            <div class="text-right padding-t-20">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>
                </div>
               
            <?php } else { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        Manage NYSC State Validators
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Manage and assign NYSC Users to validation programs.
                    </p>
                </article>
                
                <div class="row row-10">
                    <?php 
                        /*
                        *
                        * Populate Form Data from Terms
                        *
                        */
                        //Get Terms
                        $terms = get_terms( 'state', array('hide_empty' => false));

                        foreach ($terms as $term) { //Cycle through terms, one at a time

                            // Check and see if the term is a top-level parent. If so, display it.
                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                            //$saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term_id, true );
                    ?>
                        <?php if( true ){ ?>
                        <div class="col-md-3 d-flex padding-lr-10 padding-b-20">
                            <a 
                                href="<?php currenturl(false) ?>?view=form&form-title=<?php echo $term_name ?>&state-id=<?php echo $term_id; ?>"
                                class="bg-grey col d-flex align-items-center justify-content-between txt-sm txt-medium padding-o-15 border-o-1 border-color-darkgrey"
                            >
                                <span class="txt-bold"><?php echo $term_name; ?></span>
                                <span>
                                    <i class="fa fa-user padding-r-5"></i>
                                    <?php 
                                        $saved_users = get_users( array('blog_id' => 1, 'orderby' => 'nicename', 'meta_key' => $meta_key, 'meta_value' => $term_id, 'fields' => 'ids' ) );
                                        echo count($saved_users);
                                    ?>
                                </span>                 
                            </a>
                        </div>
                        <?php } ?>
                    <?php } ?>                  
                </div>
                <?php } ?>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>