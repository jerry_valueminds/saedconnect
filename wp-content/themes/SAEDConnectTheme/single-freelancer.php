<?php get_header() ?>
    
<?php
    $current_user = wp_get_current_user();
    $gv_id = 8;
    $gv_id = 30;
    
    // TO SHOW THE POST CONTENT
    while ( have_posts() ) : the_post();

        $post_id = get_the_ID();
        $post_author_id = get_the_author_meta('ID');

        /* Get current User ID */
        $current_user = wp_get_current_user();

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                'key' => 'post_id', 'value' => $post_id, //Current logged in user
            )
          )
        );

        /* Get GF Entry Count */
        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

        foreach( $entries as $entry ){

            $entry_id = $entry['id'];
        }

        /*
        *
        *   Portfolio
        *
        */
        /* Get current User ID */
        $current_user = wp_get_current_user();

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => 'created_by', 'value' => $post_author_id, //Current logged in user
            )
          )
        );

        /* Get GF Entry Count */
        $portfolio_count = GFAPI::count_entries( 11, $search_criteria );
        $portfolio = GFAPI::get_entries( 11, $search_criteria );
    
?>

    <main class="main-content">
        <header class="container-wrapper bg-yellow padding-tb-15 margin-b-40">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="txt-sm">
                        <a href="marketplace_service_home.html" class="txt-color-dark padding-r-10">
                            Service Marketplace
                        </a>
                        <i class="fa fa-angle-right padding-r-10"></i>
                        <a href="marketplace_service_category.html" class="txt-color-dark">
                            Freelancers
                        </a>
                    </h1>
                </div>
            </div>
        </header>
        
        <section class="container-wrapper">
            <div class="row row-30 padding-b-40">
                <div class="col-md-8 padding-lr-30">
                    <div class="bg-grey padding-o-30 margin-b-20">
                        <div class="d-flex">
                            <figure class="margin-b-20 padding-r-20">
                                <?php
                                    $field = 'freelancer_image';

                                    $meta = get_post_meta($post->ID, $field, true);

                                    if($meta){
                                ?>
                                   
                                    <img class="profile-image" src="<?php echo $meta; ?>" alt="" width="80">    
                                        
                                <?php } ?>
                            </figure>
                            <div class="flex_1">
                                <h2 class="txt-xlg txt-medium margin-b-10">
                                    <?php
                                        the_title();
                                    ?>
                                </h2>
                                <p class="txt-sm">
                                    <?php
                                        $field = 'freelancer_summary';

                                        $meta = get_post_meta($post->ID, $field, true);

                                        if($meta){
                                            echo $meta;
                                        }
                                    ?>
                                </p>
                                <div class="padding-t-20">
                                <?php if( $post_author_id = $current_user->id ){ ?>
                                    <a 
                                        href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry_id.'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=marketplace&form-title='.$form_title.'&post_id='.$post_id; ?>"
                                        class="btn btn-trans-bw txt-xs no-m-b"
                                    >
                                        Edit My Profile
                                    </a>
                                <?php } else { ?>
                                    <a href="" class="btn btn-blue txt-xs no-m-b">
                                        Contact Me
                                    </a>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="border-t-1 border-color-darkgrey padding-t-30 margin-t-30">
                            <h3 class="txt-normal-s txt-bold margin-b-20">
                                About me
                            </h3>
                            <p class="txt-sm">
                                <?php
                                    $field = 'freelancer_about';

                                    $meta = get_post_meta($post->ID, $field, true);

                                    if($meta){
                                        echo $meta;
                                    }
                                ?>
                            </p>
                        </div>                  
                    </div>
                                        
                    <div class="border-o-1 border-color-darkgrey padding-o-30 margin-b-20">
                        <div class="row align-items-center margin-b-20">
                            <h3 class="col-8 txt-bold">
                                Skills
                            </h3>
                            <?php if( $post_author_id = $current_user->id ){ ?>
                            <div class="col-4 text-right">
                                <a href="" class="btn btn-trans-bw txt-xxs no-m-b">
                                    Add Skill
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                        <?php if( $post_author_id = $current_user->id ){ ?>
                        <ul class="txt-sm padding-t-20">
                            <li class="row padding-b-15 margin-b-15 border-b-1 border-color-darkgrey">
                                <a class="col-8 txt-medium" href="">
                                    Web Design
                                </a>
                                <span class="col-4 text-right">
                                    <a href="" class="txt-color-green">
                                        Edit
                                    </a>
                                    <span class="padding-lr-5">|</span>
                                    <a href="" class="txt-color-red">
                                        Delete
                                    </a>
                                </span>
                            </li>
                            <li class="row padding-b-15 margin-b-15 border-b-1 border-color-darkgrey">
                                <a class="col-8 txt-medium" href="">
                                    Graphic Design
                                </a>
                                <span class="col-4 text-right">
                                    <a href="" class="txt-color-green">
                                        Edit
                                    </a>
                                    <span class="padding-lr-5">|</span>
                                    <a href="" class="txt-color-red">
                                        Delete
                                    </a>
                                </span>
                            </li>
                            <li class="row">
                                <a class="col-8 txt-medium" href="">
                                    Fashion Design
                                </a>
                                <span class="col-4 text-right">
                                    <a href="" class="txt-color-green">
                                        Edit
                                    </a>
                                    <span class="padding-lr-5">|</span>
                                    <a href="" class="txt-color-red">
                                        Delete
                                    </a>
                                </span>
                            </li>
                        </ul>
                        <?php } else { ?>
                        <p class="txt-sm">
                            <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                Web Design
                            </a>
                            <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                Graphics Design
                            </a>
                            <a class="d-inline-block padding-tb-5 padding-lr-10 border-o-1 border-color-darkgrey" href="">
                                Fashion Design
                            </a>
                        </p>
                        <?php } ?>
                    </div>
                    
                    <div class="border-o-1 border-color-darkgrey padding-lr-30 padding-t-30 padding-b-20 margin-b-40">
                        <div class="row align-items-center margin-b-20">
                            <h3 class="col-8 txt-bold">
                                Portfolio
                            </h3>
                            <?php if( $post_author_id = $current_user->id ){ ?>
                            <div class="col-4 text-right">
                                <a href="" class="btn btn-trans-bw txt-xxs no-m-b">
                                    Edit
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row row-10">
                        <?php
                            foreach( $portfolio as $portfolioItem ){
                               // echo $portfolioItem;
                                //print_r($portfolioItem);
                                //echo '<br><br>';
                                
                                $images = rgar( $portfolioItem, '2' );
                                //echo $images;
                                //echo '<br><br>';
                                
                                $imageArray = $images;
                                
                                for( $i = 0; $i < count($imageArray); $i++ ){    
                                    //echo '<br><br>';
                                    //echo $imageArray[$i];
                        ?>
                           
                            <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                <img src="<?php echo $imageArray[$i] ?>" alt="">
                            </div>   
                            
                        <?php
                                }
                            }                            
                        ?>
                            
                            <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                <img src="images/heroes/honda_4.jpg" alt="">
                            </div>
                            <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                <img src="images/heroes/honda_5.jpg" alt="">
                            </div>
                            <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                <img src="images/heroes/honda_3.jpg" alt="">
                            </div>
                            <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                <img src="images/heroes/honda_4.jpg" alt="">
                            </div>
                            <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                <img src="images/heroes/honda_5.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row align-items-center margin-b-20">
                        <h3 class="col-8 txt-bold txt-xlg">
                            My Offers
                        </h3>
                        <?php if( $post_author_id = $current_user->id ){ ?>
                        <div class="col-4 text-right">
                            <a href="" class="btn btn-trans-bw txt-xxs no-m-b">
                                Add Offer
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                    
                    <ul class="row row-10 marketplace-service-cards">
                    <?php
                        // Create Query Argument
                        $args = array(
                            'post_type' => 'offer-a-service',
                            'showposts' => -1,
                            'meta_query' => $meta_array,
                        );


                        $service_query = new WP_Query($args);

                        while ($service_query->have_posts()) : $service_query->the_post();

                        /* Get Post ID */
                        $post_id = $post->ID 
                    ?>
                        <li class="col-sm-6 col-lg-4 padding-lr-10 padding-b-20">
                            <a href="<?php the_permalink() ?>">
                                <?php
                                    $field = 'offer_image';

                                    $meta = get_post_meta($post->ID, $field, true);

                                    if($meta){
                                ?>

                                    <figure class="service-img" style="background-image:url('<?php echo $meta; ?>');">

                                    </figure>   

                                <?php } else { ?>
                                    <figure class="service-img" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/table.jpg');">

                                    </figure>
                                <?php } ?>
                                <div class="content">
                                    <div class="top">
                                        <div class="avatar">
                                            <?php
                                                $field = 'offer_avatar';

                                                $meta = get_post_meta($post->ID, $field, true);

                                                if($meta){
                                            ?>

                                                <figure style="background-image:url('<?php echo $meta; ?>');">

                                                </figure>   

                                            <?php } else { ?>
                                                <figure class="freelancer-img" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png');">

                                                </figure>
                                            <?php } ?>
                                            <div class="name">
                                                Prince Adekunle
                                            </div>
                                        </div>
                                        <div class="title">
                                            <?php the_title() ?>
                                        </div>
                                    </div>
                                    <div class="bottom">
                                        <div class="location">
                                            <?php
                                                $field = 'offer_locations';

                                                $meta = get_post_meta($post->ID, $field, false);

                                                if($meta){
                                                    foreach($meta as $key=>$value) {
                                                        echo $value;
                                                        if($key < count($meta) - 1 ){
                                                            echo ', ';
                                                        }
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <?php
                                            $field = 'offer_price';

                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                        ?>
                                            <div class="post-feen text-right">
                                                Startting at
                                                <span class="txt-color-dark txt-bold">
                                                    <?php echo '₦'.$meta; ?>
                                                </span>
                                            </div>   
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </a>   
                        </li>
                    <?php
                        endwhile;
                        wp_reset_postdata();
                    ?>
                    </ul>
                </div>
                <div class="col-md-3 padding-lr-30">
                    <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                        <h3 class="txt-bold margin-b-15">
                            Availability
                        </h3>
                        <p class="txt-sm">
                            <?php
                                $field = 'freelancer_availability';

                                $meta = get_post_meta($post->ID, $field, false);

                                if($meta){
                                    foreach($meta as $key=>$value) {
                                        echo $value;
                                        if($key < count($meta) - 1 ){
                                            echo ', ';
                                        }
                                    }
                                }
                            ?>
                        </p>
                    </div>
                    <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                        <h3 class="txt-bold margin-b-15">
                            Areas Served
                        </h3>
                        <p class="txt-sm">
                            <?php
                                $field = 'freelancer_locations';

                                $meta = get_post_meta($post->ID, $field, false);

                                if($meta){
                                    foreach($meta as $key=>$value) {
                                        echo $value;
                                        if($key < count($meta) - 1 ){
                                            echo ', ';
                                        }
                                    }
                                }
                            ?>
                        </p>
                    </div>
                    <div class="padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
                        <h3 class="txt-bold margin-b-15">
                            Portfolio Link
                        </h3>
                        <p class="txt-sm">
                            <?php
                                $field = 'freelancer_portfolio_link';

                                $meta = get_post_meta($post->ID, $field, true);

                                if($meta){
                            ?>
                            
                            <a href="<?php echo $meta; ?>" target="_blank">
                                <?php echo $meta; ?>
                            </a>
                                            
                            <?php
                                }
                            ?>
                        </p>
                    </div>
                    <div class="">
                        <h3 class="txt-bold margin-b-15">
                            Verification
                        </h3>
                        <div class="row align-items-center padding-b-10 margin-b-10 border-b-1 border-color-darkgrey">
                            <p class="col-8 txt-sm">
                                Phone Number
                            </p>
                            <p class="col-4 txt-sm text-right txt-color-green">
                                <span class="fa-stack">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                                </span>
                            </p>
                        </div>
                        <div class="row align-items-center padding-b-10 margin-b-10">
                            <p class="col-8 txt-sm">
                                Email
                            </p>
                            <p class="col-4 txt-sm text-right txt-color-red">
                                <span class="fa-stack">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-minus fa-stack-1x fa-inverse"></i>
                                </span>
                            </p>
                        </div>
                        
                        <?php if( $post_author_id = $current_user->id ){ ?>
                        <div class="">
                            <a href="" class="btn btn-trans-bw txt-xs no-m-b">
                                Request Verification
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="padding-t-40">
                        <h3 class="txt-bold margin-b-15">
                            Other Freelancers
                        </h3>
                        <ul class="marketplace-freelancer-cards">
                        <?php
                            // Create Query Argument
                            $args = array(
                                'post_type' => 'freelancer',
                                'showposts' => 4,
                                'meta_query' => $meta_array,
                            );


                            $freelancer_query = new WP_Query($args);

                            while ($freelancer_query->have_posts()) : $freelancer_query->the_post();

                            /* Get Post ID */
                            $post_id = $post->ID 
                        ?>
                            <li class="padding-b-10">
                                <a href="<?php the_permalink() ?>">
                                    <span class="wrapper">
                                        <span class="image">
                                        <?php
                                            $field = 'freelancer_image';

                                            $meta = get_post_meta($post->ID, $field, true);

                                            if($meta){
                                        ?>

                                            <figure class="freelancer-img" style="background-image:url('<?php echo $meta; ?>');">

                                            </figure>   

                                        <?php } else { ?>
                                            <figure class="freelancer-img" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png');">

                                            </figure>
                                        <?php } ?>
                                        </span>
                                        <div class="content">
                                            <div class="name">
                                                <?php the_title() ?>
                                            </div>
                                            <div class="bottom">
                                                <div class="location">
                                                    <i class="fa fa-map-marker"></i>
                                                    <?php
                                                        $field = 'freelancer_locations';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) - 1 ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                </a> 
                            </li>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
<?php
    endwhile; //resetting the page loop
?>

<?php get_footer() ?>