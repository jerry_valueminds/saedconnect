<?php 
/*
Template Name: CV
Template Post Type: gravityview
*/
?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php
                    
        if ( $_REQUEST['status'] == 'deleted' ) {
            // If Entry is deleted
            $link = 'https://www.saedconnect.org/service-provider-directory/trainer-profile/';
            
            if ( wp_redirect( $link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content">
                <div class="page-header">
                    <h1 class="page-title">
                        My CV
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Your Personal Profile is the Key information about you which would be required by potential employers
                    </p>
                </article>
                
                <!-- Trainer Information -->
                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_GET['form-title']; ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            while ( have_posts() ) : the_post();
                                echo the_content();
                            endwhile;
                            wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <script>
        function goBack() {
            window.history.back();
        }
    </script>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>

