<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1525950055587" ID="ID_1074265186" MODIFIED="1525950069394" TEXT="Do Biz Sections and Topics">
<node CREATED="1524490858914" FOLDED="true" ID="ID_1405311726" MODIFIED="1525950072037" POSITION="right" TEXT="Getting Started">
<node CREATED="1524492686345" ID="ID_595332183" MODIFIED="1524492874490" TEXT="What is Entrepreneurship?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 32
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524492726753" ID="ID_1307129011" MODIFIED="1524492899497" TEXT="Who is an Entrepreneur?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 32 - 33
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524490995888" ID="ID_678417392" MODIFIED="1524492943985" TEXT="Why you should think about Starting a business">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 33 - 34
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="idea"/>
<node CREATED="1524492233309" ID="ID_1296177225" MODIFIED="1524492248390" TEXT="Advantages &amp; Disadvantages of entrepreneurship"/>
</node>
<node CREATED="1524491023452" ID="ID_1993217434" MODIFIED="1524491032158" TEXT="What is stopping you?">
<icon BUILTIN="idea"/>
</node>
<node CREATED="1524491038812" FOLDED="true" ID="ID_193983628" MODIFIED="1525018477599" TEXT="Do you have what it takes?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 34 - 43
    </p>
  </body>
</html></richcontent>
<node CREATED="1524492271901" ID="ID_1531417907" MODIFIED="1524492282136" TEXT="Characteristics of successful entrepreneurs"/>
<node CREATED="1524492290149" ID="ID_476329411" MODIFIED="1524492304635" TEXT="Skills &amp; Education that is useful to entrepreneurs"/>
<node CREATED="1524491062148" ID="ID_249302936" MODIFIED="1524493019354" TEXT="Entrepreneur Self Assessments">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 44-46
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524493024333" ID="ID_299173276" MODIFIED="1524493063999" TEXT="Building your entrepreneurship Skills">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 46-47
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1524492160324" ID="ID_358574413" MODIFIED="1524492171891" TEXT="What you need to know about business">
<node CREATED="1524492171891" ID="ID_1799598766" MODIFIED="1524492184262" TEXT="Types of businesses (product/service, etc)"/>
</node>
<node CREATED="1524491160387" ID="ID_1984531751" MODIFIED="1524491176392" TEXT="The process: From decision to start-up"/>
<node CREATED="1524491101632" ID="ID_1733361866" MODIFIED="1524491114475" TEXT="Deciding what Business to go into"/>
</node>
<node CREATED="1524490873008" FOLDED="true" ID="ID_721448641" MODIFIED="1525161445845" POSITION="right" TEXT="Finding your Business Idea">
<node CREATED="1524491204791" ID="ID_220551116" MODIFIED="1524493135670" TEXT="What is a business idea?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 49-50
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491214904" ID="ID_551483034" MODIFIED="1524493217485" TEXT="Identifying a Business Opportunity">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 51-3
    </p>
  </body>
</html></richcontent>
<node CREATED="1524493234138" ID="ID_1155027532" MODIFIED="1524493316940" TEXT="Draw up an idea list">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 53
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524493316934" ID="ID_551885398" MODIFIED="1524493330412" TEXT="Using Experiences">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 54
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524493332111" ID="ID_1333922626" MODIFIED="1524493349602" TEXT="Investigating your environment">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 54
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524493366853" ID="ID_650982382" MODIFIED="1524493377128" TEXT="Brainstorming">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 54
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1524491225939" ID="ID_1957268393" MODIFIED="1524493168168" TEXT="Is this a good Idea? - Validating your business Idea">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 50-51
    </p>
  </body>
</html></richcontent>
<node CREATED="1524493563568" ID="ID_146075846" MODIFIED="1524493600245" TEXT="Screen your idea list">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 55
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524493602243" ID="ID_334303108" MODIFIED="1524493628230" TEXT="Perform a SWOT Analysis">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 56
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1524491235247" ID="ID_127946235" MODIFIED="1524491241737" TEXT="Crafting a workable solution"/>
<node CREATED="1524493628950" ID="ID_486301669" MODIFIED="1524493662103" TEXT="Business Idea Directory">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 57-58
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1524490895297" FOLDED="true" ID="ID_1292342225" MODIFIED="1525018317458" POSITION="right" TEXT="Thinking through your  business">
<node CREATED="1524493663584" ID="ID_1780741162" MODIFIED="1525017606643" TEXT="What is a business plan?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 60
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524493688693" ID="ID_298656529" MODIFIED="1525017606648" TEXT="Why is business planning necessary?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 60-61
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524493758342" ID="ID_1872617905" MODIFIED="1524493784033" TEXT="Sample Questions Business Planning Answers">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 61
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524493863358" ID="ID_461673122" MODIFIED="1524494785511" TEXT="Getting Information about  a business ">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 62
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491435401" ID="ID_145762825" MODIFIED="1524491443535" TEXT="Introduction to business models"/>
<node CREATED="1524493857485" ID="ID_72741305" MODIFIED="1525017654898" TEXT="Clarifying your customer segment">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 79
    </p>
  </body>
</html></richcontent>
<node CREATED="1524494823764" ID="ID_384254395" MODIFIED="1524494926225" TEXT="Understanding your custtomer">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 81-83
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1524491355288" ID="ID_578171571" MODIFIED="1524494611914" TEXT="Clarifying your value proposition &amp; ffering">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 80
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491409902" ID="ID_1457642881" MODIFIED="1524491421617" TEXT="Clarifying your revenue model"/>
<node CREATED="1525017797380" ID="ID_895339576" MODIFIED="1525017835813" TEXT="Clarifying how you will get, keep &amp; grow your customers"/>
<node CREATED="1524491613095" ID="ID_1441278033" MODIFIED="1524495130334" TEXT="Identifying your key activities">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 95
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491581527" ID="ID_1140772746" MODIFIED="1524494624081" TEXT="Clarifying your resource requirements">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 80-81
    </p>
  </body>
</html></richcontent>
<node CREATED="1524495035567" ID="ID_519044326" MODIFIED="1525017873752" TEXT="Estimating your Human Resource requirement">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 94-95
    </p>
  </body>
</html></richcontent>
<node CREATED="1524495132038" ID="ID_442696622" MODIFIED="1524495172756" TEXT="Writing Job Description">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 96 - 97
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1525017874697" ID="ID_891574853" MODIFIED="1525017888769" TEXT="Estimating your other resource requirements"/>
</node>
<node CREATED="1524491595803" ID="ID_965442752" MODIFIED="1524491605672" TEXT="Clarifying your partnership requirements"/>
<node CREATED="1524491656348" ID="ID_1990892149" MODIFIED="1524495034744" TEXT="Determining a price for your product/service offering">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 85 - 89
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491687119" ID="ID_1592228877" MODIFIED="1524494242382" TEXT="Articulating your costs">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 65
    </p>
  </body>
</html></richcontent>
<node CREATED="1524491966559" ID="ID_403822262" MODIFIED="1524491980477" TEXT="Articulating your initial capital requirement"/>
<node CREATED="1524491951995" ID="ID_606063213" MODIFIED="1524492080735" TEXT="Articulating your monthly and annual cost"/>
</node>
<node CREATED="1524491799427" FOLDED="true" ID="ID_1085425158" MODIFIED="1525018281180" TEXT="Articulating your Business Identity">
<node CREATED="1524491816232" ID="ID_1790325222" MODIFIED="1525018163955" TEXT="Articulating your Vision &amp; Mission"/>
<node CREATED="1524491821905" ID="ID_832639273" MODIFIED="1525018170881" TEXT="Articulating your Culture &amp; Values"/>
</node>
<node CREATED="1524491264970" ID="ID_588628335" MODIFIED="1524491273674" TEXT="Defining your starting point"/>
</node>
<node CREATED="1525017517068" FOLDED="true" ID="ID_1573127771" MODIFIED="1525950040543" POSITION="right" TEXT="Putting together your Business Plan">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 71-75
    </p>
  </body>
</html></richcontent>
<node CREATED="1524490956649" ID="ID_1765980918" MODIFIED="1524491329578" TEXT="Types of Business Plans">
<node CREATED="1524490918740" ID="ID_615834599" MODIFIED="1524491329578" TEXT="One page Business Plans"/>
<node CREATED="1524490926463" ID="ID_1183799621" MODIFIED="1524491329577" TEXT="Full Business Plans"/>
</node>
<node CREATED="1524491469089" ID="ID_1819978729" MODIFIED="1524491476480" TEXT="Performing a competitive analysis">
<node CREATED="1524491483157" ID="ID_1615959227" MODIFIED="1524491485291" TEXT="Why?"/>
<node CREATED="1524491485746" ID="ID_431763077" MODIFIED="1524491487884" TEXT="How?"/>
<node CREATED="1524491488668" ID="ID_29418477" MODIFIED="1524491500847" TEXT="Tips &amp; Tricks"/>
</node>
<node CREATED="1524491381727" ID="ID_1976316068" MODIFIED="1524494703470" TEXT="Crafting your marketing  plan">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 81
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491726600" FOLDED="true" ID="ID_123492136" MODIFIED="1525018071807" TEXT="Putting together your financials">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 66
    </p>
  </body>
</html></richcontent>
<node CREATED="1524491687119" ID="ID_254031527" MODIFIED="1524494242382" TEXT="Articulating your costs">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 65
    </p>
  </body>
</html></richcontent>
<node CREATED="1524491966559" ID="ID_1779304177" MODIFIED="1524491980477" TEXT="Articulating your initial capital requirement"/>
<node CREATED="1524491951995" ID="ID_1163771271" MODIFIED="1524492080735" TEXT="Articulating your monthly and annual cost"/>
</node>
<node CREATED="1524494247889" ID="ID_151672350" MODIFIED="1524494280037" TEXT="Articulating your expected revenue">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 65
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491936893" ID="ID_1024992315" MODIFIED="1524494242383" TEXT="Computing your breakeven analysis"/>
<node CREATED="1524492021807" ID="ID_310453384" MODIFIED="1524492030797" TEXT="Other required financial ratios"/>
</node>
<node CREATED="1525018212701" ID="ID_808478401" MODIFIED="1525018222890" TEXT="Business Plan Templates"/>
</node>
<node CREATED="1524490942971" FOLDED="true" ID="ID_547631133" MODIFIED="1525950040556" POSITION="right" TEXT="Getting Finance">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 67
    </p>
  </body>
</html></richcontent>
<node CREATED="1524494363199" ID="ID_1479393741" MODIFIED="1524494401698" TEXT="Getting Equity">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 68
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524494370440" ID="ID_527984131" MODIFIED="1524494397117" TEXT="Getting Loans">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 68-69
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524494373874" ID="ID_1349593201" MODIFIED="1524494420083" TEXT="Getting Grants">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 70
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1524491845153" FOLDED="true" ID="ID_1932688168" MODIFIED="1525019273451" POSITION="right" TEXT="Preparing to start">
<node CREATED="1524491866803" ID="ID_1556369619" MODIFIED="1525018434233" TEXT="Implementation Planning">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 70
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491858324" ID="ID_235268493" MODIFIED="1524495235628" TEXT="Team Building">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 98-100
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524491827919" ID="ID_122991321" MODIFIED="1524491833009" TEXT="Logo &amp; Branding"/>
</node>
<node CREATED="1524491879718" FOLDED="true" ID="ID_1218444773" MODIFIED="1525950074119" POSITION="right" TEXT="Business Registration &amp; other Legals">
<node CREATED="1524495236408" ID="ID_601388690" MODIFIED="1525018370039" TEXT="Work Contracts">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 101
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1525018373743" ID="ID_1017852003" MODIFIED="1525018380232" TEXT="Registering your Business"/>
</node>
<node CREATED="1524495274556" FOLDED="true" ID="ID_534821063" MODIFIED="1525950049847" POSITION="right" TEXT="Primary Skills for Business Success">
<node CREATED="1524495284738" ID="ID_1040435178" MODIFIED="1524495324641" TEXT="Record Keeping">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 103-109
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524495326073" ID="ID_947330187" MODIFIED="1524495355726" TEXT="Saving">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entrepreneurship Development Training Manual - Pg 111 = 116
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1524495357676" ID="ID_1035010980" MODIFIED="1524495363334" TEXT="Cashflow Management"/>
</node>
</node>
</map>
