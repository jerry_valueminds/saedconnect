<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1519752696871" ID="ID_727780041" MODIFIED="1526057437883" TEXT="Get a Job">
<node CREATED="1526057456138" FOLDED="true" ID="ID_1603785876" MODIFIED="1526404818843" POSITION="right" TEXT="Getting Started">
<node CREATED="1526057498174" ID="ID_1705689869" MODIFIED="1526057505783" TEXT="Should I actually get a Job?">
<node CREATED="1526057520132" ID="ID_73625715" MODIFIED="1526057530425" TEXT="your options"/>
<node CREATED="1526057531542" ID="ID_335381024" MODIFIED="1526057543177" TEXT="Advantages &amp; disadvantages of a Job"/>
</node>
<node CREATED="1526057475825" ID="ID_1959726803" MODIFIED="1526057485897" TEXT="What Job is right for me?"/>
<node CREATED="1526057561407" ID="ID_22699683" MODIFIED="1526057570648" TEXT="What employers are looking for"/>
<node CREATED="1526058355752" ID="ID_370008857" MODIFIED="1526058375658" TEXT="Key Things to do before you start searching for a Job"/>
</node>
<node CREATED="1519753179196" FOLDED="true" ID="ID_1893491134" MODIFIED="1526404835926" POSITION="right" TEXT="Crafting your CV &amp; Other tips to draw employer attention">
<node CREATED="1519752961763" FOLDED="true" ID="ID_1289497280" MODIFIED="1526404343935" TEXT="Before you start writing your CV:: What you need to know">
<node CREATED="1524559631454" ID="ID_384147315" MODIFIED="1524559651608" TEXT="what is a CV"/>
<node CREATED="1524559657837" ID="ID_1517278177" MODIFIED="1524559665777" TEXT="Types of CVs"/>
<node CREATED="1524559668016" ID="ID_1829483939" MODIFIED="1524559868600" TEXT="The different sections of a CV"/>
<node CREATED="1526403487906" ID="ID_385052137" MODIFIED="1526403520697" TEXT="Email Etiquette when sending a CV">
<node CREATED="1526404085832" ID="ID_1363089895" MODIFIED="1526404141296" TEXT="what do i put as the subject header when sending my CV via mail"/>
</node>
<node CREATED="1526403524456" ID="ID_761577623" MODIFIED="1526403575889" TEXT="CV vs Resume: the difference "/>
</node>
<node CREATED="1524557881804" FOLDED="true" ID="ID_1862903719" MODIFIED="1526403586132" TEXT="How to Showcase your best self" VSHIFT="-1">
<node CREATED="1519752724853" ID="ID_1256171938" MODIFIED="1519752748104" TEXT="Crafting your Personal  details"/>
<node CREATED="1519752751444" ID="ID_988129625" MODIFIED="1519752757880" TEXT="Writing about your education"/>
<node CREATED="1519752758303" ID="ID_1729578416" MODIFIED="1519752891353" TEXT="Writing about your work experience"/>
<node CREATED="1519752774838" ID="ID_1839599088" MODIFIED="1519752791723" TEXT="Writing about your achievementts, interests &amp; additionl skills"/>
<node CREATED="1519752797225" ID="ID_1501869923" MODIFIED="1519752803605" TEXT="Writing about your references"/>
<node CREATED="1519752823785" ID="ID_1640506885" MODIFIED="1519752828076" TEXT="CV checklist"/>
</node>
<node CREATED="1524557867760" FOLDED="true" ID="ID_995933212" MODIFIED="1526402620954" TEXT="How should I format my CV?">
<node CREATED="1524559881589" ID="ID_1887287867" MODIFIED="1524559933171" TEXT="Functional CV"/>
<node CREATED="1524559934638" ID="ID_1404390938" MODIFIED="1524559989644" TEXT="Performance CV"/>
<node CREATED="1524559993101" ID="ID_1140611571" MODIFIED="1524560000434" TEXT="Graduate CV"/>
</node>
<node CREATED="1519753455654" FOLDED="true" ID="ID_1765906667" MODIFIED="1526403589027" TEXT="Hacks for your CV">
<node CREATED="1519753468760" ID="ID_869711257" MODIFIED="1519753486724" TEXT="Resume Action Words"/>
<node CREATED="1524558975486" ID="ID_28963045" MODIFIED="1524558983715" TEXT="Resume Tips"/>
</node>
<node CREATED="1519752708293" ID="ID_1674422648" MODIFIED="1519752812455" TEXT="Layout &amp; Design tips"/>
<node CREATED="1524654504398" FOLDED="true" ID="ID_533392772" MODIFIED="1526403591973" TEXT="Managing your Online Image( Social media)">
<node CREATED="1524654539807" ID="ID_424985842" MODIFIED="1524654543950" TEXT="Facebook"/>
<node CREATED="1524654545646" ID="ID_1983485350" MODIFIED="1524654549157" TEXT="Twitter"/>
<node CREATED="1524654551183" ID="ID_515183974" MODIFIED="1524654558088" TEXT="Instragram "/>
</node>
<node CREATED="1526058765368" ID="ID_1402887925" MODIFIED="1526058772307" TEXT="Setting up your LInkedin Profile"/>
</node>
<node CREATED="1524484863629" FOLDED="true" ID="ID_766457648" MODIFIED="1526408257948" POSITION="right" TEXT="Crafting Your Cover Letter">
<node CREATED="1524564307364" ID="ID_1812936732" MODIFIED="1526404146491" TEXT="Before you start writing your cover letter: what you need to know">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The Fundamentals of a Cover Letter
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1524564884140" ID="ID_1166224303" MODIFIED="1524564893911" TEXT="what is a cover Letter?"/>
<node CREATED="1524564895793" FOLDED="true" ID="ID_596221581" MODIFIED="1526404267417" TEXT="How is a cover letter structured?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The key component of a good Cover Letter
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1524564825111" ID="ID_220393127" MODIFIED="1526401167043" TEXT="The Address"/>
<node CREATED="1524564833594" ID="ID_684025200" MODIFIED="1526404215064" TEXT="The Salutation/ greeting  "/>
<node CREATED="1524564843033" ID="ID_587154144" MODIFIED="1526404229502" TEXT="The Introduction "/>
<node CREATED="1524564851705" ID="ID_209183246" MODIFIED="1526404244783" TEXT="The Body of the cover letter"/>
<node CREATED="1524564862249" ID="ID_458897851" MODIFIED="1526404256138" TEXT="The conclusion "/>
<node CREATED="1526403651001" ID="ID_686314365" MODIFIED="1526404262315" TEXT="The sign off"/>
</node>
<node CREATED="1526400422388" ID="ID_1356582025" MODIFIED="1526400436909" TEXT="what is the purpose of a Cover letter"/>
<node CREATED="1526400439897" ID="ID_734328683" MODIFIED="1526400455419" TEXT="How important is a Cover Letter to me"/>
<node CREATED="1526403472337" FOLDED="true" ID="ID_1911086605" MODIFIED="1526404273492" TEXT="Email Etiquette when sending a cover letter">
<node CREATED="1526404156521" ID="ID_1916446647" MODIFIED="1526404162036" TEXT="what do i put as the subject header when sending my cover letter via mail"/>
</node>
</node>
<node CREATED="1524564786093" FOLDED="true" ID="ID_656642350" MODIFIED="1526403673548" TEXT="Selling your best self: The cover letter make up">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The structure of a cover letter
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1526400578306" ID="ID_955454831" MODIFIED="1526400627606" TEXT="writing your address and recipient&apos;s address"/>
<node CREATED="1524564833594" ID="ID_640014190" MODIFIED="1526402707480" TEXT="writing the proper greeting for cover letter?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The Proper Greeting for a Cover Letter
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1524564843033" ID="ID_1782742002" MODIFIED="1526402750569" TEXT="starting with a bang: writing a catchy opening paragraph">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Writing a Catchy Opening Paragraph
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1524564851705" ID="ID_1114207396" MODIFIED="1526402785234" TEXT="Selling your best self: writing to your strengths">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The Body of the cover letter
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1524564862249" ID="ID_813597533" MODIFIED="1526402810583" TEXT="writing the cover letter closing paragraph that will get you hired">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The Closing Paragraph
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1526401308113" ID="ID_994557928" MODIFIED="1526401369859" TEXT="how do i sign off from my cover letter?"/>
</node>
<node CREATED="1524564345336" ID="ID_115308582" MODIFIED="1526404798571" TEXT="Secrets to writing a good cover letter">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cover letter tips
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1526403998680" ID="ID_1001511175" MODIFIED="1526404762364" TEXT="should i be official in writing my cover letter?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      what language tone should i use in writing my cover letter
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1526404051370" ID="ID_39294360" MODIFIED="1526404051370" TEXT=""/>
</node>
</node>
<node CREATED="1526057353272" FOLDED="true" ID="ID_329176039" MODIFIED="1526408260666" POSITION="right" TEXT="Searching for Jobs &amp; Securing interviews">
<node CREATED="1526059378756" ID="ID_657473269" MODIFIED="1526059391890" TEXT="How to find companies with Job openings"/>
<node CREATED="1526059401837" ID="ID_1621593263" MODIFIED="1526059449835" TEXT="How do I find Juicy  and well paying Jobs?"/>
<node CREATED="1524654612428" ID="ID_1582757950" MODIFIED="1526404853183" TEXT="Using Online Job sites effectively"/>
<node CREATED="1524654419631" ID="ID_1908690040" MODIFIED="1526058634665" TEXT="Using Employment Agencies to land interviews"/>
<node CREATED="1524654444238" ID="ID_600836556" MODIFIED="1526060455953" TEXT="Networking your way to a Job"/>
<node CREATED="1526058670031" ID="ID_128156511" MODIFIED="1526058680844" TEXT="Other Job Search hacks"/>
<node CREATED="1526059481591" ID="ID_991999795" MODIFIED="1526060470662" TEXT="Applying for a Job"/>
<node CREATED="1526059493025" ID="ID_1801715517" MODIFIED="1526059501170" TEXT="Following up your application"/>
<node CREATED="1524654671127" FOLDED="true" ID="ID_553987828" MODIFIED="1526404879544" TEXT="Using Volunteering to improve your chances to get an interview">
<node CREATED="1524654750749" ID="ID_1210173424" MODIFIED="1524654768040" TEXT="How to find Volunteer work"/>
</node>
<node CREATED="1526060086975" FOLDED="true" ID="ID_911281533" MODIFIED="1526404881588" TEXT="Other tips for Signalling your strengths ">
<node CREATED="1526060197947" ID="ID_1273115732" MODIFIED="1526060216695" TEXT="Showcase subject matter expertise online"/>
<node CREATED="1526060244642" ID="ID_623907760" MODIFIED="1526060261556" TEXT="Showcase your softskills online">
<node CREATED="1526060276804" ID="ID_1790334775" MODIFIED="1526060298123" TEXT="Participating in industry events"/>
</node>
</node>
</node>
<node CREATED="1524484887630" FOLDED="true" ID="ID_542804791" MODIFIED="1526408263043" POSITION="right" TEXT="Acing Interviews">
<node CREATED="1524486215591" HGAP="36" ID="ID_596034153" MODIFIED="1526405151997" TEXT="what you need to know before going for that interview " VSHIFT="41">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The Basics
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1524486217599" ID="ID_1641100927" MODIFIED="1524486419153" TEXT="Types of Interviews">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 1" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Two Types of Interviews<br /></b></span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-size: 12.000000pt; font-family: TimesNewRomanPSMT">1. </span></font><font size="12.000000pt" face="TimesNewRomanPS"><span style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS"><b>Screening interviews </b></span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-size: 12.000000pt; font-family: TimesNewRomanPSMT">(either in person or by phone) </span></font>
          </p>
          <p>
            <font size="12.000000pt" face="SymbolMT"><span style="font-size: 12.000000pt; font-family: SymbolMT">&#61623; </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-size: 12.000000pt; font-family: TimesNewRomanPSMT">Used to reduce the size of the applicant pool </span></font>
          </p>
          <p>
            <font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-size: 12.000000pt; font-family: TimesNewRomanPSMT">&#160;</span></font><font size="12.000000pt" face="TimesNewRomanPS"><span style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS"><b>2. Job Interview </b></span></font>
          </p>
          <p>
            <font size="12.000000pt" face="SymbolMT"><span style="font-size: 12.000000pt; font-family: SymbolMT">&#61623; </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-size: 12.000000pt; font-family: TimesNewRomanPSMT">Used to check the fit of the candidate and make a hiring decision </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1524486225543" ID="ID_740649491" MODIFIED="1526408045108" TEXT="Interview Styles">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 1" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="TimesNewRomanPS" size="12.000000pt"><b>Three Common Interview Styles </b></font>
          </p>
          <ol style="list-style-type: decimal">
            <li style="font-size: 12.000000pt; font-family: TimesNewRomanPSMT">
              <p>
                <font face="TimesNewRomanPS" size="12.000000pt"><b>Situational Interviews </b></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font face="SymbolMT" size="12.000000pt">&#61623; &#160;</font><font face="TimesNewRomanPSMT" size="12.000000pt">Here the interviewer presents a hypothetical situation that calls for some sort of action or decision. Then he/she asks how the candidate would respond. </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font face="SymbolMT" size="12.000000pt">&#61623; &#160;</font><font face="TimesNewRomanPSMT" size="12.000000pt">Don&#8217;t respond too quickly. Think and reflect first. Then carefully choose your words, but answer directly. Use success statements. </font>
                  </p>
                </li>
              </ul>
            </li>
            <li style="font-size: 12.000000pt; font-family: TimesNewRomanPSMT">
              <p>
                <font face="TimesNewRomanPS" size="12.000000pt"><b>Behavioral Interviews </b></font><font face="TimesNewRomanPSMT" size="12.000000pt">(the best predictor of future behavior is past behavior) </font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font face="SymbolMT" size="12.000000pt">&#61623; &#160;</font><font face="TimesNewRomanPSMT" size="12.000000pt">The interviewer asks you about real life situations and asks you how you responded to these situations. Use accomplishment statements. </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font face="SymbolMT" size="12.000000pt">&#61623; &#160;</font><font face="TimesNewRomanPSMT" size="12.000000pt">If your past actions were effective and added benefit to the situation, your future actions will likely do the same. </font>
                  </p>
                </li>
              </ul>
            </li>
            <li style="font-size: 12.000000pt; font-family: TimesNewRomanPSMT">
              <p>
                <font face="TimesNewRomanPS" size="12.000000pt"><b>Panel (team) Interviews </b></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font face="SymbolMT" size="12.000000pt">&#61623; &#160;</font><font face="TimesNewRomanPSMT" size="12.000000pt">Several people attend and all ask questions. They may ask all types of questions. Hypothetical situations; real life situations, candidates&#8217; background; future expectations. </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font face="SymbolMT" size="12.000000pt">&#61623; &#160;</font><font face="TimesNewRomanPSMT" size="12.000000pt">Be sure you address all the panel members. Don&#8217;t just focus on one person. All will evaluate you. Sometimes the quietest member carries the most weight in terms of evaluation. </font>
                  </p>
                </li>
              </ul>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1524485179724" ID="ID_1604880435" MODIFIED="1526407454918" TEXT="Before the interview">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Before the interview
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1524485120885" ID="ID_1291251835" MODIFIED="1526407389195" TEXT="Preparing for the Job interview ">
<node CREATED="1524485438947" ID="ID_260493191" MODIFIED="1524486347250" TEXT="Main Article">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 1" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Preparation for the Job Interview </b></span></font>
              </p>
            </li>
            <p>
              <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>1. Understand yourself and what you would like to do vocationally-think about your goals </b></span></font>
            </p>
            <p>
              <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Identify you key transferable marketable skills? You have a lot of these as Liberal Arts students. Ex: Coordinating, Innovating, Analyzing, Supervising, Instructing, Serving, Compiling, etc. </span></font>
            </p>
            <p>
              <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>2. Create your career objective and be able to articulate it </b></span></font>
            </p>
            <p>
              <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Craft an &#8220;elevator speech&#8221; that summarizes where you have been, where you are going, and why you are interviewing for this position. </span></font>
            </p>
            <p>
              <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>3. Prepare your list of accomplishments </b></span></font>
            </p>
            <ul style="list-style-type: none">
              <li>
                <p>
                  <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">This will enable you to present yourself as a person who understands him/herself. </span></font>
                </p>
              </li>
              <li>
                <p>
                  <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Sell yourself </b></span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">by your accomplishments to demonstrate future benefits to the company or </span></font>
                </p>
                <p>
                  <font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">organization. Use OAR statements: </span></font>
                </p>
              </li>
              <li>
                <p>
                  <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>O= Opportunity for improvement </b></span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">(focus on &#8220;challenges,&#8221; not &#8220;weaknesses&#8221;) </span></font>
                </p>
              </li>
              <li>
                <p>
                  <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>A=Activities you engaged in to seize this opportunity</b></span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">. (What did you do?) </span></font>
                </p>
              </li>
              <li>
                <p>
                  <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>R=Results: </b></span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">What were the benefits? Increased profits; decreased costs; increased speed? </span></font>
                </p>
              </li>
            </ul>
          </ol>
        </div>
      </div>
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">State benefits quantitatively whenever possible. Use percentages, dollars, etc. </span></font>
          </p>
        </div>
      </div>
    </div>
    <div class="page" title="Page 2">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>4. Review your resume thoroughly </b></span></font>
          </p>
          <p>
            <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Be prepared to answer any question that may be asked of items in your resume. </span></font>
          </p>
          <ol style="list-style-type: decimal" start="5">
            <li style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS">
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Research the company </b></span></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Scour the company website<br /></span></font><font size="12.000000pt" face="CourierNewPSMT"><span style="font-family: CourierNewPSMT; font-size: 12.000000pt">o </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">What does the company do? </span></font><font size="12.000000pt" face="CourierNewPSMT"><span style="font-family: CourierNewPSMT; font-size: 12.000000pt">o </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">What is their philosophy?<br /></span></font><font size="12.000000pt" face="CourierNewPSMT"><span style="font-family: CourierNewPSMT; font-size: 12.000000pt">o </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Who are their customers?<br /></span></font><font size="12.000000pt" face="CourierNewPSMT"><span style="font-family: CourierNewPSMT; font-size: 12.000000pt">o </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">What are their goals? </span></font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Research them on LinkedIn- also check to see if there are any Austin College alumni working there </span></font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Talk to people who might know the company and the culture, either employees or customers </span></font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Try to find someone who either has worked in the position you&#8217;re applying for or another department </span></font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Find out as much as you can about the interviewer-look them up on the company website or on LinkedIn </span></font>
                  </p>
                </li>
              </ul>
            </li>
            <li style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS">
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Prepare a list of questions. </b></span></font>
              </p>
            </li>
          </ol>
          <p>
            <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; </span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">These questions should help you understand the fit between your vocational identity, career objective, and this job and organization. </span></font>
          </p>
          <ol style="list-style-type: decimal" start="7">
            <li style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS">
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Practice the interview </b></span></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Thoroughly review your career objective, resume, cover letter, skills, accomplishment statements, and sample questions. </span></font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Practice with someone. Your answers should be crisp and succinct, not long and drawn out. No more than 20-30 seconds, 2 minutes at the maximum. </span></font>
                  </p>
                </li>
              </ul>
            </li>
            <li style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS">
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Know where you are going-DON&#8217;T GET LOST! </b></span></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Get the exact directions </span></font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">If you are slightly unsure, drive there the day before or leave with an enormous amount </span></font>
                  </p>
                  <p>
                    <font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">of extra time to make up for any difficulties. </span></font>
                  </p>
                </li>
              </ul>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1524486280007" ID="ID_1724891491" MODIFIED="1524486283453" TEXT="Sub Article">
<node CREATED="1524485444399" ID="ID_1612155691" MODIFIED="1524485446310" TEXT="Article"/>
<node CREATED="1524485446674" ID="ID_429097153" MODIFIED="1524485448462" TEXT="Article"/>
</node>
</node>
<node CREATED="1524485245938" ID="ID_977250567" MODIFIED="1526407209183" TEXT="Dressing for the interview ">
<node CREATED="1524485438947" ID="ID_1875771054" MODIFIED="1524486424337" TEXT="Main Article">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 2" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Grooming </b></span></font>
              </p>
            </li>
            <ol style="list-style-type: decimal">
              <li style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS">
                <p>
                  <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>In most cases, dress conservatively </b></span></font>
                </p>
                <ul style="list-style-type: none">
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Research the company to discover what their culture is like, dress code, etc. </span></font>
                    </p>
                  </li>
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">The most common approach is to dress like the person to whom you would report. </span></font>
                    </p>
                  </li>
                </ul>
              </li>
              <li style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS">
                <p>
                  <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Personal Hygiene </b></span></font>
                </p>
                <ul style="list-style-type: none">
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Shower, shave, brush your hair and teeth, use deodorant, trim and clean fingernails </span></font>
                    </p>
                  </li>
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Don&#8217;t use perfumes or colognes </span></font>
                    </p>
                  </li>
                </ul>
              </li>
              <li style="font-size: 12.000000pt; font-weight: 700; font-family: TimesNewRomanPS">
                <p>
                  <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>Conservative dress for males: </b></span></font>
                </p>
                <ul style="list-style-type: none">
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Navy blue or light to dark gray two piece business suit </span></font>
                    </p>
                  </li>
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">100 % cotton long sleeved white or blue dress shirt (starched) </span></font>
                    </p>
                  </li>
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Tie, preferably made of silk and a conservative pattern </span></font>
                    </p>
                  </li>
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Black or Cordovan colored shoes </span></font>
                    </p>
                  </li>
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Belt should match shoes </span></font>
                    </p>
                  </li>
                  <li>
                    <p>
                      <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Navy or black socks, long enough not to show skin when legs are crossed </span></font>
                    </p>
                  </li>
                </ul>
              </li>
            </ol>
          </ol>
        </div>
      </div>
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="11.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 11.000000pt">The Job Interview Page 2 </span></font>
          </p>
        </div>
      </div>
    </div>
    <div class="page" title="Page 3">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="12.000000pt" face="TimesNewRomanPS"><span style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt"><b>4. Conservative dress for females: </b></span></font>
          </p>
          <ul style="list-style-type: none">
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Dark color business suit such as navy, blue, gray, or black is a safe choice &#8211; pants or skirt is okay </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Skirt length a little below the knee and never shorter than just above the knee </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Long sleeved blouse in 100 % cotton or silk. White, pale blue, gray, or light pink, the </span></font>
              </p>
              <p>
                <font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">safest is white </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Neutral color of pantyhose in skin tone (these are a must) </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Black, navy, brown, burgundy shoes </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Do not wear very high heels </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Need to wear leather closed-toe pumps </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Accessories and jewelry &#8211; less is more </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Take either a purse or a briefcase but not both </span></font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT"><span style="font-family: SymbolMT; font-size: 12.000000pt">&#61623; &#160;</span></font><font size="12.000000pt" face="TimesNewRomanPSMT"><span style="font-family: TimesNewRomanPSMT; font-size: 12.000000pt">Wear as little make-up as is comfortable </span></font>
              </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1524486024778" ID="ID_1598331988" MODIFIED="1526407181595" TEXT="Sub Articles">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Interview Appearance
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1524485444399" ID="ID_22047123" MODIFIED="1524485446310" TEXT="Article"/>
<node CREATED="1524485446674" ID="ID_926158995" MODIFIED="1524485448462" TEXT="Article"/>
</node>
</node>
</node>
<node CREATED="1524485186434" ID="ID_1944774431" MODIFIED="1526405089439" TEXT="what happens during an interview">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      During the Interview
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1524485223197" ID="ID_1262873523" MODIFIED="1524486539181" TEXT="Interview Comportment">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 3" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="12.000000pt" face="TimesNewRomanPS"><b>During the Interview </b></font>
          </p>
          <div title="Page 3" class="page">
            <div class="layoutArea">
              <div class="column">
                <p>
                  <font size="12.000000pt" face="TimesNewRomanPS"><b>APPEAR ENERGETIC AND ENTHUSIASTIC WITH A &#8220;CAN DO&#8221; ATTITUDE! </b></font>
                </p>
                <p>
                  <font size="16.000000pt" face="TimesNewRomanPS"><b>BE POSITIVE! </b></font>
                </p>
              </div>
            </div>
          </div>
          <p>
            
          </p>
          <p>
            <font size="12.000000pt" face="TimesNewRomanPS"><b>1. Body Language</b></font>
          </p>
        </div>
      </div>
      <div class="layoutArea">
        <div class="column">
          <ul style="list-style-type: none">
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Sit straight in your chair but be relaxed </font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Keep hands in your lap or clasped on the table-try not to move them around too much </font>
              </p>
              <p>
                <font size="12.000000pt" face="TimesNewRomanPSMT">when talking </font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Try to keep from fiddling with your hair or clothing too much </font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Men &#8211; unbutton the lower button of your suit when you sit down </font>
              </p>
            </li>
            <li>
              <p>
                <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Remember to look at all the interviewers when answering questions </font>
              </p>
            </li>
          </ul>
          <ol style="list-style-type: decimal" start="2">
            <li style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt">
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><b>Answering/Asking Questions </b></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Be sure you know what you will say to &#8220;Tell me about yourself.&#8221; </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Watch out for &#8220;verbal ticks&#8221; &#8211; things like &#8220;um,&#8221; &#8220;you know,&#8221; &#8220;stuff like that,&#8221; etc. </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Remember that you can ask for a little time to collect your thoughts on a question </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Answer the questions as fully as you can &#8211; avoid just &#8220;yes&#8221; or &#8220;no&#8221; </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Be sure to get your questions answered </font>
                  </p>
                </li>
              </ul>
            </li>
            <li style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt">
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><b>Salary/Benefits Discussion </b></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">You should </font><font size="12.000000pt" face="TimesNewRomanPS"><b>not </b></font><font size="12.000000pt" face="TimesNewRomanPSMT">discuss salary or benefits unless an offer for the job is made </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Never be the first to mention money &#8211; they may tell you what is available </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">If the interviewer asks you about expected salary, you can say that you are sure the </font>
                  </p>
                  <p>
                    <font size="12.000000pt" face="TimesNewRomanPSMT">company would pay you fairly. Then you may ask, &#8220;What is the salary range?&#8221; </font>
                  </p>
                </li>
              </ul>
            </li>
            <li style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt">
              <p>
                <font size="12.000000pt" face="TimesNewRomanPS"><b>Special Notes on Phone/Skype Interviews </b></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">All of the above apply even dressing for the interview! An interview in pajamas will not be as effective. </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Ensure that you have a quiet, uninterrupted place to interview </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">Fully charge your phone/computer </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font size="12.000000pt" face="SymbolMT">&#61623; &#160;</font><font size="12.000000pt" face="TimesNewRomanPSMT">For Skype, make sure your backdrop is professional and internet connection reliable </font>
                  </p>
                </li>
              </ul>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1524484934501" ID="ID_1926020040" MODIFIED="1524484945348" TEXT="Interview Questions &amp; How to Answer them"/>
</node>
<node CREATED="1524485195230" FOLDED="true" ID="ID_629782373" MODIFIED="1526406590038" TEXT="So your done with the intervised: what&apos;s  next?">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 4" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="TimesNewRomanPS"><b>After the Interview </b></font>
          </p>
          <ol style="list-style-type: decimal">
            <li style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt">
              <p>
                <font face="TimesNewRomanPS"><b>The Ending </b></font>
              </p>
              <ul style="list-style-type: none">
                <li>
                  <p>
                    <font face="SymbolMT">&#61623; &#160;</font><font face="TimesNewRomanPSMT">If the interviewer asks if you have any last questions, it is OK to ask what their timeline is for making a hiring decision. </font>
                  </p>
                </li>
                <li>
                  <p>
                    <font face="SymbolMT">&#61623; &#160;</font><font face="TimesNewRomanPSMT">Reiterate how much you want the job and feel like you would be a good fit as you leave </font>
                  </p>
                </li>
              </ul>
            </li>
            <li style="font-family: TimesNewRomanPS; font-weight: 700; font-size: 12.000000pt">
              <p>
                <font face="TimesNewRomanPS"><b>Thank-Y ou </b></font>
              </p>
            </li>
          </ol>
          <p>
            <font face="SymbolMT">&#61623; </font><font face="TimesNewRomanPSMT">The day after the interview, send a brief thank you letter (handwritten goes a long way) and include any particular strong points that you discovered through the interview. </font>
          </p>
          <p>
            <font face="TimesNewRomanPS"><b>3. Following up </b></font>
          </p>
          <p>
            <font face="SymbolMT">&#61623; </font><font face="TimesNewRomanPSMT">It is okay to call to check on the decision making process when their timeframe has past &#8211; just don&#8217;t become a pest. </font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html>
</richcontent>
<node CREATED="1524484919544" ID="ID_1750003975" MODIFIED="1524485268696" TEXT="Interview Following up"/>
</node>
</node>
<node CREATED="1524654573816" FOLDED="true" ID="ID_818603232" MODIFIED="1526407619873" POSITION="right" TEXT="Working Oversas">
<node CREATED="1526058510982" ID="ID_1311419543" MODIFIED="1526058528421" TEXT="What to consider before working overseas"/>
<node CREATED="1526058491138" ID="ID_1586373825" MODIFIED="1526058500394" TEXT="Jobsites in different Countries"/>
</node>
<node CREATED="1524653885035" FOLDED="true" ID="ID_1928778836" MODIFIED="1526058930389" POSITION="left" TEXT="Other ">
<node CREATED="1524653894049" ID="ID_594362396" MODIFIED="1524654709180" TEXT="Work experience ">
<node CREATED="1524654770118" ID="ID_66025506" MODIFIED="1524654781785" TEXT="How to find Work Experience"/>
</node>
<node CREATED="1524654820099" ID="ID_356915489" MODIFIED="1524654841428" TEXT="Job Industry Profile"/>
<node CREATED="1524655225151" ID="ID_23565005" MODIFIED="1524655235602" TEXT="Career Profile"/>
<node CREATED="1519753243085" FOLDED="true" HGAP="6" ID="ID_133131853" MODIFIED="1526058912690" TEXT="Advancing your career" VSHIFT="73">
<node CREATED="1524591304893" FOLDED="true" ID="ID_311339804" MODIFIED="1526058902694" TEXT="Career Path (Getting Started)">
<node CREATED="1524591424349" FOLDED="true" ID="ID_907815051" MODIFIED="1526058895865" TEXT="Self-Assessment Tests">
<node CREATED="1524651850036" ID="ID_181617761" MODIFIED="1524651867066" TEXT="Personalty"/>
<node CREATED="1524651868303" ID="ID_1021571976" MODIFIED="1524651885834" TEXT="Interest"/>
<node CREATED="1524651888469" ID="ID_676946992" MODIFIED="1524651901541" TEXT="Values"/>
<node CREATED="1524651919004" ID="ID_1219733459" MODIFIED="1524651924476" TEXT="Skills">
<node CREATED="1524657175175" ID="ID_1101409200" MODIFIED="1524657183314" TEXT="Education"/>
</node>
</node>
<node CREATED="1524650365047" ID="ID_1238410838" MODIFIED="1524650379308" TEXT="Career Path Tips">
<node CREATED="1524591948723" ID="ID_1425638040" MODIFIED="1524594467610" TEXT="Making Career Decisions ">
<node CREATED="1524652696550" ID="ID_1633067515" MODIFIED="1524652714667" TEXT="Making a career decision tool"/>
</node>
</node>
<node CREATED="1524658394258" FOLDED="true" ID="ID_158483192" MODIFIED="1526058893359" TEXT="Career Path (Types of movement)">
<node CREATED="1524658405732" ID="ID_639856728" MODIFIED="1524658431092" TEXT="Climbing the career ladder (Vertical)"/>
<node CREATED="1524658433662" ID="ID_282464862" MODIFIED="1524658493193" TEXT="Career lattice (Horizontal movement)"/>
</node>
</node>
<node CREATED="1524569000552" FOLDED="true" ID="ID_1734684044" MODIFIED="1526058907172" TEXT="Career Development">
<node CREATED="1524591321878" ID="ID_390448223" MODIFIED="1524591337211" TEXT="Career Awareness"/>
<node CREATED="1524591345723" ID="ID_1712504442" MODIFIED="1524591360369" TEXT="Goal Setting"/>
<node CREATED="1524591361868" ID="ID_1778835272" MODIFIED="1524591370204" TEXT="Skill Development">
<node CREATED="1524592052414" ID="ID_610608365" MODIFIED="1524650220903" TEXT="Skills every employer want"/>
</node>
</node>
<node CREATED="1524591279635" FOLDED="true" ID="ID_1045230249" MODIFIED="1526058909254" TEXT="Career Management">
<node CREATED="1524591375551" ID="ID_1557150521" MODIFIED="1524591392311" TEXT="Relationship Building/ Networking"/>
</node>
<node CREATED="1524591292927" FOLDED="true" ID="ID_1196639209" MODIFIED="1526058911080" TEXT="Career Planning">
<node CREATED="1524650454948" ID="ID_1621163672" MODIFIED="1524650470398" TEXT="Steps to Career Planning"/>
<node CREATED="1524591399720" ID="ID_144399721" MODIFIED="1524591412742" TEXT="Career Development Action Plan"/>
<node CREATED="1524650559843" ID="ID_589986413" MODIFIED="1524650571666" TEXT="Career plan checklist"/>
<node CREATED="1524652193068" ID="ID_886325198" MODIFIED="1524652209136" TEXT="Career Planning Models"/>
</node>
</node>
<node CREATED="1526058478430" FOLDED="true" ID="ID_162543114" MODIFIED="1526058488173" TEXT="Parking Lot">
<node CREATED="1524498094562" FOLDED="true" HGAP="-14" ID="ID_1940321987" MODIFIED="1526058473490" TEXT="Getting a Job" VSHIFT="67">
<node CREATED="1524484872668" FOLDED="true" ID="ID_1357920568" MODIFIED="1526057328720" TEXT="Browse Jobs">
<node CREATED="1524649250511" ID="ID_9896394" MODIFIED="1524650740974" TEXT="Job directory (Find Jobs)" VSHIFT="-3">
<node CREATED="1524649315189" ID="ID_136706428" MODIFIED="1524649332522" TEXT="Vocational Jobs"/>
<node CREATED="1524649334858" ID="ID_4670628" MODIFIED="1524649342352" TEXT="Entry Level jobs"/>
<node CREATED="1524649344795" ID="ID_704487883" MODIFIED="1524649636207" TEXT="Experienced Job "/>
</node>
<node CREATED="1524649276400" ID="ID_1249022347" MODIFIED="1524649923738" TEXT="Talent directory(Discover Talents)" VSHIFT="48">
<node CREATED="1524649436618" ID="ID_1705147227" MODIFIED="1524649445939" TEXT="Vocational Skill"/>
<node CREATED="1524649449057" ID="ID_474786339" MODIFIED="1524649487407" TEXT="Graduate / Entry Level"/>
<node CREATED="1524649489009" ID="ID_781410133" MODIFIED="1524649616971" TEXT="Experienced "/>
</node>
</node>
<node CREATED="1524651372675" HGAP="30" ID="ID_1778088751" MODIFIED="1524651637657" TEXT="Linkedin" VSHIFT="35"/>
</node>
</node>
</node>
</node>
</map>
