<?php /*Template Name: Profile - Discussions*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Query */
        global $wpdb; //Include WP Global Object
        $message_db = new wpdb('root','umMv65ekyMRxfNfm','messages','localhost');
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>

            <div class="dashboard-multi-main-content full">
                <div class="page-header justify-content-between">
                    <div>
                        <h1 class="page-title">
                            Discussions
                        </h1>
                    </div>
                    <div>
                        <div class="dropdown show">
                            <a class="btn btn-trans txt-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                All Discussions
                            </a>
                            <div class="dropdown-menu txt-sm" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="<?php printf('%s&view=form&action=publication&value=%s&redir=%s&post-id=%s', currentUrl(false), $actions[$i]['action'], $get_page_view, $post_id ) ?>">
                                    The Entrepreneurship Incubator
                                </a>
                                <a class="dropdown-item" href="#">Job Advisor</a>
                                <a class="dropdown-item" href="#">Business Clinic</a>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if( $_GET['view'] == 'single' ){ ?>
                
                <?php
                    /* Get Message */
                    $message_id = $_GET['message_id'];
                    $message = $message_db->get_row( "SELECT * FROM messages WHERE ID = $message_id" );
    
                    /* Get Message Information */
                    $blog_id = $message->connected_post_site;
                    $ref_post_id = $message->connected_post;
                    $sender_id = $message->sender;
                    $receiver_id = $message->receiver;
    
                    echo $sender_id.' - '.$receiver_id;
    
                    /* Get Source Post */
                    /*switch_to_blog( 18 );
    
                    $ref_post = get_post( 844 );
                    $ref_post_title = get_the_title($ref_post);
                    $ref_post_link = get_permalink($ref_post);

                    restore_current_blog();*/
                ?>
                
                <?php
                    /* Get Avatar */
                    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                    $meta_key = 'user_avatar_url';
                    $get_sender_avatar_url = get_user_meta($sender_id, $meta_key, true);
                    $get_receive_avatar_url = get_user_meta($receiver_id, $meta_key, true);

                    if($get_sender_avatar_url){
                        $sender_avatar_url = $get_sender_avatar_url;
                    }
    
                    if($get_receiver_avatar_url){
                        $receiver_avatar_url = $get_receiver_avatar_url;
                    }

                    /* Get Sender User Display Name */
                    switch_to_blog(1);

                    $gf_id = 4; //Form ID
                    $username_entry_count = 0;

                    if($sender_id == 1){
                        $sender_displayname = 'SAEDConnect Admin';
                    }else{
                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $sender_id, //Current logged in user
                                )
                            )
                        );

                        /* Get Entries */
                        $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* Get GF Entry Count */
                        $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                        if($username_entry_count){ //If no entry
                            $sender_displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                        } 
                    }
    
                    /* Get Receiver User Display Name */
                    if($receiver_id == 1){
                        $receiver_displayname = 'SAEDConnect Admin';
                    }else{
                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search

                            array(

                                'key' => 'created_by', 'value' => $receiver_id, //Current logged in user
                                )
                            )
                        );

                        /* Get Entries */
                        $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                        /* Get GF Entry Count */
                        $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                        if($username_entry_count){ //If no entry
                            $receiver_displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                        } 
                        
                    }
    
                    restore_current_blog();
                ?>
                
                <h2 class="txt-lg margin-b-10">
                    <a href="<?php echo $ref_post_link; ?>" class="txt-color-blue">
                        <?php echo $ref_post_title; ?>
                    </a>
                </h2>
                
                <div class="txt-color-dark margin-b-40 padding-tb-40 border-b-1 border-color-darkgrey">
                    <h2 class="txt-lg txt-medium margin-b-20">
                        Subject: <?php echo $message->subject ?>
                    </h2>
                    <article class="text-box txt-height-1-7 txt-normal-s">
                        <?php echo $message->content ?>
                    </article>
                </div>
                
                <div class="padding-b-40">
                    <h3 class="txt-bold margin-b-10">Reply</h3>
                    <form class="form" action="<?php echo currentUrl(false).'&action=form-message'; ?>" method="post">
                        <?php
                            /* Meta Key */
                            $redirect_link = 'https://www.saedconnect.org/competency-profile/my-messages/?view=single&message_id='.$message_id;

                            /*
                            *
                            * Send Message
                            *
                            */
                            if($_POST && $_GET['action'] == 'form-message'){

                                /* Message Data */
                                $sender = $current_user->ID;
                                $receiver = $message->sender;
                                $parent_message = 0;
                                $type = 'reply';
                                $message = wp_kses_post( $_POST['message-content'] );
                                $connected_post = $post_id;
                                $connected_post_site = get_current_blog_id();
                                $read_status_sender = 'unread';
                                $read_status_receiver = 'unread';

                                $message_db->insert( 
                                    'messages', 
                                    array( 
                                        "sender" => $current_user->ID,
                                        "receiver" => 0,
                                        "parent_message" => $message_id,
                                        "type" => $type,
                                        "subject" => 'empty',
                                        "content" => $message,
                                        "connected_post" => 0,
                                        "connected_post_site" => 0,
                                        "read_status_sender" => $read_status_sender,
                                        "read_status_receiver" => $read_status_receiver,
                                    ), 
                                    array( "%d", "%d", "%d", "%s", "%s", "%s", "%d", "%d", "%s", "%s" ) 
                                );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                            }
                        ?>
                        <!-- Reply -->
                        <div class="form-item">

                            <textarea class="editor" name="message-content" cols="30" rows="8"></textarea>
                        </div>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </form>
                </div>
                
                <?php
                    $message_replies = $message_db->get_results( "SELECT * FROM messages WHERE parent_message = $message_id ORDER BY timestamp DESC" );

                    /* Get Entries */

                    /* Get GF Entry Count */
                    
                    
                    foreach( $message_replies as $message_reply ){
                ?>
                
                <div class="txt-color-dark padding-o-30 margin-b-20 <?php echo ( $message_reply->sender == $current_user->ID )? 'bg-beige' : 'bg-darkgrey'; ?>">
                    
                    <article class="text-box txt-sm txt-height-1-5 padding-b-20 margin-b-20 margin-b-1 border-b-1 border-color-darkgrey">
                        <?php echo $message_reply->content ?>
                    </article>
                    <div class="txt-normal-s">
                        <span class="txt-sm txt-medium padding-r-10">
                            <?php 
                                if($message_reply->sender == $message->sender)
                                    echo $sender_displayname;
                                elseif($message_reply->sender == $message->receiver)
                                    echo $receiver_displayname;
                            ?>
                        </span>
                        <span class="txt-sm txt-medium txt-color-dark">
                            <?php 
                                $deadline = $message_reply->timestamp;

                                $date = strtotime( $deadline );
                                echo date('h:i a - j F Y', $date);
                            ?>
                        </span>
                    </div>
                </div>
                
                <?php } ?>
                
                
                <?php }else{ ?>
                
                <article class="page-summary margin-b-20">
                    <p>
                        Read and respond to discussions you follow
                    </p>
                </article>

               <!-- <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 

                               href="<?php printf('%s/?view=%s&validate=%s', currentUrl(true), $key, $template_view ) ?>"
                               class="<?php echo ($_GET['view'] == $key) ? 'active' : ''; ?>"
                            >
                                Received
                            </a>
                        </li>
                        <li>
                            <a 

                               href="<?php printf('%s/?view=%s&validate=%s', currentUrl(true), $key, $template_view ) ?>"
                               class="<?php echo ($_GET['view'] == $key) ? 'active' : ''; ?>"
                            >
                                Sent
                            </a>
                        </li>
                    </ul>
                </nav>-->

                <div class="">
                    <?php
                        function truncate($string, $length){
                            if (strlen($string) > $length) {
                                $string = substr($string, 0, $length) . '...';
                            }

                            return $string;
                        }

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'topic',
                                'post_status' => 'publish',
                                'author' => $current_user->ID,
                                'posts_per_page' => -1,
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID
                            $post_author_id = get_the_author_meta('ID'); //Author ID
                    ?>
                       
                        <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                            <div class="col-8 padding-lr-15">
                                <p class="txt-xlg margin-b-15">
                                    <a href="<?php the_permalink(); ?>" class="txt-color-blue" target="_blank">
                                        <?php the_title() ?>
                                    </a>
                                </p>
                                <div class="txt-sm margin-b-15">
                                    <?php the_excerpt() ?>
                                </div>
                                <div class="">
                               <?php
                                    /* Get Avatar */
                                    $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                    $meta_key = 'user_avatar_url';
                                    $get_avatar_url = get_user_meta($post_author_id, $meta_key, true);

                                    if($get_avatar_url){
                                        $avatar_url = $get_avatar_url;
                                    }

                                    /* Get User Display Name */
                                    switch_to_blog(1);

                                    $gf_id = 4; //Form ID
                                    $username_entry_count = 0;

                                    /* GF Search Criteria */
                                    $search_criteria = array(

                                    'field_filters' => array( //which fields to search

                                        array(

                                            'key' => 'created_by', 'value' => $post_author_id, //Current logged in user
                                            )
                                        )
                                    );

                                    /* Get Entries */
                                    $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                                    /* Get GF Entry Count */
                                    $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                                    if($username_entry_count){ //If no entry
                                        $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                    }   

                                    restore_current_blog();
                                ?>

                                <style>
                                   .author_avatar {
                                        display: inline-block;
                                        background-size: cover !important;
                                        background-position: center !important;
                                        background-repeat: no-repeat !important;
                                        width: 1.5rem;
                                        height: 1.5rem;
                                        margin-right: 10px;
                                        border-radius: 50%;
                                    }
                                </style>
                                <div class="d-flex align-items-center">
                                    <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                                    </figure>
                                    <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                        <?php echo ($message_entry->sender == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!--<div class="col-2 padding-lr-15">
                                <a href="<?php printf("https://www.saedconnect.org/competency-profile/mail-user/?reference_post=%s&user_id=%s", get_permalink(), get_the_author_meta('ID') ) ?>" target="_blank">
                                    <i class="fa fa-envelope-o"></i>
                                    Mail Author
                                </a>
                            </div>-->
                            <div class="col-2 padding-lr-15 text-right">
                                <i class="fa fa-comment-o"></i>
                            </div>
                        </div>
                        
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <div class="padding-t-20 padding-b-40">
                            <h2 class="txt-lg txt-medium">
                                No Topics found.
                            </h2>
                        </div>   

                    <?php } ?>
                

                </div>
                <?php } ?>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>