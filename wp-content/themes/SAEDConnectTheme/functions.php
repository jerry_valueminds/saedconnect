<?php
    
/* 
*
*===================================================================================================================
* Enqueue Front Styles & Scripts BEGIN
*===================================================================================================================
*
*/
    function saedconnect_script_enqueue(){
        //Enqueue Styles
        wp_enqueue_style('Bootstrap', get_template_directory_uri().'/bootstrap_4/css/bootstrap.min.css', array(), '4.0.0', 'all');
        wp_enqueue_style('FontAwesome', get_template_directory_uri().'/stylesheets/font-awesome.min.css', array(), '3.0.0', 'all');
        wp_enqueue_style('HamburgerMenuIcon', get_template_directory_uri().'/stylesheets/hamburgers.css', array(), '3.0.0', 'all');
        wp_enqueue_style('Swiper', get_template_directory_uri().'/stylesheets/swiper.min.css', array(), '3.0.0', 'all');
        wp_enqueue_style('PopUpVideoPlayer', get_template_directory_uri().'/stylesheets/YouTubePopUp.css', array(), '3.0.0', 'all');
        wp_enqueue_style('Slippry', get_template_directory_uri().'/stylesheets/slippry.css', array(), '3.0.0', 'all');
        wp_enqueue_style('customStyle', get_template_directory_uri().'/stylesheets/styles.css', array(), '1.0.0', 'all');
        
        //Enqueue Scripts
        //wp_enqueue_script('JQuery', get_template_directory_uri().'/scripts/jquery-3.1.0.min.js', array('jquery'), '3.1.0', true);
        wp_enqueue_script('PopperJs', get_template_directory_uri().'/bootstrap_4/js/popper.min.js', array(), '4.0.0', true);
        wp_enqueue_script('BootstrapJs', get_template_directory_uri().'/bootstrap_4/js/bootstrap.min.js', array(), '4.0.0', true);
        wp_enqueue_script('SwiperJS', get_template_directory_uri().'/scripts/swiper.min.js', array(), '1.0.0', true);
        wp_enqueue_script('PopUpVideoJS', get_template_directory_uri().'/scripts/YouTubePopUp.jquery.js', array(), '1.0.0', true);
        wp_enqueue_script('AjaxScript', get_template_directory_uri().'/scripts/ajax-script.js', array(), '1.0.0', true);
        wp_enqueue_script('JSCookie', get_template_directory_uri().'/scripts/js.cookie.js', array(), '1.0.0', true);
        wp_enqueue_script('JQueryColllapser', get_template_directory_uri().'/scripts/jquery.collapser.js', array(), '1.0.0', true);
        wp_enqueue_script('Slippry', get_template_directory_uri().'/scripts/slippry.min.js', array(), '1.0.0', true);
        wp_enqueue_script('TinyMCE', get_template_directory_uri().'/tinymce/tinymce.min.js', array(), '1.0.0', true);
        wp_enqueue_script('Conditionize', get_template_directory_uri().'/scripts/conditionize.flexible.jquery.min.js', array(), '1.0.0', true);
        wp_enqueue_script('URIJs', get_template_directory_uri().'/scripts/URI.min.js', array(), '1.0.0', true);
        wp_enqueue_script('customScript', get_template_directory_uri().'/scripts/script.js', array(), '1.0.0', true);
    }

    //Call function on wp_enqueue_scripts
    add_action('wp_enqueue_scripts', 'saedconnect_script_enqueue');

    //Make var ajaxurl available on the front-end
    add_action('wp_head', 'myplugin_ajaxurl');

    function myplugin_ajaxurl() {
        echo '<script type="text/javascript">
               var ajaxurl = "' . admin_url('admin-ajax.php') . '";
             </script>';
    }

/* 
*
*===================================================================================================================
* Enqueue Admin Styles & Scripts BEGIN
*===================================================================================================================
*
*/
    function admin_scripts_enqueued(){
        //Enqueue Styles
        wp_enqueue_style('AlertableCSS', get_template_directory_uri().'/admin_assets/alertable/jquery.alertable.css', array(), '1.0.0', 'all');
        wp_enqueue_style('FontAwesome', get_template_directory_uri().'/stylesheets/font-awesome.min.css', array(), '3.0.0', 'all');
        wp_enqueue_style('Stylsheet', get_template_directory_uri().'/admin_assets/style.css', array(), '1.0.0', 'all');
        
        //Enqueue Scripts
        wp_enqueue_script('AlertableJS', get_template_directory_uri().'/admin_assets/alertable/jquery.alertable.min.js', array(), '1.0.0', 'all');
        wp_enqueue_script('AdminScript', get_template_directory_uri().'/admin_assets/script.js', array(), '1.0.0', true);
        
        //JQuery UI
        wp_enqueue_script( 'jquery-ui-core' );
        wp_enqueue_script( 'jquery-ui-widget' );
        wp_enqueue_script( 'jquery-ui-mouse' );
        wp_enqueue_script( 'jquery-ui-accordion' );
        wp_enqueue_script( 'jquery-ui-autocomplete' );
        wp_enqueue_script( 'jquery-ui-slider' );
        wp_enqueue_script( 'jquery-ui-tabs' );
        wp_enqueue_script( 'jquery-ui-sortable' );
        wp_enqueue_script( 'jquery-ui-draggable' );
        wp_enqueue_script( 'jquery-ui-droppable' );
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_enqueue_script( 'jquery-ui-resize' );
        wp_enqueue_script( 'jquery-ui-dialog' );
        wp_enqueue_script( 'jquery-ui-button' );
    }

    //Call function on wp_enqueue_scripts
    add_action('admin_enqueue_scripts', 'admin_scripts_enqueued');


/* 
*
*===================================================================================================================
* Remove Extra Space on top of pages when admin is logged in
*===================================================================================================================
*
*/
    add_action('get_header', 'remove_admin_login_header');
    function remove_admin_login_header() {
        remove_action('wp_head', '_admin_bar_bump_cb');
    }


/* 
*
*===================================================================================================================
* Activate WP Theme Support BEGIN
*===================================================================================================================
*
*/

    function saedconnect_theme_setup(){
        //Activate Post Thumbnails
        add_theme_support('post-thumbnails');
        
        //Activate Menu Support
        add_theme_support('menus');
        
        //Activate Post Formats
        add_theme_support('post-formats', array('aside', 'image', 'video'));
        
        //Activate Custom Menu Support
        register_nav_menu('primary', 'Main Navigation Menu');
    }

    add_action('init', 'saedconnect_theme_setup');




/* 
*
*===================================================================================================================
* Add file Edit to Contributor BEGIN
*===================================================================================================================
*
*/


function add_theme_caps() {
    // get the contributor role
    $contributor_role = get_role( 'contributor' );

    // grant the upload_files capability
    $contributor_role->add_cap( 'upload_files', true );
    
    // grant the edit_files capability
    $contributor_role->add_cap( 'edit_files', true );
}
add_action( 'admin_init', 'add_theme_caps');
    
/* 
*
*===================================================================================================================
* Add file Edit to Contributor END
*===================================================================================================================
*
*/

/* 
*
*===================================================================================================================
* Include Theme Customizer BEGIN
*===================================================================================================================
*
*/

    //include 'includes/customizer.php';

/* 
*
*===================================================================================================================
* Include Sendi Mail List Actions BEGIN
*===================================================================================================================
*
*/

    include 'includes/mail_actions.php.php';

/* 
*
*===================================================================================================================
* Include Settings Page BEGIN
*===================================================================================================================
*
*/

    include 'includes/_mb_settings.php';


/* 
*
*===================================================================================================================
* Include Meta Box Files BEGIN
*===================================================================================================================
*
*/

    include 'metabox_files/metaboxes.php';
    include 'metabox_files/taxonomies.php';
    include 'metabox_files/post_relationships.php';
/* 
*
*===================================================================================================================
* Include Gravity Forms Files BEGIN
*===================================================================================================================
*
*/

    include 'includes/gf_process.php';

/* 
*
*===================================================================================================================
* Include BBPress Actions & Filters BEGIN
*===================================================================================================================
*
*/

    include 'includes/bbpress.php';

/* 
*
*===================================================================================================================
* Include GF Select BEGIN
*===================================================================================================================
*
*/

    include 'includes/_gf_select.php';

/* 
*
*===================================================================================================================
* Include GV Redirect After Update BEGIN
*===================================================================================================================
*
*/

    include 'includes/_gv_update_redirect.php';

/* 
*
*===================================================================================================================
* Include GF Custom Fields for User Edit Page BEGIN
*===================================================================================================================
*
*/

    include 'includes/_user_edit.php';

/* 
*
*===================================================================================================================
* Include Add ID to User column BEGIN
*===================================================================================================================
*
*/

    include 'includes/_show_id.php';
        


/* 
*
*===================================================================================================================
* Add Custom Relation Meta Box BEGIN
*===================================================================================================================
*
*/

//include 'includes/custom_metaboxes.php';
add_action('wp_ajax_get_subcategories_form', 'get_subcategories_form');
add_action('wp_ajax_nopriv_get_subcategories_form', 'get_subcategories_form');

function get_subcategories_form()
{
    //$parentCategoryID = $_POST['parentCategoryID'];
    
    $parent_term_id = $_POST['parentCategoryID']; // term id of parent term

    $taxonomies = array( 
        'category',
    );

    $args = array(
        //'parent'         => $parent_term_id,
        'child_of'      => $parent_term_id,
        'hide_empty' => false,
    ); 

    $terms = get_terms($taxonomies, $args);
    
    $response;
    
    foreach($terms as $term){
    
        $term_id = $term->term_id; //Get the term ID
        $term_name = $term->name; //Get the term name
        $response .= '<option value="' .$term->term_id. '" class="sub-category-list-item">'. $term_name . '</option>';
    
    };
    
    echo $response;
    
    wp_die();
}


// Create Widget Area for Login Page
if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'Name of Widgetized Area',
        'id' => 'sidebar-1',
        'before_widget' => '<div class = "widgetizedArea">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="d-none">',
        'after_title' => '</h3>',
    )
);


/* Restrict Dashboard accces from non-admin Users */
add_action( 'init', 'blockusers_init' );
 
function blockusers_init() {
    if ( is_admin() && ! current_user_can( 'administrator' ) && 
       ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        wp_redirect( home_url() );
        exit;
    }
}

/**
 * Hide admin bar for non-admins
 */
function js_hide_admin_bar( $show ) {
	if ( ! current_user_can( 'administrator' ) ) {
		return false;
	}
	return $show;
}
add_filter( 'show_admin_bar', 'js_hide_admin_bar' );



/**
* Gravity Forms Custom Activation Template
* https://gravitywiz.com/customizing-gravity-forms-user-registration-activation-page
*/
add_action('wp', 'custom_maybe_activate_user', 9);
function custom_maybe_activate_user() {
    $template_path = STYLESHEETPATH . '/activate.php';
    $is_activate_page = isset( $_GET['page'] ) && $_GET['page'] == 'gf_activation';

    if( ! file_exists( $template_path ) || ! $is_activate_page )
    return;

    require_once( $template_path );

    exit();
}


/* Allow (.) in permalink */

remove_filter('sanitize_title', 'sanitize_title_with_dashes');
function sanitize_title_with_dots_and_dashes($title) {
        $title = strip_tags($title);
        // Preserve escaped octets.
        $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
        // Remove percent signs that are not part of an octet.
        $title = str_replace('%', '', $title);
        // Restore octets.
        $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);
        $title = remove_accents($title);
        if (seems_utf8($title)) {
                if (function_exists('mb_strtolower')) {
                        $title = mb_strtolower($title, 'UTF-8');
                }
                $title = utf8_uri_encode($title);
        }
        $title = strtolower($title);
        $title = preg_replace('/&.+?;/', '', $title); // kill entities
        $title = preg_replace('/[^%a-z0-9 ._-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');
        $title = str_replace('-.-', '.', $title);
        $title = str_replace('-.', '.', $title);
        $title = str_replace('.-', '.', $title);
        $title = preg_replace('|([^.])\.$|', '$1', $title);
        $title = trim($title, '-'); // yes, again
        return $title;
}
add_filter('sanitize_title', 'sanitize_title_with_dots_and_dashes');

/* 
*
*===================================================================================================================
* Assign Editor role to all users BEGIN
*===================================================================================================================
*
*/

add_action('wp','add_current_user_to_blog',10);

    /*add logged in user to current blog*/
    function add_current_user_to_blog(){
    if(!is_user_logged_in())
    return false;

    global $current_user,$blog_id;

    $role = 'author';

    if( current_user_can('editor') ){
        
    }else{
        $current_user->add_role( 'editor' );
    }
    
}

/* 
*
*===================================================================================================================
* Change default sender email and name BEGIN
*===================================================================================================================
*
*/
function beetcore_change_from_email( $original ) {
	return 'no-reply@saedconnect.org';
}
add_filter( 'wp_mail_from', 'beetcore_change_from_email' );

function beetcore_change_from_name( $original ) {
	return 'SAEDConnect';
}
add_filter( 'wp_mail_from_name', 'beetcore_change_from_name' );


/* Global Vars */
global $nysc_state_codes;
$nysc_state_codes = array(
    'AB', 'AD', 'AK', 'FC'
);

function switch_on_comments_automatically(){
    global $wpdb;
    $wpdb->query( $wpdb->prepare("UPDATE $wpdb->posts SET comment_status = 'open'")); 
}
switch_on_comments_automatically();



// Image Attachment
