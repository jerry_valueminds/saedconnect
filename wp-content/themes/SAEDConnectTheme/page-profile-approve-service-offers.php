<?php /*Template Name: Profile - Approve Service Offers*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Post Type */
        $postType = 'offer-a-service';

        /* Publication */
        $publication_key   = 'publication_status';

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if( $_GET['view'] == 'form' ){ //Display form  ?>

                <?php
                    if($_GET['post-id']){
                        $post_id = $_GET['post-id'];
                        $post = get_post($post_id);
                        $postName = $post->post_title;

                        /* Publish / Unpublish & Return */
                        if($_GET['action'] == 'publication'){

                            /* Get saved meta */
                            $saved_meta = get_post_meta( $post_id, $publication_key, true );

                            if ( $saved_meta == 'admin_published' ){ //If published, Unpublish
                                
                                update_post_meta( $post_id, $publication_key, 'user_published' );
                                $redirect_link = currentUrl(true).'/?view=approved';
                                
                            } else { //If Unpublished, Publish
                                
                                update_post_meta( $post_id, $publication_key, 'admin_published' );
                                $redirect_link = currentUrl(true);
                            }

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    }

                ?>
               
            <?php } elseif( $_GET['view'] == 'approved') { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        Publish Service Offers
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        These posts have been reviewed and approved.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'approved') ? 'active' : ''; ?>"
                            >
                                Pending Approval
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=approved'; ?>"
                               class="<?php echo ($_GET['view'] == 'approved') ? 'active' : ''; ?>"
                            >
                                Approved
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="overflow-hidden">
                    <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                       <div class="col-5 padding-lr-15">
                            NAME
                       </div>
                       <div class="col-3 padding-lr-15">
                            AUTHOR
                       </div>
                    </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */

                    // Create Query Argument
                    $args = array(
                        'post_type' => $postType,
                        'showposts' => -1,
                        'meta_query' => array(
                            array(
                                'key' => $publication_key,
                                'value' => 'admin_published'
                            )
                        )
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                ?>
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-5 padding-lr-15">
                            <a href="<?php the_permalink() ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                           <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($post->post_author, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $post->post_author, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );
                          
                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                          

                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                }   

                                restore_current_blog();
                            ?>
                            
                            <style>
                               .author_avatar {
                                    display: inline-block;
                                    background-size: cover !important;
                                    background-position: center !important;
                                    background-repeat: no-repeat !important;
                                    width: 1.5rem;
                                    height: 1.5rem;
                                    margin-right: 10px;
                                    border-radius: 50%;
                                }
                            </style>
                            <div class="d-flex align-items-center">
                                <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                                </figure>
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php echo ($post->post_author == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </div>
                            </div>
                       </div>
                        <div class="col-2 padding-lr-15">
                            <a 
                               href="<?php printf("https://www.saedconnect.org/competency-profile/mail-user/?reference_post=%s&user_id=%s", get_permalink(), get_the_author_meta('ID') ) ?>" 
                               class="txt-color-blue"
                               target="_blank"
                            >
                                <i class="fa fa-envelope-o"></i>
                                Mail Author
                            </a>
                        </div>
                        <div class="col-2 padding-lr-15 text-right">
                        <?php
                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                            if($publication_meta == "admin_published"){
                        ?>
                            <a 
                               href="<?php printf('%s/?view=form&action=publication&post-id=%s', currentUrl(true), $post_id ) ?>"
                               class="btn btn-blue txt-sm no-m-b"
                            >
                                Unpublish
                            </a>   
                        <?php        
                            } else {
                        ?>
                            <a 
                               href="<?php printf('%s/?view=form&action=publication&post-id=%s', currentUrl(true), $post_id ) ?>"
                               class="btn btn-trans-bw txt-sm no-m-b"
                            >
                                Publish
                            </a>
                        <?php
                            }
                        ?>
                       </div>
                    </div>
                <?php
                    endwhile;
                ?>
                </div>
            
            <?php } else { ?>
                <div class="page-header">
                    <h1 class="page-title">
                        Publish Service Offers
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        These posts are under review.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'approved') ? 'active' : ''; ?>"
                            >
                                Pending Approval
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=approved'; ?>"
                               class="<?php echo ($_GET['view'] == 'approved') ? 'active' : ''; ?>"
                            >
                                Approved
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="overflow-hidden">
                    <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                       <div class="col-5 padding-lr-15">
                            NAME
                       </div>
                       <div class="col-3 padding-lr-15">
                            AUTHOR
                       </div>
                    </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */

                    // Create Query Argument
                    $args = array(
                        'post_type' => $postType,
                        'showposts' => -1,
                        'meta_query' => array(
                            array(
                                'key' => $publication_key,
                                'value' => 'user_published'
                            )
                        )
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                ?>
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-5 padding-lr-15">
                            <a href="<?php the_permalink() ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                           <?php
                                /* Get Avatar */
                                $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                                $meta_key = 'user_avatar_url';
                                $get_avatar_url = get_user_meta($post->post_author, $meta_key, true);

                                if($get_avatar_url){
                                    $avatar_url = $get_avatar_url;
                                }

                                /* Get User Display Name */
                                switch_to_blog(1);

                                $gf_id = 4; //Form ID
                                $username_entry_count = 0;

                                /* GF Search Criteria */
                                $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $post->post_author, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );
                          
                                /* Get GF Entry Count */
                                $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                          

                                if($username_entry_count){ //If no entry
                                    $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                                }   

                                restore_current_blog();
                            ?>
                            
                            <style>
                               .author_avatar {
                                    display: inline-block;
                                    background-size: cover !important;
                                    background-position: center !important;
                                    background-repeat: no-repeat !important;
                                    width: 1.5rem;
                                    height: 1.5rem;
                                    margin-right: 10px;
                                    border-radius: 50%;
                                }
                            </style>
                            <div class="d-flex align-items-center">
                                <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                                </figure>
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php echo ($post->post_author == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                </div>
                            </div>
                       </div>
                        <div class="col-2 padding-lr-15">
                            <a 
                               href="<?php printf("https://www.saedconnect.org/competency-profile/mail-user/?reference_post=%s&user_id=%s", get_permalink(), get_the_author_meta('ID') ) ?>" 
                               class="txt-color-blue"
                               target="_blank"
                            >
                                <i class="fa fa-envelope-o"></i>
                                Mail Author
                            </a>
                        </div>
                        <div class="col-2 padding-lr-15 text-right">
                        <?php
                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                            if($publication_meta == "admin_published"){
                        ?>
                            <a 
                               href="<?php printf('%s/?view=form&action=publication&post-id=%s', currentUrl(true), $post_id ) ?>"
                               class="btn btn-blue txt-sm no-m-b"
                            >
                                Unpublish
                            </a>   
                        <?php        
                            } else{
                        ?>
                            <a 
                               href="<?php printf('%s/?view=form&action=publication&post-id=%s', currentUrl(true), $post_id ) ?>"
                               class="btn btn-trans-bw txt-sm no-m-b"
                            >
                                Publish
                            </a>
                        <?php
                            }
                        ?>
                       </div>
                    </div>
                <?php
                    endwhile;
                ?>
                </div>
            <?php } ?> 
                
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>