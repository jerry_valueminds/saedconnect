<?php /*Template Name: Profile - Corp Member Verification*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        Corp Member Verification
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Here you can view the status of all the opportunities you have applied for.
                    </p>
                </article>
                
                
                    <?php 
                        if (!$_GET['call-up-number']){
                            
                    ?>
                        <h1 class="txt-lg txt-medium margin-b-20">
                            Something went wrong! Go back and try again.
                        </h1>
                           
                    <?php } else { ?>
                    
                    <div class="section-wrapper">   
                        <div class="entry">   
                    <?php
                        function generateVerificationCode(){
                            /* Validate Verification Code Uniqueness */
                            $generated_token = md5(time());
                            
                            /* Check if token exists */
                            global $wpdb;
                            $nysc_list_db = new wpdb('root','root','saedconnect_wp_db','localhost');
                            $token = $nysc_list_db->get_row("SELECT verification_token FROM corp_member_profile WHERE verification_token = '".$generated_token."' LIMIT 0,1");
                            
                            if( $token ){
                                generateVerificationCode();
                            } else {
                                return $generated_token;
                            }
                        }
                            
                        /* Get Call-Up Number User entered */  
                        $query_cu_number = $_GET['call-up-number'];
                                                        
                        /* Check Call-Up number in from DB */
                        //$nysc_list_db = new wpdb('root','root','nysc_db','localhost'); //Local
                        $nysc_list_db = new wpdb('root','umMv65ekyMRxfNfm','nysc_db','localhost'); //Server
                        $corp_member = $nysc_list_db->get_row("SELECT call_up_number, verification_status, first_name, last_name, middle_name, email FROM corp_member_profile WHERE call_up_number = '".$query_cu_number."' LIMIT 0,1");

                            if ( $corp_member ){
                                                                
                                /* If User is trying to verify Call-Up Number for the first time */
                                if ( !$corp_member->verification_status ){
                                    
                                    /* Generate Verification Token */
                                    $verification_token = generateVerificationCode();

                                    /* Update Verification status to PENDING */
                                    $nysc_list_db->update( 
                                        'corp_member_profile', //Table
                                        array(
                                            "verification_status" => "pending", 
                                            "verification_token" => $verification_token
                                        ), //Data
                                        array("call_up_number" => $corp_member->call_up_number), //Where?
                                        array("%s", "%s"), //Data format
                                        array("%s") //Where format
                                    );
                                    
                                    /* Send Activation mail */
                                    $activation_link = 'https://www.saedconnect.org/corp-member-profile-activation/?activation-code='.$verification_token;
                                    
                                    $to = $corp_member->email;
                                    $subject = 'Activate your SAEDConnect NYSC SAED Profile';
                                    
                                    $message = '<p>Hello, '.$corp_member->last_name.' '.$corp_member->first_name.' '.$corp_member->middle_name.'</p>';
                                    
                                    $message .= '<p>Your Call-Up Number: <strong>'.$corp_member->call_up_number.'</strong> has been submitted for the SAEDConnect Corp Member Profile activation. Click the link below to activate your account.</p>';
                                    
                                    $message .= '<p><strong><a href="'.$activation_link.'">Activate profile</a></strong></p>';

                                    $headers[] = 'From: SAEDConnect <no-reply@saedconnect.org>';
                                    
                                    wp_mail( $to, $subject, $message, $headers );
                                    
                                    /* Confirmation Message: Success */
                                    printf('<div class="padding-b-15">Congatulations %s, %s %s, <br><br> Your Call Up number: <span class="txt-bold">%s</span> has been verified.<br><br> An activation mail has been sent to your email address with instructions on how to activate your SAEDConnect Corp Member Profile.</div>', $corp_member->last_name, $corp_member->first_name, $corp_member->middle_name, $corp_member->call_up_number);
                                    
                                } elseif ( $corp_member->verification_status == 'verified' ) {
                                    
                                    /* Confirmation Message: Verified */
                                    printf('<div class="padding-b-15">Congratulations %s, %s %s, <br><br> Your Call Up number: <span class="txt-bold">%s</span> has been verified.<br><br> And <span class="txt-bold">Your SAEDConnect Corp Member Profile is activated</span>.</div>', $corp_member->last_name, $corp_member->first_name, $corp_member->middle_name, $corp_member->call_up_number);
                                    
                                    echo '<p class="txt-normal-s txt-medium padding-b-15">If this is your Call-up Number and you would like to claim it, kindly contact the SAEDConnect <a class="txt-color-blue" href="">Help Center</a></p>';
                                    
                                } elseif ( $corp_member->verification_status == 'pending' ) {
                                
                                    /* Confirmation Message: Pending Verification */
                                    printf('<div class="padding-b-15">Hello %s, %s %s, <br><br> Your Call Up number: <span class="txt-bold">%s</span> has already been verified.<br><br> An activation mail was sent to your email address with instructions on how to activate your SAEDConnect Corp Member Profile.</div>', $corp_member->last_name, $corp_member->first_name, $corp_member->middle_name, $corp_member->call_up_number);
                                    
                                    echo '<p class="txt-normal-s txt-medium padding-b-15">If this is your Call-up Number and you are not the one who requested the pending verification request, kindly contact the SAEDConnect <a class="txt-color-blue" href="">Help Center</a></p>';
                                    
                                }
                            
                            } else {
                                
                                /* Confirmation Message: Record not found */
                                printf('<p class="margin-b-15 txt-lg txt-height-1-5 txt-color-red">The call-up number you entered: <span class="txt-bold">%s</span>, does not exist in our Database. <br>Please return to your NYSC SAED Profile and ensure you entered a correct Call Up Number.</p>', $query_cu_number);
                                
                            }
                          ?>  
                          </div>
                    </div>
                    
                    <?php } ?>
                
                <div class="margin-t-40">
                    <a href="https://www.saedconnect.org/nysc-saed-profile/" class="btn btn-ash txt-normal-s">
                        <i class="fa fa-arrow-left"></i>
                        <span class="padding-l-10">
                            Go to Your NYSC SAED Profile
                        </span>
                    </a>
                </div>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>