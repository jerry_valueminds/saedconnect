<?php /*Template Name: Profile - Tasks*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Publication */
        $publication_key = 'publication_status';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
            
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                </div>
                
            <?php } elseif($_GET['view'] == 'interactions'){ ?>
            
                <div class="page-header">
                    <h1 class="page-title">
                        My Tasks / Gigs I have engaged
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        In this section you could view all the tasks/gigs you have added to the service market place, and also add new tasks to the service market place.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Tasks / Gig Requests
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Tasks / Gigs I have engages
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="row row-15">
                <?php
                    /* GF Search Criteria */
                    $gf_id = 10;
                    $parent_post_id = 2;
                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                    foreach( $entries as $entry ){

                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $parent_post = get_post($parent_post_id);
                ?>
                    <div class="col-md-4 padding-lr-15 padding-b-30 d-flex">
                        <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                            <h3 class="margin-b-10 txt txt-color-blue margin-b-10">
                                <?php echo rgar( $entry, '1' ) ?>
                            </h3>
                            <div class="txt-sm txt-color-dark margin-b-10">
                                <p>
                                    Amount: <?php echo rgar( $entry, '4' ) ?>
                                </p>
                            </div>
                            <div class="txt-normal-s margin-b-20">
                                <p>
                                    <?php echo rgar( $entry, '3' ) ?>
                                </p>
                            </div>
                            <h3 class="txt-sm txt-color-dark txt-medium">
                                For <a class="txt-color-light" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                            </h3>
                        </div>
                    </div>
                <?php } ?>
                </div>
            
            <?php } else { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        My Tasks / Gig Requests
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&gf-id=3&form-title=Add a Task" class="cta-btn">
                            Add Task
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        In this section you could view all the tasks/gigs you have added to the service market place, and also add new tasks to the service market place.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Tasks / Gig Requests
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Tasks / Gigs I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <ul class="row row-10 marketplace-job-cards padding-b-40">
                <style>
                    .publication-status{
                        position: absolute;
                        display: flex;
                        align-items: center;
                        top: -0.1em;
                        right: 0.4em;
                        padding: 5px 10px;
                        color: black;
                        background-color: gainsboro;
                    }

                    .user_published{
                       background-color: #f4c026; 
                    }

                    .admin_published{
                       background-color: #00bfe7; 
                    }

                    .home-community-card{
                        overflow: visible;
                    }
                </style>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */

                    // Create Query Argument
                    $args = array(
                        'post_type' => 'task',
                        'author' => $current_user->ID,
                        'showposts' => -1,
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                          
                    /* Publication status */      
                    $publication_meta = get_post_meta( $post_id, $publication_key, true );
                ?>

                    <li class="col-sm-6 col-lg-4 padding-o-10 d-flex">
                        <a href="<?php the_permalink() ?>" class="flex_1">
                            <div class="content">
                                <div class="title">
                                    <?php the_title(); ?>
                                </div>
                                <div class="location">
                                    Abuja | Full Time
                                </div>
                                <div class="post-time">
                                    Posted 2 days ago
                                </div>
                                <p class="txt-sm txt-color-dark padding-t-20">
                                    <?php 
                                        $response_gf_id = 10; //Form ID

                                        /* GF Search Criteria */
                                        $response_search_criteria = array(

                                        'field_filters' => array( //which fields to search

                                            array(

                                                'key' => '2', 'value' => $post_id, //Current logged in user
                                                )
                                            )
                                        );

                                        /* Get Entries */
                                        $response_entries = GFAPI::get_entries( $response_gf_id, $response_search_criteria );

                                        /* Get GF Entry Count */
                                        $response_entry_count = GFAPI::count_entries( $response_gf_id, $response_search_criteria );
                                    ?>
                                    <span class="txt-bold">
                                        <?php echo $response_entry_count ?>
                                    </span>
                                    Bid(s)
                                </p>
                                
                            </div>
                        </a> 
                    </li>

                <?php
                    endwhile;
                ?>
                </ul>
                
            <?php } ?>
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>