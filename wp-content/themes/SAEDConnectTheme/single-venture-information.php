   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    ?>
    
    <?php
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';
    ?>
    
   
    <?php while ( have_posts() ) : the_post(); ?>
    
    <main class="main-content txt-color-light" style="margin-top: 100px">
        <section class="container-wrapper margin-b-20">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="txt-2em txt-bold margin-b-30">
                        <?php echo the_title(); ?> 
                    </h1>
                </div>
            </div>
        </section>
    
    <?php
        /* Get User */
        $current_user = wp_get_current_user();

        /* Check if User has only mode role */
        $onlymod = true;

        /* Check if User has only Moderator role */
        foreach( $current_user->roles as $role ){
            if( $role == 'administrator' || $role == 'subscriber'  || $role == 'bbp_participant' ){
                /* User is not only mode */
                $onlymod = false;
                //echo '<h1 class="txt-2em padding-t-80">Role: '.$role.'</h1>';
            }
        }


        /* If User has */
        if($page_type){

            get_template_part( 'template-parts/user-dashboard/'.$page_type );

        } else {

            get_template_part( 'template-parts/user-dashboard/business-venture-information' );

        }

    ?>
    </main>
    
    <?php endwhile; // end of the loop. ?>

    
<!--Load Scripts-->