<?php
    /* view template */
    $view = 'about';

?>
    
<?php get_header() ?>
   
<style>
    .menu li{
        display: inline;
        
    }
    
    .menu li a{
        display: inline-block;
        font-size: 0.9rem;
        padding: 20px 20px;
        color: white;
    }
    
    .menu li a.active{
        background-color: white;
        color: black;
    }
</style>
    
<main class="main-content">
    <?php 
        while ( have_posts() ) : the_post(); 
            /* Get Track ID */
            $track_id = get_the_ID();

            /* Get Form ID from request */
            $gf_id = rwmb_meta( 'program-form-shortcode' );


            /*
            *==============================================
            *
            *   Get data from Program
            *
            *==============================================
            */
            $connected = new WP_Query( array(
                'relationship' => array(
                    'id'   => 'program_to_track',
                    'to' => $track_id, // You can pass object ID or full object
                ),
                'nopaging' => true,
            ) );
            while ( $connected->have_posts() ) : $connected->the_post();

                /* Get Program Information */
                $program_id = $post->ID;
                $program_name = get_the_title();
                $program_url = get_the_permalink();
                $program_theme_color = rwmb_meta( 'skilli-program-theme-color' );
                $program_theme_color_alt = rwmb_meta( 'skilli-program-alt-theme-color' );

            endwhile;
            wp_reset_postdata();





            if( $_GET['view'] ){
                $view = $_GET['view'];
            }

            $entry_count = 0;

            /* Get current User ID */
            $current_user = wp_get_current_user();

            /* GF Search Criteria */
            $search_criteria = array(

            'field_filters' => array( //which fields to search

                array(

                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                )
              )
            );

            /* Get GF Entry Count */
            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


            if( $entry_count && $view == 'about' && in_array($program_id, array(632, 649) ) && is_user_logged_in() ){
                $view = 'welcome';
            }
    
            /* Get Template Part */
            get_template_part( 'template-parts/track/_'.$view );
            //get_template_part( 'template-parts/track/_resources' );
    
        endwhile; // end of the loop. 
    ?>
</main>

<?php get_footer() ?>