<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    <meta name="description" content="">
    <meta name="author" content="saedconnect.com">
	<title>SAEDConnect - Activating Africa's Youth Human Capital</title>
	
	<!--Site icon-->
	<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" type="image/x-icon">
	
    <!--Load Styles-->
	<?php wp_head(); ?>
	
	<!-- JSCookie CDN -->
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    
    <!-- JQuery CDN -->
	<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    
    <script>
        $(document).ready(function() {

            $(".remember-page, #gform_submit_button_0").click(function(){
                /* Set Previoys Page Cookie */
                Cookies.set('previous-page', window.location.href);
            });
        })
    </script>
	
	<!--FontAwesome CDN-->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
	<style>
        .bbp-breadcrumb{
            display: none !important;
        }
        
        .freshness-link a{
            color: white !important;
        }
        
        .bbp-forum-content{
            font-size: 0.8rem !important;
            margin-top: 10px;
        }
        
        /* Reply */
        .reply-wrapper a {
            font-weight: 500;
        }

        .reply-icon a{
            display: block;
            position: relative;
            height: 1em;
            width: 1em;
            padding-left: 1.5em;
            font-size: 0.7rem;
            font-weight: 500;
            margin-bottom: 10px;
        }

        .reply-icon a:after{
            font-family: FontAwesome;
            content: '\f112';
            position: absolute;
            top: 0;
            left: 0;

            color: inherit;
            font-size: 0.9em;
        }
        
        .reply-icon.edit a:after{
            content: '\f040';
        }
        
        .reply-icon.trash a:after{
            content: '\f014';
        }
        
        .reply-icon.spam a:after{
            content: '\f071';
        }
        
        /* Disable Template Notice */
        .bbp-template-notice{
            display: none !important;
        }
        
        /* BBP Form */
        .bbp-form{}
        
        .bbp-form label{
            font-size: 0.9rem;
            font-weight: 500;
            margin-bottom: 0.5rem;
        }
        
        #bbp_topic_title{
            display: block !important;
            width: 100% !important;
            border: 1px solid gainsboro !important;
            border-radius: 0.3rem;
            outline: 0;
            padding: 0.4rem 0.6rem;
            font-size: 0.9rem;
        }
        
        .bbp-form div.bbp-submit-wrapper{
            float: left;
        }
        
        .bbp-form .button{
            font-size: 0.9em;
            font-weight: 500;
            padding: 0.8em 2em;
            border: 0;
            outline: 0;
            background-color: darkgray;
            float: left;
            border-radius: 20px;
            cursor: pointer;
            transform: 0.3s all ease;
        }
        
        .bbp-form .button:hover{
            background-color: gainsboro;
            transform: 0.3s all ease;
        }
    </style>
    
    <style>
        .con-list li{
            display: flex;
            line-height: 1.4;
        }

        .con-list li span{
            flex: 1;
            padding-left: 5px;
        }

        .con-list li i{
            padding-top: 0.3em;
        }
    </style>
	
</head>
<body>

	
	<header class="main-navigation font-main" id="myHeader">
        <div class="navigation-wrapper">
            <a class="brand" href="https://www.saedconnect.org/">
                <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                <span class="name">
                    SAEDConnect
                </span>
                <span class="txt-sm txt-color-dark padding-l-10 margin-l-10 border-l-1">
                    In Partnership
                    <br>
                    with the NYSC
                </span>
            </a>
            <div class="action-btn-wrapper">
                
                <div class="search">
                    <div class="search-btn">
                        <i class="fa fa-search"></i>
                    </div>
                    <form action="" class="search-form">
                        <div class="wrapper">
                            <input class="search-box" type="search" placeholder="Type to Search">
                            <div class="close-btn">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-times fa-stack-1x"></i>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- User Account -->
                <div class="user-account">
                <?php
                    
                    if ( is_user_logged_in() ) {
                        $current_user = wp_get_current_user();
                ?>
                        
                    <div class="dropdown">
                        <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="account-info">
                                <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="">
                                <span class="profile-name">
                                    <?php echo $current_user->display_name ?>
                                </span>
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                            <a class="dropdown-item" href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=career-dashboard">
                                My Career Dashboard
                            </a>
                            <a class="dropdown-item" href="https://www.saedconnect.org/growth-programs/my-dashboard/">
                                My Entrepreneurship Dashboard
                            </a>
                            <a class="dropdown-item" href="<?php echo wp_logout_url( 'https://www.saedconnect.org' ); ?> ">
                                Logout
                            </a>
                        </div>
                    </div>
                        
                <?php } else { ?>        

                    <a class="login" href="https://www.saedconnect.org/login">Login</a>
                    <a class="signup" href="https://www.saedconnect.org/register">Sign up</a>
                    
                <?php } ?>
                </div>
                <button class="menu-btn hamburger hamburger--spring d-block d-sm-none">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>

        <!-- For Corp Members -->
        <nav class="navigation-list-wrapper left">
            <ul class="navigation-list">
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'find-your-path')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(573); ?>">
                        Find Your Path
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Discover Yourself
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Use our career guidance online tool to gain clarity about your strengths & weaknesses, and discover viable careers that match your talents.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" data-toggle="modal" href="#comingSoonModal">Start Here</a>  
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Get Counseling
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Let our counselors work with you to uncover your strengths, interpret your passions and provide objective and professional advice about optimal career paths to choose.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/about/reference/purpose-discovery-services/">Start Here</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Self Discovery Guide
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Dive into concepts & explanations that help you appreciate your uniqueness and guide you in making informed career decisions.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/self-discovery-guide/">Start Here</a>  
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
                
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="https://www.saedconnect.org/about/synopsis/find-your-path/">
                        Find Your Path
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </li>
                
                <li class="hasSubMenu">
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'get-a-job')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/get-your-job">
                        Get a Job
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-6 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Career Development Programs
                                    </h4>
                                    <div class="row row-10">
                                        <div class="col-12 col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/JobTrac_program.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/jobtrac/">
                                                            JobTrac
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block  txt-sm txt-height-1-1">
                                                        Preparing young people for work & life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="col-12 col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/successKeys_program.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/successkeys/">
                                                            SuccessKeys
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block  txt-sm txt-height-1-1">
                                                        Preparing young people for work & life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>-->
                                        <div class="col-12 col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/job_interview_preparation.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/program/career-forum/">
                                                            JobAdvisor
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block  txt-sm txt-height-1-1">
                                                        Create an amazing CV for yourself.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <!--<div class="row row-5 sub-menu-image-box padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/staff_source.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="https://www.saedconnect.org/esaed/mini-site/talentsource/">
                                                    The Job Challenge
                                                </a>
                                            </h4>
                                            <p class="txt-sm txt-height-1-1">
                                                Get hired for specific job roles.
                                            </p>
                                        </div>
                                    </div>-->
                                    </div>
                                </div>
                                
                                <!--<div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Tools
                                    </h4>
                                    
                                    <div class="row row-5 sub-menu-image-box padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/cv_cover_letter_service.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="http://www.saedconnect.org/esaed/mini-site/tutorlink/">
                                                    Online CV Creator
                                                </a>
                                            </h4>
                                            <p class="txt-sm txt-height-1-1">
                                                Create an amazing CV for yourself.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row row-5 sub-menu-image-box padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/service_provider_directory.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="https://www.saedconnect.org/esaed/mini-site/talentsource/">
                                                    Database of Stars
                                                </a>
                                            </h4>
                                            <p class="txt-sm txt-height-1-1">
                                                Get exposed to local & international employers.
                                            </p>
                                        </div>
                                    </div>
                                </div> -->  
                            
                                <!--<div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/career-guide">
                                            Job Search & Career Development Guide
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 3)
                                        switch_to_blog(3);
                                                                                                        
                                        //WP Query to get Guides
                                        
                                        $temp = $wp_query; $wp_query= null;
                                        $wp_query = new WP_Query();
                                        $wp_query->query(array('post_type' => 'guide-section'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php echo the_permalink() ?>">
                                                <?php echo the_title() ?>
                                            </a>
                                        </li>
                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                        endwhile;
                                        wp_reset_postdata();
                                        wp_reset_query();
                                        $wp_query = $temp;
                                                                                                        
                                    ?>
                                    </ul>
                                </div>-->
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Get Expert Help
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/cv-cover-letter-prep//">
                                                CV & Cover letter Creation & Review
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/linkedin-page-creation-review-networking-coaching/">
                                                Linkedin Profile Creation & Review
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/job-search-coaching/">
                                                Job Search Coaching
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/interview-prep/">
                                                Interview Coaching
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                
                <li class="hasSubMenu">
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/start-your-business/">
                        Do Business
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                               
                                <div class="col-md-6 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Entrepreneurship Development Programs
                                    </h4>
                                    <div class="row row-10">
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/side_hustle_communities.jpeg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/small-business-communities/">
                                                            Side Hustle Communities
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Meet with fellow Entrepreneurs accross over 40 businesses nationwide.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/join_a_professional_association.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://saedconnect.org/growth-programs/small-business-mentor/">
                                                            Small Business Mentor
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Get all the help you need to start and grow your business.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>-->
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/the_entrepreneurship_incubator.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/the-entrepreneurship-incubator/">
                                                            The Entrepreneurship Incubator
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Generate your brilliant idea & build it into a thriving business.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/get_business_counseling.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/business-clinic/">
                                                            The Business Clinic
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Start earning from a small business of your choice.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/join_a_professional_association.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/program/side-hustle-skills/">
                                                            Side Hustle Mentor Hubs
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block txt-sm txt-height-1-1">
                                                        Connect with expert mentors to help you start and grow your side hustle.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <!--<div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Venture ToolPack
                                    </h4>
                                    <div class="row sub-menu-image-box padding-b-10">
                                        <figure class="image-item">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/cv_cover_letter_service.jpg" alt="">
                                        </figure>

                                        <p class="txt-sm margin-t-5">
                                            A suit of tools to structure and showcase your business to investors and potential partners
                                        </p>
                                    </div>
                                </div>-->
                                
                                <!--<div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/entrepreneurship-guide/">
                                            Doing Business Guide
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 2)
                                        switch_to_blog(2);
                                                                                                        
                                        //WP Query to get Guides
                                        $temp = $wp_query; $wp_query= null;
                                        $wp_query = new WP_Query();
                                        $wp_query->query(array('post_type' => 'guide-section'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php echo the_permalink() ?>">
                                                <?php echo the_title() ?>
                                            </a>
                                        </li>
                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                        endwhile;
                                        wp_reset_postdata();
                                        wp_reset_query();
                                        $wp_query = $temp;
                                    ?>
                                    </ul>
                                </div>-->
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Get Expert Help
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-development-consulting/">
                                                Business Counseling
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-plan-creation/">
                                                Business Plan Creation
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/raise-finance-for-your-business/">
                                                Raise Finance for your Business
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-registrations/">
                                                Business Registration & Legal Services
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/about/discourse/business-branding/">
                                                Business Branding
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="https://www.saedconnect.org/service-provider-directory/trainers/">
                        Learn a Skill
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </li>
                <!--
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'make-money')
                                echo 'active';
                            }
                        ?>" href="https://www.saedconnect.org/programs/make-money/">
                        Make Money
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                -->
                <!--<li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'learn-a-skill')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/learn-a-skill">
                        Growth Programs
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <h4 class="txt-xlg txt-bold txt-height-1-4">
                                        Innovative programs to help you develop & activate your employability & entrepreneurship capacity.
                                    </h4>
                                </div>
                                <div class="col-md-9 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Available Programs
                                    </h4>
                                    <div class="row row-10">
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/school_oversea.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/side-hustle-skills/">
                                                            Side Hustle Communities
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Start earning from a small business of your choice.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/successKeys_program.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/successkeys/">
                                                            SuccessKeys
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Preparing young people for work & life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/staff_source.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/esaed/mini-site/talentsource/">
                                                            The Job Challenge
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Get hired for specific job roles.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/the_entrepreneurship_incubator.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/the-entrepreneurship-incubator/">
                                                            The Entrepreneurship Incubator
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Generate your brilliant idea & build it into a thriving business.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/get_business_counseling.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/growth-programs/program/business-clinic/">
                                                            The Business Clinic
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Start earning from a small business of your choice.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/JobTrac_program.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/jobtrac/">
                                                            JobTrac
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Preparing young people for work & life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/job_interview_preparation.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/program/career-forum/">
                                                            JobAdvisor
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Create an amazing CV for yourself.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-4 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/school_oversea.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/skill-splash/program/tech-career-mentorship/">
                                                            Tech Mentorship Scheme
                                                        </a>
                                                    </h4>
                                                    <p class="txt-sm txt-height-1-1">
                                                        Join the revolution. Start a career in tech.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
            </ul>
            <ul class="navigation-list">
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="http://www.saedconnect.org/esaed">
                        eSAED
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                -->
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="https://www.saedconnect.org/opportunity-center/">
                        Opportunity Center
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </li>
                <li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                    >
                        Get Involved
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="d-none d-md-block col-md-3 padding-lr-20 padding-b-10">
                                    <h4 class="txt-xlg txt-bold txt-height-1-4">
                                        Join us in building tomorrow's leaders, today.
                                    </h4>
                                </div>
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/trainings-overview/">
                                            Offer Trainings & Youth Services
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/join-the-trainer-directory/">
                                                Join the Trainer Directory
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/list-your-offers-events-and-special-programs-on-our-opportunity-directory/">
                                                List your offers, events, and special programs on our opportunity Directory
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/join-the-service-provider-directory/">
                                                Join the Service Provider Directory
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="sub-menu-list">               
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 8)
                                        switch_to_blog(11);

                                        $connected = new WP_Query( array(
                                            'relationship' => array(
                                                'id'   => 'mini_site_to_two_c_m_page',
                                                'from' => 608, // You can pass object ID or full object
                                            ),
                                            'nopaging' => true,
                                        ) );
                                        while ( $connected->have_posts() ) : $connected->the_post(); ?>
                                            <!--<li>
                                               <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                            </li>-->
                                    <?php
                                        endwhile;
                                        wp_reset_postdata();
                                                                                                        
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/support-youth-development-overview/">
                                            Support Youth Development
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/corporates-donor-agencies/">
                                                Corporates & Donor Agencies
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/individuals/">
                                                Individuals
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/schools-youth-institutions/">
                                                Schools & Youth Institutions
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/religious-organisations/">
                                                Religious Organizations
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/governments-government-agencies/">
                                                Governments & Government Agencies
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Ways to Support
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/execute-or-co-create-a-youth-development-program/">
                                                Execute or Co-create a Youth Development Program
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/support-with-sensitization/">
                                                Inspire & Promote Excellence
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/share-your-story/">
                                                Become a Mentor/Coach
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/lend-your-expertise/">
                                                Lend Your Expertise
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/support-a-project/">
                                                Support a Project
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/contribute/discourse/volunteer/">
                                                Volunteer
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="sub-menu-list">               
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 8)
                                        switch_to_blog(11);

                                        $connected = new WP_Query( array(
                                            'relationship' => array(
                                                'id'   => 'mini_site_to_two_c_m_page',
                                                'from' => 625, // You can pass object ID or full object
                                            ),
                                            'nopaging' => true,
                                        ) );
                                        while ( $connected->have_posts() ) : $connected->the_post(); ?>
                                            <!--<li>
                                               <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                            </li>-->
                                    <?php
                                        endwhile;
                                        wp_reset_postdata();
                                                                                                        
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>
                                    </ul>
                                </div>
                                <!--<div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/donate-overview/">
                                            Donate
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="http://www.saedconnect.org/contribute/synopsis/donate-ways-to-give/">
                                                Ways to Give
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/contribute/synopsis/donate-online/">
                                                Donate Online
                                            </a>
                                        </li>
                                    </ul>
                                </div>-->
                                
                            </div>
                        </div>
                    </div>
                </li>
                
                <li class="hasSubMenu">
                    <span
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                    >
                        About
                        <i class="fa fa-plus"></i>
                    </span>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <!--<div class="d-none d-md-block col-md-3 padding-lr-20 padding-b-10">
                                    <h4 class="txt-xlg txt-bold txt-height-1-4">
                                        Join us in building tomorrow's leaders, today.
                                    </h4>
                                </div>-->
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="https://www.saedconnect.org/about/synopsis/about-saedconnect-overview/">
                                            About Us
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="https://www.saedconnect.org/about/discourse/our-mission/">
                                                Our Mission
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/about/discourse/the-problem-we-are-solving/">
                                                The Problem We are Solving
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/about/discourse/how-we-help/">
                                                How we Help
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/about/discourse/our-approach/">
                                                Our Approach
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/about/discourse/our-platforms/">
                                                Our Platforms
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.saedconnect.org/about/discourse/partnership-with-nysc/">
                                                Our Partnership with NYSC
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="#">
                                            Programs & Projects
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="#">
                                                PushUp
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Small Business Mentor Award
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Reboot
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                DisKover
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="https://www.saedconnect.org/growth-programs/mentor-directory/">
                                            Meet Our Mentors
                                        </a>
                                    </h4>
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/JobTrac_program.jpg" alt="" class="margin-t-20">
<!--                                    <div class="row row-10">
                                        <div class="col-12 col-md-8 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="d-none d-md-block col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://www.saedconnect.org/growth-programs/mentor-directory/">
                                                            The Mentor Directory
                                                        </a>
                                                    </h4>
                                                    <p class="d-none d-md-block  txt-sm txt-height-1-1">
                                                        Browse our growing list of mentors to find your personal design coach!
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </li>
                
                <!--<li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                        href="https://www.saedconnect.org/about/synopsis/about-saedconnect-overview/">
                        About
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </li>-->
            </ul>
            <div class="user-account padding-t-30">
            <?php

                if ( is_user_logged_in() ) {
                    $current_user = wp_get_current_user();
            ?>

                <div class="dropdown">
                    <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="account-info">
                            <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="">
                            <span class="profile-name">
                                <?php echo $current_user->display_name ?>
                            </span>
                        </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                        <a class="dropdown-item" href="https://www.saedconnect.org/my-dashboard/growth-programs/?action=career-dashboard">
                            My Career Dashboard
                        </a>
                        <a class="dropdown-item" href="https://www.saedconnect.org/growth-programs/my-dashboard/">
                            My Entrepreneurship Dashboard
                        </a>
                        <a class="dropdown-item" href="<?php echo wp_logout_url( 'https://www.saedconnect.org' ); ?> ">
                            Logout
                        </a>
                    </div>
                </div>

            <?php } else { ?>        

                <a class="login" href="https://www.saedconnect.org/login">Login</a>
                <a class="signup" href="https://www.saedconnect.org/register">Sign up</a>

            <?php } ?>
            </div>
        </nav>

	</header>
	