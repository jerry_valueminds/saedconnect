<?php /*Template Name: Profile - Thematic Areas*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        $tax_type = 'thematic-category';

        /* Publication */
        $publication_key   = 'publication_status';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
                
                <?php if($_GET['view'] == 'form'){ //Display form  ?>
                
                    <?php if( $_GET['form'] == 'thematic-area' ){ //GF View ?>

                        <div class="page-header">
                            <h1 class="page-title">
                                Create / Edit a Thematic Area         
                            </h1>
                            <div class="cta">
                                <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                                    Cancel
                                </a>
                            </div>
                        </div>
                        <article class="page-summary">
                            <p>
                                These are the abilities you possess. These would help you get offers or gigs.
                            </p>
                        </article>
                        <form class="form" action="<?php echo currenturl(false) ?>" method="post">
                            <?php

                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */

                                /* Variables */
                                $post_type = 'thematic-area';
                                $post_id = $_GET['post-id'];   //Get Program ID

                                if( $post_id && $_GET['action'] == 'delete' ){
                                    wp_delete_post($post_id); // Delete
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) ); // Redirect
                                }


                                if($_POST){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $thematic_categories = $_POST['thematic_categories'];
                                    $summary = sanitize_text_field( $_POST['summary'] ); 
                                    $description = wp_kses_post( $_POST['description'] ); 
                                    $sdg = $_POST['sdg'];

                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $post_type,
                                        'post_title' => $post_name,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $post_id, 'thematic_categories', $thematic_categories );
                                    update_post_meta( $post_id, 'summary', $summary );
                                    update_post_meta( $post_id, 'description', $description );
                                    update_post_meta( $post_id, 'sdg', $sdg );

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                }
                            ?>
                            <div class="form-item">
                                <label for="post_name">Title</label>
                                <input 
                                    type="text" 
                                    name="post_name" 
                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                >
                            </div>
                            <?php 
                                $terms = get_terms( $tax_type, array('hide_empty' => false, 'parent' => 0) );
                                if( $terms ){
                                    $meta = get_post_meta( $post_id, 'thematic_categories', true );
                            ?>
                            <div class="form-item">
                                <label class="d-block padding-b-10">Select Categories</label>
                                <?php foreach($terms as $term ){ ?>
                                <label class="txt-sm d-inline-flex align-items-center padding-b-10 padding-r-15">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="<?php echo $term->term_id ?>" 
                                        name="thematic_categories[]" 
                                        <?php echo ( in_array( $term->term_id, $meta) ) ? 'checked' : '' ?>
                                    >
                                    <span class="padding-l-10">
                                        <?php echo $term->name ?>
                                    </span>
                                </label>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <div class="form-item">
                                <label for="summary">Summary</label>
                                <textarea name="summary" id="summary" rows="4"><?php echo get_post_meta( $post_id, 'summary', true ); ?></textarea>
                            </div>
                            <div class="form-item">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" rows="8" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                            </div>
                            <div class="form-item">
                                <?php $meta = get_post_meta( $post_id, 'sdg', true ); ?>
                                <label class="padding-b-10">SDGs addressed by this project</label>
                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="No Poverty" 
                                        name="sdg[]" 
                                        <?php echo ( in_array( 'No Poverty', $meta) ) ? 'checked' : '' ?>
                                    >
                                    <span class="padding-l-10">
                                        No Poverty
                                    </span>
                                </label>
                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="Zero Hunger" 
                                        name="sdg[]" 
                                        <?php echo ( in_array( 'Zero Hunger', $meta) ) ? 'checked' : '' ?>
                                    >
                                    <span class="padding-l-10">
                                        Zero Hunger
                                    </span>
                                </label>
                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="Good Health and Well-being" 
                                        name="sdg[]" 
                                        <?php echo ( in_array( 'Good Health and Well-being', $meta) ) ? 'checked' : '' ?>
                                    >
                                    <span class="padding-l-10">
                                        Good Health and Well-being
                                    </span>
                                </label>
                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="Quality Education" 
                                        name="sdg[]" 
                                        <?php echo ( in_array( 'Quality Education', $meta) ) ? 'checked' : '' ?>
                                    >
                                    <span class="padding-l-10">
                                        Quality Education
                                    </span>
                                </label>
                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="Gender Equality" 
                                        name="sdg[]" 
                                        <?php echo ( in_array( 'Gender Equality', $meta) ) ? 'checked' : '' ?>
                                    >
                                    <span class="padding-l-10">
                                        Gender Equality
                                    </span>
                                </label>
                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="Clean Water and Sanitation" 
                                        name="sdg[]" 
                                        <?php echo ( in_array( 'Clean Water and Sanitation', $meta) ) ? 'checked' : '' ?>
                                    >
                                    <span class="padding-l-10">
                                        Clean Water and Sanitation
                                    </span>
                                </label>
                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="Affordable and Clean Energy" 
                                        name="sdg[]" 
                                        <?php echo ( in_array( 'Affordable and Clean Energy', $meta) ) ? 'checked' : '' ?>
                                    >
                                    <span class="padding-l-10">
                                        Affordable and Clean Energy
                                    </span>
                                </label>
                                <label class="txt-sm d-flex align-items-center padding-b-10">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="Decent Work and Economic Growth" 
                                        name="sdg[]"
                                        <?php echo ( in_array( 'Decent Work and Economic Growth', $meta) ) ? 'checked' : '' ?> 
                                    >
                                    <span class="padding-l-10">
                                        Decent Work and Economic Growth
                                    </span>
                                </label>
                            </div>
                            <div class="text-right">
                                <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>

                    <?php } elseif( $_GET['form'] == 'problem-area' ) { ?>
                    
                        <?php 
                            $parent_id = $_GET['parent'];
                        ?>
                    
                        <div class="page-header">
                            <h1 class="page-title">
                                Add / Edit a Problem Area on <?php echo get_the_title($parent_id); ?>
                            </h1>
                            <div class="cta">
                                <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                                    Cancel
                                </a>
                            </div>
                        </div>
                        <article class="page-summary">
                            <p>
                                These are the abilities you possess. These would help you get offers or gigs.
                            </p>
                        </article>
                        <form class="form" action="<?php echo currenturl(false) ?>" method="post">
                            <?php

                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */

                                /* Variables */
                                $post_type = 'problem-area';
                                $post_id = $_GET['post-id'];   //Get Program ID

                                if( $post_id && $_GET['action'] == 'delete' ){
                                    wp_delete_post($post_id); // Delete
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=manage-problems&post-id='.$parent_id ); // Redirect
                                }


                                if($_POST){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $summary = sanitize_text_field( $_POST['summary'] ); 
                                    $description = wp_kses_post( $_POST['description'] ); 

                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $post_type,
                                        'post_title' => $post_name,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $post_id, 'summary', $summary );
                                    update_post_meta( $post_id, 'description', $description );
                                    update_post_meta( $post_id, 'parent_post', $parent_id );

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=manage-problems&post-id='.$parent_id );
                                }
                            ?>
                            <div class="form-item">
                                <label for="post_name">Title</label>
                                <input 
                                    type="text" 
                                    name="post_name" 
                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                >
                            </div>
                            <div class="form-item">
                                <label for="summary">Summary</label>
                                <textarea name="summary" id="summary" rows="4"><?php echo get_post_meta( $post_id, 'summary', true ); ?></textarea>
                            </div>
                            <div class="form-item">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" rows="8" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                            </div>
                            <div class="text-right">
                                <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                        
                    <?php } elseif( $_GET['form'] == 'question' ) { ?>
                    
                        <?php 
                            $parent_id = $_GET['parent'];
                        ?>
                    
                        <div class="page-header">
                            <h1 class="page-title">
                                Add / Edit a Question on <?php echo get_the_title($parent_id); ?>
                            </h1>
                            <div class="cta">
                                <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                                    Cancel
                                </a>
                            </div>
                        </div>
                        <article class="page-summary">
                            <p>
                                Add or edit a question on <?php echo get_the_title($parent_id); ?>.
                            </p>
                        </article>
                        <form class="form" action="<?php echo currenturl(false) ?>" method="post">
                            <?php

                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */

                                /* Variables */
                                $post_type = 'theme-question';
                                $post_id = $_GET['post-id'];   //Get Program ID

                                if( $post_id && $_GET['action'] == 'delete' ){
                                    wp_delete_post($post_id); // Delete
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=manage-questions&post-id='.$parent_id ); // Redirect
                                }


                                if($_POST){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $type = sanitize_text_field( $_POST['type'] ); 
                                    $summary = sanitize_text_field( $_POST['summary'] ); 
                                    $description = wp_kses_post( $_POST['description'] ); 

                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $post_type,
                                        'post_title' => $post_name,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $post_id, 'type', $type );
                                    update_post_meta( $post_id, 'summary', $summary );
                                    update_post_meta( $post_id, 'description', $description );
                                    update_post_meta( $post_id, 'parent_post', $parent_id );

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=manage-questions&post-id='.$parent_id );
                                }
                            ?>
                            <div class="form-item">
                                <label for="post_name">Question</label>
                                <input 
                                    type="text" 
                                    name="post_name" 
                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                >
                            </div>
                            <div class="form-item">
                                <label for="type">
                                    Question Type
                                </label>
                                <select name="type" id="capacity">
                                    <?php $meta = get_post_meta( $post_id, 'type', true ); ?>
                                    <option value="Idea" <?php echo ($meta == 'Idea') ? 'selected' : '' ?>>Idea</option>
                                    <option value="Experience" <?php echo ($meta == 'Experience') ? 'selected' : '' ?>>Experience</option>
                                    <option value="Suggestion" <?php echo ($meta == 'Suggestion') ? 'selected' : '' ?>>Suggestion</option>
                                </select>
                            </div>
                            <div class="form-item">
                                <label for="summary">Summary</label>
                                <textarea name="summary" id="summary" rows="4"><?php echo get_post_meta( $post_id, 'summary', true ); ?></textarea>
                            </div>
                            <div class="form-item">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" rows="8" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                            </div>
                            <div class="text-right">
                                <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form> 
                        
                    <?php } elseif( $_GET['form'] == 'task' ) { ?>
                    
                        <?php 
                            $parent_id = $_GET['parent'];
                        ?>
                    
                        <div class="page-header">
                            <h1 class="page-title">
                                Add / Edit a Task on <?php echo get_the_title($parent_id); ?>
                            </h1>
                            <div class="cta">
                                <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                                    Cancel
                                </a>
                            </div>
                        </div>
                        <article class="page-summary">
                            <p>
                                Add or edit a task for <?php echo get_the_title($parent_id); ?>.
                            </p>
                        </article>
                        <form class="form" action="<?php echo currenturl(false) ?>" method="post">
                            <?php

                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */

                                /* Variables */
                                $post_type = 'theme-task';
                                $post_id = $_GET['post-id'];   //Get Program ID

                                if( $post_id && $_GET['action'] == 'delete' ){
                                    wp_delete_post($post_id); // Delete
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=manage-tasks&post-id='.$parent_id ); // Redirect
                                }


                                if($_POST){

                                    /* Get Post Name */
                                    $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $type = sanitize_text_field( $_POST['type'] ); 
                                    $description = wp_kses_post( $_POST['description'] ); 
                                    $expiry_date = $_POST['expiry_date']; 
                                    $link = sanitize_text_field( $_POST['link'] ); 
                                    $requires_registeration = sanitize_text_field( $_POST['requires_registeration'] ); 

                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $post_type,
                                        'post_title' => $post_name,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    update_post_meta( $post_id, 'type', $type );
                                    update_post_meta( $post_id, 'description', $description );
                                    update_post_meta( $post_id, 'expiry_date', $expiry_date );
                                    update_post_meta( $post_id, 'link', $link );
                                    update_post_meta( $post_id, 'requires_registeration', $requires_registeration );
                                    
                                    update_post_meta( $post_id, 'parent_post', $parent_id );

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?view=manage-tasks&post-id='.$parent_id );
                                }
                            ?>
                            <div class="form-item">
                                <label for="post_name">Activity Title</label>
                                <input 
                                    type="text" 
                                    name="post_name" 
                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                >
                            </div>
                            <div class="form-item">
                                <label for="type">
                                    Activity Type
                                </label>
                                <select name="type" id="type">
                                    <?php $meta = get_post_meta( $post_id, 'type', true ); ?>
                                    <option value="Event" <?php echo ($meta == 'Event') ? 'selected' : '' ?>>Event</option>
                                    <option value="Research" <?php echo ($meta == 'Research') ? 'selected' : '' ?>>Research</option>
                                    <option value="Content Development" <?php echo ($meta == 'Content Development') ? 'selected' : '' ?>>Content Development</option>
                                    <option value="Advocacy" <?php echo ($meta == 'Advocacy') ? 'selected' : '' ?>>Advocacy</option>
                                    <option value="Speaking" <?php echo ($meta == 'Speaking') ? 'selected' : '' ?>>Speaking</option>
                                    <option value="Implementation" <?php echo ($meta == 'Implementation') ? 'selected' : '' ?>>Implementation</option>
                                </select>
                            </div>
                            <div class="form-item">
                                <label for="description">Activity Description</label>
                                <textarea name="description" id="description" rows="8" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                            </div>
                            <div class="form-item">
                                <label for="expiry_date">
                                    Participation Expiry date
                                </label>
                                <input type="date" name="expiry_date" value="<?php echo get_post_meta( $post_id, 'expiry_date', true ); ?>">
                            </div>
                            <div class="form-item">
                                <label for="link">
                                    Link
                                </label>
                                <input type="url" name="link" value="<?php echo get_post_meta( $post_id, 'link', true ); ?>">
                            </div>
                            <div class="form-item">
                                <label for="requires_registeration">
                                    Register to participate?
                                </label>
                                <select name="requires_registeration" id="requires_registeration">
                                    <?php $meta = get_post_meta( $post_id, 'requires_registeration', true ); ?>
                                    <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?>>Yes</option>
                                    <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?>>No</option>
                                </select>
                            </div>
                            <div class="text-right">
                                <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>

                    <?php } else { ?>
                    
                        <div class="section-wrapper">
                            <p>Something went wrong.</p>
                        </div>

                    <?php } ?>
                    
                <?php } elseif( $_GET['view'] == 'manage-problems' && $_GET['post-id'] ) { ?>    
                
                    <?php $parent_id = $_GET['post-id']; ?>
                
                    <div class="page-header">
                        <h1 class="page-title">
                            <?php echo get_the_title( $parent_id ) ?>
                        </h1>
                        <div class="cta">
                            <a href="<?php echo currentUrl(true); ?>" class="cta-btn margin-r-10">
                                All Thematic Areas
                            </a>
                            <a href="<?php echo currentUrl(true).'?view=form&form=problem-area&parent='.$parent_id; ?>" class="cta-btn btn-ash txt-color-white" style="color:white">
                                Add Problem Area
                            </a>
                        </div>
                    </div>

                    <article class="page-summary">
                        <p>
                            Create new projects and view all of the projects that you have previously added and are currently working on.
                        </p>
                    </article>

                    <div class="row row-5">
                        <style>
                            .publication-status{
                                position: absolute;
                                display: flex;
                                align-items: center;
                                top: 0;
                                right: 0;
                                padding: 5px 10px;
                                color: black;
                                background-color: gainsboro;
                            }

                            .user_published{
                               background-color: #f4c026; 
                            }

                            .admin_published{
                               background-color: #00bfe7; 
                            }

                            .home-community-card{
                                overflow: visible;
                            }
                        </style>
                        <?php
                            function truncate($string, $length){
                                if (strlen($string) > $length) {
                                    $string = substr($string, 0, $length) . '...';
                                }

                                return $string;
                            }

                            wp_reset_postdata();
                            wp_reset_query();
                            $temp = $wp_query; $wp_query= null;
                            $wp_query = new WP_Query();
                            $wp_query->query( 
                                array(
                                    'post_type' => 'problem-area',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'parent_post',
                                            'value' => $parent_id,
                                        ),
                                    ),
                                ) 
                            );

                            if ( $wp_query->have_posts() ) {

                                while ($wp_query->have_posts()) : $wp_query->the_post();

                                /* Variables */
                                $post_id = $post->ID;    //Get Program ID

                                /* Publication status */      
                                $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                /* Get Opportunity Banner */
                                $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                                $image = reset( $images );
                        ?>
                            <div class="col-md-4 padding-lr-5 padding-b-20 d-flex">
                                <div class="home-community-card position-relative">
                                    <article class="content">
                                        <div class="info bg-white">
                                            <h3 class=" txt-medium padding-t-40 margin-b-10">
                                                <a href="<?php the_permalink() ?>">
                                                    <?php the_title() ?>
                                                </a>
                                            </h3>
                                            <p class="txt-sm">
                                                <?php
                                                    $meta = get_post_meta($post_id, 'personal_project_description', true);
                                                    if($meta){
                                                        echo truncate($meta, 70);
                                                    }
                                                ?>
                                            </p>
                                            <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                                <div>
                                                    <a 
                                                       href="<?php printf('%s/?view=form&form=problem-area&post-id=%s&parent=%s', currentUrl(true), $post_id, $parent_id ) ?>"
                                                       class="txt-medium txt-color-green"
                                                    >
                                                        Edit
                                                    </a>
                                                    |
                                                    <a 
                                                       href="<?php printf('%s/?view=form&form=problem-area&action=delete&post-id=%s&parent=%s', currentUrl(true), $post_id, $parent_id ) ?>"
                                                       class="confirm-delete txt-color-red"
                                                    >
                                                        Delete
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </article>  
                                </div>   
                            </div>
                        <?php
                                endwhile;

                            }else{
                        ?>
                            <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                                <h2 class="txt-lg txt-medium">
                                    No Areas found.
                                </h2>
                            </div>   

                        <?php } ?>
                    </div>
                    
                <?php } elseif( $_GET['view'] == 'manage-questions' && $_GET['post-id'] ) { ?>    
                
                    <?php $parent_id = $_GET['post-id']; ?>
                
                    <div class="page-header">
                        <h1 class="page-title">
                            Add questions to <?php echo get_the_title( $parent_id ) ?>
                        </h1>
                        <div class="cta">
                            <a href="<?php echo currentUrl(true); ?>" class="cta-btn margin-r-10">
                                All Thematic Areas
                            </a>
                            <a href="<?php echo currentUrl(true).'?view=form&form=question&parent='.$parent_id; ?>" class="cta-btn btn-ash txt-color-white" style="color:white">
                                Add Question
                            </a>
                        </div>
                    </div>

                    <article class="page-summary">
                        <p>
                            Assign questions to <?php echo get_the_title( $parent_id ) ?>.
                        </p>
                    </article>

                    <div class="row row-5">
                        <style>
                            .publication-status{
                                position: absolute;
                                display: flex;
                                align-items: center;
                                top: 0;
                                right: 0;
                                padding: 5px 10px;
                                color: black;
                                background-color: gainsboro;
                            }

                            .user_published{
                               background-color: #f4c026; 
                            }

                            .admin_published{
                               background-color: #00bfe7; 
                            }

                            .home-community-card{
                                overflow: visible;
                            }
                        </style>
                        <?php
                            function truncate($string, $length){
                                if (strlen($string) > $length) {
                                    $string = substr($string, 0, $length) . '...';
                                }

                                return $string;
                            }

                            wp_reset_postdata();
                            wp_reset_query();
                            $temp = $wp_query; $wp_query= null;
                            $wp_query = new WP_Query();
                            $wp_query->query( 
                                array(
                                    'post_type' => 'theme-question',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'parent_post',
                                            'value' => $parent_id,
                                        ),
                                    ),
                                ) 
                            );

                            if ( $wp_query->have_posts() ) {

                                while ($wp_query->have_posts()) : $wp_query->the_post();

                                /* Variables */
                                $post_id = $post->ID;    //Get Program ID

                                /* Publication status */      
                                $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                /* Get Opportunity Banner */
                                $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                                $image = reset( $images );
                        ?>
                            <div class="col-md-4 padding-lr-5 padding-b-20 d-flex">
                                <div class="home-community-card position-relative">
                                    <article class="content">
                                        <div class="info bg-white">
                                            <h3 class="txt-medium txt-height-1-4 margin-b-10">
                                                <a href="<?php the_permalink() ?>">
                                                    <?php the_title() ?>
                                                </a>
                                            </h3>
                                            <p class="txt-sm">
                                                <?php
                                                    $meta = get_post_meta($post_id, 'personal_project_description', true);
                                                    if($meta){
                                                        echo truncate($meta, 70);
                                                    }
                                                ?>
                                            </p>
                                            <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                                <div>
                                                    <a 
                                                       href="<?php printf('%s/?view=form&form=question&post-id=%s&parent=%s', currentUrl(true), $post_id, $parent_id ) ?>"
                                                       class="txt-medium txt-color-green"
                                                    >
                                                        Edit
                                                    </a>
                                                    |
                                                    <a 
                                                       href="<?php printf('%s/?view=form&form=question&action=delete&post-id=%s&parent=%s', currentUrl(true), $post_id, $parent_id ) ?>"
                                                       class="confirm-delete txt-color-red"
                                                    >
                                                        Delete
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </article>  
                                </div>   
                            </div>
                        <?php
                                endwhile;

                            }else{
                        ?>
                            <div class="col-12 padding-lr-5 padding-t-20 padding-b-40">
                                <h2 class="txt-lg txt-medium">
                                    No Questions found.
                                </h2>
                            </div>   

                        <?php } ?>
                    </div>
                    
                <?php } elseif( $_GET['view'] == 'manage-tasks' && $_GET['post-id'] ) { ?>    
                
                    <?php $parent_id = $_GET['post-id']; ?>
                
                    <div class="page-header">
                        <h1 class="page-title">
                            Add tasks to <?php echo get_the_title( $parent_id ) ?>
                        </h1>
                        <div class="cta">
                            <a href="<?php echo currentUrl(true); ?>" class="cta-btn margin-r-10">
                                All Thematic Areas
                            </a>
                            <a href="<?php echo currentUrl(true).'?view=form&form=task&parent='.$parent_id; ?>" class="cta-btn btn-ash txt-color-white" style="color:white">
                                Add Task
                            </a>
                        </div>
                    </div>

                    <article class="page-summary">
                        <p>
                            Assign Tasks to <?php echo get_the_title( $parent_id ) ?>.
                        </p>
                    </article>

                    <div class="row row-5">
                        <style>
                            .publication-status{
                                position: absolute;
                                display: flex;
                                align-items: center;
                                top: 0;
                                right: 0;
                                padding: 5px 10px;
                                color: black;
                                background-color: gainsboro;
                            }

                            .user_published{
                               background-color: #f4c026; 
                            }

                            .admin_published{
                               background-color: #00bfe7; 
                            }

                            .home-community-card{
                                overflow: visible;
                            }
                        </style>
                        <?php
                            function truncate($string, $length){
                                if (strlen($string) > $length) {
                                    $string = substr($string, 0, $length) . '...';
                                }

                                return $string;
                            }

                            wp_reset_postdata();
                            wp_reset_query();
                            $temp = $wp_query; $wp_query= null;
                            $wp_query = new WP_Query();
                            $wp_query->query( 
                                array(
                                    'post_type' => 'theme-task',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => 'parent_post',
                                            'value' => $parent_id,
                                        ),
                                    ),
                                ) 
                            );

                            if ( $wp_query->have_posts() ) {

                                while ($wp_query->have_posts()) : $wp_query->the_post();

                                /* Variables */
                                $post_id = $post->ID;    //Get Program ID

                                /* Publication status */      
                                $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                /* Get Opportunity Banner */
                                $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                                $image = reset( $images );
                        ?>
                            <div class="col-md-4 padding-lr-5 padding-b-20 d-flex">
                                <div class="home-community-card position-relative">
                                    <article class="content">
                                        <div class="info bg-white">
                                            <h3 class="txt-medium txt-height-1-4 margin-b-10">
                                                <a href="<?php the_permalink() ?>">
                                                    <?php the_title() ?>
                                                </a>
                                            </h3>
                                            <p class="txt-sm">
                                                <?php
                                                    $meta = get_post_meta($post_id, 'personal_project_description', true);
                                                    if($meta){
                                                        echo truncate($meta, 70);
                                                    }
                                                ?>
                                            </p>
                                            <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                                <div>
                                                    <a 
                                                       href="<?php printf('%s/?view=form&form=task&post-id=%s&parent=%s', currentUrl(true), $post_id, $parent_id ) ?>"
                                                       class="txt-medium txt-color-green"
                                                    >
                                                        Edit
                                                    </a>
                                                    |
                                                    <a 
                                                       href="<?php printf('%s/?view=form&form=task&action=delete&post-id=%s&parent=%s', currentUrl(true), $post_id, $parent_id ) ?>"
                                                       class="confirm-delete txt-color-red"
                                                    >
                                                        Delete
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </article>  
                                </div>   
                            </div>
                        <?php
                                endwhile;

                            }else{
                        ?>
                            <div class="col-12 padding-lr-5 padding-t-20 padding-b-40">
                                <h2 class="txt-lg txt-medium">
                                    No Tasks found.
                                </h2>
                            </div>   

                        <?php } ?>
                    </div>
                
                <?php } else { ?>
                
                    <div class="page-header">
                        <h1 class="page-title">
                            Thematic Areas
                        </h1>
                        <div class="cta">
                            <a href="<?php echo currentUrl(true); ?>?view=form&form=thematic-area" class="cta-btn">
                                Add Area
                            </a>
                        </div>
                    </div>

                    <article class="page-summary">
                        <p>
                            Create new projects and view all of the projects that you have previously added and are currently working on.
                        </p>
                    </article>

                    <div class="row row-5">
                        <style>
                            .publication-status{
                                position: absolute;
                                display: flex;
                                align-items: center;
                                top: 0;
                                right: 0;
                                padding: 5px 10px;
                                color: black;
                                background-color: gainsboro;
                            }

                            .user_published{
                               background-color: #f4c026; 
                            }

                            .admin_published{
                               background-color: #00bfe7; 
                            }

                            .home-community-card{
                                overflow: visible;
                            }
                        </style>
                        <?php
                            function truncate($string, $length){
                                if (strlen($string) > $length) {
                                    $string = substr($string, 0, $length) . '...';
                                }

                                return $string;
                            }

                            wp_reset_postdata();
                            wp_reset_query();
                            $temp = $wp_query; $wp_query= null;
                            $wp_query = new WP_Query();
                            $wp_query->query( 
                                array(
                                    'post_type' => 'thematic-area',
                                    'post_status' => 'publish',
                                    'author' => $current_user->ID,
                                    'posts_per_page' => -1,
                                ) 
                            );

                            if ( $wp_query->have_posts() ) {

                                while ($wp_query->have_posts()) : $wp_query->the_post();

                                /* Variables */
                                $post_id = $post->ID;    //Get Program ID

                                /* Publication status */      
                                $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                /* Get Opportunity Banner */
                                $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                                $image = reset( $images );
                        ?>
                            <div class="col-md-4 padding-lr-5 padding-b-20 d-flex">
                                <div class="home-community-card position-relative">
                                    <article class="content">
                                        <div class="info bg-white">
                                            <h3 class="txt-medium txt-height-1-4 padding-t-40 margin-b-10">
                                                <a href="<?php the_permalink() ?>">
                                                    <?php the_title() ?>
                                                </a>
                                            </h3>
                                            <p class="txt-sm">
                                                <?php
                                                    $meta = get_post_meta($post_id, 'personal_project_description', true);
                                                    if($meta){
                                                        echo truncate($meta, 70);
                                                    }
                                                ?>
                                            </p>
                                            <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                                <div>
                                                    <a 
                                                       href="<?php printf('%s/?view=form&form=thematic-area&post-id=%s&', currentUrl(true), $post_id ) ?>"
                                                       class="txt-medium txt-color-green"
                                                    >
                                                        Edit
                                                    </a>
                                                    |
                                                    <a 
                                                       href="<?php printf('%s/?view=form&form=thematic-area&action=delete&post-id=%s', currentUrl(true), $post_id ) ?>"
                                                       class="confirm-delete txt-color-red"
                                                    >
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="publication-status <?php echo $publication_meta ?>">
                                                    <?php
                                                        $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                                        if($publication_meta == 'user_published'){
                                                            echo 'Undergoing review';
                                                        } elseif($publication_meta == 'admin_published') {
                                                            echo 'Published';
                                                        }else{
                                                            echo 'Unpublished';
                                                        }
                                                    ?>
                                                    <div class="dropdown padding-l-20">
                                                        <a class="" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="fa-stack">
                                                                <i class="fa fa-circle fa-stack-2x txt-color-white"></i>
                                                                <i class="fa fa-ellipsis-v fa-stack-1x"></i>
                                                            </span>
                                                        </a>

                                                        <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuLink">
                                                           <!-- <a 
                                                               href="<?php printf('%s/?view=form&form-view=collaborators&post-id=%s&form-title=%s Collaborators', currentUrl(true), $post_id, get_the_title() ) ?>" 
                                                               class="dropdown-item"
                                                            >
                                                                Collaborators
                                                            </a>-->
                                                            <a 
                                                               href="<?php printf('%s/?view=manage-problems&post-id=%s&', currentUrl(true), $post_id ) ?>"
                                                               class="dropdown-item"
                                                            >
                                                                Problem Areas
                                                            </a>
                                                            <a 
                                                               href="<?php printf('%s/?view=manage-questions&post-id=%s&', currentUrl(true), $post_id ) ?>"
                                                               class="dropdown-item"
                                                            >
                                                                Questions
                                                            </a>
                                                            <a 
                                                               href="<?php printf('%s/?view=manage-tasks&post-id=%s&', currentUrl(true), $post_id ) ?>"
                                                               class="dropdown-item"
                                                            >
                                                                Tasks
                                                            </a>
                                                            <a 
                                                               href="<?php printf('%s/?view=form&action=publication&post-id=%s', currentUrl(true), $post_id ) ?>"
                                                               class="dropdown-item"
                                                            >
                                                                <?php

                                                                    if($publication_meta == 'user_published'){
                                                                        echo 'Unpublish';
                                                                    } elseif($publication_meta == 'admin_published') {
                                                                        echo 'Unpublish';
                                                                    }else{
                                                                        echo 'Publish';
                                                                    }
                                                                ?>

                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </article>  
                                </div>   
                            </div>
                        <?php
                                endwhile;

                            }else{
                        ?>
                            <div class="col-5 padding-lr-20 padding-t-20 padding-b-40">
                                <h2 class="txt-lg txt-medium">
                                    No Areas found.
                                </h2>
                            </div>   

                        <?php } ?>
                    </div>
                
                <?php } ?> 
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>