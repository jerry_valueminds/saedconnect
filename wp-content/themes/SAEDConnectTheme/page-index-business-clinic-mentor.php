    <?php /*Template Name: Homepage - Business Clinic Mentor*/ ?>     
    
    <?php get_header() ?>
    
    <style>
        .theme-bg{
            background-color: <?php echo $theme_color ?>;
        }
        
        .theme-alt{
            color: <?php echo $theme_alt_color ?>;
        }
        
        .how-it-works-container::before{
            background-color: #3a4002;
        }
    </style>

    <main class="main-content">
        <?php
            while ( have_posts() ) : the_post();
                /* Get Program Information */
                $main_program_id = $post->ID;
                $program_name = get_the_title();
                $program_url = get_the_permalink();

                $how_it_works_brief = rwmb_meta( 'skilli-program-hiw-brief', array(), $main_program_id );
                $how_it_works = rwmb_meta( 'skilli_program_hiw_group', array(), $program_id );
                $what_you_get = rwmb_meta( 'skilli_program_wyg_group', array(), $program_id );
                $program_faq = rwmb_meta( 'skilli-program-faq' );

                /* Include Skilli Navigation */
                get_template_part( 'template-parts/navigation-skilli-secondary' );

            endwhile; // end of the loop.
        ?>

        <!-- Intro -->
        <section class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h2 class="d-inline-block bg-green-army txt-color-white txt-xlg emtxt-bold padding-o-10 margin-b-20">
                        The Business Clinic
                    </h2>
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        Get help with the strategies and skills you need to build and grow your business
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        The Business clinic is an expert led, peer-to-peer forum that facilitates sharing knowledge, experience and ideas to solve business challenges, enable business growth and get feedback on your operational ideas. It provides an avenue to benefit from the practical wisdom of other entrepreneurs.
                    </h2>
                </div>
            </div>
        </section>

        <!-- Tracks -->
        <section class="bg-green-army padding-tb-40">
            <div class="container-wrapper">
                <header class="txt-light margin-b-30 txt-color-white">

                    <h2 class="txt-xxlg txt-medium margin-b-20">
                        Tracks
                    </h2>
                    <p class="txt-height-1-">
                        What ever your business challenge is, we have  experts waiting to help.
                        <br>
                        Join any of the tracks below that matches your business need. 
                    </p>
                </header>
                <?php // Display posts
                    $program_query = new WP_Query( array(
                        'post_type' => 'track',
                        'posts_per_page' => -1
                    ) );
                    while ( $program_query->have_posts() ) : $program_query->the_post();
                
                            $program_id = $post->ID;    //Get Program ID
                            $track_title = rwmb_meta( 'track-short-name' );
                            //  Get Program Featured Image
                            $program_images = rwmb_meta( 'program-feautured-image', array( 'limit' => 1 ) );
                            $program_images = reset( $program_images );
                ?>
                           
                
                <div class="overflow-hidden card-shadow-2 margin-b-40">
                    <div class="row ">
                        <div class="col-md-8 order-2 order-md-1  bg-white">
                            <div class="padding-o-30">
                                <h2 class="txt-normal-s txt-mediym uppercase margin-b-10">
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_title() ?>
                                    </a>
                                </h2>
                                <h2 class="txt-xlg txt-medium txt-height-1-2 padding-b-20">
                                    <?php echo rwmb_meta( 'program-name' ); ?>
                                </h2>
                                <article class="text-box sm">
                                    <?php echo rwmb_meta( 'program-description' ); ?>
                                </article>
                                
                                <!-- Action Buttons for Web -->
                                <div 
                                   class="d-none d-md-block row justify-content-between margin-t-20 padding-t-20"
                                >
                                    <style>
                                        button[aria-expanded="false"]{
                                            background-color: transparent;
                                            border-color: black;
                                            color: black;
                                        }

                                        .btn-trans-bw:hover, button[aria-expanded="true"]{
                                            background-color: grey;
                                            border-color: grey;
                                            color: white;
                                        }
                                    </style>
                                   
                                    <div class="col-md-6">
                                    <?php
                                        /* Get Group Values */
                                        $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                        if ( ! empty( $group_values ) ) {
                                    ?>
                                        <button
                                           class="btn btn-trans-bw txt-sm margin-r-10"
                                           type="button" data-toggle="collapse" 
                                           data-target="#program-<?php echo $program_id ?>" 
                                           aria-expanded="false" 
                                           aria-controls="collapseExample"
                                        >
                                            Learn more
                                        </button>
                                    <?php
                                        }
                                    ?>
                                        <button 
                                            class="btn btn-trans-bw txt-sm" 
                                            type="button" 
                                            data-toggle="collapse" 
                                            data-target="#program-<?php echo $program_id ?>-2" 
                                            aria-expanded="false" 
                                            aria-controls="collapseExample"
                                        >
                                            Connect with Experts
                                        </button>
                                    </div>
                                </div>
                                
                                <!-- Action Buttons Mobile -->
                                <div 
                                   class="d-md-none row justify-content-between margin-t-20 padding-t-20 border-t-1 border-color-darkgrey"
                                >
                                    <style>
                                        button[aria-expanded="false"]{
                                            background-color: transparent;
                                            border-color: black;
                                            color: black;
                                        }

                                        button[aria-expanded="true"]{
                                            background-color: grey;
                                            border-color: grey;
                                            color: white;
                                        }
                                    </style>
                                   
                                    <div class="col-md-6">
                                    <?php
                                        /* Get Group Values */
                                        $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                        if ( ! empty( $group_values ) ) {
                                    ?>

                                       

                                    <?php
                                        } else {
                                    ?>

                                        

                                    <?php

                                        }
                                    ?>
                                        <a class="btn btn-trans-bw txt-sm" data-toggle="modal" href="#mentorModal-<?php echo $program_id ?>">
                                            Connect with Experts
                                        </a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-md-4 order-1 order-md-2  generic-card-bg" style="background-image:url('<?php echo $program_images['full_url']; ?>');">
                            <div class="cta text-center">
                                <!--<div>
                                    <button type="button" class="btn btn-blue" data-toggle="modal" data-target="#formModal">
                                        Enroll for free online
                                    </button>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-white" data-toggle="modal" data-target="#smsEnrollModal-<?php echo $program_id ?>">
                                        Enroll for free via SMS
                                    </button>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    
                    <div class="faq-collapse border-t-1 border-color-darkgrey" id="accordion-<?php echo $program_id ?>">
                        <div
                           class="faq-collapse collapse bg-white" 
                           id="program-<?php echo $program_id ?>" 
                           data-parent="#accordion-<?php echo $program_id ?>"
                        >
                            <div class="padding-lr-80">
                            <?php
                                /* Get Group Values */
                                $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                if ( ! empty( $group_values ) ) {
                            ?>
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Curriculum
                                </h2>
                                    
                            <?php
                                    foreach ( $group_values as $group_value ) {
                            ?>
                               
                                    <!-- Article Group -->
                                    <div class="padding-tb-20 border-t-1 border-color-darkgrey">
                                       
                                    <?php if( $group_value['group-title-url'] ){ ?>
                                        
                                        <h3>
                                            <a
                                               href="<?php echo $group_value['group-title-url']; ?>"
                                               class="txt-color-dark d-flex align-items-center"
                                            >
                                                <span class="txt-xs padding-r-5">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                </span>
                                                <?php echo $group_value['group-title']; ?>
                                            </a>
                                        </h3>
                                        
                                    <?php } else { ?>
                                       
                                        <h3>
                                            <a
                                               href="<?php echo $group_value['group-title-url']; ?>"
                                               class="txt-color-dark d-flex align-items-center"
                                            >
                                                <span class="txt-xs padding-r-5">
                                                    <i class="fa fa-long-arrow-right"></i>
                                                </span>
                                                <?php echo $group_value['group-title']; ?>
                                            </a>
                                        </h3>

                                    <?php } ?>
                                        
                                    <?php 
                                        $values = $group_value['forum-topics-group']['forum-topic-item'];
                                            
                                            if($values){
                                    ?>
                                        <ul class="row row-10 icon-list black txt-normal-s margin-t-20 padding-l-20">
                                        <?php 
                                            foreach ( $values as $value ) { 
                                        ?>
                                           
                                            <li class="col-md-3 padding-lr-10">
                                                <a href="<?php echo $value['forum-topic-url']; ?>">
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        <?php echo $value['forum-topic-title']; ?>
                                                    </span>
                                                </a>
                                            </li>
                                        
                                        <?php } ?>
                                        
                                        </ul>
                                        
                                    <?php } ?>
                                        
                                    </div>

                                <?php
                                    }
                                }
                            ?>
                            </div>
                        </div>

                        <div 
                           class="faq-collapse collapse bg-white" 
                           id="program-<?php echo $program_id ?>-2" 
                           data-parent="#accordion-<?php echo $program_id ?>"
                        >
                            <div class="container-wrapper">
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Engage with Expert Mentors waiting to help you
                                </h2>
                                <p class="txt-normal-s margin-b-40">
                                    Where do you go to get expert advice about solving that challenge or getting that business question answered? The <?php echo $track_title ?> mentor hub is the expert support community you always wished you could reach out to when you needed some motivation, advice or expert experience.
                                </p>

                                <div class="row">
                                    <div class="col-md-7 bg-green-army txt-color-white">
                                        <div class="padding-o-30">
                                            <div class="row row-10 overflow-hidden">
                                                <div class="col-md-6 padding-lr-20 payment-border border-color-darkgrey margin-b-10 padding-b-10">
                                                    <h3 class="txt-xxlg txt-bold margin-b-5">
                                                        Just N10,000 / Qtr
                                                    </h3>
                                                    <p class="txt-sm">
                                                        To join <?php echo $track_title ?>
                                                        <br>
                                                        Mentor Hub
                                                    </p>
                                                </div>
                                                <div class="col-md-6 padding-lr-20">
                                                    <p class="txt-normal-s">
                                                        We charge a small fee to cover our costs, maintain expert team & sustain a high quality moderated Mentor Hub.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="con-list black txt-normal-s margin-t-40">
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Tap into the wisdom & experience of a lot of mentors
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get customised answers to your questions
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get inspired by your peers
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get access to growth support tools, templates & special opportunities.
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Engage with Experts and Mentors until you are satisfied.
                                        </span>
                                    </li>
                                </ul>
                                <div class="margin-t-40 padding-b-20">
                                    <a href="https://www.saedconnect.org/help-center/forums/forum/the-entrepreneurship-incubator-help-center/" class="btn btn-green txt-sm">
                                        Subscribe
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- Curriculum Modal -->
                <div class="modal fade font-main shc-modal" id="curriculumModal-<?php echo $program_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="container-wrapper">
                                <div class="col-md-10 mx-auto">
                                <?php
                                    /* Get Group Values */
                                    $group_values = rwmb_meta( 'connected-forum-topics-group' );


                                    if ( ! empty( $group_values ) ) {
                                ?>
                                    <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                        Curriculum
                                    </h2>

                                <?php
                                        foreach ( $group_values as $group_value ) {
                                ?>

                                        <!-- Article Group -->
                                        <div class="padding-tb-20 border-t-1 border-color-darkgrey">

                                        <?php if( $group_value['group-title-url'] ){ ?>

                                            <h3>
                                                <a
                                                   href="<?php echo $group_value['group-title-url']; ?>"
                                                   class="txt-color-dark d-flex align-items-center"
                                                >
                                                    <span class="txt-xs padding-r-5">
                                                        <i class="fa fa-long-arrow-right"></i>
                                                    </span>
                                                    <?php echo $group_value['group-title']; ?>
                                                </a>
                                            </h3>

                                        <?php } else { ?>

                                            <h3>
                                                <a
                                                   href="<?php echo $group_value['group-title-url']; ?>"
                                                   class="txt-color-dark d-flex align-items-center"
                                                >
                                                    <span class="txt-xs padding-r-5">
                                                        <i class="fa fa-long-arrow-right"></i>
                                                    </span>
                                                    <?php echo $group_value['group-title']; ?>
                                                </a>
                                            </h3>

                                        <?php } ?>

                                        <?php 
                                            $values = $group_value['forum-topics-group']['forum-topic-item'];

                                                if($values){
                                        ?>
                                            <ul class="row row-10 icon-list black txt-normal-s margin-t-20 padding-l-20">
                                            <?php 
                                                foreach ( $values as $value ) { 
                                            ?>

                                                <li class="col-md-3 padding-lr-10">
                                                    <a href="<?php echo $value['forum-topic-url']; ?>">
                                                        <i class="fa fa-chevron-right"></i>
                                                        <span>
                                                            <?php echo $value['forum-topic-title']; ?>
                                                        </span>
                                                    </a>
                                                </li>

                                            <?php } ?>

                                            </ul>

                                        <?php } ?>

                                        </div>

                                    <?php
                                        }
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Mentor Modal -->
                <div class="modal fade font-main shc-modal" id="mentorModal-<?php echo $program_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="container-wrapper padding-t-40">
                                <h2 class="txt-medium padding-b-20 padding-t-40 txt-xxlg">
                                    Engage with Expert Mentors waiting to help you
                                </h2>
                                <p class="txt-normal-s margin-b-40">
                                    Where do you go to get expert advice about solving that challenge or getting that business question answered? The <?php echo $track_title ?> mentor hub is the expert support community you always wished you could reach out to when you needed some motivation, advice or expert experience.
                                </p>

                                <div class="row">
                                    <div class="col-md-7 bg-green-army txt-color-white">
                                        <div class="padding-o-30">
                                            <div class="row row-10 overflow-hidden">
                                                <div class="col-md-6 padding-lr-20 payment-border border-color-darkgrey margin-b-10 padding-b-10">
                                                    <h3 class="txt-xxlg txt-bold margin-b-5">
                                                        Just N10,000 / Qtr
                                                    </h3>
                                                    <p class="txt-sm">
                                                        To join <?php echo $track_title ?>
                                                        <br>
                                                        Mentor Hub
                                                    </p>
                                                </div>
                                                <div class="col-md-6 padding-lr-20">
                                                    <p class="txt-normal-s">
                                                        We charge a small fee to cover our costs, maintain expert team & sustain a high quality moderated Mentor Hub.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="con-list black txt-normal-s margin-t-40">
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Tap into the wisdom & experience of a lot of mentors
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get customised answers to your questions
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get inspired by your peers
                                        </span>
                                    </li>
                                    <li class="padding-b-20">
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Get access to growth support tools, templates & special opportunities.
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-chevron-right"></i>
                                        <span>
                                            Engage with Experts and Mentors until you are satisfied.
                                        </span>
                                    </li>
                                </ul>
                                <div class="margin-t-40 padding-b-20">
                                    <a href="https://www.saedconnect.org/help-center/forums/forum/the-entrepreneurship-incubator-help-center/" class="btn btn-green txt-sm">
                                        Subscribe
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </section>
        
        <!-- How it works -->
        <?php if ( $how_it_works ) { ?>
        <section class="padding-t-80 padding-b-60">
            <div class="container-wrapper">
                <div class="row text-center"> 
                    <div class="col-md-10 mx-auto">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-20 ">
                            How it Works
                        </h1>
                        <p class="txt-normal-s margin-b-60 text-center">
                            <?php echo $how_it_works_brief ?>
                        </p>
                      </div>  
                </div>
            </div>
            <div class="container-wrapper how-it-works-container">
                <div class="row text-center"> 
                    <div class="col-md-10 mx-auto">
                        <div class="row row-20 ">
                        <?php
                            
                            $counter = 1;

                            if ( ! empty( $how_it_works ) ) {
                                foreach ( $how_it_works as $group_value ) {
                        ?>

                            <div class="col-md-4 padding-lr-20">
                                <div class="how-card">
                                    <div class="number">
                                        <span>
                                            <?php
                                                echo $counter;
                                                $counter++;
                                            ?>
                                        </span>
                                    </div>
                                    <div class="content">
                                        <h3 class="txt-medium txt-height-1-2 margin-b-5">
                                            <?php echo $group_value[ 'skilli-program-hiw-title' ]; ?>
                                        </h3>
                                        <p class="txt-sm txt-height-1-7">
                                            <?php echo $group_value[ 'skilli-program-hiw-content' ]; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        <?php
                                }
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        
        <!-- What you get -->
        <?php if ( $what_you_get ) { ?>
        <section class="bg-grey padding-t-80 padding-b-80">
            <div class="container-wrapper row text-center">
                <div class="row"> 
                    <div class="col-md-8 mx-auto">
                        <h1 class="txt-xxlg txt-height-1-1 margin-b-20">
                            What you get
                        </h1>
                        <p class="txt-normal-s margin-b-60">
                            <?php echo rwmb_meta( 'skilli-program-wyg-brief', array(), $main_program_id ); ?>
                        </p>
                        <div class="row row-20">
                        <?php
                            

                            if ( ! empty( $what_you_get ) ) {
                                foreach ( $what_you_get as $group_value ) {
                        ?>
                            <div class="col-md-6 padding-lr-20 d-flex">
                                <div class="bg-white collapsible-card">
                                    <div class="header">
                                        <div class="cta"></div>
                                        <div class="title">
                                            <?php echo $group_value[ 'skilli-program-wyg-title' ]; ?>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="wrapper">
                                            <div class="margin-b-20">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/legacy/user_mentor.png" alt="" width="100">
                                            </div>
                                            <h3 class="txt-medium margin-b-20">
                                                <?php echo $group_value[ 'skilli-program-wyg-title' ]; ?>
                                            </h3>
                                            <p class="txt-sm txt-height-1-7">
                                                <?php echo $group_value[ 'skilli-program-wyg-content' ]; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                                }
                            }
                        ?>
                        </div>
                    </div>  
                </div>
            </div>
        </section>
        <?php } ?>

        <!-- Meet Your mentors -->
        <section class="padding-tb-80">
            <div class="container-wrapper text-center">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <h1 class="txt-xlg txt-height-1-1 margin-b-20">
                            Meet your Mentors
                        </h1> 
                        <p class="txt-normal-s margin-b-60">
                            We are constantly reaching out to the best of the best professionals & experts to help you make progress.
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 mx-auto">                
                        <div class="row row-20">
                        <?php
                             // Create Query Argument
                            $args = array(
                                'post_type' => 'mentor',
                                'showposts' => 6,
                                'meta_query' => $meta_array,
                            );


                            $mentor_query = new WP_Query($args);

                            while ($mentor_query->have_posts()) : $mentor_query->the_post();

                            /* Get Post ID */
                            $post_id = $post->ID;

                            /* Get Mentor mentorship areas */
                            $mentor_field = 'mentor-mentorship-areas';

                            $mentor_areas = get_post_meta($post->ID, $mentor_field, false);

                            /* Check if Menotor mentors on this Track's Program */
                            if ( in_array($program_name, $mentor_areas) ){

                        ?>

                            <!-- Mentor -->
                            <div class="col-md-2 padding-lr-20 padding-b-40 d-flex">
                                <div class="mentor-card">
                                    <div class="image-box">
                                        <?php
                                            $field = 'mentor-profile-image';

                                            $meta = get_post_meta($post->ID, $field, true);

                                        ?>
                                        <img src="<?php if($meta){  echo $meta; } ?>" alt="">
                                    </div>
                                    <div class="name">
                                        <a data-toggle="modal" href="#mentor-<?php echo $post_id ?>">
                                            <?php echo the_title(); ?>
                                        </a>
                                    </div>
                                    <div class="position">
                                        <?php
                                            $field_position = 'mentor-position';
                                            $field_company = 'mentor-company';

                                            $meta_position = get_post_meta($post->ID, $field_position, true);
                                            $meta_company = get_post_meta($post->ID, $field_company, true);

                                            echo $meta_company;
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <!-- Mentor Modal -->
                            <div class="modal fade font-main coming-soon-modal" id="mentor-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content padding-o-40">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <div class="text-center">
                                            <div class="margin-b-10">
                                                <?php
                                                    $field = 'mentor-profile-image';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <img src="<?php if($meta){  echo $meta; } ?>" width="120" class="profile-image">
                                            </div>
                                            <div class="txt-normal-s margin-b-10">
                                                Mentor
                                            </div>
                                            <h4 class="txt-xlg txt-medium margin-b-10">
                                                <?php echo the_title(); ?>
                                            </h4>
                                            <h5 class="txt-medium uppercase txt-sm margin-b-20">
                                                <?php
                                                    $field_position = 'mentor-position';
                                                    $field_company = 'mentor-company';

                                                    $meta_position = get_post_meta($post->ID, $field_position, true);
                                                    $meta_company = get_post_meta($post->ID, $field_company, true);

                                                    echo $meta_position.' @ '.$meta_company;
                                                ?>
                                            </h5>
                                            <p class="txt-sm txt-height-1-7 margin-b-20">
                                                <?php
                                                    $field = 'mentor-short-profile';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                    if($meta){
                                                        echo $meta;
                                                    }
                                                ?>
                                            </p>
                                            <div class="txt-xlg">
                                                <?php
                                                    $field = 'mentor-facebook';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-facebook fa-fw"></i>
                                                </a>

                                                <?php
                                                    $field = 'mentor-twitter';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a 
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-twitter fa-fw"></i>
                                                </a>

                                                <?php
                                                    $field = 'mentor-linkedin';

                                                    $meta = get_post_meta($post->ID, $field, true);

                                                ?>
                                                <a 
                                                   href="<?php if($meta){  echo $meta; } ?>" 
                                                   class="margin-lr-10 d-inline-block padding-o-10 border-o-1 border-color-darkgrey"
                                                   target="_blank"
                                                >
                                                    <i class="fa fa-linkedin fa-fw"></i>
                                                </a>
                                            </div>
                                            <div class="padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Mentors on
                                                </div>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field = 'mentor-mentorship-areas';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="border-t-1 border-color-darkgrey padding-t-20 margin-t-20">
                                                <div class="txt-sm txt-medium margin-b-10">
                                                    Small Business Mentorship area
                                                </div>
                                                <p class="txt-sm">
                                                    <?php
                                                        $field = 'mentor-small-business-mentorship-interest';

                                                        $meta = get_post_meta($post->ID, $field, false);

                                                        if($meta){
                                                            foreach($meta as $key=>$value) {
                                                                echo $value;
                                                                if($key < count($meta) ){
                                                                    echo ', ';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                            }   // Check if Menotor mentors on this Track's Program: End

                            endwhile;
                        ?>  
                        </div>
                        <h2 class="txt-normal-s txt-height-1-7">
                            Have questions? Check out the
                                Help Center
                            .
                        </h2>
                        <div class="margin-t-20">
                            <a href="https://www.saedconnect.org/growth-programs/mentor-directory/" class="btn btn-green txt-sm no-m-b">
                                See Full List
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- FAQ -->
        <section class="container-wrapper bg-grey padding-tb-60">
            <header class="margin-b-60 text-center">
                <h2 class="txt-xxlg txt-light padding-b-20">Frequently Asked Questions</h2>
                <p>
                    Got questions? Require clarifications? Find your answers here
                </p> 
            </header>
            <div class="row row-10">
            <?php 
                $accordionCounter = 1;

                foreach ( $program_faq as $value ) { 
            ?>
                <div class="col-md-4 padding-lr-10 padding-b-20">
                    <div class="faq-collapse padding">
                        <button class="faq-btn" type="button" data-toggle="collapse" data-target="#faq-<?php echo $accordionCounter ?>" aria-expanded="false" aria-controls="collapseExample">
                            <?php echo $value['accordion-title']; ?>
                        </button>
                        <div class="collapse" id="faq-<?php echo $accordionCounter ?>">
                            <div class="card card-body">
                                <article class="">
                                    <p class="txt-sm">
                                        <?php echo $value['accordion-content']; ?>
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                    $accordionCounter++;
                }
            ?> 
            </div>
            <p class="txt-xlg txt-medium text-center margin-t-40 margin-b-20">
                Didn't find your answer?
            </p>
            <div class="text-center">
                <a href="https://www.saedconnect.org/help-center/forums/forum/business-clinic-help-center/" class="btn btn-green txt-normal-s">
                    Business Clinic Help Center
                </a>
            </div>
        </section>
        
        <?php
            /* Include Skilli Footer */
            get_template_part( 'template-parts/footer-skilli' );
        ?>
    </main>


    <?php get_footer() ?>