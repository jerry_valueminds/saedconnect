/*// When the user scrolls the page, execute myFunction 
window.onscroll = function() {myFunction()};

// Get the header
var header = document.getElementById("myHeader");
// Get the Side Nav
var nav = document.getElementById("side-nav");


// Get the offset position of the navbar
if (header){
    var sticky = header.offsetTop;
}
// Get the offset position of the Side Nav

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
    if (window.pageYOffset >= 20) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
    
    if (window.pageYOffset >= 50) {
        nav.classList.add("sticky");
    } else {
        nav.classList.remove("sticky");
    }
}*/



$(document).ready(function() {
    
    
    //Main Menu
    $(".menu-btn.old").click(function(){
        $(this).toggleClass("is-active");
        $(".navigation-list-wrapper").slideToggle();
        $("body").toggleClass("noScroll");
    });
    
    $(".menu-btn.new, .mobile-navigation .menu-btn").click(function(){
        //$(this).toggleClass("is-active");
        $(".mobile-navigation").fadeToggle();
        $("body").toggleClass("noScroll");
    });
    
    $(".main-navigation .navigation-list > li > span > i.fa-plus, .main-navigation .navigation-list > li > a > i.fa-plus").click(function(e){
        
        //Prevent href event
        e.preventDefault();
        
        //Get Parent
        var parent_link = $(this).closest('a, span');
        
        //Responsive
        if($(window).width() <= 768){
            //e.preventDefault();
            parent_link.next().slideToggle();
        }
        
        var icon = $(this);
        
        if (icon.hasClass( "fa-plus" ) ) {
            icon.removeClass("fa-plus");
            icon.addClass("fa-minus");
        }else{
            icon.removeClass("fa-minus");
            icon.addClass("fa-plus");   
        }
    });
    
    //Responsive
    if($(window).width() > 992){
       
    }
    
    if( $(window).width() < 992){
        $('.hideInMobile').collapse('hide');
    }
    
    $( ".main-navigation .navigation-list > li.hasSubMenu").hover(
        function() {
            $("body").addClass("dim");
        }, function() {
            $("body").removeClass("dim");
        }
    );
    
    
    $(".main-navigation .search .search-btn").click(function(){
        $(".search-form").fadeIn();
        $(".search-box").focus();
        $("body").addClass("dim");
    });
    
    $(".main-navigation .search .close-btn").click(function(){
        $(".search-box").focusout();
        $(".search-form").slideUp();
        $("body").removeClass("dim");
    });
    
    $(".header-nav-title").click(function(){
        $(".header-nav").toggleClass("open");
        $("body").toggleClass("dim");
    });
        
    
    //Image Upload
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
    
    //Custom Tabs
    $(".custom-tabs .trigger").on('click', function() {
        var id = $(this).attr('id');
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        $(".custom-tabs-content #content-"+id).removeClass("d-none");
        $(".custom-tabs-content #content-"+id).siblings().addClass("d-none");
        
        console.log(id)
    });
    
    
    //Form
    $(".form-item input, .form-item textarea").focusin(function(){
        $(this).parent(".form-item").addClass("active");
    });
    
    $(".form-item input, .form-item textarea").focusout(function(){
        $(this).parent(".form-item").removeClass("active");
    });
    
    
    /*
    *
    *   Swiper BEGIN
    *
    */
    
    //Home Sliders
    var swiper = new Swiper('.swiper-container-home-services', {
        spaceBetween: 30,
        effect: 'fade',
        speed: 2000,
        centeredSlides: true,
        autoplay: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    //Generic Gallery
    var swiper = new Swiper('.generic-gallery, .generic-gallery-text', {
        spaceBetween: 0,
        effect: 'slide',
        speed: 400,
        loop: true,
        centeredSlides: true,
        autoplay: false,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
    
    //Generic Gallery Two Slides
    var swiper = new Swiper('.generic-gallery-two-slides', {
        spaceBetween: 0,
        effect: 'slide',
        speed: 400,
        loop: true,
        centeredSlides: false,
        autoplay: true,
        autoplay: {
            delay: 10000,
            disableOnInteraction: false,
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            3000: {
                slidesPerView: 2,
            },
            2000: {
                slidesPerView: 2,
            },
            1600: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 2,
            },
            640: {
                slidesPerView: 1,
            },
            320: {
                slidesPerView: 1,
            }
        }
    });
    

    
    //Album Gallery
    var swiper = new Swiper('.album-gallery', {
        spaceBetween: 0,
        effect: 'slide',
        speed: 400,
        loop: true,
        centeredSlides: true,
        autoplay: false,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
    });
    
    //Home Gallery
    var swiper = new Swiper('.home-gallery', {
        spaceBetween: 0,
        effect: 'fade',
        speed: 400,
        loop: true,
        autoplay: {
            delay: 10000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination-home',
            clickable: true,
        },
    });
    
    var swiper = new Swiper('.home-gallery-2', {
        spaceBetween: 0,
        effect: 'fade',
        speed: 400,
        loop: true,
        autoplay: {
            delay: 10000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination-home',
            clickable: true,
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
    });
    
    // Dashboard Updates
    var swiper = new Swiper('.dashboard-updates-slider', {
        spaceBetween: 0,
        effect: 'slide',
        speed: 400,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
    });
    
    var swiper = new Swiper('.featured-opportunities', {
        //slidesPerView: 4,
        spaceBetween: 30,
        centeredSlides: false,
        freeMode: true,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        breakpoints: {
        3000: {
            slidesPerView: 4,
        },
        2000: {
            slidesPerView: 4,
        },
        1600: {
            slidesPerView: 4,
        },
        1024: {
            slidesPerView: 3,
        },
        768: {
            slidesPerView: 2,
        },
        640: {
            slidesPerView: 1,
        },
        320: {
            slidesPerView: 1,
        }
    }
    });
    
    //Project Gallery
    var swiper = new Swiper('.swiper-container-project-gallery', {
        spaceBetween: 0,
        loop: true,
        speed: 10000,
        autoplay: {
            delay: 4000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            3000: {
                slidesPerView: 8,
            },
            2000: {
                slidesPerView: 6,
            },
            1600: {
                slidesPerView: 4,
            },
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 2,
            },
            320: {
                slidesPerView: 1,
            }
        }
    });
    
    
    //Program Gallery
    var swiper = new Swiper('.swiper-container', {
        
        slidesPerView: 3,
        
        spaceBetween: 0,
        
        freeMode: true,
        
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 0,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 0,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 0,
            }
        }
        
    });
    
    /*
    *
    *   Swiper END
    *
    */
    
    //Video Player
    $(".popup-video").YouTubePopUp();
    
    /*Image Pop up*/
    /*$('.popup-image').magnificPopup({
      type: 'image'
      // other options
    });*/
    
    
    /*  
    *
    *    Flip Card BEGIN
    *
    */
    $(".flip-card .flip-trigger").click(function(){
        
        $(this).closest('.flip-card').siblings().removeClass("flipped");
        $(this).closest('.flip-card').toggleClass("flipped");
    });
    
    
    /*  
    *
    *    Flip Card END
    *
    */
    
    
    /*  
    *
    *    collapsible-card BEGIN
    *
    */
    $(".collapsible-card .header").click(function(){
        $(this).closest('.collapsible-card').toggleClass('open');
    });
    
    /*  
    *
    *    CV Menu List BEGIN
    *
    */
    $(".cv-menu-toggle").click(function(){
        $(".nav-wrapper").slideToggle();
    });
    
    
    /*  
    *
    *    Boostrap 4 Dropdown to Select BEGIN
    *
    */
    $(".dropdown-to-select .dropdown-item").click(function(e){
        e.preventDefault();     //Prevent selection links from loading a new page

        var selection = $(this).text();     //Get selctection made
        var dropdown = $(this).closest('.dropdown');    //Get Parent dropdown

        dropdown.find('.dropdown-input').val(selection);    //Update hidden input field
        dropdown.find('button').text(selection);    //Update dropdown display button text

    })
    
    
    
});