<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    <meta name="description" content="">
    <meta name="author" content="saedconnect.com">
	<title>SAEDConnect</title>
	
	<!--Site icon-->
	<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" type="image/x-icon">
	
    <!--Load Styles-->
	<?php wp_head(); ?>
	
	<!--FontAwesome CDN-->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
</head>
<body>

	
	<header class="main-navigation font-main" id="myHeader">
        <div class="navigation-wrapper">
            <a class="brand" href="http://www.saedconnect.org/">
                <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                <span class="name">
                    SAEDConnect
                </span>
            </a>
            <div class="action-btn-wrapper">
                
                <div class="search">
                    <div class="search-btn">
                        <i class="fa fa-search"></i>
                    </div>
                    <form action="" class="search-form">
                        <div class="wrapper">
                            <input class="search-box" type="search" placeholder="Type to Search">
                            <div class="close-btn">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-times fa-stack-1x"></i>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <!--<div class="user-account">
                </div>-->
                <button class="menu-btn hamburger hamburger--spring d-block d-sm-none">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    <?php 
        //global $wp;
        if(home_url( $wp->request ) != 'http://www.saedconnect.org/partner' ){ 
    ?>
        <!-- For Corp Members -->
        <nav class="navigation-list-wrapper left">
            <ul class="navigation-list">
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'find-your-path')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(573); ?>">
                        Find Your Path
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Discover Yourself
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Use our career guidance online tool to gain clarity about your strengths & weaknesses, and discover viable careers that match your talents.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" data-toggle="modal" href="#comingSoonModal">Start Here</a>  
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Get Counseling
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Let our counselors work with you to uncover your strengths, interpret your passions and provide objective and professional advice about optimal career paths to choose.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/about/reference/purpose-discovery-services/">Start Here</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Self Discovery Guide
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        Dive into concepts & explanations that help you appreciate your uniqueness and guide you in making informed career decisions.
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/self-discovery-guide/">Start Here</a>  
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'get-a-job')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(575); ?>">
                        Get a Job
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Are you ready?
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        You need to develop the right mindset to get a great job and keep it. Explore the key attitudes & principles that guarentee career success. 
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/prepare-yourself/">Start Here</a>  
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/career-guide">
                                            Job Search & Career Development Guide
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 3)
                                        switch_to_blog(3);
                                                                                                        
                                        //WP Query to get Guides
                                        
                                        $temp = $wp_query; $wp_query= null;
                                        $wp_query = new WP_Query();
                                        $wp_query->query(array('post_type' => 'guide-section'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php echo the_permalink() ?>">
                                                <?php echo the_title() ?>
                                            </a>
                                        </li>
                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                        endwhile;
                                        wp_reset_postdata();
                                        wp_reset_query();
                                        $wp_query = $temp;
                                                                                                        
                                    ?>
                                    </ul>
                                </div>
                                
                                <div class="col-md-6 row row-20 padding-lr-20">
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/tutor_link.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/esaed/mini-site/tutorlink/">Tutor Link</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/talent_source.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://www.saedconnect.org/esaed/mini-site/talentsource/">TalentSource</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/job.png" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://www.saedconnect.org/esaed/mini-site/job-sites-syndication">Job Site Syndicate</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/i_credential.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://www.saedconnect.org/esaed/mini-site/icredentials/">iCredentials</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/cv_creator.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://www.saedconnect.org/esaed/mini-site/cv-creator">CV & Cover Letter Services</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/tech_mentorship.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://skilli.ng/program/tech-career-mentorship/?saedconnect=true">Tech Mentorship Scheme</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(577); ?>">
                        Do Business
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="txt-xxlg txt-bold margin-b-20">
                                        Are you ready?
                                    </h4>
                                    <p class="txt-normal-s txt-height-1-5 margin-b-20">
                                        You need to develop the right mindset to start & build a great business. Explore the key attitudes & principles to guarentee success in entreprenuership. 
                                    </p>
                                    <a class="btn btn-trans btn-trans-blue" href="http://www.saedconnect.org/prepare-yourself/">Start Here</a>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/entrepreneurship-guide/">
                                            Doing Business Guide
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 2)
                                        switch_to_blog(2);
                                                                                                        
                                        //WP Query to get Guides
                                        $temp = $wp_query; $wp_query= null;
                                        $wp_query = new WP_Query();
                                        $wp_query->query(array('post_type' => 'guide-section'));
                                        while ($wp_query->have_posts()) : $wp_query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php echo the_permalink() ?>">
                                                <?php echo the_title() ?>
                                            </a>
                                        </li>
                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                        endwhile;
                                        wp_reset_postdata();
                                        wp_reset_query();
                                        $wp_query = $temp;
                                    ?>
                                    </ul>
                                </div>
                                <div class="col-md-6 row row-20 padding-lr-20">
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/opportunity_centre.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://www.saedconnect.org/esaed/mini-site/business-communities/">
                                                        Business Opportunuties
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/opportunity.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://www.saedconnect.org/esaed/mini-site/opportunity-center">Access to Market Opportunuties</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/side_hustle.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://skilli.ng/program/side-hustle-skills/">Side Hustle Skills</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/business_community.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://www.saedconnect.org/esaed/mini-site/business-communities/">
                                                        Business Communities
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/influencer_network.png" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="http://www.saedconnect.org/esaed/mini-site/influencer-network/">
                                                        Influencer Network
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/esaed/business_plan_creator.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://www.saedconnect.org/esaed/mini-site/interactive-business-plan-creator/">
                                                        Business Plan Creator
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-lr-20">
                                        <div class="row row-5 sub-menu-image-box padding-b-10">
                                            <div class="col-md-5 padding-lr-5">
                                                <figure class="image-item">
                                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/entreprenuership_incubator.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="col-md-7 padding-lr-5">
                                                <h4 class="sub-menu-title">
                                                    <a href="https://skilli.ng/program/entrepreneurship-incubator/?saedconnect=true">
                                                        The Entrepreneurship Incubator
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'learn-a-skill')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/learn-a-skill">
                        Skills Splash
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20 padding-b-10">
                                    <h4 class="txt-xlg txt-bold txt-height-1-4">
                                        Find trainers near you or check out available courses across a wide variety of subject areas.
                                    </h4>
                                </div>
                                <div class="col-md-6 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        Programs
                                    </h4>
                                    <div class="row row-10">
                                        <div class="col-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/entreprenuership_incubator.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://skilli.ng/program/side-hustle-skills/?saedconnect=true">
                                                            The Entrepreneurship Incubator Program
                                                        </a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/side_hustle.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="http://www.saedconnect.org/about/blog/mini-site/growth-support-services/">
                                                            Side Hustle Skills
                                                        </a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 padding-lr-10">
                                            <div class="row row-5 sub-menu-image-box padding-b-10">
                                                <div class="col-md-5 padding-lr-5">
                                                    <figure class="image-item">
                                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skills_splash/tech_mentorship.jpg" alt="">
                                                    </figure>
                                                </div>
                                                <div class="col-md-7 padding-lr-5">
                                                    <h4 class="sub-menu-title">
                                                        <a href="https://skilli.ng/program/tech-career-mentorship/?saedconnect=true">
                                                            Technology Mentorship Program
                                                        </a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="navigation-list">
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="http://www.saedconnect.org/programs">
                        Opportunity Center
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/contribute/">
                        Partnerships
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="sub-menu">
                        <div class="sub-menu-content">
                            <div class="row row-20">
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/trainimgs-overview/">
                                            Offer Trainings & Youth Services
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">               
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 8)
                                        switch_to_blog(11);

                                        $connected = new WP_Query( array(
                                            'relationship' => array(
                                                'id'   => 'mini_site_to_two_c_m_page',
                                                'from' => 608, // You can pass object ID or full object
                                            ),
                                            'nopaging' => true,
                                        ) );
                                        while ( $connected->have_posts() ) : $connected->the_post(); ?>
                                            <li>
                                               <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                            </li>
                                    <?php
                                        endwhile;
                                        wp_reset_postdata();
                                                                                                        
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header padding-lr-20">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/support-youth-development-overview/">
                                            Support Youth Development
                                        </a>
                                    </h4>                                    
                                    <div class="row row-5 sub-menu-image-box padding-lr-20 padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/for_corporate.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="http://www.saedconnect.org/contribute/synopsis/corporate/">For Corporate</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row row-5 sub-menu-image-box padding-lr-20 padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/for_schools_and_youth_institutions.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="http://www.saedconnect.org/contribute/synopsis/schools-youth-development/">For Schools & Youth Institutions</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row row-5 sub-menu-image-box padding-lr-20 padding-b-10">
                                        <div class="col-md-5 padding-lr-5">
                                            <figure class="image-item">
                                                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/government_and_donationg.png" alt="">
                                            </figure>
                                        </div>
                                        <div class="col-md-7 padding-lr-5">
                                            <h4 class="sub-menu-title">
                                                <a href="http://www.saedconnect.org/contribute/synopsis/government-donor-agencies/">For Government & Donor Agencies</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/ways-to-support-overview/">
                                            Ways to Support
                                        </a>
                                    </h4>
                                    <!--<ul class="sub-menu-list">
                                        <li>
                                            <a href="about.html">
                                                Lend your Expertise
                                            </a>
                                        </li>
                                        <li>
                                            <a href="mission.html">
                                                Share your Story
                                            </a>
                                        </li>
                                        <li>
                                            <a href="mission.html">
                                                Support a Project
                                            </a>
                                        </li>
                                        <li>
                                            <a href="vision.html">
                                                Support with Sensitization
                                            </a>
                                        </li>
                                        <li>
                                            <a href="vision.html">
                                                Volunteer
                                            </a>
                                        </li>
                                        <li>
                                            <a href="vision.html">
                                                Execute or Co-create a Youth Development Program
                                            </a>
                                        </li>
                                    </ul>-->
                                    <ul class="sub-menu-list">               
                                    <?php
                                        //Switch to Learn a Skill Multisite (id = 8)
                                        switch_to_blog(11);

                                        $connected = new WP_Query( array(
                                            'relationship' => array(
                                                'id'   => 'mini_site_to_two_c_m_page',
                                                'from' => 625, // You can pass object ID or full object
                                            ),
                                            'nopaging' => true,
                                        ) );
                                        while ( $connected->have_posts() ) : $connected->the_post(); ?>
                                            <li>
                                               <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                            </li>
                                    <?php
                                        endwhile;
                                        wp_reset_postdata();
                                                                                                        
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>
                                    </ul>
                                </div>
                                <div class="col-md-3 padding-lr-20">
                                    <h4 class="sub-menu-header">
                                        <a href="http://www.saedconnect.org/contribute/synopsis/donate-overview/">
                                            Donate
                                        </a>
                                    </h4>
                                    <ul class="sub-menu-list">
                                        <li>
                                            <a href="http://www.saedconnect.org/contribute/synopsis/donate-ways-to-give/">
                                                Ways to Give
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.saedconnect.org/contribute/synopsis/donate-online/">
                                                Donate Online
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/esaed">
                        eSAED
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
            </ul>
        </nav>
    <?php }else{ ?>
        <!-- For Corp Partners -->
        <nav class="navigation-list-wrapper left">
            <ul class="navigation-list">
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'find-your-path')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(573); ?>">
                        Support Youth Development
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'get-a-job')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(575); ?>">
                        Share Your Programs
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'do-business')
                                echo 'active';
                            }
                        ?>"
                        href="<?php echo get_page_link(577); ?>">
                        Talent Source
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
            </ul>
            <ul class="navigation-list">
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'nysc-saed')
                                echo 'active';
                            }
                        ?>" href="http://www.saedconnect.org/programs">
                        Partner
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'contribute')
                                echo 'active';
                            }
                        ?>"
                        href="http://www.saedconnect.org/accelerate-blog/">
                        NYSC SAED
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a
                        class="blue <?php
                            if(get_post_type( get_the_ID() ) == 'page'){
                                if( rwmb_get_value( 'select-active-main-menu' ) == 'about')
                                echo 'active';
                            }
                        ?>"
                        data-toggle="modal" href="#comingSoonModal">
                        Youth Portal
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
            </ul>
        </nav>
    <?php } ?>
	</header>
	
