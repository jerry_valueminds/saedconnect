<?php /*Template Name: Homepage - Get your Job*/ ?>    

<?php get_header('home-page') ?>

<?php get_header() ?>
   
    <main class="main-content">
        <div class="container-wrapper padding-t-40"> 
            <div class="color-hero bg-purple txt-color-white">
                <div class="content">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="txt-4em txt-bold margin-b-20">
                                Get your Job
                            </h1>
                            <h2 class="txt-height-1-7 txt-lg">
                                Build, showcase & verify your competency - and get connected to a large pool of employers looking to hire you.
                            </h2>
                        </div>
                    </div>
                </div>
            </div>

            <!-- General Section -->
            <section>
                <div class="row">
                    <div class="col-md-6 d-flex">
                        <article class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (21).jpg');">
                            <div class="content txt-color-white">
                                <h3 class="txt-xxlg txt-medium margin-b-10">
                                    The JobTrac
                                </h3>
                                <p class="txt-sm margin-b-30">
                                    JobTrac helps you build, compile and verify your competency in specific job roles and then connects you to employers looking to hire verified, job-ready candidates in those roles.
                                </p>
                                <a class="btn btn-trans-wb" href="https://www.saedconnect.org/jobtrac/">
                                    Explore
                                </a>
                            </div>   
                        </article>
                    </div>
                    <div class="col-md-6 d-flex">
                        <article class="feature-image-block sm" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (12).jpg');">
                            <div class="content txt-color-white">
                                <h3 class="txt-xxlg txt-medium margin-b-10">
                                    JobAdvisor
                                </h3>
                                <p class="txt-sm margin-b-30">
                                    Get seasoned mentors to show you how to craft a great CV or cover letter, search for the best jobs and get interviews, ace job interviews and more
                                </p>
                                <a class="btn btn-trans-wb" href="https://www.saedconnect.org/growth-programs/program/career-forum/">
                                    Connect
                                </a>
                            </div>   
                        </article>
                    </div>
                </div>
            </section>
        </div>
        <div class="container-wrapper">
            <div class="padding-tb-40">
                <h4 class="txt-xxlg txt-bold txt-height-1-2 margin-b-40">
                    Hire a Career Expert
                </h4>
                <div class="support-list">
                    <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-purple" href="https://www.saedconnect.org/about/synopsis/find-your-path/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        Career Coaching Services
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Take advantage of our career coaching services to make critical decisions about career direction, choices and advancement.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-purple" href="http://www.saedconnect.org/learn-a-skill/do-business/">
                                    <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-purple" href="https://www.saedconnect.org/about/discourse/cv-cover-letter-prep/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        CV & Cover Letter Creation & Review
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Get practical tips and live interview practice to increase your chances for success in the real interview.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-purple" href="http://www.saedconnect.org/about/discourse/business-plan-creation/">
                                    <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-purple" href="https://www.saedconnect.org/about/discourse/linkedin-page-creation-review-networking-coaching/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        LinkedIn Page Creation / Review & Networking Coaching
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Get practical tips and live interview practice to increase your chances for success in the real interview.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-purple" href="http://www.saedconnect.org/about/discourse/raise-finance-for-your-business/">
                                    <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="padding-b-10 margin-b-20 border-b-1 border-color-darkgrey">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-purple" href="https://www.saedconnect.org/about/discourse/job-search-coaching/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        Job Search Coaching
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Get practical tips and live interview practice to increase your chances for success in the real interview.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-purple" href="http://www.saedconnect.org/about/discourse/job-search-coaching/">
                                    <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="txt-bold txt-height-1-2 margin-b-10">
                                    <a class="txt-color-purple" href="https://www.saedconnect.org/about/discourse/interview-prep/">
                                        <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                        Interview Coaching
                                    </a>
                                </h4>
                            </div>
                            <div class="col-md-4"> 
                                <article class="text-box sm">
                                    <p>
                                        Get live interview practice & feedback to increase your chances for success in the real interview.
                                    </p>
                                </article>
                            </div>
                            <div class="col-md-3 text-right txt-xlg">
                                <a class="txt-color-purple" href="http://www.saedconnect.org/about/discourse/business-branding/">
                                    <i class="fa fa-long-arrow-right padding-r-5 d-none d-md-inline"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-wrapper text-center padding-b-20">
            <div class="bg-grey padding-tb-40 padding-lr-20">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <h3 class="txt-medium txt-xlg margin-b-15">
                            Interested in starting a business instead?
                        </h3>
                        <p class="txt-normal-s">
                            Make money on your own terms. Get help to overcome your fears, start any business of your choice, and grow your small business into an empire.
                        </p>
                        <div class="margin-t-30">
                            <a class="btn btn-blue no-m-b" href="https://www.saedconnect.org/start-your-business/">
                                Do Business
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
<?php get_footer() ?>