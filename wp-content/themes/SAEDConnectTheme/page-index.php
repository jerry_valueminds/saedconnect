    <?php /*Template Name: Homepage-general*/ ?>
    
    <?php get_header() ?>
    
    <main class="main-content">       
        <div class="home-gallery">
            <section class="swiper-wrapper" href="gallery">
                <div class="swiper-slide bg-yellow txt-color-white">
                    <div class="container-wrapper padding-tb-40 content">
                        <h1 class="title">
                            <span class="main">
                                Make Progress
                            </span>
                            <span class="sub txt-color-dark">
                                Towards Your Dreams    
                            </span>
                        </h1>
                    </div>
                </div>
            </section>
            <!-- Add Pagination -->
            <div class="swiper-pagination-home"></div>
        </div>
        
        <!-- General Section -->
        <section>
            <div class="row">
                <div class="col-md-4">
                    <a class="feature-video-block popup-video" href="https://www.youtube.com/watch?v=754f1w90gQU" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (31).png');">
                        <span class="caption">
                            <h3 class="title txt-color-white">
                                Fight for it
                            </h3>
                        </span>
                        <div class="play-btn">
                            <i class="fa fa-play"></i>
                        </div>      
                    </a>
                </div>
                <article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (20).jpg');">
                    <div class="content txt-color-white">
                        <h4 class="margin-b-30">Featured Article</h4>
                        <h3 class="txt-xxlg txt-medium margin-b-30">
                            What Do You Want to Do With Your Life?
                        </h3>
                        <a class="btn btn-trans-wb" href="https://www.scotthyoung.com/blog/2007/07/29/what-do-you-want-to-do-with-your-life/">
                            Read more
                        </a>
                    </div>   
                </article>
                <div class="col-md-4 cta-block bg-grey txt-color-dark">
                    <article>
                        <article class="content">
                            <h3 class="txt-xxlg txt-height-1-4 txt-medium margin-b-30">
                                Find information, support and opportunities you need to make progress towards your dream job or new business.
                            </h3>
                        </article>
                    </article>
                </div>
                
                <div class="col-md-4 bg-black padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Find Your Path
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                Let us share your programs, trainings & initiatives with our over 1 million - strong youth audience.
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a data-toggle="modal" href="http://www.saedconnect.org/prepare-yourself/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Discover Yourself
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Get Counseling
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/self-discovery-guide/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Self Discovery Guide
                                    </span>
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
                <div class="col-md-4 bg-black padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Get a Job
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                Getting a Job & excelling in it is not rocket science. Find all the information, tips and tricks that will help you navigate the maze to get and grow in your dream career.
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a href="http://www.saedconnect.org/career-guide">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Job Search & Career Development Guide
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/about/discourse/career-coaching/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Career Coaching Services
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/about/discourse/cv-cover-letter-prep/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        CV & Cover Letter Services
                                    </span>
                                </a>
                            </li>
                            
                            <li>
                                <a href="http://www.saedconnect.org/about/discourse/interview-prep/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Interview Prep Services
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/professional-association/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Register for a Professional Exam
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/about/discourse/school-overseas/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        School Overseas
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/about/discourse/work-overseas/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Work Overseas
                                    </span>
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
                <div class="col-md-4 bg-black padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Do Business
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                Get access to information, resources, support, programs and people that will help you in your quest to start and grow your business.
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a data-toggle="modal" href="http://www.saedconnect.org/entrepreneurship-guide/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Doing Business Guide
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Xplore Business Idea Directory
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Join the Venture Oven Virtual Incubator
                                    </span>
                                </a>
                            </li>
                            
                            <li>
                                <a data-toggle="modal" href="#comingSoonModal">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Get Business Counseling
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/about/discourse/business-plan-creation/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Create a Business Plan
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/about/discourse/business-branding/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Get Complete Business Branding
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.saedconnect.org/about/discourse/business-registrations/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Business Registration & Other Legal Services
                                    </span>
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
                
                <article class="col-md-8 feature-image-block"
                    style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (23).jpg');">
                    <div class="content txt-color-white">
                        <h4 class="margin-b-30">xPlore</h4>
                        <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                            Find the information you need to start and excel in specific careers.
                        </h3>
                        <p class="txt-height-1-5 margin-b-30">
                            xPlore offers information to better understand how to start and excel in specific careers, so you can focus on what matters most of all: Preparation. Through rich content, connections with mentors and other practitioners, xPlore aims to provide resources, useful advice, and personal connections to help you thrive.
                        </p>
                        <a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
                            Explore 
                        </a>
                    </div>   
                </article>
                <div class="col-md-4 cta-block bg-black txt-color-white">
                    <article>
                        <div class="txt-3em txt-color-white padding-tb-20 border-t-4 border-color-white">
                            <i class="fa fa-quote-left"></i>
                        </div>
                        <h2 class="title">
                            Learn everything you can, anytime you can, from anyone you can – There will always come a time when you will be grateful you did

                        </h2>
                        <div class="txt-medium txt-height-1-2">
                            Sarah Caldwell
                        </div>
                    </article>
                </div>
                <div class="col-md-12 cta-block">
                    <h4 class="txt-xlg margin-b-30">Get in Touch</h4>
                    <div class="row row-20">
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 pre-footer" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (22).jpg');">
                    <div class="content">
                        <h4 class="title">
                            Empower People, Transform Lives
                        </h4>
                        <p class="txt-height-1-4 txt-color-white margin-b-40">
                            The world needs more problem solvers. Your support will create possibilities and amplify impact.
                        </p>
                        <article class="btn-wrapper">
                            <a class="btn btn-trans-wb icon" data-toggle="modal" href="#comingSoonModal">
                                Contribute
                            </a>
                            <a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
                                Support Youth Development
                            </a>
                            <a class="btn btn-white" data-toggle="modal" href="#comingSoonModal">
                                Donate Online
                            </a>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>