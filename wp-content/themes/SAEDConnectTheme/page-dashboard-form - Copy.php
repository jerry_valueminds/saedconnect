<?php /*Template Name: Dashboard-Form*/ ?>

<?php get_header('user-dashboard') ?>

<?php while ( have_posts() ) : the_post(); ?>
    <main class="font-main">
        <div class="user-dashboard">
            <nav class="dashboard-nav">
                <div class="nav-header">
                    <a class="brand" href="index.html">
                        <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                        <span class="name">SAEDConnect</span>
                    </a>
                </div>
                <ul class="nav-list">
                    <?php
                        /* Get Base URL */
                        $base_url = get_site_url().'/my-dashboard';
                    ?>
                    <li>
                        <a
                            href="<?php echo $base_url ?>/?action=esaed"
                            class="
                                <?php 
                                    if( $page_type === 'esaed' || $page_type === 'esaed-form' || $page_type === '' ){
                                        echo 'active';
                                    }   
                                ?>
                            ">
                            eSAED
                        </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo $base_url ?>/?action=tools"
                            class="
                                <?php 
                                    if($page_type === 'tools'){
                                        echo 'active';
                                    }   
                                ?>
                            ">
                            Tools
                        </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo $base_url ?>/?action=contributor-profile"
                            class="
                                <?php 
                                    if($page_type === 'contributor-profile'){
                                        echo 'active';
                                    }   
                                ?>
                            ">
                            Contibutor Profile
                        </a>
                    </li>
                </ul>
            </nav>
            <section class="dashboard-body">
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a class="active" href="">Sub-nav</a>
                        </li>
                        <li>
                            <a href="">Sub-nav-2</a>
                        </li>
                    </ul>
                </nav>
                <div class="col-md-12 mx-auto padding-tb-20 padding-lr-30 esaed-form">
                    <?php
                        /* Select Page View */
                        the_content()
                    ?>
                </div>
            </section>
        </div>
    </main>
    
<?php endwhile; // end of the loop. ?>