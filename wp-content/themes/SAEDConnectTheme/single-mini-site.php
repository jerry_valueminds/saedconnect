<?php
    while ( have_posts() ) : the_post();

        /*First Loop: Find First Multipage related to Topic*/
        $connected = new WP_Query( array(
            'relationship' => array(
                'id'   => 'mini_site_to_m_item_page',
                'from' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
        while ( $connected->have_posts() ) : $connected->the_post();
            /*Redirect to Overview Page (Temporary: Add Redirect Option on Child Posts)*/
            if ( wp_redirect( get_permalink()) ) {
                exit;
            }
        endwhile;
        wp_reset_postdata();
    
    
    
    
    if( $_REQUEST['view'] == 'resources' ){
    
        get_template_part( 'template-parts/mini-site/resources' );
    
     } else { 
    
        /* First Loop: Find First Article related to Topic */
        $connected = new WP_Query( array(
            'relationship' => array(
                'id'   => 'mini_site_to_topic_article',
                'from' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
        while ( $connected->have_posts() ) : $connected->the_post();
    
            if ( wp_redirect( get_permalink()) ) {
                exit;
            }
                        
        endwhile;
        wp_reset_postdata();
    
     }

endwhile; // end of the loop. ?>