<?php /*Template Name: Profile - New CV */ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */ 
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Publication */
        $publication_key = 'publication_status';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
    <section class="row">
        <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>

        <div class="dashboard-multi-main-content full">
            <div class="page-header">
                <h1 class="page-title">
                    My CV
                </h1>
            </div>
            <article class="page-summary">
                <p>
                    Your Personal Profile is the Key information about you which would be required by potential employers
                </p>
            </article>
            <div class="row row-15">
                <div class="col-md-8 padding-lr-15">
                    <?php if( $_GET['view'] == 'form'){ ?>
                        <?php if( $_GET['form'] == 'personal-information'){ ?>
                            <!-- Personal Information form-->
                            <div class="section-wrapper">
                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        Add / Edit Personal Information         
                                    </h2>
                                    <div class="text-right">
                                        <a href="<?php echo currenturl(true) ?>" 
                                            class="edit-btn"
                                        >
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                                <div class="entry">
                                    <form action="<?php echo currenturl(false) ?>" method="post" class="form">
                                        <?php
                                            /*
                                            *
                                            * Save / Retrieve Form Data
                                            *
                                            */
                                            /* Get Post */
                                            $profile_query = new WP_Query();
                                            $profile_query->query( 
                                                array(
                                                    'post_type' => 'personal-info',
                                                    'post_status' => 'publish',
                                                    'author' => $current_user->ID,
                                                    'posts_per_page' => 1,
                                                ) 
                                            );

                                            if ( $profile_query->have_posts() ) {

                                                while ($profile_query->have_posts()) : $profile_query->the_post();

                                                /* Variables */
                                                $post_id = $post->ID;   //Get Program ID

                                                endwhile;

                                            }

                                            if($_POST){

                                                /* Get Post Name */
                                                $postName = 'Personal-Information-'.$current_user->ID;

                                                /* Get Form Data */
                                                $gender = sanitize_text_field( $_POST['gender'] ); 
                                                $date_of_birth = sanitize_text_field( $_POST['date_of_birth'] ); 
                                                $marital_status = sanitize_text_field( $_POST['marital_status'] ); 
                                                $nationality = sanitize_text_field( $_POST['nationality'] ); 
                                                $state_of_origin = sanitize_text_field( $_POST['state_of_origin'] ); 
                                                $work_location = sanitize_text_field( $_POST['work_location'] ); 
                                                $phone_work = sanitize_text_field( $_POST['phone_work'] ); 
                                                $whatsapp_work = sanitize_text_field( $_POST['whatsapp_work'] ); 
                                                $highest_level_of_education = sanitize_text_field( $_POST['highest_level_of_education'] ); 

                                                /* Save Post to DB */
                                                $post_id = wp_insert_post(array (
                                                    'ID' => $post_id,
                                                    'post_type' => 'personal-info',
                                                    'post_title' => $postName,
                                                    'post_content' => "",
                                                    'post_status' => 'publish',
                                                ));

                                                /* Add Meta */
                                                update_post_meta( $post_id, 'gender', $gender );
                                                update_post_meta( $post_id, 'date_of_birth', $date_of_birth );
                                                update_post_meta( $post_id, 'marital_status', $marital_status );
                                                update_post_meta( $post_id, 'nationality', $nationality );
                                                update_post_meta( $post_id, 'state_of_origin', $state_of_origin );
                                                update_post_meta( $post_id, 'work_location', $work_location );
                                                update_post_meta( $post_id, 'phone_work', $phone_work );
                                                update_post_meta( $post_id, 'whatsapp_work', $whatsapp_work );
                                                update_post_meta( $post_id, 'highest_level_of_education', $highest_level_of_education );

                                                /* Redirect */
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                            }
                                        ?>
                                        <div class="form-item">
                                            <label for="gender">
                                                Gender
                                            </label>
                                            <select name="gender" id="gender">
                                                <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                                <option value="Male" <?php echo ($meta == 'Male') ? 'selected' : '' ?>>Male</option>
                                                <option value="Female" <?php echo ($meta == 'Female') ? 'selected' : '' ?>>Female</option>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <label for="date_of_birth">
                                                Year of Birth
                                            </label>
                                            <input 
                                                type="date" 
                                                name="date_of_birth" 
                                                id="date_of_birth"
                                                value="<?php echo get_post_meta( $post_id, 'date_of_birth', true ); ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="marital_status">
                                                Marital Status
                                            </label>
                                            <select name="marital_status" id="marital_status">
                                                <?php $meta = get_post_meta( $post_id, 'marital_status', true ); ?>
                                                <option value="Single" <?php echo ($meta == 'Single') ? 'selected' : '' ?>>Single</option>
                                                <option value="Married" <?php echo ($meta == 'Married') ? 'selected' : '' ?>>Married</option>
                                                <option value="Divorced" <?php echo ($meta == 'Divorced') ? 'selected' : '' ?>>Divorced</option>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <label for="nationality">
                                                Nationality
                                            </label>
                                            <select name="nationality" id="nationality">
                                                <?php $meta = get_post_meta( $post_id, 'nationality', true ); ?>
                                                <option value="Nigerian" <?php echo ($meta == 'Nigerian') ? 'selected' : '' ?>>Nigerian</option>
                                                <option value="Other" <?php echo ($meta == 'Other') ? 'selected' : '' ?>>Other</option>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <label for="state_of_origin">
                                                State of Origin
                                            </label>
                                            <select name="state_of_origin" id="state_of_origin">
                                                <?php $meta = get_post_meta( $post_id, 'state_of_origin', true ); ?>
                                                <?php 
                                                    switch_to_blog(110);
                                                    $terms = get_terms( 'state', array('hide_empty' => false));
                                                    restore_current_blog();
                                                                           
                                                    foreach ($terms as $term) { 
                                                ?>
                                                <option value="<?php echo $term->term_id ?>" <?php echo ( $term->term_id == $meta ) ? 'selected' : ''; ?>><?php echo $term->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <label for="work_location">
                                                States in Nigeria where you can work
                                            </label>
                                            <select name="work_location" id="work_location">
                                                <?php $meta = get_post_meta( $post_id, 'work_location', true ); ?>
                                                <?php 
                                                    switch_to_blog(110);
                                                    $terms = get_terms( 'state', array('hide_empty' => false));
                                                    restore_current_blog();
                                                                           
                                                    foreach ($terms as $term) { 
                                                ?>
                                                <option value="<?php echo $term->term_id ?>" <?php echo ( $term->term_id == $meta ) ? 'selected' : ''; ?>><?php echo $term->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <label for="phone_work">
                                                Preferred Contact Telephone Number (For Work)
                                            </label>
                                            <input 
                                                type="telephone" 
                                                name="phone_work" 
                                                id="phone_work" 
                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                value="<?php echo get_post_meta( $post_id, 'phone_work', true ); ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="whatsapp_work">
                                                Preferred Contact Whatsapp Number (For Work)
                                            </label>
                                            <input 
                                                type="telephone" 
                                                name="whatsapp_work" 
                                                id="whatsapp_work" 
                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                value="<?php echo get_post_meta( $post_id, 'whatsapp_work', true ); ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="highest_level_of_education">
                                                Highest Level of Education
                                            </label>
                                            <select name="highest_level_of_education" id="highest_level_of_education">
                                                <?php $meta = get_post_meta( $post_id, 'highest_level_of_education', true ); ?>
                                                <option value="Primary School Certificate" <?php echo ($meta == 'Primary School Certificate') ? 'selected' : '' ?>>Primary School Certificate</option>
                                                <option value="Secondary School Certificate" <?php echo ($meta == 'Secondary School Certificate') ? 'selected' : '' ?>>Secondary School certificate</option>
                                                <option value="Diploma/NCE" <?php echo ($meta == 'Diploma/NCE') ? 'selected' : '' ?>>Diploma/NCE</option>
                                                <option value="Bachelors Degree" <?php echo ($meta == 'Bachelors Degree') ? 'selected' : '' ?>>Bachelors Degree</option>
                                                <option value="Post Graduate Diploma" <?php echo ($meta == 'Post Graduate Diploma') ? 'selected' : '' ?>>Post Graduate Diploma</option>
                                                <option value="Masters Degree" <?php echo ($meta == 'Masters Degree') ? 'selected' : '' ?>>Masters Degree</option>
                                                <option value="Phd" <?php echo ($meta == 'Phd') ? 'selected' : '' ?>>Phd</option>
                                            </select>
                                        </div>
                                        <div class="text-right">
                                            <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php }elseif( $_GET['form'] == 'education'){ ?>
                            <!-- Education form-->
                            <div class="section-wrapper">
                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        Add Education         
                                    </h2>
                                    <div class="text-right">
                                        <a href="<?php echo currenturl(true) ?>" 
                                            class="edit-btn"
                                        >
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                                <div class="entry">
                                    <form action="<?php echo currenturl(false) ?>" method="post" class="form">
                                        <?php
                                            /*
                                            *
                                            * Save / Retrieve Form Data
                                            *
                                            */
                                            /* Get Post */
                                            $post_type = 'education';
                                            $post_id = $_GET['post-id'];   //Get Program ID
                                                                     
                                            if( $post_id && $_GET['action'] == 'delete' ){
                                                wp_delete_post($post_id); // Delete
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) ); // Redirect
                                            }

                                            if($_POST){

                                                /* Get Post Data */
                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                $school = sanitize_text_field( $_POST['school'] ); 
                                                $field_of_study = sanitize_text_field( $_POST['field_of_study'] ); 
                                                $start_date = sanitize_text_field( $_POST['start_date'] ); 
                                                $end_date = sanitize_text_field( $_POST['end_date'] ); 
                                                $achievements = sanitize_text_field( $_POST['achievements'] ); 

                                                /* Save Post to DB */
                                                $post_id = wp_insert_post(array (
                                                    'ID' => $post_id,
                                                    'post_type' => $post_type,
                                                    'post_title' => $post_name,
                                                    'post_content' => "",
                                                    'post_status' => 'publish',
                                                ));

                                                /* Add Post Meta */
                                                update_post_meta( $post_id, 'school', $school );
                                                update_post_meta( $post_id, 'field_of_study', $field_of_study );
                                                update_post_meta( $post_id, 'start_date', $start_date );
                                                update_post_meta( $post_id, 'end_date', $end_date );
                                                update_post_meta( $post_id, 'achievements', $achievements );

                                                /* Redirect */
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                            }
                                        ?>
                                        <div class="form-item">
                                            <label for="deadline">
                                                Degree Attained (e.g BA, BS, JD, PhD)
                                            </label>
                                            <input 
                                                type="text" 
                                                name="post_name" 
                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="school">
                                                School
                                            </label>
                                            <input 
                                                type="text" 
                                                name="school" 
                                                value="<?php echo get_post_meta( $post_id, 'school', true ); ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="field_of_study">
                                                Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)
                                            </label>
                                            <input 
                                                type="text" 
                                                name="field_of_study" 
                                                value="<?php echo get_post_meta( $post_id, 'field_of_study', true ); ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="start_date">
                                                Start Month/Year
                                            </label>
                                            <input 
                                                type="date" 
                                                name="start_date" 
                                                value="<?php echo get_post_meta( $post_id, 'start_date', true ); ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="end_date">
                                                End Month/Year (Current students: Enter your expected graduation year)
                                            </label>
                                            <input 
                                                type="date" 
                                                name="end_date" 
                                                value="<?php echo get_post_meta( $post_id, 'end_date', true ); ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="achievements">
                                                Achievements
                                            </label>
                                            <input 
                                                type="text" 
                                                name="achievements" 
                                                value="<?php echo get_post_meta( $post_id, 'achievements', true ); ?>"
                                            >
                                        </div>
                                        <div class="text-right">
                                            <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php }elseif( $_GET['form'] == 'interest'){ ?>   
                            <!-- Interest form -->
                            <div class="section-wrapper">
                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        Add Interest          
                                    </h2>
                                    <div class="text-right">
                                        <a href="<?php echo currenturl(true) ?>" 
                                            class="edit-btn"
                                        >
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                                <div class="entry">
                                    <form action="<?php echo currenturl(false) ?>" method="post" class="form">
                                        <?php
                                            /*
                                            *
                                            * Save / Retrieve Form Data
                                            *
                                            */
                                            /* Get Post */
                                            $post_type = 'interest';
                                            $post_id = $_GET['post-id'];   //Get Program ID
                                                                     
                                            if( $post_id && $_GET['action'] == 'delete' ){
                                                wp_delete_post($post_id); // Delete
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) ); // Redirect
                                            }

                                            if($_POST){

                                                /* Get Post Data */
                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                $description = wp_kses_post( $_POST['description'] );

                                                /* Save Post to DB */
                                                $post_id = wp_insert_post(array (
                                                    'ID' => $post_id,
                                                    'post_type' => $post_type,
                                                    'post_title' => $post_name,
                                                    'post_content' => "",
                                                    'post_status' => 'publish',
                                                ));

                                                /* Add Post Meta */
                                                update_post_meta( $post_id, 'description', $description );

                                                /* Redirect */
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                            }
                                        ?>
                                        <div class="form-item">
                                            <label for="deadline">
                                                Title
                                            </label>
                                            <input 
                                                type="text" 
                                                name="post_name" 
                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="description">
                                                Description
                                            </label>
                                            <textarea id="description" name="description" class="editor" ><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                        </div>
                                        <div class="text-right">
                                            <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php }elseif( $_GET['form'] == 'language'){ ?> 
                            <!-- Language form -->
                            <div class="section-wrapper">
                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        Add Language          
                                    </h2>
                                    <div class="text-right">
                                        <a href="<?php echo currenturl(true) ?>" 
                                            class="edit-btn"
                                        >
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                                <div class="entry">
                                    <form action="<?php echo currenturl(false) ?>" method="post" class="form">
                                        <?php
                                            /*
                                            *
                                            * Save / Retrieve Form Data
                                            *
                                            */
                                            /* Get Post */
                                            $post_type = 'language';
                                            $post_id = $_GET['post-id'];   //Get Program ID
                                                                     
                                            if( $post_id && $_GET['action'] == 'delete' ){
                                                wp_delete_post($post_id); // Delete
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) ); // Redirect
                                            }

                                            if($_POST){

                                                /* Get Post Data */
                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                $spoken_level = sanitize_text_field( $_POST['spoken_level'] );
                                                $written_level = sanitize_text_field( $_POST['written_level'] );

                                                /* Save Post to DB */
                                                $post_id = wp_insert_post(array (
                                                    'ID' => $post_id,
                                                    'post_type' => $post_type,
                                                    'post_title' => $post_name,
                                                    'post_content' => "",
                                                    'post_status' => 'publish',
                                                ));

                                                /* Add Post Meta */
                                                update_post_meta( $post_id, 'spoken_level', $spoken_level );
                                                update_post_meta( $post_id, 'written_level', $written_level );

                                                /* Redirect */
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                            }
                                        ?>
                                        <div class="form-item">
                                            <div class="form-group">
                                                <label for="post_name">
                                                    Language
                                                </label>
                                                <select name="post_name" id="post_name">
                                                    <?php $meta = get_the_title( $post_id ); ?>
                                                    <option value="English" <?php echo ($meta == 'English') ? 'selected' : '' ?>>English</option>
                                                    <option value="Yoruba" <?php echo ($meta == 'Yoruba') ? 'selected' : '' ?>>Yoruba</option>
                                                    <option value="Hausa" <?php echo ($meta == 'Hausa') ? 'selected' : '' ?>>Hausa</option>
                                                    <option value="Fulani" <?php echo ($meta == 'Fulani') ? 'selected' : '' ?>>Fulani</option>
                                                    <option value="Ibo" <?php echo ($meta == 'Ibo') ? 'selected' : '' ?>>Ibo</option>
                                                    <option value="Swahili" <?php echo ($meta == 'Swahili') ? 'selected' : '' ?>>Swahili</option>
                                                    <option value="Chinese" <?php echo ($meta == 'Chinese') ? 'selected' : '' ?>>Chinese</option>
                                                    <option value="French" <?php echo ($meta == 'French') ? 'selected' : '' ?>>French</option>
                                                    <option value="Arabic" <?php echo ($meta == 'Arabic') ? 'selected' : '' ?>>Arabic</option>
                                                    <option value="Portuguese" <?php echo ($meta == 'Portuguese') ? 'selected' : '' ?>>Portuguese</option>
                                                    <option value="Spanish" <?php echo ($meta == 'Spanish') ? 'selected' : '' ?>>Spanish</option>
                                                    <option value="German" <?php echo ($meta == 'German') ? 'selected' : '' ?>>German</option>
                                                    <option value="Hindi" <?php echo ($meta == 'Hindi') ? 'selected' : '' ?>>Hindi</option>
                                                    <option value="Gujarati" <?php echo ($meta == 'Gujarati') ? 'selected' : '' ?>>Gujarati</option>
                                                    <option value="Punjabi" <?php echo ($meta == 'Punjabi') ? 'selected' : '' ?>>Punjabi</option>
                                                    <option value="Swedish" <?php echo ($meta == 'Swedish') ? 'selected' : '' ?>>Swedish</option>
                                                    <option value="Japanese" <?php echo ($meta == 'Japanese') ? 'selected' : '' ?>>Japanese</option>
                                                    <option value="Russian" <?php echo ($meta == 'Russian') ? 'selected' : '' ?>>Russian</option>
                                                    <option value="Korean" <?php echo ($meta == 'Korean') ? 'selected' : '' ?>>Korean</option>
                                                    <option value="Turkish" <?php echo ($meta == 'Turkish') ? 'selected' : '' ?>>Turkish</option>
                                                    <option value="Italian" <?php echo ($meta == 'Italian') ? 'selected' : '' ?>>Italian</option>
                                                    <option value="Finnish" <?php echo ($meta == 'Finnish') ? 'selected' : '' ?>>Finnish</option>
                                                    <option value="Polish" <?php echo ($meta == 'Polish') ? 'selected' : '' ?>>Polish</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-item">
                                            <div class="form-group">
                                                <label for="spoken_level">
                                                    Spoken level
                                                </label>
                                                <select name="spoken_level" id="spoken_level">
                                                    <?php $meta = get_post_meta( $post_id, 'spoken_level', true ); ?>
                                                    <option value="None" <?php echo ($meta == 'None') ? 'selected' : '' ?>>None</option>
                                                    <option value="Basic" <?php echo ($meta == 'Basic') ? 'selected' : '' ?>>Basic</option>
                                                    <option value="Fluent" <?php echo ($meta == 'Fluent') ? 'selected' : '' ?>>Fluent</option>
                                                    <option value="Native" <?php echo ($meta == 'Native') ? 'selected' : '' ?>>Native</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-item">
                                            <div class="form-group">
                                                <label for="written_level">
                                                    Written level
                                                </label>
                                                <select name="written_level" id="written_level">
                                                    <?php $meta = get_post_meta( $post_id, 'written_level', true ); ?>
                                                    <option value="None" <?php echo ($meta == 'None') ? 'selected' : '' ?>>None</option>
                                                    <option value="Basic" <?php echo ($meta == 'Basic') ? 'selected' : '' ?>>Basic</option>
                                                    <option value="Fluent" <?php echo ($meta == 'Fluent') ? 'selected' : '' ?>>Fluent</option>
                                                    <option value="Native" <?php echo ($meta == 'Native') ? 'selected' : '' ?>>Native</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php }elseif( $_GET['form'] == 'certification'){ ?>
                            <!-- Certification form -->
                            <div class="section-wrapper">
                                <div class="header">
                                    <h2 class="section-wrapper-title">
                                        Add Education         
                                    </h2>
                                    <div class="text-right">
                                        <a href="<?php echo currenturl(true) ?>" 
                                            class="edit-btn"
                                        >
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                                <div class="entry">
                                    <form action="<?php echo currenturl(false) ?>" method="post" class="form">
                                        <?php
                                            /*
                                            *
                                            * Save / Retrieve Form Data
                                            *
                                            */
                                            /* Get Post */
                                            $post_type = 'certification';
                                            $post_id = $_GET['post-id'];   //Get Program ID
                                                                     
                                            if( $post_id && $_GET['action'] == 'delete' ){
                                                wp_delete_post($post_id); // Delete
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) ); // Redirect
                                            }

                                            if($_POST){

                                                /* Get Post Data */
                                                $post_name = sanitize_text_field( $_POST['post_name'] ); 
                                                $type = sanitize_text_field( $_POST['type'] );
                                                $description = wp_kses_post( $_POST['description'] );

                                                /* Save Post to DB */
                                                $post_id = wp_insert_post(array (
                                                    'ID' => $post_id,
                                                    'post_type' => $post_type,
                                                    'post_title' => $post_name,
                                                    'post_content' => "",
                                                    'post_status' => 'publish',
                                                ));

                                                /* Add Post Meta */
                                                update_post_meta( $post_id, 'type', $type );
                                                update_post_meta( $post_id, 'description', $description );

                                                /* Redirect */
                                                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                                            }
                                        ?>
                                        <div class="form-item">
                                            <label for="deadline">
                                                Name
                                            </label>
                                            <input 
                                                type="text" 
                                                name="post_name" 
                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                            >
                                        </div>
                                        <div class="form-item">
                                            <label for="type">Certification or Affiliation?</label>
                                            <select name="type" id="type">
                                                <?php $meta = get_post_meta( $post_id, 'type', true ); ?>
                                                <option value="Certification" <?php echo ($meta == 'Certification') ? 'selected' : '' ?>>Certification</option>
                                                <option value="Affiliation" <?php echo ($meta == 'Affiliation') ? 'selected' : '' ?>>Affiliation</option>
                                            </select>
                                        </div>                                        
                                        <div class="form-item">
                                            <label for="description">
                                                Desription
                                            </label>
                                            <textarea id="description" name="description" class="editor"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                        </div>
                                        <div class="text-right">
                                            <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php }else{ ?>
                            <div class="text-center padding-o-40">
                                <p class="txt-3em txt-color-yellow padding-b-15">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </p>
                                <p class="txt-color-lighter padding-b-30">
                                    Oops! Something went wrong. Kindly go back and try again.
                                </p>
                                <p>
                                    <a href="<?php echo currenturl(true); ?>" class="btn btn-blue txt-sm">Go back</a>
                                </p>
                            </div>
                        <?php }?>

                    <?php }else{ ?>
                        <!-- persornal Information view -->
                        <div class="section-wrapper">
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    Personal Information                                        
                                </h2>
                                <div class="text-right">
                                    <a href="<?php echo currenturl(); ?>?view=form&form=personal-information" class="edit-btn">
                                        Edit
                                    </a>
                                </div>
                            </div>
                            <div class="entry">
                               <?php
                                    $profile_query = new WP_Query();
                                    $profile_query->query( 
                                        array(
                                            'post_type' => 'personal-info',
                                            'post_status' => 'publish',
                                            'author' => $current_user->ID,
                                            'posts_per_page' => 1,
                                        ) 
                                    );

                                    if ( $profile_query->have_posts() ) {

                                        while ($profile_query->have_posts()) : $profile_query->the_post();

                                        /* Variables */
                                        $post_id = $post->ID;   //Get Program ID
                                ?>
                                
                                <div class="row row-10">
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Gender          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'gender', true ); ?>                                                
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Yeah of Birth          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'date_of_birth', true ); ?>                                                
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Marital Status          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'marital_status', true ); ?>                                                
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Nationality          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'nationality', true ); ?>                                                
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            State of Origin          
                                        </p>
                                        <p class="txt-sm">
                                            <?php 
                                                $saved_id = get_post_meta( $post_id, 'state_of_origin', true ); 
                                                switch_to_blog(110);
                                                $term = get_term( $saved_id );
                                                echo $term->name;
                                                restore_current_blog();
                                            ?>                                                
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            States in Nigeria where you can work         
                                        </p>
                                        <p class="txt-sm">
                                            <?php 
                                                $saved_id = get_post_meta( $post_id, 'work_location', true ); 
                                                switch_to_blog(110);
                                                $term = get_term( $saved_id );
                                                echo $term->name;
                                                restore_current_blog();
                                            ?>                                               
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Preferred Contact Telephone Number (For Work)
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'phone_work', true ); ?>                                               
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Preferred Contact Whatsapp Number (For Work)         
                                        </p>
                                        <p class="txt-sm">
                                            +<?php echo get_post_meta( $post_id, 'whatsapp_work', true ); ?>                                                
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Highest Level of Education          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'highest_level_of_education', true ); ?>                                                
                                        </p>
                                    </div>
                                    
                                </div>
                                
                                <?php
                                        endwhile;

                                    }else{
                                ?>

                                    <p class="txt-sm padding-b-15">You have not added your mentor information.</p>

                                <?php } ?>
                            </div>
                        </div>
                        <!-- Education view -->
                        <div class="section-wrapper">
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    Education &amp; Training                                
                                </h2>
                            </div>
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'education',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );

                                if ( $profile_query->have_posts() ) {

                                    while ($profile_query->have_posts()) : $profile_query->the_post();

                                    /* Variables */
                                    $post_id = $post->ID;   //Get Program ID
                            ?>

                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Degree Attained (e.g BA, BS, JD, PhD)         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_the_title() ?>                                           
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            School      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'school', true ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'field_of_study', true ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Start Month/Year      
                                        </p>
                                        <p class="txt-sm">
                                           <?php echo get_post_meta( $post_id, 'start_date', true ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            End Month/Year (Current students: Enter your expected graduation year)     
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'end_date', true ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Achievements      
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'achievements', true ); ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="<?php echo currenturl(false).'?view=form&form=education&&post-id='.$post_id; ?>" class="edit-btn">
                                        Edit
                                    </a>
                                    <a href="<?php echo currenturl(false).'?view=form&form=education&action=delete&post-id='.$post_id; ?>" class="delete-btn margin-l-5 confirm-delete">
                                        Delete
                                    </a>
                                </div>
                            </div>
                           
                            <?php
                                    endwhile;

                                }else{
                            ?>

                            <div class="entry">
                                <p class="txt-sm">You have not added your education information.</p>
                            </div>

                            <?php } ?>                           
                            <div class="padding-o-20">
                                <a class="btn btn-ash txt-xxs no-m-b" href="<?php echo currenturl(); ?>?view=form&form=education">
                                    Add Education &amp; Training                                    
                                </a>
                            </div>
                        </div>
                        <!-- Interest -->
                        <div class="section-wrapper">
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    Interests                                
                                </h2>
                            </div>                         
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'interest',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );

                                if ( $profile_query->have_posts() ) {

                                    while ($profile_query->have_posts()) : $profile_query->the_post();

                                    /* Variables */
                                    $post_id = $post->ID;   //Get Program ID
                            ?>

                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Title         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_the_title() ?>                                           
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Description      
                                        </p>
                                        <article class="text-box txt-sm">
                                            <?php echo get_post_meta( $post_id, 'description', true ); ?>
                                        </article>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="<?php echo currenturl(false).'?view=form&form=interest&post-id='.$post_id; ?>" class="edit-btn">
                                        Edit
                                    </a>
                                    <a href="<?php echo currenturl(false).'?view=form&form=interest&action=delete&post-id='.$post_id; ?>" class="delete-btn margin-l-5 confirm-delete">
                                        Delete
                                    </a>
                                </div>
                            </div>
                           
                            <?php
                                    endwhile;

                                }else{
                            ?>

                            <div class="entry">
                                <p class="txt-sm">You have not added your interests.</p>
                            </div>

                            <?php } ?>
                            <div class="padding-o-20">
                                <a class="btn btn-ash txt-xxs no-m-b" href="<?php echo currenturl(); ?>?view=form&form=interest">
                                    Add Interest                                    
                                </a>
                            </div>
                        </div>
                        <!-- Language view -->
                        <div class="section-wrapper">
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    Languages                                
                                </h2>
                            </div>                           
                            
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'language',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );

                                if ( $profile_query->have_posts() ) {

                                    while ($profile_query->have_posts()) : $profile_query->the_post();

                                    /* Variables */
                                    $post_id = $post->ID;   //Get Program ID
                            ?>
                            
                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-4 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Language        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_the_title() ?>                                            
                                        </p>
                                    </div>
                                    <div class="col-md-4 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Spoken Level    
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'spoken_level', true ); ?>                                            
                                        </p>
                                    </div>
                                    <div class="col-md-4 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Written Level    
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_post_meta( $post_id, 'written_level', true ); ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="<?php echo currenturl(false).'?view=form&form=language&post-id='.$post_id; ?>" class="edit-btn">
                                        Edit
                                    </a>
                                    <a href="<?php echo currenturl(false).'?view=form&form=language&action=delete&post-id='.$post_id; ?>" class="delete-btn margin-l-5 confirm-delete">
                                        Delete
                                    </a>
                                </div>
                            </div>
                           
                            <?php
                                    endwhile;

                                }else{
                            ?>

                            <div class="entry">
                                <p class="txt-sm">You have not added your languages.</p>
                            </div>

                            <?php } ?>
                            <div class="padding-o-20">
                                <a class="btn btn-ash txt-xxs no-m-b" href="<?php echo currenturl(); ?>?view=form&form=language">
                                    Add Languages                                    
                                </a>
                            </div>
                        </div>
                        <!-- Certification view -->
                        <div class="section-wrapper">
                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    Certifications / Affiliations (if any)                        
                                </h2>
                            </div>
                            <?php
                                $profile_query = new WP_Query();
                                $profile_query->query( 
                                    array(
                                        'post_type' => 'certification',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => -1,
                                    ) 
                                );

                                if ( $profile_query->have_posts() ) {

                                    while ($profile_query->have_posts()) : $profile_query->the_post();

                                    /* Variables */
                                    $post_id = $post->ID;   //Get Program ID
                            ?>

                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Title         
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo get_the_title() ?>                                           
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Type      
                                        </p>
                                        <article class="text-box txt-sm">
                                            <?php echo get_post_meta( $post_id, 'type', true ); ?>
                                        </article>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Description      
                                        </p>
                                        <article class="text-box txt-sm">
                                            <?php echo get_post_meta( $post_id, 'description', true ); ?>
                                        </article>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a href="<?php echo currenturl(false).'?view=form&form=certification&post-id='.$post_id; ?>" class="edit-btn">
                                        Edit
                                    </a>
                                    <a href="<?php echo currenturl(false).'?view=form&form=certification&action=delete&post-id='.$post_id; ?>" class="delete-btn margin-l-5 confirm-delete">
                                        Delete
                                    </a>
                                </div>
                            </div>
                           
                            <?php
                                    endwhile;

                                }else{
                            ?>

                            <div class="entry">
                                <p class="txt-sm">What certifications do you possess? What year were they acquired? Please provide all the information about your certifications here.</p>
                            </div>

                            <?php } ?>
                            <div class="padding-o-20">
                                <a class="btn btn-ash txt-xxs no-m-b" href="<?php echo currenturl(true); ?>?view=form&form=certification">
                                    Add Certifications / Affiliations                                    
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-4 padding-lr-15">
                    <div class="dashboard-multi-main-sidebar w-100 p-0">
                        <div class="side-bar-card">
                            <h4 class="txt-medium txt-color-dark margin-b-20">
                                Download CV
                            </h4>
                            <p class="txt-sm">
                                Download a PDF of DOC version of your completed CV.
                            </p>
                            <div class="margin-t-20">
                                <!--<a href="" class="btn btn-ash txt-xxs no-m-b">
                                    <i class="fa fa-download"></i>
                                    <span class="padding-l-5">
                                        - Coming soon -
                                    </span>
                                </a>-->
                                <span class="btn btn-ash txt-xxs no-m-b">
                                    - Coming soon -
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>