<?php /*Template Name: Homepage-Business Clinic*/ ?>
    
<?php get_header() ?>
   
<main class="main-content">
        <section class="container-wrapper padding-tb-80 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        Business Clinic
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis deserunt reiciendis, architecto eveniet eos delectus, facilis ut ab dolorem ea dignissimos corporis harum repellat quo, possimus veniam odit maxime provident!
                    </h2>
                </div>
            </div>
        </section>
        
        <section>
            <div class="row txt-color-white">
            <?php wp_reset_postdata(); ?>
                <?php wp_reset_query(); ?>
                <?php // Display posts
                    $temp = $wp_query; $wp_query= null;
                    $wp_query = new WP_Query();
                    $wp_query->query(array('post_type' => 'information-session'));
                        while ($wp_query->have_posts()) : $wp_query->the_post();                
            ?>
                <div class="col-md-3">
                    <?php

                        $images = rwmb_meta( 'info-session-feautured-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                    ?>
                    <a
                       href="<?php the_permalink() ?>"
                       class="generic-card-bg h-180-480 align-bottom"
                       style="background-image:url('<?php echo $image['full_url']; ?>');"
                    >
                        <h2 class="txt-lg txt-medium txt-height-1-2 margin-b-60 txt-color-white">
                            <?php the_title() ?>
                        </h2>
                    </a>
                </div>
            <?php endwhile; ?>
            </div>
        </section>
    </main>
    
<?php get_footer() ?>