<?php get_header() ?>
<?php
    $product_category = get_queried_object(); 
?>

    <main class="main-content">
        <header class="container-wrapper dashboard-multi-header bg-yellow padding-tb-20">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="txt-xlg txt-medium ">
                        <a href="<?php echo get_site_url().'/products/' ?>" class="txt-color-dark">
                            <i class="fa fa-angle-left"></i>
                            Marketplace
                        </a>
                    </h1>
                </div>
            </div>
        </header>
        <section class="container-wrapper" style="padding-top: 62px">
            <div class="row">
                <div class="col-md-2 marketplace-main-menu">
                    <div class="cv-menu-toggle">
                        Filter
                    </div>
                    <div class="filter-box cv-menu-list">
                        <h2 class="txt-sm margin-b-40 d-none d-md-block">Filter Results</h2>
                        <form method="get">
                        <?php
                            /* Filter Options Array */
                            $filter_items_array = array(
                                array(
                                    'term_slug' =>'product-category',
                                    'term_name' => 'Category',
                                    'meta_key' => 'sell-product-category',
                                    'show_default' => true
                                ),
                                
                                array(
                                    'term_slug' =>'coverage-nigeria',
                                    'term_name' => 'Location',
                                    'meta_key' => 'trainer_locations',
                                    'show_default' => false
                                ),

                            );
                        ?>

                        <?php foreach($filter_items_array as $filter_item){ ?>

                            <div class="filter-group">
                                <button
                                   class="title dropdown-toggle"
                                   type="button"
                                   data-toggle="collapse"
                                   data-target="#filter-content-<?php echo $filter_item['term_slug']; ?>"
                                   aria-expanded="false"
                                   aria-controls="filter-content-<?php echo $filter_item['term_slug']; ?>"
                                >
                                    <?php echo $filter_item['term_name']; ?>
                                </button>
                                <div class="collapse show" id="filter-content-<?php echo $filter_item['term_slug']; ?>">
                                    <ul class="filter-list">
                                        <li>
                                            <span class="input-area">
                                                <input
                                                    type="radio"
                                                    name="<?php echo $filter_item['meta_key']; ?>"
                                                    value="all" 
                                                <?php
                                                    if ( esc_html($_REQUEST[$tax_slug]) == 'all' || esc_html($_REQUEST[$tax_slug]) == '' ) {
                                                        echo 'checked';
                                                    } 
                                                ?>

                                                >
                                                <label for="">All</label>
                                            </span>
                                        </li>
                                    <?php 

                                        //Get Terms
                                        $terms = get_terms( $filter_item['term_slug'], array('hide_empty' => false, 'parent' => 0)); //Get all the terms

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;

                                            $tax_slug = $filter_item['term_slug'];

                                            $term_id = $term->term_id; //Get the term ID
                                            $term_slug = $term->slug; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                            $term_url = get_term_link($term);
                                    ?>
                                        <li>
                                            <span class="input-area">
                                                <input
                                                    type="radio"
                                                    name="<?php echo $filter_item['meta_key']; ?>"
                                                    value="<?php echo $term_slug ?>"
                                                    <?php echo esc_html($_REQUEST[$tax_slug]) == $term_slug  ? 'checked' : '' ?>
                                                >
                                                <label for=""><?php echo $term_name; ?></label>
                                            </span>
                                        </li>
                                        
                                    <?php } ?>
                                    </ul>
                                </div>
                            </div>

                        <?php } ?>
                            <input class="btn btn-blue txt-xs full-width" type="submit" value="Filter" id="searchsubmit">
                        </form>
                    </div>
                </div>
                <div class="col-md-10 marketplace-main-content">
                    <header class="margin-b-40">
                        <h2 class="txt-xlg txt-medium margin-b-15">
                            <?php echo $product_category->name; ?>
                            <?php //print_r($term); ?>
                        </h2> 
                        <p class="txt-normal-s">
                            Browse through some of our most popular Products / Services.
                        </p>  
                    </header>
                    <div class="">
                        <ul class="row row-10 marketplace-cards">
                        <?php
                            /*
                            *==================================================================================
                            *==================================================================================
                            *   WP Query
                            *==================================================================================
                            *==================================================================================
                            */
                            // Define Tax Query
                            $meta_array = array('relation' => 'AND');

                            // Add Arguements from _REQUEST
                            foreach($_GET as $key => $value){

                                /* Check & Exclude:
                                        - S ( WP Search field )
                                        - Search-type ( For selecting Search Template View to use )
                                        - Empty values
                                */
                                if($key !== 's' && $key !== 'search-type' && !empty($value) && $value != 'all'){

                                    // Create Tax Array entry
                                    $category_array = array(

                                        // Add Term Term Values
                                        array (
                                            'key' => trim($key), //Texanomy Type
                                            'value' => $value, //Search field
                                        ),

                                    );

                                    // Add just created Array to Tax Array. 
                                    $meta_array = array_merge($meta_array, $category_array); 
                                }
                            }

                            // Create Query Argument
                            $args = array(
                                'post_type' => 'sell-product',
                                'showposts' => -1,
                                'meta_query' => $meta_array,
                            );


                            $wp_query = new WP_Query($args);

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Get Post ID */
                            $post_id = $post->ID 
                        ?>
                            <li class="col-sm-6 col-lg-3 padding-o-10">
                                <a href="<?php the_permalink() ?>">
                                    <?php
                                        /* Twitter */
                                        $field = 'sell-product-image';

                                        $meta = get_post_meta($post->ID, $field, true);

                                        if($meta){
                                    ?>
                                       
                                        <figure style="background-image:url('<?php echo $meta; ?>');">
                                        
                                        </figure>
                                    <?php } else{ ?>
                                        <figure style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/table.jpg');">
                                        
                                        </figure>
                                    <?php } ?>
                                    <div class="content">
                                        <div class="top">
                                            <div class="title">
                                                <?php the_title(); ?>
                                            </div>
                                        </div>
                                        <div class="bottom">
                                            <div class="location">
                                                Abuja, Lagos
                                            </div>
                                            <div class="post-time">
                                                2 March 2019
                                            </div>
                                        </div>
                                    </div>
                                </a>   
                            </li>
                        <?php
                            endwhile;
                        ?>
                        </ul>
                        <div class="padding-t-40 text-center">
                            <a href="" class="btn btn-trans-bw txt-sm">
                                Show More
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

<?php get_footer() ?>