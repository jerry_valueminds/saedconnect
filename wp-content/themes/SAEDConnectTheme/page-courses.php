    <?php /*Template Name: Courses Home*/ ?>
    
    <?php get_header() ?>
    
    <main class="main-content">
        <header class="course-directory-banner">
            <div class="content container-wrapper">
                <h1 class="title">
                    Learn from industry experts, connect with a global network of experience.
                </h1>
            </div>
        </header>
        <section class="container-wrapper bg-grey padding-tb-20 margin-b-40">
            <form class="filter-form row row-20" action="<?php echo home_url('/') ?>" method="GET">
                <div class="col-md-2"></div>              
                <div class="col-md-3 padding-lr-20">
                    <div class="form-item">
                        <!--<?php $args = array(
                            'show_option_all'    => 'Select Category',
                            'show_option_none'   => '',
                            'option_none_value'  => '-1',
                            'orderby'            => 'name',
                            'order'              => 'ASC',
                            'show_count'         => 0,
                            'hide_empty'         => 0,
                            'child_of'           => 0,
                            'exclude'            => '',
                            'include'            => '',
                            'echo'               => 1,
                            'selected'           => 0,
                            'hierarchical'       => 0,
                            'name'               => 'cat',
                            'id'                 => 'category-lis',
                            'class'              => 'postform',
                            'depth'              => 0,
                            'tab_index'          => 0,
                            'taxonomy'           => 'category',
                            'hide_if_empty'      => false,
                            'value_field'	     => 'term_id',
                        ); ?>
                        <?php wp_dropdown_categories( $args ); ?>-->
                        
                        <select name="category-list" id="category-list">
                            <option value="0" selected disabled hidden="">All Categories</option>
                            <?php 
                                $terms = get_terms( 'category', array('hide_empty' => false,)); //Get all the terms

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    
                                    if ( $parent=='0' ) {

                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                        
                                        echo '<option value="' .$term->term_id. '">'. $term_name . '</option>';
                                    } 
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 padding-lr-20">
                    <div class="form-item">
                        <select name="sub-category-list" id="sub-category-list">
                            <option value="0" selected>All Sub-Categories</option>
                        </select>
                    </div>
                </div>
                <input type="text" value="" name="s" id="s" hidden>
                <div class="col-md-3 padding-lr-20">
                    <input class="submit-btn" type="submit" value="Filter" id="searchsubmit">
                </div>
            </form>
        </section>
        <?php
        // Get all the categories
        $categories = get_terms( 'model' );

        // Loop through all the returned terms
        foreach ( $categories as $category ):
            
            // set up a new query for each category, pulling in related posts.
            $services = new WP_Query(
                array(
                    'post_type' => 'course',
                    'showposts' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy'  => 'model',
                            'terms'     => array( $category->slug ),
                            'field'     => 'slug'
                        )
                    )
                )
            );
        ?>
        <section class="container-wrapper padding-b-40">
            <header class="margin-b-20">
                <h2 class="txt-medium uppercase">Top <?php echo $category->name; ?> Courses</h2>    
            </header>
            <div class="row row-10">
                <?php
                    $article_counter = 1;
                    while ($services->have_posts()) : $services->the_post();
                ?>
                <?php
                    if($article_counter == 1){
                        if($category_counter % 2 == 1){
                            $column_width = 12;
                        }else{
                            $column_width = 8;
                        }
                    }else{
                        $column_width = 4;
                    }
                ?>
                <div class="col-md-3 padding-lr-10 padding-b-20">
                    <a class="course-directory-card" href="<?php the_permalink() ?>">
                        <article class="content">
                            <figure class="image-box">
                                <img src="images/heroes/honda_1.jpg" alt="">
                                <?php if (has_post_thumbnail()) { ?>

                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>" alt="">

                                <?php } else { ?>

                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.jpg" alt="">

                                <?php } ?>
                                
                                <?php if ( has_term( 'virtual-class', 'model' ) ) {?>
                                    <h4 class="model">Virtual Class</h4>
                                <?php } elseif ( has_term( 'blended', 'model' ) ) {?>
                                    <h4 class="model">Blended Class</h4>
                                <?php } elseif ( has_term( 'classroom', 'model' ) ) {?>
                                    <h4 class="model">Classroom</h4>
                                <?php } elseif ( has_term( 'fully-online', 'model' ) ) {?>
                                    <h4 class="model">Fully Online Class</h4>
                                <?php } ?>
                            </figure>
                            <div class="course-info">
                                <div class="skill-category">
                                    <span class="category">
                                        Category Name
                                    </span>
                                    <span class="sub-category">
                                        Sub-category Name
                                    </span>
                                </div>
                                <h3 class="title"><?php the_title() ?></h3>
                                <h4 class="instructor">
                                    <span class="const">BY:</span>
                                    <span class="name">Maero Uwede</span>
                                </h4>
                                <div class="footer">
                                    <div class="level">
                                        <?php if ( has_term( 'beginner', 'level' ) ) {?>
                                            Advanced
                                        <?php } elseif ( has_term( 'intermediate', 'level' ) ) {?>
                                            Intermediate
                                        <?php } elseif ( has_term( 'advanced', 'level' ) ) {?>
                                            Advanced
                                        <?php } ?>
                                    </div>
                                    <div class="duration">
                                        6 Hours
                                    </div>
                                </div>
                            </div>
                        </article>  
                    </a>   
                </div>
                <?php endwhile;
                    // Reset things, for good measure
                    $services = null;
                    wp_reset_postdata();
                ?>
            </div>
        </section>
        <?php
        // end the loop
        endforeach;
        ?>
    </main>
    
    <?php get_footer() ?>