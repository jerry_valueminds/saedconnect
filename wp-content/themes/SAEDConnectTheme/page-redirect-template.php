<?php /*Template Name: Redirect Template*/ ?>    

<?php
    // TO SHOW THE PAGE CONTENTS
    while ( have_posts() ) : the_post(); //Because the_content() works only inside a WP Loop

                    
        // If User is Logged in, redirect to User Dashbord
        $redirect_link = get_the_content(); //Get Daasboard Page Link by ID

        if ( wp_redirect( $redirect_link ) ) {
            exit;
        }
        

    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
?>