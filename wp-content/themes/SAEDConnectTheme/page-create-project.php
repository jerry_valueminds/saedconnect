<?php /*Template Name: Create Project */ ?>

<?php
                    
    if ( !is_user_logged_in() ) {
        // If User is Logged in, redirect to User Dashbord
        $dashboard_link = network_home_url().'/ventures-directory/login'; //Get Daasboard Page Link by ID

        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }

?>

<?php get_header() ?>

<?php $post_id = $_GET['post-id']; ?>

<?php
    $current_user = wp_get_current_user();
    $check = array();
    $project_id = $_GET['project-id'];
?>

<?php
    /* Publish Post */
    if( $_GET['project-id'] ){
        $post = get_post( $_GET['project-id'] ); //Get Post
        $post_author_id = get_post_field( 'post_author', $post ); // Get Author ID
    }else{
        $post_author_id = 0; // Get Author ID
    }

    /* Verify Post Author */
    if( $post_author_id && $post_author_id != $current_user->ID ){
        printf('<script>window.location.replace("%s")</script>', 'https://www.saedconnect.org/ventures-directory/create-project/' );
    }
    
    /* Publish Post */
    if( $_GET['form'] == 'publish' && $post->ID == $_GET['project-id'] && $post_author_id == $current_user->ID ){
        update_post_meta( $post->ID, 'publication_status', 'user_published' );
        printf('<script>window.location.replace("%s")</script>', get_the_permalink($post->ID) );
    }

    /* Get State Terms */
    switch_to_blog(110);
    $states = get_terms( 'state', array('hide_empty' => false));
    restore_current_blog();

    /* Get Post Types */
    switch_to_blog(104);
    
    /* Get Thematic Areas */
    $thematic_areas_query = new WP_Query();
    $thematic_areas_query->query( 
        array(
            'post_type' => 'thematic-area',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        ) 
    );

    /* Get SDGs */
    $sdgs_query = new WP_Query();
    $sdgs_query->query( 
        array(
            'post_type' => 'sdg',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        ) 
    );

    restore_current_blog();
?>
 
<?php
    /* Precess Image */
    function image_upload_process($post_id){
        /* Image Upload */
        $images = get_attached_media( 'image', $post_id );
        $previousImg_id = 0;
        if($images){
            foreach($images as $image) { 
                $previousImg_id = $image->ID;
            }
        }

        // Check that the nonce is valid, and the user can edit this post.
        if ( 
            isset( $_POST['my_image_upload_nonce'], $post_id ) 
            && wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
            && current_user_can( 'edit_post', $post_id )
            && ($_FILES['my_image_upload']['error'] != 4 )
        ) {
            // The nonce was valid and the user has the capabilities, it is safe to continue.

            // These files need to be included as dependencies when on the front end.
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            /* Delete prevoius image */
            if($previousImg_id){
                wp_delete_attachment( $previousImg_id );
            }

            // Let WordPress handle the upload.
            // Remember, 'my_image_upload' is the name of our file input in our form above.
            $attachment_id = media_handle_upload( 'my_image_upload', $post_id );

            if ( is_wp_error( $attachment_id ) ) {
                // There was an error uploading the image.
            }

        } else {
            // The security check failed, maybe show the user an error.
        }
    }

    /* Get Image Field */
    function the_image_upload_field($post_id){
        $images = get_attached_media( 'image', $post_id );
        $previousImg_id = 0;
        if($images){
            foreach($images as $image) { 
                $previousImg_id = $image->ID;
            }
        }
        
        echo '<div class="image-upload-card ">';
            echo '<input type="file" name="my_image_upload" class="upload-field my_image_upload" multiple="false">';
            wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' );
            echo '<div class="featuredImagePreview text-align-center bg-darkgrey txt-xlg text-center txt-light" style="background-image: url('.wp_get_attachment_url($previousImg_id,"full").')">';  
                echo '<span class="image-label">Click here to select an image from your device</span>';
            echo '</div>';
        echo '</div>';  
    }
?>
  
    <style>
        .image-upload-card{
            position: relative;
        }

        .image-upload-card .upload-field{
            position: absolute;
            width: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            opacity: 0;
            z-index: 100;
            cursor: pointer;
        }

        .image-upload-card .featuredImagePreview {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 400px;
            padding: 15px;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
        
        .image-upload-card .featuredImagePreview .image-label {
            font-size: 0.8em;
            line-height: 1.5em;
            color: white;
            padding: 15px;
            background-color: rgba(0,0,0,0.5);
        }
    </style>
   
    <main class="main-content" style="padding-top: 70px;margin-top:0;background-color: #e0e0e0">
        <header class="bg-orange padding-tb-80 d-none">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-sm-10 col-md-10 col-lg-6 mx-auto text-center">
                        <?php
                            /* Get current user */
                            $current_user = wp_get_current_user();

                            /* Get Avatar */
                            $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                            $meta_key = 'user_avatar_url';
                            $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

                            if($get_avatar_url){
                                $avatar_url = $get_avatar_url;
                            }

                            /* Get User Display Name */
                            switch_to_blog(1);

                            $gf_id = 4; //Form ID
                            $gv_id = 1385; //Gravity View ID
                            $title = 'Profile';

                            $entry_count = 0;

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                    )
                                )
                            );

                            /* Get Entries */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

                            if($entry_count){ //If no entry
                                foreach( $entries as $entry ){          
                                    $displayname = rgar( $entry, '4.3' );
                                }                
                            }  
                        
                            restore_current_blog();
                        ?>
                        <h1 class="txt-xxlg txt-color-white txt-medium padding-b-20">Hello <?php echo $displayname ?>!</h1>
                        <h1 class="txt-2em txt-height-1-2 margin-b-20">
                            Tell us about your Project
                        </h1>
                        <h2 class="txt-lg txt-height-1-5">
                            We know there are people like you doing amazing things to make the world a better place, and we would like to showcase your work to the world and connect you to supporters, globally. Tell us about your project. What are you doing to make the world a better place?
                        </h2>
                    </div>
                </div>
            </div>
        </header>
        
        <section class="container-wrapper padding-t-40 padding-b-40">
            <div class="row">
                <div class="col-lg-10 mx-auto padding-t-80">
                    <header class="col txt-xlg txt-height-1-2 txt-color-dark">
                        <h1 class="txt-light margin-b-20">Hello <?php echo ($current_user->ID == 1)? 'SAEDConnect Admin' : $displayname; ?>!</h1>
                        <h2 class="txt-bold margin-b-10">Tell us about your Project</h2>
                    </header>
                    <p>We know there are people like you doing amazing things to make the world a better place, and we would like to showcase your work to the world and connect you to supporters, globally. Tell us about your project. What are you doing to make the world a better place?</p>
                </div>
            </div>
        </section>
        
        <section class="padding-b-40">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
        
                        <div>
                            
                            <?php $post = get_post( $_GET['project-id'] ); ?>

                            <?php if( $post->ID == $_GET['project-id'] ){ ?>

                                <?php if( $post_author_id == $current_user->ID ){ ?>
                                
                                    <!-- Info -->
                                    <article class="bg-ash padding-o-30 margin-b-40">
                                        <h2 class="txt-bold txt-color-dark padding-b-15">Instruction</h2>
                                        <p class="txt-sm txt-color-white">Provide all the information requested below to create your project. You will only be able to submit this project after you have provided all the requested information.</p>
                                    </article>
                                
                                    <!-- Project Forms -->
                                    <div>
                                        <!-- Project Information -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <?php $post = get_post( $_GET['project-id'] ); ?>
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#create-project-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Project Information</span>
                                                        <?php if( $post->ID == $_GET['project-id'] ){ ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="create-project-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What project have you started? When?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php 
                                                                if ( $post->ID == $_GET['project-id'] ) {
                                                                    $check[] = 'complete';  $post_id = $post->ID; 
                                                            ?> 

                                                                <!-- Entry -->
                                                                <div class="txt-color-light">
                                                                    <div class="padding-b-15">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Project Title          
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="padding-b-15">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Short summary about the project         
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'summary', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="padding-b-15">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Focus areas your project aligns with     
                                                                        </p>
                                                                        <article class="text-box txt-sm">
                                                                            <ul>
                                                                                <?php
                                                                                    $list = get_post_meta( $project_id, 'thematic_areas', false );

                                                                                    if ( $thematic_areas_query->have_posts() ) {

                                                                                        while ($thematic_areas_query->have_posts()) : $thematic_areas_query->the_post();
                                                                                            if( in_array($post->ID, $list) ){
                                                                                ?>
                                                                                            <li><?php the_title() ?></li>

                                                                                        <?php } ?>
                                                                                    <?php endwhile; ?>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        </article>
                                                                    </div>
                                                                    <div>
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            SDGs this project is related    
                                                                        </p>
                                                                        <article class="text-box txt-sm">
                                                                            <ul>
                                                                                <?php
                                                                                    $list = get_post_meta( $post_id, 'sdgs', true );

                                                                                    if ( $sdgs_query->have_posts() ) {

                                                                                        while ($sdgs_query->have_posts()) : $sdgs_query->the_post();
                                                                                            if( in_array($post->ID, $list) ){
                                                                                ?>
                                                                                            <li><?php the_title() ?></li>

                                                                                        <?php } ?>
                                                                                    <?php endwhile; ?>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        </article>
                                                                    </div>
                                                                </div>

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't created your project yet.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#createProjectModal">
                                                                <?php $post = get_post( $_GET['project-id'] ); ?>
                                                                <?php if( $post->ID == $_GET['project-id'] ){ ?>
                                                                    Edit
                                                                <?php }else{ ?>
                                                                    Add New
                                                                <?php } ?>
                                                            </a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Project Snapshot -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-snapshot-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Project Snapshot</span>
                                                        <?php 
                                                             $post = get_post( $_GET['project-id'] ); 
                                                             $post_id = $post->ID; 
                                                        ?>
                                                        <?php 
                                                            if( 
                                                                get_post_meta( $post_id, 'start_year', true ) 
                                                                && get_post_meta( $post_id, 'capacity', true )
                                                                && get_post_meta( $post_id, 'target_age_group', true )
                                                                && get_post_meta( $post_id, 'state', true )
                                                            ){ 
                                                                $check[] = 'complete'; 
                                                        ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <?php $check[] = 'incomplete';  ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-snapshot-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What project have you started? When?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php 
                                                                if( 
                                                                    get_post_meta( $post_id, 'start_year', true ) 
                                                                    || get_post_meta( $post_id, 'capacity', true )
                                                                    || get_post_meta( $post_id, 'target_age_group', true )
                                                                    || get_post_meta( $post_id, 'state', true )
                                                                ){ 
                                                            ?>

                                                                <!-- Entry -->
                                                                <div class="txt-color-light">
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Year you started this project          
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'start_year', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Capacity in which you are doing this project        
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'capacity', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                    <div>
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Target Age-group(s)   
                                                                        </p>
                                                                        <article class="text-box txt-sm">
                                                                            <ul>
                                                                            <?php
                                                                                $list = get_post_meta( $post_id, 'target_age_group', true );
                                                                                $age_groups_array = array(
                                                                                    '1 – 5', '6 – 12', '13 – 19', '20 – 25', '26 – 30', '30 – 40', '40 – 60', 'Above 60',   
                                                                                );
                                                                            ?>

                                                                            <?php foreach ( $age_groups_array as $age_group_item ) { ?>
                                                                                <?php foreach ($list as $list_item){ ?>
                                                                                    <?php if ($age_group_item == $list_item){ ?>
                                                                                    <li><?php echo $age_group_item ?></li>     
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                            </ul>
                                                                        </article>
                                                                    </div>
                                                                    <div>
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            States this project active    
                                                                        </p>
                                                                        <article class="text-box txt-sm">
                                                                            <ul>
                                                                            <?php $meta = get_post_meta( $post_id, 'state', true ); ?>

                                                                            <?php foreach ($states as $state) { ?>
                                                                                <?php foreach ($meta as $saved_state){ ?>
                                                                                    <?php if ($state->term_id == $saved_state){ ?>
                                                                                        <li><?php echo $state->name ?></li>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                            </ul>
                                                                        </article>
                                                                    </div>
                                                                </div>

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't created your project yet.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#projectSnapshotModal">
                                                                Add / Edit
                                                            </a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="projectSnapshotModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=project-snapshot'; echo ( isset($_GET['project-id']) ) ? '&project-id='.$_GET['project-id'] : ''; ?>" method="post">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Project Snapshot</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;
                                                                    $post = get_post( $_GET['project-id'] );
                                                                    if( $_GET['project-id'] && $_GET['project-id'] == $post->ID ){
                                                                        $post_id = $_GET['project-id'] ;
                                                                    }


                                                                    if( $_POST && $_GET['form'] == 'project-snapshot' ){

                                                                        update_post_meta( $post_id, 'start_year', sanitize_text_field( $_POST['start_year'] ) );
                                                                        update_post_meta( $post_id, 'capacity', sanitize_text_field( $_POST['capacity'] ) );
                                                                        update_post_meta( $post_id, 'target_age_group', $_POST['target_age_group'] );
                                                                        update_post_meta( $post_id, 'number_of_users', sanitize_text_field( $_POST['number_of_users'] ) );
                                                                        update_post_meta( $post_id, 'state', $_POST['state'] );

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$post_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="start_year">
                                                                        What year did you start this project?
                                                                    </label>
                                                                    <input 
                                                                        type="number" 
                                                                        name="start_year" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo get_post_meta( $post_id, 'start_year', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="capacity">
                                                                        In what capacity are you doing this project?
                                                                    </label>
                                                                    <select name="capacity" id="capacity">
                                                                        <?php $meta = get_post_meta( $post_id, 'capacity', true ); ?>
                                                                        <option value="As my Individual pet project" <?php echo ($meta == 'As my Individual pet project') ? 'selected' : '' ?>>As my Individual pet project</option>
                                                                        <option value="As a NGO project" <?php echo ($meta == 'As a NGO project') ? 'selected' : '' ?>>As a NGO project</option>
                                                                        <option value="It is a CSR Project of my company" <?php echo ($meta == 'It is a CSR Project of my company') ? 'selected' : '' ?>>It is a CSR Project of my company</option>
                                                                        <option value="It is run as a social enterprise business" <?php echo ($meta == 'It is run as a social enterprise business') ? 'selected' : '' ?>>It is run as a social enterprise business</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="target_age_group" class="d-block padding-b-10">
                                                                        Target Age-group
                                                                    </label>
                                                                    <?php
                                                                        $list = get_post_meta( $post_id, 'target_age_group', true );
                                                                        $age_groups_array = array(
                                                                            '1 – 5', '6 – 12', '13 – 19', '20 – 25', '26 – 30', '30 – 40', '40 – 60', 'Above 60',   
                                                                        );

                                                                        foreach ( $age_groups_array as $age_group_item ) {

    #                                                                ?>
                                                                    <label class="txt-sm d-flex align-items-center padding-b-10 padding-r-15">
                                                                        <input
                                                                            class="custom-check"
                                                                            type="checkbox" 
                                                                            value="<?php echo $age_group_item ?>" 
                                                                            name="target_age_group[]" 
                                                                            <?php echo in_array($age_group_item, $list) ? "checked" : "" ?>
                                                                            <?php 
                                                                                foreach ($list as $list_item){
                                                                                    echo ( $age_group_item == $list_item ) ? "checked" : "";
                                                                                }
                                                                            ?>
                                                                        >
                                                                        <span class="font-weight-normal padding-l-10">
                                                                            <?php echo $age_group_item ?>
                                                                        </span>
                                                                    </label>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="number_of_users">
                                                                        No. Of Users/Beneficiaries till date
                                                                    </label>
                                                                    <input 
                                                                        type="number" 
                                                                        name="number_of_users" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo get_post_meta( $post_id, 'number_of_users', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="state" class="d-block padding-b-10">
                                                                        In what states is this project active?
                                                                    </label>

                                                                    <?php $meta = get_post_meta( $post_id, 'state', true ); ?>

                                                                    <?php foreach ($states as $state) { ?>

                                                                    <label class="txt-sm d-inline-flex align-items-center padding-b-10 padding-r-15">
                                                                        <input
                                                                            class="custom-check"
                                                                            type="checkbox" 
                                                                            value="<?php echo $state->term_id ?>" 
                                                                            name="state[]" 
                                                                            <?php 
                                                                                foreach ($meta as $saves_state){
                                                                                    echo ( $saves_state== $state->term_id ) ? "checked" : "";
                                                                                }
                                                                            ?>
                                                                        >
                                                                        <span class="font-weight-normal padding-l-10">
                                                                            <?php echo $state->name ?>
                                                                        </span>
                                                                    </label>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Project Description -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-description-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Project Description</span>
                                                        <?php 
                                                             $post = get_post( $_GET['project-id'] ); 
                                                             $post_id = $post->ID; 
                                                        ?>
                                                        <?php 
                                                            if( 
                                                                get_post_meta( $post_id, 'problem_solved', true ) 
                                                                && get_post_meta( $post_id, 'your_solution', true )
                                                                && get_post_meta( $post_id, 'tips_for_implementation', true )
                                                            ){ 
                                                                $check[] = 'complete'; 
                                                        ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <?php $check[] = 'incomplete';  ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-description-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What project have you started? When?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php 
                                                                if( 
                                                                    get_post_meta( $post_id, 'problem_solved', true ) 
                                                                    || get_post_meta( $post_id, 'your_solution', true )
                                                                    || get_post_meta( $post_id, 'tips_for_implementation', true )
                                                                ){ 
                                                            ?>
                                                                <!-- Entry -->
                                                                <div class="txt-color-light">
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            The problem this project solves         
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'problem_solved', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Your solution      
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'your_solution', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Tips for Implementation        
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'tips_for_implementation', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                </div>

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't added your project description.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#projectDescriptionModal">
                                                                Add / Edit
                                                            </a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="projectDescriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=project-description'; echo ( isset($_GET['project-id']) ) ? '&project-id='.$_GET['project-id'] : ''; ?>" method="post">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Project Description</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;
                                                                    $post = get_post( $_GET['project-id'] );
                                                                    if( $_GET['project-id'] && $_GET['project-id'] == $post->ID ){
                                                                        $post_id = $_GET['project-id'] ;
                                                                    }


                                                                    if( $_POST && $_GET['form'] == 'project-description' ){

                                                                        update_post_meta( $post_id, 'problem_solved', wp_kses_post( $_POST['problem_solved'] ) );
                                                                        update_post_meta( $post_id, 'your_solution', wp_kses_post( $_POST['your_solution'] ) );
                                                                        update_post_meta( $post_id, 'tips_for_implementation', wp_kses_post( $_POST['tips_for_implementation'] ) );

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$post_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="problem_solved">
                                                                        What problem does this project solve?
                                                                    </label>
                                                                    <textarea name="problem_solved" id="problem_solved" rows="8"><?php echo get_post_meta( $post_id, 'problem_solved', true ); ?></textarea>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="your_solution">
                                                                        What is your solution?
                                                                    </label>
                                                                    <textarea name="your_solution" id="your_solution" rows="8"><?php echo get_post_meta( $post_id, 'your_solution', true ); ?></textarea>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="tips_for_implementation">
                                                                        Tips for Implementation (What would you advise anyone who wants to implement this project to have in place?)
                                                                    </label>
                                                                    <textarea name="tips_for_implementation" id="tips_for_implementation" rows="8"><?php echo get_post_meta( $post_id, 'tips_for_implementation', true ); ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Project Updates -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <?php
                                                $profile_query = new WP_Query();
                                                $profile_query->query( 
                                                    array(
                                                        'post_type' => 'project-meta',
                                                        'post_status' => 'publish',
                                                        'author' => $current_user->ID,
                                                        'posts_per_page' => -1,
                                                        'meta_query' => array(
                                                            array(
                                                                'key' => 'meta-type',
                                                                'value' => 'project-update',
                                                            ),
                                                            array(
                                                                'key' => 'parent',
                                                                'value' => $project_id
                                                            )
                                                        ),
                                                    ) 
                                                );
                                            ?>
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-updates-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Project Updates </span>
                                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-updates-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What schools have you attended? When? What did you study? What degrees do you have?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                                <?php $check[] = 'complete'; ?>

                                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                                                    <?php $post_id = $post->ID; ?> 

                                                                    <!-- Entry -->
                                                                    <div class="row row-10 padding-b-20">
                                                                        <figure class="col-auto padding-lr-10">
                                                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/application_manager/education.png" alt="" width="40">
                                                                        </figure>
                                                                        <div class="col padding-lr-10 txt-color-light">
                                                                            <p class="txt-normal-s txt-medium">
                                                                                <?php the_title() ?>

                                                                            </p>
                                                                            <p class="txt-sm">
                                                                                <?php echo get_post_meta( $post_id, 'type', true ) ?> |
                                                                                <a href="<?php echo ''.get_post_meta( $post_id, 'url', true ); ?>" class="txt-color-blue">
                                                                                    <?php echo ''.get_post_meta( $post_id, 'url', true ); ?>
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                                            <a class="txt-green" data-toggle="modal" href="#projectEditUpdateModal-<?php echo $post_id ?>">
                                                                                <i class="fa fa-pencil"></i>
                                                                                Edit
                                                                            </a>
                                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=project-edit-update-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                                <i class="fa fa-trash"></i>
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                    <!-- Edit Modal -->
                                                                    <div class="modal fade font-main filter-modal" id="projectEditUpdateModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <form action="<?php echo currentUrl(true).'?form=project-edit-update-'.$post_id.'&project-id='.$project_id.'&post-id='.$post_id; ?>" method="post" enctype="multipart/form-data">
                                                                                    <div class="modal-header padding-lr-30">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Update</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body padding-o-30 form">
                                                                                        <?php

                                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'project-edit-update-'.$post_id ){
                                                                                                wp_delete_post($post_id); // Delete
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }

                                                                                            $images = get_attached_media( 'image', $post_id );
                                                                                            $previousImg_id = 0;
                                                                                            if($images){
                                                                                                foreach($images as $image) { 
                                                                                                    $previousImg_id = $image->ID;
                                                                                                }
                                                                                            }

                                                                                            if( $_POST && $_GET['form'] == 'project-edit-update-'.$post_id ){

                                                                                                /* Get Post Data */
                                                                                                $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                                                /* Save Post to DB */
                                                                                                $post_id = wp_insert_post(array (
                                                                                                    'ID' => $post_id,
                                                                                                    'post_type' => 'project-meta',
                                                                                                    'post_title' => $post_name,
                                                                                                    'post_content' => "",
                                                                                                    'post_status' => 'publish',
                                                                                                ));

                                                                                                update_post_meta( $post_id, 'type', sanitize_text_field( $_POST['type'] ) );
                                                                                                update_post_meta( $post_id, 'url', sanitize_text_field( $_POST['url'] ) );

                                                                                                /* Image Upload */
                                                                                                image_upload_process($post_id);

                                                                                                /* Redirect */
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }
                                                                                        ?>
                                                                                        <div class="form-item">
                                                                                            <label for="post_name">
                                                                                                Title
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="post_name" 
                                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="padding-b-30">
                                                                                            <?php the_image_upload_field(); ?>
                                                                                        </div> 
                                                                                        <div class="form-item">
                                                                                            <label for="type">
                                                                                                Type 
                                                                                            </label>
                                                                                            <select name="type" id="type">
                                                                                                <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                                                                                <option value="Article" <?php echo ($meta == 'Article') ? 'selected' : '' ?>>Article</option>
                                                                                                <option value="Album" <?php echo ($meta == 'Album') ? 'selected' : '' ?>>Album</option>
                                                                                                <option value="Video" <?php echo ($meta == 'Video') ? 'selected' : '' ?>>Video</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="url">
                                                                                                URL
                                                                                            </label>
                                                                                            <input 
                                                                                                type="url" 
                                                                                                name="url" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'url', true ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer padding-lr-30">
                                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <?php endwhile; ?> 

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't any updates yet.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#AddProductUpdatesModal">Add New</a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="AddProductUpdatesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=add-project-update&project-id='.$project_id; ?>" method="post" enctype="multipart/form-data">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Project Updates</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;

                                                                    if( $_POST && $_GET['form'] == 'add-project-update' ){
                                                                        /* Get Post Data */
                                                                        $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                        /* Save Post to DB */
                                                                        $post_id = wp_insert_post(array (
                                                                            'ID' => $post_id,
                                                                            'post_type' => 'project-meta',
                                                                            'post_title' => $post_name,
                                                                            'post_content' => "",
                                                                            'post_status' => 'publish',
                                                                        ));

                                                                        /* Add Post Meta */
                                                                        update_post_meta( $post_id, 'parent', $project_id );
                                                                        update_post_meta( $post_id, 'meta-type', 'project-update' );

                                                                        update_post_meta( $post_id, 'type', sanitize_text_field( $_POST['type'] ) );
                                                                        update_post_meta( $post_id, 'url', sanitize_text_field( $_POST['url'] ) );

                                                                        /* Image Upload */
                                                                        image_upload_process($post_id);

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="post_name">
                                                                        Title
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="post_name" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                    >
                                                                </div>
                                                                <div class="padding-b-30">
                                                                    <?php the_image_upload_field(); ?>
                                                                </div> 
                                                                <div class="form-item">
                                                                    <label for="type">
                                                                        Type 
                                                                    </label>
                                                                    <select name="type" id="type">
                                                                        <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                                                        <option value="Article" <?php echo ($meta == 'Article') ? 'selected' : '' ?>>Article</option>
                                                                        <option value="Album" <?php echo ($meta == 'Album') ? 'selected' : '' ?>>Album</option>
                                                                        <option value="Video" <?php echo ($meta == 'Video') ? 'selected' : '' ?>>Video</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="url">
                                                                        URL
                                                                    </label>
                                                                    <input 
                                                                        type="url" 
                                                                        name="url" 
                                                                        value="<?php echo get_post_meta( $post_id, 'url', true ); ?>"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Project Contact Details -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-contact-details-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Project Contact Details</span>
                                                        <?php 
                                                             $post = get_post( $_GET['project-id'] ); 
                                                             $post_id = $post->ID; 
                                                        ?>
                                                        <?php 
                                                            if( 
                                                                get_post_meta( $post_id, 'contact_website', true ) 
                                                                && get_post_meta( $post_id, 'contact_email', true )
                                                                && get_post_meta( $post_id, 'contact_name', true )
                                                                && get_post_meta( $post_id, 'contact_phone', true )
                                                            ){ 
                                                                $check[] = 'complete'; 
                                                        ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <?php $check[] = 'incomplete';  ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-contact-details-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What project have you started? When?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php 
                                                                if( 
                                                                    get_post_meta( $post_id, 'contact_website', true ) 
                                                                    || get_post_meta( $post_id, 'contact_email', true )
                                                                    || get_post_meta( $post_id, 'contact_name', true )
                                                                    || get_post_meta( $post_id, 'contact_phone', true )
                                                                ){ 
                                                            ?>

                                                                <!-- Entry -->
                                                                <div class="txt-color-light">
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Project Website Address         
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'contact_website', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Project Contact Email      
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'contact_email', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Name of Primary Contact Person        
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'contact_name', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="padding-b-20">
                                                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                            Project Contact Telephone Number        
                                                                        </p>
                                                                        <p class="txt-sm">
                                                                            <?php echo get_post_meta( $post_id, 'contact_phone', true ); ?>
                                                                        </p>
                                                                    </div>
                                                                </div>

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't added your contact information.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#projectContactDetailsModal">
                                                                Add / Edit
                                                            </a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="projectContactDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=project-contact-details'; echo ( isset($_GET['project-id']) ) ? '&project-id='.$_GET['project-id'] : ''; ?>" method="post">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Project Contact Details</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;
                                                                    $post = get_post( $_GET['project-id'] );
                                                                    if( $_GET['project-id'] && $_GET['project-id'] == $post->ID ){
                                                                        $post_id = $_GET['project-id'] ;
                                                                    }


                                                                    if( $_POST && $_GET['form'] == 'project-contact-details' ){

                                                                        update_post_meta( $post_id, 'contact_website', sanitize_text_field( $_POST['contact_website'] ) );
                                                                        update_post_meta( $post_id, 'contact_email', sanitize_text_field( $_POST['contact_email'] ) );
                                                                        update_post_meta( $post_id, 'contact_name', sanitize_text_field( $_POST['contact_name'] ) );
                                                                        update_post_meta( $post_id, 'contact_phone', sanitize_text_field( $_POST['contact_phone'] ) );

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$post_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="contact_website">
                                                                        Project Website Address
                                                                    </label>
                                                                    <input 
                                                                        type="url" 
                                                                        name="contact_website" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo get_post_meta( $post_id, 'contact_website', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="contact_email">
                                                                        Project Contact Email
                                                                    </label>
                                                                    <input 
                                                                        type="email" 
                                                                        name="contact_email" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo get_post_meta( $post_id, 'contact_email', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="contact_name">
                                                                        Name of Primary Contact Person
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="contact_name" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo get_post_meta( $post_id, 'contact_name', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="contact_phone">
                                                                        Project Contact Telephone Number
                                                                    </label>
                                                                    <input 
                                                                        type="tel" 
                                                                        name="contact_phone" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo get_post_meta( $post_id, 'contact_phone', true ); ?>"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Project Milestones -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <?php
                                                $profile_query = new WP_Query();
                                                $profile_query->query( 
                                                    array(
                                                        'post_type' => 'project-meta',
                                                        'post_status' => 'publish',
                                                        'author' => $current_user->ID,
                                                        'posts_per_page' => -1,
                                                        'meta_query' => array(
                                                            array(
                                                                'key' => 'meta-type',
                                                                'value' => 'project-milestone',
                                                            ),
                                                            array(
                                                                'key' => 'parent',
                                                                'value' => $project_id
                                                            )
                                                        ),
                                                    ) 
                                                );
                                            ?>
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-milestones-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Project Milestones</span>
                                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-milestones-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What schools have you attended? When? What did you study? What degrees do you have?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                                <?php $check[] = 'complete'; ?>

                                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                                                    <?php $post_id = $post->ID; ?> 

                                                                    <!-- Entry -->
                                                                    <div class="row row-10 padding-b-20">
                                                                        <figure class="col-auto padding-lr-10">
                                                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/application_manager/education.png" alt="" width="40">
                                                                        </figure>
                                                                        <div class="col padding-lr-10 txt-color-light">
                                                                            <p class="txt-normal-s txt-medium">
                                                                                <?php the_title() ?>
                                                                            </p>
                                                                            <p class="txt-sm">
                                                                                <i class="fa fa-calendar"></i>
                                                                                <span class="padding-l-10">
                                                                                    <?php 
                                                                                        $date = strtotime( get_post_meta( $post_id, 'month_year', true ) );
                                                                                        echo date('M Y', $date);
                                                                                    ?>
                                                                                </span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                                            <a class="txt-green" data-toggle="modal" href="#projectEditMilestoneModal-<?php echo $post_id ?>">
                                                                                <i class="fa fa-pencil"></i>
                                                                                Edit
                                                                            </a>
                                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=project-edit-milestone-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                                <i class="fa fa-trash"></i>
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                    <!-- Edit Modal -->
                                                                    <div class="modal fade font-main filter-modal" id="projectEditMilestoneModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <form action="<?php echo currentUrl(true).'?form=project-edit-milestone-'.$post_id.'&project-id='.$project_id.'&post-id='.$post_id; ?>" method="post" enctype="multipart/form-data">
                                                                                    <div class="modal-header padding-lr-30">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Milestone</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body padding-o-30 form">
                                                                                        <?php

                                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'project-edit-milestone-'.$post_id ){
                                                                                                wp_delete_post($post_id); // Delete
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }

                                                                                            if( $_POST && $_GET['form'] == 'project-edit-milestone-'.$post_id ){

                                                                                                /* Get Post Data */
                                                                                                $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                                                /* Save Post to DB */
                                                                                                $post_id = wp_insert_post(array (
                                                                                                    'ID' => $post_id,
                                                                                                    'post_type' => 'project-meta',
                                                                                                    'post_title' => $post_name,
                                                                                                    'post_content' => "",
                                                                                                    'post_status' => 'publish',
                                                                                                ));

                                                                                                update_post_meta( $post_id, 'month_year', sanitize_text_field( $_POST['month_year'] ) );

                                                                                                /* Image Upload */
                                                                                                image_upload_process($post_id);

                                                                                                /* Redirect */
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }
                                                                                        ?>
                                                                                        <div class="form-item">
                                                                                            <label for="post_name">
                                                                                                Achievement title
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="post_name" 
                                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="padding-b-30">
                                                                                            <?php the_image_upload_field(); ?>
                                                                                        </div> 
                                                                                        <div class="form-item">
                                                                                            <label for="month_year">
                                                                                                Month/Year
                                                                                            </label>
                                                                                            <input 
                                                                                                type="month" 
                                                                                                name="month_year" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'month_year', true ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer padding-lr-30">
                                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <?php endwhile; ?> 

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't any updates yet.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#AddProjectMilestonesModal">Add New</a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="AddProjectMilestonesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=add-project-milestone&project-id='.$project_id; ?>" method="post" enctype="multipart/form-data">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Project Milestones</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;

                                                                    if( $_POST && $_GET['form'] == 'add-project-milestone' ){
                                                                        /* Get Post Data */
                                                                        $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                        /* Save Post to DB */
                                                                        $post_id = wp_insert_post(array (
                                                                            'ID' => $post_id,
                                                                            'post_type' => 'project-meta',
                                                                            'post_title' => $post_name,
                                                                            'post_content' => "",
                                                                            'post_status' => 'publish',
                                                                        ));

                                                                        /* Add Post Meta */
                                                                        update_post_meta( $post_id, 'parent', $project_id );
                                                                        update_post_meta( $post_id, 'meta-type', 'project-milestone' );

                                                                        update_post_meta( $post_id, 'month_year', sanitize_text_field( $_POST['month_year'] ) );

                                                                        /* Image Upload */
                                                                        image_upload_process($post_id);

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="post_name">
                                                                        Achievement title
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="post_name" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                    >
                                                                </div>
                                                                <div class="padding-b-30">
                                                                    <?php the_image_upload_field(); ?>
                                                                </div> 
                                                                <div class="form-item">
                                                                    <label for="month_year">
                                                                        Month/Year
                                                                    </label>
                                                                    <input 
                                                                        type="month" 
                                                                        name="month_year" 
                                                                        value="<?php echo get_post_meta( $post_id, 'month_year', true ); ?>"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Project Resources -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <?php
                                                $profile_query = new WP_Query();
                                                $profile_query->query( 
                                                    array(
                                                        'post_type' => 'project-meta',
                                                        'post_status' => 'publish',
                                                        'author' => $current_user->ID,
                                                        'posts_per_page' => -1,
                                                        'meta_query' => array(
                                                            array(
                                                                'key' => 'meta-type',
                                                                'value' => 'project-resource',
                                                            ),
                                                            array(
                                                                'key' => 'parent',
                                                                'value' => $project_id
                                                            )
                                                        ),
                                                    ) 
                                                );
                                            ?>
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-resource-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Project Resources</span>
                                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-resource-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What schools have you attended? When? What did you study? What degrees do you have?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                                <?php $check[] = 'complete'; ?>

                                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                                                    <?php $post_id = $post->ID; ?> 

                                                                    <!-- Entry -->
                                                                    <div class="row row-10 padding-b-20">
                                                                        <div class="col padding-lr-10 txt-color-light">
                                                                            <p class="txt-normal-s txt-medium">
                                                                                <?php the_title() ?>
                                                                            </p>
                                                                            <p class="txt-sm padding-b-5">
                                                                                <?php echo get_post_meta( $post_id, 'type', true ) ?> |
                                                                                <a href="<?php echo ''.get_post_meta( $post_id, 'url', true ); ?>" class="txt-color-blue">
                                                                                    <?php echo ''.get_post_meta( $post_id, 'url', true ); ?>
                                                                                </a>
                                                                            </p>
                                                                            <p class="txt-sm padding-b-5">
                                                                                <?php echo ''.get_post_meta( $post_id, 'summary', true ); ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                                            <a class="txt-green" data-toggle="modal" href="#projectEditResourceModal-<?php echo $post_id ?>">
                                                                                <i class="fa fa-pencil"></i>
                                                                                Edit
                                                                            </a>
                                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=project-edit-resource-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                                <i class="fa fa-trash"></i>
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                    <!-- Edit Modal -->
                                                                    <div class="modal fade font-main filter-modal" id="projectEditResourceModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <form action="<?php echo currentUrl(true).'?form=project-edit-resource-'.$post_id.'&project-id='.$project_id.'&post-id='.$post_id; ?>" method="post">
                                                                                    <div class="modal-header padding-lr-30">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Update</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body padding-o-30 form">
                                                                                        <?php

                                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'project-edit-resource-'.$post_id ){
                                                                                                wp_delete_post($post_id); // Delete
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }

                                                                                            if( $_POST && $_GET['form'] == 'project-edit-resource-'.$post_id ){

                                                                                                /* Get Post Data */
                                                                                                $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                                                /* Save Post to DB */
                                                                                                $post_id = wp_insert_post(array (
                                                                                                    'ID' => $post_id,
                                                                                                    'post_type' => 'project-meta',
                                                                                                    'post_title' => $post_name,
                                                                                                    'post_content' => "",
                                                                                                    'post_status' => 'publish',
                                                                                                ));

                                                                                                update_post_meta( $post_id, 'type', sanitize_text_field( $_POST['type'] ) );
                                                                                                update_post_meta( $post_id, 'summary', sanitize_text_field( $_POST['summary'] ) );
                                                                                                update_post_meta( $post_id, 'url', sanitize_text_field( $_POST['url'] ) );

                                                                                                /* Redirect */
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }
                                                                                        ?>
                                                                                        <div class="form-item">
                                                                                            <label for="post_name">
                                                                                                Title
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="post_name" 
                                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="type">
                                                                                                Type 
                                                                                            </label>
                                                                                            <select name="type" id="type">
                                                                                                <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                                                                                <option value="Article" <?php echo ($meta == 'Article') ? 'selected' : '' ?>>Article</option>
                                                                                                <option value="Album" <?php echo ($meta == 'Album') ? 'selected' : '' ?>>Album</option>
                                                                                                <option value="Video" <?php echo ($meta == 'Video') ? 'selected' : '' ?>>Video</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="summary">
                                                                                                Short Description of resource
                                                                                            </label>
                                                                                            <textarea name="summary" id="summary" rows="8"><?php echo get_post_meta( $post_id, 'summary', true ); ?></textarea>
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="url">
                                                                                                URL
                                                                                            </label>
                                                                                            <input 
                                                                                                type="url" 
                                                                                                name="url" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'url', true ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer padding-lr-30">
                                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <?php endwhile; ?> 

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't any updates yet.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#AddProjectResourcesModal">Add New</a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="AddProjectResourcesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=add-project-update&project-id='.$project_id; ?>" method="post">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Project Resource</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;

                                                                    if( $_POST && $_GET['form'] == 'add-project-update' ){
                                                                        /* Get Post Data */
                                                                        $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                        /* Save Post to DB */
                                                                        $post_id = wp_insert_post(array (
                                                                            'ID' => $post_id,
                                                                            'post_type' => 'project-meta',
                                                                            'post_title' => $post_name,
                                                                            'post_content' => "",
                                                                            'post_status' => 'publish',
                                                                        ));

                                                                        /* Add Post Meta */
                                                                        update_post_meta( $post_id, 'parent', $project_id );
                                                                        update_post_meta( $post_id, 'meta-type', 'project-resource' );

                                                                        update_post_meta( $post_id, 'type', sanitize_text_field( $_POST['type'] ) );
                                                                        update_post_meta( $post_id, 'url', sanitize_text_field( $_POST['url'] ) );
                                                                        update_post_meta( $post_id, 'summary', sanitize_text_field( $_POST['summary'] ) );

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="post_name">
                                                                        Resource Title
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="post_name" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="type">
                                                                        Resource Type 
                                                                    </label>
                                                                    <select name="type" id="type">
                                                                        <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                                                        <option value="Article" <?php echo ($meta == 'Article') ? 'selected' : '' ?>>Article</option>
                                                                        <option value="Album" <?php echo ($meta == 'Album') ? 'selected' : '' ?>>Album</option>
                                                                        <option value="Video" <?php echo ($meta == 'Video') ? 'selected' : '' ?>>Video</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="summary">
                                                                        Short Description of resource
                                                                    </label>
                                                                    <textarea name="summary" id="summary" rows="8"><?php echo get_post_meta( $post_id, 'summary', true ); ?></textarea>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="url">
                                                                        URL
                                                                    </label>
                                                                    <input 
                                                                        type="url" 
                                                                        name="url" 
                                                                        value="<?php echo get_post_meta( $post_id, 'url', true ); ?>"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Project Steps -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <?php
                                                $profile_query = new WP_Query();
                                                $profile_query->query( 
                                                    array(
                                                        'post_type' => 'project-meta',
                                                        'post_status' => 'publish',
                                                        'author' => $current_user->ID,
                                                        'posts_per_page' => -1,
                                                        'orderby' => 'meta_value_num',
                                                        'meta_key'  => 'step_number',
                                                        'order' => 'ASC',
                                                        'meta_query' => array(
                                                            array(
                                                                'key' => 'meta-type',
                                                                'value' => 'project-step',
                                                            ),
                                                            array(
                                                                'key' => 'parent',
                                                                'value' => $project_id
                                                            )
                                                        ),
                                                    ) 
                                                );
                                            ?>
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-step-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Steps to take for anyone to Implement this Project in their local community </span>
                                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-step-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What schools have you attended? When? What did you study? What degrees do you have?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                                <?php $check[] = 'complete'; ?>

                                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                                                    <?php $post_id = $post->ID; ?> 

                                                                    <!-- Entry -->
                                                                    <div class="row row-10 padding-b-20">
                                                                        <div class="col padding-lr-10 txt-color-light">
                                                                            <p class="txt-normal-s txt-medium padding-b-15">
                                                                                <span class="txt-bold padding-r-10">
                                                                                    <?php echo ''.get_post_meta( $post_id, 'step_number', true ); ?>
                                                                                </span>
                                                                                <span><?php the_title() ?></span>
                                                                            </p>
                                                                            <div class="padding-b-15">
                                                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                                    Summary        
                                                                                </p>
                                                                                <p class="txt-sm">
                                                                                    <?php echo ''.get_post_meta( $post_id, 'summary', true ); ?>
                                                                                </p>
                                                                            </div>
                                                                            <div>
                                                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                                                    Description        
                                                                                </p>
                                                                                <p class="txt-sm">
                                                                                    <?php echo ''.get_post_meta( $post_id, 'description', true ); ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                                            <a class="txt-green" data-toggle="modal" href="#projectEditStepModal-<?php echo $post_id ?>">
                                                                                <i class="fa fa-pencil"></i>
                                                                                Edit
                                                                            </a>
                                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=project-edit-step-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                                <i class="fa fa-trash"></i>
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                    <!-- Edit Modal -->
                                                                    <div class="modal fade font-main filter-modal" id="projectEditStepModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <form action="<?php echo currentUrl(true).'?form=project-edit-step-'.$post_id.'&project-id='.$project_id.'&post-id='.$post_id; ?>" method="post" enctype="multipart/form-data">
                                                                                    <div class="modal-header padding-lr-30">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Step</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body padding-o-30 form">
                                                                                        <?php

                                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'project-edit-step-'.$post_id ){
                                                                                                wp_delete_post($post_id); // Delete
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }

                                                                                            if( $_POST && $_GET['form'] == 'project-edit-step-'.$post_id ){

                                                                                                /* Get Post Data */
                                                                                                $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                                                /* Save Post to DB */
                                                                                                $post_id = wp_insert_post(array (
                                                                                                    'ID' => $post_id,
                                                                                                    'post_type' => 'project-meta',
                                                                                                    'post_title' => $post_name,
                                                                                                    'post_content' => "",
                                                                                                    'post_status' => 'publish',
                                                                                                ));

                                                                                                update_post_meta( $post_id, 'step_number', sanitize_text_field( $_POST['step_number'] ) );
                                                                                                update_post_meta( $post_id, 'summary', wp_kses_post( $_POST['summary'] ) );
                                                                                                update_post_meta( $post_id, 'description', wp_kses_post( $_POST['description'] ) );

                                                                                                /* Image Upload */
                                                                                                image_upload_process($post_id);

                                                                                                /* Redirect */
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }
                                                                                        ?>
                                                                                        <div class="form-item">
                                                                                            <label for="step_number">
                                                                                                Step Number
                                                                                            </label>
                                                                                            <input 
                                                                                                type="number" 
                                                                                                name="step_number" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'step_number', true ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="post_name">
                                                                                                Title
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="post_name" 
                                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="padding-b-30">
                                                                                            <?php the_image_upload_field(); ?>
                                                                                        </div> 
                                                                                        <div class="form-item">
                                                                                            <label for="summary">
                                                                                                Summary
                                                                                            </label>
                                                                                            <textarea name="summary" id="summary" rows="8"><?php echo get_post_meta( $post_id, 'summary', true ); ?></textarea>
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="description">
                                                                                                Full Description
                                                                                            </label>
                                                                                            <textarea name="description" id="description" rows="8"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer padding-lr-30">
                                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <?php endwhile; ?> 

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't any steps yet.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#AddProjectStepsModal">Add New</a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="AddProjectStepsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=add-project-step&project-id='.$project_id; ?>" method="post" enctype="multipart/form-data">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Step to take for anyone to Implement this Project in their local community </h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;

                                                                    if( $_POST && $_GET['form'] == 'add-project-step' ){
                                                                        /* Get Post Data */
                                                                        $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                        /* Save Post to DB */
                                                                        $post_id = wp_insert_post(array (
                                                                            'ID' => $post_id,
                                                                            'post_type' => 'project-meta',
                                                                            'post_title' => $post_name,
                                                                            'post_content' => "",
                                                                            'post_status' => 'publish',
                                                                        ));

                                                                        /* Add Post Meta */
                                                                        update_post_meta( $post_id, 'parent', $project_id );
                                                                        update_post_meta( $post_id, 'meta-type', 'project-step' );

                                                                        update_post_meta( $post_id, 'step_number', sanitize_text_field( $_POST['step_number'] ) );
                                                                        update_post_meta( $post_id, 'summary', wp_kses_post( $_POST['summary'] ) );
                                                                        update_post_meta( $post_id, 'description', wp_kses_post( $_POST['description'] ) );

                                                                        /* Image Upload */
                                                                        image_upload_process($post_id);

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="step_number">
                                                                        Step Number
                                                                    </label>
                                                                    <input 
                                                                        type="number" 
                                                                        name="step_number" 
                                                                        value="<?php echo get_post_meta( $post_id, 'step_number', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="post_name">
                                                                        Title
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="post_name" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                    >
                                                                </div>
                                                                <div class="padding-b-30">
                                                                    <?php the_image_upload_field(); ?>
                                                                </div> 
                                                                <div class="form-item">
                                                                    <label for="summary">
                                                                        Summary
                                                                    </label>
                                                                    <textarea name="summary" id="summary" rows="8"><?php echo get_post_meta( $post_id, 'summary', true ); ?></textarea>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="description">
                                                                        Full Description
                                                                    </label>
                                                                    <textarea name="description" id="description" rows="8"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Request for help -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <?php
                                                $profile_query = new WP_Query();
                                                $profile_query->query( 
                                                    array(
                                                        'post_type' => 'project-meta',
                                                        'post_status' => 'publish',
                                                        'author' => $current_user->ID,
                                                        'posts_per_page' => -1,
                                                        'meta_query' => array(
                                                            array(
                                                                'key' => 'meta-type',
                                                                'value' => 'project-request',
                                                            ),
                                                            array(
                                                                'key' => 'parent',
                                                                'value' => $project_id
                                                            )
                                                        ),
                                                    ) 
                                                );
                                            ?>
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-request-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Request for help</span>
                                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-request-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What schools have you attended? When? What did you study? What degrees do you have?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                                <?php $check[] = 'complete'; ?>

                                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                                                    <?php $post_id = $post->ID; ?> 

                                                                    <!-- Entry -->
                                                                    <div class="row row-10 padding-b-20">
                                                                        <div class="col padding-lr-10 txt-color-light">
                                                                            <p class="txt-normal-s txt-medium">
                                                                                <?php the_title() ?>
                                                                            </p>
                                                                            <p class="txt-sm txt-italics padding-b-10">
                                                                                <?php echo get_post_meta( $post_id, 'type', true ) ?>
                                                                            </p>
                                                                            <p class="txt-sm padding-b-5">
                                                                                <?php echo ''.get_post_meta( $post_id, 'description', true ); ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                                            <a class="txt-green" data-toggle="modal" href="#projectEditRequestModal-<?php echo $post_id ?>">
                                                                                <i class="fa fa-pencil"></i>
                                                                                Edit
                                                                            </a>
                                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=project-edit-request-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                                <i class="fa fa-trash"></i>
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                    <!-- Edit Modal -->
                                                                    <div class="modal fade font-main filter-modal" id="projectEditRequestModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <form action="<?php echo currentUrl(true).'?form=project-edit-request-'.$post_id.'&project-id='.$project_id.'&post-id='.$post_id; ?>" method="post">
                                                                                    <div class="modal-header padding-lr-30">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Update</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body padding-o-30 form">
                                                                                        <?php

                                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'project-edit-request-'.$post_id ){
                                                                                                wp_delete_post($post_id); // Delete
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }

                                                                                            if( $_POST && $_GET['form'] == 'project-edit-request-'.$post_id ){

                                                                                                /* Get Post Data */
                                                                                                $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                                                /* Save Post to DB */
                                                                                                $post_id = wp_insert_post(array (
                                                                                                    'ID' => $post_id,
                                                                                                    'post_type' => 'project-meta',
                                                                                                    'post_title' => $post_name,
                                                                                                    'post_content' => "",
                                                                                                    'post_status' => 'publish',
                                                                                                ));

                                                                                                update_post_meta( $post_id, 'type', sanitize_text_field( $_POST['type'] ) );
                                                                                                update_post_meta( $post_id, 'description', sanitize_text_field( $_POST['description'] ) );

                                                                                                /* Redirect */
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }
                                                                                        ?>
                                                                                        <div class="form-item">
                                                                                            <label for="post_name">
                                                                                                Title
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="post_name" 
                                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="type">
                                                                                                Type of Request 
                                                                                            </label>
                                                                                            <select name="type" id="type">
                                                                                                <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                                                                                <option value="Volunteer (Expert)" <?php echo ($meta == 'Volunteer (Expert)') ? 'selected' : '' ?>>Volunteer (Expert)</option>
                                                                                                <option value="Volunteer (Admin Support)" <?php echo ($meta == 'Volunteer (Admin Support)') ? 'selected' : '' ?>>Volunteer (Admin Support)</option>
                                                                                                <option value="Donation" <?php echo ($meta == 'Donation') ? 'selected' : '' ?>>Donation</option>
                                                                                                <option value="Tool Request" <?php echo ($meta == 'Tool Request') ? 'selected' : '' ?>>Tool Request</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="description">
                                                                                                Describe your request
                                                                                            </label>
                                                                                            <textarea name="description" id="description" rows="8"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer padding-lr-30">
                                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <?php endwhile; ?> 

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't added any requests yet.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#AddProjectRequestsModal">Add New</a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="AddProjectRequestsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=add-project-request&project-id='.$project_id; ?>" method="post">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Add a Help Request</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;

                                                                    if( $_POST && $_GET['form'] == 'add-project-request' ){
                                                                        /* Get Post Data */
                                                                        $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                        /* Save Post to DB */
                                                                        $post_id = wp_insert_post(array (
                                                                            'ID' => $post_id,
                                                                            'post_type' => 'project-meta',
                                                                            'post_title' => $post_name,
                                                                            'post_content' => "",
                                                                            'post_status' => 'publish',
                                                                        ));

                                                                        /* Add Post Meta */
                                                                        update_post_meta( $post_id, 'parent', $project_id );
                                                                        update_post_meta( $post_id, 'meta-type', 'project-request' );

                                                                        update_post_meta( $post_id, 'type', sanitize_text_field( $_POST['type'] ) );
                                                                        update_post_meta( $post_id, 'description', sanitize_text_field( $_POST['description'] ) );

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="post_name">
                                                                        Title
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="post_name" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="type">
                                                                        Type of Request 
                                                                    </label>
                                                                    <select name="type" id="type">
                                                                        <?php $meta = get_post_meta( $post_id, 'gender', true ); ?>
                                                                        <option value="Volunteer (Expert)" <?php echo ($meta == 'Volunteer (Expert)') ? 'selected' : '' ?>>Volunteer (Expert)</option>
                                                                        <option value="Volunteer (Admin Support)" <?php echo ($meta == 'Volunteer (Admin Support)') ? 'selected' : '' ?>>Volunteer (Admin Support)</option>
                                                                        <option value="Donation" <?php echo ($meta == 'Donation') ? 'selected' : '' ?>>Donation</option>
                                                                        <option value="Tool Request" <?php echo ($meta == 'Tool Request') ? 'selected' : '' ?>>Tool Request</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="description">
                                                                        Describe your request
                                                                    </label>
                                                                    <textarea name="description" id="description" rows="8"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Project Team  -->
                                        <div class="bg-white padding-tb-20 padding-l-20 padding-r-30 margin-b-10">
                                            <?php
                                                $profile_query = new WP_Query();
                                                $profile_query->query( 
                                                    array(
                                                        'post_type' => 'project-meta',
                                                        'post_status' => 'publish',
                                                        'author' => $current_user->ID,
                                                        'posts_per_page' => -1,
                                                        'meta_query' => array(
                                                            array(
                                                                'key' => 'meta-type',
                                                                'value' => 'project-team',
                                                            ),
                                                            array(
                                                                'key' => 'parent',
                                                                'value' => $project_id
                                                            )
                                                        ),
                                                    ) 
                                                );
                                            ?>
                                            <div class="profile-collapse padding">
                                                <button class="faq-btn" type="button" data-toggle="collapse" data-target="#project-team-collapse" aria-expanded="false" aria-controls="collapseExample">
                                                    <span class="d-flex align-items-center justify-content-between margin-l-10">
                                                        <span class="txt-medium txt-color-light">Project Team</span>
                                                        <?php if( $profile_query->found_posts > 0 ){ ?>
                                                        <i class="fa fa-check-circle txt-xlg txt-color-green"></i>
                                                        <?php }else{ ?>
                                                        <i class="fa fa-times-circle txt-xlg txt-color-red"></i>
                                                        <?php } ?>
                                                    </span>
                                                </button>
                                                <div class="collapse" id="project-team-collapse">
                                                    <div class="padding-lr-30">
                                                        <p class="txt-sm txt-light padding-t-10">What schools have you attended? When? What did you study? What degrees do you have?</p>
                                                        <article class="padding-t-20 margin-t-20 border-t-1 border-color-lighter">
                                                            <?php if ( $profile_query->have_posts() ) { ?>
                                                                <?php $check[] = 'complete'; ?>

                                                                <?php while ($profile_query->have_posts()) : $profile_query->the_post(); ?>

                                                                    <?php $post_id = $post->ID; ?> 

                                                                    <!-- Entry -->
                                                                    <div class="row row-10 padding-b-20">
                                                                        <div class="col padding-lr-10 txt-color-light">
                                                                            <p class="txt-medium padding-b-10">
                                                                                <span><?php the_title() ?></span> -
                                                                                <span class="txt-normal-s txt-italics"><?php echo get_post_meta( $post_id, 'role', true ); ?></span>
                                                                            </p>
                                                                            <div class="row row-10 padding-b-20 txt-color-light">
                                                                                <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                                                                    <p class="txt-sm txt-medium">
                                                                                        <i class="fa fa-envelope-o"></i>
                                                                                        <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'email', true ); ?></span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                                                                    <p class="txt-sm txt-medium">
                                                                                        <i class="fa fa-twitter"></i>
                                                                                        <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'twitter', true ); ?></span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                                                                    <p class="txt-sm txt-medium">
                                                                                        <i class="fa fa-linkedin"></i>
                                                                                        <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'linkedin', true ); ?></span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="col-6 col-md-3 padding-lr-10 padding-b-10">
                                                                                    <p class="txt-sm txt-medium">
                                                                                        <i class="fa fa-instagram"></i>
                                                                                        <span class="padding-l-5"><?php echo get_post_meta( $post_id, 'instagram', true ); ?></span>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <p class="txt-sm txt-italics padding-b-10">
                                                                                <?php echo get_post_meta( $post_id, 'type', true ) ?>
                                                                            </p>
                                                                            <p class="txt-sm padding-b-5">
                                                                                <?php echo ''.get_post_meta( $post_id, 'description', true ); ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-auto padding-lr-10 text-right txt-sm">
                                                                            <a class="txt-green" data-toggle="modal" href="#projectEditTeamModal-<?php echo $post_id ?>">
                                                                                <i class="fa fa-pencil"></i>
                                                                                Edit
                                                                            </a>
                                                                            <a href="<?php echo currentUrl(false).'&action=delete&form=project-edit-team-'.$post_id; ?>" class="txt-color-red margin-l-10 confirm-delete">
                                                                                <i class="fa fa-trash"></i>
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                    <!-- Edit Modal -->
                                                                    <div class="modal fade font-main filter-modal" id="projectEditTeamModal-<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <form action="<?php echo currentUrl(true).'?form=project-edit-team-'.$post_id.'&project-id='.$project_id.'&post-id='.$post_id; ?>" method="post">
                                                                                    <div class="modal-header padding-lr-30">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Team Member</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body padding-o-30 form">
                                                                                        <?php

                                                                                            if( $_GET['action'] == 'delete' && $_GET['form'] == 'project-edit-team-'.$post_id ){
                                                                                                wp_delete_post($post_id); // Delete
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }

                                                                                            if( $_POST && $_GET['form'] == 'project-edit-team-'.$post_id ){

                                                                                                /* Get Post Data */
                                                                                                $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                                                /* Save Post to DB */
                                                                                                $post_id = wp_insert_post(array (
                                                                                                    'ID' => $post_id,
                                                                                                    'post_type' => 'project-meta',
                                                                                                    'post_title' => $post_name,
                                                                                                    'post_content' => "",
                                                                                                    'post_status' => 'publish',
                                                                                                ));

                                                                                                update_post_meta( $post_id, 'role', sanitize_text_field( $_POST['role'] ) );
                                                                                                update_post_meta( $post_id, 'email', sanitize_text_field( $_POST['email'] ) );
                                                                                                update_post_meta( $post_id, 'twitter', sanitize_text_field( $_POST['twitter'] ) );
                                                                                                update_post_meta( $post_id, 'linkedin', sanitize_text_field( $_POST['linkedin'] ) );
                                                                                                update_post_meta( $post_id, 'instagram', sanitize_text_field( $_POST['instagram'] ) );

                                                                                                /* Redirect */
                                                                                                printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                                            }
                                                                                        ?>
                                                                                        <div class="form-item">
                                                                                            <label for="post_name">
                                                                                                Name
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="post_name" 
                                                                                                class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="role">
                                                                                                Role
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="role" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'role', role ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="email">
                                                                                                Email Address
                                                                                            </label>
                                                                                            <input 
                                                                                                type="email" 
                                                                                                name="email" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'email', true ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="twitter">
                                                                                                Twitter Handle
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="twitter" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'twitter', true ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="linkedin">
                                                                                                LinkedIn Bio
                                                                                            </label>
                                                                                            <input 
                                                                                                type="url" 
                                                                                                name="linkedin" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'linkedin', true ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                        <div class="form-item">
                                                                                            <label for="instagram">
                                                                                                Instagram Handle
                                                                                            </label>
                                                                                            <input 
                                                                                                type="text" 
                                                                                                name="instagram" 
                                                                                                value="<?php echo get_post_meta( $post_id, 'instagram', true ); ?>"
                                                                                            >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer padding-lr-30">
                                                                                        <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <?php endwhile; ?> 

                                                            <?php }else{ ?> 
                                                                <?php $check[] = 'incomplete'; ?> 
                                                                <p class="txt-normal-s padding-b-20">
                                                                    You haven't added any team members yet.
                                                                </p>
                                                            <?php } ?>
                                                        </article>
                                                        <!--CTA-->
                                                        <article class="padding-t-20 border-t-1 border-color-lighter">
                                                            <a class="btn btn-application-manager no-m-b txt-xs" data-toggle="modal" href="#AddProjectTeamModal">Add New</a>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Add Modal -->
                                            <div class="modal fade font-main filter-modal" id="AddProjectTeamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?php echo currentUrl(true).'?form=add-project-team&project-id='.$project_id; ?>" method="post">
                                                            <div class="modal-header padding-lr-30">
                                                                <h5 class="modal-title" id="exampleModalLabel">Add a Team Member</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body padding-o-30 form">
                                                                <?php
                                                                    $post_id = 0;

                                                                    if( $_POST && $_GET['form'] == 'add-project-team' ){
                                                                        /* Get Post Data */
                                                                        $post_name = sanitize_text_field( $_POST['post_name'] );

                                                                        /* Save Post to DB */
                                                                        $post_id = wp_insert_post(array (
                                                                            'ID' => $post_id,
                                                                            'post_type' => 'project-meta',
                                                                            'post_title' => $post_name,
                                                                            'post_content' => "",
                                                                            'post_status' => 'publish',
                                                                        ));

                                                                        /* Add Post Meta */
                                                                        update_post_meta( $post_id, 'parent', $project_id );
                                                                        update_post_meta( $post_id, 'meta-type', 'project-team' );

                                                                        update_post_meta( $post_id, 'role', sanitize_text_field( $_POST['role'] ) );
                                                                        update_post_meta( $post_id, 'email', sanitize_text_field( $_POST['email'] ) );
                                                                        update_post_meta( $post_id, 'twitter', sanitize_text_field( $_POST['twitter'] ) );
                                                                        update_post_meta( $post_id, 'linkedin', sanitize_text_field( $_POST['linkedin'] ) );
                                                                        update_post_meta( $post_id, 'instagram', sanitize_text_field( $_POST['instagram'] ) );

                                                                        /* Redirect */
                                                                        printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$project_id );
                                                                    }
                                                                ?>
                                                                <div class="form-item">
                                                                    <label for="post_name">
                                                                        Name
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="post_name" 
                                                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                                                        value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="role">
                                                                        Role
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="role" 
                                                                        value="<?php echo get_post_meta( $post_id, 'role', role ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="email">
                                                                        Email Address
                                                                    </label>
                                                                    <input 
                                                                        type="email" 
                                                                        name="email" 
                                                                        value="<?php echo get_post_meta( $post_id, 'email', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="twitter">
                                                                        Twitter Handle
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="twitter" 
                                                                        value="<?php echo get_post_meta( $post_id, 'twitter', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="linkedin">
                                                                        LinkedIn Bio
                                                                    </label>
                                                                    <input 
                                                                        type="url" 
                                                                        name="linkedin" 
                                                                        value="<?php echo get_post_meta( $post_id, 'linkedin', true ); ?>"
                                                                    >
                                                                </div>
                                                                <div class="form-item">
                                                                    <label for="instagram">
                                                                        Instagram Handle
                                                                    </label>
                                                                    <input 
                                                                        type="text" 
                                                                        name="instagram" 
                                                                        value="<?php echo get_post_meta( $post_id, 'instagram', true ); ?>"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer padding-lr-30">
                                                                <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                                                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!-- Publication -->
                                    <div>
                                        <?php if (in_array("incomplete", $check)){ ?>

                                            <!-- Incomplete Info -->   
                                            <section class="padding-t-20 padding-b-40">
                                                <div class="bg-ash row align-items-center padding-o-30">
                                                    <div class="col-auto ">
                                                        <span class="fa-stack">
                                                            <i class="fa fa-circle fa-stack-2x txt-color-red"></i>
                                                            <i class="fa fa-times fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </div>
                                                    <p class="col txt-sm txt-color-white padding-l-15">You have uncompleted sections marked with this symbol. You must complete those section before you will be able to submit your project.</p>
                                                </div>
                                            </section>

                                        <?php } else { ?>

                                            <!-- Submit-->   
                                            <section class="padding-t-20 padding-b-40 text-center">
                                                <a href="<?php echo currentUrl(true).'?form=publish&project-id='.$project_id; ?>" class="btn btn-orange">Submit</a>
                                            </section>

                                        <?php } ?>

                                    <?php } ?>
                                </div>

                                <?php }else{ ?>
                                
                                <!-- New Project -->
                                <section class="padding-t-20 padding-b-40">
                                    <div class="bg-ash txt-color-white text-center padding-o-40">
                                        <h3 class="txt-xxlg txt-height-1-2 padding-b-20">
                                            Start by creating a project
                                        </h3>
                                        <p class="padding-b-40">
                                            Tel us what your project is about
                                        </p>
                                        <p class="">
                                            <a class="btn btn-orange no-m-b txt-normal-s" data-toggle="modal" href="#createProjectModal">
                                                Create a New Project
                                            </a>
                                        </p>
                                    </div>
                                </section>
                                                                
                            <?php } ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Add Project Modal -->
        <div class="modal fade font-main filter-modal" id="createProjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="<?php echo currentUrl(true).'?form=add-project'; echo ( isset($_GET['project-id']) ) ? '&project-id='.$_GET['project-id'] : ''; ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-header padding-lr-30">
                            <h5 class="modal-title" id="exampleModalLabel">Create Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body padding-o-30 form">
                            <?php
                                $post_id = 0;
                                $post = get_post( $_GET['project-id'] );
                                if( $_GET['project-id'] && $_GET['project-id'] == $post->ID ){
                                    $post_id = $_GET['project-id'] ;
                                }

                                $images = get_attached_media( 'image', $post_id );

                                if($images){
                                    foreach($images as $image) { 
                                        $previousImg_id = $image->ID;
                                    }
                                }


                                if( $_POST && $_GET['form'] == 'add-project' ){

                                    /* Get Post Name */
                                    $postName = sanitize_text_field( $_POST['post_name'] ); 

                                    /* Meta */
                                    $summary = wp_kses_post( $_POST['summary'] ); 
                                    $thematic_areas = $_POST['thematic_areas']; 
                                    $sdgs = $_POST['sdgs']; 

                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => 'personal-project',
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));
                                    
                                    $project_id = $post_id;
                                    
                                    /* Image Upload */
                                    image_upload_process($post_id);

                                    /* publication status */
                                    if(!get_post_meta( $post_id, 'publication_status', true )){
                                        update_post_meta( $post_id, 'publication_status', 'unpublished' );
                                    }

                                    update_post_meta( $post_id, 'summary', $summary );
                                    
                                    delete_post_meta($post_id, 'thematic_areas');
                                    foreach( $thematic_areas as $thematic_area ){
                                        add_post_meta( $post_id, 'thematic_areas', $thematic_area, false );
                                    }
                                    
                                    update_post_meta( $post_id, 'sdgs', $sdgs );

                                    

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true).'?project-id='.$post_id );
                                }
                            ?>
                            <div class="form-item">
                                <label for="post_name">
                                    Project Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post_name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                                >
                            </div>
                            <div class="form-item">
                                <label for="summary">
                                    Short summary about the project
                                </label>
                                <textarea id="summary" name="summary" rows="8"><?php echo get_post_meta( $post_id, 'summary', true ); ?></textarea>
                            </div>
                            <div class="form-item">
                                <label class="d-block padding-b-10">
                                    Which of these Focus areas does your project align with? 
                                </label>
                                <?php
                                    $list = get_post_meta( $post_id, 'thematic_areas', false );

                                    if ( $thematic_areas_query->have_posts() ) {

                                        while ($thematic_areas_query->have_posts()) : $thematic_areas_query->the_post();
                                ?>
                                <label class="txt-sm d-flex align-items-center padding-b-10 padding-r-15">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="<?php echo $post->ID ?>" 
                                        name="thematic_areas[]" 
                                        <?php echo in_array($post->ID, $list) ? "checked" : "" ?>
                                    >
                                    <span class="font-weight-normal padding-l-10">
                                        <?php the_title() ?>
                                    </span>
                                </label>
                                <?php endwhile; ?>
                                <?php } ?>
                            </div>
                            <div class="form-item">
                                <label class="d-block padding-b-10">
                                    Which of the SDGs is this project related to?
                                </label>
                                <?php
                                    $list = get_post_meta( $post_id, 'sdgs', true );

                                    if ( $sdgs_query->have_posts() ) {

                                        while ($sdgs_query->have_posts()) : $sdgs_query->the_post();
                                ?>
                                <label class="txt-sm d-flex align-items-center padding-b-10 padding-r-15">
                                    <input
                                        class="custom-check"
                                        type="checkbox" 
                                        value="<?php echo $post->ID ?>" 
                                        name="sdgs[]" 
                                        <?php echo in_array($post->ID, $list) ? "checked" : "" ?>
                                    >
                                    <span class="font-weight-normal padding-l-10">
                                        <?php the_title() ?>
                                    </span>
                                </label>
                                <?php endwhile; ?>
                                <?php } ?>
                            </div>
                            <div>
                                <?php the_image_upload_field($project_id); ?>
                            </div> 
                        </div>
                        <div class="modal-footer padding-lr-30">
                            <button type="button" class="btn btn-white txt-xs" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    
<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('input.custom-check').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>  
<script>
    /*function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#featuredImagePreview').css('background-image', 'url(' + e.target.result + ')').text("");
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#my_image_upload").change(function() {
        readURL(this);
    });*/

    $(".my_image_upload").change(function() {
        var preview = $(this).siblings('.featuredImagePreview');
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(preview).css('background-image', 'url(' + e.target.result + ')');
            }

            reader.readAsDataURL(this.files[0]);
          }
    });
</script>
    
<?php get_footer() ?>