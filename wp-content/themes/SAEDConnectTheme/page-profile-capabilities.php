<?php /*Template Name: Profile - Capabilities*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        Capability Profile         
                    </h1>
                    <div class="cta">
                        <a href="<?php printf("https://www.saedconnect.org/service-marketplace/my-capabilities/?view=form&form-title=%s", $title); ?>" class="cta-btn">
                            Add Capability
                        </a>
                    </div>
                </div>
                <article class="page-summary">
                    <p>
                        These are the abilities you possess. These would help you get offers or gigs.
                    </p>
                </article>
                
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                <?php if( $_REQUEST['gf-id'] ){ ?>
                   
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/service-marketplace/my-capabilities/" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>

                <?php } else { ?>

                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/service-marketplace/my-capabilities/" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">   
                        <form action="https://www.saedconnect.org/service-marketplace/my-capabilities/?view=form" method="post">
                            <?php
                                /* Meta Key */
                                $meta_key = 'capability_profile';
                                $tax_type = 'capability';
                                $redirect_link = 'https://www.saedconnect.org/service-marketplace/my-capabilities/';
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if($_POST){
                                    /* Serialize Form Submission */
                                    $serialized_data = maybe_serialize($_POST);

                                    /* Save usermeta */
                                    update_user_meta( $current_user->ID, $meta_key, $serialized_data);
                                    
                                    /* Update Mailing List */
                                    /* Data */
                                    $api_key = '0fkxtL9crlJFX7cqCXUV';
                                    $master_list = 'py763E5sO36V5fa9WGjoUK4Q'; //Master
                                    $email = $current_user->user_email;
                                    
                                    $email_selection;
                                    
                                    $a = array($_POST);
                                    $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($a));
                                    
                                    $it_count = iterator_count($it);
                                    
                                    foreach($it as $key => $v) {
                                        
                                        $saved_term = get_term_by('id', $v, $tax_type);
                                        $email_selection .= $saved_term->name;
                                        
                                        if( $key < $it_count - 1 ){
                                            $email_selection .= ", ";
                                        }
                                    }                                    

                                    /* Endpoints */
                                    $subscribe_url = 'http://maylbox.com/subscribe';

                                    /* Subscribe Body */
                                    $body = array(
                                        'Capabilities' => $email_selection,
                                        'email' => $email,
                                        'list' => $master_list,
                                        'boolean' => 'true'
                                    );

                                    $args = array(
                                        'body' => $body,
                                        'timeout' => '5',
                                        'redirection' => '5',
                                        'httpversion' => '1.0',
                                        'blocking' => true,
                                        'headers' => array(),
                                        'cookies' => array()
                                    );

                                    $response = wp_remote_post( $subscribe_url, $args );
                                    $http_code = wp_remote_retrieve_response_code( $response );
                                    $body = wp_remote_retrieve_body( $response );
                                    /* Update Mailing List End */

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }

                                /* Get User Meta to populate Form */
                                $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, $meta_key, true) );

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type, array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <div class="margin-b-20">
                                <?php if( $parent == 0 ){ ?>

                                    <div class="txt-normal-s txt-medium txt-color-dark margin-b-15">
                                        <?php echo $term_name; ?>
                                    </div>
                                    <?php
                                        foreach ($terms as $child_term) {
                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $child_parent = $child_term->parent;
                                            $child_term_id = $child_term->term_id; //Get the term ID
                                            $child_term_name = $child_term->name; //Get the term name

                                            if( $child_parent == $term_id ){
                                    ?>
                                            <label class="txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                <input
                                                    class="margin-r-5"
                                                    type="checkbox" 
                                                    value="<?php echo $child_term_id ?>" 
                                                    name="<?php echo $term_id ?>[]" 
                                                    <?php echo in_array($child_term_id, $unserialized_data[$term_id]) ? "checked" : "" ?>
                                                >
                                                <?php echo $child_term_name; ?>
                                            </label>
                                    <?php
                                            }
                                        }
                                    ?>
                                <?php } ?>
                                </div>

                            <?php 
                                } 
                            ?>
                            <div class="text-right">
                                <input type="submit" class="btn btn-yellow-dark txt-xs">
                            </div>
                        </form>
                    </div>

                <?php } ?>                    
            </div>      

            <?php } else { //Empty Profile Message ?>

                <!-- Training Area Competency -->
                <div class="section-wrapper padding-b-10">
                <?php
                    $title = 'Capabilities';

                    $competency_gf_id = 12; //Form ID
                    $competency_gv_id = 81; //Gravity View ID

                    $entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get Competency Entries */
                    $competency_entries = GFAPI::get_entries( $competency_gf_id, $search_criteria );

                    /* Get Competency Entry Count */
                    $competency_entry_count = GFAPI::count_entries( $competency_gf_id, $search_criteria );



                    /* Get User Meta to populate Form */
                    $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, 'capability_profile', true) );

                ?>

                    <?php if(!$unserialized_data){ //If no entry ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                My <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Empty! Start by Adding your <?php echo $title ?>.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-career-purple txt-xxs no-m-b"
                                    href="<?php printf("https://www.saedconnect.org/service-marketplace/my-capabilities/?view=form&form-title=%s", $title); ?>"
                                >
                                    Add Competencies
                                </a>
                            </div>
                        </div>                            

                    <?php } else { ?>

                            <?php
                                foreach($unserialized_data as $key => $subterms){
                                    $term = get_term($key);
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <div class="entry">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-15">
                                        <?php echo $term_name ?>      
                                    </p>
                                        <?php 
                                            foreach($subterms as $subterm){
                                                $child_term = get_term($subterm);
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name
                                        ?>

                                    <div class="row bg-ghostwhite padding-o-10 margin-b-10">
                                        <div class="col-5">
                                            <p class="txt-sm txt-color-dark txt-medium">
                                                <?php echo $child_term_name ?>
                                            </p>                                 
                                        </div>
                                        <p class="col-7 txt-xs text-right">
                                        <?php 
                                            $itExist = false;

                                            foreach( $competency_entries as $competency_entry ){ 
                                                if(rgar( $competency_entry, 1 ) == trim($child_term_id)){
                                                    $itExist = true;
                                                }
                                            }

                                            if(!$itExist){
                                        ?>
                                           <a href="<?php printf("https://www.saedconnect.org/service-marketplace/my-capabilities/?view=form&gf-id=12&form-title=%s Description&parent_id=%s", $child_term_name, trim($child_term_id)); ?>"class="">
                                               <i class="fa fa-plus"></i>
                                               Add Description
                                           </a>
                                        <?php } ?>
                                        </p>

                                            <?php 
                                                foreach( $competency_entries as $competency_entry ){ 
                                                    if(rgar( $competency_entry, 1 ) == $child_term_id){
                                            ?>
                                        <div class="col-12 padding-t-10 margin-t-10 border-t-1">
                                            <div class="row row-10">
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What is your experience in this subject?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 2 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        Number of Years of Experience     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 3 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What training models are you open to providing for this subject area?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php
                                                            $field_id = 4; // Update this number to your field id number
                                                            $field = RGFormsModel::get_field( $competency_gf_id, $field_id );
                                                            $value = is_object( $field ) ? $field->get_value_export( $competency_entry ) : '';
                                                            echo $value;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10 padding-t-20 padding-b-10">
                                                    <a href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$competency_entry['id'].'" view_id="'.$competency_gv_id.'" action="edit" return="url" /]').'&view=trainer-profile' ?>" class="edit-btn">
                                                       <i class="fa fa-pencil"></i>
                                                       Edit Description
                                                   </a>
                                                </div>
                                            </div>
                                        </div>
                                            <?php 
                                                    }
                                                }
                                            ?>

                                    </div>

                                        <?php } ?>
                                </div>

                            <?php } ?>


                    <?php } ?>
                </div>   

            <?php } ?>   
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>