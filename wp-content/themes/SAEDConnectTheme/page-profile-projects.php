<?php /*Template Name: Profile - Projects*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Publication */
        $publication_key   = 'publication_status';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
                
                <?php if($_GET['view'] == 'form'){ //Display form  ?>
                
                
                <?php if( $_GET['form'] = 'project' ){ //GF View ?>
                   
                    <div class="page-header">
                        <h1 class="page-title">
                            Create / Edit a Project         
                        </h1>
                        <div class="cta">
                            <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                                Cancel
                            </a>
                        </div>
                    </div>
                    <article class="page-summary">
                        <p>
                            These are the abilities you possess. These would help you get offers or gigs.
                        </p>
                    </article>
                    <form class="form" action="<?php echo currenturl(false) ?>" method="post">
                        <?php

                            /*
                            *
                            * Save / Retrieve Form Data
                            *
                            */

                            /* Variables */
                            $post_type = 'personal-project';
                            $post_id = $_GET['post-id'];   //Get Program ID
                                                      
                            if( $post_id && $_GET['action'] == 'delete' ){
                                    wp_delete_post($post_id); // Delete
                                    printf('<script>window.location.replace("%s")</script>', currenturl(true) ); // Redirect
                                }


                            if($_POST){

                                /* Get Post Name */
                                $post_name = sanitize_text_field( $_POST['post_name'] ); 

                                /* Meta */
                                $description = sanitize_text_field( $_POST['description'] ); 
                                $nature_of_project = wp_kses_post( $_POST['nature_of_project'] );
                                $sdg = $_POST['sdg'];

                                /* Save Post to DB */
                                $post_id = wp_insert_post(array (
                                    'ID' => $post_id,
                                    'post_type' => $post_type,
                                    'post_title' => $post_name,
                                    'post_content' => "",
                                    'post_status' => 'publish',
                                ));

                                update_post_meta( $post_id, 'description', $description );
                                update_post_meta( $post_id, 'nature_of_project', $nature_of_project );
                                update_post_meta( $post_id, 'sdg', $sdg );

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                            }
                        ?>
                        <div class="form-item">
                            <label for="post_name">Name of Project</label>
                            <input 
                                type="text" 
                                name="post_name" 
                                value="<?php echo ( $post_id ) ? get_the_title( $post_id ): ''; ?>"
                            >
                        </div>
                        <div class="form-item">
                            <label for="description">Describe your Project in 140 characters</label>
                            <textarea name="description" id="description" rows="4" maxlength="140"><?php echo get_post_meta( $post_id, 'description', true ); ?></textarea>
                        </div>
                        <div class="form-item">
                            <label for="nature_of_project">Nature of Project</label>
                            <select name="nature_of_project" id="nature_of_project">
                                <?php $meta = get_post_meta( $post_id, 'nature_of_project', true ); ?>
                                <option value="Advocacy" <?php echo ($meta == 'Advocacy') ? 'selected' : '' ?> >Advocacy</option>
                                <option value="Relief Effort" <?php echo ($meta == 'Relief Effort') ? 'selected' : '' ?> >Relief Effort</option>
                                <option value="People Development" <?php echo ($meta == 'People Development') ? 'selected' : '' ?> >People Development</option>
                                <option value="Community Development" <?php echo ($meta == 'Community Development') ? 'selected' : '' ?> >Community Development</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <?php $meta = get_post_meta( $post_id, 'sdg', true ); ?>
                            <label class="padding-b-10">SDGs addressed by this project</label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="No Poverty" 
                                    name="sdg[]" 
                                    <?php echo ( in_array( 'No Poverty', $meta) ) ? 'checked' : '' ?>
                                >
                                <span class="padding-l-10">
                                    No Poverty
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="Zero Hunger" 
                                    name="sdg[]" 
                                    <?php echo ( in_array( 'Zero Hunger', $meta) ) ? 'checked' : '' ?>
                                >
                                <span class="padding-l-10">
                                    Zero Hunger
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="Good Health and Well-being" 
                                    name="sdg[]" 
                                    <?php echo ( in_array( 'Good Health and Well-being', $meta) ) ? 'checked' : '' ?>
                                >
                                <span class="padding-l-10">
                                    Good Health and Well-being
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="Quality Education" 
                                    name="sdg[]" 
                                    <?php echo ( in_array( 'Quality Education', $meta) ) ? 'checked' : '' ?>
                                >
                                <span class="padding-l-10">
                                    Quality Education
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="Gender Equality" 
                                    name="sdg[]" 
                                    <?php echo ( in_array( 'Gender Equality', $meta) ) ? 'checked' : '' ?>
                                >
                                <span class="padding-l-10">
                                    Gender Equality
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="Clean Water and Sanitation" 
                                    name="sdg[]" 
                                    <?php echo ( in_array( 'Clean Water and Sanitation', $meta) ) ? 'checked' : '' ?>
                                >
                                <span class="padding-l-10">
                                    Clean Water and Sanitation
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="Affordable and Clean Energy" 
                                    name="sdg[]" 
                                    <?php echo ( in_array( 'Affordable and Clean Energy', $meta) ) ? 'checked' : '' ?>
                                >
                                <span class="padding-l-10">
                                    Affordable and Clean Energy
                                </span>
                            </label>
                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="Decent Work and Economic Growth" 
                                    name="sdg[]"
                                    <?php echo ( in_array( 'Decent Work and Economic Growth', $meta) ) ? 'checked' : '' ?> 
                                >
                                <span class="padding-l-10">
                                    Decent Work and Economic Growth
                                </span>
                            </label>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                            <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                        </div>
                    </form>
                    
                <?php } elseif( $_GET['form-view'] == 'collaborators' ) { //Collaborators ?>
                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            $post_id = $_GET['post-id'];
                            $meta_key = 'collab_project';
                            $redirect_link = currentUrl(true);

                            if($_POST['users']){
                                /* Delete existing meta */
                                $saved_users = get_post_meta($post_id, $meta_key);

                                foreach($saved_users as $saved_user){
                                    delete_post_meta($post_id, $meta_key, $saved_user);
                                }

                                /* Save Submitted User Selection */
                                $submitted_users = $_POST['users'];

                                foreach($submitted_users as $submitted_user){
                                    add_post_meta( $post_id, $meta_key, $submitted_user );
                                }

                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                            }

                            /* Get Saved Users */
                            $saved_users = get_post_meta($post_id, $meta_key);

                            /* Get All Users */
                            $blogusers = get_users( 'blog_id=1&orderby=nicename&role=subscriber' );
                        ?>
                        <form action="#" method="POST">
                            <p class="txt-sm txt-medium margin-b-10">
                                Search & assign Collaborators to this project
                            </p>
                            <select class="select-collaborators full-width" name="users[]" multiple="multiple">
                                <option value="1" >
                                    Super Admin
                                </option>
                            <?php foreach( $blogusers as $user ){ ?>

                                <option 
                                   value="<?php echo esc_html( $user->ID ); ?>"
                                   <?php echo ( in_array($user->ID, $saved_users) ) ? "selected" : ""; ?>
                                >
                                    <?php echo esc_html( $user->user_login ); ?>
                                </option>

                            <?php } ?>
                            </select>
                            <div class="padding-t-10 text-right">
                                <input type="submit" value="Save" class="btn btn-blue txt-xs">
                            </div>
                        </form>
                    </div>
                </div>
                
                <?php } else { //Configuration ?>
                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post">
                            <?php
                                /* Meta Key */
                                $postType = 'personal-project';
                                $tax_types = array(
                                    array(
                                        'name' => 'Project Categories',
                                        'slug' => 'project-category',
                                        'hierachical' => false,
                                    ),
                                );
                                $redirect_link = currentUrl(true);
                              
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    $post = get_post($post_id);
                                    $postName = $post->post_title;
                                    
                                    /* Delete & Return */
                                    if($_GET['action'] == 'delete'){
                                        /* Delete Post */
                                        wp_delete_post($post_id);

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                    
                                    /* Publish / Unpublish & Return */
                                    if($_GET['action'] == 'publication'){
                                        /* Meta value to save */
                                        $value = "user_published";
                                        
                                        /* Get saved meta */
                                        $saved_meta = get_post_meta( $post_id, $publication_key, true );

                                        if ( $saved_meta ) //If published, Unpublish
                                            delete_post_meta( $post_id, $publication_key );
                                        else //If Unpublished, Publish
                                            update_post_meta( $post_id, $publication_key, $value );

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                }
                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if($_POST){
                                    
                                    /* Get Post Name */
                                    $postName = $_POST['post-name'];
                                    
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));

                                    /* Save terms to post */
                                    foreach($tax_types as $tax_type){
                                        wp_set_post_terms( $post_id, $_POST[$tax_type['slug']], $tax_type['slug'] );
                                    }
                                    
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }
                            ?>
                            
                            <div class="margin-b-40">
                                <label for="post-name" class="d-block txt-normal-s txt-color-dark txt-medium margin-b-10">
                                    Project / Goal Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post-name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $postName ?>"
                                >
                            </div>
                            
                            
                            <?php foreach($tax_types as $tax_type){ ?>
                            
                                <?php if( $tax_type['hierachical']){ ?>
                            
                                    <!-- Tax Title -->
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_type['name']; ?>
                                    </h2>

                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <?php if( $parent == 0 ){ ?>

                                            <div class="padding-b-20">
                                                <div class="txt-medium txt-color-dark margin-b-15">
                                                    <?php echo $term_name; ?>
                                                </div>
                                                <?php
                                                    foreach ($terms as $child_term) {
                                                        // Check and see if the term is a top-level parent. If so, display it.
                                                        $child_parent = $child_term->parent;
                                                        $child_term_id = $child_term->term_id; //Get the term ID
                                                        $child_term_name = $child_term->name; //Get the term name

                                                        if( $child_parent == $term_id ){
                                                ?>
                                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                            <input
                                                                class="margin-r-5"
                                                                type="checkbox" 
                                                                value="<?php echo $child_term_id ?>" 
                                                                name="<?php echo $tax_type['slug'] ?>[]" 
                                                                <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                            >
                                                            <span class="bg-label">
                                                                <?php echo $child_term_name; ?>
                                                            </span>
                                                        </label>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </div>

                                        <?php } ?>

                                    <?php } ?>
                                
                                <?php } else { ?>
                                
                                    <!-- Tax Title -->
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_type['name']; ?>
                                    </h2>
                                    
                                    <div class="padding-b-20">
                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_type['slug'], array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_type['slug'], array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                            <input
                                                class="margin-r-5"
                                                type="checkbox" 
                                                value="<?php echo $term_id ?>" 
                                                name="<?php echo $tax_type['slug'] ?>[]" 
                                                <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                            >
                                            <span class="bg-label">
                                                <?php echo $term_name; ?>
                                            </span>
                                        </label>

                                    <?php } ?>
                                    </div>
                                <?php } ?>
                                
                            <?php } ?>
                                                        
                            <div class="text-right">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                    </div>
                </div>
                
                <?php } ?>  
                
                <?php } elseif($_GET['view'] == 'interactions'){ ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        Projects I have engaged
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Find all the projects you have interacted with on the service market place.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] == '') ? 'active' : ''; ?>"
                            >
                                My Projects
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Projects I have engaged
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=collaborations'; ?>"
                               class="<?php echo ($_GET['view'] == 'collaborations') ? 'active' : ''; ?>"
                            >
                                Projects I can Collaborate on
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="row row-15">
                <?php
                    /* GF Search Criteria */
                    $gf_id = 31;
                    $parent_post_id = 2;
                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                        )
                      )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                    foreach( $entries as $entry ){

                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $parent_post = get_post($parent_post_id);
                ?>
                    <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                        <div class="flex_1 padding-o-30 border-o-1 border-color-darkgrey">
                            <h3 class="margin-b-10 txt-lg txt margin-b-10">
                                 <a class="txt-color-blue" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                            </h3>
                            <div class="txt-normal-s">
                                <p>
                                    <?php echo rgar( $entry, '4' ) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
                
                <?php } elseif($_GET['view'] == 'collaborations'){ ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        Projects I can Collaborate on
                    </h1>
                </div>
                
                <article class="page-summary">
                    <p>
                        Create new projects and view all of the projects that you have previously added and are currently working on.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] == '') ? 'active' : ''; ?>"
                            >
                                My Projects
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Projects I have engaged
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=collaborations'; ?>"
                               class="<?php echo ($_GET['view'] == 'collaborations') ? 'active' : ''; ?>"
                            >
                                Projects I can Collaborate on
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="row row-5">
                    <?php
                        function truncate($string, $length){
                            if (strlen($string) > $length) {
                                $string = substr($string, 0, $length) . '...';
                            }

                            return $string;
                        }

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'personal-project',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'collab_project',
                                        'value' => $current_user->ID
                                    )
                                )
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID

                            /* Get Opportunity Banner */
                            $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                            $image = reset( $images );
                    ?>
                        <div class="col-md-4 padding-lr-5 padding-b-20 d-flex">
                            <div class="home-community-card">
                                <article class="content">
                                    <div class="info bg-white">
                                        <h3 class=" txt-medium margin-b-10">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <p class="txt-sm">
                                            <?php
                                                $meta = get_post_meta($post_id, 'personal_project_description', true);
                                                if($meta){
                                                    echo truncate($meta, 70);
                                                }
                                            ?>
                                        </p>
                                        <p class="txt-sm txt-color-dark margin-t-20">
                                            <?php 
                                                $response_gf_id = 31; //Form ID

                                                /* GF Search Criteria */
                                                $response_search_criteria = array(

                                                'field_filters' => array( //which fields to search

                                                    array(

                                                        'key' => '2', 'value' => $post_id, //Current logged in user
                                                        )
                                                    )
                                                );

                                                /* Get Entries */
                                                $response_entries = GFAPI::get_entries( $response_gf_id, $response_search_criteria );

                                                /* Get GF Entry Count */
                                                $response_entry_count = GFAPI::count_entries( $response_gf_id, $response_search_criteria );
                                            ?>
                                            <span class="txt-bold">
                                                <?php echo $response_entry_count ?>
                                            </span>
                                            Messages
                                        </p>
                                        <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                            <div class="text-right">
                                                <?php
                                                    $request_desc_gf_id = 29; //Form ID

                                                    /* GF Search Criteria */
                                                    $request_desc_search_criteria = array(

                                                    'field_filters' => array( //which fields to search

                                                        array(

                                                            'key' => '2', 'value' => $post_id, //Current logged in user
                                                            )
                                                        )
                                                    );

                                                    /* Get Entries */
                                                    $request_desc_entries = GFAPI::get_entries( $request_desc_gf_id, $request_desc_search_criteria );

                                                    /* Get GF Entry Count */
                                                    $request_desc_entry_count = GFAPI::count_entries( $request_desc_gf_id, $request_desc_search_criteria );

                                                ?>

                                                <?php if (!$request_desc_entry_count){ ?>

                                                <a 
                                                   href="<?php echo currentUrl(true); ?>?view=form&gf-id=29&parent_id=<?php echo $post_id ?>&form-title=Describe <?php the_title(); ?>" 
                                                   class="txt-medium txt-color-blue"
                                                >
                                                    Add Description
                                                </a>

                                                <?php } else { ?>

                                                <a 
                                                   href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$request_desc_entries[0]['id'].'" view_id="135" action="edit" return="url" /]').'&view=project&form-title=Edit '.get_the_title().' Description' ?>" 
                                                   class="txt-medium txt-color-blue"
                                                >
                                                    Edit Description
                                                </a>

                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </article>  
                            </div>   
                        </div>
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                            <h2 class="txt-lg txt-medium">
                                No Project found.
                            </h2>
                        </div>   

                    <?php } ?>
                </div>
                
                <?php } else { ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        My Projects
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form=project" class="cta-btn">
                            Add Project
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Create new projects and view all of the projects that you have previously added and are currently working on.
                    </p>
                </article>
                
                <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] == '') ? 'active' : ''; ?>"
                            >
                                My Projects
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Projects I have engaged
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=collaborations'; ?>"
                               class="<?php echo ($_GET['view'] == 'collaborations') ? 'active' : ''; ?>"
                            >
                                Projects I can Collaborate on
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="row row-5">
                    <style>
                        .publication-status{
                            position: absolute;
                            display: flex;
                            align-items: center;
                            top: 0;
                            right: 0;
                            padding: 5px 10px;
                            color: black;
                            background-color: gainsboro;
                        }

                        .user_published{
                           background-color: #f4c026; 
                        }

                        .admin_published{
                           background-color: #00bfe7; 
                        }

                        .home-community-card{
                            overflow: visible;
                        }
                    </style>
                    <?php
                        function truncate($string, $length){
                            if (strlen($string) > $length) {
                                $string = substr($string, 0, $length) . '...';
                            }

                            return $string;
                        }

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'personal-project',
                                'post_status' => 'publish',
                                'author' => $current_user->ID,
                                'posts_per_page' => -1,
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID
                            
                            /* Publication status */      
                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                            /* Get Opportunity Banner */
                            $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                            $image = reset( $images );
                    ?>
                        <div class="col-md-4 padding-lr-5 padding-b-20 d-flex">
                            <div class="home-community-card position-relative">
                                <article class="content">
                                    <div class="info bg-white">
                                        <h3 class=" txt-medium padding-t-40 margin-b-10">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <p class="txt-sm">
                                            <?php
                                                $meta = get_post_meta($post_id, 'personal_project_description', true);
                                                if($meta){
                                                    echo truncate($meta, 70);
                                                }
                                            ?>
                                        </p>
                                        <p class="txt-sm txt-color-dark margin-t-20">
                                            <?php 
                                                $response_gf_id = 31; //Form ID

                                                /* GF Search Criteria */
                                                $response_search_criteria = array(

                                                'field_filters' => array( //which fields to search

                                                    array(

                                                        'key' => '2', 'value' => $post_id, //Current logged in user
                                                        )
                                                    )
                                                );

                                                /* Get Entries */
                                                $response_entries = GFAPI::get_entries( $response_gf_id, $response_search_criteria );

                                                /* Get GF Entry Count */
                                                $response_entry_count = GFAPI::count_entries( $response_gf_id, $response_search_criteria );
                                            ?>
                                            <span class="txt-bold">
                                                <?php echo $response_entry_count ?>
                                            </span>
                                            Messages
                                        </p>
                                        <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                            <div>
                                                <a 
                                                   href="https://www.saedconnect.org/ventures-directory/create-project/?project-id=<?php echo $post_id ?>"
                                                   class="txt-medium txt-color-green"
                                                >
                                                    Edit
                                                </a>
                                                |
                                                <a 
                                                   href="<?php printf('%s/?view=form&form=project&action=delete&post-id=%s', currentUrl(true), $post_id ) ?>"
                                                   class="confirm-delete txt-color-red"
                                                >
                                                    Delete
                                                </a>
                                            </div>
                                            <div class="publication-status <?php echo $publication_meta ?>">
                                                <?php
                                                    $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                                    if($publication_meta == 'user_published'){
                                                        echo 'Undergoing review';
                                                    } elseif($publication_meta == 'admin_published') {
                                                        echo 'Published';
                                                    }else{
                                                        echo 'Unpublished';
                                                    }
                                                ?>
                                                <div class="dropdown padding-l-20">
                                                    <a class="" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="fa-stack">
                                                            <i class="fa fa-circle fa-stack-2x txt-color-white"></i>
                                                            <i class="fa fa-ellipsis-v fa-stack-1x"></i>
                                                        </span>
                                                    </a>

                                                    <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuLink">

                                                        <a 
                                                           href="<?php printf('%s/?view=form&form-view=collaborators&post-id=%s&form-title=%s Collaborators', currentUrl(true), $post_id, get_the_title() ) ?>" 
                                                           class="dropdown-item"
                                                        >
                                                            Collaborators
                                                        </a>
                                                        <a 
                                                           href="<?php printf('%s/?view=form&action=publication&post-id=%s', currentUrl(true), $post_id ) ?>"
                                                           class="dropdown-item"
                                                        >
                                                            <?php

                                                                if($publication_meta == 'user_published'){
                                                                    echo 'Unpublish';
                                                                } elseif($publication_meta == 'admin_published') {
                                                                    echo 'Unpublish';
                                                                }else{
                                                                    echo 'Publish';
                                                                }
                                                            ?>

                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </article>  
                            </div>   
                        </div>
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                            <h2 class="txt-lg txt-medium">
                                No Project found.
                            </h2>
                        </div>   

                    <?php } ?>
                </div>
                
                <?php } ?> 
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>