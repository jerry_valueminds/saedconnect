<?php /*Template Name: Side Hustle Communities Forum*/ ?>


<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main-content">
    <header class="overview-header container-wrapper">
        <h1 class="txt-2em txt-bold margin-b-20">
            <?php the_title() ?>
        </h1>
        <h2 class="txt-lg txt-height-1-4 margin-b-80">
            <?php echo the_content(); ?>
        </h2>
    </header>
<?php endwhile; // end of the loop. ?>
    
    <section class="container-wrapper padding-tb-40">
    <?php
        bbp_list_forums(array (
            'before'              => '<ul class="row row-40 directory-list">',
            'after'               => '</ul>',
            'link_before'         => '<li class="col-md-3 padding-lr-40">',
            'link_after'          => '</li>',
            'count_before'        => '',
            'count_after'         => '',
            'count_sep'           => '',
            'separator'           => '',
            'forum_id'            => '1119',
            'show_topic_count'    => false,
            'show_reply_count'    => false,
        ));    
    ?>
    
    
    
    <?php 
        
        
        // Create Query Argument
        $args = array(
            'post_type' => 'forum',
            'showposts' => -1,
            'meta_query' => array(
                array(
                    'key' => '_bbp_forum_id',
                    'value' => '1119'
                )
            )
        );
        
        // Create Topics Query
        wp_reset_postdata();
        wp_reset_query();
        
        $temp = $wp_query; $wp_query= null;
        
        $wp_query = new WP_Query($args);
    ?>   
    
        <!--
        <ul class="row row-40 directory-list">
        <?php 
                
                if ( $wp_query->have_posts() ) {
                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Variables */
                    $program_id = $post->ID;    //Get Program ID               
            ?>
                <li class="col-md-3 padding-lr-40">
                    <a href="<?php the_permalink() ?>">
                        <span class="title">
                            <?php the_title() ?>
                        </span> 
                    </a>
                </li>
           
            <?php
                    endwhile;
                
                }else{
            ?>
                <div class="padding-tb-40 padding-lr-40">
                    <h2 class="txt-lg txt-medium">
                        No Forums found.
                    </h2>
                </div>   
                
            <?php
                    
                }
            ?>
            
        </ul>
        -->
    </section>
</main>

<?php get_footer() ?>
