    <?php /*Template Name: Trainer Directory*/ ?>
    
    <?php get_header() ?>
   
    <main class="main-content">
        <header class="overview-header container-wrapper">
            <h1 class="title">Trainers Directory</h1>
        </header>
        <section class="container-wrapper">
            <header class="margin-t-20 margin-b-30">
                <h2 class="">Locations</h2>
            </header>
            <div class="directory-tab">
                <!-- Location Tabs -->
                <ul class="nav nav-tabs" id="locationTab" role="tablist">
                <?php 
                    $terms = get_terms('state', array('hide_empty' => false));
                    $firstTerm = true;

                    foreach($terms as $term){
                ?>
                        
                    <li class="nav-item">
                        <a class="nav-link <?php echo ($firstTerm  ? 'active' : '') ?>"
                           id="location-tab-<?php echo $term->term_id ?>"
                           data-toggle="tab"
                           href="#location-<?php echo $term->term_id ?>" role="tab"
                           aria-controls="location-<?php echo $term->term_id ?>"
                           aria-selected="<?php echo ($firstTerm  ? 'true' : 'false') ?>">
                           
                            <?php echo $term->name ?>
                        </a>
                    </li>
                           
                <?php

                        $firstTerm = false;
                    };
                ?>
                </ul>
                
                <!-- Location Tab Content -->
                <div class="tab-content" id="locationTab">
                <?php 
                    $terms = get_terms('state', array('hide_empty' => false));
                    $firstTerm = true;

                    foreach($terms as $term){
                ?>
                    <div 
                       class="tab-pane fade <?php echo ($firstTerm  ? 'active show' : '') ?>"
                       id="location-<?php echo $term->term_id ?>"
                       role="tabpanel"
                       aria-labelledby="location-tab-<?php echo $term->term_id ?>">
                       
                        <div class="directory-tab">
                            <ul class="nav nav-tabs" id="skillTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="location-1-skill-tab-1" data-toggle="tab" href="#location-1-skill-1" role="tab" aria-controls="location-1-skill-1" aria-selected="true">
                                        Skill Area <?php echo $term->term_id ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="location-1-skill-tab-2" data-toggle="tab" href="#location-1-skill-2" role="tab" aria-controls="location-1-skill-2" aria-selected="false">
                                        Skill Area Two
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="skillTabContent">
                                <div class="tab-pane fade show active" id="location-1-skill-1" role="tabpanel" aria-labelledby="location-tab-1">
                                    <div class="trainer-card border-t-2">
                                        <header class="header">
                                            <h3 class="title">
                                                <a href="">Trainer Name</a>
                                            </h3>
                                            <a class="location-header" data-toggle="collapse" href="#collapseSuspend" aria-expanded="false" aria-controls="collapseSuspend">
                                                <i class="fa fa-map-marker"></i>
                                                <span class="">
                                                    Training Locations
                                                </span>
                                            </a>
                                        </header>
                                        <div class="collapse bg-grey padding-o-20  border-color-darkgrey" id="collapseSuspend" aria-labelledby="headingTwo">
                                            <p class="dropdown-action">
                                                <span class="">
                                                    Available Locations
                                                </span>
                                                <a class="txt-normal-s text-right" data-toggle="collapse" href="#collapseSuspend" aria-expanded="false" aria-controls="collapseSuspend">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </p>
                                            <article class="txt-sm margin-b-30">
                                                <p class="uppercase txt-bold txt-regular margin-b-10">
                                                    Abuja Address
                                                </p>
                                                <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                            </article>
                                            <article class="txt-sm margin-b-30">
                                                <p class="uppercase txt-bold txt-regular margin-b-10">
                                                    Lagos Address
                                                </p>
                                                <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                            </article>
                                        </div>
                                        <div class="row border-b-1 border-color-darkgrey">
                                            <div class="col-md-4 padding-tb-20 padding-r-20">
                                                <h4 class="subtitle">
                                                    Contact Information
                                                </h4>
                                                <ul class="contact-section">
                                                    <li>
                                                        <span class="key">Trainer ID:</span>
                                                        <span class="value">XXXXXX</span>
                                                    </li>
                                                    <li>
                                                        <span class="key">Trainer Address:</span>
                                                        <span class="value">XXXXXX</span>
                                                    </li>
                                                    <li>
                                                        <span class="key">Trainer Phone:</span>
                                                        <span class="value">XXXXXX</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 padding-o-20">
                                                <h4 class="subtitle">
                                                    Social Media
                                                </h4>
                                                <ul class="social-section">
                                                    <li>
                                                        <a href="">
                                                            <i class="fa fa-facebook-square"></i>
                                                            <span class="">facebook.com/saedconnect</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            <i class="fa fa-linkedin"></i>
                                                            <span class="">linkedin.com/saedconnect</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            <i class="fa fa-twitter"></i>
                                                            <span class="">@saedconnect</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                        <i class="fa fa-globe"></i>
                                                            <span class="">www.saedconnect.com</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 padding-o-20">
                                                <h4 class="subtitle">
                                                    Courses Taught
                                                </h4>
                                                <ul class="courses">
                                                    <li>Course One</li>
                                                    <li>Course Two</li>
                                                    <li>Course Three</li>
                                                    <li>Course Four</li>
                                                    <li>Course Five</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="cta">
                                            <a class="btn bg-blue txt-color-white txt-normal-s" href="">Read more</a>
                                        </div>
                                    </div>
                                    <div class="trainer-card border-t-2">
                                        <header class="header">
                                            <h3 class="title">
                                                <a href="">Trainer Name</a>
                                            </h3>
                                            
                                        </header>
                                        <div class="row">
                                            <div class="col-md-3 padding-tb-20 padding-r-20 border-b-1 border-color-darkgrey">
                                                <h4 class="subtitle">
                                                    Contact Information
                                                </h4>
                                                <ul class="contact-section">
                                                    <li>
                                                        <span class="key">Trainer ID:</span>
                                                        <span class="value">XXXXXX</span>
                                                    </li>
                                                    <li>
                                                        <span class="key">Trainer Address:</span>
                                                        <span class="value">XXXXXX</span>
                                                    </li>
                                                    <li>
                                                        <span class="key">Trainer Phone:</span>
                                                        <span class="value">XXXXXX</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3 bg-grey padding-o-20">
                                                <h4 class="subtitle">
                                                    Social Media
                                                </h4>
                                                <ul class="social-section">
                                                    <li>
                                                        <a href="">
                                                            <i class="fa fa-facebook-square"></i>
                                                            <span class="">facebook.com/saedconnect</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            <i class="fa fa-linkedin"></i>
                                                            <span class="">linkedin.com/saedconnect</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            <i class="fa fa-twitter"></i>
                                                            <span class="">@saedconnect</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                        <i class="fa fa-globe"></i>
                                                            <span class="">www.saedconnect.com</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6 bg-blue padding-o-20">
                                                <h4 class="subtitle txt-color-white">
                                                    Courses Taught
                                                </h4>
                                                <ul class="courses">
                                                    <li>Course One</li>
                                                    <li>Course Two</li>
                                                    <li>Course Three</li>
                                                    <li>Course Four</li>
                                                    <li>Course Five</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="cta">
                                            <a class="" href="">Read more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="location-1-skill-2" role="tabpanel" aria-labelledby="location-tab-2">
                                    Content 2
                                </div>
                            </div>
                        </div>    
                    </div>
                <?php

                        $firstTerm = false;
                    };
                ?> 
                </div>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>