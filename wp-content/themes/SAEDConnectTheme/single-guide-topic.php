<?php
    while ( have_posts() ) : the_post();
    
    
    if( $_REQUEST['view'] == 'resources' ){
    
        get_template_part( 'template-parts/mini-site/resources' );
    
     } else { 
    
        /* First Loop: Find First Article related to Topic */
        $connected = new WP_Query( array(
            'relationship' => array(
                'id'   => 'guide_topic_to_guide_article',
                'from' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );
        while ( $connected->have_posts() ) : $connected->the_post();
    
            if ( wp_redirect( get_permalink()) ) {
                exit;
            }
                        
        endwhile;
        wp_reset_postdata();
    
     }

endwhile; // end of the loop. ?>