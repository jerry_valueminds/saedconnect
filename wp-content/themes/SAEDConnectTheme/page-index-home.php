    <?php /*Template Name: Homepage-1*/ ?>
    
    <?php get_header() ?>
    
    <main class="home-content">       
        
        <section class="container-wrapper header-section padding-tb-80">
            <h1 class="txt-3em txt-bold txt-height-1-1">
                We exist to  help you
                <br>
                Get a Job or Start your Business
            </h1>
        </section>
        
        <div class="accordion-wrapper">
            <div class="faq-accordion bg-grey container-wrapper padding-tb-30">
                <a class="d-flex row txt-xlg txt-color-dark" href="https://www.saedconnect.org/about/synopsis/find-your-path/">
                    <h4 class="col-4 btn-link col-8 txt-bold">
                        Find Your Path
                    </h4>
                    <div class="col-4 text-right">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                </a>
            </div>

            <section class="faq-accordion bg-yellow">
                <div class="container-wrapper padding-tb-30">
                    <button class="btn-link collapsed txt-xlg txt-bold" data-toggle="collapse" data-target="#main-1" aria-expanded="false" aria-controls="main-1">
                        <span class="row">
                            <div class="col-8">
                                Start Your Business
                            </div>
                            <div class="col-4 text-right">
                                <i class="fa fa-chevron-down"></i>
                            </div>
                        </span>
                    </button>
                </div>
                <div class="container-wrapper">   
                    <div id="main-1" class="collapse" aria-labelledby="heading-1" data-parent="#accordion">
                        <div class="row row-30 padding-t-20">
                            <div class="col-md-4 padding-lr-30 padding-b-30">
                                <div class="border-b-1 padding-b-20 margin-b-30">
                                    <h4 class="txt-lg txt-medium margin-b-20">
                                        Side Hustle Communities
                                    </h4>
                                    <p class="txt-sm txt-height-1-7 margin-b-30">
                                        Meet with fellow Entrepreneurs accross over 40 businesses nationwide.
                                    </p>
                                    <div>
                                        <a class="btn btn-trans-bw txt-sm no-m-b" href="https://www.saedconnect.org/growth-programs/small-business-communities/">
                                            Get Started
                                        </a>
                                    </div>
                                </div>
                                <div class="border-b-1 b-md-none padding-b-20">
                                    <h4 class="txt-lg txt-medium margin-b-20 txt-color-dark">
                                        The Entrepreneurship Incubator
                                    </h4>
                                    <p class="txt-sm txt-height-1-7 margin-b-30">
                                        Not sure if entrepreneurship is right for you? Looking for a great business idea? Want to turn your skill into money? Want to take your idea to the next level? Need to build a business plan? Get into the Entrepreneurship Incubator.
                                    </p>
                                    <div>
                                        <a class="btn btn-trans-bw txt-sm no-m-b" href="https://www.saedconnect.org/growth-programs/program/the-entrepreneurship-incubator/">
                                            Get Started
                                        </a>
                                    </div>
                                    <!--<h4 class="txt-lg txt-medium margin-b-20">
                                        Small Business Mentor
                                    </h4>
                                    <p class="txt-sm txt-height-1-7 margin-b-30">
                                        Get all the help you need to start and grow any of 40+ Businesses types. Ask questions from experts, meet potential partners, discover tips & tricks, and much more.
                                    </p>
                                    <div>
                                        <a class="btn btn-trans-bw txt-sm no-m-b" href="https://www.saedconnect.org/growth-programs/side-hustle-Skills/">
                                            Get Started
                                        </a>
                                    </div>-->
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-30 padding-b-30">
                                <div class="border-b-1 padding-b-20 margin-b-30">
                                    <h4 class="txt-lg txt-medium margin-b-20">
                                        <a href="" class="txt-color-dark">
                                            Side Hustle Mentor Hubs
                                        </a>
                                    </h4>
                                    <p class="txt-sm txt-height-1-7 margin-b-30">
                                        Connect with expert mentors to help you start and grow any of 40+ different side hustles.
                                    </p>
                                    <div>
                                        
                                        <a class="btn btn-trans-bw txt-sm no-m-b" data-toggle="modal" href="#comingSoonModal">
                                            Get Started
                                        </a>
                                    </div>
                                </div>
                                <div class="border-b-1 b-md-none padding-b-20">
                                    <h4 class="txt-lg txt-medium margin-b-20">
                                        Business Clinic
                                    </h4>
                                    <p class="txt-sm txt-height-1-7 margin-b-30">
                                        Want to raise finance for your business? Need help with business registration and legal? Need more customers for your business? Having issues with partnerships or team building? Join the business clinic.
                                    </p>
                                    <div>
                                        <a class="btn btn-trans-bw txt-sm no-m-b" href="https://www.saedconnect.org/growth-programs/program/business-clinic/">
                                            Get Started
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-30 padding-b-30">
                                <h4 class="txt-lg txt-medium margin-b-20">
                                    Get Expert Help
                                </h4>
                                <ul class="icon-list black">
                                    <li>
                                        <a href="https://www.saedconnect.org/about/reference/business-entrepreneurship-support-services/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Business Counseling
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/about/discourse/business-plan-creation/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Business Plan Creation
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/about/discourse/raise-finance-for-your-business/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                 Raise Finance for your Business
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/about/discourse/business-registrations/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Business Registration & Other Legal Services
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/about/discourse/business-branding/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Business Branding
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="faq-accordion bg-yellow-dark">
                <div class="container-wrapper padding-tb-30">
                    <button class="btn-link collapsed txt-xlg txt-bold" data-toggle="collapse" data-target="#main-2" aria-expanded="false" aria-controls="main-2">
                        <span class="row">
                            <div class="col-8">
                                Get a Job
                            </div>
                            <div class="col-4 text-right">
                                <i class="fa fa-chevron-down"></i>
                            </div>
                        </span>
                    </button>
                </div>
                <div class="container-wrapper">   
                    <div id="main-2" class="collapse" aria-labelledby="heading-2" data-parent="#accordion">
                        <div class="row row-30 padding-t-20">
                            <div class="col-md-4 padding-lr-30 padding-b-30">
                                <div class="border-b-1 b-md-none padding-b-20">
                                    <h4 class="txt-lg txt-medium margin-b-20">
                                        The JobTrac
                                    </h4>
                                    <p class="txt-sm txt-height-1-7 margin-b-30">
                                        JobTrac helps you build, compile and verify your competency in specific job roles and then connects you to employers looking to hire verified, job-ready candidates in those roles.
                                    </p>
                                    <div>
                                        <a href="https://www.saedconnect.org/jobtrac/" class="btn btn-trans-bw txt-sm no-m-b">
                                            Explore
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-30 padding-b-30">
                                <div class="border-b-1 b-md-none padding-b-20">
                                    <h4 class="txt-lg txt-medium margin-b-20">
                                        JobAdvisor
                                    </h4>
                                    <p class="txt-sm txt-height-1-7 margin-b-30">
                                        Get seasoned mentors to show you how to craft a great CV or cover letter, search for the best jobs and get interviews, ace job interviews and more.
                                    </p>
                                    <div>
                                        <a href="https://www.saedconnect.org/growth-programs/program/career-forum/" class="btn btn-trans-bw txt-sm no-m-b">
                                            Get Started
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 padding-lr-30 padding-b-30">
                                <h4 class="txt-lg txt-medium margin-b-20">
                                    Get Expert Help
                                </h4>
                                <ul class="icon-list black">
                                    <li>
                                        <a href="https://www.saedconnect.org/about/synopsis/find-your-path/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Career Coaching Services
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/about/discourse/cv-cover-letter-prep/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                CV & Cover Letter Creation & Review
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/about/discourse/linkedin-page-creation-review-networking-coaching/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                 LinkedIn Page Creation / Review & Networking Coaching
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/about/discourse/job-search-coaching/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Job Search Coaching
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.saedconnect.org/about/discourse/interview-prep/">
                                            <i class="fa fa-chevron-right"></i>
                                            <span>
                                                Interview Coaching
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="faq-accordion bg-yellow-darker">
                <div class="container-wrapper padding-tb-30">
                    <button class="btn-link collapsed txt-xlg txt-bold" data-toggle="collapse" data-target="#main-3" aria-expanded="false" aria-controls="main-3">
                        <span class="row">
                            <div class="col-8">
                                Opportunity Center
                            </div>
                            <div class="col-4 text-right">
                                <i class="fa fa-chevron-down"></i>
                            </div>
                        </span>
                    </button>
                </div>
                <div class="container-wrapper">   
                    <div id="main-3" class="collapse" aria-labelledby="heading-3" data-parent="#accordion">
                    <?php
                        switch_to_blog(12);
                    ?>
                        <div>
                            <div class="program-card-wrapper">
                                <div class="row row-20">
                                <?php wp_reset_postdata();
                                    //wp_reset_query();
                                    //$temp = $wp_query; $wp_query= null;
                                    $wp_query = new WP_Query();
                                    $wp_query->query( 
                                        array(
                                            'post_type' => 'opportunity',
                                            'post_status' => 'publish',
                                            'posts_per_page' => 4
                                        ) 
                                    );

                                    if ( $wp_query->have_posts() ) {

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Variables */
                                        $program_id = $post->ID;    //Get Program ID

                                        /* Get Opportunity Banner */
                                        $images = rwmb_meta( 'opportunity-upload-banner', array( 'limit' => 1 ) );
                                        $image = reset( $images );                
                                ?>
                                    <div class="col-md-3 program-card white">
                                        <div class="content">
                                            <figcaption>
                                                <?php
                                                    if( rwmb_get_value( 'opportunity-close-date' ) ){
                                                ?>

                                                    <h4 class="date">
                                                        <span class="expires">
                                                            Expires
                                                        </span>
                                                        <?php rwmb_the_value( 'opportunity-close-date', array( 'format' => 'd\ M Y' ) ); ?>
                                                    </h4>

                                                <?php } ?>


                                                <div class="header">
                                                    <h3 class="title">
                                                        <a href="<?php the_permalink() ?>">
                                                            <?php echo rwmb_get_value( 'opportunity-title' ); ?>
                                                        </a>
                                                    </h3>
                                                </div>
                                                <p class="description">
                                                    <?php echo rwmb_get_value( 'opportunity-short-description' ); ?>
                                                </p>
                                                <?php
                                                    if( rwmb_get_value( 'opportunity-author' ) ){
                                                ?>

                                                    <h4 class="author">
                                                        Powered by <?php echo rwmb_get_value( 'opportunity-author' ); ?>
                                                    </h4>

                                                <?php } ?>
                                            </figcaption>
                                            <div class="image-box">
                                                <img src="<?php if($image){ echo $image['full_url']; } ?>" alt="">
                                            </div>
                                        </div>
                                    </div>


                                <?php
                                        endwhile;

                                    }else{
                                ?>
                                    <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                                        <h2 class="txt-lg txt-medium">
                                            No Opportunities found.
                                        </h2>
                                    </div>   

                                <?php

                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                        

                    <?php
                        //Revert to Previous Multisite
                        restore_current_blog();
                    ?>
                        <div class="padding-tb-30">
                            <a href="https://www.saedconnect.org/opportunity-center/" class="btn btn-trans-wb txt-sm no-m-b">
                                View more
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
    
    <?php get_footer('home-page') ?>
    
    <!-- SHC Modal -->
    <?php get_template_part( 'template-parts/_shc_modal' ); ?>