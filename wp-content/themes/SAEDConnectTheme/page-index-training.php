    <?php /*Template Name: Homepage-Training*/ ?>
    
    <?php get_header() ?>

	<main class="main-content">
		<header class="overview-header container-wrapper">

			<div class="row row-40 padding-tb-20">
				<div class="col-md-7 padding-lr-40">
					<h1 class="txt-2-6em txt-bold txt-height-1-2 padding-b-20">
						Showcase Your Trainings, Programs & Youth Development Services on SAEDConnect
					</h1>
				</div>
				<div class="col-md-5 padding-lr-40">
					<ul class="homepage-list">
						<li class="active">
							<a href="<?php echo get_page_link(577); ?>">
                                Offer Trainings & Mentorship
                            </a>
						</li>
						<li>
							<a href="<?php echo get_page_link(575); ?>">
                                Support Youth Development
                            </a>
						</li>
						<li>
							<a href="#">
                                Support a Project
                            </a>
						</li>
						<li>
							<a href="#">
                                Donate
                            </a>
						</li>
					</ul>
				</div>
			</div>
		</header>
        <section class="bg-yellow padding-tb-20">
            <div class="container-wrapper">
                <h2 class="txt-xlg txt-height-1-3">
                    Let us share your programs, trainings & initiatives with our over 1 million - strong youth audience.
                </h2>
            </div>
        </section>
		<!-- General Section -->
		<section>
			<div class="row">
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (32).png');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Join our Training Provider Directory
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							List your contact details in our trainer directory so people looking for trainers in your location can easily find you.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn More 
							</a>
							<a class="btn btn-white" data-toggle="modal" href="#comingSoonModal">
								Join
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (33).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Showcase your Training Courses
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Showcase details of your training courses and packages and get direct registrations from our large community of skill seekers.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn More 
							</a>
							<a class="btn btn-white" data-toggle="modal" href="#comingSoonModal">
								Post your Training 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (34).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Showcase your distribution & franchise Opportunities
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Take your distribution or franchise network nationwide by enlisting & empowering ready & able young entrepreneurs as affiliate marketers & micro franchisees.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn More 
							</a>
						</div>
					</div>
				</article>

				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (35).png');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Showcase your youth development programs & Events
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Are you hosting a competition, offering a scholarship, giving a grant or hosting any program or event that could be beneficial to youth entrepreneurship and development? List your program on our site and we’ll share it with our audience nationwide.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn More 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (36).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Offer a Support Service?
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							We list providers who offer support services like legal support, career coaching, small business consultancy, etc so our audience can easily find you when they need you.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn More 
							</a>
						</div>
					</div>
				</article>
				<article class="col-md-4 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (37).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Targeted Advertisements
						</h3>
						<p class="txt-height-1-5 margin-b-30">
							Get hyper targeted advertisements for your youth development programs & initiatives. We can target adverts by locations, course of study, year of graduation, etc.
						</p>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Learn More 
							</a>
						</div>
					</div>
				</article>

				<article class="col-md-8 feature-image-block" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (38).jpg');">
					<div class="content txt-color-white">
						<h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
							Partner verification demonstrates to our users that your program is authentic. Listings by verified partners are prioritized on the site.
						</h3>
						<div class="btn-wrapper">
							<a class="btn btn-trans-wb" data-toggle="modal" href="#comingSoonModal">
								Get Started 
							</a>
						</div>
					</div>
				</article>
				<div class="col-md-4 cta-block bg-black txt-color-white">
					<article>
						<div class="txt-3em txt-color-white padding-tb-20 border-t-4 border-color-white">
							<i class="fa fa-quote-left"></i>
						</div>
						<h2 class="title">
							SAEDConnect is the ideal destination for individuals & organizations with products, services, programs and initiatives targeted at the youth market.
						</h2>
						<div class="txt-medium txt-height-1-2">
							Lawrence Anawheokhai 
							<br> Assistant Director Partnerships, NYSC SAED
						</div>
					</article>
				</div>

				<div class="col-md-12 cta-block">
                    <h4 class="txt-xlg margin-b-30">Get in Touch</h4>
                    <div class="row row-20">
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-10">
                                Maero Uwede
                            </h4>
                            <h4 class="txt-medium txt-italics margin-b-10">
                                Director, Annual Giving
                            </h4>
                            <h4 class="txt-color-yellow-dark txt-medium">
                                maero.uewde@valuemindsng.com
                            </h4>
                        </div>
                    </div>
                </div>
			</div>
		</section>
		<section class="pre-footer" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (39).jpg');">
            <div class="content">
                <h4 class="title">
                    Join the SAEDConnect Training Community
                </h4>
                <p class="txt-height-1-4 txt-color-white margin-b-40">
                    Be the first to know about trainings, events, programs, support and opportunities available to help you start and grow your business.
                </p>
                <article class="btn-wrapper margin-b-40">
                    <a class="btn btn-trans-wb-icon" data-toggle="modal" href="#comingSoonModal">
                        Connect on Facebook
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="btn btn-trans-wb-icon" data-toggle="modal" href="#comingSoonModal">
                        Follow us on Twitter
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="btn btn-trans-wb-icon" data-toggle="modal" href="#comingSoonModal">
                        Join the Telegram Channel
                        <i class="fa fa-telegram"></i>
                    </a>
                </article>
                <article>
                    <h4 class="txt-xlg txt-color-white margin-b-20">Join the Training Support Mailing List</h4>
                    <form action="" class="mailing-list-form">
                        <input class="mail-field" type="email" placeholder="Enter your email address">
                        <input class="submit-btn" type="submit" value="subscribe">
                    </form>
                </article>
            </div>
        </section>
	</main>
   
    <?php get_footer() ?>
