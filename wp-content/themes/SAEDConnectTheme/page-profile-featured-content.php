<?php /*Template Name: Profile - Featured Content*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Post Type */
        $postType = 'featured-content';

        /* Publication */
        $publication_key   = 'publication_status';

    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
               
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post">
                            <?php
                                /* Meta Key */
                                $meta_key = 'assigned_users';
                                $validation_meta_key = 'validation_types';
                                $redirect_link = currentUrl(true);
                              
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    $bundle_id = $_GET['post-id'];
                                    $post = get_post($post_id);
                                    $postName = $post->post_title;
                                    
                                    /* Delete & Return */
                                    if($_GET['action'] == 'delete'){
                                        /* Delete Post */
                                        wp_delete_post($post_id);

                                        /* Redirect */
                                        printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                    }
                                }
                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if( $_POST ){
                                    
                                    /* Get Post Name */
                                    $postName = $_POST['post-name'];
                                    
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));
                                    
                                    /* Get form data */
                                    $infoPageUrl = $_POST['info-page-url'];
                                    $summary = $_POST['summary'];
                                    $bgColor = $_POST['bg-color'];
                                    $altColor = $_POST['alt-color'];
                                    
                                    $submitted_jobs = $_POST['jobs'];
                                    $submitted_offers = $_POST['service-offers'];
                                    $submitted_opportunities = $_POST['opportunities'];
                                    $submitted_courses = $_POST['courses'];
                                    $submitted_businesses = $_POST['businesses'];
                                    
                                    
                                    /* Serialize Form Submission */
                                    $serialized_jobs = maybe_serialize( $submitted_jobs );
                                    $serialized_offers = maybe_serialize( $submitted_offers );
                                    $serialized_opportunities = maybe_serialize( $submitted_opportunities );
                                    $serialized_courses = maybe_serialize( $submitted_courses );
                                    $serialized_businesses = maybe_serialize( $submitted_businesses );

                                    
                                    /* Save data */
                                    update_post_meta($post_id, 'info-page-url', $infoPageUrl);
                                    update_post_meta($post_id, 'summary', $summary);
                                    update_post_meta($post_id, 'bg-color', $bgColor);
                                    update_post_meta($post_id, 'alt-color', $altColor);
                                    
                                    update_post_meta($post_id, 'jobs', $serialized_jobs);
                                    update_post_meta($post_id, 'service-offers', $serialized_offers);
                                    update_post_meta($post_id, 'opportunities', $serialized_opportunities);
                                    update_post_meta($post_id, 'courses', $serialized_courses);
                                    update_post_meta($post_id, 'businesses', $serialized_businesses);
                                }
                                              
                                              
                                /* Get Saved data */             
                                $infoPageUrl = get_post_meta($post_id, 'info-page-url', true);
                                $summary = get_post_meta($post_id, 'summary', true);
                                $bgColor = get_post_meta($post_id, 'bg-color', true);
                                $altColor = get_post_meta($post_id, 'alt-color', true);
                              
                                /* Get Saved Jobs */
                                $saved_jobs = maybe_unserialize( get_post_meta($post_id, 'jobs', true) );
                                              
                                /* Get Saved Serfice Offers */
                                $saved_offers = maybe_unserialize( get_post_meta($post_id, 'service-offers', true) );
                                              
                                /* Get Saved Opportunities */
                                $saved_opportunities = maybe_unserialize( get_post_meta($post_id, 'opportunities', true) );
                                              
                                /* Get Saved Courses */
                                $saved_courses = maybe_unserialize( get_post_meta($post_id, 'courses', true) );                                              
                                /* Get Saved businesses */
                                $saved_businesses = maybe_unserialize( get_post_meta($post_id, 'businesses', true) );

                            ?>
                            
                            <!-- Title -->
                            <div class="margin-b-40">
                                <label for="post-name" class="d-block txt-normal-s txt-medium margin-b-10">
                                    Title
                                </label>
                                <input 
                                    type="text" 
                                    name="post-name" 
                                    class="d-block padding-tb-5 padding-lr-10 full-width"
                                    value="<?php echo $postName ?>"
                                >
                            </div>
                            
                            <!-- Jobs -->
                            <div class="padding-b-30">
                                <p class="txt-normal-s txt-medium margin-b-10">
                                    Assign Jobs
                                </p>
                                <?php switch_to_blog(101); ?>
                                <select class="select-collaborators full-width" name="jobs[]" multiple="multiple">
                                    <?php
                                        /*
                                        *==================================================================================
                                        *==================================================================================
                                        *   WP Query
                                        *==================================================================================
                                        *==================================================================================
                                        */
                                        // Create Query Argument
                                        $args = array(
                                            'post_type' => 'job',
                                            'showposts' => -1,
                                        );


                                        $wp_query = new WP_Query($args);

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Get Post ID */
                                        $post_id = $post->ID;
                                             
                                        if($_POST){
                                            delete_post_meta($post_id, 'featured', $bundle_id);
                                            
                                            if( in_array($post_id, $saved_jobs) ){
                                                
                                                add_post_meta($post_id, 'featured', $bundle_id, false);
                                            }
                                        }
                                    ?> 
                                        
                                        <option 
                                           value="<?php echo $post_id; ?>"
                                           <?php echo ( in_array($post_id, $saved_jobs) ) ? "selected" : ""; ?>
                                        >
                                            <?php echo '#'.$post_id.' - '.get_the_title(); ?>
                                        </option>

                                    <?php
                                        endwhile;
                                    ?>
                                </select>
                                <?php restore_current_blog(); ?>
                            </div>
                            
                            <!-- Service Offer -->
                            <div class="padding-b-30">
                                <p class="txt-normal-s txt-medium margin-b-10">
                                    Freelancer Offers
                                </p>
                                <?php switch_to_blog(101); ?>
                                <select class="select-collaborators full-width" name="service-offers[]" multiple="multiple">
                                    <?php
                                        /*
                                        *==================================================================================
                                        *==================================================================================
                                        *   WP Query
                                        *==================================================================================
                                        *==================================================================================
                                        */
                                        // Create Query Argument
                                        $args = array(
                                            'post_type' => 'offer-a-service',
                                            'showposts' => -1,
                                        );


                                        $wp_query = new WP_Query($args);

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Get Post ID */
                                        $post_id = $post->ID;
                                             
                                        if($_POST){
                                            delete_post_meta($post_id, 'featured', $bundle_id);
                                            
                                            if( in_array($post_id, $saved_offers) ){
                                                
                                                add_post_meta($post_id, 'featured', $bundle_id, false);
                                            }
                                        }
                                    ?> 
                                        
                                        <option 
                                           value="<?php echo $post_id; ?>"
                                           <?php echo ( in_array($post_id, $saved_offers) ) ? "selected" : ""; ?>
                                        >
                                            <?php echo '#'.$post_id.' - '.get_the_title(); ?>
                                        </option>

                                    <?php
                                        endwhile;
                                    ?>
                                </select>
                                <?php restore_current_blog(); ?>
                            </div>
                            
                            <!-- Opportunities -->
                            <div class="padding-b-30">
                                <p class="txt-normal-s txt-medium margin-b-10">
                                    Assign Opportunities
                                </p>
                                <?php switch_to_blog(12); ?>
                                
                                <select class="select-collaborators full-width" name="opportunities[]" multiple="multiple">
                                    <?php
                                        /*
                                        *==================================================================================
                                        *==================================================================================
                                        *   WP Query
                                        *==================================================================================
                                        *==================================================================================
                                        */
                                        // Create Query Argument
                                        $args = array(
                                            'post_type' => 'opportunity',
                                            'showposts' => -1,
                                        );


                                        $wp_query = new WP_Query($args);

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Get Post ID */
                                        $post_id = $post->ID;

                                        if($_POST){
                                            delete_post_meta($post_id, 'featured', $bundle_id);
                                            
                                            if( in_array($post_id, $saved_opportunities) ){
                                                
                                                add_post_meta($post_id, 'featured', $bundle_id, false);
                                            }
                                        }
                                    ?> 

                                        <option 
                                           value="<?php echo $post_id; ?>"
                                           <?php echo ( in_array($post_id, $saved_opportunities) ) ? "selected" : ""; ?>
                                        >
                                            <?php echo '#'.$post_id.' - '.get_the_title(); ?>
                                        </option>

                                    <?php
                                        endwhile;
                                    ?>
                                </select>
                                
                                <?php restore_current_blog(); ?>
                            </div>
                            
                            <!-- Training -->
                            <div class="padding-b-15">
                                <p class="txt-normal-s txt-medium margin-b-10">
                                    Assign Courses
                                </p>
                                <?php switch_to_blog(18); ?>
                                <select class="select-collaborators full-width" name="courses[]" multiple="multiple">
                                    <?php
                                        /*
                                        *==================================================================================
                                        *==================================================================================
                                        *   WP Query
                                        *==================================================================================
                                        *==================================================================================
                                        */
                                        // Create Query Argument
                                        $args = array(
                                            'post_type' => 'training-offer',
                                            'showposts' => -1,
                                        );


                                        $wp_query = new WP_Query($args);

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Get Post ID */
                                        $post_id = $post->ID;

                                        if($_POST){
                                            delete_post_meta($post_id, 'featured', $bundle_id);
                                            
                                            if( in_array($post_id, $saved_courses) ){
                                                
                                                add_post_meta($post_id, 'featured', $bundle_id, false);
                                            }
                                        }
                                    ?> 

                                        <option 
                                           value="<?php echo $post_id; ?>"
                                           <?php echo ( in_array($post_id, $saved_courses) ) ? "selected" : ""; ?>
                                        >
                                            <?php echo '#'.$post_id.' - '.get_the_title(); ?>
                                        </option>

                                    <?php
                                        endwhile;
                                    ?>
                                </select>
                                <?php restore_current_blog(); ?>
                            </div>
                            
                            <!-- Businesses -->
                            <div class="padding-b-15">
                                <p class="txt-normal-s txt-medium margin-b-10">
                                    Assign Businesses
                                </p>
                                <?php switch_to_blog(103); ?>
                                <select class="select-collaborators full-width" name="businesses[]" multiple="multiple">
                                    <?php
                                        /*
                                        *==================================================================================
                                        *==================================================================================
                                        *   WP Query
                                        *==================================================================================
                                        *==================================================================================
                                        */
                                        // Create Query Argument
                                        $args = array(
                                            'post_type' => 'business',
                                            'showposts' => -1,
                                        );


                                        $wp_query = new WP_Query($args);

                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        /* Get Post ID */
                                        $post_id = $post->ID;

                                        if($_POST){
                                            delete_post_meta($post_id, 'featured', $bundle_id);
                                            
                                            if( in_array($post_id, $saved_businesses) ){
                                                
                                                add_post_meta($post_id, 'featured', $bundle_id, false);
                                            }
                                        }
                                    ?> 

                                        <option 
                                           value="<?php echo $post_id; ?>"
                                           <?php echo ( in_array($post_id, $saved_businesses) ) ? "selected" : ""; ?>
                                        >
                                            <?php echo '#'.$post_id.' - '.get_the_title(); ?>
                                        </option>

                                    <?php
                                        endwhile;
                                    ?>
                                </select>
                                <?php restore_current_blog(); ?>
                            </div>
                            
                            <div class="text-right padding-t-20">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                        <?php 
                            if( $_POST )
                                /* Redirect */
                                printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        ?>
                    </div>
                </div>
               
            <?php } else { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        Featured Content
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currentUrl(true); ?>?view=form&form-title=Create a Validation Program" class="cta-btn">
                            Add Group
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        Create, manage and Feature Jobs, Opportunities, offers.
                    </p>
                </article>
                
               <!-- <nav class="sub-nav">
                    <ul>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true); ?>"
                               class="<?php echo ($_GET['view'] != 'interactions') ? 'active' : ''; ?>"
                            >
                                My Service Offers
                            </a>
                        </li>
                        <li>
                            <a 
                               href="<?php echo currentUrl(true).'?view=interactions'; ?>"
                               class="<?php echo ($_GET['view'] == 'interactions') ? 'active' : ''; ?>"
                            >
                                Service Offers I have engaged
                            </a>
                        </li>
                    </ul>
                </nav>-->
                <div class="row row-15 txt-sm txt-medium padding-b-10 margin-b-15 border-b-1 border-color-darkgrey">
                    <div class="col-4 padding-lr-15">
                        NAME
                    </div>
                    <div class="col-3 padding-lr-15">
                        ASSIGNED USERS
                    </div>
                    <div class="col-3 padding-lr-15">
                        VALIDATION TYPES
                   </div>                  
                </div>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */
                    // Create Query Argument
                    $args = array(
                        'post_type' => $postType,
                        'showposts' => -1,
                        'author' => $current_user->ID,
                        'meta_query' => $meta_array,
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                          
                ?>
                    
                    <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                        <div class="col-4 padding-lr-15">
                            <a href="<?php the_permalink() ?>" class="txt-color-blue" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </div>
                        <div class="col-3 padding-lr-15">
                            <div class="d-flex align-items-center">
                                <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                    <?php
                                        /* Get Saved Users */
                                        $saved_users = get_post_meta($post_id, $meta_key);
                                        $saved_users = $saved_users['assigned_users'];
                          
                                        foreach( $saved_users as $key => $saved_user ){
                                            
                                            if( $saved_user == 1 ){
                                                
                                                echo 'SAEDConnect Admin'; 
                                                
                                            } else {
                                                
                                                $user_info = get_userdata($saved_user);
                                                echo $user_info->user_nicename;
                                                
                                            }
                                            
                                            if( $key + 1 < count($saved_users) ){
                                                echo ", ";
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 padding-lr-15">
                            <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                <?php 
                                    /* Get Saved Users */
                                    $saved_validation_types = get_post_meta($post_id, $validation_meta_key);
                                                    
                          
                                    $saved_validation_types = $saved_validation_types['validation_types'];

                                    foreach( $saved_validation_types as $key => $saved_validation_type ){

                                        echo $saved_validation_type; 

                                        if( $key + 1 < count($saved_validation_types) ){
                                            echo ", ";
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-2 padding-lr-15 text-right">
                            <a 
                               href="<?php printf('%s/?view=form&post-id=%s&form-title=Edit %s', currentUrl(true), $post_id, get_the_title() ) ?>"
                               class="txt-medium txt-color-green"
                            >
                                Edit
                            </a>
                            |
                            <a 
                               href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', currentUrl(true), $post_id ) ?>"
                               class="confirm-delete txt-color-red"
                            >
                                Delete
                            </a>
                        </div>
                    </div>
                    
                <?php
                    endwhile;
                ?>
                
            <?php } ?> 
                
            </div>
        </section>
    </main>
<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>