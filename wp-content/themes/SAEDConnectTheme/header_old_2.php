<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    <meta name="description" content="">
    <meta name="author" content="saedconnect.com">
	<title>SAEDConnect</title>
	
	<!--Site icon-->
	<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" type="image/x-icon">
	
    <!--Load Styles-->
	<?php wp_head(); ?>
	
	<!-- JQuery CDN -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	
	<!--FontAwesome CDN-->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
</head>
<body>

<?php if ( get_post_type( get_the_ID() ) !== 'album' &&  get_post_type( get_the_ID() ) !== 'gallery' ) { ?>
	
	<header class="main-navigation font-main" id="myHeader">
        <div class="navigation-wrapper">
            <a class="brand" href="http://www.saedconnect.org/">
                <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                <span class="name">
                    SAEDConnect
                </span>
            </a>
            <div class="action-btn-wrapper">
                
                <div class="search">
                    <div class="search-btn">
                        <i class="fa fa-search"></i>
                    </div>
                    <form action="" class="search-form">
                        <div class="wrapper">
                            <input class="search-box" type="search" placeholder="Type to Search">
                            <div class="close-btn">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-times fa-stack-1x"></i>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="user-account">
                    
                    <?php
                    
                        if ( is_user_logged_in() ) {
                         $display_name = um_user('display_name');
                    ?>
                        
                        <div class="dropdown">
                            <button class="login dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="account-info">
                                    <img class="profile-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/user-icon.png" alt="">
                                    <span class="profile-name">
                                        <?php echo $display_name; // prints the user's display name ?>
                                    </span>
                                </span>
                            </button>
                            <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                                <a class="dropdown-item" href="<?php echo network_home_url() ?>user">
                                    View Profile
                                </a>
                                <a class="dropdown-item" href="<?php echo network_home_url() ?>logout">
                                    Logout
                                </a>
                            </div>
                        </div>
                         
                         
                    <?php } else { ?>
                           
                        <a class="login" href="<?php echo network_home_url() ?>login">Login</a>
                        <a class="signup" href="<?php echo network_home_url() ?>register">Sign up</a>
                            
                    <?php } ?>
                </div>
                <button class="menu-btn hamburger hamburger--spring d-block d-sm-none">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    
	</header>
	
<?php } ?>