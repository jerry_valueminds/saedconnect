<?php /*Template Name: Profile - Education / Experience*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>

            <div class="dashboard-multi-main-content full">
                <div class="page-header">
                    <h1 class="page-title">
                        Education & Experience          
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Input information about where you went to school, the trainings you have attended and details about what you did in your previous jobs. If you are a freelancer, include the names of clients you have worked for, and what you did for them.
                    </p>
                </article>
            
            <?php
                /* If Add form */
                if ( $_REQUEST['view'] == 'edit-view' ){
            ?>

                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title']; ?>          
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/competency-profile/education-experience-profile/" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>

                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                </div>

            <?php
                /* Else view template */    
                } else {

            ?>
                <!-- Education & Training -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 31; //Form ID
                    $gv_id = 1128; //Gravity View ID
                    $title = 'Education & Training';

                    $entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );   
                    
                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $title ?>
                        </h2>
                    </div>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                What degree do you possess? What school did you attend? What did you study? input all of that information here.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/competency-profile/education-experience-profile/?view=edit-view&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Degree Attained (e.g BA, BS, JD, PhD)         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        School      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 8 ); ?>
                                    </p>
                                </div>

                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Field of Study (e.g Biology, Computer Science, Nursing, Marketing, etc)      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 9 ); ?>
                                    </p>
                                </div>

                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Start Month/Year      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 10 ); ?>
                                    </p>
                                </div>

                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        End Month/Year (Current students: Enter your expected graduation year)     
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 11 ); ?>
                                    </p>
                                </div>

                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Achievements      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 12 ); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=education&form-title=Education / Training' ?>" 
                                   class="edit-btn"
                                >
                                    Edit
                                </a>
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=education';  ?>" 
                                   class="delete-btn margin-l-5 confirm-delete"
                                >
                                    Delete
                                </a>
                            </div>
                        </div>

                        <?php } ?>

                        <div class="padding-o-20">
                            <a 
                                class="btn btn-ash txt-xxs no-m-b" 
                                href="<?php printf("https://www.saedconnect.org/competency-profile/education-experience-profile/?view=edit-view&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                            >
                                Add <?php echo $title ?>
                            </a>
                        </div>

                    <?php } ?>
                </div>

                <!-- Experiences -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 29; //Form ID
                    $gv_id = 1130; //Gravity View ID
                    $title = 'Experiences';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $title ?>
                        </h2>
                    </div>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Provide information on your previous employment and previously done work.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/competency-profile/education-experience-profile/?view=edit-view&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Position/Job Title        
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Company/Company Website      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 9 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Location     
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 10 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Start Month/Start Year     
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 11 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        End Month/Year      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 12 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Describe what you did/Your Achievements     
                                    </p>
                                    <article class="text-box txt-normal-s">
                                        <?php echo rgar( $entry, 13 ); ?>
                                    </article>
                                </div>
                            </div>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=education&form-title=Experience' ?>" 
                                   class="edit-btn"
                                >
                                    Edit
                                </a>
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=education';  ?>" 
                                   class="delete-btn margin-l-5 confirm-delete"
                                >
                                    Delete
                                </a>
                            </div>
                        </div>

                        <?php } ?>

                        <div class="padding-o-20">
                            <a 
                                class="btn btn-ash txt-xxs no-m-b" 
                                href="<?php printf("https://www.saedconnect.org/competency-profile/education-experience-profile/?view=edit-view&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                            >
                                Add <?php echo $title ?>
                            </a>
                        </div>

                    <?php } ?>
                </div>

                <!-- Certifications / Affiliations -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 35; //Form ID
                    $gv_id = 1126; //Gravity View ID
                    $title = 'Certifications / Affiliations';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $title ?>
                        </h2>
                    </div>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                What certifications do you possess? What year were they acquired? Please provide all the information about your certifications here.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/competency-profile/education-experience-profile/?view=edit-view&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Certifications / Affiliations Name
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Type       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 15 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        From       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 11 ); ?>
                                    </p>
                                </div>

                                <?php if( rgar( $entry, 12 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        To         
                                    </p>
                                    <p class="txt-sm">
                                    <?php 
                                        if( rgar( $entry, 12 ) ){
                                            echo rgar( $entry, 12 );
                                        } else {
                                            echo 'Present';
                                        }

                                    ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 19 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        To       
                                    </p>
                                    <p class="txt-sm">
                                    <?php 
                                        if( rgar( $entry, 19 ) ){
                                            echo rgar( $entry, 19 );
                                        } else {
                                            echo 'Present';
                                        }

                                    ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 13 ) ){ ?>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Your Achievements in the group       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 13 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Description      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 18 ); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=education&form-title=Certification / Affiliation' ?>" 
                                    class="edit-btn"
                                >
                                    Edit
                                </a>
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=education';  ?>" 
                                   class="delete-btn margin-l-5 confirm-delete"
                                >
                                    Delete
                                </a>
                            </div>
                        </div>

                        <?php } ?>

                        <div class="padding-o-20">
                            <a 
                                class="btn btn-ash txt-xxs no-m-b" 
                                href="<?php printf("https://www.saedconnect.org/competency-profile/education-experience-profile/?view=edit-view&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                            >
                                Add <?php echo $title ?>
                            </a>
                        </div>

                    <?php } ?>
                </div>

                <!-- Accomplishments -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 33; //Form ID
                    $gv_id = 1104; //Gravity View ID
                    $title = 'Accomplishments';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $title ?>
                        </h2>
                    </div>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Let us know what you have achieved over the years.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/competency-profile/education-experience-profile/?view=edit-view&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Accomplishment Name          
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Accomplishment Type         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 4 ); ?>
                                    </p>
                                </div>

                                <?php if( rgar( $entry, 5 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Awarder/Awarder Website         
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 5 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 6 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Award Date        
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 6 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 7 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Patent Number       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 7 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 9 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        URL       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 9 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 8 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Date Awarded       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 8 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 10 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        URL       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 10 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 11 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Publication Date       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 11 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Description      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 3 ); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=education&form-title=Accomplishment' ?>" 
                                   class="edit-btn"
                                >
                                    Edit
                                </a>
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=education';  ?>" 
                                   class="delete-btn margin-l-5 confirm-delete"
                                >
                                    Delete
                                </a>
                            </div>
                        </div>

                        <?php } ?>

                        <div class="padding-o-20">
                            <a 
                                class="btn btn-ash txt-xxs no-m-b" 
                                href="<?php printf("https://www.saedconnect.org/competency-profile/education-experience-profile/?view=edit-view&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                            >
                                Add <?php echo $title ?>
                            </a>
                        </div>

                    <?php } ?>
                </div>
            <?php } ?>
            </div>

        </section>
    </main>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>