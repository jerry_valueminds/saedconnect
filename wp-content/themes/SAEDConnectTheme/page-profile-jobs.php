<?php /*Template Name: Profile - Jobs*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Publication */
        $publication_key = 'publication_status';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
              
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                <?php if( $_REQUEST['gf-id'] ){ //GF View ?>
                   
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>
                    
                <?php } else { //Configuratin ?>
                
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="<?php echo currentUrl(true); ?>" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    
                    <?php
                        $postType = 'job';
                             
                        /* Get Post ID */
                        if($_GET['post-id']){
                            $post_id = $_GET['post-id'];
                        }
      
                        /* Redirect Link */
                        $redirect_link = currentUrl(true);
                              
                        /* Delete & Return */
                        if($_GET['action'] == 'delete'){
                            /* Delete Post */
                            wp_delete_post($post_id);

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }

                        /* Publish / Unpublish & Return */
                        if($_GET['action'] == 'publication'){
                            /* Meta value to save */
                            $value = "user_published";

                            /* Get saved meta */
                            $saved_meta = get_post_meta( $post_id, $publication_key, true );

                            if ( $saved_meta ) //If published, Unpublish
                                delete_post_meta( $post_id, $publication_key );
                            else //If Unpublished, Publish
                                update_post_meta( $post_id, $publication_key, $value );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', $redirect_link);
                        }
                    ?>
                    
                    <div class="entry">
                        <?php if($_GET['action'] == 'job-description'){ ?>
                           
                        <form action="<?php echo currentUrl(false); ?>" method="post">
                            <?php
                                /* Add/Edit Job (Specification) */
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    
                                    /* Get post */
                                    $post = get_post($post_id);
                                    
                                    /* Get Post data */
                                    $jobSummary = get_post_meta( $post->ID, 'job-summary', true );
                                    $jobDescription = get_post_meta( $post->ID, 'job-description', true );
                                    $paymentType = get_post_meta( $post->ID, 'payment-type', true );
                                    $offerAmount = get_post_meta( $post->ID, 'offer-amount', true );
                                    $offerAmountConfidential = get_post_meta( $post->ID, 'offer-amount-confidential', true );
                                    $commissionAmount = get_post_meta( $post->ID, 'commission-amount', true );
                                    $commissionAmountConfidential = get_post_meta( $post->ID, 'commission-amount-confidential', true );
                                }
                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if($_POST){
                                    
                                    /* Get Post Name */
                                    $jobSummary = sanitize_text_field( $_POST['job-summary'] );
                                    $jobDescription = wp_kses_post( $_POST['job-description'] );
                                    $paymentType = sanitize_text_field( $_POST['payment-type'] );
                                    $offerAmount = sanitize_text_field( $_POST['offer-amount'] );
                                    $offerAmountConfidential = sanitize_text_field( $_POST['offer-amount-confidential'] );
                                    $commissionAmount = sanitize_text_field( $_POST['commission-amount'] );
                                    $commissionAmountConfidential = sanitize_text_field( $_POST['commission-amount-confidential'] );
                                                                        
                                    /* Update Meta */
                                    update_post_meta( $post_id, 'job-summary', $jobSummary );
                                    update_post_meta( $post_id, 'job-description', $jobDescription );
                                    update_post_meta( $post_id, 'payment-type', $paymentType );
                                    update_post_meta( $post_id, 'offer-amount', $offerAmount );
                                    update_post_meta( $post_id, 'offer-amount-confidential', $offerAmountConfidential );
                                    update_post_meta( $post_id, 'commission-amount', $commissionAmount );
                                    update_post_meta( $post_id, 'commission-amount-confidential', $commissionAmountConfidential );

                                    
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }
                            ?>
                            
                            <div class="form margin-b-40">
                                <!-- Summary -->
                                <div class="form-item">
                                    <label for="post-name">
                                        Job Summary
                                    </label>
                                    <textarea name="job-summary" cols="30" rows="4"><?php echo $jobSummary ?></textarea>
                                </div>
                                <!-- Description -->
                                <div class="form-item">
                                    <label for="post-name">
                                        Full Job Description
                                    </label>
                                    <textarea class="summernote" name="job-description" cols="30" rows="20"><?php echo $jobDescription ?></textarea>
                                </div>
                                <!-- Job Type -->
                                <div class="form-item">
                                    <label for="job-type">
                                        Payment Type
                                    </label>
                                    <select name="payment-type" required>
                                        <option value="commission" <?php echo ($paymentType == 'commission')? "selected" : "" ?>>
                                            Commission
                                        </option>
                                        <option value="oneOff" <?php echo ($paymentType == 'oneOff')? "selected" : "" ?>>
                                            One-off
                                        </option>
                                        <option value="salary" <?php echo ($paymentType == 'salary')? "selected" : "" ?>>
                                            Salary
                                        </option>
                                    </select>
                                </div>
                                
                                <!-- Offer Amount -->
                                <div class="row conditional" data-condition="['salary','oneOff'].includes(payment-type)">
                                    <div class="col-12 form-item">
                                        <label for="offer-amount">
                                            Offer Amount
                                        </label>
                                        <input 
                                            type="number" 
                                            name="offer-amount" 
                                            class="d-block padding-tb-5 padding-lr-10 full-width"
                                            value="<?php echo $offerAmount ?>"
                                        >
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <label for="offer-amount-confidential" class="txt-normal-s txt-medium padding-r-10">
                                            Confidential
                                        </label>
                                        <input 
                                            type="checkbox" 
                                            name="offer-amount-confidential" 
                                            <?php echo ($offerAmountConfidential)? "checked" : "" ?>
                                        >
                                    </div>
                                </div>
                                
                                
                                <!-- Offer Amount -->
                                <div class="row conditional" data-condition="['commission'].includes(payment-type)">
                                    <div class="col-12 form-item">
                                        <label for="commission-amount">
                                            Commission Amount
                                        </label>
                                        <input 
                                            type="number" 
                                            name="commission-amount" 
                                            class="d-block padding-tb-5 padding-lr-10 full-width"
                                            value="<?php echo $commissionAmount ?>"
                                        >
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <label for="commission-amount-confidential" class="txt-normal-s txt-medium padding-r-10">
                                            Confidential
                                        </label>
                                        <input 
                                            type="checkbox" 
                                            name="commission-amount-confidential" 
                                            <?php echo ($commissionAmountConfidential)? "checked" : "" ?>
                                        >
                                    </div>
                                </div>
                            </div>
                                                 
                            <div class="text-right">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                       
                        <?php } elseif($_GET['action'] == 'application-steps') { ?>
                        
                            <h2>
                               How to apply
                            </h2>
                            
                        <?php } elseif($_GET['action'] == 'broadcast') { ?>
                        
                            <h2>
                                Broadcast this Job offer
                            </h2>
                        
                        <?php } else { ?>
                        
                        <form action="<?php echo currentUrl(false); ?>?view=form" method="post">
                            <?php
                                /* Add/Edit Job (Specification) */
                                if($_GET['post-id']){
                                    $post_id = $_GET['post-id'];
                                    
                                    /* Get post */
                                    $post = get_post($post_id);
                                    
                                    /* Get Post data */
                                    $postName = $post->post_title;
                                    $jobType = get_post_meta( $post->ID, 'job-type', true );
                                    $company = get_post_meta( $post->ID, 'company', true );
                                    $jobFunction = get_post_meta( $post->ID, 'job-function', true );
                                    $jobLevel = get_post_meta( $post->ID, 'job-level', true );
                                    $physicalPresence = get_post_meta( $post->ID, 'physical-presence', true );
                                    $deadline = get_post_meta( $post->ID, 'deadline', true );
                                }
                              
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if($_POST){
                                    
                                    /* Get Post Name */
                                    $postName = sanitize_text_field( $_POST['post-name'] );
                                    $jobType = sanitize_text_field( $_POST['job-type'] );
                                    $company = sanitize_text_field( $_POST['company'] );
                                    $jobFunction = sanitize_text_field( $_POST['job-function'] );
                                    $jobLevel = sanitize_text_field( $_POST['job-level'] );
                                    $physicalPresence = sanitize_text_field( $_POST['physical-presence'] );
                                    $deadline = sanitize_text_field( $_POST['deadline'] );
                                                                        
                                    /* Save Post to DB */
                                    $post_id = wp_insert_post(array (
                                        'ID' => $post_id,
                                        'post_type' => $postType,
                                        'post_title' => $postName,
                                        'post_content' => "",
                                        'post_status' => 'publish',
                                    ));
                                    
                                    update_post_meta( $post_id, 'job-type', $jobType );
                                    update_post_meta( $post_id, 'company', $company );
                                    update_post_meta( $post_id, 'job-function', $jobFunction );
                                    update_post_meta( $post_id, 'job-level', $jobLevel );
                                    update_post_meta( $post_id, 'physical-presence', $physicalPresence );
                                    update_post_meta( $post_id, 'deadline', $deadline );

                                    /* Save terms to post */
                                    wp_set_post_terms( $post_id, $_POST['nigerian-state'], 'nigerian-state' );
                                    wp_set_post_terms( $post_id, $_POST['industry'], 'industry' );
                                    wp_set_post_terms( $post_id, $_POST['capability'], 'capability' );
                                    
                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }
                            ?>
                            
                            <div class="form margin-b-40">
                                <!-- Job Type -->
                                <div class="form-item">
                                    <label for="job-type">
                                        Job Type
                                    </label>
                                    <select name="job-type" required>
                                        <option value="fullTime" <?php echo ($jobType == 'fullTime')? "selected" : "" ?>>
                                            Full-time
                                        </option>
                                        <option value="partTime" <?php echo ($jobType == 'partTime')? "selected" : "" ?>>
                                            Part-time
                                        </option>
                                        <option value="freelancer" <?php echo ($jobType == 'freelancer')? "selected" : "" ?>>
                                            Freelancer/Independent Contractor
                                        </option>
                                    </select>
                                </div>
                                <!-- Job Name -->
                                <div class="form-item">
                                    <label for="post-name" class="conditional" data-condition="['fullTime','partTime'].includes(job-type)">
                                        Job Title
                                    </label>
                                    <label for="post-name" class="conditional" data-condition="['freelancer'].includes(job-type)">
                                        What do you need the freelancer to help you do?
                                    </label>
                                    <input 
                                        type="text" 
                                        name="post-name" 
                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                        value="<?php echo $postName ?>"
                                        required
                                    >
                                </div>
                                <!-- Job Name -->
                                <div class="form-item">
                                    <label for="post-name">
                                        Company
                                    </label>
                                    <input 
                                        type="text" 
                                        name="company" 
                                        value="<?php echo $company ?>"
                                    >
                                </div>
                                <!-- Location -->
                                <div>
                                   <?php
                                        $tax_name = 'Location';
                                        $tax_slug = 'nigerian-state';
                                    ?>
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_name; ?>
                                    </h2>

                                    <div class="padding-b-20">
                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_slug, array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                            <input
                                                class="margin-r-5"
                                                type="checkbox" 
                                                value="<?php echo $term_id ?>" 
                                                name="<?php echo $tax_slug ?>[]" 
                                                <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                            >
                                            <span class="bg-label">
                                                <?php echo $term_name; ?>
                                            </span>
                                        </label>

                                    <?php } ?>
                                    </div>
                                </div>
                                <!-- Job Function -->
                                <div class="form-item conditional" data-condition="['fullTime','partTime'].includes(job-type)">
                                    <label for="job-function">
                                        Job Function
                                    </label>
                                    <input 
                                        type="text" 
                                        name="job-function" 
                                        class="d-block padding-tb-5 padding-lr-10 full-width"
                                        value="<?php echo $jobFunction ?>"
                                    >
                                </div>
                                <!-- Industries-->
                                <div class="conditional" data-condition="['fullTime','partTime'].includes(job-type)">
                                   <?php
                                        $tax_name = 'Industries';
                                        $tax_slug = 'industry';
                                    ?>
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_name; ?>
                                    </h2>

                                    <div class="padding-b-20">
                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_slug, array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                            <input
                                                class="margin-r-5"
                                                type="checkbox" 
                                                value="<?php echo $term_id ?>" 
                                                name="<?php echo $tax_slug ?>[]" 
                                                <?php echo in_array($term_id, $term_list) ? "checked" : "" ?>
                                            >
                                            <span class="bg-label">
                                                <?php echo $term_name; ?>
                                            </span>
                                        </label>

                                    <?php } ?>
                                    </div>
                                </div>
                                <!-- Job Level -->
                                <div class="form-item conditional" data-condition="['fullTime','partTime'].includes(job-type)">
                                    <label for="job-type">
                                        Job Level
                                    </label>
                                    <select name="job-level">
                                        <option value="internship" <?php echo ($jobLevel == 'internship')? "selected" : "" ?>>
                                            Internship
                                        </option>
                                        <option value="entry" <?php echo ($jobLevel == 'entry')? "selected" : "" ?>>
                                            Entry Level
                                        </option>
                                        <option value="officer" <?php echo ($jobLevel == 'officer')? "selected" : "" ?>>
                                            Officer Level
                                        </option>
                                        <option value="mid" <?php echo ($jobLevel == 'mid')? "selected" : "" ?>>
                                            Mid Level
                                        </option>
                                        <option value="management" <?php echo ($jobLevel == 'management')? "selected" : "" ?>>
                                            Management Level
                                        </option>
                                    </select>
                                </div>
                                <!-- Type of work -->
                                <div class="conditional" data-condition="['freelancer'].includes(job-type)">
                                    <?php
                                        $tax_name = 'Type of Work';
                                        $tax_slug = 'capability';
                                    ?>
                                    <h2 class="txt-medium txt-color-blue margin-b-20">
                                        Select <?php echo $tax_name; ?>
                                    </h2>

                                    <?php 
                                        /* Return Terms assigned to Post */
                                        $term_list = wp_get_post_terms($post_id, $tax_slug, array("fields" => "ids")); 

                                        /*
                                        *
                                        * Populate Form Data from Terms
                                        *
                                        */
                                        //Get Terms
                                        $terms = get_terms( $tax_slug, array('hide_empty' => false));

                                        foreach ($terms as $term) { //Cycle through terms, one at a time

                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $parent = $term->parent;
                                            $term_id = $term->term_id; //Get the term ID
                                            $term_name = $term->name; //Get the term name
                                    ?>

                                        <?php if( $parent == 0 ){ ?>

                                            <div class="padding-b-20">
                                                <div class="txt-medium txt-color-dark margin-b-15">
                                                    <?php echo $term_name; ?>
                                                </div>
                                                <?php
                                                    foreach ($terms as $child_term) {
                                                        // Check and see if the term is a top-level parent. If so, display it.
                                                        $child_parent = $child_term->parent;
                                                        $child_term_id = $child_term->term_id; //Get the term ID
                                                        $child_term_name = $child_term->name; //Get the term name

                                                        if( $child_parent == $term_id ){
                                                ?>
                                                        <label class="checkbox-item txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                            <input
                                                                class="margin-r-5"
                                                                type="checkbox" 
                                                                value="<?php echo $child_term_id ?>" 
                                                                name="<?php echo $tax_slug ?>[]" 
                                                                <?php echo in_array($child_term_id, $term_list) ? "checked" : "" ?>
                                                            >
                                                            <span class="bg-label">
                                                                <?php echo $child_term_name; ?>
                                                            </span>
                                                        </label>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </div>

                                        <?php } ?>

                                    <?php } ?>
                                </div>
                                <!-- Physical Presence -->
                                <div class="form-item conditional" data-condition="['freelancer'].includes(job-type)">
                                    <label for="job-type">
                                        Does the job require candidate's physical presence?
                                    </label>
                                    <select name="physical-presence">
                                        <option value="yes" <?php echo ($physicalPresence == 'yes')? "selected" : "" ?>>
                                            Yes
                                        </option>
                                        <option value="no" <?php echo ($physicalPresence == 'no')? "selected" : "" ?>>
                                            No. Candidate can work from anywhere
                                        </option>
                                        <option value="flexible" <?php echo ($physicalPresence == 'flexible')? "selected" : "" ?>>
                                            Flexible
                                        </option>
                                    </select>
                                </div>
                                <!-- Deadline -->
                                <div class="form-item conditional" data-condition="['freelancer'].includes(job-type)">
                                    <label for="deadline">
                                        Deadline to deliver job
                                    </label>
                                    <input type="date" name="deadline" value="<?php echo $deadline ?>">
                                </div>
                            </div>
                            
                            <!--<div class="margin-b-40">
                                <p>
                                    <label>
                                        <input type="checkbox" id="example1"> 
                                        Are you sure?
                                    </label>
                                </p>
                                
                                <p class="conditional" data-condition="#example1">
                                    <label>
                                        <input type="checkbox" name="example2"> 
                                        Really super sure?
                                    </label>
                                </p>
                                <p class="conditional" data-condition="#example1 && example2">
                                    <label>Then type "yay": </label>
                                    <input type="text" id="example3" placeholder="yay">
                                </p>
                                 This will be shown only if BOTH examle1 and examle2 are checked AND 'yay' typed in examle3 
                                <p class="conditional msg" data-condition="#example1 && example2 && #example3 == 'yay'">
                                   Both are selected and YAY is typed!
                                </p>
                                <p>
                                    <label>Pick two or three:</label>
                                    <select class="select" name="example5">
                                        <option>....</option>
                                        <option value="one">One!</option>
                                        <option value="two">Two!</option>
                                        <option value="three">Three!</option>
                                        <option value="four">Four!</option>
                                    </select>
                                </p>
                                <div class="conditional msg" data-condition="['two','three'].includes(example5)">
                                   See?! It works with selects!
                                </div>
                            </div>-->

                                                 
                            <div class="text-right">
                                <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                            </div>
                        </form>
                        
                        <?php } ?>
                    </div>
                
                <?php } ?>  
                </div>
               
            <?php } else { ?>
               
                <div class="page-header">
                    <h1 class="page-title">
                        My Job Posts
                    </h1>
                    <div class="cta">
                        <!--<a href="<?php echo currentUrl(true); ?>?view=form&gf-id=5&form-title=Add a Job" class="cta-btn">
                            Add Job
                        </a>-->                        
                        <a href="<?php echo currentUrl(false); ?>?view=form&form-title=Add a Job" class="cta-btn">
                            Add Job
                        </a>
                    </div>
                </div>
                
                <article class="page-summary">
                    <p>
                        You could add Job offers here, and also view all the previous job you have posted.
                    </p>
                </article>
                
                <ul class="row row-10 marketplace-job-cards">
                <style>
                    .publication-status{
                        position: absolute;
                        display: flex;
                        align-items: center;
                        top: -15px;
                        right: -15px;
                        padding: 5px 10px;
                        color: black;
                        background-color: gainsboro;
                    }

                    .user_published{
                       background-color: #f4c026; 
                    }

                    .admin_published{
                       background-color: #00bfe7; 
                    }

                    .home-community-card{
                        overflow: visible;
                    }
                </style>
                <?php
                    /*
                    *==================================================================================
                    *==================================================================================
                    *   WP Query
                    *==================================================================================
                    *==================================================================================
                    */
                    // Define Meta Args
                    /*$meta_array = array(
                        'relation' => 'AND',
                        array (
                            'key' => 'request-what-need', //Meta Key
                            'value' => 'A skilled person for a Job', //Meta Value
                        ),
                    );

                    // Add Arguements from _REQUEST
                    foreach($_GET as $key => $value){

                        /* Check & Exclude:
                                - S ( WP Search field )
                                - Search-type ( For selecting Search Template View to use )
                                - Empty values

                        if($key !== 's' && $key !== 'search-type' && $key !== 'view' && !empty($value) && $value != 'all'){

                            // Create Tax Array entry
                            $category_array = array(

                                // Add Term Term Values
                                array (
                                    'key' => trim($key), //Texanomy Type
                                    'value' => $value, //Search field
                                ),

                            );

                            // Add just created Array to Tax Array. 
                            $meta_array = array_merge($meta_array, $category_array); 
                        }
                    }*/

                    // Create Query Argument
                    $args = array(
                        'post_type' => 'job',
                        'showposts' => -1,
                        'author' => $current_user->ID,
                        'meta_query' => $meta_array,
                    );


                    $wp_query = new WP_Query($args);

                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    /* Get Post ID */
                    $post_id = $post->ID;
                          
                    /* Publication status */      
                    $publication_meta = get_post_meta( $post_id, $publication_key, true );
                ?>
                    <li class="col-sm-6 col-lg-4 padding-lr-10 padding-b-20 d-flex">
                        <div class="">
                            <div class="content position-relative">
                                <div class="title padding-t-40">
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </div>
                                <div class="employer">
                                    <?php
                                        $field = 'job_post_organization_name';

                                        $meta = get_post_meta($post->ID, $field, true);

                                        if($meta){
                                            echo $meta;
                                        }
                                    ?>
                                </div>
                                <div class="location">
                                    <?php
                                        $field = 'job_post_locations';

                                        $meta = get_post_meta($post->ID, $field, true);

                                        if($meta){
                                            echo $meta;
                                        }
                                    ?> | Full Time
                                </div>
                                <div class="post-time">
                                    Posted 2 days ago
                                </div>
                                <p class="txt-sm txt-color-dark padding-t-20">
                                    <?php 
                                        $response_gf_id = 13; //Form ID

                                        /* GF Search Criteria */
                                        $response_search_criteria = array(

                                        'field_filters' => array( //which fields to search

                                            array(

                                                'key' => '2', 'value' => $post_id, //Current logged in user
                                                )
                                            )
                                        );

                                        /* Get Entries */
                                        $response_entries = GFAPI::get_entries( $response_gf_id, $response_search_criteria );

                                        /* Get GF Entry Count */
                                        $response_entry_count = GFAPI::count_entries( $response_gf_id, $response_search_criteria );
                                    ?>
                                    <span class="txt-bold">
                                        <?php echo $response_entry_count ?>
                                    </span>
                                    Application(s)
                                </p>
                                <div class="d-flex align-items-center justify-content-between txt-sm padding-t-20">
                                    <div>
                                        <a 
                                           href="<?php printf('%s?view=form&post-id=%s&form-title=Edit %s', currentUrl(true), $post_id, get_the_title() ) ?>"
                                           class="txt-medium txt-color-green"
                                        >
                                            Edit
                                        </a>
                                        |
                                        <a 
                                           href="<?php printf('%s/?view=form&action=delete&post-id=%s&action=delete', currentUrl(true), $post_id ) ?>"
                                           class="confirm-delete txt-color-red"
                                        >
                                            Delete
                                        </a>
                                    </div>
                                    <div class="publication-status <?php echo $publication_meta ?>">
                                        <?php
                                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                                            if($publication_meta == 'user_published'){
                                                echo 'Undergoing review';
                                            } elseif($publication_meta == 'admin_published') {
                                                echo 'Published';
                                            }else{
                                                echo 'Unpublished';
                                            }
                                        ?>
                                        <div class="dropdown padding-l-20">
                                            <a class="" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa-stack">
                                                    <i class="fa fa-circle fa-stack-2x txt-color-white"></i>
                                                    <i class="fa fa-ellipsis-v fa-stack-1x"></i>
                                                </span>
                                            </a>

                                            <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuLink">

                                                <a 
                                                   href="<?php printf('%s/?view=form&action=job-description&post-id=%s&form-title=%s', currentUrl(true), $post_id, 'Job Description' ) ?>" 
                                                   class="dropdown-item"
                                                >
                                                    Job Description
                                                </a>
                                                <a 
                                                   href="<?php printf('%s/?view=form&action=application-steps&post-id=%s&form-title=%s', currentUrl(true), $post_id, 'How to Apply' ) ?>" 
                                                   class="dropdown-item"
                                                >
                                                    How to Apply
                                                </a>
                                                <a 
                                                   href="<?php printf('%s/?view=form&action=broadcast&post-id=%s&form-title=%s', currentUrl(true), $post_id, 'Broadcast Job' ) ?>" 
                                                   class="dropdown-item"
                                                >
                                                    Broadcast Job
                                                </a>
                                                <a 
                                                   href="<?php printf('%s/?view=form&action=publication&post-id=%s', currentUrl(true), $post_id ) ?>"
                                                   class="dropdown-item"
                                                >
                                                    <?php

                                                        if($publication_meta == 'user_published'){
                                                            echo 'Unpublish';
                                                        } elseif($publication_meta == 'admin_published') {
                                                            echo 'Unpublish';
                                                        }else{
                                                            echo 'Publish';
                                                        }
                                                    ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </li>

                <?php
                    endwhile;
                ?>
                </ul>
                
            <?php } ?> 
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>


<script>
    $('.conditional').conditionize();
</script>