<?php /*Template Name: Profile - Enquiry: Opportunities*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content full">
                <section class="section-wrapper">
                    <div class="page-header">
                        <h1 class="page-title">
                            Opportunity Enquiries
                        </h1>
                    </div>
                    <article class="page-summary">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium culpa, ullam placeat quisquam aliquid nesciunt incidunt doloribus nobis animi.
                        </p>
                    </article>
                    <div class="entry overflow-hidden">
                       <?php

                            $get_search_fields = array( //which fields to search
                                'mode' => 'any',   
                            );

                            /* GF Search Criteria */
                            $gf_id = 1;
                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                )
                              )
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $get_search_field = array(
                                    array(
                                        'key' => '2',
                                        'value' => $entry['post_id'], //Current logged in user
                                    )
                                );

                                $get_search_fields = array_merge($get_search_fields, $get_search_field);

                            }
                        ?>


                        <div class="row row-15">
                        <?php
                            /* GF Search Criteria */
                            $gf_id = 2;
                            $parent_post_field = 2;
                            /* GF Search Criteria */
                            $search_criteria = array(

                                'field_filters' => $get_search_fields
                            );

                            /* Get GF Entry Count */
                            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );


                            foreach( $entries as $entry ){

                                $parent_post_id = rgar( $entry, $parent_post_field );
                                $parent_post = get_post($parent_post_id);

                                $author_id = $entry['created_by'];


                                 /* Get User Display Name */
                                switch_to_blog(1);

                                $user_gf_id = 4; //Form ID

                                /* GF Search Criteria */
                                $user_search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $author_id, //Current logged in user
                                        )
                                    )
                                );

                                /* Get Entries */
                                $user_entries = GFAPI::get_entries( $user_gf_id, $user_search_criteria );

                                /* Get GF Entry Count */
                                $user_entry_count = GFAPI::count_entries( $user_gf_id, $user_search_criteria );

                                if($user_entry_count){ //If no entry
                                    foreach( $user_entries as $user_entry ){          
                                        $displayname = rgar( $user_entry, '4.3' ).' '.rgar( $user_entry, '4.6' );
                                    }                
                                }   

                                restore_current_blog();
                        ?>

                            <div class="col-md-6 padding-lr-15 padding-b-30 d-flex">
                                <div class="flex_1 padding-o-20 border-o-1 border-color-darkgrey">
                                    <h3 class="margin-b-10 txt-lg txt margin-b-10">
                                         <a class="txt-color-blue" href="<?php the_permalink($parent_post) ?>"><?php echo $parent_post->post_title; ?></a>
                                    </h3>
                                    <div class="txt-normal-s margin-b-20">
                                        <p>
                                            <?php echo rgar( $entry, '3' ) ?>
                                        </p>
                                    </div>
                                    <div class="txt-xs txt-medium">
                                        <div class="txt-color-dark" style="text-transform:capitalize">
                                            by
                                            <?php echo ($post->post_author == 1)? 'SAEDConnect Admin' : $displayname; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        </div>
                    </div>
                </section>   
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>