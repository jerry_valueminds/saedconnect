<?php /*Template Name: Mentor / Service Provider Profiles*/ ?>
    
<?php get_header() ?>
   
<main class="main-content font-main">
    <section class="container">
        <div class="col-md-8 mx-auto">
            <h1 class="txt-2em txt-medium margin-b-20">
                My Mentor and Service Provider Profiles
            </h1>

            <?php
                // Get Base URL
                $base_url = get_site_url().'/mentor-service-provider-forms';

                // Get Currently logged in User
                $current_user = wp_get_current_user();
            ?>  

            <header class="txt-light">
                <h1 class="txt-2em txt-height-1-2 margin-b-20">
                    Welcome,
                    <?php

                        echo $current_user->display_name;
                    ?>
                </h1>
                <h2 class="txt-lg txt-height-1-7 margin-b-20">
                    Technology roles are becoming one of the highest-paying jobs globally, and you don’t require a college degree to be a tech star. The Tech Career Mentorship Scheme is designed to help people across all backgrounds to start up a brilliant career in tech related areas.
                </h2>
            </header>

           <section class="margin-b-20">
                <div class="container">
                    <div class="border-o-1 border-color-darkgrey padding-tb-20 padding-lr-30">
                        <p class="txt-bold margin-b-30">
                            Add New Profile
                        </p>
                        <?php echo do_shortcode('[gravityform id="79" title="false" description="false"]'); ?>
                   </div>
                </div>
            </section>
           
            <div id="main-1" class="collapse show" aria-labelledby="heading-1" data-parent="#accordion">                                   
            <?php

                $form_id = 79;  // Get eSAED Registration Form ID

                // Prepare Search Parameters
                $search_criteria = array(
                    'status'        => 'active',
                    'field_filters' => array(
                        'created_by' => $current_user->ID
                    )
                );

                // Search for entries for this User
                $entries = GFAPI::get_entries( $form_id );

            ?>

            <?php if($entries){ ?>

                <?php foreach($entries as $entry){ // Start foreach ?>

                    <?php    if( $entry['created_by'] == $current_user->ID ){ ?>
                        <?php
                            //var_dump($entry);
                            $entry_id = $entry['id'];
                            $post = get_post($entry['post_id']);
                            //var_dump($post);
                        ?>

                        <section class="margin-b-20">
                            <div class="border-o-1 border-color-darkgrey padding-tb-20 padding-lr-30">
                                <h2 class="txt-bold">
                                   <?php echo $post->post_title ?>
                                </h2>
                               <div class="row margin-t-20">
                                    <a class="btn btn btn-trans-bw txt-normal-s margin-r-10" href="<?php echo get_post_permalink($post->ID) ?>">
                                        View Profile
                                    </a>                                            
                                    <form action="<?php echo $base_url ?>/?action=bank-information-form" method="post">
                                        <button class="sticky-list-edit submit btn btn btn-trans-bw txt-normal-s">Edit Profile</button>
                                        <input type="hidden" name="mode" value="edit">
                                        <input type="hidden" name="edit_id" value="<?php echo $entry_id; ?>">
                                    </form>
                                </div>
                           </div>
                        </section>

                    <?php } //End If ?>


                <?php } //  End Foreach ?>


            <?php } else { ?>
                <div class="border-o-1 border-color-darkgrey padding-tb-20 padding-lr-30 margin-b-40">
                    <p class="txt-bold">
                        You do not have any Profiles
                    </p>
                </div>

                <!--<div class="margin-t-40">
                    <a class="btn btn-green txt-normal-s" href="<?php echo $base_url ?>/?action=bank-information-form">
                        Add CV
                    </a>
                </div>-->

            <?php } ?>
            
            
            </div>
        </div>  
    </section> 
</main>
        
<?php get_footer() ?>