<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require 'rest/config/database.php';
require 'rest/services/corpers.php';

$headers = getallheaders();
$auth = $headers['Authorization'];
 
$db  = new Database();
$corper = new Corpers($db->getConnection());
 
// get posted data
$body = json_decode(file_get_contents("php://input"));
 

$requestRef = $body->requestRef;

$hash = md5("12SC345;".$requestRef);

if($auth != "Bearer ". $hash){
    echo json_encode(array("statusCode"=>"01","statusMessage"=>"Invalid Authorization","responseMessage"=>"","Hash"=>$hash));
}

 //Verify if all fields was supplied
 if(empty($body->firstName) || empty($body->lastName) || empty($body->middleName) || empty($body->gender)
 || empty($body->callUpNumber) || empty($body->stateCode) || empty($body->courseOfStudy) || empty($body->school) || empty($body->email) || empty($body->phone || !$body->batch ||
!$body->stream)){
 echo json_encode(array("statusCode"=>"06","statusMessage"=>"Missing fields","responseMessage"=>"All fields are required","Hash"=>$hash));
}
else{
    $corper->firstName = $body->firstName;
    $corper->middleName = $body->middleName;
    $corper->lastName = $body->lastName;
    $corper->gender = $body->gender;
    $corper->callUpNumber = $body->callUpNumber;
    $corper->stateCode = $body->stateCode;
    $corper->courseOfStudy = $body->courseOfStudy;
    $corper->school = $body->school;
    $corper->email = $body->email;
    $corper->phone = $body->phone;
    $corper->batch = $body->batch;
    $corper->stream = $body->stream;
    
    //Verify if callup number already exists
    
    $callUpNumberExists = $corper->callNumberExists($corper->callUpNumber);
    if($callUpNumberExists){
        echo json_encode(array("statusCode"=>"01","statusMessage"=>"Duplicate Records","responseMessage"=>$corper->callUpNumber ." already exists","Hash"=>$hash));
    
    }
    else{
        $res = $corper->Create();
        if($res=="true"){
            echo json_encode(array("statusCode"=>"00","statusMessage"=>"Successful","responseMessage"=>"
            Thanks for registering on SAEDCONNECT. To complete your registration, please check your email and click on the link which has been sent to you",
                "Hash"=>$hash));
        }
        else{
            echo json_encode(array("statusCode"=>"01","statusMessage"=>"Failed","responseMessage"=>$res,"Hash"=>$hash));
        }
    }
}





   

?>