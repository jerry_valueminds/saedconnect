<?php
    /* Check if User is logged in */              
    /*if ( !is_user_logged_in() ) {
        // If User is Logged in, redirect to User Dashbord
        $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID

        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }*/

    /* Check if User does not have level */
    //$current_user = wp_get_current_user();

?>

<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php 
    /* Get Variables */
    $forum_id = $post->ID;
    $forum_title = $post->post_title;
    $forum_parent = $post->post_parent;
?>

<main class="main-content">
    <style>
        .bbp-breadcrumb{
            /*display: none !important;*/
        }
    </style>

    <header class="overview-header container-wrapper">
        <div class="row padding-tb-20">
            <div class="col-md-10">
                <h1 class="txt-2em txt-medium txt-height-1-2 padding-b-20">
                    <?php bbp_forum_title(); //echo ' - '.pmpro_hasMembershipLevel('1', $current_user->id); ?>
                </h1>
                <p class="txt-color-light txt-light txt-normal-s">
                    <?php echo bbp_forum_content(); ?>
                </p>
                <p class="txt-normal-s">
                    <a class="txt-color-blue" href="https://www.saedconnect.org/help-center/forums/topic/community-guidelines-read-this-first/" target="_blank">
                        <i class="fa fa-info padding-r-10"></i>
                        Read Community Guidelines here.
                    </a>
                </p>
            </div>
        </div>
        <div class="row">
	        <?php bbp_breadcrumb(); ?>
	    </div>
    </header>
    
    <?php
        /* Get Forum Category */
        $forum_category = rwmb_meta( 'forum_category', array( 'object_type' => 'setting' ), 'my_options' );
    
        /* Get forum theme color */
        $forum_theme = rwmb_meta( 'forum-theme-color', array( 'object_type' => 'setting' ), 'my_options' );
        $forum_theme_alt = rwmb_meta( 'forum-alt-theme-color', array( 'object_type' => 'setting' ), 'my_options' );
    
        /* Get Action Button theme color */
        $button_bg = rwmb_meta( 'action-btn-background', array( 'object_type' => 'setting' ), 'my_options' );
        $button_color = rwmb_meta( 'action-btn-color', array( 'object_type' => 'setting' ), 'my_options' );
    
        /* Action Bar Variables */
        $action_bar_var = array(
            'forum_theme' => $forum_theme,
            'forum_theme_alt' => $forum_theme_alt,
            'button_bg' => $button_bg,
            'button_color' => $button_color,
        );
    
        /* Pass action bar variables */
        set_query_var('action_bar_var', $action_bar_var);
    
        /* Get Mentor Title */
        $title = rwmb_meta( 'bbp-mentor-forum-title', $post->ID );

        /* Get first letter of title */
        $sub = substr($title, 0, 1);

        /* Check if Vowel or Consonant */
        $isVowel = false;

        if( $sub == 'a' || $sub == 'e' || $sub == 'i' || $sub == 'o' || $sub == 'u' ){
            $isVowel = true;
        }

        if( true ):
/*
        if( $title ):
*/
            /* Get Action Bar */
            if($forum_category){
                get_template_part( 'template-parts/forum-action-bar/'.$forum_category );
            }
    
        endif; 
    ?>
        
    <section class="bg-grey padding-b-80">


        <?php

        /**
         * Single Forum Content Part
         *
         * @package bbPress
         * @subpackage Theme
         */

        ?>

        <?php //bbp_forum_subscription_link(); ?>

        <?php do_action( 'bbp_template_before_single_forum' ); ?>

        <?php if ( post_password_required() ) : ?>

            <?php bbp_get_template_part( 'form', 'protected' ); ?>

        <?php else : ?>

            <?php //bbp_single_forum_description(); ?>

            <?php if ( bbp_has_forums() ) : ?>

                <?php bbp_get_template_part( 'loop', 'forums' ); ?>

            <?php endif; ?>

            <?php if ( !bbp_is_forum_category() && bbp_has_topics() ) : ?>
               
                <!--<div class="container-wrapper">
                    <div class="padding-t-20 padding-b-20">
                        <a class="btn btn-blue txt-sm no-m-b" data-toggle="collapse" href="#topic-form-1" role="button" aria-expanded="false" aria-controls="topic-form-1">
                            Add Topic
                        </a>
                    </div>
                </div>
                
                <div class="collapse" id="topic-form-1">

                    <?php // bbp_get_template_part( 'form',       'topic'     ); ?>
                
                </div>-->

                <?php bbp_get_template_part( 'loop',       'topics'    ); ?>
                
                <div class="container-wrapper">
                    <div class="padding-t-40 padding-b-20">
                        <a class="btn btn-blue txt-sm no-m-b" data-toggle="collapse" href="#topic-form-2" role="button" aria-expanded="false" aria-controls="topic-form-2">
                            Add Topic
                        </a>
                    </div>
                </div>
               
                <div class="collapse" id="topic-form-2">
                    <div class="margin-b-40">
                    <?php 
                        if( is_user_logged_in() ){

                            bbp_get_template_part( 'form',       'topic'     );

                        } else { 
                    ?>
                        
                        <!-- Login Form -->
                        <style>
                            .gfield_checkbox label, .login-form .gfield_label{
                                display: block !important;
                            }

                            .gfield_checkbox li{
                                display: flex;
                                align-items: center;
                            }

                            .gfield_checkbox input{
                                margin: 0 !important;
                                margin-right: 10px !important;
                            }
                        </style>
    
                        <section class="container-wrapper topic">
                            <div class="row">
                                <div class="col-md-4">
                                    <?php
                                        switch_to_blog(1);
                                    ?>

                                    <div class="form-toggle">
                                        <!-- Login Tab -->
                                        <div class="login">
                                            <p class="margin-b-40">
                                                You need to login to add a topic. Don't have an account?                                
                                                <a class="toggle-btn txt-color-blue remember-page" href="https://www.saedconnect.org/register">
                                                    Click Here to get started.
                                                </a>
                                            </p>
                                            <div class="login-form">
                                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                                                <?php endif;?>
                                            </div>
                                        </div>

                                        <!-- Signup Tab -->
                                        <div class="register">
                                            <p class="margin-b-40">
                                                You need to login to add a topic. Already have an account?
                                                <a class="toggle-btn txt-color-blue" href="#">
                                                    Click here Login.
                                                </a>
                                            </p>
                                            <div class="login-form">
                                                <?php
                                                    echo do_shortcode( "[gravityform id='4' title='false' description='false' ajax='false']"); 
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>

                                </div>
                            </div>
                        </section>
                        
                        <style>
                            .register{
                                display: none;
                            }
                        </style>

                        <script>
                            $(document).ready(function() {
                                /*function ButtonText() {
                                    if ($(".form-toggle .login").is(":visible")) {
                                        $(".form-toggle .toggle-btn").text("Click Here to get started.");
                                    } else {
                                        $(".form-toggle .toggle-btn").text("Click here Login.");
                                    }
                                }

                                ButtonText();

                                $(".form-toggle .toggle-btn").click(function () {
                                    $(".form-toggle .login").toggle();
                                    $(".form-toggle .register").toggle();
                                    ButtonText();
                                });*/
                            });
                        </script>
                        
                    <?php
                        } 
                    ?>
                    </div>
                </div>
                
                <?php bbp_get_template_part( 'pagination', 'topics'    ); ?>
                

            <?php elseif ( !bbp_is_forum_category() ) : ?>

                <?php bbp_get_template_part( 'feedback',   'no-topics' ); ?>
                
                <div class="container-wrapper">
                    <div class="padding-t-20 padding-b-40">
                        <a class="btn btn-blue txt-sm no-m-b" data-toggle="collapse" href="#topic-form-2" role="button" aria-expanded="false" aria-controls="topic-form-2">
                            Add Topic
                        </a>
                    </div>
                </div>
               
                <div class="collapse" id="topic-form-2">
                    <div class="margin-b-40">
                    <?php 
                        if( is_user_logged_in() ){

                            bbp_get_template_part( 'form', 'topic' );

                        } else { 
                    ?>
                       
                        <!-- Login Form -->
                        <style>
                            .gfield_checkbox label, .login-form .gfield_label{
                                display: block !important;
                            }

                            .gfield_checkbox li{
                                display: flex;
                                align-items: center;
                            }

                            .gfield_checkbox input{
                                margin: 0 !important;
                                margin-right: 10px !important;
                            }
                        </style>
    
                        <section class="container-wrapper topic">
                            <div class="row">
                                <div class="col-md-4">
                                    <?php
                                        switch_to_blog(1);
                                    ?>

                                    <div class="form-toggle">
                                        <!-- Login Tab -->
                                        <div class="login">
                                            <p class="margin-b-40">
                                                You need to login to add a topic. Don't have an account?  ?                              
                                                <a class="toggle-btn txt-color-blue remember-page" href="https://www.saedconnect.org/register">
                                                    Click Here to get started.
                                                </a>
                                            </p>
                                            <div class="login-form">
                                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                                                <?php endif;?>
                                            </div>
                                        </div>

                                        <!-- Signup Tab -->
                                        <div class="register">
                                            <p class="margin-b-40">
                                                You need to login to add a topic. Already have an account?
                                                <a class="toggle-btn txt-color-blue" href="#">
                                                    Click here Login.
                                                </a>
                                            </p>
                                            <div class="login-form">
                                                <?php
                                                    echo do_shortcode( "[gravityform id='4' title='false' description='false' ajax='false']"); 
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                        //Revert to Previous Multisite
                                        restore_current_blog();
                                    ?>

                                </div>
                            </div>
                        </section>
                        
                        <style>
                            .register{
                                display: none;
                            }
                        </style>

                        <script>
                            $(document).ready(function() {
                                /*function ButtonText() {
                                    if ($(".form-toggle .login").is(":visible")) {
                                        $(".form-toggle .toggle-btn").text("Click Here to get started.");
                                    } else {
                                        $(".form-toggle .toggle-btn").text("Click here Login.");
                                    }
                                }

                                ButtonText();

                                $(".form-toggle .toggle-btn").click(function () {
                                    $(".form-toggle .login").toggle();
                                    $(".form-toggle .register").toggle();
                                    ButtonText();
                                });*/
                            });
                        </script>
                        
                    <?php
                        } 
                    ?>
                    </div>
                </div>

            <?php endif; ?>

        <?php endif; ?>

        <?php do_action( 'bbp_template_after_single_forum' ); ?>

    </section>
    
    <section class="other-forums-sticky container-wrapper bg-dark padding-tb-10 row align-items-center">
        <h4 class="col-7 txt-medium txt-color-white">
        <?php
            $value = rwmb_meta( 'forum_category', array( 'object_type' => 'setting' ), 'my_options' );

            if( $value == '_shc_modal' ){

                echo 'Explore other businesses';

            } else if ( $value == '_help_center_modal' ){

                echo 'Explore other Help Topics';

            } else {

                echo 'Explore other Tracks';
            }
        ?>
        </h4>
        <div class="col-5 text-right">
            <a class="btn btn-trans-wb txt-sm no-m-b" data-toggle="modal" href="#comingSoonModal">
                Explore
            </a>
        </div>
    </section>
</main>

<?php endwhile; // end of the loop. ?>

<?php get_footer('user-dashboard') ?>

<!-- SHC Modal -->
<?php
    /* Get related forums modal */
    if($forum_category){
        get_template_part( 'template-parts/'.$forum_category );
    }
?>


<!-- First Time Visiting Modal -->
<div class="modal fade font-main coming-soon-modal show" id="firstTime" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="padding-o-60">
                <h3 class="txt-lg txt-bold txt-height-1-5 margin-b-15">
                    You are one of the first visitors in the <?php echo $forum_title ?>, and we are delighted to have you here.
                </h3>
                <article class="text-box sm margin-b-20">
                    <p>
                        Do not be surprised if you don't see much yet. The community is just growing, and your input is important.
                    </p>
                    <p>
                        Before you go, Make sure you:
                    </p>
                </article>
                <ul class="txt-normal-s margin-b-20">
                    <li class="d-flex margin-b-15">
                        <i class="fa fa-chevron-right"></i>
                        <span class="flex_1 txt-height-1-5 padding-l-15">
                             Ask that question that is bothering you, and maybe the next person who visits will have your answer.
                        </span>
                    </li>
                    <li class="d-flex margin-b-15">
                        <i class="fa fa-chevron-right"></i>
                        <span class="flex_1 txt-height-1-5 padding-l-15">
                            Share any useful information you have about <?php echo $forum_title ?> business so that the next person who visits can learn.
                        </span>
                    </li>
                </ul>
                <article class="text-box sm margin-b-20">
                    <p>
                        Thank you.
                    </p>
                </article>
                <button type="button" class="btn btn-blue txt-sm no-m-b" data-dismiss="modal" aria-label="Close">
                    Continue
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Confirmation Modal -->
<div class="modal fade font-main coming-soon-modal show" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row content">
                <div class="left gf-white full-width">
                    <div class="gform_heading">
                        <h3 class="txt-bold txt-height-1-5 text-center">
                            <?php echo $_GET['confirmation'] ?>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    /* First Time Visiting */
    if ( $forum_parent == 0 ) {
?>
   
    <script type="text/javascript">
        $(window).on('load',function(){
            
            /* If User has visited before, Cookie Exists */
            if( !Cookies.get('<?php echo $forum_id ?>') ){
                /* Open Confirmation Modal */
                $('#firstTime').modal('show');
            }
            
        });
        
        /* When User clicks continue, Create a Cookie for that Forum */
        $('#firstTime').on('hidden.bs.modal', function (e) {
            Cookies.set('<?php echo $forum_id ?>', 'visited');
        });
    </script>
    
<?php 
        
    } 
?>


<?php
    /* On form submission, show confirmation */
    if ( $_GET['confirmation'] ) {
?>

    <script type="text/javascript">
        $(window).on('load',function(){

            /* Open Confirmation Modal */
            $('#confirmationModal').modal('show');

            /* Clean URL (Remove Confirmation Query String)  */
            var uri = window.location.toString();

            if (uri.indexOf("?") > 0) {

                var clean_uri = uri.substring(0, uri.indexOf("?"));

                window.history.replaceState({}, document.title, clean_uri);

            }
        });
    </script>

<?php 
        $_GET = array();
    } 
?>

<!-- Pre-registrstion Modal -->
<?php
    switch_to_blog(20);
?>

<div class="modal fade font-main coming-soon-modal" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row content">
                <div class="left gf-white full-width" style="background-color:<?php echo $theme_color ?>;">
                    <section id="get-started">
                        <header class="padding-b-20 text-center" style="color:<?php echo $theme_alt_color ?>;">
                            <h1 class="txt-2-2em txt-light txt-height-1-1 margin-b-20">
                                Start you journey to financial freedom through <?php echo $forum_title ?> business
                            </h1>
                            <h2 class="txt-normal-s txt-height-1-7">
                                You don't have to walk alone. Tell us what small business you want to start to build & we will take it from there.
                            </h2>
                        </header>
                        <div class="">
                            <!-- Local -->
                            <?php //echo do_shortcode('[gravityform id="101" title="false" description="false" field_values="business-name='.$session_title.'"]'); ?>

                            <!--Online-->
                            <?php echo do_shortcode('[gravityform id="21" title="false" description="false" ]'); ?>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    //Revert to Previous Multisite
    restore_current_blog();
?>
