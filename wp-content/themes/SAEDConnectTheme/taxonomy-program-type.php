<?php get_header() ?>

	<main class="main-content">
		<header class="overview-header container-wrapper">
            <div class="info-box">
                <div class="info">
                    <h1 class="title txt-color-lighter">
                        Programs
                        
                        <?php
                            /* Get Term Slug */
                            $queried_object = get_queried_object();
                            
                            /* Check if this term is a parent or child */
                            if($queried_object->parent == 0){
                                /* Term is Parent */
                                $parent_term_id = $queried_object->term_id;
                            }else{
                                /* Term is a child */
                                $parent_term_id = $queried_object->parent;
                                $child_term_id = $queried_object->term_id;
                                $child_term_name = $queried_object->name;
                            }

                        ?>
                    </h1>
                </div>
            </div>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <li>
                        <a href="http://www.saedconnect.org/programs/">
                            All Programs  
                        </a>
                    </li>
                <!-- Get All Magazine Terms -->
                <?php
                    //Get Term name
                    $category_name = single_term_title("", false);;
                    //Get Terms
                    $terms = get_terms( 'program-type', array('hide_empty' => false,)); //Get all the terms

                    foreach ($terms as $term) { //Cycle through terms, one at a time

                        // Check and see if the term is a top-level parent. If so, display it.
                        $parent = $term->parent;

                        if ( $parent=='0' ) {

                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                            $term_url = get_term_link($term);
                            
                            if($parent_term_id == $term_id){
                                $parent_term_url = $term_url;
                                $parent_term_name = $term_name;
                            }
                ?>

                    <li class="<?php echo ($parent_term_id == $term_id  ? 'active' : '') ?>">
                        <a href="<?php echo $term_url  ?>">
                            <?php echo $term_name; ?>   
                        </a>
                    </li>
                <?php
                        } 
                    }

                ?>
                </ul>
            </nav>
        </header>
        <!-- Child Program Child -->
            <?php if(true){ ?>
            <div class="container-wrapper padding-t-15 txt-color-white bg-blue">
                <div class="row row-10 align-items-center">
                    <div class="padding-lr-10 padding-b-15">
                        <h2>
                            Filter by
                        </h2>
                    </div>
                    <div class="col-md-4 magazine-dropdown padding-lr-10 padding-b-15">
                        <div class="dropdown">
                            <button class="btn btn-trans-wb dropdown-toggle full-width" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php
                                if($child_term_name){
                                    echo $child_term_name;
                                }else{
                                    echo 'All';
                                }
                            ?>
                            </button>
                            <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                                <a
                                    class="dropdown-item <?php echo ($child_term_id == 0  ? 'selected' : '') ?>"
                                    href="<?php echo $parent_term_url ?>">
                                    All
                                </a>
            <?php
                $program_children = get_term_children( $parent_term_id, 'program-type' );

                foreach ( $program_children as $child ) {
                    $term = get_term_by( 'id', $child, 'program-type' );
                    $term_url = get_term_link($term);
            ?>

                                <a class="dropdown-item <?php echo ($child_term_id == $term->term_id  ? 'selected' : '') ?>"
                                    href="<?php echo $term_url  ?>">
                                    <?php echo $term->name ?>
                                </a>

            <?php
                } 
            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <?php } ?>

		<!-- General Section -->
		<section>
			<div class="row">
            <?php wp_reset_postdata();
                wp_reset_query();
                $temp = $wp_query; $wp_query= null;
                
                $tax_array = array('relation' => 'AND');
                
                if($parent_term_id){
                    $category_array = array(
                        //Program Type
                        array (
                            'taxonomy' => 'program-type',
                            'terms' => $parent_term_id
                        )
                    );

                    $tax_array = array_merge($tax_array, $category_array); 
                }
                
                if($child_term_id){
                    $category_array = array(
                        //Program Type
                        array (
                            'taxonomy' => 'program-type',
                            'terms' => $child_term_id
                        )
                    );

                    $tax_array = array_merge($tax_array, $category_array); 
                }
                
                $args = array(
                    'post_type' => 'bottom-cb',
                    'showposts' => -1,
                    'tax_query' => $tax_array,
                );
                
                
                $wp_query = new WP_Query();
                $wp_query->query($args);
                while ($wp_query->have_posts()) : $wp_query->the_post();
                
                $images = rwmb_meta( 'post-feature-image', array( 'limit' => 1 ) );
                $image = reset( $images );
            ?>

                <!-- CTA 2 -->
                <?php
                    $images = rwmb_meta( 'event-image', array( 'limit' => 1 ) );
                    $image = reset( $images );
                ?>
                <div
                    class="col-md-4 feature-image-block"
                    style="background-image:url('<?php echo $image['full_url']; ?>"   
                >

                    <div class="content txt-color-white">
                        <div class="event-title margin-b-20">
                            <?php
                                //Get Event Date
                                $event_date = rwmb_meta( 'event-date' );
                            
                                //Event Expiry Date Calculation
                                $start = new DateTime();
                                $start->setTimestamp($event_date);
                                $end   = new DateTime(); // Current date time
                                $diff  = $start->diff($end);
                            
                               // echo  $diff->days . ' days, ';
                            
                            ?>
                            <?php if($event_date){ ?>
                                <div class="play-btn">
                                    <!--<i class="hidden">
                                        <div class="expired">
                                            Expired
                                        </div>
                                    </i>-->
                                    <i>
                                        <div class="month uppercase">
                                            <?php echo date( 'M', $event_date ); ?>
                                        </div>
                                        <div class="day">
                                           <?php echo date( 'j', $event_date ); ?> 
                                        </div>
                                    </i>
                                </div>
                            <?php } ?>
                            <h3 class="title txt-xlg txt-medium txt-height-1-2">
                                <?php echo rwmb_get_value( 'event-title' ); ?>
                            </h3>
                        </div>
						<p class="txt-height-1-5 margin-b-30">
							<?php echo rwmb_get_value( 'event-description' ); ?>
						</p>
                        <article class="btn-wrapper">
                            <?php $values = rwmb_meta('event-btns');
                                if($values){
                                    foreach ( $values as $value ) {
                                        if($value['footer-btn-type'] == 1){ ?>

                                            <a
                                                class="btn btn-trans-wb icon"
                                                href="<?php echo $value['footer-btn-url']; ?>"
                                                <?php
                                                    if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                ?>   
                                            >
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>

                                        <?php }elseif($value['footer-btn-type'] == 2){ ?>
                                            <a
                                                class="btn btn-trans-wb"
                                                href="<?php echo $value['footer-btn-url']; ?>"
                                                <?php
                                                    if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                ?>    
                                            >
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>
                                        <?php }else{ ?>
                                            <a
                                                class="btn btn-white"
                                                href="<?php echo $value['footer-btn-url']; ?>"
                                                <?php
                                                    if(rwmb_get_value( 'event-select-target' )=="new") echo "target='_blank'";
                                                ?>
                                            >
                                                <?php echo $value['footer-btn-text']; ?>
                                            </a>
                                        <?php }
                                    }
                                }
                            ?>
                        </article>
					</div>
                </div>
                
            <?php endwhile; ?>
			</div>
		</section>
	</main>
   
    <?php get_footer() ?>