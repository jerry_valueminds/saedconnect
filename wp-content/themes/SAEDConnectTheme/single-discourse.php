<?php get_header() ?>
    
    <?php while ( have_posts() ) : the_post(); ?>
   
    <main class="main-content">
        
    <?php
        /* 
        *=============================================
        * Get Content for this view
        *=============================================
        */
        /* Variable to Check if Post has Mini-site Grand Parent */
        $has_parent = true;
        
        /* Get Content for this view */
        /* Variables to store data */
        $sectionName;
        $section_id;
        $section_theme_color;
        $section_hover_color;

        $multiPageID;

        $topicName;
        $topicID;
        $sponsorName;
        $sponsorImage;

        $multiPageName;
        $articleTitle = get_the_title();
        $articleID = get_the_ID();
    
        /*
        *=============================================
        * First Query to get Multi-page Name 
        *=============================================
        */
        $multiPageQuery = new WP_Query( array(
            'relationship' => array(
                'id'   => 'two_c_m_page_to_m_page_article',
                'to' => get_the_ID(), // You can pass object ID or full object
            ),
            'nopaging' => true,
        ) );

        while ( $multiPageQuery->have_posts() ) : $multiPageQuery->the_post(); 
            $multiPageName = get_the_title();
            $multiPageID = get_the_ID(); 
            $sponsorName = rwmb_get_value( 'sponsor-name' );

            /*
            *=============================================
            * Second Query to get Topic Name 
            *=============================================
            */
            $topicNameQuery = new WP_Query( array(
                'relationship' => array(
                    'id'   => 'mini_site_to_two_c_m_page',
                    'to' => get_the_ID(), // You can pass object ID or full object
                ),
                'nopaging' => true,
            ) );
                /* Check If Multipage Parent has a Mini-site Parent. If yes, get Mini-site parent data, */
                if ( $topicNameQuery->have_posts() ){

                    while ( $topicNameQuery->have_posts() ) : $topicNameQuery->the_post();

                        $topicName = get_the_title();
                        $topicID = get_the_ID();
                        $images = rwmb_meta( 'sponsor-image', array( 'limit' => 1 ) );
                        $image = reset( $images );
                        $sponsorImage = $image['full_url'];

                        /*
                        *=============================================
                        * Third Query to get Section Name 
                        *=============================================
                        */
                        /*$sectionNameQuery = new WP_Query( array(
                            'relationship' => array(
                                'id'   => 'og_section_to_og_topic',
                                'to' => get_the_ID(), // You can pass object ID or full object
                            ),
                            'nopaging' => true,
                        ) );
                        while ( $sectionNameQuery->have_posts() ) : $sectionNameQuery->the_post();

                            $sectionName = get_the_title();
                            $section_id = get_the_ID();
                            $section_theme_color = rwmb_meta( 'section-theme-color' );
                            $section_hover_color = rwmb_meta( 'section-link-hover-color' );


                        endwhile;
                        wp_reset_postdata();*/
                        /*
                        *=============================================
                        * Third Query: END
                        *=============================================
                        */

                    endwhile;
                    wp_reset_postdata();

            }else{
                /* Set to false if there is no Mini-site grand parent to this article  */
                $has_parent = false;
            }
            /*
            *=============================================
            * Second Query: END
            *=============================================
            */

        endwhile;
        wp_reset_postdata();
        /*
        *=============================================
        * First Query: END
        *=============================================
        */
    ?>
       
        <?php if($has_parent) { ?>
            <!-- Section Header -->
            <header class="overview-header container-wrapper">
                <div class="info-box">
                    <div class="info">
                    <?php if($section_id){ ?>
                        <h2 class="subtitle">
                            <?php echo $sectionName ?>
                        </h2>
                    <?php } ?>
                        <h1 class="title slim txt-color-lighter">
                            <?php echo $topicName ?>
                        </h1>
                    </div>
                    <?php if($section_id){ ?>
                        <div class="sponsor">
                            <div class="intro-text">
                                Content Sponsored by:
                            </div>
                            <img class="logo" src="<?php echo $sponsorImage ?>" alt="">
                        </div>
                    <?php } ?>
                </div>
                <nav class="header-nav">
                    <div class="header-nav-title">
                        <span class="name">
                            Submenu
                        </span>
                        <span class="icon"></span>
                    </div>
                    <ul>
                        <!-- Get All Three column Posts related to the Parent Topic -->
                        <?php 
                            /* Third Query to get Section Name */
                            /*$subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'og_topic_to_three_c_m_page',
                                    'from' => $topicID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post();*/ ?>   

                            <!--<li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>-->

                        <?php
                            /*endwhile;
                            wp_reset_postdata();*/
                            /* THird Query: END */
                        ?>

                        <!-- Get All Three column Posts related to the Parent Topic -->
                        <?php 
                            /* Third Query to get Section Name */
                            $subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_m_item_page',
                                    'from' => $topicID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   

                            <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* THird Query: END */
                        ?>

                        <!-- Get All Two Column Multi-page Articles related to the Parent Topic -->
                        <?php 
                            /* Third Query to get Section Name */
                            $subNavQuery = new WP_Query( array(
                                'relationship' => array(
                                    'id'   => 'mini_site_to_two_c_m_page',
                                    'from' => $topicID, // You can pass object ID or full object
                                ),
                                'nopaging' => true,
                            ) );
                            while ( $subNavQuery->have_posts() ) : $subNavQuery->the_post(); ?>   

                            <li class="<?php echo ($multiPageName == get_the_title()  ? 'active' : '') ?>">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                                <ul class="multi-article-sub-list txt-normal-s">
                                    <?php
                                    /* 
                                    *
                                    *====================================================
                                    * Nav Loop: Get ALl Sibling Posts from Multi-Page Parent
                                    *====================================================
                                    *
                                    */
                                        $multi_page_query = new WP_Query( array(
                                            'relationship' => array(
                                                'id'   => 'two_c_m_page_to_m_page_article',
                                                'from' => $multiPageID, // You can pass object ID or full object
                                            ),
                                            'nopaging' => true,
                                        ) );
                                        while ( $multi_page_query->have_posts() ) : $multi_page_query->the_post();
                                    ?>

                                        <li class="<?php echo ($articleTitle == get_the_title()  ? 'active' : '') ?>">
                                            <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                        </li>
                                    <?php
                                        endwhile;
                                        wp_reset_postdata();
                                    /* 
                                    *
                                    *====================================================
                                    * Nav Loop: END
                                    *====================================================
                                    *
                                    */
                                    ?>
                                </ul>
                            </li>

                        <?php
                            endwhile;
                            wp_reset_postdata();
                            /* THird Query: END */
                        ?>
                    </ul>
                </nav>
            </header>
        <?php }else{ ?>
            <!-- Section Header -->
            <header class="overview-header container-wrapper">
                <h1 class="title">
                    <?php echo the_title(); ?>
                </h1>
            </header>
        <?php } ?>
        <style>
            a{
                color: <?php echo $section_theme_color; ?>;
            }

            a:active {
                color: <?php echo $section_theme_color; ?>;
            }

            a:hover {
                color: <?php echo $section_hover_color; ?>;
            }

            .overview-header .header-nav li.active a {
                color: <?php echo $section_theme_color; ?>;
            }

            .overview-header .header-nav li.active a:before {
                background-color: <?php echo $section_theme_color; ?>;
            }
        </style>
        <section class="container-content-inverse">
            <div class="row">
                <div class="col-md-4 side-nav white-text bg-blue side-nav-container" id="side-nav">
                    <?php //if($multi_page_type == 'multi-page'){ ?>
                        <article>
                            <h2 class="title">
                                Outline
                            </h2>
                            <ul class="list txt-normal-s">
                            <?php
                            /* 
                            *
                            *====================================================
                            * Nav Loop: Get ALl Sibling Posts from Multi-Page Parent
                            *====================================================
                            *
                            */
                                $multi_page_query = new WP_Query( array(
                                    'relationship' => array(
                                        'id'   => 'two_c_m_page_to_m_page_article',
                                        'from' => $multiPageID, // You can pass object ID or full object
                                    ),
                                    'nopaging' => true,
                                ) );
                                while ( $multi_page_query->have_posts() ) : $multi_page_query->the_post();
                            ?>

                                <li class="<?php echo ($articleTitle == get_the_title()  ? 'active' : '') ?>">
                                    <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                </li>
                            <?php
                                endwhile;
                                wp_reset_postdata();
                            /* 
                            *
                            *====================================================
                            * Nav Loop: END
                            *====================================================
                            *
                            */
                            ?>
                            </ul>
                            <div class="dropdown">
                                <button class="btn btn-trans-white dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    SECTIONS
                                </button>
                                <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuButton" x-placement="top-start">
                                <?php
                                /* 
                                *
                                *====================================================
                                * First Loop: Get ALl Sibling Posts from Multi-Page Parent
                                *====================================================
                                *
                                */
                                    $multi_page_query = new WP_Query( array(
                                        'relationship' => array(
                                            'id'   => 'two_c_m_page_to_m_page_article',
                                            'from' => $multiPageID, // You can pass object ID or full object
                                        ),
                                        'nopaging' => true,
                                    ) );
                                    while ( $multi_page_query->have_posts() ) : $multi_page_query->the_post();
                                ?>
                                    <a class="dropdown-item" href="<?php the_permalink() ?>">
                                        <?php the_title() ?>
                                    </a>
                                <?php
                                    endwhile;
                                    wp_reset_postdata();
                                /* 
                                *
                                *====================================================
                                * First Loop: END
                                *====================================================
                                *
                                */
                                ?>
                                </div>
                            </div>
                        </article>
                    <?php //}else{ ?>
                        <article>
                            <h2 class="title">
                                <?php echo rwmb_get_value( 'multi-page-article-title' ); ?>
                            </h2>
                            <article class="content txt-color-white">
                                <?php echo rwmb_get_value( 'multi-page-article-content' ); ?>
                            </article>
                        </article>
                    <?php //} ?>
                </div>
                <div class="col-md-8 left-content-inverse padding-tb-40">
                    <div class="row-40 margin-b-20">
                        <!-- New -->
                        <?php get_template_part( 'template-parts/content-block-template' ); ?>
                       
                        <!-- Old -->
                        <?php 
                            global $main_content_relashionship_id;
                            global $main_content_relationship_parent_id;

                            $main_content_relashionship_id = 'two_c_multi_to_cb';
                            $main_content_relationship_parent_id = $articleID;
                        ?>
                        <?php //get_template_part( 'template-parts/multi-item-main-content' ); ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- General Section -->
        <section>
            <?php 
                global $relashionship_id;
                global $relationship_parent_id;

                $relashionship_id = '2_c_multi_page_article_to_bcb';
                $relationship_parent_id = $articleID;
            ?>
            <?php get_template_part( 'template-parts/multi-item-general-content' ); ?>
        </section>
    </main>
    
    <?php endwhile; // end of the loop. ?>

    <?php get_footer() ?>