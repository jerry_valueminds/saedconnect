<?php get_header() ?>
   
<?php 
    /*Get Theme Color*/
    $theme_color = '#15153e';

    /*Get Theme Alt Color*/
    $theme_alt_color = 'white';

    /* Get Currently Signed In user */
    $current_user = wp_get_current_user(); 
?>
    
<?php while ( have_posts() ) : the_post(); ?>

<?php
    $post_id = get_the_ID();

    /* GF Search Criteria */
    $search_criteria = array(

    'field_filters' => array( //which fields to search

        array(

            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
            'key' => '31', 'value' => $post_id, //Current logged in user
        )
      )
    );

    /* Get GF Entry Count */
    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

    foreach( $entries as $entry ){

        $entry_id = $entry['id'];
    }
?>

<main class="main-content">
    <header class="padding-t-80 padding-b-40" style="background-color:<?php echo $theme_color; ?>">
        <div class="container-wrapper">
            <div class="row padding-b-40" style="color:<?php echo $theme_alt_color ?>">
                <div class="col-md-8">
                    <h1 class="txt-2em txt-medium margin-b-15">
                        <?php the_title(); ?>
                    </h1>
                    <h2 class="txt-normal-s txt-height-1-7 margin-b-20">
                        <?php echo rgar( $entry, 3 ) ?>
                    </h2>
                    <p>
                        Powered by
                        <span class="txt-medium txt-normal-s">
                        <?php echo rgar( $entry, 2 ) ?>
                        </span>
                    </p>
                </div>
            </div>
            <!-- Right Side Content -->
            <div class="col-md-3 program-sidebar">
                <!-- Quick links -->
                <figure class="padding-lr-5 padding-t-5">
                    <?php
                        $meta = rgar( $entry, '12' );

                        if($meta){
                            $meta = str_ireplace( 'http:', 'https:', $meta );
                        }
                    ?>

                    <img src="<?php echo $meta; ?>" alt="">
                </figure>

                <div class="padding-lr-20 padding-tb-20 border-b-1 border-color-darkgrey">
          
                    <div class="txt-medium txt-xxlg txt-color-blue margin-b-10">
                        <?php

                            $meta = rgar( $entry, '4' );

                            if($meta){
                                echo $meta;
                            }
                        ?>
                    </div>
                  
                    <?php
                        if( rwmb_get_value( 'opportunity-open-date' ) ){
                    ?>
                        <!--<div class="txt-light txt-normal-s">
                            Opens
                            <span class="txt-medium">
                                <?php rwmb_the_value( 'opportunity-open-date', array( 'format' => 'F  d\, Y' ) ); ?>
                            </span>
                        </div>-->
                    <?php } ?>

                    <div class="txt-light txt-normal-s margin-t-5">
                        Expires
                        <span class="txt-medium">
                            <?php
                                $date = strtotime( rgar( $entry, '7' ) );
                                echo date('j F Y', $date);
                            ?>
                        </span>
                    </div>
                </div>
                
                <div class="padding-o-20">  
                        
                    <?php
                        $term_list = wp_get_post_terms( $post_id, 'coverage-nigeria', array( 'fields' => 'names' ) );
                    
                        if( $term_list ){
                    ?>
                        <div class="row row-5 margin-b-20 txt-sm">
                            <div class="col-4 padding-lr-5 txt-height-1-5">
                                Locations
                            </div>
                            <div class="col-md-8 padding-lr-5">
                                <p class="txt-medium">
                                    <?php

                                        foreach( $term_list as $term_item ){
                                            echo $term_item.', ';
                                        }

                                    ?>
                                </p>
                            </div>
                        </div>
                    <?php } ?>
                    
                    <?php
                        if( rgar( $entry, '17' ) ){
                    ?>
                        <div class="row row-5 margin-b-20 txt-sm">
                            <div class="col-4 padding-lr-5 txt-height-1-5">
                                Venue Address
                            </div>
                            <div class="col-md-8 padding-lr-5 txt-height-1-5">
                                <div class="text-box sm txt-medium margin-b-10">
                                    <?php echo rgar( $entry, '17' ); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    
                    <?php
                        if( rgar( $entry, '18' ) ){
                    ?>
                        <div class="row row-5 margin-b-20 txt-sm">
                            <div class="col-4 padding-lr-5 txt-height-1-5">
                                TIme
                            </div>
                            <div class="col-md-8 padding-lr-5 txt-height-1-5">
                                <div class="text-box sm txt-medium margin-b-10">
                                    <?php echo rgar( $entry, '18' ); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    
                    <?php
                        if( rgar( $entry, '15' ) ){
                    ?>
                        <div class="">
                            <a
                                href="<?php echo rgar( $entry, '15' ); ?>"
                                class="btn btn-trans-blue txt-sm full-width"
                            >
                                Visit Website
                            </a>
                        </div>
                    <?php } ?>
                    
                    <?php
                        if( rwmb_get_value( 'opportunity-application-link' ) ){
                    ?>
                        <div class="">
                            <a
                                href="<?php echo rwmb_get_value( 'opportunity-application-link' ); ?>"
                                class="btn btn-blue txt-sm full-width no-m-b"
                                target="_blank"
                            >
                                Apply Now
                            </a>
                        </div>
                    <?php } ?>
                    
                    <?php
                    
                        $field = 'submit_opportunity_application_type';

                        $meta = get_post_meta($post->ID, $field, true);

                        if( $user_data && $meta == 'On SAEDConnect' ){

                    ?>
                    
                    <div class="">
                        <a
                            href="https://www.saedconnect.org/opportunity-center/application-form/?gf-id=2&form-title=Apply for <?php the_title() ?>&opportunity_id=<?php echo $post_id ?>"
                            class="btn btn-blue txt-sm full-width no-m-b"
                        >
                            Apply Here
                        </a>
                    </div>
                    
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>

    <?php
        if( $_REQUEST['view'] == 'apply' ){
            
            /* Check if User is signed */
            if ( is_user_logged_in() ) {

                /* Check if User has Signed Up for Opportunity*/
                $meta_key = 'opportunity_'.$post_id.'_applied';

                if( !metadata_exists( 'user', $current_user->ID, $meta_key ) ){

                    /* Save usermeta indicating application for opportunity */
                    update_user_meta( $current_user->ID, $meta_key, "1");

                    /* Update Application count on Opportunity */
                    $application_count = get_post_meta( $post_id, 'opportunity_application_count', true );

                    $application_count = $application_count + 1;

                    update_post_meta($post_id, 'opportunity_application_count', $application_count);
                    
                    /* Save User meta to opportunity */
                    add_post_meta($post_id, 'signed_up_user', $current_user->ID);
                }
            }
    ?>
    
    <?php } else { ?>
    
    <section>
        <div class="row">
            <!-- Main Content -->
            <div class="col-md-8 left-content padding-t-40">
                <div class="row row-40">
                    <!-- Text content -->
                    <div class="col-md-12 padding-lr-40">
                        <div class="margin-b-40">
                            <h2 class="txt-lg txt-medium margin-b-20">
                                Program Description
                            </h2>
                            <article class="text-box sm txt-height-1-7 txt-normal-s">
                                <?php echo rgar( $entry, 13 ) ?>
                            </article>
                        </div>
                    </div>
                    
                    <div class="col-md-12 padding-lr-40">
                        <div class="margin-b-40">
                            <h2 class="txt-lg txt-medium margin-b-20">
                                How to Apply
                            </h2>
                            <article class="text-box sm txt-height-1-7 txt-normal-s">
                                <?php echo rgar( $entry, 30 ) ?>
                            </article>
                        </div>
                    </div>
        
                    <?php
                        /* Get Accordion Content */
                        $values = rwmb_meta( 'opportunity-faq' );
                        $accordionCounter = 1;
                        
                        /* Check if Accordion is empty */
                        if($values[0]['accordion-title']){
                    ?>
                    <div class="col-md-12 padding-lr-40">
                        <div class="margin-b-40">
                            <h2 class="article-header padding-b-20 border-b-1 border-color-darkgrey">
                                Frequently Ask Question
                            </h2>
                            <div class="faq-accordion margin-b-20">
                                <div id="accordion">
                                <?php foreach ( $values as $value ) { ?>

                                    <div class="card">
                                        <div class="card-header" id="heading-<?php echo $accordionCounter; ?>">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $accordionCounter; ?>" aria-expanded="false" aria-controls="collapse<?php echo $accordionCounter; ?>">
                                                    <?php echo $value['accordion-title']; ?>
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapse-<?php echo $accordionCounter; ?>" class="collapse" aria-labelledby="heading-<?php echo $accordionCounter; ?>" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                <article class="text-box sm">
                                                    <div class="txt-height-1-5">
                                                        <?php echo $value['accordion-content']; ?>
                                                    </div>
                                                </article>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php $accordionCounter++ ?>
                                    
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
    
    <?php } ?>
</main>

<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>