<?php /*Template Name: Forum Form*/ ?>
   
<?php
                    
    if ( !is_user_logged_in() ) {
        // If User is Logged in, redirect to User Dashbord
        $dashboard_link = 'http://www.saedconnect.org/login/?action=signin'; //Get Daasboard Page Link by ID

        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }

?>

<?php get_header() ?>

<?php
    // TO SHOW THE PAGE CONTENTS
    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
?>
    
    <main class="main-content">
        
        <section class="container-wrapper padding-t-80 padding-b-40 text-center text-center txt-light">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-height-1-2 margin-b-20">
                        <?php the_title() ?>
                    </h1>
                    <h2 class="txt-lg txt-height-1-5">
                        <p>
                            
                        </p>
                    </h2>
                </div>
            </div>
        </section>
        
        <style>
            
            .gform_body{
                border: 0 !important;
            }
            
            .gform_wrapper .top_label .gfield_label {
                font-size: 0.8rem !important;
                margin-bottom: 0 !important;
            }
            
            .ginput_container input, .ginput_container select, .ginput_container textarea{
                width: 100% !important;
                font-size: 0.9rem !important;
                padding: 0.4rem 0.8rem !important;
            }
            
            .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
                margin-top: 0px !important;
            }
            
            .gform_button{
                text-align: center;
                margin: 0 !important;
                margin-bottom: 20px !important;
            }
            
            .gform_footer{
                display: flex !important;
                flex-direction: column;
                justify-content: center !important;
                align-items: center !important;
            }
            
    
        </style>
        
        <section class="container-wrapper padding-b-40" id="get-started">
            <div class="row">
                <div class="col-md-8 mx-auto esaed-form">
                    <?php echo the_content() ?>
                </div>
            </div>
        </section>

    </main>
    
<?php
    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
?>

<?php get_footer() ?>
