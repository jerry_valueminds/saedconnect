    <?php /*Template Name: Homepage-2*/ ?>
    
    <?php get_header() ?>
    
    <?php
        /* Publication key for getting Admin Approved content */
        $publication_key = 'publication_status';
    ?>
    
    <main class="main-content">
        <section class="container-wrapper bg-ash padding-tb-15">
            <div class="row">
                <div class="col-md-6">
                    <p class="txt-normal-s txt-color-white">
                        SAEDConnect is a <span class="txt-medium txt-color-yellow">Youth Empowerment Accelerator</span> dedicated to providing the solutions, resources, tools, opportunities and networks that young people need to make progress.
                    </p>
                </div>
            </div>
        </section>
        <section class="container-wrapper padding-t-10 padding-b-20">
            <div class="row">
                <section class="col-md-9 slippry-section">     
                    <ul id="thumbnails">
                    <?php
                        //WP Query
                        $args = array( 'post_type' => 'homepage-slide' );

                        $homeSlide_query = new WP_Query();
                        $homeSlide_query->query($args);

                        $counter = 1;

                        while ($homeSlide_query->have_posts()) : $homeSlide_query->the_post();

                            $images = rwmb_meta( 'slide-image', array( 'limit' => 1 ) );
                            $image = reset( $images ); 
                            $image_url = $image['full_url']; 
                    ?>
                        
                        <li>
                            <a
                                href="<?php echo rwmb_get_value( 'slide-cta-link' ); ?>"
                                style="background-image: url('<?php echo $image_url; ?>')"
                            >
                                
                            </a>
                        </li>
                             
                    <?php 
                        $counter++; 
                        endwhile; 
                    ?>
                    </ul>
                    <div class="thumb-box">
                        <ul class="thumbs">
                        <?php 
                            
                            $counter = 1; 
                            while ($homeSlide_query->have_posts()) : $homeSlide_query->the_post(); 
                        ?>
                            
                            <li style="background-color: ghostwhite;">
                                <a href="#<?php echo $counter ?>" data-slide="<?php echo $counter ?>">
                                    <div class="content">
                                        <?php if( rwmb_get_value( 'slide-title' ) ){ ?>
                                        <h2 class="txt-lg txt-medium margin-b-10">
                                            <?php echo rwmb_get_value( 'slide-title' ); ?>
                                        </h2>
                                        <?php } ?>
                                        
                                        <?php if( rwmb_get_value( 'slide-subtitle' ) ){ ?>
                                        <h3 class="txt-normal-s txt-height-1-4 txt-medium margin-b-10">
                                            <?php echo rwmb_get_value( 'slide-subtitle' ); ?>
                                        </h3>
                                        <?php } ?>
                                        
                                        <?php if( rwmb_get_value( 'slide-caption' ) ){ ?>
                                        <p class="txt-xs">
                                            <?php 
                                                echo truncate(rwmb_get_value( 'slide-caption' ), 70);
                                            ?>
                                        </p>
                                        <?php } ?>
                                    </div>
                                </a>
                            </li>

                        <?php 
                            $counter++;
                            endwhile; 
                        ?>
                        </ul>
                    </div>
                </section>
                <div class="col-md-3 slide-sid-bar d-flex">
                    <div class="flex_1 d-flex flex-column">
                        <div class="flex_1 d-flex padding-b-20">
                            <div class="flex_1 padding-o-20 bg-grey">
                                <p class="txt-sm txt-medium  margin-b-10">
                                    Corpers Wee!
                                </p>
                                <p class="txt-sm margin-b-10 ">
                                    Join the SAED program is designed to help you easily access all the support you need to become self-reliant by the end of your service year.
                                </p>
                                <a href="https://www.saedconnect.org/information/information-page/nysc-saed/" class="btn btn-ash txt-medium txt-xxs no-m-b">
                                    Learn More
                                </a>
                            </div>
                        </div>                        
                        <div class="flex_1 d-flex padding-b-20">
                            <div class="flex_1 padding-o-20 bg-grey">
                                <p class="txt-sm txt-medium margin-b-20">
                                    Find help to accelerate your business or personal project
                                </p>
                                <div class="dropdown show">
                                    <a class="btn-pop-yellow txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        What do you need?
                                    </a>
                                    <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="https://www.saedconnect.org/ventures-directory/volunteer-requests/">I need Volunteers</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/ventures-directory/funding-requests/">I need Funding</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/ventures-directory/business-partner-requests/">I need Partners</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/ventures-directory/tool-requests/">I need Tools</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/ventures-directory/workspace-requests/">I need a Workspace/office Space</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/ventures-directory/land-requests/">I need a Land</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/ventures-directory/mentor-requests/">I need a Mentor</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/ventures-directory/marketing-requests/">I need Marketing/Advertising</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-md-12 d-flex padding-lr-5 padding-b-20">
                            <div class="flex_1 padding-o-20 bg-grey">
                                <p class="txt-sm txt-medium margin-b-20">
                                    For Corp Members!
                                </p>
                                <p class="txt-sm margin-b-15">
                                    Help other young people start your kind of business.
                                </p>
                                <a href="" class="btn btn-ash txt-medium txt-xxs no-m-b">
                                    Learn more
                                </a>
                            </div>
                        </div>-->
                        <div class="flex_1 d-flex">
                            <div class="flex_1 padding-o-20 bg-grey">
                                <p class="txt-sm txt-medium  margin-b-10">
                                    Make some extra Income for yourself
                                </p>
                                <p class="txt-sm margin-b-10 ">
                                    Explore various opportunities to earn some extra cash
                                </p>
                                <div class="dropdown show">
                                    <a class="btn-pop-yellow txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        See How
                                    </a>
                                    <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="https://www.saedconnect.org/information/information-page/data-collection/">Become a Data Collection Agent</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/information/information-page/tutor-link/">Teach whatever you know & Earn</a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/information/information-page/Influencer-network/">Make money from your social media account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="container-wrapper">
            <!--<h1 class="txt-2em txt-bold txt-height-1-1 margin-b-40">
                Get a Job
            </h1>-->
            <div class="row row-5 text-center">
                <div class="col-md-3 d-flex padding-lr-5 padding-b-20">
                    <div class="flex_1 padding-lr-20 padding-tb-30 bg-ash txt-color-white" style="//background-color: #dc65a4">
                        <div class="txt-2em margin-b-20 txt-color-yellow">
                            <i class="fa fa-user"></i>
                        </div>
                        <p class="txt-medium margin-b-10">
                            Start here!
                            <br>
                            Complete your competency profile
                        </p>
                        <p class="txt-sm margin-b-20">
                            Complete your competency profile and let opportunities that matche your skills & capabilities come to you.
                        </p>
                        <a href="https://www.saedconnect.org/information/information-page/competency-profile/" class="btn btn-white txt-xs no-m-b">
                            Get started
                        </a>
                    </div>
                </div>
                
                <div class="col-md-3 d-flex padding-lr-5 padding-b-20">
                    <div class="flex_1 padding-lr-20 padding-tb-30 bg-ash txt-color-white" style="//background-color: #ec9bc6">
                        <div class="txt-2em margin-b-20 txt-color-yellow">
                            <i class="fa fa-home"></i>
                        </div>
                        <p class="txt-medium margin-b-10">
                            Get help to start over 50 different businesses
                        </p>
                        <p class="txt-sm margin-b-20">
                            No more trial & error. Access resources, guidance & support to start specific small businesses and get it right the first time. 
                        </p>
                        <a href="https://www.saedconnect.org/growth-programs/programs/side-hustle-skills/" class="btn btn-white txt-xs no-m-b">
                            Select a Business
                        </a>
                    </div>
                </div>
                <div class="col-md-3 d-flex padding-lr-5 padding-b-20">
                    <div class="flex_1 padding-lr-20 padding-tb-30 bg-ash txt-color-white">
                        <div class="txt-2em margin-b-20 txt-color-yellow">
                            <i class="fa fa-plane"></i>
                        </div>
                        <p class="txt-medium margin-b-10">
                            Make money <br> from your skill/hobby
                        </p>
                        <p class="txt-sm margin-b-20">
                            Somebody needs your skill, no matter how small. Set up your freelancer profile, create a work offer and start making money.
                        </p>
                        <a href="https://www.saedconnect.org/information/information-page/make-money/" class="btn btn-white txt-xs no-m-b">
                            Get started
                        </a>
                    </div>
                </div>
                <div class="col-md-3 d-flex padding-lr-5 padding-b-20">
                    <div class="flex_1 padding-lr-20 padding-tb-30 bg-ash txt-color-white" style="//background-color: #ffd9ed">
                        <div class="txt-2em margin-b-20 txt-color-yellow">
                            <i class="fa fa-gears"></i>
                        </div>
                        <p class="txt-medium margin-b-10">
                            Find Trainers
                        </p>
                        <p class="txt-sm margin-b-20">
                            What do you want to learn? Explore a nationwide pool of trainers accross different skills & subjects.
                        </p>
                        <a href="https://www.saedconnect.org/service-provider-directory/trainers/" class="btn btn-white txt-xs no-m-b">
                            Get started
                        </a>
                    </div>
                </div>
            </div>
            <div class="row row-10">
                <div class="col-md-8 d-flex padding-lr-10 margin-b-20 d-flex">
                    <div class="padding-o-20 border-o-1 border-color-darkgrey flex_1 bg-white">
                        <h2 class="txt-medium margin-b-30">
                            Featured Jobs & Freelancer Opportunities
                        </h2>
                        <div class="overflow-hidden">
                        <?php
                            //Switch to Learn a Skill Multisite (id = 3)
                            switch_to_blog(101);
                            
                            $blog_url = get_home_url();

                            //WP Query
                            $args = array(
                                'post_type' => array('task', 'job'),
                                'meta_query' => array(
                                    array(
                                        'key' => 'featured',
                                        'value' => '1422',
                                    ),
                                    array(
                                        'key' => $publication_key,
                                        'value' => 'admin_published'
                                    )
                                ),
                            );
                            $job_query = new WP_Query();
                            $job_query->query($args);
                            
                            
                            
                            while ($job_query->have_posts()) : $job_query->the_post();
                                //print_r($post);
                                //echo '<br>';
                            
                                if( get_post_type() == 'job' ){
                                    
                        ?>
                        
                            <div class="row row-10 border-b-1 border-color-darkgrey padding-t-15 padding-b-5">
                                <div class="col-auto col-md-1 padding-lr-10 padding-b-10 txt-lg">
                                    <i class="fa fa-suitcase fa-fw job-icon"></i>
                                </div>
                                <div class="col-10 col-md-5 padding-lr-10 padding-b-10 txt-sm txt-medium">
                                    <?php the_title() ?>
                                </div>
                                <div class="col-auto col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light" style="text-transform:capitalize">
                                    <?php
                                        $field = 'job_post_locations';

                                        $meta = get_post_meta($post->ID, $field, true);

                                        if($meta){
                                            echo $meta;
                                        }
                                    ?>
                                </div>
                                <div class="col-auto col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light">
                                    Fulltime
                                </div>
                                <div class="col-12 col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light text-right text-md-left">
                                    <?php
                                        $permalink =  $blog_url.'/'.$post->post_type.'/'.$post->post_name;
                                    ?>
                                    <a href="<?php echo $permalink; ?>" class="txt-bold txt-underline">
                                        Apply
                                    </a>
                                </div>
                            </div>
                            
                        <?php } elseif( get_post_type() == 'task' ) { ?>
                            
                            <div class="row row-10 border-b-1 border-color-darkgrey padding-t-15 padding-b-5">
                                <div class="col-auto col-md-1 padding-lr-10 padding-b-10 txt-lg">
                                    <i class="fa fa-user fa-fw job-icon"></i>
                                </div>
                                <div class="col-10 col-md-5 padding-lr-10 padding-b-10 txt-sm txt-medium txt-height-1-2">
                                    <?php the_title() ?>
                                </div>
                                <div class="col-auto col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light" style="text-transform:capitalize">
                                    Lagos State
                                </div>
                                <div class="col-auto col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light">
                                    Freelance
                                </div>
                                <div class="col-12 col-md-2 padding-lr-10 padding-b-10 txt-sm txt-color-light text-right text-md-left">
                                    <?php
                                        $permalink =  $blog_url.'/'.$post->post_type.'/'.$post->post_name;
                                    ?>
                                    <a href="<?php echo $permalink; ?>" class="txt-bold txt-underline">
                                        Bid
                                    </a>
                                </div>
                            </div>
                            
                        <?php
                                }
                            //Revert to Previous Multisite
                            
                            endwhile;
                            //wp_reset_postdata();
                            //wp_reset_query();
                            //$wp_query = $temp;
                            restore_current_blog();
                        ?>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="dropdown show margin-t-20">
                            <a class="btn-pop-ash txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Post a Job / Task
                            </a>
                            <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="https://www.saedconnect.org/information/information-page/post-a-job/">Post a Job</a>
                                <a class="dropdown-item" href="https://www.saedconnect.org/information/information-page/post-a-task/">Post a Task</a>
                                <a class="dropdown-item" href="https://www.saedconnect.org/information/information-page/post-a-training-request/">Post a Training Request</a>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex padding-lr-10 margin-b-20 d-flex">
                    <div 
                        class="flex_1 bg-grey"
                    >
                        <div class="content padding-o-30">
                            <div class="padding-b-20">
                                <p class="txt-normal-s txt-bold margin-b-10">
                                    <a class="txt-color-light txt-lg" href="https://www.saedconnect.org/service-marketplace/service-category/career-advice/?view=freelancers">
                                        Discuss, share ideas and make connections in special online communities
                                    </a>
                                </p>
                            </div>
                            
                            <div class="border-color-darkgrey padding-b-20">
                                <p class="txt-normal-s txt-bold margin-b-10">
                                    <a class="txt-color-yellow-dark" href="https://www.saedconnect.org/service-marketplace/service-category/career-advice/?view=jobs">
                                        Side Hustle Communities 
                                    </a>
                                </p>
                                <p class="txt-sm txt-height-1-1 padding-b-10">
                                    One community per business - for over 50 business types.
                                </p>
                                <div class="">
                                    <a
                                       class="btn-pop-ash txt-sm no-m-b"
                                       data-toggle="modal"
                                       href="#communityModal"
                                    >
                                        Visit a Community
                                    </a>
                                </div>
                            </div>
                            <div class="border-color-darkgrey padding-b-20">
                                <p class="txt-normal-s txt-bold margin-b-10">
                                    <a class="txt-color-yellow-dark" href="https://www.saedconnect.org/service-marketplace/service-category/career-advice/?view=freelancers">
                                        Career Communities
                                    </a>
                                </p>
                                <p class="txt-sm txt-height-1-1 padding-b-10">
                                    Discuss issues relating to CV writing, job search, interview prep, etc. 
                                </p>
                                <div class="dropdown show">
                                    <a class="btn-pop-ash txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Visit a Community
                                    </a>

                                    <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="https://www.saedconnect.org/job-advisor-forum/forums/forum/ace-the-interviews/">
                                            Preparing for, & Acing your interviews
                                        </a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/job-advisor-forum/forums/forum/job-search/">
                                            Searching & Applying for Jobs
                                        </a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/job-advisor-forum/forums/forum/cv-and-cover-letters/">
                                            Crafting your CV, Social Profiles & Cover Letters
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <p class="txt-normal-s txt-bold margin-b-10">
                                    <a class="txt-color-yellow-dark" href="https://www.saedconnect.org/service-marketplace/service-category/career-advice/?view=freelancers">
                                        Entrepreneurship Communities
                                    </a>
                                </p>
                                <p class="txt-sm txt-height-1-1 padding-b-10">
                                    Discuss about starting a business, building a business plan, raising finance, overcoming marketing challenges, etc. 
                                </p>
                                <div class="dropdown show">
                                    <a class="btn-pop-ash txt-sm no-m-b" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Visit a Community
                                    </a>

                                    <div class="dropdown-menu txt-normal-s" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="https://www.saedconnect.org/tei-forum/forums/forum/entrepreneurship-101/">
                                            Entrepreneurship 101
                                        </a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/tei-forum/forums/forum/finding-your-business-idea/">
                                            Finding a great business Idea
                                        </a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/tei-forum/forums/forum/validate-build/">
                                            Planning, Developing & Activating your Business Idea
                                        </a>
                                        <a class="dropdown-item" href="https://www.saedconnect.org/tei-forum/forums/forum/going-live/">
                                            Going Live
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Help Requests -->
        <section class="container-wrapper padding-b-20 overflow-hidden">
            <!--<h1 class="txt-2em txt-bold txt-height-1-1 margin-b-40">
                Start & Grow Your Business
            </h1>-->
            <div class="bg-yellow padding-lr-40 padding-t-40 margin-b-100">
                <div class="row row-20">
                    <div class="col-md-8 padding-lr-20">
                        <h3 class="txt-lg txt-height-1-4 txt-bold padding-b-20">
                            Support / Collaborate with a young person to make progress
                        </h3>
                        <p class="txt-normal-s padding-b-40">
                            Empower creators and entrepreneurs to give life to unique projects - by supporting or collaborating with them, in any little way you can.
                        </p>
                    </div>
                    <div class="col-md-4 padding-lr-20">
                        
                    </div>
                </div>
                <div class="border-b-1 border-color-white padding-b-40">
                    <h3 class="txt-height-1-4 txt-bold padding-b-20">
                        Explore Existing Requests
                    </h3>
                    <div class="">
                <?php
                    switch_to_blog(103);
                    
                    $request_type_array = array(
                        array(
                            'gf_id' => 20,
                            'title' => 'Tool Requests',
                            'url' => 'https://www.saedconnect.org/ventures-directory/tool-requests/'
                        ),
                        array(
                            'gf_id' => 21,
                            'title' => 'Land Requests',
                            'url' => 'https://www.saedconnect.org/ventures-directory/land-requests/'
                        ),
                        array(
                            'gf_id' => 22,
                            'title' => 'Workspace Requests',
                            'url' => 'https://www.saedconnect.org/ventures-directory/workspace-requests/'
                        ),
                        array(
                            'gf_id' => 23,
                            'title' => 'Volunteer Requests',
                            'url' => 'https://www.saedconnect.org/ventures-directory/volunteer-requests/'
                        ),
                        array(
                            'gf_id' => 24,
                            'title' => 'Funding Requests',
                            'url' => 'https://www.saedconnect.org/ventures-directory/funding-requests/'
                        ),
                        array(
                            'gf_id' => 25,
                            'title' => 'Marketing Requests',
                            'url' => 'https://www.saedconnect.org/ventures-directory/marketing-requests/'
                        ),
                        array(
                            'gf_id' => 27,
                            'title' => 'Mentorship Requests',
                            'url' => 'https://www.saedconnect.org/ventures-directory/mentor-requests/'
                        ),  
                    );
                    
                    
                    foreach( $request_type_array as $request_item ){ 
                        /* GF Search Criteria */
                        $search_criteria = array();

                        $order_count = GFAPI::count_entries( $request_item['gf_id'], $search_criteria );
                ?>
                   
                    <a href="<?php echo $request_item['url'] ?>" class="btn btn-white txt-xs featured-request">
                        <?php echo $request_item['title'] ?> 
                        <span class="value" style="min-width: 2.25em;display: inline-block;"><?php echo $order_count ?></span>
                    </a>
                    
                <?php } ?>
                <?php
                    $search_criteria = array();

                    $order_count = GFAPI::count_entries( 19, $search_criteria );
                ?>
                        <a href="https://www.saedconnect.org/ventures-directory/business-partner-requests/" class="btn btn-blue txt-xs featured-request">Business Partner Requests<span class="value" style="min-width: 2.25em;display: inline-block;"><?php echo $order_count ?></span></a>
                    </div>
                    <p class="txt-sm txt-color-dark">
                        <i class="fa fa-question-circle padding-r-5"></i>
                        Need help to advance your business or project? <a href="" class="txt-color-dark txt-underline">Request here</a>
                    </p>
                </div>
                <div class="" style="position:relative;transform:translateY(80px)">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="txt-lg txt-height-1-4 txt-bold padding-b-20">
                                Featured Ventures & Projects 
                            </h2>
                        </div>
                        <div class="col-md-4 text-md-right">
                            <a href="https://www.saedconnect.org/ventures-directory/business-ventures/" class="txt-color-dark txt-sm txt-medium txt-underline">
                                Explore the Ventures Directory
                            </a>
                        </div>
                    </div>
                    <div class="row row-5">
                    <?php 
                        switch_to_blog(103);

                        wp_reset_postdata();
                        wp_reset_query();
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query();
                        $wp_query->query( 
                            array(
                                'post_type' => 'business',
                                'post_status' => 'publish',
                                'posts_per_page' => 4,
                                'meta_query' => array(
                                    array(
                                        'key' => 'featured',
                                        'value' => '1422',
                                    ),
                                    array(
                                        'key' => $publication_key,
                                        'value' => 'admin_published'
                                    )
                                ),
                            ) 
                        );

                        if ( $wp_query->have_posts() ) {

                            while ($wp_query->have_posts()) : $wp_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;    //Get Program ID

                            /* Get Entry */
                            /* GF Search Criteria */
                            $gf_id = 12;
                            $search_criteria = array(

                                'field_filters' => array( //which fields to search

                                    array(

                                        'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                                        'key' => '24', 'value' => $post_id, //Current logged in user
                                    )
                                )
                            );

                            /* Get GF Entry Count */
                            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                            $entry = $entries[0];
  
                    ?>
                       
                        <div class="col-md-3 padding-lr-5 padding-b-20 d-flex">
                            <a href="<?php the_permalink() ?>" class="home-community-card">
                                <article class="content">
                                    <?php
                                        $meta = rgar( $entry, '3' );
                                        $meta = str_ireplace( 'http:', 'https:', $meta );
                            
                                        if($meta){
                                            $image = $meta;
                                        } else {
                                            $image = 'https://www.saedconnect.org/wp-content/themes/SAEDConnectTheme/images/heroes/table.jpg';
                                        }
                                    ?>
                                    <figure class="image-box" style="background-image: url('<?php echo $image; ?>')">

                                    </figure>
                                    <div class="info bg-white">
                                        <h3 class=" txt-medium margin-b-10">
                                            <?php the_title() ?>
                                        </h3>
                                        <p class="txt-sm">
                                            <?php
                                                $meta = wp_strip_all_tags( rgar( $entry, '4' ) );
                                                if($meta){
                                                    echo truncate($meta, 70);
                                                }
                                            ?>
                                        </p>
                                    </div>
                                </article>  
                            </a>   
                        </div>
                        
                    <?php
                            endwhile;

                        }else{
                    ?>
                        <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                            <h2 class="txt-lg txt-medium">
                                No Businesses found.
                            </h2>
                        </div>   

                    <?php

                        }

                        restore_current_blog();
                    ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-r-md-30">
                    <!--<div class="row">
                        <div class="col-md-8">
                            <h2 class="txt-medium margin-b-20">
                                Featured Ventures
                            </h2>
                        </div>
                        <div class="col-md-4 text-md-right">
                            <a href="" class="txt-color-dark txt-sm txt-medium txt-underline">
                                Explore the Ventures Directory
                            </a>
                        </div>
                    </div>-->
                    <!--<ul class="row row-5 marketplace-freelancer-cards margin-b-20">
                        <li class="col-sm-6 col-lg-4 padding-lr-5">
                            <a href="marketplace_service_entry.html">
                                <span class="wrapper row">
                                    <span class="col-4 padding-r-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/Database of the Stars.jpg" alt="">
                                    </span>
                                    <div class="col-8 content">
                                        <div class="name">
                                            Jimlas Agric Pen
                                        </div>
                                        <p class="txt-xs">
                                            Feeding the Nation, one bag at a time.
                                        </p>
                                    </div>
                                </span>
                                <div class="services">
                                    Abakaliki, Agriculture
                                </div>
                            </a> 
                        </li>
                        <li class="col-sm-6 col-lg-4 padding-lr-5">
                            <a href="marketplace_service_entry.html">
                                <span class="wrapper row">
                                    <span class="col-4 padding-r-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/distribution_network.jpg" alt="">
                                    </span>
                                    <div class="col-8 content">
                                        <div class="name">
                                            Jimlas Agric Pen
                                        </div>
                                        <p class="txt-xs">
                                            Feeding the Nation, one bag at a time.
                                        </p>
                                    </div>
                                </span>
                                <div class="services">
                                    Abakaliki, Agriculture
                                </div>
                            </a> 
                        </li>
                        <li class="col-sm-6 col-lg-4 padding-lr-5">
                            <a href="marketplace_service_entry.html">
                                <span class="wrapper row">
                                    <span class="col-4 padding-r-10">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nav/Database of the Stars.jpg" alt="">
                                    </span>
                                    <div class="col-8 content">
                                        <div class="name">
                                            Jimlas Agric Pen
                                        </div>
                                        <p class="txt-xs">
                                            Feeding the Nation, one bag at a time.
                                        </p>
                                    </div>
                                </span>
                                <div class="services">
                                    Abakaliki, Agriculture
                                </div>
                            </a> 
                        </li>
                    </ul>-->
                </div>
                <!--<div class="col-md-4 d-flex border-o-1 border-color-darkgrey">
                    <div class="flex_1">
                        <h3 class="txt-medium txt-xlg padding-lr-30 border-b-1 border-color-darkgrey padding-tb-20 margin-b-20">
                            Business Partner Connect
                        </h3>
                        <div class="padding-lr-30 border-b-1 border-color-darkgrey padding-b-20 margin-b-20">
                            <p class="txt-normal-s txt-bold margin-b-10">
                                Looking for partner to support with marketing
                            </p>
                            <p class="txt-sm margin-b-10">
                                Showcase your business venture & let investors, customers & collaborators find you.
                            </p>
                            <p class="txt-sm txt-medium">
                                Abakaliki, Agriculture
                            </p>
                        </div>
                        <div class="padding-lr-30 border-b-1 border-color-darkgrey padding-b-20 margin-b-20">
                            <p class="txt-normal-s txt-bold margin-b-10">
                                Looking for partner to support with marketing
                            </p>
                            <p class="txt-sm margin-b-10">
                                Showcase your business venture & let investors, customers & collaborators find you.
                            </p>
                            <p class="txt-sm txt-medium">
                                Abakaliki, Agriculture
                            </p>
                        </div>
                        <div class="padding-lr-30 border-b-1 border-color-darkgrey padding-b-20 margin-b-20">
                            <p class="txt-normal-s txt-bold margin-b-10">
                                Looking for partner to support with marketing
                            </p>
                            <p class="txt-sm margin-b-10">
                                Showcase your business venture & let investors, customers & collaborators find you.
                            </p>
                            <p class="txt-sm txt-medium">
                                Abakaliki, Agriculture
                            </p>
                        </div>
                        <div class="padding-lr-30 padding-b-20">
                            <p class="txt-normal-s txt-bold margin-b-10">
                                <a href="" class="txt-lg txt-bold txt-color-dark txt-underline">
                                    Find a Business Partner
                                </a>
                            </p>
                        </div>
                    </div>
                </div>-->
            </div>
        </section>
        
        <!-- Opportunities -->
        <section class="container-wrapper padding-b-80">
            <!--<div class="row">
                <h1 class="col-md-8 txt-2em txt-bold txt-height-1-1 margin-b-40">
                    Open Growth Opportunities
                </h1>
                <p class="col-md-4 text-md-right txt-normal-s txt-bold margin-b-10">
                    <a href="https://www.saedconnect.org/opportunity-center/" class="txt-medium txt-color-dark txt-underline">
                        See all Open Opportunities
                        <i class="fa fa-arrow-right"></i>
                    </a>
                </p>
            </div>-->
            <div class="padding-t-20 padding-b-10 border-tb-1">
                <div class="row">
                    <div class="col-md-8 padding-b-10">
                        <h4 class="txt-lg txt-bold txt-height-1-4">
                            Open Growth Opportunities
                        </h4>
                    </div>
                    <div class="col-md-4 text-md-right padding-b-10">
                        <a href="https://www.saedconnect.org/opportunity-center/" class="btn btn-trans-bw txt-xs no-m-b">
                            See All
                        </a>
                    </div>
                </div>
            </div>
            <div class="program-card-wrapper">
                <div class="row row-20">
                <?php wp_reset_postdata();
                    switch_to_blog(12);
                    
                    wp_reset_query();
                    $temp = $wp_query; $wp_query= null;
                    $wp_query = new WP_Query();
                    $wp_query->query( 
                        array(
                            'post_type' => 'opportunity',
                            'post_status' => 'publish',
                            'posts_per_page' => 4,
                            'meta_query' => array(
                                array(
                                    'key' => 'featured',
                                    'value' => '1422',
                                ),
                                array(
                                    'key' => $publication_key,
                                    'value' => 'admin_published'
                                )
                            ),
                        ) 
                    );

                    if ( $wp_query->have_posts() ) {

                        while ($wp_query->have_posts()) : $wp_query->the_post();

                        /* Variables */
                        $program_id = $post->ID;    //Get Program ID
                        
                        /* Get Entry */
                        /* GF Search Criteria */


                        /* Get GF Entry Count */
                                    
                        /* Get expiry date */
                        $expiry_date = rwmb_meta( 'opportunity-close-date' );
                        $field = 'submit_opportunity_expires';
                        $exp = 2;
                        $td = 1;


                        if( rwmb_meta( 'opportunity-close-date' ) ){

                            $expiry_date = rwmb_meta( 'opportunity-close-date' );
                            $today_date = date('y-m-d');

                            $exp = strtotime( $expiry_date );
                            $td = strtotime( $today_date );

                        } else if( get_post_meta($post->ID, $field, true) ) {


                            $meta = get_post_meta($post->ID, $field, true);

                            if($meta){
                                $expiry_date = $meta;
                            }

                            $today_date = date('m/d/y');

                            $expiry_date = strtotime( $expiry_date );
                            $td = strtotime( $today_date );

                            /*echo "<br><br><br>back-today: ".$exp;
                            echo "<br>Expire: ".$td;*/

                        }
                        
                        /* Check if expired */
                        if( true ){
                        /*if( $td < $expiry_date ){*/
                        
                ?>
                   
                    <div class="col-md-3 program-card">
                        <div class="content">
                            <figcaption>
                                <?php 
                                    $deadline = get_post_meta( $program_id, 'deadline', true );
                            
                                    if( $deadline ){
                                ?>
                                <h4 class="date">
                                    <span class="expires">
                                        Expires
                                    </span>
                                    <?php
                                        $field = 'submit_opportunity_expires';
                                        $date = strtotime( $deadline );

                                        echo date('j F Y', $date);
                                    ?>
                                </h4>
                                <?php } ?>
                                <div class="header">
                                    <h3 class="title">
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                                <p class="description">
                                    <?php echo get_post_meta( $program_id, 'summary', true ) ?>
                                </p>
                                <?php $meta = get_post_meta( 'author', $program_id ); ?>
                                <?php if($meta){ ?>
                                <h4 class="author">Powered by
                                    <?php echo $meta ?>
                                </h4>
                                <?php } ?>
                            </figcaption>
                            <div class="image-box">
                                <?php 
                                    $images = "";
                                    $images = get_attached_media( 'image', $program_id ); 
                                ?>
            
                                <?php if($images){ ?>

                                    <?php  
                                        foreach($images as $image) { //print_r( $image ); 
                                            $previousImg_id = $image->ID;
                                    ?>
                                        <img src="<?php echo wp_get_attachment_url($image->ID,'full'); ?>">

                                    <?php } ?>

                                <?php } else { ?>
                                   
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heroes/noimage.png">

                                <?php } ?>
                            </div>
                        </div>
                    </div>

                <?php
                        }
                        endwhile;

                    } else {
                ?>
                    <div class="col-12 padding-lr-20 padding-t-20 padding-b-40">
                        <h2 class="txt-lg txt-medium">
                            No Opportunities found.
                        </h2>
                    </div>   

                <?php

                    }
                    
                    restore_current_blog();
                ?>
			    </div>
            </div>
            <div class="bg-grey padding-lr-20 padding-t-20 padding-b-10 margin-b-20">
                <div class="row">
                    <div class="col-md-8 padding-b-10">
                        <h4 class="txt-medium txt-height-1-4">
                            Have a program or initiative that can be beneficial to youths in any way? Share it here and get a lot of responses from interested young people.
                        </h4>
                    </div>
                    <div class="col-md-4 text-md-right padding-b-10">
                        <a href="" class="btn btn-trans-bw txt-sm no-m-b">
                            Get started
                        </a>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="container-wrapper bg-ash txt-color-white padding-t-80 text-center">
            <h3 class="txt-xxlg txt-height-1-4 txt-medium padding-b-10">
                Hire Freelancers to Help you get Stuff done
            </h3>
            <h4 class="padding-b-80">
                Quicker. Cheaper. Better
            </h4>
            <div class="row row-20 justify-content-md-center">
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/cv-resumes-cover-letter-writing/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-user-o fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2">
                        Create your CV/Cover Letter
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/business-plan-development/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-send-o fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2">
                        Create a Business Plan
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/logo-business-cards-stationery-design/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-connectdevelop fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2 ">
                        Build your brand (Logo, complimentary card, etc)
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/social-media-management/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-object-ungroup fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2 ">
                        Reach more people on social media
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/website-builders-cms/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-clone fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2 ">
                        Build a website
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/android-app-development/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-mobile fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2 ">
                        Create a Mobile App
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/photography/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-photo fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2 ">
                        Hire a Photographer
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/video-editing/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-film fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2 ">
                        Make a video
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/capability/writing-translation/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-pencil-square-o fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2 ">
                        Hire a Content Writer
                    </h4>
                </a>
                <a class="col-6 col-md-2 txt-color-white padding-lr-20 padding-b-80" href="https://www.saedconnect.org/service-marketplace/">
                    <figure class="txt-2-2em margin-b-20">
                        <i class="fa fa-ellipsis-h fa-fw"></i>
                    </figure>
                    <h4 class="txt-normal-s txt-medium txt-height-1-2 ">
                        Find More Freelancers
                    </h4>
                </a>
            </div>
        </section>
        
        <!--<div class="pre-footer lg" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (22).jpg')">
            <div class="content">
                <h4 class="title">
                    Empower People, Transform Lives
                </h4>
                <p class="txt-height-1-4 txt-color-white margin-b-40">
                    The world needs more problem solvers. Your support will create possibilities and amplify impact.
                </p>
                <article class="btn-wrapper">
                    <a class="btn btn-trans-wb icon" href="">
                        Contribute
                    </a>
                    <a class="btn btn-trans-wb" href="">
                        Support Youth Development
                    </a>
                    <a class="btn btn-white" href="">
                        Donate Online
                    </a>
                </article>
            </div>
        </div>-->
    </main>
    
    <?php get_footer() ?>
    
    <script>
        var thumbs = jQuery('#thumbnails').slippry({
            // general elements & wrapper
            slippryWrapper: '<div class="slippry_box thumbnails" />',
            // options
            transition: 'fade',
            pager: false,
            auto: true,
            onSlideBefore: function (el, index_old, index_new) {
                jQuery('.thumbs a').removeClass('active');
                jQuery(jQuery('.thumbs a')[index_new]).addClass('active');
            }
        });

        jQuery('.thumbs a').click(function () {
            thumbs.goToSlide($(this).data('slide'));
            return false;
        });
        
        $('.thumbs a').hover(function() {
            //$( this ).trigger( "click" );
       });
    </script>