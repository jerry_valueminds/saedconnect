    <?php /*Template Name: Login Page*/ ?>
    
    <?php
                    
        if ( is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            //$dashboard_link = get_site_url().'/my-dashboard'; //Get Daasboard Page Link by ID
            $dashboard_link = network_home_url().'/growth-programs/my-dashboard'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header() ?>
    
    <style>
        .gfield_checkbox label, .login-form .gfield_label{
            display: block !important;
        }
        
        .gfield_checkbox li{
            display: flex;
            align-items: center;
        }
        
        .gfield_checkbox input{
            margin: 0 !important;
            margin-right: 10px !important;
        }
    </style>
    
    <!-- // If User is not Logged in, Load Login Page -->
    <main class="main-content">
        <section class="container-wrapper padding-b-80">
            <div class="row row-40">
                <div class="col-md-5 padding-lr-40">
                    <header class="padding-t-80 margin-b-40">
                        <h1 class="txt-2em txt-bold margin-b-20">
                            Sign In
                        </h1>
                        <h2>
                            Sign into your account on SAEDConnect.
                            <span class="txt-sm">
                                Don't have an account?
                            </span>
                            <a class="txt-sm txt-medium txt-color-green" href="<?php echo network_home_url().'/register'; ?>">
                                Click here to Register
                            </a>
                        </h2>
                    </header>
                    <div class="login-form">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Name of Widgetized Area") ) : ?>
                        <?php endif;?>
                    </div>
                    <div class="txt-normal-s padding-t-20">
                       Forgot Password?
                        <a class="txt-medium txt-color-green" href="https://www.saedconnect.org/password-reset/">
                            Click here to Reset your Password
                        </a>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 padding-lr-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/welcome_doodle.png" alt="">
                </div>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>