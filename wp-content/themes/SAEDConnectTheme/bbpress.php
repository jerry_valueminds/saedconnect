    
    
    <?php
        /* Check if PMPro no access */
        $level_id = $_REQUEST['noaccess'];

        if ( $level_id ) {
            
            /* Generate Redirect URL */
            $redirect_url = get_site_url().'/membership-account/membership-checkout/?level='.$level_id;
            echo 'Redirect';
            if ( wp_redirect( $redirect_url ) ) {
                exit;
            }
        
        }
    ?>
    
    <?php get_header() ?>
    
    <main class="main-content">
        <h1 class="txt-xxlg txt-medium padding-o-80">
            Hello, This is the forum Homepage
        </h1>
        <?php 
            /* The Loop */
            while ( have_posts() ) : the_post(); 
        ?>
        
        <div class="container-wrapper">
            <?php the_content() ?>
        </div>
        
        <?php endwhile; // end of the loop. ?>
    </main>

    <?php get_footer() ?>