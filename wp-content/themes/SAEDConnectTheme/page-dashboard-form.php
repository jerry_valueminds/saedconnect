<?php /*Template Name: Dashboard-Form*/ ?>

<?php get_header('user-dashboard') ?>

<style>
    .gform_wrapper select, .gform_wrapper input {
        line-height: 1.5;
        width: 100% !important;
    }
    
    .gform_footer {
          display: inline-block !important;
          justify-content: unset;
    }

    .gform_footer input {
        display: inline-block !important;
        width: auto;
    }
    
    .gform_wrapper .gform_button {
        width: auto !important;
    }
</style>

<?php while ( have_posts() ) : the_post(); ?>
   
    
    <main class="main-content txt-color-light" style="margin-top: 100px">
    <section class="container-wrapper">
        <div class="container-wrapper bg-white padding-t-80 padding-b-40 margin-b-80">
            <div class="row padding-b-40">
                <div class="col-md-6">
                    <h1 class="txt-2-4em txt-bold margin-b-30">
                        <?php the_title() ?>
                    </h1>
                    <p class="txt-height-1-7">
                        Welcome to SAEDConect. Contribute to SAEDConnect as a Mentor or by moderating a community.
                    </p>
                </div>
            </div>
            <div class="">
                <?php echo the_content(); ?>
            </div>
        </div>
    </section>
</main>
    
<?php endwhile; // end of the loop. ?>