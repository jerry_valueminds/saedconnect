<?php /*Template Name: Hompage - Job Track*/ ?>    
    
<?php get_header() ?>
  
<?php
    /* Select Search form */
    if(isset($_GET['action'])){

        $type = $_GET['action'];
?>
    <main class="main-content">
        <header class="overview-header container-wrapper">
            <h1 class="title">JobTrac Employability Program</h1>
            <nav class="header-nav">
                <div class="header-nav-title">
                    <span class="name">
                        Submenu
                    </span>
                    <span class="icon"></span>
                </div>
                <ul>
                    <li class="
                           <?php
                                if($type == 'overview'){

                                    echo 'active';
                                }   
                            ?>
                    ">
                        <a href="<?php echo the_permalink() ?>/?action=overview">
                            Overview
                        </a>
                    </li>
                    <li class="
                           <?php
                                if($type == 'concept'){

                                    echo 'active';
                                }   
                            ?>
                    ">
                        <a href="<?php echo the_permalink() ?>/?action=concept">
                            Concept
                        </a>
                    </li>
                    <li class="
                           <?php
                                if($type == 'how-it-works'){

                                    echo 'active';
                                }   
                            ?>
                    ">
                        <a href="<?php echo the_permalink() ?>/?action=how-it-works">
                            How it Works
                        </a>
                    </li>
                    <li class="
                           <?php
                                if($type == 'roles'){

                                    echo 'active';
                                }   
                            ?>
                    ">
                        <a href="<?php echo the_permalink() ?>/?action=roles">
                            Roles
                        </a>
                    </li>
                    <li class="
                           <?php
                                if($type == 'faq'){

                                    echo 'active';
                                }   
                            ?>
                    ">
                        <a href="<?php echo the_permalink() ?>/?action=faq">
                            FAQ
                        </a>
                    </li>
                </ul>
            </nav>
        </header>
       
        <?php get_template_part( 'template-parts/job-trac/'.$type ); ?>
        
    </main>
        
<?php
        
    }
?>
   
<?php get_footer() ?>