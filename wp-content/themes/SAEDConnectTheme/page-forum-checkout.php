<?php /*Template Name: Forum Checkout*/ ?>
    
<?php get_header() ?>
   
<main class="main-content">
    <header class="container-wrapper text-center padding-t-60 padding-b-40">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <h1 class="txt-medium txt-2em margin-b-20">
                    Sign up for the SAEDConnect Package
                </h1>
                <article class="margin-b-40">
                    <p class="txt-normal-s">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </p>
                </article>
                <article class="d-inline-block bg-yellow padding-lr-30 padding-tb-10 margin-b-20">
                    <span class="txt-2em txt-medium">
                        N3000
                    </span>
                    <span class="">
                        / yr
                    </span>               
                </article>
                <div class="margin-t-20">
                    <a href="" class="btn btn-blue txt-normal-s">
                        Get Started
                    </a>
                </div>
            </div>
        </div>
        <!--<figure class="task-image" style="background-image: url(images/heroes/honda_1.jpg)">

        </figure>-->
        <!--<div class="margin-t-40">
            <a class="btn btn-trans-blue no-m-b" href="">
                Sign me up
            </a>
        </div>-->
    </header>

    <section class="padding-tb-80 bg-grey">
        <div class="container-wrapper">
            <div class="row">
                <div class="col-md-10 mx-auto">
                    <header class="padding-b-80 text-center">
                        <h1 class="txt-2-2em txt-light txt-height-1-1 margin-b-20">
                            What you get
                        </h1>
                        <h2 class="txt-normal-s txt-height-1-7">
                            We are going all out to ensure you get all the support &amp; resources you require to start and excel in the Import/Export Business. 
                        </h2>
                    </header>
                    <div class="row row-20">
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <div class="row row-10">
                                <div class="col-3 padding-lr-10">
                                    <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/eSAED/eS_expert_instructions.png" alt="">
                                </div>
                                <div class="col-9 padding-lr-10">
                                    <p class="txt-bold txt-color-light txt-height-1-1">
                                        Expert Resources, Instruction &amp; Guidance
                                    </p>
                                    <div class="collapse" id="collapse-1" style="">
                                        <div class="padding-tb-20">
                                            <ul class="icon-list txt-sm">
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Learn the skills &amp; business acumen required to start &amp; thrive in the Import/Export Business
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Access to experienced Mentors available to help you in real time
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Access useful Templates, Reference Guides &amp; tailored advice from experts
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Access Tons of real-life questions &amp; answers
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Connect directly with other practitioners who can handhold you to start and grow your own business
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>
                                        <a class="txt-sm txt-color-green collapsed" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">
                                            Learn more
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <div class="row row-10">
                                <div class="col-3 padding-lr-10">
                                    <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/eSAED/eS_access_to_grow.png" alt="">
                                </div>
                                <div class="col-9 padding-lr-10">
                                    <p class="txt-bold txt-color-light txt-height-1-1">
                                        Real-Life Learning from wherever you are
                                    </p>
                                    <div class="collapse" id="collapse-2">
                                        <div class="padding-tb-20">
                                            <ul class="icon-list txt-sm">
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Real-Life Learning from wherever you are
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Discover what works for other businesses. Success stories and pitfalls to avoid
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Get critical, tailored information relevant to your situation, not just Generic, one-size-fit all content
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Meet practitioners near you who can give you the opportunity to get practical experience before you start
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>
                                        <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-1">
                                            Learn more
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <div class="row row-10">
                                <div class="col-3 padding-lr-10">
                                    <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/eSAED/eS_real_world_experience.png" alt="">
                                </div>
                                <div class="col-9 padding-lr-10">
                                    <p class="txt-bold txt-color-light txt-height-1-1">
                                        Start your own business 
                                    </p>
                                    <div class="collapse" id="collapse-3">
                                        <div class="padding-tb-20">
                                            <ul class="icon-list txt-sm">
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Learn new skills and develop fresh new ideas to use in your business  
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        You are challenged to apply the skills and support you get to start up your own business
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Hit the road running: You will learn things that you can use immediately after a session
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p class="">
                                        <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-1">
                                            Learn more
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <div class="row row-10">
                                <div class="col-3 padding-lr-10">
                                    <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/eSAED/eS_amazing_support_network.png" alt="">
                                </div>
                                <div class="col-9 padding-lr-10">
                                    <p class="txt-bold txt-color-light txt-height-1-1">
                                        Thrive within a Powerful Support Network
                                    </p>
                                    <div class="collapse" id="collapse-4">
                                        <div class="padding-tb-20">
                                            <ul class="icon-list txt-sm">
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Benefit from Peer-to-Peer Motivation 
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Test new ideas and strategies an get a wide range of opinions
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Connect directly with other practitioners who can handhold you to start and grow your own business
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        We occasionally run programs that would keep you accountable to start your business within a specified time frame
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p class="">
                                        <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">
                                            Learn more
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <div class="row row-10">
                                <div class="col-3 padding-lr-10">
                                    <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/eSAED/eS_challenge_the_status_quo.png" alt="">
                                </div>
                                <div class="col-9 padding-lr-10">
                                    <p class="txt-bold txt-color-light txt-height-1-1">
                                        Explore every aspect of the business
                                    </p>
                                    <div class="collapse" id="collapse-5">
                                        <div class="padding-tb-20">
                                            <p class="txt-sm">
                                                You will find someone with experience in virtually every aspect of the business to help you through your challenges.
                                            </p>
                                        </div>
                                    </div>
                                    <p class="">
                                        <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">
                                            Learn more
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <div class="row row-10">
                                <div class="col-3 padding-lr-10">
                                    <img src="https://www.saedconnect.org/growth-programs/wp-content/themes/SAEDConnectTheme/images/icons/eSAED/eS_test_your_ideas.png" alt="">
                                </div>
                                <div class="col-9 padding-lr-10">
                                    <p class="txt-bold txt-color-light txt-height-1-1">
                                        Access to Growth Opportunities
                                    </p>
                                    <div class="collapse" id="collapse-6">
                                        <div class="padding-tb-20">
                                            <ul class="icon-list txt-sm">
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Experience personal development
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Challenge the status quo, get inspired with new strategies to grow your business 
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Access a Network of suppliers and partners
                                                    </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-chevron-right"></i>
                                                    <span>
                                                        Get first-hand information about local Events and network meetings where you can meet potential partners, buyers, experts &amp; investors
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p class="">
                                        <a class="txt-sm txt-color-green" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">
                                            Learn more
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="padding-t-40 padding-b-20">
        <div class="container-wrapper">
            <div class="row row-40 text-center">
                <div class="col-md-3 padding-lr-40 padding-b-20">
                    <h4 class="txt-2em txt-medium margin-b-10">
                        46
                    </h4>
                    <h4 class="txt-normal-s txt-medium margin-b-10">
                        Side Hustle Communities
                    </h4>
                    <p class="txt-xs">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </p>
                </div>
                <div class="col-md-3 padding-lr-40 padding-b-20">
                    <h4 class="txt-2em txt-medium margin-b-10">
                        56
                    </h4>
                    <h4 class="txt-normal-s txt-medium margin-b-10">
                        Mentors
                    </h4>
                    <p class="txt-xs">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </p>
                </div>
                <div class="col-md-3 padding-lr-40 padding-b-20">
                    <h4 class="txt-2em txt-medium margin-b-10">
                        100+
                    </h4>
                    <h4 class="txt-normal-s txt-medium margin-b-10">
                        Job Listings
                    </h4>
                    <p class="txt-xs">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </p>
                </div>
                <div class="col-md-3 padding-lr-40 padding-b-20">
                    <h4 class="txt-2em txt-medium margin-b-10">
                        46
                    </h4>
                    <h4 class="txt-normal-s txt-medium margin-b-10">
                        Mentors
                    </h4>
                    <p class="txt-xs">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-ocean-6 padding-t-80 padding-b-60">
        <div class="container-wrapper text-center txt-color-white">
            <div class="row">
                <div class="col-md-6 mx-auto padding-lr-20">
                    <h1 class="txt-medium txt-2em margin-b-30">
                        Sign up for the SAEDConnect Package
                    </h1>
                    <article class="description">
                        <p class="txt-normal-s">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus repudiandae sed ipsum id, praesentium. Maiores possimus facere, dolores ducimus doloribus similique voluptas. Dicta nobis adipisci libero iste, incidunt! Voluptatem, nam.
                        </p>
                    </article>
                    <div class="margin-t-40">
                        <a class="btn btn-trans-wb no-m-b">
                            Get Started
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
        
<?php get_footer() ?>