<?php /*Template Name: Venture Request - Partners*/ ?>
    
<?php get_header() ?>

<?php
    /* Reset Global Queried Object  */
    wp_reset_postdata();
    wp_reset_query();
   
    while ( have_posts() ) : the_post();

        $page_title = get_the_title();
        $gf_id = get_the_content();
   
    endwhile; // end of the loop.
?>

<main class="main-content">
    <section class="bg-venture-home-hero" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/heroes/table.jpg)">
        <div class="container-wrapper content padding-t-40 padding-b-20">
            <div class="row">
                <div class="col-md-5 txt-color-white">
                    <h1 class="txt-3em txt-height-1-2 txt-bold margin-b-20">
                        Hire freelancers. <br> Make things <br> happen.
                    </h1>
                    <h2 class="txt-lg txt-height-1-7">
                        Grow your business through the top freelancing website. Hire talent nearby or worldwide.
                    </h2>
                </div>
            </div>
        </div>
    </section>

    <section class="venture-request-filter padding-tb-15 margin-b-40">
        <div class="container-wrapper">
            <div class="row row-40 align-items-center">
                <div class="col-md-6 padding-lr-40">
                    <div class="txt-xlg txt-medium txt-color-light txt-height-1-2">
                        6,334 Live Business Partner Requests
                    </div>
                </div>
                <div class="col-md-6 txt-normal-s text-right padding-lr-40">
                    <span class="txt-color-blue padding-r-20">
                        Filter by:
                    </span>
                    <a href="" class="padding-r-20">
                        Location
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <a href="" class="padding-r-20">
                        Location
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <a href="">
                        Location
                        <i class="fa fa-angle-down"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
        
    <section class="container-wrapper padding-b-80">
        <div class="row row-15">
        <?php
            $request_type_array = array(
                array(
                    'gf_id' => 20,
                    'parent_post_field' => 15,
                ),
                array(
                    'gf_id' => 21,
                    'parent_post_field' => 12,
                    'gv_id' => 99,
                    'title' => 'Land Request',
                ),
                array(
                    'gf_id' => 22,
                    'parent_post_field' => 13,
                    'gv_id' => 20,
                    'title' => 'Request for Workspace/Office Space/Building/Factory Space',
                ),
                array(
                    'gf_id' => 23,
                    'parent_post_field' => 13,
                    'gv_id' => 20,
                    'title' => 'Request for Collaborator/Volunteer',
                ),
                array(
                    'gf_id' => 24,
                    'parent_post_field' => 9,
                    'gv_id' => 20,
                    'title' => 'Request for Funding',
                ),
                array(
                    'gf_id' => 25,
                    'parent_post_field' => 11,
                    'gv_id' => 20,
                    'title' => 'Request for Marketing/Publicity',
                ),
                array(
                    'gf_id' => 27,
                    'parent_post_field' => 2,
                    'gv_id' => 20,
                    'title' => 'Request for a Mentor',
                ),
            );

            /* GF Search Criteria */
            $search_criteria = array();

            /* Get GF Entry Count */
            $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
            $entries = GFAPI::get_entries( $gf_id, $search_criteria );
            $parent_post_id;

            foreach( $entries as $entry ){
                /* Get Parent Post ID */
                foreach( $request_type_array as $request_item ){
                    /*echo $request_item['gf_id'];
                    echo '<br>';
                    echo $gf_id;
                    echo '<br><br><br>';*/
                    if($request_item['gf_id'] == $gf_id){
                        $parent_post_id = $request_item['parent_post_field'];
                    }
                }

                $parent_post_id = rgar( $entry, $parent_post_id );
                $entry_post_id = $entry['post_id'];

                $parent_post = get_post($parent_post_id);
                $entry_post = get_post($entry_post_id);
        ?>
            <div class="col-md-6 padding-lr-15 padding-b-30">
                <div 
                    class="venture-request-card"
                    req-title="<?php echo $entry_post->post_title; ?>"
                    <?php
                        $meta = get_post_meta($entry_post_id, 'Co-Founder_search', true);
                        if($meta){
                            echo 'Co-Founder_search="'.$meta.'"';
                        }
                
                        $meta = get_post_meta($entry_post_id, 'Co-Founder_role', true);
                        if($meta){
                            echo 'Co-Founder_role="'.$meta.'"';
                        }
                
                        $meta = get_post_meta($entry_post_id, 'Co-Founder_responsibilities', true);
                        if($meta){
                            echo 'Co-Founder_responsibilities="'.$meta.'"';
                        }
                
                        $meta = get_post_meta($entry_post_id, 'Co-Founder_commitment', true);
                        if($meta){
                            echo 'Co-Founder_commitment="'.$meta.'"';
                        }
                
                        /* Reward */
                        $field = 'Co-Founder_reward';
                        $output_string;

                        $meta = get_post_meta($entry_post_id, $field, false);

                        if($meta){
                            
                            foreach($meta as $key=>$value) {
                                $output_string .= $value;
                                if($key < count($meta) - 1 ){
                                    $output_string .= ', ';
                                }
                            }
                            
                            echo 'Co-Founder_reward="'.$output_string.'"';
                        }
                
                        $meta = get_post_meta($entry_post_id, 'Co-Founder_location', true);
                        if($meta){
                            echo 'Co-Founder_location="'.$meta.'"';
                        }
                
                        $meta = get_post_meta($entry_post_id, 'Co-Founder_date', true);
                        if($meta){
                            $date = strtotime($meta);
                            echo 'Co-Founder_date="'.date('j F Y',$date).'"';
                        }
                    ?>
                >
                    <div class="row row-10 margin-b-20">
                        <div class="col-8 padding-lr-10">
                            <h3 class="margin-b-10 txt-lg txt-medium txt-color-blue">
                                <?php echo $entry_post->post_title; ?>
                            </h3>
                            <h3 class="txt-xs txt-color-light">
                                by <span class="txt-color-dark"><?php echo $parent_post->post_title; ?></span>
                            </h3>
                        </div>
                        <div class="col-4 txt-sm text-right padding-lr-10">
                            <i class="fa fa-clock-o txt-color-red"></i>
                            <span class="txt-color-lighter padding-l-5">
                                Posted 9 minutes ago
                            </span>
                        </div>
                    </div>
                    <div class="txt-sm margin-b-10">
                        <span class="padding-r-10 txt-color-lighter">
                            Type:
                        </span>
                        <span class="txt-medium">
                            <?php
                                $meta = get_post_meta($entry_post_id, 'tool_type', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </span>
                    </div>
                    <div class="txt-sm margin-b-10">
                        <span class="padding-r-10 txt-color-lighter">
                            Quantity:
                        </span>
                        <span class="txt-medium">
                            <?php
                                $meta = get_post_meta($entry_post_id, 'tool_quantity', true);
                                if($meta){
                                    echo $meta;
                                }
                            ?>
                        </span>
                    </div>
                    <div class="txt-sm margin-b-10">
                        <span class="padding-r-10 txt-color-lighter">
                            Duration:
                        </span>
                        <span class="txt-medium">
                            <?php
                                $meta = get_post_meta($entry_post_id, 'tool_need_duration', true);
                                if($meta){
                                    echo $meta; 
                                }
                            ?>
                        </span>
                    </div>
                    <div class="row row-10 margin-t-30">
                        <h3 class="col-8 padding-lr-10 txt-normal-s txt-medium txt-color-red">
                            8 People responded
                        </h3>
                        <div class="col-4 txt-sm text-right padding-lr-10">
                            <i class="fa fa-map-marker txt-color-red"></i>
                            <span class="padding-l-5">
                                Lagos
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
    </section>
        
    <section class="container-wrapper bg-grey padding-t-80 padding-b-40 text-center">
        <h2 class="txt-2-4em txt-medium margin-b-60">
            How it works
        </h2>
        <div class="row row-20">
            <div class="col-md-3 padding-lr-40 padding-b-40">
                <div class="margin-b-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_access_to_grow.png" alt="" width="80">
                </div>
                <h3 class="txt-bold margin-b-10">
                    Post a job (it’s free) 
                </h3>
                <p class="txt-sm txt-height-1-7">
                    Tell us about your project. Upwork connects you with top talent around the world, or near you. 
                </p>
            </div>
            <div class="col-md-3 padding-lr-40 padding-b-40">
                <div class="margin-b-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_build.png" alt="" width="80">
                </div>
                <h3 class="txt-bold margin-b-10">
                    Freelancers come to you
                </h3>
                <p class="txt-sm txt-height-1-7">
                    Tell us about your project. Upwork connects you with top talent around the world, or near you. 
                </p>
            </div>
            <div class="col-md-3 padding-lr-40 padding-b-40">
                <div class="margin-b-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_amazing_support_network.png" alt="" width="80">
                </div>
                <h3 class="txt-bold margin-b-10">
                    Payment simplified
                </h3>
                <p class="txt-sm txt-height-1-7">
                    Tell us about your project. Upwork connects you with top talent around the world, or near you. 
                </p>
            </div>
            <div class="col-md-3 padding-lr-40 padding-b-40">
                <div class="margin-b-40">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/eSAED/eS_challenge_the_status_quo.png" alt="" width="80">
                </div>
                <h3 class="txt-bold margin-b-10">
                    Collaborate easily
                </h3>
                <p class="txt-sm txt-height-1-7">
                    Tell us about your project. Upwork connects you with top talent around the world, or near you. 
                </p>
            </div>
        </div>
    </section>

    <section class="container-wrapper padding-tb-80">
        <h3 class="txt-lg txt-medium margin-b-40">
            Top skills
        </h3>
        <div class="row row-20">
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>

            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
            <div class="col-md-3 padding-lr-20 padding-b-20">
                <a href="">
                    Android Developer
                </a>
            </div>
        </div>
    </section>
</main>
  
<aside class="venture-request-detail font-main padding-o-20">
    <div class="overflow-hidden padding-b-20 margin-b-20 border-b-1 border-color-darkgrey">
        <div class="row row-10">
            <div class="col-8 padding-lr-10">
                <h2 class="txt-lg txt-medium txt-color-light" id="req-title">
                    I need a laptop
                </h2>
            </div>
            <div class="col-4 padding-lr-10 text-right">
                <i class="fa fa-times close"></i>
            </div>
        </div>
    </div>
    <div class="font-weight-normal" id="request-data">
                
    </div>
    <div>
        <a 
            href="<?php printf('https://www.saedconnect.org/ventures-directory/respond-to-request/?form-title=Respond To Request&gf-id=26&parent_id=%s', $entry_post_id); ?>" 
            class="btn btn-blue txt-sm full-width"
        >
            Respond to Request
        </a>
    </div>
</aside>

<div class="overlay"></div>
   
<?php get_footer() ?>

<script>
    $(".venture-request-card, .venture-request-detail .close, .overlay").click(function(){
        $("body").toggleClass("noScroll");
        $(".overlay").fadeToggle(200);
        $(".venture-request-detail").toggleClass("open");
    });

    $(".venture-request-card").click(function(){
        var request = $(this);
        
        $(".venture-request-detail #req-title").text(request.attr('req-title'));
        $(".venture-request-detail #request-data").empty();
        
        var meta_array = [
            {name: 'Why are you looking for a co-founder?', id:'Co-Founder_search'}, 
            {name: 'What role do you want the co-founder to fill?', id:'Co-Founder_role'},
            {name: 'Responsibilities the co-founder will handle.', id:'Co-Founder_responsibilities'},
            {name: 'Level of commitment?', id:'Co-Founder_commitment'},
            {name: 'How will the co-founder be rewarded?', id:'Co-Founder_reward'},
            {name: 'Location where you need the Partner/Co Founder', id:'Co-Founder_location'},
            {name: 'Request Expires', id:'Co-Founder_date'},
        ];

        meta_array.forEach(function(curValue, index, arr){
            if(request.attr(curValue.id)){
                //console.log( request.attr(curValue));
                //console.log( curValue.name + ': ' + request.attr(curValue.id));

                //$(".venture-request-detail #"+curValue).text(request.attr(curValue));
                var append_string = '<div class="padding-o-10 margin-b-20 border-o-1 border-color-darkgrey"><p class="txt-xs txt-bold uppercase txt-color-light margin-b-10">'+curValue.name+'</p><p class="txt-sm">'+request.attr(curValue.id)+'</p></div>'; 
                
                $(".venture-request-detail #request-data").append(append_string);
            }
        });
    });


</script>