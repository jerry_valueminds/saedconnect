    <?php /*Template Name: Homepage-Career*/ ?>
    
    <?php get_header() ?>
   
    <main class="main-content">
        <header class="overview-header container-wrapper">
            <h1 class="title">
                <span class="txt-bold txt-height-1-2 padding-b-20">
                    Start / Build your Career
                </span>
            </h1>
        </header>
        
        <!-- General Section -->
        <section>
            <div class="row">
                <article class="col-md-8 feature-image-block d-flex"
                    style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (30).png');">
                    <div class="content txt-color-white d-flex">
                        <div class="align-self-end">
                            <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                                Success Keys
                            </h3>
                            <p class="txt-height-1-5 margin-b-30">
                                Our Success keys program focuses on helping you build core life and workplace readiness skills that will accelerate your growth. You will gain broad knowledge, skills  & core abilities required across the job & entrepreneurship landscape.
                            </p>
                            <div class="d-flex" style="user-select:none">
                                <span class="d-inline padding-lr-20 padding-tb-10 border-o-1 border-color-white txt-normal-s">
                                    Coming Soon
                                </span>
                            </div>
                        </div>
                    </div>   
                </article>
                <!--<article class="col-md-4 feature-image-block d-flex"
                    style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (20).jpg');">
                    <div class="content txt-color-white d-flex flex-column justify-content-between">
                        <div class="">
                            <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                                The Job Challenge
                            </h3>
                            <p class="txt-height-1-5 margin-b-30">
                                The Job Challenge is an immersive coaching/mentoring program that helps you build competency to excel in specific job roles, & connects you to employers hiring for that role. Interviews are guaranteed for every Job Challenge participant.
                            </p>
                        </div>
                        <div>
                            <a class="btn btn-trans-wb" href="">
                                Explore
                            </a>
                        </div>
                    </div>   
                </article>-->
                
                <div class="col-md-4 cta-block bg-yellow">
                    <article class="txt-color-white-dark">
                        <h2 class="title txt-height-1-2">
                            Getting a Job & excelling in it is not rocket science. Find all the information, tips and tricks that will help you navigate the maze to get and grow in your dream career.
                        </h2>
                    </article>
                </div>
                
                <div class="col-md-4 bg-black-off padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Position Yourself
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                You are responsible for how employers see you. Learn all you need to craft the right impression.
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/guide-section/finding-your-path/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Finding Your Path
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/mini-site/what-employers-are-looking-for/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        What employers want
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/guide-section/crafting-your-cv-social-profiles-cover-letters/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Building a great CV & Cover letter
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/mini-site/managing-your-online-image-social-media/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Crafting an appealing social media profile
                                    </span>
                                </a>
                            </li>
                            
                        </ul>
                        <div class="margin-t-30">
                            <a class="btn btn-trans-wb" href="https://www.saedconnect.org/career-guide/">
                                Find Answers &amp; More
                            </a> 
                        </div>  
                    </div>
                </div>
                <div class="col-md-4 bg-black-off padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Job Search & Interviews
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                Find out how to discover great Jobs, secure interviews and get hired.
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/guide-section/searching-for-jobs-securing-interviews/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Searching for Jobs & Securing Interviews
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/mini-site/networking-your-way-to-a-job/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Networking your way to a Job
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/guide-section/acing-interviews/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Acing interviews: What to do before, during & after the interview.
                                    </span>
                                </a>
                            </li>
                        </ul>  
                    </div>
                </div>
                <div class="col-md-4 bg-black-off padding-tb-40">
                    <div class="right-content txt-color-white">
                        <h2 class="txt-xxlg txt-medium padding-b-30">
                            Becoming Successful
                        </h2>
                        <article class="text-box sm txt-color-white">
                            <p class="txt-sm">
                                You are hired! What next? Here is what you need to know to excel on your job.
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/mini-site/your-first-day-week-at-work/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Your first day/Week at work
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/mini-site/career-planning/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Career Planning
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.saedconnect.org/career-guide/mini-site/navigating-office-politics/">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Navigating Office Politics
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="margin-t-30">
                            <a class="btn btn-trans-wb" href="https://www.saedconnect.org/career-guide/guide-section/advancing-your-career/">See all</a> 
                        </div>  
                    </div>
                </div>
                
                <div class="col-md-4 flip-card">
                    <div class="feature-image-block front d-flex"
                       style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (27).jpg');">
                        <div class="content txt-color-white align-self-end">
                            <h4 class="txt-normal-s margin-b-30">
                                <span class="d-inline bg-yellow txt-color-dark padding-tb-5 padding-lr-10 rounded-corners">
                                    Tool
                                </span>
                            </h4>
                            <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-10">
                                Online CV Creator
                            </h3>
                            <p class="txt-height-1-5 margin-b-30">
                                Create an amazing CV for yourself and download different designs in PDF.
                            </p>
                            <button class="btn btn-trans-wb">
                                Learn more 
                            </button>
                        </div> 
                    </div>
                    <div class="feature-image-block back" style="background-image:url('images/heroes/hero_4.jpg')">
                        <div class="content txt-color-white">
                            <p class="txt-height-1-5 margin-b-30">
                                Create an amazing CV for yourself on the fly using our CV creator tool and download different designs in PDF. Pick beautiful industry standard templates, use our well crafted examples to guide you & download your awesome resume.
                            </p>
                            <article class="btn-wrapper">
                                <a class="btn btn-trans-wb" href="https://hashcode.withgoogle.com/">
                                    Get started                                          
                                </a>
                            </article>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 flip-card">
                    <div class="feature-image-block front d-flex"
                       style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (28).jpg');">
                        <div class="content txt-color-white align-self-end">
                            <h4 class="txt-normal-s margin-b-30 txt-color-dark">
                                <span class="d-inline bg-yellow padding-tb-5 padding-lr-10 rounded-corners">
                                    Tool
                                </span>
                            </h4>
                            <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-10">
                                The Database of Stars
                            </h3>
                            <p class="txt-height-1-5 margin-b-30">
                                Get hired for specific job roles by local and international employers looking for graduates like you
                            </p>
                            <button class="btn btn-trans-wb">
                                Learn more 
                            </button>
                        </div> 
                    </div>
                    <div class="feature-image-block back" style="background-image:url('images/heroes/hero_4.jpg')">
                        <div class="content txt-color-white">
                            <p class="txt-height-1-5 margin-b-30">
                                You have a skill, strength and advantage that employers are looking for. Join the JobStar database & get exposed to local and international employers looking for graduates like you. Through your JobStar profile, you will be able to access the experience log where you get to update your growth & development activities which allows potential employers see your capabilities in a clearer picture boosts your chances of getting a good job on time.
                            </p>
                            <article class="btn-wrapper">
                                <a class="btn btn-trans-wb" href="https://hashcode.withgoogle.com/">
                                    Get started                                           
                                </a>
                            </article>
                        </div>
                    </div>
                </div>
                
                <article class="col-md-4 feature-image-block"
                    style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (22).jpg');">
                    <div class="content txt-color-white">
                        <h3 class="txt-xlg txt-medium txt-height-1-2 margin-b-20">
                            Are you an employer?
                        </h3>
                        <p class="txt-height-1-5 margin-b-30">
                            Hire exceptional talents who successfully complete the Job Challenge Program or from our Database of Stars - an exclusive pool of candidates with verified skill sets.
                        </p>
                        <a class="btn btn-trans-wb" href="">
                            Learn more 
                        </a>
                    </div>   
                </article>
                
                <div class="col-md-12 cta-block" style="padding-bottom:0;">
                    <h4 class="txt-xlg margin-b-30">Career Support Services</h4>
                    <div class="row row-20">
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15 txt-height-1-3">
                                Career Coaching Services
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Take advantage of our career coaching services to make critical decisions about career direction, choices and advancement.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/career-coaching/">
                                Get started
                            </a> 
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15 txt-height-1-3">
                                CV & Cover Letter Creation & Review
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Get practical tips and live interview practice to increase your chances for success in the real interview.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/cv-cover-letter-prep/">

                                I'm interested
                            </a> 
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15 txt-height-1-3">
                                LinkedIn Page Creation / Review & Networking Coaching
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Get practical tips and live interview practice to increase your chances for success in the real interview.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/linkedin-page-creation-review-networking-coaching/">
                                I'm interested
                            </a> 
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15 txt-height-1-3">
                                Job Search Coaching
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Get practical tips and live interview practice to increase your chances for success in the real interview.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/job-search-coaching/">
                                I'm interested
                            </a> 
                        </div>
                        <div class="col-md-4 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15 txt-height-1-3">
                                Interview Coaching
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Get live interview practice & feedback to increase your chances for success in the real interview. 
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/interview-prep/">
                                Sign me up
                            </a> 
                        </div>
                        <!--<div class="col-md-6 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15">
                                Register for a Professional Exam
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Our exam logistics team will help you register & prepare for any local or international professional exam you are interested in.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/professional-association/">
                                Learn more
                            </a> 
                        </div>
                        <div class="col-md-6 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15">
                                School Overseas
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    Thinking to further your education in a school outside your home country? Let our education consultants help you. 
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/school-overseas/">
                                Learn more
                            </a> 
                        </div>
                        <div class="col-md-6 padding-lr-20 padding-b-40">
                            <h4 class="txt-bold margin-b-15">
                                Work Overseas
                            </h4>
                            <article class="text-box sm">
                                <p>
                                    There are opportunities to work across Africa, America, Europe, Asia and Australia. Our consultants will help you through the process of finding your dream job overseas.
                                </p>
                            </article>
                            <a class="btn btn-trans-blue" href="http://www.saedconnect.org/about/discourse/work-overseas/">
                                Learn more
                            </a> 
                        </div>-->
                    </div>
                </div>
            </div>
        </section>
        <section class="pre-footer" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/grid/image (18).jpg');">
            <div class="content">
                <h4 class="title">
                    Join the SAEDConnect Community
                </h4>
                <p class="txt-height-1-4 txt-color-white margin-b-40">
                    Be the first to know about trainings, events, programs, support and opportunities available to help you start and grow your business.
                </p>
                <article class="btn-wrapper margin-b-40">
                    <a class="btn btn-trans-wb-icon" href="https://www.facebook.com/saedconnect">
                        Connect on Facebook
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="btn btn-trans-wb-icon" href="https://www.twitter.com/saedconnect">
                        Follow us on Twitter
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="btn btn-trans-wb-icon" href="https://t.me/esaed">
                        Join the Telegram Channel
                        <i class="fa fa-telegram"></i>
                    </a>
                </article>
                <article>
                    <h4 class="txt-xlg txt-color-white margin-b-20">Join the Support Mailing List</h4>
                    <form action="" class="mailing-list-form">
                        <input class="mail-field" type="email" placeholder="Enter your email address">
                        <input class="submit-btn" type="submit" value="subscribe">
                    </form>
                </article>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>