<?php
                    
    if ( !is_user_logged_in() && $_GET['view'] == 'apply' ) {
        // If User is Logged in, redirect to User Dashbord
        //$dashboard_link = get_site_url().'/my-dashboard'; //Get Daasboard Page Link by ID
        $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID

        if ( wp_redirect( $dashboard_link ) ) {
            exit;
        }
    }

?>

<?php get_header() ?>

<?php
    /* Include WP Global Object */
    global $wpdb;

    /* Applications DB Table Name */
    $table = "applications";

    /* Get Applications DB */
    $application_db = new wpdb('root','umMv65ekyMRxfNfm','applications_db','localhost'); //Server

    /* Get Current User */
    $current_user = wp_get_current_user();

    /* Gravity Forms/Views IDs */
    $gf_id = 5;
    $gv_id = 35;
    $entry_id = 0;
    
    // TO SHOW THE POST CONTENT
    while ( have_posts() ) : the_post();

        $post_id = get_the_ID();
        $post_link = get_permalink();
        $post_author_id = get_the_author_meta('ID');

         /* Get current User ID */
        $current_user = wp_get_current_user();

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                'key' => '13', 'value' => $post_id, //Current logged in user
            )
          )
        );

        /* Get GF Entry Count */
        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

        foreach( $entries as $entry ){

            $entry_id = $entry['id'];
        }
    
?>

    <main class="main-content">
        <header class="container-wrapper padding-tb-20">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="txt-sm">
                        <a href="marketplace_service_home.html" class="txt-color-dark padding-r-10">
                            Job Marketplace
                        </a>
                        <i class="fa fa-angle-right padding-r-10"></i>
                        <a href="marketplace_service_category.html" class="txt-color-dark">
                            Jobs
                        </a>
                    </h1>
                </div>
            </div>
        </header>
        
        <?php if ( $_GET['view'] == 'apply' ) { ?>
        
        <div class="container-wrapper padding-tb-40 bg-grey">
            <div class="row">
                <div class="col-md-8 mx-auto text-center">
                    <h1 class="txt-xlg">
                        <?php the_title(); ?> Application
                    </h1>
                </div>
            </div>
        </div>
        <section class="container-wrapper padding-t-40 padding-b-80">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="row padding-b-20 margin-b-40 border-b-1 border-color-darkgrey">
                        <h2 class="col-md-8 txt-lg txt-medium">
                            My CV
                        </h2>
                        <div class="col-md-4 text-md-right">
                            <a href="https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv" class="btn btn-trans-bw txt-xs no-m-b">
                                View CV
                            </a>
                        </div>
                    </div>
                    <?php

                        /*
                        *
                        *   Check if user has CV Profile
                        *
                        */
                        /* Get current User ID */
                        $current_user = wp_get_current_user();

                        //echo 'User ID: '.$current_user->ID;

                        /* GF Search Criteria */
                        $search_criteria = array(

                        'field_filters' => array( //which fields to search
                                array(

                                    'key' => 'created_by', 'value' => $current_user->ID //Current logged in user
                                )
                            )
                        );

                        switch_to_blog(20);

                        $reqCV = array(
                            array(
                                'id' => 84,
                                'name' => 'Personal Information',
                                'link' => 'https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv',
                            ),

                            array(
                                'id' => 28,
                                'name' => 'Social Details',
                                'link' => 'https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&cv-form=28&cv-part=Social%20Details',
                            ),

                            array(
                                'id' => 31,
                                'name' => 'Education & Training',
                                'link' => 'https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&cv-form=31&cv-part=Education%20&%20Training',
                            ),

                            array(
                                'id' => 34,
                                'name' => 'Skills',
                                'link' => 'https://www.saedconnect.org/growth-programs/my-dashboard/?action=cv&cv-form=34&cv-part=Skills',
                            ),
                        );

                        $check = array();

                        foreach($reqCV as $cv){
                            /* Get GF Entry Count */
                            $profile_count = GFAPI::count_entries( $cv['id'], $search_criteria );

                            if( $profile_count ){
                                $check[] = 'complete';
                    ?>

                                <p class="row align-items-center margin-b-10">
                                    <span class="col-8">
                                        <?php echo $cv['name'] ?>
                                        <i class="txt-color-green fa fa-check"></i>
                                    </span> 
                                    <span class="col-md-4 text-right txt-sm">
                                        <a href="<?php echo $cv['link'] ?>" class="txt-underline txt-color-dark txt-medium">
                                            Edit
                                        </a>
                                    </span>
                                </p>

                    <?php 
                            } else { 
                                $check[] = 'incomplete';
                    ?>

                        <p class="row align-items-center margin-b-10">
                            <span class="col-8">
                                <?php echo $cv['name'] ?>
                                <i class="txt-color-red fa fa-times"></i>
                            </span> 
                            <span class="col-md-4 text-right txt-sm">
                                <a href="<?php echo $cv['link'] ?>" class="txt-underline txt-color-dark txt-medium">
                                    Complete
                                </a>
                            </span>
                        </p>

                    <?php
                            } 
                        }


                        if (in_array("incomplete", $check)){

                            echo '<p class="txt-medium text-center txt-color-red padding-o-20 margin-t-40 border-o-1 border-color-darkgrey">You must Complete the parts of your CV highlighted above before you can submit an Application</p>';

                        } else {
                            printf('<p class="text-center margin-t-40"><a href="%s?view=application-confirmation" class="btn btn-trans-bw txt-sm">Submit CV</a></p>', $post_link);
                        }

                        restore_current_blog();
                    ?>
            </div>
            </div>
        </section>
        
        <?php } elseif ( $_GET['view'] == 'application-confirmation' ) {  ?>
                
        <div class="container-wrapper padding-tb-40">
            <div class="row">
                <div class="col-md-8 mx-auto text-center">
                    <h1 class="txt-xlg margin-b-20">
                    <?php
                        /* Check if User is signed */
                        if ( is_user_logged_in() ) {

                            /* Get Applications DB */
                            $application_db = new wpdb('root','umMv65ekyMRxfNfm','applications_db','localhost'); //Server

                            /* Check Application Entry in DB */
                            $application_entry = $application_db->get_row("SELECT post_id, post_type FROM ".$table." WHERE user_id = '".$current_user->ID."' LIMIT 0,1");

                            /* Check if User has applied*/
                            if( $application_entry ){

                                echo "You have already applied.";

                            } else {

                                /* Create application status*/
                                $application_db->insert( 
                                    $table, 
                                    array( 
                                        "post_id" => $post_id,
                                        "post_type" => "job",
                                        "user_id" => $current_user->ID,
                                        "status" => "applied",
                                    ), 
                                    array( "%d", "%s", "%d", "%s" ) 
                                );

                                echo "Your Application for the ".get_the_title().' was succesfully submitted.';
                            }
                            
                        } else {
                            
                            echo "You must be logged in to continue";
                            
                        }
                    ?>
                    </h1>
                </div>
            </div>
        </div>
               
        <?php }elseif( $_GET['view'] == 'manage-applicants' ){ ?>
               
            <div class="container-wrapper padding-tb-20 bg-grey margin-b-40">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <p class="d-inline-block bg-ash txt-xxs txt-medium uppercase txt-color-white padding-tb-5 padding-lr-10 margin-b-5">
                            Application Manager
                        </p>
                        <h1 class="txt-lg txt-medium margin-b-10">
                            <?php the_title(); ?>
                        </h1>
                        <?php if( $post_author_id == $current_user->id ){ ?>
                            <p class="txt-sm">
                                <a
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry_id.'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=job&form-title='.$form_title.'&post_id='.$post_id; ?>"
                                    class="txt-color-green"
                                >
                                    Edit
                                </a>
                                <span class="padding-lr-5">|</span>
                                <a
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=job';  ?>" 
                                    class="txt-color-red confirm-delete"
                                >
                                    Delete
                                </a>
                            </p>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 text-md-right">
                        <a href="<?php echo get_permalink(); ?>" class="btn btn-blue txt-xs no-m-b">
                            Back Job Details
                        </a>                   
                    </div>
                </div>
                
                
            </div>
               
            <div class="container-wrapper">
            <?php
                $application_entries = $application_db->get_results("SELECT user_id, status FROM ".$table." WHERE post_id = ".$post_id." LIMIT 0,10");
                                                              
                //print_r($application_entries);
            ?>
               
            <?php foreach( $application_entries as $application_entry ){ ?>
               
                <div class="row row-15 txt-normal-s txt-height-1-4 padding-tb-10 border-b-1 border-color-darkgrey">
                    <div class="col-3 padding-lr-15">
                       <?php
                            /* Get Avatar */
                            $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
                            $meta_key = 'user_avatar_url';
                            $get_avatar_url = get_user_meta($application_entry->user_id, $meta_key, true);

                            if($get_avatar_url){
                                $avatar_url = $get_avatar_url;
                            }

                            /* Get User Display Name */
                            switch_to_blog(1);

                            $gf_id = 4; //Form ID
                            $username_entry_count = 0;

                            /* GF Search Criteria */
                            $search_criteria = array(

                            'field_filters' => array( //which fields to search

                                array(

                                    'key' => 'created_by', 'value' => $application_entry->user_id, //Current logged in user
                                    )
                                )
                            );

                            /* Get Entries */
                            $username_entries = GFAPI::get_entries( $gf_id, $search_criteria );

                            /* Get GF Entry Count */
                            $username_entry_count = GFAPI::count_entries( $gf_id, $search_criteria );


                            if($username_entry_count){ //If no entry
                                $displayname = rgar( $username_entries[0], '4.3' ).' '.rgar( $username_entries[0], '4.6' );
                            }   

                            restore_current_blog();
                        ?>

                        <style>
                           .author_avatar {
                                display: inline-block;
                                background-size: cover !important;
                                background-position: center !important;
                                background-repeat: no-repeat !important;
                                width: 1.5rem;
                                height: 1.5rem;
                                margin-right: 10px;
                                border-radius: 50%;
                            }
                        </style>
                        <div class="d-flex align-items-center">
                            <figure class="author_avatar" style="background-image:url('<?php echo $avatar_url; ?>');">

                            </figure>
                            <div class="d-inline-block txt-normal-s txt-medium" style="text-transform:capitalize">
                                <?php echo ($application_entry->user_id == 1)? 'SAEDConnect Admin' : $displayname; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-2 padding-lr-15">
                        <a href="<?php printf("https://www.saedconnect.org/competency-profile/mail-user/?reference_post=%s&user_id=%s&blog_id=%s&post_id=%s", get_permalink(), get_the_author_meta('ID'), $template_information[ 'blog_id' ], $post_id ) ?>" target="_blank">
                            <i class="fa fa-file-o padding-r-5"></i>
                            View CV
                        </a>
                    </div>
                    <div class="col-2 padding-lr-15 text-right">
                        <div class="dropdown padding-l-20">
                            <a class="" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa-stack">
                                    <i class="fa fa-circle fa-stack-2x txt-grey"></i>
                                    <i class="fa fa-ellipsis-v fa-stack-1x txt-color-white"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu txt-normal" aria-labelledby="dropdownMenuLink">

                                <?php 
                                    $page_view_array = array(
                                        'pending' => array(
                                            'name' => 'Pending Approval',
                                            'meta_key' => 'user_published',
                                            'page_summary' => 'These posts have not been reviewed.',
                                            'actions' => array(
                                                array(
                                                    'cta_text' => 'Publish',
                                                    'action' => 'admin_published',
                                                    'btn_styles' => 'btn btn-trans-bw txt-sm no-m-b'
                                                ),
                                                array(
                                                    'cta_text' => 'Move to Review',
                                                    'action' => 'under_review',
                                                    'btn_styles' => 'btn btn-trans-bw txt-sm no-m-b'
                                                )
                                            )
                                        ),

                                        'approved' => array(
                                            'name' => 'Approved',
                                            'meta_key' => 'admin_published',
                                            'page_summary' => 'These posts have been reviewed and approved.',
                                            'actions' => array(
                                                array(
                                                    'cta_text' => 'Unpublish',
                                                    'action' => 'user_published',
                                                    'btn_styles' => 'btn btn-blue txt-sm no-m-b'
                                                ),
                                                array(
                                                    'cta_text' => 'Move to Review',
                                                    'action' => 'under_review',
                                                    'btn_styles' => 'btn btn-trans-bw txt-sm no-m-b'
                                                )
                                            )
                                        ),

                                        'reviewing' => array(
                                            'name' => 'Under Review',
                                            'meta_key' => 'under_review',
                                            'page_summary' => 'These posts are under review.',
                                            'actions' => array(
                                                array(
                                                    'cta_text' => 'Publish',
                                                    'action' => 'admin_published',
                                                    'btn_styles' => 'btn btn-trans-bw txt-sm no-m-b'
                                                ),
                                                array(
                                                    'cta_text' => 'Move to Unpublished',
                                                    'action' => 'user_published',
                                                    'btn_styles' => 'btn btn-blue txt-sm no-m-b'
                                                ),

                                            )
                                        ),            
                                    );
                            
                            
                                    $page_view = $page_view_array[ 'pending' ];
                                    $actions = $page_view['actions'];
                                    $get_page_view ="";

                                    for ($i = 0; $i < count( $actions ); $i++) {

                                ?>

                                <a 
                                   href="<?php printf('%s&view=form&action=publication&value=%s&redir=%s&post-id=%s', currentUrl(false), $actions[$i]['action'], $get_page_view, $post_id ) ?>"
                                   class="dropdown-item"
                                >
                                    <?php echo $actions[$i]['cta_text'] ?>
                                </a>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                
            <?php } ?>
            </div>
                
        <?php } else { ?>
        
        <div class="container-wrapper padding-tb-40 bg-grey">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="txt-xxlg txt-medium margin-b-20">
                        <?php the_title(); ?>
                    </h1>
                    <p class="txt-normal-s">
                        <span>
                            <?php echo rgar( $entry, 14 ) ?>
                        </span>
                        <span class="padding-lr-10">
                            |
                        </span>
                        <span>
                            <?php
                                $field_id = 10; // Update this number to your field id number
                                $field = RGFormsModel::get_field( $gf_id, $field_id );
                                $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                echo $value;
                            ?>
                        </span>
                    </p>
                    <?php if( $post_author_id == $current_user->id ){ ?>
                        <p class="txt-sm margin-t-15">
                            <a
                                href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry_id.'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=job&form-title='.$form_title.'&post_id='.$post_id; ?>"
                                class="txt-color-green"
                            >
                                Edit
                            </a>
                            <span class="padding-lr-5">|</span>
                            <a
                                href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=job';  ?>" 
                                class="txt-color-red confirm-delete"
                            >
                                Delete
                            </a>
                        </p>
                    <?php } ?>
                </div>
                <div class="col-md-4 text-md-right">
                <?php if( $current_user->ID == $post_author_id ){ ?>
                   <a href="<?php echo get_permalink().'/?view=manage-applicants'; ?>" class="btn btn-blue txt-sm no-m-b">
                        Manage Applicants
                    </a> 
                <?php }else{ ?>
                    <a href="<?php echo get_permalink().'/?view=apply'; ?>" class="btn btn-blue txt-sm no-m-b">
                        Apply Now
                    </a>   
                <?php } ?>                
                </div>
            </div>
        </div>
        
        <section class="padding-t-40 padding-b-80">
            <div class="container-wrapper">
                <div class="row">
                    <div class="col-md-8">
                        <h1 class="txt-lg txt-medium margin-b-15">
                            Job Summary
                        </h1>
                        <article class="text-box txt-normal-s txt-height-1-5">
                            <?php echo rgar( $entry, 2 ) ?>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        
        <?php } ?>
    </main>
    
<?php
    endwhile; //resetting the page loop
?>

<script type="text/javascript">
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure? This action can not be undone');
    });
</script>

<?php get_footer() ?>