<?php get_header('user-dashboard') ?>
   
<?php
    /* Get Base URL */
    $base_url = get_site_url().'/my-dashboard';
?>
    
    <header class="dashboard-main-nav font-main ">
        <div class="container-wrapper">
            <div class="box wrapper">
                <a class="brand" href="index.html">
                    <img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/logo.png" alt="">
                    <span class="name">My SAEDConnect</span>
                </a>
                <nav class="nav-box">
                    <ul class="navigation-list">
                        <li>
                            <a href="<?php echo $base_url.'/?action=entrepreneurship-dashboard' ?>">
                                Entrepreneurship Dashboard
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $base_url.'/?action=career-dashboard' ?>">
                                Career Dashboard
                            </a>
                        </li>
                        <li><a href="programs.html">eSAED</a></li>
                        <li><a href="about.html">My Courses</a></li>
                        <li><a href="programs.html">Partner Dashboard</a></li>
                    </ul>
                </nav>
                <div class="home-button bg-white-trans">
                    <button class="hamburger hamburger--spring menu-btn" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
	</header>
    
<?php while ( have_posts() ) : the_post(); ?>

<main class="main-content font-main bg-white">
   
    <div class="container-wrapper">
        <section class="bg-grey text-center padding-tb-60">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h1 class="txt-2em txt-medium margin-b-20">
                        <?php the_title(); ?>
                    </h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo similique ad velit, ex nobis. Ratione sunt quis ipsam, soluta est excepturi ad iusto. Nemo laborum, laudantium quia praesentium eius. Ipsa?
                    </p>
                </div>
            </div>
        </section>
    </div>
    
    <section class="container-wrapper padding-tb-60">
        <div class="row row-40">
            <div class="col-md-6 padding-lr-40">

                <?php 
                    $post_ids = rwmb_meta( 'career-role-tasks' );
                
                    foreach ( $post_ids as $post_id ) {
                        
                        $post = get_post( $post_id )
                            
                        //echo '<p>', get_the_title( $post_id ), '</p>';

                ?>
                    <article class="padding-o-15 margin-b-15 border-o-1 border-color-darkgrey">
                        <a
                            class="btn-link collapsed txt-medium"
                            data-toggle="collapse"
                            href="#main-<?php echo $post->ID ?>"
                            aria-expanded="false"
                            aria-controls="main-1"
                        >
                            <span class="txt-color-dark">
                                <?php the_title(); ?>
                            </span>
                        </a>
                       
                        <div id="main-<?php echo $post->ID ?>" class="collapse" aria-labelledby="heading-1">
                            <div class="padding-b-20 margin-t-20 margin-b-20 border-b-1 border-color-darkgrey">
                                <p class="txt-sm txt-height-1-7 ">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                </p>
                            </div>
                            <div class="padding-b-20">
                                <h4 class="txt-medium margin-b-20">
                                    Experiences
                                </h4>
                                <div class="row txt-normal-s margin-b-15">
                                    <div class="col-6">
                                        Experience Name
                                    </div>
                                    <div class="col text-right">
                                        <a href="" class="padding-r-20 txt-color-green">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="" class="txt-color-red">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row txt-normal-s margin-b-15">
                                    <div class="col-6">
                                        Experience Name
                                    </div>
                                    <div class="col text-right">
                                        <a href="" class="padding-r-20 txt-color-green">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="" class="txt-color-red">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row txt-normal-s margin-b-15">
                                    <div class="col-6">
                                        Experience Name
                                    </div>
                                    <div class="col text-right">
                                        <a href="" class="padding-r-20 txt-color-green">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="" class="txt-color-red">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="margin-t-10">
                                <a href="" class="btn btn-blue txt-sm no-m-b">
                                    Add Experience
                                </a>
                            </div>
                        </div>
                    </article>
                <?php
                    }
                ?>

            </div>
            <div class="col-md-6 padding-lr-40">
                <article class="text-box">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore quo sed, quos odit alias atque maiores officiis explicabo, reprehenderit corporis quae eum maxime dolorem dicta praesentium in veniam. Quia, vero!
                    </p>
                </article>
            </div>
        </div>
    </section>
</main>    
   
<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>