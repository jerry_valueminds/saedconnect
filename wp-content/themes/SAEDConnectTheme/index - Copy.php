    <?php /*Template Name: Homepage*/ ?>
    
    <?php get_header() ?>

    <main class="main-content">
        <!-- Home Gallery -->
        <div class="home-gallery">

            <section class="swiper-wrapper" href="gallery">
            <?php
                $slides = new WP_Query(
                    array(
                        'post_type' => 'homepage-slide',
                        'showposts' => -1,
                    )
                );

                while ($slides->have_posts()) : $slides->the_post();

                    $theme = rwmb_get_value( 'select-slide-theme' );
            ?>
                <div
                    class="swiper-slide txt-color-white
                   
                    <?php
                           
                        if($theme == "themeOne"){
                            echo 'bg-yellow';
                        }elseif($theme == "themeTwo"){
                            echo 'bg-yellow-dark';
                        }
                    ?>
                   
                   ">
                    <div class="container-wrapper padding-tb-40 content">
                        <h2 class="caption"><?php echo rwmb_get_value( 'slide-caption' ); ?></h2>
                        <h1 class="title">
                            <span class="main">
                                <?php echo rwmb_get_value( 'slide-title' ); ?>
                            </span>
                            <span class="sub txt-color-dark">
                                <?php echo rwmb_get_value( 'slide-subtitle' ); ?>   
                            </span>
                        </h1>
                        <a href="<?php echo rwmb_get_value( 'slide-cta-link' ); ?>" class="cta txt-color-white">
                            <span>
                                <?php echo rwmb_get_value( 'slide-cta-text' ); ?>
                            </span>
                        </a>
                    </div>
                </div>
                
             <?php endwhile;
                // Reset things, for good measure
                $slides = null;
                wp_reset_postdata();
            ?>
            </section>
                <!-- Add Pagination -->
            <div class="swiper-pagination-home"></div>
        </div>
        
        <!-- General Section -->
        <section>
            <div class="row">
                <div class="col-md-4">
                    <a class="feature-video-block" href="#" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/people/onewoman.png');">
                        <span class="caption padding-r-40">
                            <h3 class="subtitle txt-color-white">
                                Connect Magazine
                            </h3>
                            <h3 class="txt-xlg txt-medium txt-height-1-2 txt-color-white margin-t-10">
                                Internships give SAEDConnect Students a Leg Up on the Job Market
                            </h3>
                        </span>      
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="feature-video-block" href="#" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/hero_6.jpg');">
                        <span class="caption">
                            <h3 class="title txt-color-white">
                                Event Name
                            </h3>
                        </span>
                        <div class="play-btn">
                            <i>
                                <div class="month">
                                    APR
                                </div>
                                <div class="day">17</div>
                            </i>
                        </div>      
                    </a>
                </div>
                <div class="col-md-4 cta-block bg-black txt-color-white">
                    <article>
                        <article class="content">
                            <h3 class="txt-xxlg txt-height-1-4 txt-medium margin-b-30">
                                We’re here to answer your questions and serve as your mentors through the application process. We’re here to answer your questions and serve as your mentors through the application process.
                            </h3>
                        </article>
                    </article>
                </div>
                
                <div class="col-md-12">
                    <div class="generic-gallery-two-slides relative">
                        <div class="swiper-wrapper" href="gallery">
                            <div class="swiper-slide">
                                <div class="home-slider-item">
                                    <div
                                        class="slide-bg"
                                        style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/honda_5.jpg');">
                                    </div>
                                    <div class="floating-container">
                                        <h4 class="floating-caption bg-blue txt-color-white">
                                            Events
                                        </h4>
                                        <div class="floating-content txt-height-1-4">
                                            <h3 class="uppercase txt-xxlg margin-b-5">Jumpstart</h3>
                                            <h3 class="uppercase txt-xxlg txt-color-light margin-b-20">
                                                July 9 - August 3, 2018
                                            </h3>
                                            <h3 class="uppercase txt-light">
                                                <a class="uppercase txt-color-blue" href="">View</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="home-slider-item">
                                    <div
                                        class="slide-bg"
                                        style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/hero_4.jpg');">
                                    </div>
                                    <div class="floating-container">
                                        <h4 class="floating-caption bg-blue txt-color-white">
                                            Program
                                        </h4>
                                        <div class="floating-content txt-height-1-4">
                                            <h3 class="uppercase txt-xxlg margin-b-5">Jumpstart</h3>
                                            <h3 class="uppercase txt-xxlg txt-color-light margin-b-20">
                                                July 9 - August 3, 2018
                                            </h3>
                                            <h3 class="uppercase txt-light">
                                                <a class="uppercase txt-color-blue" href="">
                                                    <span class="fa fa-play"></span>
                                                    <span>Watch Video</span>
                                                </a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="home-slider-item">
                                    <div
                                        class="slide-bg"
                                        style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/honda_3.jpg');">
                                    </div>
                                    <div class="floating-container">
                                        <h4 class="floating-caption bg-blue txt-color-white">
                                            Connect Magazine
                                        </h4>
                                        <div class="floating-content txt-height-1-4">
                                            <h3 class="uppercase txt-xxlg margin-b-5">Jumpstart</h3>
                                            <h3 class="uppercase txt-xxlg txt-color-light margin-b-20">
                                                July 9 - August 3, 2018
                                            </h3>
                                            <h3 class="uppercase txt-light">
                                                <a class="uppercase txt-color-blue" href="">
                                                    Read More
                                                </a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="home-slider-item">
                                    <div
                                        class="slide-bg"
                                        style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/honda_2.jpg');">
                                    </div>
                                    <div class="floating-container">
                                        <h4 class="floating-caption bg-blue txt-color-white">
                                            Connect Magazine
                                        </h4>
                                        <div class="floating-content txt-height-1-4">
                                            <h3 class="uppercase txt-xxlg margin-b-5">Jumpstart</h3>
                                            <h3 class="uppercase txt-xxlg txt-color-light margin-b-20">
                                                July 9 - August 3, 2018
                                            </h3>
                                            <h3 class="uppercase txt-light">
                                                <a class="uppercase txt-color-blue" href="">
                                                    Read More
                                                </a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Slider Caption -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <article class="feature-image-block bg-red">
                        <div class="content txt-color-white">
                            <h4 class="margin-b-10">Get a Job</h4>
                            <h3 class="txt-xxlg txt-height-1-2 txt-medium margin-b-30">
                                SAEDConnect offers undergraduate and graduate degrees in creative disciplines.
                            </h3>
                        </div>    
                    </article>
                </div> 

                <div class="col-md-4 cta-block bg-orange txt-color-white">
                    <article class="content txt-color-white">
                        <h4 class="margin-b-10">Do Business</h4>
                            <h3 class="txt-xxlg txt-height-1-2 txt-medium margin-b-30">
                                "Pursue your passions. If you like doing what you do, it shows and that can take you places."
                            </h3>
                    </article> 
                </div>       
                <div class="col-md-4 cta-block bg-yellow txt-color-white">
                    <article class="content txt-color-white">
                        <h4 class="margin-b-10">Prepare Yourself</h4>
                        <h3 class="txt-xxlg txt-height-1-2 txt-medium margin-b-30">
                            Take the next step toward a career in art and design.
                        </h3>
                    </article> 
                </div>
                
                <div class="col-md-4 bg-black">
                    <div class="right-content padding-tb-40 txt-color-white">
                        <h2 class="uppercase txt-xlg padding-b-20 margin-b-20 border-b-2">
                            Program Centers
                        </h2>
                        <article class="text-box">
                            <p class="txt-color-white">
                                Learn more about resources available to students, learn more about resources available to students
                            </p>
                        </article>
                        <ul class="icon-list white">
                            <li>
                                <a href="">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Tuition and Fees
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Estimated Costs
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="fa fa-chevron-right"></i>
                                    <span>
                                        Financial Aid
                                    </span>
                                </a>
                            </li>
                        </ul>  
                    </div>
                </div>
                <div class="col-md-8 bg-blue">
                    <article class="feature-image-block">
                        <div class="content txt-color-white">
                            <h4 class="txt-2-6em txt-light margin-b-40">
                                <i class="fa fa-telegram"></i>
                                <span>Join Our Telegram Community</span>
                            </h4>
                            <h3 class="txt-lg txt-height-1-5 margin-b-40">
                                The UCLA Friends of English is a vibrant and energetic community whose members join together to support UCLA’s Department of English and to participate in the unique events afforded by membership.
                            </h3>
                            <a class="btn btn-trans-wb" href="">
                                Join Us
                            </a>
                        </div>   
                    </article>
                </div>
            </div>
        </section>
    </main>
        
    <?php get_footer() ?>