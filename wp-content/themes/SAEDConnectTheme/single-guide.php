<?php while ( have_posts() ) : the_post(); ?>

                
        <!-- Second Loop: Find First Article related to Multi Page-->
        <?php 

            $nav = new WP_Query( array(
                'relationship' => array(
                    'id'   => 'three_c_m_page_to_m_page_article',
                    'from' => get_the_ID(), // You can pass object ID or full object
                ),
                'posts_per_page' => 1,
            ) );
            while ( $nav->have_posts() ) : $nav->the_post(); ?>   

                <?php
                    if ( wp_redirect( get_permalink()) ) {
                        exit;
                    }
                ?>

                <li>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </li>

        <?php
            endwhile;
            wp_reset_postdata();
        ?>
        <!-- Second Loop: END -->

<?php endwhile; // end of the loop. ?>