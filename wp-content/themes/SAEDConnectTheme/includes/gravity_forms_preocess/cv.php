<?php
/*
*
*==========================================
* PROCESS CV FORM
*============================================
*
*/
//  School form
add_action( 'gform_after_submission_4', 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {
    //Save Entry ID
    //getting post
    $post = get_post( $entry['post_id'] );
 
    //changing post content
    $post->post_content = $entry['id'];
 
    //updating post
    wp_update_post( $post );
    
    /*
    *================================================
    *   MB Relationships
    *================================================
    */
    $parentId = rgar( $entry, '2' );
    $post_id = $entry['post_id'];
    $relationshipType = rgar( $entry, '3' );
    
    /* Get Current Multi-site ID */
    $blog_id = get_current_blog_id();
    
    /* MB Relationship Table */
    $mb_relationship_table = 'wp_mb_relationships';
    
    /* Relation Table to insert into on a multisite */
    if($blog_id != 1){
        $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
    }
    
    /* Get Clobal Relationship Table Object */
    global $wpdb;
    
    /* Insert into relationship table */
    $wpdb->insert( 
	$mb_relationship_table, 
        array( 
            'from' => $parentId, 
            'to' => $post_id,
            'type' => $relationshipType,
        ), 
        array( 
            '%d',
            '%d',
            '%s'
        ) 
    );
    
}



























//  CV to Personal Information Form
add_action( 'gform_after_submission_3', 'submit_cv_form', 10, 2 );
function submit_cv_form( $entry, $form ) {
    //Save Entry ID
    //getting post
    $post = get_post( $entry['post_id'] );
 
    //changing post content
    $post->post_content = $entry['id'];
 
    //updating post
    wp_update_post( $post );
    /*
    *
    *================================================================================================
    *
    *   AUTO CREATE ENTRY & CHILD POST
    *
    *================================================================================================
    *
    */
    
    // Create child forms parameters array
    $forms = array(
        // Personal Info Entry
        array(
            'child-form-id' => 6,
            'relationship-type' => 'cv_to_personal_info',
            'form-type' => 'personal-info',
            'child-post-type' => 'cv-personal-info'
        ),
        
        // Summary Entry
        array(
            'child-form-id' => 7,
            'relationship-type' => 'cv_to_summary',
            'form-type' => 'summary',
            'child-post-type' => 'cv-summary'
        ),
        
        
        
        
        // Contact Details
        array(
            'child-form-id' => 8,
            'relationship-type' => 'cv_to_contact',
            'form-type' => 'contact-details',
            'child-post-type' => 'cv-contact'
        ),
        
        // Experience
        array(
            'child-form-id' => 9,
            'relationship-type' => 'cv_to_experience',
            'form-type' => 'experience',
            'child-post-type' => 'cv-experience'
        ),
        
        // Personal Projects
        array(
            'child-form-id' => 10,
            'relationship-type' => 'cv_to_personal_project',
            'form-type' => 'personal-projects',
            'child-post-type' => 'cv-personal-project'
        ),
        
        // Education & Training
        array(
            'child-form-id' => 11,
            'relationship-type' => 'cv_to_education',
            'form-type' => 'education',
            'child-post-type' => 'cv-education'
        ),
        
        // Awards
        array(
            'child-form-id' => 12,
            'relationship-type' => 'cv_to_award',
            'form-type' => 'awards',
            'child-post-type' => 'cv-award'
        ),
        
        // Accomplishments
        array(
            'child-form-id' => 13,
            'relationship-type' => 'cv_to_accomplishment',
            'form-type' => 'accomplishments',
            'child-post-type' => 'cv-accomplishment'
        ),
        
        // Skills
        array(
            'child-form-id' => 14,
            'relationship-type' => 'cv_to_skill',
            'form-type' => 'skills',
            'child-post-type' => 'cv-skill'
        ),
        
        // Certifications
        array(
            'child-form-id' => 15,
            'relationship-type' => 'cv_to_certification',
            'form-type' => 'certifications',
            'child-post-type' => 'cv-certification'
        ),
        
        // Affiliations
        array(
            'child-form-id' => 16,
            'relationship-type' => 'cv_to_affiliation',
            'form-type' => 'affiliations',
            'child-post-type' => 'cv-affiliation'
        ),
        
        // Patents
        array(
            'child-form-id' => 17,
            'relationship-type' => 'cv_to_patent',
            'form-type' => 'patents',
            'child-post-type' => 'cv-patent'
        ),
        
        // Publications
        array(
            'child-form-id' => 18,
            'relationship-type' => 'cv_to_publication',
            'form-type' => 'publications',
            'child-post-type' => 'cv-publication'
        ),
        
        // Interests
        array(
            'child-form-id' => 19,
            'relationship-type' => 'cv_to_interest',
            'form-type' => 'interests',
            'child-post-type' => 'cv-interest'
        ),
        
        // Languages
        array(
            'child-form-id' => 20,
            'relationship-type' => 'cv_to_language',
            'form-type' => 'languages',
            'child-post-type' => 'cv-language'
        ),
        
        // References
        array(
            'child-form-id' => 21,
            'relationship-type' => 'cv_to_reference',
            'form-type' => 'references',
            'child-post-type' => 'cv-reference'
        ),
    );
    

    foreach($forms as $form){
        /*
        *
        * Variables
        *
        */
        // Child Form
        //$form_id = 6; // Personal In0fomation
        $form_id = $form['child-form-id'];

        $post = get_post( $entry['post_id'] ); // Get CV Post    

        // CV Entry
        $cv_entry_id = $entry['id'];

        // CV Post
        $cv_post_id = $post->ID;
        $cv_post_name = $post->post_title;
        $relationshipType = $form['relationship-type'];

        // Prepare Child Entry Name
        $child_entry_title = $entry['id']."-".rgar( $entry, '1' )."-".$form['form-type'];

        // Create Child Form Entry
        $child_entry = array(
            "form_id" => $form_id,
            "1" => $child_entry_title,
            "2" => $cv_entry_id
        );

        $child_entry_id = GFAPI::add_entry($child_entry);


        // Create Child Entry Post
        $postName = $cv_post_id."-".$cv_post_name."-".$form['form-type'];
        $childPostType = $form['child-post-type'];

        //$relationshipType = $_POST['relationshipType'];

        $post_id = wp_insert_post(array (
            'post_type' => $childPostType,
            'post_title' => $postName,
            'post_content' => $child_entry_id,
            'post_status' => 'publish',
        ));

        /*
        *================================================
        *   MB Relationships
        *================================================
        */
        $parentId = $cv_post_id;

        /* Get Current Multi-site ID */
        $blog_id = get_current_blog_id();

        /* MB Relationship Table */
        $mb_relationship_table = 'wp_mb_relationships';

        /* Relation Table to insert into on a multisite */
        if($blog_id != 1){
            $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
        }

        /* Get Clobal Relationship Table Object */
        global $wpdb;

        /* Insert into relationship table */
        $wpdb->insert( 
        $mb_relationship_table, 
            array( 
                'from' => $parentId, 
                'to' => $post_id,
                'type' => $relationshipType,
            ), 
            array( 
                '%d',
                '%d',
                '%s'
            ) 
        );
    }
    
}






//  Personal Project form
add_action( 'gform_after_submission_10', 'submit_cv_personal_project', 10, 2 );
function submit_cv_personal_project( $entry, $form ) {
    //Save Entry ID
    //getting post
    $post = get_post( $entry['post_id'] );
 
    //changing post content
    $post->post_content = $entry['id'];
 
    //updating post
    wp_update_post( $post );
    
    /*
    *================================================
    *   MB Relationships
    *================================================
    */
    
    $parent_entry_id = GFAPI::get_entry( rgar( $entry, '2' ) ); // Get Parent Entry from ID
    
    $parentId = $parent_entry_id['post_id'];
    $post_id = $entry['post_id'];
    $relationshipType = 'cv_to_personal_project';
    
    /* Get Current Multi-site ID */
    $blog_id = get_current_blog_id();
    
    /* MB Relationship Table */
    $mb_relationship_table = 'wp_mb_relationships';
    
    /* Relation Table to insert into on a multisite */
    if($blog_id != 1){
        $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
    }
    
    /* Get Clobal Relationship Table Object */
    global $wpdb;
    
    /* Insert into relationship table */
    $wpdb->insert( 
	$mb_relationship_table, 
        array( 
            'from' => $parentId, 
            'to' => $post_id,
            'type' => $relationshipType,
        ), 
        array( 
            '%d',
            '%d',
            '%s'
        ) 
    );
    
}