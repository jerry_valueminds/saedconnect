<?php
/*
*
*==========================================
* PROCESS MENTOR / SERVICE PROVIDER FORM
*============================================
*
*/

add_action( 'gform_after_submission_79', 'submit_mentor_form', 10, 2 );
function submit_mentor_form( $entry, $form ) {
    //Save Entry ID
    //getting post
    $post = get_post( $entry['post_id'] );
 
    //changing post content
    $post->post_content = $entry['id'];
 
    //updating post
    wp_update_post( $post );
    /*
    *
    *================================================================================================
    *
    *   AUTO CREATE ENTRY & CHILD POST
    *
    *================================================================================================
    *
    */
    
    // Create child forms parameters array
    $forms = array(
        // Basic Info Entry
        array(
            'child-form-id' => 80,
            'relationship-type' => 'mentor_to_basic_info',
            'form-type' => 'basic-info',
            'child-post-type' => 'mentor-basic-info'
        ),
        
        // Trainer Profile Entry
        array(
            'child-form-id' => 81,
            'relationship-type' => 'mentor_to_trainer_profile',
            'form-type' => 'trainer-profile',
            'child-post-type' => 'mentor-trainer-prof'
        ),
        
        // Youth Support Service Entry
        array(
            'child-form-id' => 82,
            'relationship-type' => 'mentor_to_youth_service',
            'form-type' => 'youth-support-service',
            'child-post-type' => 'mentor-youth-service'
        ),
    );
    

    foreach($forms as $form){
        /*
        *
        * Variables
        *
        */
        // Child Form
        //$form_id = 6; // Personal In0fomation
        $form_id = $form['child-form-id'];

        $post = get_post( $entry['post_id'] ); // Get CV Post    

        // CV Entry
        $cv_entry_id = $entry['id'];

        // CV Post
        $cv_post_id = $post->ID;
        $cv_post_name = $post->post_title;
        $relationshipType = $form['relationship-type'];

        // Prepare Child Entry Name
        $child_entry_title = $entry['id']."-".rgar( $entry, '1' )."-".$form['form-type'];

        // Create Child Form Entry
        $child_entry = array(
            "form_id" => $form_id,
            "1" => $child_entry_title,
            "2" => $cv_entry_id
        );

        $child_entry_id = GFAPI::add_entry($child_entry);


        // Create Child Entry Post
        $postName = $cv_post_id."-".$cv_post_name."-".$form['form-type'];
        $childPostType = $form['child-post-type'];

        //$relationshipType = $_POST['relationshipType'];

        $post_id = wp_insert_post(array (
            'post_type' => $childPostType,
            'post_title' => $postName,
            'post_content' => $child_entry_id,
            'post_status' => 'publish',
        ));

        /*
        *================================================
        *   MB Relationships
        *================================================
        */
        $parentId = $cv_post_id;

        /* Get Current Multi-site ID */
        $blog_id = get_current_blog_id();

        /* MB Relationship Table */
        $mb_relationship_table = 'wp_mb_relationships';

        /* Relation Table to insert into on a multisite */
        if($blog_id != 1){
            $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
        }

        /* Get Clobal Relationship Table Object */
        global $wpdb;

        /* Insert into relationship table */
        $wpdb->insert( 
        $mb_relationship_table, 
            array( 
                'from' => $parentId, 
                'to' => $post_id,
                'type' => $relationshipType,
            ), 
            array( 
                '%d',
                '%d',
                '%s'
            ) 
        );
    }
    
}






//  Basic Information form
add_action( 'gform_after_submission_80', 'submit_mentor_basic_info', 10, 2 );
function submit_mentor_basic_info( $entry, $form ) {
    //Save Entry ID
    //getting post
    $post = get_post( $entry['post_id'] );
 
    //changing post content
    $post->post_content = $entry['id'];
 
    //updating post
    wp_update_post( $post );
    
    /*
    *================================================
    *   MB Relationships
    *================================================
    */
    
    $parent_entry_id = GFAPI::get_entry( rgar( $entry, '2' ) ); // Get Parent Entry from ID
    
    $parentId = $parent_entry_id['post_id'];
    $post_id = $entry['post_id'];
    $relationshipType = 'mentor_to_basic_info';
    
    /* Get Current Multi-site ID */
    $blog_id = get_current_blog_id();
    
    /* MB Relationship Table */
    $mb_relationship_table = 'wp_mb_relationships';
    
    /* Relation Table to insert into on a multisite */
    if($blog_id != 1){
        $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
    }
    
    /* Get Clobal Relationship Table Object */
    global $wpdb;
    
    /* Insert into relationship table */
    $wpdb->insert( 
	$mb_relationship_table, 
        array( 
            'from' => $parentId, 
            'to' => $post_id,
            'type' => $relationshipType,
        ), 
        array( 
            '%d',
            '%d',
            '%s'
        ) 
    );
    
}

