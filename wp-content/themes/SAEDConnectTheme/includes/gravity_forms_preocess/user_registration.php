<?php
/*
*
*==========================================
* PROCESS CV FORM
*============================================
*
*/
//  School form
add_action( 'gform_after_submission_4', 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {
    //Save Entry ID
    //getting post
    $post = get_post( $entry['post_id'] );
 
    //changing post content
    $post->post_content = $entry['id'];
 
    //updating post
    wp_update_post( $post );
    
    /*
    *================================================
    *   MB Relationships
    *================================================
    */
    $parentId = rgar( $entry, '2' );
    $post_id = $entry['post_id'];
    $relationshipType = rgar( $entry, '3' );
    
    /* Get Current Multi-site ID */
    $blog_id = get_current_blog_id();
    
    /* MB Relationship Table */
    $mb_relationship_table = 'wp_mb_relationships';
    
    /* Relation Table to insert into on a multisite */
    if($blog_id == 1){
        $mb_relationship_table = 'wp_'.$blog_id.'_mb_relationships';
    }
    
}
