<?php
/*
*
*==========================================
* cHANGE LOGIN FORM LABEL
*============================================
*
*/
add_filter( 'gform_userregistration_login_form', 'change_form', 10, 1 );

function change_form( $form ) {
    $fields = $form['fields'];
    foreach ( $fields as &$field ) {
        if ( $field->label == 'Username' ) {
            $field->label = 'Email / Username';
        }
    }
    return $form;
}


/*
*
*==========================================
* PROCESS GRAVITY FORMs SuBMISSIONS
*============================================
*
*/

//  CV
include 'gravity_forms_preocess/cv.php';

//  Mentor
include 'gravity_forms_preocess/mentor_service_provider.php';

//  Program
include 'gravity_forms_preocess/user_program.php';

//  Course
include 'gravity_forms_preocess/user_course.php';