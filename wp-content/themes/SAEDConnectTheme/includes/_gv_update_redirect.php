<?php

    add_action( 'gravityview/edit_entry/after_update', 'gravityview_redirect_after_update', 10, 2 );
        
function gravityview_redirect_after_update( $form, $entry_id ) {
    // Get the current View ID
    $view_id  = GravityView_View::getInstance()->getViewId();
    
    // Get the link to the View
    $permalink_esc = esc_url( get_permalink( $view_id ) . '?updated=true' );
    
    if( $_REQUEST['view'] == 'cv' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv' . '&updated=true' );
        
            /*$entry = GFAPI::get_entry( $entry_id );
            print_r($entry);*/
        
    }elseif ( $_REQUEST['view'] == 'basic-information' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/my-profile/' . '?updated=true' );
    }elseif ( $_REQUEST['view'] == 'marketplace' ){
        $permalink_esc = esc_url( get_permalink( $_GET['post_id'] ) . '?updated=true' );
    }elseif ( $_REQUEST['view'] == 'trainer-profile' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/service-provider-directory/trainer-profile/?updated=true' );
    }elseif( $_REQUEST['view'] == 'education' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/competency-profile/education-experience-profile/?updated=true' );
    }elseif( $_REQUEST['view'] == 'skills-profile' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/competency-profile/my-skills/?updated=true' );
    }elseif( $_REQUEST['view'] == 'edit-profile' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/competency-profile/my-dashboard/?action=cv&cv-view=personal-information/?updated=true' );
    }elseif( $_REQUEST['view'] == 'service-offer' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/service-marketplace/my-service-offers' );
    }elseif( $_REQUEST['view'] == 'task' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/service-marketplace/my-tasks' );
    }elseif( $_REQUEST['view'] == 'job' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/service-marketplace/my-jobs' );
    }elseif( $_REQUEST['view'] == 'training-offer' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/service-provider-directory/my-training-offers/' );
    }elseif( $_REQUEST['view'] == 'training-request' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/service-provider-directory/my-training-requests/' );
    }elseif( $_REQUEST['view'] == 'business' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/ventures-directory/my-businesses/' );
    }elseif( $_REQUEST['view'] == 'project' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/ventures-directory/my-projects/' );
    }elseif( $_REQUEST['view'] == 'opportunity' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/opportunity-center/my-programs/' );
    }elseif( $_REQUEST['view'] == 'nysc-saed' ){
        $permalink_esc = esc_url( 'https://www.saedconnect.org/nysc-saed-profile/' );
    }
?>
   
    <script>
        window.location.replace( "<?php echo $permalink_esc; ?>" );
    </script>
    
<?php } ?>


<?php
/**
 * Change the update entry success message, including the link
 * 
 * @param $message string The message itself
 * @param $view_id int View ID
 * @param $entry array The Gravity Forms entry object
 * @param $back_link string Url to return to the original entry
 */
function gv_my_update_message( $message, $view_id, $entry, $back_link ) {
    $link = str_replace( 'entry/'.$entry['id'].'/', '', $back_link );
    return 'Entry Updated. <a href="'.esc_url($link).'">Return to the list</a>';
}
add_filter( 'gravityview/edit_entry/success', 'gv_my_update_message', 10, 4 );