<?php


function child_post_meta_box()
{
    add_meta_box( 'post-child-meta-box', 'Edit Children', 'child_post_meta_box_cb', 'guide-topic', 'side', 'default' );
}

function child_post_meta_box_cb()
{
    $thisParentPostId = get_the_ID();
    
    ?>
    <ul class="child-list">
    <?php
    
    $nav = new WP_Query( array(
        'relationship' => array(
            'id'   => 'og_topic_to_multipage',
            'from' => get_the_ID(), // You can pass object ID or full object
        ),
        'nopaging' => true,
    ) );
    while ( $nav->have_posts() ) : $nav->the_post();
        ?>   
        
            <li>
                <div id="delete-action">
                    <a id="post-status-display" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>    
                </div>
                <div id="publishing-action">
                    <span class="spinner"></span>
                    <a href="<?php echo get_edit_post_link( $id, $context ); ?>" class="button button-primary">
                        Edit Post
                    </a>
                    <a href="<?php echo get_delete_post_link( $id, $context ); ?>" class="button button-primary">
                        Delete Post
                    </a>
                </div>
                <div class="clear"></div>
            </li>
            <br>
        
    
    <?php
    endwhile;
    ?>
    </ul>
    <br>
    <p class="" style="font-weight:600;border-bottom: 1px solid gainsboro; padding-bottom: 10px;">Add New Child</p>
        <form method="post" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
            <label for="meta-box-text">Post Name</label>
            <input name="post-name" type="text" value="" id="post-name">
<!--
            <input name="post-slug" type="text" value="" id="post-slug">
-->
            <input name="relationship_type" type="text" value="og_topic_to_multipage" hidden id="relationship-type">
            <input name="parent_id" type="text" value="<?php echo $thisParentPostId ?>" hidden id="parent-id">
            <input name="child_post_type" type="text" value="guide-article-group" hidden id="child-post-type">
            
            <a href="" class="button button-primary ajax-submit">
                Ajax Submit
            </a>
        </form>
    <?php
}

add_action( 'add_meta_boxes', 'child_post_meta_box' );




/*function parent_post_meta_box()
{
    add_meta_box( 'post-parent-meta-box', 'Edit Parent', 'parent_post_meta_box_cb', 'multiitem', 'normal', 'default' );
}
function parent_post_meta_box_cb()
{
    $parent = new WP_Query( array(
        'relationship' => array(
            'id'   => 'mini_sites_to_multi_items',
            'to' => get_the_ID(), // You can pass object ID or full object
        ),
        'nopaging' => true,
    ) );
    while ( $parent->have_posts() ) : $parent->the_post();
        ?>   
        <ul>
            <li>
                <div id="delete-action">
                    <a id="post-status-display" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>    
                </div>
                <div id="publishing-action">
                    <span class="spinner"></span>
                    <a href="<?php echo get_edit_post_link( $id, $context ); ?>" class="button button-primary">
                        Edit Post
                    </a>
                </div>
                <div class="clear"></div>
            </li>
        </ul>
    <?php
    endwhile;
}

add_action( 'add_meta_boxes', 'parent_post_meta_box' );*/

/*
*
*==========================================
* PROCESS FORM
*============================================
*
*/
add_action('wp_ajax_contact_form', 'contact_form');
add_action('wp_ajax_nopriv_contact_form', 'contact_form');

function contact_form()
{
    $postName = $_POST['postName'];
    $postSlug = $_POST['postSlug'];
    $childPostType = $_POST['childPostType'];
    $parentId = $_POST['parentId'];
    $relationshipType = $_POST['relationshipType'];
    
    $post_id = wp_insert_post(array (
        'post_type' => $childPostType,
        'post_title' => $postName,
        'post_content' => "",
        'post_status' => 'publish',
    ));
    
    /* Relationship Table */
    global $wpdb;
    
    $wpdb->insert( 
	'wp_mb_relationships', 
        array( 
            'from' => $parentId, 
            'to' => $post_id,
            'type' => 'og_topic_to_multipage',
        ), 
        array( 
            '%d',
            '%d',
            '%s'
        ) 
    );
    
    echo 
    
        '<li>'.
            '<div id="delete-action">'.
                '<a id="post-status-display" href="'.get_permalink($post_id).'">'.
                    get_the_title($post_id).
                '</a>'.  
            '</div>'.
            '<div id="publishing-action">'.
                '<span class="spinner"></span>'.
                '<a href="'.get_edit_post_link( $post_id, $context ).'" class="button button-primary">'.
                    'Edit Post'.
                '</a>'.
                '<a href="'.get_delete_post_link( $post_id, $context ).'" class="button button-primary">'.
                    'Delete Post'.
                '</a>'.
            '</div>'.
            '<div class="clear"></div>'.
        '</li>';
    
    wp_die();
}


