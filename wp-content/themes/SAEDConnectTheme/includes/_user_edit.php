<?php
/*
===========================================================================================
=   Business Clinic
===========================================================================================
*/  
/**
 * Add Business Clinic Additional Checkbox Profile Fields.
 *
 * @param WP_User $user User object.
 */
function additional_biz_clinic_user_profile( $user ) {    
    
    $choices = get_user_meta( $user->ID, 'moderator_select_biz_clinic', true );

?>
    <h3>
        Assign Business Clinic Communities
    </h3>

    <table class="form-table">
        <tr>
            <th>
                <label for="birth-date-day">
                    Select Forum(s)
                </label>
            </th>
   	        <td>
   	        <?php // Display posts
                $program_query = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'program_to_track',
                        //'from' => 1041, // You can pass object ID or full object
                        'from' => 650, // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $program_query->have_posts() ) : $program_query->the_post();

            ?>
                
                <input
                    type="checkbox"
                    name="select-biz-clinic[]"
                    value="<?php echo get_the_ID() ?>"
                    <?php
                        if($choices){
                            if (in_array(get_the_ID(), $choices)) {
                                echo "checked";
                            }
                        }
                    ?>
                >
                <label style="padding-right:10px;">
                    <?php the_title(); ?>
                </label><br>
            
            <?php
                endwhile;
            ?>
   		 </td>
   	 </tr>
    </table>
    <?php
}

add_action( 'show_user_profile', 'additional_biz_clinic_user_profile' );
add_action( 'edit_user_profile', 'additional_biz_clinic_user_profile' );

/**
 * Save Business Clinic Additional Checkbox Profile Fields.
 *
 * @param  int $user_id Current user ID.
 */
function save_biz_clinic_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	    return false;
    }

    if ( empty( $_POST['select-shc'] ) ) {
   	    return false;
    }
    
    $serialized = $_POST['select-biz-clinic'];

    update_usermeta( $user_id, 'moderator_select_biz_clinic', $serialized );
}

add_action( 'personal_options_update', 'save_biz_clinic_profile_fields' );
add_action( 'edit_user_profile_update', 'save_biz_clinic_profile_fields' );



/*
===========================================================================================
=   Entrepreneurship Incubator
===========================================================================================
*/ 
/**
 * Add Entrepreneurship Additional Checkbox Profile Fields.
 *
 * @param WP_User $user User object.
 */
function additional_entrepreneurship_user_profile( $user ) {    
    
    $choices = get_user_meta( $user->ID, 'moderator_select_entrepreneurship', true );

?>
    <h3>
        Assign Entrepreneurship Communities
    </h3>

    <table class="form-table">
        <tr>
            <th>
                <label for="birth-date-day">
                    Select Forum(s)
                </label>
            </th>
   	        <td>
   	        <?php // Display posts
                $program_query = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'program_to_track',
                        //'from' => 1041, // You can pass object ID or full object
                        'from' => 632, // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $program_query->have_posts() ) : $program_query->the_post();

            ?>
                
                <input
                    type="checkbox"
                    name="select-entrepreneurship[]"
                    value="<?php echo get_the_ID() ?>"
                    <?php
                        if($choices){
                            if (in_array(get_the_ID(), $choices)) {
                                echo "checked";
                            }
                        }
                    ?>
                >
                <label style="padding-right:10px;">
                    <?php the_title(); ?>
                </label><br>
            
            <?php
                endwhile;
            ?>
   		 </td>
   	 </tr>
    </table>
    <?php
}

add_action( 'show_user_profile', 'additional_entrepreneurship_user_profile' );
add_action( 'edit_user_profile', 'additional_entrepreneurship_user_profile' );

/**
 * Save Entrepreneurship Additional Checkbox Profile Fields.
 *
 * @param  int $user_id Current user ID.
 */
function save_entrepreneurship_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	    return false;
    }

    if ( empty( $_POST['select-shc'] ) ) {
   	    return false;
    }
    
    $serialized = $_POST['select-entrepreneurship'];

    update_usermeta( $user_id, 'moderator_select_entrepreneurship', $serialized );
}

add_action( 'personal_options_update', 'save_entrepreneurship_profile_fields' );
add_action( 'edit_user_profile_update', 'save_entrepreneurship_profile_fields' );



/*
===========================================================================================
=   Job Advisor
===========================================================================================
*/ 
/**
 * Add Job Advisor Additional Checkbox Profile Fields.
 *
 * @param WP_User $user User object.
 */
function additional_job_advisor_user_profile( $user ) {    
    
    $choices = get_user_meta( $user->ID, 'moderator_select_job_advisor', true );

?>
    <h3>
        Assign Job Advisor Communities
    </h3>

    <table class="form-table">
        <tr>
            <th>
                <label for="birth-date-day">
                    Select Forum(s)
                </label>
            </th>
   	        <td>
   	        <?php // Display posts
                $program_query = new WP_Query( array(
                    'relationship' => array(
                        'id'   => 'program_to_track',
                        //'from' => 1041, // You can pass object ID or full object
                        'from' => 649, // You can pass object ID or full object
                    ),
                    'nopaging' => true,
                ) );
                while ( $program_query->have_posts() ) : $program_query->the_post();

            ?>
                
                <input
                    type="checkbox"
                    name="select-job_advisor[]"
                    value="<?php echo get_the_ID() ?>"
                    <?php
                        if($choices){
                            if (in_array(get_the_ID(), $choices)) {
                                echo "checked";
                            }
                        }
                    ?>
                >
                <label style="padding-right:10px;">
                    <?php the_title(); ?>
                </label><br>
            
            <?php
                endwhile;
            ?>
   		 </td>
   	 </tr>
    </table>
    <?php
}

add_action( 'show_user_profile', 'additional_job_advisor_user_profile' );
add_action( 'edit_user_profile', 'additional_job_advisor_user_profile' );

/**
 * Save Job Advisor Additional Checkbox Profile Fields.
 *
 * @param  int $user_id Current user ID.
 */
function save_job_advisor_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	    return false;
    }

    if ( empty( $_POST['select-shc'] ) ) {
   	    return false;
    }
    
    $serialized = $_POST['select-job_advisor'];

    update_usermeta( $user_id, 'moderator_select_job_advisor', $serialized );
}

add_action( 'personal_options_update', 'save_job_advisor_profile_fields' );
add_action( 'edit_user_profile_update', 'save_job_advisor_profile_fields' );




/*
===========================================================================================
=   Side Hustle Communities
===========================================================================================
*/ 
/**
 * Add SHC Additional Checkbox Profile Fields.
 *
 * @param WP_User $user User object.
 */
function additional_shc_user_profile( $user ) {    
    /**/
    $choices = get_user_meta( $user->ID, 'moderator_select_shc', true );

?>
    <h3>
        Assign Side Hustle Communities
    </h3>

    <table class="form-table">
        <tr>
            <th>
                <label for="birth-date-day">
                    Select Forum(s)
                </label>
            </th>
   	        <td>
   	        <?php // Display posts
                $wp_query = new WP_Query();
                $wp_query->query(array('post_type' => 'information-session'));
                    while ($wp_query->have_posts()) : $wp_query->the_post();
            ?>
                
                <input
                    type="checkbox"
                    name="select-shc[]"
                    value="<?php echo get_the_ID() ?>"
                    <?php
                        if($choices){
                            if (in_array(get_the_ID(), $choices)) {
                                echo "checked";
                            }
                        }
                    ?>
                >
                <label style="padding-right:10px;">
                    <?php the_title(); ?>
                </label><br>
            
            <?php
                endwhile;
            ?>
   		 </td>
   	 </tr>
    </table>
    <?php
}

add_action( 'show_user_profile', 'additional_shc_user_profile' );
add_action( 'edit_user_profile', 'additional_shc_user_profile' );

/**
 * Save SHC Additional Checkbox Profile Fields.
 *
 * @param  int $user_id Current user ID.
 */
function save_shc_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	    return false;
    }

    if ( empty( $_POST['select-shc'] ) ) {
   	    return false;
    }
    
    $serialized = $_POST['select-shc'];

    update_usermeta( $user_id, 'moderator_select_shc', $serialized );
}

add_action( 'personal_options_update', 'save_shc_profile_fields' );
add_action( 'edit_user_profile_update', 'save_shc_profile_fields' );