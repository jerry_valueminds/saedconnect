<?php

add_action( 'customize_register', 'sc_customizer_settings' );
function sc_customizer_settings( $wp_customize ) {
    
    $wp_customize->add_section( 'cd_colors' , array(
        'title'      => 'Forum Colors',
        'priority'   => 30,
    ) );
    
    $wp_customize->add_setting( 'background_color' , array(
        'default'     => '#43C6E4',
        'transport'   => 'refresh',
    ) );
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color', array(
        'label'        => 'Background Color',
        'section'    => 'cd_colors',
        'settings'   => 'background_color',
    ) ) );
}