<?php
add_filter( 'mb_settings_pages', 'prefix_settings_pages' );
function prefix_settings_pages( $settings_pages ) {
    $settings_pages[] = array(
        'id'            => 'my-options-general',
        'menu_title'    => 'Theme Settings',
        'option_name'   => 'my_options',
        'icon_url'      => 'dashicons-images-alt',
        'submenu_title' => 'General Settings', // Note this
        
        'tabs'        => array(
            'general' => 'General Settings',
            'design'  => 'Design Customization',
            'faq'     => 'FAQ & Help',
        ),
    );
    $settings_pages[] = array(
        'id'          => 'my-options-forum',
        'option_name' => 'my_options',
        'menu_title'  => 'Forum Settings',
        'parent'      => 'my-options-general', // Note this
    );
    $settings_pages[] = array(
        'id'          => 'my-options-others',
        'option_name' => 'my_options',
        'menu_title'  => 'Other Settings',
        'parent'      => 'my-options-general',
    );
    return $settings_pages;
}


/*
*
*   Meta Boxes
*
*/
// Register settings page. In this case, it's a theme options page
/*add_filter( 'mb_settings_pages', 'prefix_options_page' );
function prefix_options_page( $settings_pages ) {
    $settings_pages[] = array(
        'id'          => 'pencil',
        'option_name' => 'pencil',
        'menu_title'  => 'Pencil',
        'icon_url'    => 'dashicons-edit',
        'style'       => 'no-boxes',
        'columns'     => 1,
        'tabs'        => array(
            'general' => 'General Settings',
            'design'  => 'Design Customization',
            'faq'     => 'FAQ & Help',
        ),
        'position'    => 68,
    );
    return $settings_pages;
}*/

// Register meta boxes and fields for settings page
add_filter( 'rwmb_meta_boxes', 'prefix_options_meta_boxes' );
function prefix_options_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'             => 'general',
        'title'          => 'General',
        'settings_pages' => 'my-options-forum',
        //'tab'            => 'general',

        'fields' => array(
            array(
				'id' => 'forum_category',
				'name' => esc_html__( 'Forum Category', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'_shc_modal' => 'Side Hustle Communities',
					'_tei_modal' => 'The Entrepreneurship Incubator',
					'_business_clinic_modal' => 'Business Clinic',
					'_job_advisor_modal' => 'Job Advisor',
					'_help_center_modal' => 'Help Center'
				),
				'std' => 'text-content',
			),
            
            array(
				'id' => 'forum_subcategory',
				'name' => esc_html__( 'Free or Paid?', 'metabox-online-generator' ),
				'type' => 'select',
				'options' => array(
					'free' => 'Free Forum',
					'mentor' => 'Paid Forum',
				),
				'std' => 'text-content',
			),
            
            array(
                'name' => 'Forum theme Color',
                'id'   => 'forum-theme-color',
                'type' => 'color',
                'js_options'    => array(
                    'palettes' => array( '#f4c026', '#d26819', '#78b', '#3a4002', '#b55085' )
                ),
            ),
            
            array(
                'name' => 'Forum Alt theme Color',
                'id'   => 'forum-alt-theme-color',
                'type' => 'color',
                'js_options'    => array(
                    'palettes' => array( '#f4c026', '#d26819', '#78b', '#3a4002', '#b55085' )
                ),
            ),
            
            array(
                'name' => 'Action Button Bacground',
                'id'   => 'action-btn-background',
                'type' => 'color',
                'js_options'    => array(
                    'palettes' => array( '#f4c026', '#d26819', '#78b', '#3a4002', '#b55085' )
                ),
            ),
            
            array(
                'name' => 'Action Button Color',
                'id'   => 'action-btn-color',
                'type' => 'color',
                'js_options'    => array(
                    'palettes' => array( '#f4c026', '#d26819', '#78b', '#3a4002', '#b55085' )
                ),
            ),
            
            
        ),
    );
    $meta_boxes[] = array(
        'id'             => 'colors',
        'title'          => 'Colors',
        'settings_pages' => 'pencil',
        'tab'            => 'design',

        'fields' => array(
            array(
                'name' => 'Heading Color',
                'id'   => 'heading-color',
                'type' => 'color',
            ),
            array(
                'name' => 'Text Color',
                'id'   => 'text-color',
                'type' => 'color',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'             => 'info',
        'title'          => 'Theme Info',
        'settings_pages' => 'pencil',
        'tab'            => 'faq',
        'fields'         => array(
            array(
                'type' => 'custom_html',
                'std'  => 'Having questions? Check out our documentation',
            ),
        ),
    );
    return $meta_boxes;
}
