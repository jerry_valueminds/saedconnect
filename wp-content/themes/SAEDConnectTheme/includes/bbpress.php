<?php

// BBPress Activate show lead topic
function custom_bbp_show_lead_topic( $show_lead ) {
    
    $show_lead[] = 'true';
    
    return $show_lead;
}
 
add_filter('bbp_show_lead_topic', 'custom_bbp_show_lead_topic' );

// Remove | from Reply Admin Linls
function custom_bbp_admin_links ($args) {
    
	$args['before'] = '<div class="padding-tb-5">' ;
	$args['after'] = '</div>' ;
    $args['sep'] = '';
    
    return $args ;
}

add_filter ('bbp_before_get_reply_admin_links_parse_args', 'custom_bbp_admin_links' ) ;

// Reply-To link text
function custom_bbp_change_reply_text ($args) {
    
	$args['reply_text'] = 'Reply';
    $args['link_before'] = '<p class="reply-icon">';
    $args['link_after'] = '</p>';
    
    return $args ;
}

add_filter ('bbp_before_get_reply_to_link_parse_args', 'custom_bbp_change_reply_text' ) ;


// Reply - Edit link text
function custom_bbp_change_reply_edit_text ($args) {
    
	$args['edit_text'] = 'Edit';
    $args['link_before'] = '<p class="reply-icon edit">';
    $args['link_after'] = '</p>';
    
    return $args ;
}

add_filter ('bbp_before_get_reply_edit_link_parse_args', 'custom_bbp_change_reply_edit_text' ) ;

// Reply - Trash link text
function custom_bbp_change_reply_trash_text ($args) {
    
	$args['trash_text'] = 'Delete';
    $args['link_before'] = '<p class="reply-icon trash">';
    $args['link_after'] = '</p>';
    
    return $args ;
}

add_filter ('bbp_before_get_reply_trash_link_parse_args', 'custom_bbp_change_reply_trash_text' ) ;

// Reply - Spam link text
function custom_bbp_change_reply_spam_text ($args) {
    
	$args['spam_text'] = 'Report';
    $args['link_before'] = '<p class="reply-icon spam">';
    $args['link_after'] = '</p>';
    
    return $args ;
}

add_filter ('bbp_before_get_reply_spam_link_parse_args', 'custom_bbp_change_reply_spam_text' ) ;




// Disable Private Prefix in Private Forum Title
add_filter('protected_title_format', 'ntwb_remove_protected_title');
function ntwb_remove_protected_title($title) {
	return '%s';
}

// Disable Private Prefix in Private Forum Title
add_filter('private_title_format', 'ntwb_remove_private_title');
function ntwb_remove_private_title($title) {
	return '%s';
}

// Edit Breadcrumb
function mycustom_breadcrumb_options() {
	// Home - default = true
	$args['include_home']    = false;
	// Forum root - default = true
	$args['include_root']    = false;
	// Current - default = true
	$args['include_current'] = true;

	return $args;
}

add_filter('bbp_before_get_breadcrumb_parse_args', 'mycustom_breadcrumb_options' );

// Change Last reply date format
 
/*function bbpress_enable_date_translation( $result, $reply_id, $humanize, $gmt, $date, $time ) {
 
	$date = get_post_time( get_option( 'date_format' ), $gmt, $reply_id, $translate = true );
	$result = sprintf( _x( '%1$s at %2$s', 'date at time', 'bbpress' ), $date, $time );
	return $result;
}

add_filter ( 'bbp_get_reply_post_date', 'bbpress_enable_date_translation');
add_filter ( 'bbp_get_topic_post_date', 'bbpress_enable_date_translation');*/


// Enable TinyMCE Editor
function bbp_enable_visual_editor( $args = array() ) {
    $args['tinymce'] = true;
    $args['teeny'] = false;
    return $args;
}
add_filter( 'bbp_after_get_the_content_parse_args', 'bbp_enable_visual_editor' );

// Clean Pasted Text 
function bbp_tinymce_paste_plain_text( $plugins = array() ) {
    $plugins[] = 'paste';
    return $plugins;
}
add_filter( 'bbp_get_tiny_mce_plugins', 'bbp_tinymce_paste_plain_text' );

// TEMP: WP Better Email
function use_wpbe_template( $message, $content_type ) {
    global $wp_better_emails;
    if ( 'text/plain' == $content_type ) {
      $message = $wp_better_emails->process_email_text( $message );
    } else {
      $message = $wp_better_emails->process_email_html( $message );
    }
    return $message;
}

add_filter( 'sendgrid_override_template', 'use_wpbe_template', 10, 2 );