<?php

add_filter( 'gform_pre_render_21', 'populate_posts' );
add_filter( 'gform_pre_validation_21', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_21', 'populate_posts' );
add_filter( 'gform_admin_pre_render_21', 'populate_posts' );
function populate_posts( $form ) {
 
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'info-session-hidden' ) === false ) {
            continue;
        }
 
        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $posts = get_posts( 'post_type=information-session&numberposts=-1&post_status=publish' );
 
        $choices = array();
 
        foreach ( $posts as $post ) {
            $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
        }
 
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a Business Idea';
        $field->choices = $choices;
 
    }
 
    return $form;
}


/*
*
* Add Predefined Choices: States
*
*/
add_filter( 'gform_predefined_choices', 'add_predefined_choice_nigerian_states' );

function add_predefined_choice_nigerian_states( $choices ) {
   $choices['Nigerian States'] = array(
        'Abia',
        'Adamawa',
        'Akwa Ibom',
        'Anambra',
        'Bauchi',
        'Bayelsa',
        'Benue',
        'Borno',
        'Cross river',
        'Delta',
        'Ebonyi',
        'Edo',
        'Ekiti',
        'Enugu',
        'Gombe',
        'Imo',
        'Jigawa',
        'Kaduna',
        'Kano',
        'Katsina',
        'Kebbi',
        'Kogi',
        'Kwara',
        'Lagos',
        'Nassarawa',
        'Niger',
        'Ogun',
        'Ondo',
        'Osun',
        'Oyo',
        'Plateau',
        'Rivers',
        'Sokoto',
        'Taraba',
        'Yobe',
        'Zamfara',
        'Federal Capital Territory (FCT)'
   );
   return $choices;
}

/*function add_predefined_choice_nigerian_states( $choices ) {
   $choices['Nigerian States'] = array(
        'Abia|abia',
        'Adamawa|adamawa',
        'Akwa Ibom|akwa-ibom',
        'Anambra|anambara',
        'Bauchi|bauchi',
        'Bayelsa|bayelsa',
        'Benue|benue',
        'Borno|borno',
        'Cross river|cross-river',
        'Delta|delta',
        'Ebonyi|ebonyi',
        'Edo|edo',
        'Ekiti|ekiti',
        'Enugu|enugu',
        'Gombe|gombe',
        'Imo|imo',
        'Jigawa|jigawa',
        'Kaduna|kaduna',
        'Kano|kano',
        'Katsina|katsina',
        'Kebbi|kebbi',
        'Kogi|kogi',
        'Kwara|kwara',
        'Lagos|lagos',
        'Nassarawa|nassarawa',
        'Niger|niger',
        'Ogun|ogun',
        'Ondo|ondo',
        'Osun|osun',
        'Oyo|oyo',
        'Plateau|plateau',
        'Rivers|rivers',
        'Sokoto|sokoto',
        'Taraba|taraba',
        'Yobe|yobe',
        'Zamfara|zamfara',
        'Federal Capital Territory (FCT)|federal-capital-territory-fct'
   );
   return $choices;
}*/

/*
*
* Add Predefined Choices: Model
*
*/
add_filter( 'gform_predefined_choices', 'add_predefined_choice_model' );

function add_predefined_choice_model( $choices ) {
   $choices['Models'] = array(
       'Class room Training (Your training requires the students to come to a physical location)|classroom',
       'Online/Virtual Training (Your offer training virtually online or via social media, e.g whatsapp, youtube, e-learning portal, etc)|online',
   );
   return $choices;
}

/*
*
* Add Predefined Choices: Industries
*
*/
add_filter( 'gform_predefined_choices', 'add_predefined_choice_industries' );

function add_predefined_choice_industries( $choices ) {
    $choices['Industries'] = array(
        'Art & Craft|art_and_craft',
        'Barbing, Hairdressing & Beauty Services',
        'Bead Making & Fashion Accesories (Production)',
        'Bee Keeping',
        'Business Support Services Center',
        'Crop Farming',
        'Education & Training Businesses',
        'Event management',
        'Fertilizer & Livestock Feed',
        'Fish Farming',
        'FOREX & Crypto currency',
        'Freelancing',
        'Horticulture & landscaping',
        'Import/Export',
        'Interior Design & Decoration',
        'Laundry & DryCleaning',
        'Logistics & Transportation Business',
        'Make-up Artistry & Gele Tying Business',
        'Music, DJ & Sound Engineering',
        'Online Selling',
        'Paint Production, Sales & Services',
        'Pest Control',
        'Pet Breeding (Dogs, Cats, Birds, etc',
        'Photography & Cinematography',
        'Piggery',
        'Poultry Farming',
        'Printing & Publishing',
        'Production of Soap, Perfumes, Deodorants, etc',
        'Rabbit Farming',
        'Rentals',
        'Restaurant/Bar, Catering, Food Processing & Preservation',
        'Running a Supermarket/Shop',
        'Shoe/Bag Making & Leatherworks',
        'Snail Farming',
        'Software, Hardware & Technology related small businesses',
        'Solar & inverter related businesses',
        "Starting & Running a Male/Female/Children's Boutique",
        'Starting & Running a Pharmacy',
        'Tye & Dye Business Discussions',
        'Waste Management',
        'Wholesale & Retail of Agro Produce',
        'Wholesale/retail of Building Materials (Electrical, Mechanical, Civil)',
        'Wholesale/retail of Mobile phones, Computer & Technology related items and accessories',
        'Wholesale/retail of Spare Parts & Automobile Items',
        'Wholesale/retail of Books, Office Supplies, Stationaries and related items',
        'Writing/Blogging'
   );
   return $choices;
}