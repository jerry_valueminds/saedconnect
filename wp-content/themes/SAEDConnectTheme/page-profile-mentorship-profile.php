<?php /*Template Name: Profile - Mentor Profile*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();
        /* Meta Key */
        $meta_key = 'my_mentor_skills';
        $tax_type = 'business-mentorship';
        $post_type = 'mentor-profile';
    ?>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content">
                
            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <?php if( $_GET['type'] == 'mentor-profile' ) { ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        Edit My Mentor Profile          
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                            Cancel
                        </a>
                    </div>
                </div>
                <article class="page-summary">
                    <p>
                        These are the abilities you possess. These would help you get offers or gigs.
                    </p>
                </article>
                <form class="form" action="<?php echo currenturl(false) ?>" method="post">
                    <?php

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        /* Get Post */
                        $profile_query = new WP_Query();
                        $profile_query->query( 
                            array(
                                'post_type' => 'mentor-profile',
                                'post_status' => 'publish',
                                'author' => $current_user->ID,
                                'posts_per_page' => 1,
                            ) 
                        );

                        if ( $profile_query->have_posts() ) {

                            while ($profile_query->have_posts()) : $profile_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;   //Get Program ID

                            endwhile;

                        }
                                                               
                        if($_POST){
                            
                            /* Get Post Name */
                            $postName = 'Mentor-Profile-'.$current_user->ID;
                            
                            /* Meta */
                            $training_mentorship_difference = wp_kses_post( $_POST['training_mentorship_difference'] );
                            $why_become_mentor = wp_kses_post( $_POST['why_become_mentor'] );
                            $mentor_qualification = wp_kses_post( $_POST['mentor_qualification'] );
                            $mentor_qualities = wp_kses_post( $_POST['mentor_qualities'] );
                            
                            $have_smartphone = sanitize_text_field( $_POST['have_smartphone'] ); 
                            $internet_subscription = sanitize_text_field( $_POST['internet_subscription'] ); 
                            $have_whatsapp = sanitize_text_field( $_POST['have_whatsapp'] ); 
                            $have_telegram = sanitize_text_field( $_POST['have_telegram'] ); 
                            
                            $about_virtual_mentorship = wp_kses_post( $_POST['about_virtual_mentorship'] );

                            /* Save Post to DB */
                            $post_id = wp_insert_post(array (
                                'ID' => $post_id,
                                'post_type' => $post_type,
                                'post_title' => $postName,
                                'post_content' => "",
                                'post_status' => 'publish',
                            ));

                            update_post_meta( $post_id, 'training_mentorship_difference', $training_mentorship_difference );
                            update_post_meta( $post_id, 'why_become_mentor', $why_become_mentor );
                            update_post_meta( $post_id, 'mentor_qualification', $mentor_qualification );
                            update_post_meta( $post_id, 'mentor_qualities', $mentor_qualities );
                            
                            update_post_meta( $post_id, 'have_smartphone', $have_smartphone );
                            update_post_meta( $post_id, 'internet_subscription', $internet_subscription );
                            update_post_meta( $post_id, 'have_whatsapp', $have_whatsapp );
                            update_post_meta( $post_id, 'have_telegram', $have_telegram );
                            
                            update_post_meta( $post_id, 'about_virtual_mentorship', $about_virtual_mentorship );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                        }
                    ?>
                    <div class="form-item">
                        <label for="">What is the difference between Training & Mentorship?</label>
                        <textarea name="training_mentorship_difference" id="training_mentorship_difference" class="editor"><?php echo get_post_meta( $post_id, 'training_mentorship_difference', true ); ?></textarea>
                    </div>
                    <div class="form-item">
                        <label for="why_become_mentor">Why do you want to be a mentor?</label>
                        <textarea name="why_become_mentor" id="why_become_mentor" class="editor"><?php echo get_post_meta( $post_id, 'why_become_mentor', true ); ?></textarea>
                    </div>
                    <div class="form-item">
                        <label for="mentor_qualification">What qualifies you to be a mentor?</label>
                        <textarea name="mentor_qualification" id="mentor_qualification" class="editor"><?php echo get_post_meta( $post_id, 'mentor_qualification', true ); ?></textarea>
                    </div>
                    <div class="form-item">
                        <label for="mentor_qualities">In your opinion, what are the qualities of a great mentor?</label>
                        <textarea name="mentor_qualities" id="mentor_qualities" class="editor"><?php echo get_post_meta( $post_id, 'mentor_qualities', true ); ?></textarea>
                    </div>
                    <div class="form-item">
                        <label for="have_smartphone">Do you have a Smart Phone?</label>
                        <select name="have_smartphone" id="have_smartphone">
                            <?php $meta = get_post_meta( $post_id, 'have_smartphone', true ); ?>
                            <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?> >Yes</option>
                            <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?> >No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="internet_subscription">How regularly do you subscribe for internet data</label>
                        <select name="internet_subscription" id="internet_subscription">
                            <?php $meta = get_post_meta( $post_id, 'internet_subscription', true ); ?>
                            <option value="I always have internet on my phone" <?php echo ($meta == 'I always have internet on my phone') ? 'selected' : '' ?> >I always have internet on my phone</option>
                            <option value="I subscribe only when I need to" <?php echo ($meta == 'I subscribe only when I need to') ? 'selected' : '' ?> >I subscribe only when I need to</option>
                        </select>
                    </div>
	                <div class="form-item">
                        <label for="have_whatsapp">Do you have an active WhatsApp Number?</label>
                        <select name="have_whatsapp" id="have_whatsapp">
                            <?php $meta = get_post_meta( $post_id, 'have_whatsapp', true ); ?>
                            <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?> >Yes</option>
                            <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?> >No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="have_telegram">Are you on the Telegram Messaging App?</label>
                        <select name="have_telegram" id="have_telegram">
                            <?php $meta = get_post_meta( $post_id, 'have_telegram', true ); ?>
                            <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?> >Yes</option>
                            <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?> >No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="about_virtual_mentorship">If you were to mentor someone over WhatsApp or Telegram, how will you go about it?</label>
                        <textarea name="about_virtual_mentorship" id="about_virtual_mentorship" class="editor"><?php echo get_post_meta( $post_id, 'about_virtual_mentorship', true ); ?></textarea>
                    </div>
                    <div class="text-right">
                        <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
                
                <?php } elseif( $_GET['type'] == 'capabilities' ){ ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        Add / Edit My Mentorship Capabilities         
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                            Cancel
                        </a>
                    </div>
                </div>
                <article class="page-summary">
                    <p>
                        These are the abilities you possess. These would help you get offers or gigs.
                    </p>
                </article>
                <form action="<?php echo currenturl(false) ?>" method="post">
                    <?php

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        if($_POST){
                            /* Serialize Form Submission */
                            $serialized_data = maybe_serialize($_POST);

                            /* Save usermeta */
                            update_user_meta( $current_user->ID, $meta_key, $serialized_data);

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                        }

                        /* Get User Meta to populate Form */
                        $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, $meta_key, true) );

                        /*
                        *
                        * Populate Form Data from Terms
                        *
                        */
                        //Get Terms
                        $terms = get_terms( $tax_type, array('hide_empty' => false));

                        foreach ($terms as $term) { //Cycle through terms, one at a time

                            // Check and see if the term is a top-level parent. If so, display it.
                            $parent = $term->parent;
                            $term_id = $term->term_id; //Get the term ID
                            $term_name = $term->name; //Get the term name
                    ?>

                        <?php if( $parent == 0 ){ ?>

                            <label class="txt-sm d-inline-flex align-items-center padding-r-20 padding-b-15">
                                <input
                                    class="custom-check"
                                    type="checkbox" 
                                    value="<?php echo $term_id ?>" 
                                    name="<?php echo $meta_key ?>[]" 
                                    <?php echo in_array( $term_id, $unserialized_data[$meta_key] ) ? "checked" : "" ?>
                                >
                                <span class="padding-l-10">
                                    <?php echo $term_name; ?>
                                </span>
                            </label>

                        <?php } ?>

                    <?php 
                        } 
                    ?>
                    <div class="text-right">
                        <input type="submit" class="btn btn-yellow-dark txt-xs">
                    </div>
                </form>
                
                <?php }elseif( $_GET['type'] == 'capability-description' && $_GET['term-id'] ){ ?>
                
                <?php $parent_term = get_term( $_GET['term-id'] ); ?>
                <div class="page-header">
                    <h1 class="page-title">
                        <?php echo $parent_term->name ?> Description     
                    </h1>
                    <div class="cta">
                        <a href="<?php echo currenturl(true); ?>" class="cta-btn">
                            Cancel
                        </a>
                    </div>
                </div>
                <article class="page-summary">
                    <p>
                        These are the abilities you possess. These would help you get offers or gigs.
                    </p>
                </article>
                <form action="<?php echo currenturl(false) ?>" method="post" class="form">
                    <?php

                        /*
                        *
                        * Save / Retrieve Form Data
                        *
                        */
                        $term_id = $_GET['term-id']; 
                        /* Get Post */
                        $profile_query = new WP_Query();
                        $profile_query->query( 
                            array(
                                'post_type' => 'cap-description',
                                'post_status' => 'publish',
                                'author' => $current_user->ID,
                                'posts_per_page' => 1,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => $tax_type,
                                        'terms' => $term_id,
                                    )
                                ),
                            ) 
                        );

                        if ( $profile_query->have_posts() ) {

                            while ($profile_query->have_posts()) : $profile_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;   //Get Program ID

                            endwhile;

                        }
                                                           
                        if($_POST){
                            
                            /* Get Post Name */
                            $postName = 'Mentor-Business-Capability-Description-'.$current_user->ID.'-'.$term_id;
                            
                            /* Meta */
                            $years_of_experience = wp_kses_post( $_POST['years_of_experience'] );
                            $business_experience = wp_kses_post( $_POST['business_experience'] );
                            $other_mentorship_models = wp_kses_post( $_POST['other_mentorship_models'] );
                            $face_to_face_locations = wp_kses_post( $_POST['face_to_face_locations'] );
                            
                            $sub_terms = $_POST['sub_terms'];
                            $solutions_proferred = $_POST['solutions_proferred'];
                            $mentorship_models = $_POST['mentorship_models'];
 
                            $previous_mentorship_experience = sanitize_text_field( $_POST['previous_mentorship_experience'] ); 
                            

                            /* Save Post to DB */
                            $post_id = wp_insert_post(array (
                                'ID' => $post_id,
                                'post_type' => 'cap-description',
                                'post_title' => $postName,
                                'post_content' => "",
                                'post_status' => 'publish',
                            ));

                            update_post_meta( $post_id, 'years_of_experience', $years_of_experience );
                            update_post_meta( $post_id, 'business_experience', $business_experience);
                            update_post_meta( $post_id, 'other_mentorship_models', $other_mentorship_models );
                            update_post_meta( $post_id, 'face_to_face_locations', $face_to_face_locations );
                            
                            update_post_meta( $post_id, 'sub_terms', $sub_terms );
                            update_post_meta( $post_id, 'solutions_proferred', $solutions_proferred );
                            update_post_meta( $post_id, 'mentorship_models', $mentorship_models );
                            
                            update_post_meta( $post_id, 'previous_mentorship_experience', $previous_mentorship_experience );
                                                        
                            wp_set_post_terms( $post_id, $term_id, $tax_type );

                            /* Redirect */
                            printf('<script>window.location.replace("%s")</script>', currenturl(true) );
                        }
                    ?>
                    <div class="form-item">
                        <label for="years_of_experience">For how long have you been practicing this business (in Years)</label>
                        <input type="number" id="years_of_experience" name="years_of_experience" value="<?php echo get_post_meta( $post_id, 'years_of_experience', true ); ?>">
                    </div>
                    <div class="form-item">
                        <label for="business_experience">Describe your experience in this business.</label>
                        <textarea name="business_experience" id="business_experience" class="editor"><?php echo get_post_meta( $post_id, 'business_experience', true ); ?></textarea>
                    </div>
                    <?php 
                        $child_terms = get_terms( $tax_type, array('hide_empty' => false, 'parent' => $parent_term->term_id) );
                        if( $child_terms ){
                            $meta = get_post_meta( $post_id, 'sub_terms', true );
                    ?>
                    <div class="form-item">
                        <label class="padding-b-10">What area of this business are you great at?</label>
                        <?php foreach($child_terms as $child_term ){ ?>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="<?php echo $child_term->term_id ?>" 
                                name="sub_terms[]" 
                                <?php echo ( in_array( $child_term->term_id, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                <?php echo $child_term->name ?>
                            </span>
                        </label>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <div class="form-item">
                        <label for="other_mentorship_models">Apart from the above listed, what other models of this business can you mentor on?</label>
                        <textarea name="other_mentorship_models" id="other_mentorship_models" class="editor"><?php echo get_post_meta( $post_id, 'other_mentorship_models', true ); ?></textarea>
                    </div>
                    <div class="form-item">
                        <?php $meta = get_post_meta( $post_id, 'solutions_proferred', true ); ?>
                        <label class="padding-b-10">What sort of problems can you help people solve in this area?</label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="1" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array( 1, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                How to start the business from scratch
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="2" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array( 2, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                How to raise money to start the business
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="3" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array( 3, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                Where to get suppliers and tools
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="4" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array( 4, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                How to find partners and staff for the business
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="5" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array( 5, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                How to setup the business
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="6" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array( 6, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                How to get customers
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="7" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array( 7, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                How to grow the business
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="8" 
                                name="solutions_proferred[]"
                                <?php echo ( in_array( 8, $meta) ) ? 'checked' : '' ?> 
                            >
                            <span class="padding-l-10">
                                How to avoid and overcome common challenges in the business
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="9" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array( 9, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                Technical Skills in the business
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="10" 
                                name="solutions_proferred[]" 
                                <?php echo ( in_array(10, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                How to optimize your profit in the business
                            </span>
                        </label>
                    </div>
                    <div class="form-item">
                        <?php $meta = get_post_meta( $post_id, 'previous_mentorship_experience', true ); ?>
                        <label for="previous_mentorship_experience">Have you ever mentored someone in this business before?</label>
                        <select name="previous_mentorship_experience" id="previous_mentorship_experience">
                            <option value="Yes" <?php echo ($meta == 'Yes') ? 'selected' : '' ?>>Yes</option>
                            <option value="No" <?php echo ($meta == 'No') ? 'selected' : '' ?>>No</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <?php $meta = get_post_meta( $post_id, 'mentorship_models', true ); ?>
                        <label class="padding-b-10">What mentorship Models can you offer?</label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="1" 
                                name="mentorship_models[]" 
                                <?php echo ( in_array(1, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                I can mentor over WhatsApp or Telegram
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center padding-b-10">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="2" 
                                name="mentorship_models[]"
                                <?php echo ( in_array(2, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                I can mentor via Facebook
                            </span>
                        </label>
                        <label class="txt-sm d-flex align-items-center">
                            <input
                                class="custom-check"
                                type="checkbox" 
                                value="3" 
                                name="mentorship_models[]"
                                <?php echo ( in_array(3, $meta) ) ? 'checked' : '' ?>
                            >
                            <span class="padding-l-10">
                                I can mentor in-person
                            </span>
                        </label>
                    </div>
                    <div class="form-item">
                        <label for="face_to_face_locations">In what State can you provide face-to-face mentorship for this business?</label>
                        <input type="text" id="face_to_face_locations" name="face_to_face_locations" value="<?php echo get_post_meta( $post_id, 'face_to_face_locations', true ); ?>">
                    </div>
                    <div class="text-right">
                        <a href="<?php echo currenturl(true); ?>" class="btn btn-white txt-xs">Cancel</a>
                        <input type="submit" class="btn btn-blue txt-sm padding-lr-15">
                    </div>
                </form>
                 
                <?php }else{ ?> 
                  
                <div class="text-center padding-o-40">
                    <p class="txt-3em txt-color-yellow padding-b-15">
                        <i class="fa fa-exclamation-triangle"></i>
                    </p>
                    <p class="txt-color-lighter padding-b-30">
                        Oops! Something went wrong. Kindly go back and try again.
                    </p>
                    <p>
                        <a href="<?php echo currenturl(true); ?>" class="btn btn-blue txt-sm">Go back</a>
                    </p>
                </div>
                                   
                <?php } ?>                    

            <?php } else { //Empty Profile Message ?>
                
                <div class="page-header">
                    <h1 class="page-title">
                        My Mentor Profile         
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        These are the abilities you possess. These would help you get offers or gigs.
                    </p>
                </article>
                
                <!-- Mentor Profile -->
                <div class="section-wrapper">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Mentor Information                                
                        </h2>
                        <div class="text-right">
                            <a href="<?php echo currenturl(true); ?>?view=form&type=mentor-profile" class="edit-btn">
                                Add / Edit Profile
                            </a>
                        </div>
                    </div>
                    
                    <div class="entry">
                    <?php
                        $profile_query = new WP_Query();
                        $profile_query->query( 
                            array(
                                'post_type' => 'mentor-profile',
                                'post_status' => 'publish',
                                'author' => $current_user->ID,
                                'posts_per_page' => 1,
                            ) 
                        );

                        if ( $profile_query->have_posts() ) {

                            while ($profile_query->have_posts()) : $profile_query->the_post();

                            /* Variables */
                            $post_id = $post->ID;   //Get Program ID
                    ?>
                        <div class="row row-10">
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    What is the difference between Training & Mentorship?      
                                </p>
                                <article class="text-box txt-sm">
                                    <?php echo get_post_meta( $post_id, 'training_mentorship_difference', true ); ?>
                                </article>
                            </div>
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    Why do you want to be a mentor?       
                                </p>
                                <article class="text-box txt-sm">
                                    <?php echo get_post_meta( $post_id, 'why_become_mentor', true ); ?>
                                </article>
                            </div>
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    What qualifies you to be a mentor?      
                                </p>
                                <article class="text-box txt-sm">
                                    <?php echo get_post_meta( $post_id, 'mentor_qualification', true ); ?>
                                </article>
                            </div>
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    In your opinion, what are the qualities of a great mentor?      
                                </p>
                                <article class="text-box txt-sm">
                                    <?php echo get_post_meta( $post_id, 'mentor_qualities', true ); ?>
                                </article>
                            </div>
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    Do you have a Smart Phone?     
                                </p>
                                <p class="txt-sm">
                                    <?php echo get_post_meta( $post_id, 'have_smartphone', true ); ?>
                                </p>
                            </div>
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    How regularly do you subscribe for internet data    
                                </p>
                                <p class="txt-sm">
                                    <?php echo get_post_meta( $post_id, 'internet_subscription', true ); ?>
                                </p>
                            </div>
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    Do you have an active WhatsApp Number?    
                                </p>
                                <p class="txt-sm">
                                    <?php echo get_post_meta( $post_id, 'have_whatsapp', true ); ?>
                                </p>
                            </div>
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    Are you on the Telegram Messaging App?     
                                </p>
                                <p class="txt-sm">
                                    <?php echo get_post_meta( $post_id, 'have_telegram', true ); ?>
                                </p>
                            </div>
                            <div class="col-md-12 padding-lr-10 padding-b-20">
                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                    If you were to mentor someone over WhatsApp or Telegram, how will you go about it?     
                                </p>
                                <article class="text-box txt-sm">
                                    <?php echo get_post_meta( $post_id, 'about_virtual_mentorship', true ); ?>
                                </article>
                            </div>
                        </div>
                        
                    <?php
                            endwhile;

                        }else{
                    ?>
                       
                        <p class="txt-sm padding-b-15">You have not added your mentor information.</p>

                    <?php } ?>
                    </div>
                </div>

                <!-- Mentor Capabilities -->
                <div class="section-wrapper padding-b-10">
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            My Mentor Capabilities                            
                        </h2>
                        <div class="text-right">
                            <a href="<?php echo currenturl(true); ?>?view=form&type=capabilities" class="edit-btn">
                                Add Capabilities
                            </a>
                        </div>
                    </div>
                    <?php
                        $title = 'Capabilities';
                        /* Get User Meta to populate Form */
                        $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, 'my_mentor_skills', true) );

                    ?>

                    <?php if(!$unserialized_data){ //If no entry ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                My <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Empty! Start by Adding your <?php echo $title ?>.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-career-ash txt-xxs no-m-b"
                                    href="<?php echo currenturl(true); ?>?view=form"
                                >
                                    Add Capabilities
                                </a>
                            </div>
                        </div>                            

                    <?php } else { ?>

                        <div class="entry">
                        <?php
                            foreach($unserialized_data[$meta_key]  as $saved_id){
                                $term = get_term($saved_id);
                                $term_id = $term->term_id; //Get the term ID
                                $term_name = $term->name; //Get the term name
                                $post_id = 0;
                        ?>
                            
                            <?php
                                $description_query = new WP_Query();
                                $description_query->query( 
                                    array(
                                        'post_type' => 'cap-description',
                                        'post_status' => 'publish',
                                        'author' => $current_user->ID,
                                        'posts_per_page' => 1,
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => $tax_type,
                                                'terms' => $term_id,
                                            )
                                        ),
                                    ) 
                                );

                                if ( $description_query->have_posts() ) {

                                    while ($description_query->have_posts()) : $description_query->the_post();

                                        /* Variables */
                                        $post_id = $post->ID;   //Get Program ID
                                    
                                    endwhile; 
                                }
                            ?>

                            <div class="bg-ghostwhite padding-o-10 margin-b-10">
                                <div class="row">
                                    <div class="col-5">
                                        <p class="txt-sm txt-color-dark txt-medium">
                                            <?php echo $term_name ?>
                                        </p>                                 
                                    </div>
                                    <p class="col-7 txt-xs text-right">
                                        <?php if ($post_id){ ?>
                                            <a href="#capability-description-collapse-<?php echo $term_id; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="capability-description-collapse-<?php echo $term_id; ?>">
                                               View Description
                                               <i class="fa fa-chevron-down"></i>
                                           </a>

                                       <?php }else{ ?>
                                           <a href="<?php echo 'https://www.saedconnect.org/competency-profile/my-mentor-profile/?view=form&type=capability-description&term-id='.$term_id; ?>"class="">
                                               <i class="fa fa-plus"></i>
                                               Add Description
                                           </a>
                                       <?php } ?>
                                    </p>
                                </div>

                                <?php if($post_id){ ?>   
                                <div class="collapse" id="capability-description-collapse-<?php echo $term_id; ?>">
                                    <div class="padding-t-10 margin-t-10 border-t-1">
                                        <div class="row row-10">
                                            <div class="col-md-12 padding-lr-10 padding-b-10">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    For how long have you been practicing this business (in Years)    
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo get_post_meta( $post_id, 'years_of_experience', true ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-12 padding-lr-10 padding-b-10">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Describe your experience in this business    
                                                </p>
                                                <article class="text-box txt-sm">
                                                    <?php echo get_post_meta( $post_id, 'business_experience', true ); ?>
                                                </article>
                                            </div>
                                            <div class="col-md-12 padding-lr-10 padding-b-10">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    What area of this business are you great at?    
                                                </p>
                                                <p class="txt-sm">
                                                    <?php 
                                                        $meta_ids = get_post_meta( $post_id, 'sub_terms', true );
                                                        $counter = 1;
                                                   
                                                        foreach( $meta_ids as $meta_id ){
                                                            $sub_term = get_term($meta_id);
                                                            echo '<span class="d-block">'.$counter.'. '.$sub_term->name.'</span>';         
                                                            $counter++;
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-md-12 padding-lr-10 padding-b-10">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Apart from the above listed, what other models of this business can you mentor on?    
                                                </p>
                                                <article class="text-box txt-sm">
                                                    <?php echo get_post_meta( $post_id, 'other_mentorship_models', true ); ?>
                                                </article>
                                            </div>
                                            <div class="col-md-12 padding-lr-10 padding-b-10">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    What sort of problems can you help people solve in this area?     
                                                </p>
                                                <p class="txt-sm">
                                                    <?php 
                                                        $meta_ids = get_post_meta( $post_id, 'solutions_proferred', true );
                                                        $counter = 1;
                                                   
                                                        foreach( $meta_ids as $meta_id ){
                                                            if( $meta_id == 1 )
                                                                echo '<span class="d-block">'.$counter.'. How to start the business from scratch</span>'; 
                                                            elseif( $meta_id == 2 )
                                                                echo '<span class="d-block">'.$counter.'. How to raise money to start the business.</span>'; 
                                                            elseif( $meta_id == 3 )
                                                                echo '<span class="d-block">'.$counter.'. Where to get suppliers and tools</span>';
                                                            elseif( $meta_id == 4 )
                                                                echo '<span class="d-block">'.$counter.'. How to find partners and staff for the business.</span>'; 
                                                            elseif( $meta_id == 5 )
                                                                echo '<span class="d-block">'.$counter.'. How to setup the business</span>'; 
                                                            elseif( $meta_id == 6 )
                                                                echo '<span class="d-block">'.$counter.'. How to get customers</span>'; 
                                                            elseif( $meta_id == 7 )
                                                                echo '<span class="d-block">'.$counter.'. How to grow the business</span>'; 
                                                            elseif( $meta_id == 8 )
                                                                echo '<span class="d-block">'.$counter.'. How to avoid and overcome common challenges in the business</span>'; 
                                                            elseif( $meta_id == 9 )
                                                                echo '<span class="d-block">'.$counter.'. Technical Skills in the business</span>'; 
                                                            elseif( $meta_id == 10 )
                                                                echo '<span class="d-block">'.$counter.'. How to optimize your profit in the business</span>'; 
                                                            
                                                            $counter++;
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-md-12 padding-lr-10 padding-b-10">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    Have you ever mentored someone in this business before?     
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo get_post_meta( $post_id, 'previous_mentorship_experience', true ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-12 padding-lr-10 padding-b-10">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    What mentorship Models can you offer?    
                                                </p>
                                                <p class="txt-sm">
                                                    <?php 
                                                        $meta_ids = get_post_meta( $post_id, 'mentorship_models', true );
                                                        $counter = 1;
                                                   
                                                        foreach( $meta_ids as $meta_id ){
                                                            if( $meta_id == 1 )
                                                                echo '<span class="d-block">'.$counter.'. I can mentor over WhatsApp or Telegram</span>'; 
                                                            elseif( $meta_id == 2 )
                                                                echo '<span class="d-block">'.$counter.'. I can mentor via Facebook</span>'; 
                                                            elseif( $meta_id == 3 )
                                                                echo '<span class="d-block">'.$counter.'. I can mentor in-person</span>'; 
                                                            
                                                            $counter++;
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-md-12 padding-lr-10">
                                                <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                    In what State can you provide face-to-face mentorship for this business?    
                                                </p>
                                                <p class="txt-sm">
                                                    <?php echo get_post_meta( $post_id, 'face_to_face_locations', true ); ?>
                                                </p>
                                            </div>
                                            <div class="col-md-12 padding-lr-10 padding-t-20 padding-b-10">
                                                <a href="<?php echo 'https://www.saedconnect.org/competency-profile/my-mentor-profile/?view=form&type=capability-description&term-id='.$term_id; ?>" class="edit-btn">
                                                   <i class="fa fa-pencil"></i>
                                                   Edit Description
                                               </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>    

                            </div>

                        <?php } ?>
                        </div>
                        
                    <?php } ?>
                </div>   

            <?php } ?>   
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php get_footer('user-dashboard'); ?>