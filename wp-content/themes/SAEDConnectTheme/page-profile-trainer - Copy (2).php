<?php /*Template Name: Profile - Trainer*/ ?>
   
   <?php
                    
        if ( !is_user_logged_in() ) {
            // If User is Logged in, redirect to User Dashbord
            $dashboard_link = network_home_url().'/login'; //Get Daasboard Page Link by ID
            
            if ( wp_redirect( $dashboard_link ) ) {
                exit;
            }
        }
    
    ?>
    
    <?php get_header('user-dashboard') ?>
    

    <?php
        /* Publication */
        $publication_key   = 'publication_status';
        $accreditation_key   = 'nysc_accreditation_status';
        /* User is Logged in */

        /* Select Page View Request */
        if(isset($_GET['action'])){
            $page_type = $_GET['action']; 
        } else {
            $page_type = '';
        }
    
        /* Get Base URL */
        $base_url = get_site_url().'/my-dashboard';

        /* Get User */
        $current_user = wp_get_current_user();

        /* Get Avatar */
        $avatar_url = 'https://www.saedconnect.org/service-provider-directory/wp-content/themes/SAEDConnectTheme/images/icons/user-icon.png';
        $meta_key = 'user_avatar_url';
        $get_avatar_url = get_user_meta($current_user->ID, $meta_key, true);

        if($get_avatar_url){
            $avatar_url = $get_avatar_url;
        }


        /* Get Trainer profile */
        $gf_id = 1; //Form ID
        $gv_id = 637; //Gravity View ID
        $title = 'Trainer Information';

        $entry_count = 0;

        /* GF Search Criteria */
        $search_criteria = array(

        'field_filters' => array( //which fields to search

            array(

                'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                )
            )
        );

        /* Get GF Entry Count */
        $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );

        /* Get Entries */
        $entries = GFAPI::get_entries( $gf_id, $search_criteria );

        $post_id = $entries[0]['post_id'];
                
    ?>
    
    <style>
        .work-profile{
            display: none !important;
        }

        .gform_wrapper .top_label .gfield_label {
            font-size: 0.8rem !important;
            font-weight: 500 !important;
        }

        .gform_wrapper textarea, .gform_wrapper input, .gform_wrapper select {
            font-size: 0.9rem !important;
            width: 100% !important;
        }

        .gform_wrapper .gform_button {
            background-color: #b55085 !important;
            font-size: 0.7rem !important;
            width: auto !important;
        }

        .gv-edit-entry-title, .gv-button-delete, .gv-button-cancel{
            display: none !important;
        }
        
        .gform_wrapper .gsection .gfield_label, .gform_wrapper h2.gsection_title, .gform_wrapper h3.gform_title {
            font-weight: 500;
            font-size: 0.9rem !important;
        }
        
        .gform_wrapper ul.gfield_checkbox li, .gform_wrapper ul.gfield_radio li {
            margin-right: 15px !important;
            display: inline-flex !important;
            align-items: center;
        }
        
        .gform_wrapper ul.gfield_checkbox li label, .gform_wrapper ul.gfield_radio li label {
            max-width: unset !important;
        }
        
        .gform_wrapper.gf_browser_chrome .gfield_checkbox li input, .gform_wrapper.gf_browser_chrome .gfield_checkbox li input[type=checkbox], .gform_wrapper.gf_browser_chrome .gfield_radio li input[type=radio] {
            margin-top: 0 !important;
            margin-right: 3px !important;
            font-size: 0.9em !important;
        }
    </style>
    
    <?php get_template_part( 'template-parts/user-dashboard/dashboard_header' ); ?>
    
    <main class="main-content txt-color-light bg-white" style="margin-top: 70px">
        <section class="row">
            <?php get_template_part( 'template-parts/user-dashboard/_dashboard-nav' ); ?>
            <div class="dashboard-multi-main-content">
                <div class="page-header">
                    <h1 class="page-title">
                        Trainer Profile
                    </h1>
                </div>
                <article class="page-summary">
                    <p>
                        Give information about your training outfit as this would help potential clients understand what you do. Also you should add your key competencies, as this would help increase your credibility.
                    </p>
                </article>

            <?php if($_GET['view'] == 'form'){ //Display form  ?>

                <div class="section-wrapper">
                <?php if( $_REQUEST['gf-id'] ){ ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/service-provider-directory/trainer-profile/" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">
                        <?php 
                            echo do_shortcode( "[gravityform id='".$_REQUEST['gf-id']."' title='false' description='false' ajax='false']"); 
                        ?>
                    </div>

                <?php } else { ?>

                    <div class="header">
                        <h2 class="section-wrapper-title">
                            Add <?php echo $_REQUEST['form-title'] ?>
                        </h2>
                        <div class="text-right">
                            <a 
                                href="https://www.saedconnect.org/service-provider-directory/trainer-profile/" 
                                class="edit-btn"
                            >
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="entry">   
                        <form action="https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form" method="post">
                            <?php
                                /* Meta Key */
                                $meta_key = 'training_area';
                                $tax_type = 'training-category';
                                $redirect_link = 'https://www.saedconnect.org/service-provider-directory/trainer-profile/';
                                /*
                                *
                                * Save / Retrieve Form Data
                                *
                                */
                                if($_POST){
                                    /* Serialize Form Submission */
                                    $serialized_data = maybe_serialize($_POST);

                                    /* Save usermeta */
                                    update_user_meta( $current_user->ID, $meta_key, $serialized_data);

                                    /* Redirect */
                                    printf('<script>window.location.replace("%s")</script>', $redirect_link);
                                }

                                /* Get User Meta to populate Form */
                                $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, $meta_key, true) );

                                /*
                                *
                                * Populate Form Data from Terms
                                *
                                */
                                //Get Terms
                                $terms = get_terms( $tax_type, array('hide_empty' => false));

                                foreach ($terms as $term) { //Cycle through terms, one at a time

                                    // Check and see if the term is a top-level parent. If so, display it.
                                    $parent = $term->parent;
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>


                                <div class="margin-b-20">
                                <?php if( $parent == 0 ){ ?>

                                    <div class="txt-normal-s txt-medium txt-color-dark margin-b-15">
                                        <?php echo $term_name; ?>
                                    </div>
                                    <?php
                                        foreach ($terms as $child_term) {
                                            // Check and see if the term is a top-level parent. If so, display it.
                                            $child_parent = $child_term->parent;
                                            $child_term_id = $child_term->term_id; //Get the term ID
                                            $child_term_name = $child_term->name; //Get the term name

                                            if( $child_parent == $term_id ){
                                    ?>
                                            <label class="txt-sm d-inline-flex align-items-center padding-r-10 padding-b-10">
                                                <input
                                                    class="margin-r-5"
                                                    type="checkbox" 
                                                    value="<?php echo $child_term_id ?>" 
                                                    name="<?php echo $term_id ?>[]" 
                                                    <?php echo in_array($child_term_id, $unserialized_data[$term_id]) ? "checked" : "" ?>
                                                >
                                                <?php echo $child_term_name; ?>
                                            </label>
                                    <?php
                                            }
                                        }
                                    ?>
                                <?php } ?>
                                </div>

                            <?php 
                                } 
                            ?>
                            <div class="text-right">
                                <input type="submit" class="btn btn-yellow-dark txt-xs">
                            </div>
                        </form>
                    </div>

                <?php } ?>                    
            </div>      

            <?php } elseif( $_GET['view'] == 'publication' ){ ?>
            
            <?php
                /* Publish / Unpublish & Return */
                /* Meta value to save */
                $value = "user_published";

                /* Get saved meta */
                $saved_meta = get_post_meta( $post_id, $publication_key, true );

                if ( $saved_meta ) //If published, Unpublish
                    delete_post_meta( $post_id, $publication_key );
                else //If Unpublished, Publish
                    update_post_meta( $post_id, $publication_key, $value );

                /* Redirect */
                printf('<script>window.location.replace("%s")</script>', currentUrl(true));
            ?>
            
            <?php } elseif( $_GET['view'] == 'form-nysc-accreditation' ){ ?>
            
            <?php
                if($_POST){
                    /* Publish / Unpublish & Return */
                    /* Meta value to save */
                    $value = 'user_published';

                    foreach ($_POST['state'] as $term) { //Cycle through terms, one at a time
                        /* Get saved meta */
                        $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term, true );

                        if ( $saved_meta ) //If published, Unpublish
                            delete_post_meta( $post_id, $accreditation_key.'_'.$term );
                        else //If Unpublished, Publish
                            update_post_meta( $post_id, $accreditation_key.'_'.$term, $value );
                    }

                    /* Redirect */
                    printf('<script>window.location.replace("%s")</script>', currentUrl(true));
                }
            ?>

            <?php } else { //Empty Profile Message ?>

                <!-- Trainer Information -->
                <div class="section-wrapper">

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Empty! Start by creating your <?php echo $title ?>.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b"
                                    href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Create Profile
                                </a>
                            </div>
                        </div>    

                    <?php } else { ?>

                        <?php foreach( $entries as $entry ){ ?>

                            <div class="header">
                                <h2 class="section-wrapper-title">
                                    <?php echo $title ?>
                                </h2>
                            </div>

                            
                            <div class="entry">
                                <div class="row row-10">
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Name of Training Outfit        
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 1 ); ?>
                                        </p>
                                    </div>
                                    <div class="col-md-12 padding-lr-10 padding-b-20">
                                        <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                            Brief Trainer Profile          
                                        </p>
                                        <p class="txt-sm">
                                            <?php echo rgar( $entry, 5 ); ?>
                                        </p>
                                    </div>
                                </div>
                                <div id="trainerProfileContent" class="collapse" aria-labelledby="headingOne">
                                    <div class="row row-10">
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                What training models are you open to providing?         
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 3; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Skill Areas where you provide training          
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 2; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Name of Contact person for any enquiries         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 8 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                States where you can offer private tutoring & classroom trainings          
                                            </p>
                                            <p class="txt-sm">
                                                <?php
                                                    $field_id = 7; // Update this number to your field id number
                                                    $field = RGFormsModel::get_field( $gf_id, $field_id );
                                                    $value = is_object( $field ) ? $field->get_value_export( $entry ) : '';
                                                    echo $value;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Primary Contact Address         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 9 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact Email address (Where anyone interested in this training should email for enquiry)
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 10 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact Phone Number (Where anyone interested in training with you should call for enquiry)         
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 11 ); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-12 padding-lr-10 padding-b-20">
                                            <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                Contact WhatsApp Number (Where anyone interested in training with you should WhatsApp for enquiry)          
                                            </p>
                                            <p class="txt-sm">
                                                <?php echo rgar( $entry, 12 ); ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="padding-b-15">
                                        <a 
                                            href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=trainer-profile' ?>&form-title=Trainer Information" 
                                            class="btn btn-ash txt-xs no-b-m"
                                        >
                                            Edit
                                        </a>
                                    </div>
                                </div>
                                <div class="text-center padding-b-15">
                                    <a data-toggle="collapse" href="#trainerProfileContent" aria-expanded="false" class="d-inline-block padding-o-10 txt-normal-s bg-blue txt-color-white">
                                        <span class="padding-r-10 txt-color-white">show more</span>
                                        <i class="fa fa-chevron-down txt-color-white"></i>
                                    </a>
                                </div>
                            </div>

                        <?php } ?>

                    <?php } ?>
                </div>

                <!-- Training Area -->
                <div class="section-wrapper padding-b-10">
                <?php
                    $title = 'Skills You Train On';

                    $competency_gf_id = 11; //Form ID
                    $competency_gv_id = 681; //Gravity View ID

                    $entry_count = 0;

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get Competency Entries */
                    $competency_entries = GFAPI::get_entries( $competency_gf_id, $search_criteria );

                    /* Get Competency Entry Count */
                    $competency_entry_count = GFAPI::count_entries( $competency_gf_id, $search_criteria );



                    /* Get User Meta to populate Form */
                    $unserialized_data = maybe_unserialize( get_user_meta($current_user->ID, 'training_area', true) );

                ?>

                    <?php if(!$unserialized_data){ //If no entry ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                        <div class="entry">
                            <h3 class="txt-color-dark txt-normal-s txt-medium margin-t-10 margin-b-20">
                                Empty! Start by Adding your <?php echo $title ?>.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b"
                                    href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&form-title=%s", $title); ?>"
                                >
                                    Add Skills
                                </a>
                            </div>
                        </div>                            

                    <?php } else { ?>

                        <div class="header">
                            <h2 class="section-wrapper-title">
                                <?php echo $title ?>
                            </h2>
                            <div class="text-right">
                                <a 
                                    href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&form-title=%s", $title); ?>" 
                                    class="edit-btn"
                                >
                                    Add Skills
                                </a>
                            </div>
                        </div>
                            <?php
                                foreach($unserialized_data as $key => $subterms){
                                    $term = get_term($key);
                                    $term_id = $term->term_id; //Get the term ID
                                    $term_name = $term->name; //Get the term name
                            ?>

                                <div class="entry">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-15">
                                        <?php echo $term_name ?> skills    
                                    </p>
                                        <?php 
                                            foreach($subterms as $subterm){
                                                $child_term = get_term($subterm);
                                                $child_term_id = $child_term->term_id; //Get the term ID
                                                $child_term_name = $child_term->name; //Get the term name
                                        ?>

                                    <div class="row bg-ghostwhite padding-o-10 margin-b-10">
                                        <div class="col-5">
                                            <p class="txt-sm txt-color-dark txt-medium">
                                                <?php echo $child_term_name ?>
                                            </p>                                 
                                        </div>
                                        <p class="col-7 txt-xs text-right">
                                        <?php 
                                            $itExist = false;

                                            foreach( $competency_entries as $competency_entry ){ 
                                                if(rgar( $competency_entry, 1 ) == trim($child_term_id)){
                                                    $itExist = true;
                                                }
                                            }

                                            if(!$itExist){
                                        ?>
                                           <a href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=11&form-title=your %s Experience&parent_id=%s", $child_term_name, trim($child_term_id)); ?>"class="">
                                               <i class="fa fa-plus"></i>
                                               Add Description
                                           </a>
                                        <?php } ?>
                                        </p>

                                            <?php 
                                                foreach( $competency_entries as $competency_entry ){ 
                                                    if(rgar( $competency_entry, 1 ) == $child_term_id){
                                            ?>
                                        <div class="col-12 padding-t-10 margin-t-10 border-t-1">
                                            <div class="row row-10">
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What is your experience in this subject?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 2 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10 padding-b-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        Number of Years of Experience     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php echo rgar( $competency_entry, 3 ); ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10">
                                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                                        What training models are you open to providing for this subject area?     
                                                    </p>
                                                    <p class="txt-sm">
                                                        <?php
                                                            $field_id = 4; // Update this number to your field id number
                                                            $field = RGFormsModel::get_field( $competency_gf_id, $field_id );
                                                            $value = is_object( $field ) ? $field->get_value_export( $competency_entry ) : '';
                                                            echo $value;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 padding-lr-10 padding-t-20 padding-b-10">
                                                    <a href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$competency_entry['id'].'" view_id="'.$competency_gv_id.'" action="edit" return="url" /]').'&view=trainer-profile' ?>" class="edit-btn">
                                                       <i class="fa fa-pencil"></i>
                                                       Edit Description
                                                   </a>
                                                </div>
                                            </div>
                                        </div>
                                            <?php 
                                                    }
                                                }
                                            ?>

                                    </div>

                                        <?php } ?>
                                </div>

                            <?php } ?>


                    <?php } ?>
                </div>  
                
                <!-- Certifications / Affiliations -->
                <div class="section-wrapper">
                <?php
                    $gf_id = 13; //Form ID
                    $gv_id = 912; //Gravity View ID
                    $title = 'Certifications / Affiliations (if any)';

                    $entry_count = 0;

                    /* Get Entries */
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );

                    /* GF Search Criteria */
                    $search_criteria = array(

                    'field_filters' => array( //which fields to search

                        array(

                            'key' => 'created_by', 'value' => $current_user->ID, //Current logged in user
                            )
                        )
                    );

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );    
                ?>
                    <div class="header">
                        <h2 class="section-wrapper-title">
                            <?php echo $title ?>
                        </h2>
                    </div>

                    <?php if(!$entry_count){ //If no entry ?>

                        <div class="entry">
                            <h3 class="txt-color-dark txt-sm txt-height-1-4 margin-t-10 margin-b-20">
                                What certifications do you possess? What year were they acquired? Please provide all the information about your certifications here.
                            </h3>

                            <div class="padding-b-20">
                                <a 
                                    class="btn btn-ash txt-xxs no-m-b" 
                                    href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                                >
                                    Add <?php echo $title ?>
                                </a>
                            </div>
                        </div>

                    <?php } else { ?>
                        <?php foreach( $entries as $entry ){ ?>

                        <div class="entry">
                            <div class="row row-10">
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Certifications / Affiliations Name
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 1 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Type       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 15 ); ?>
                                    </p>
                                </div>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        From       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 11 ); ?>
                                    </p>
                                </div>

                                <?php if( rgar( $entry, 12 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        To         
                                    </p>
                                    <p class="txt-sm">
                                    <?php 
                                        if( rgar( $entry, 12 ) ){
                                            echo rgar( $entry, 12 );
                                        } else {
                                            echo 'Present';
                                        }

                                    ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 19 ) ){ ?>
                                <div class="col-md-6 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        To       
                                    </p>
                                    <p class="txt-sm">
                                    <?php 
                                        if( rgar( $entry, 19 ) ){
                                            echo rgar( $entry, 19 );
                                        } else {
                                            echo 'Present';
                                        }

                                    ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <?php if( rgar( $entry, 13 ) ){ ?>
                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Your Achievements in the group       
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 13 ); ?>
                                    </p>
                                </div>
                                <?php } ?>

                                <div class="col-md-12 padding-lr-10 padding-b-20">
                                    <p class="txt-xs txt-color-dark txt-medium padding-b-5">
                                        Description      
                                    </p>
                                    <p class="txt-sm">
                                        <?php echo rgar( $entry, 18 ); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="text-right">
                                <a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="edit" return="url" /]').'&view=trainer-profile&form-title=Certification / Affiliation' ?>" 
                                    class="edit-btn"
                                >
                                    Edit
                                </a>
                                <!--<a 
                                    href="<?php echo do_shortcode( '[gv_entry_link entry_id="'.$entry['id'].'" view_id="'.$gv_id.'" action="delete" return="url" /]').'&view=trainer-profile';  ?>" 
                                   class="delete-btn margin-l-5 confirm-delete"
                                >
                                    Delete
                                </a>-->
                            </div>
                        </div>

                        <?php } ?>

                        <div class="padding-o-20">
                            <a 
                                class="btn btn-ash txt-xxs no-m-b" 
                                href="<?php printf("https://www.saedconnect.org/service-provider-directory/trainer-profile/?view=form&gf-id=%s&form-title=%s", $gf_id, $title); ?>"
                            >
                                Add <?php echo $title ?>
                            </a>
                        </div>

                    <?php } ?>
                </div>  

            <?php } ?>   
            </div>
            <div class="dashboard-multi-main-sidebar">
               <div class="bg-grey padding-tb-30 padding-lr-20 margin-b-15">
                    <div class="txt-normal-s txt-medium text-center margin-b-15">
                        Your Trainer Profile Status
                    </div>
                    <div class="bg-yellow padding-o-10 rounded-o-12 text-center margin-b-15">
                        <?php
                            $publication_meta = get_post_meta( $post_id, $publication_key, true );

                            if($publication_meta == 'user_published'){
                                echo 'Undergoing Review';
                            } elseif($publication_meta == 'admin_published') {
                                echo 'Published';
                            }else{
                                echo 'Draft';
                            }
                        ?>
                    </div>
                    <p class="txt-xs text-center">
                       Publishing your profile allows you to be listed in the SAEDConnect trainer directory.
                       You must complete the Trainer Information section & select at least 1 skill that you train on, before you can publish your Trainer profile.
                    </p>
                    <div class="text-center padding-t-30">
                        <a href="<?php echo currentUrl(true).'?view=publication' ?>" class="txt-normal-s txt-medium no-m-b">
                           <u class="txt-color-dark"><?php echo ( $publication_meta ) ? 'Unpublish' : 'Publish Profile' ?></u>
                        </a>
                    </div>
                </div>
                <div class="padding-o-20 border-o-1 border-color-blue">
                    <div class="txt-normal-s txt-medium text-center margin-b-20">
                       NYSC Accreditation
                    </div>
                    <!--<div class="bg-blue txt-color-white padding-o-10 rounded-o-12 text-center margin-b-15">
                        <?php
                            $accreditation_meta = maybe_unserialize( get_post_meta( $post_id, $accreditation_key, true ) );

                            if($accreditation_meta['status'] == 'user_published'){
                                echo 'Undergoing Review';
                            } elseif($accreditation_meta['status'] == 'admin_published') {
                                echo 'Published';
                            }else{
                                echo 'Draft';
                            }
                        ?>
                    </div>-->
                    <?php if( false/*$accreditation_meta['status']*/){ ?>
                      
                        <p class="txt-sm text-center">
                           You have requested accreditation in 
                           <span class="txt-medium"><?php echo $accreditation_meta['state']; ?></span> 
                           state.
                        </p>
                      
                    <?php }else{ ?>
                        <p class="txt-xs text-center padding-b-15 margin-b-15">
                           Become recognized as a NYSC approved trainer under the SAED Program. Corp members who attend a training offered by an accredited SAED trainer are eligible to receive a special SAED certificate at the end of their service year.
                        </p>
                        <form class="" method="post" action="<?php echo currentUrl(true).'?view=form-nysc-accreditation'; ?>">
                            <div style="margin-bottom:10px">
                                <div class="padding-t-20 margin-b-40">
                                <?php 

                                    /*
                                    *
                                    * Populate Form Data from Terms
                                    *
                                    */
                                    //Get Terms
                                    $terms = get_terms( 'state', array('hide_empty' => false));

                                    foreach ($terms as $term) { //Cycle through terms, one at a time

                                        // Check and see if the term is a top-level parent. If so, display it.
                                        $term_id = $term->term_id; //Get the term ID
                                        $term_name = $term->name; //Get the term name
                                        $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term_id, true );
                                ?>
                                    <?php if( $saved_meta ){ ?>

                                    <div class="txt-sm txt-medium d-flex justify-content-between align-items-center margin-b-20">
                                        <span>
                                            <?php echo $term_name; ?>
                                            <span class="txt-color-yellow-dark">
                                            (
                                            <?php

                                                if($saved_meta == 'user_published'){
                                                    echo 'In Review';
                                                } elseif($saved_meta == 'admin_published') {
                                                    echo 'Published';
                                                }
                                            ?>
                                            )
                                            </span>
                                        </span>
                                        <a href="" class="txt-color-blue">Cancel</a>
                                    </div>

                                    <?php } ?>
                                <?php } ?>
                            </div>
                                <a data-toggle="collapse" href="#trainerProfileSelectStates" aria-expanded="false" class="d-flex">
                                    <span class="flex_1 padding-o-10 d-flex justify-content-between txt-normal-s bg-blue txt-color-white">
                                        <label for="state">Request Accreditation</label>
                                        <i class="fa fa-angle-down"></i>
                                    </span>
                                    
                                </a>
                                <div id="trainerProfileSelectStates" class="collapse" aria-labelledby="headingOne">
                                    <div class="padding-t-20">
                                        <p class="txt-sm padding-b-15">Select the state(s) where you want to be accredited</p>
                                        <?php 

                                            /*
                                            *
                                            * Populate Form Data from Terms
                                            *
                                            */
                                            //Get Terms
                                            $terms = get_terms( 'state', array('hide_empty' => false));

                                            foreach ($terms as $term) { //Cycle through terms, one at a time

                                                // Check and see if the term is a top-level parent. If so, display it.
                                                $term_id = $term->term_id; //Get the term ID
                                                $term_name = $term->name; //Get the term name
                                                $saved_meta = get_post_meta( $post_id, $accreditation_key.'_'.$term_id, true );
                                        ?>
                                            <?php if( $saved_meta ){ ?>
                                            
                                            
                                            <?php }else{ ?>
                                            <label class="txt-sm d-flex align-items-center padding-b-10">
                                                <input
                                                    class="margin-r-5 icheck"
                                                    type="checkbox" 
                                                    value="<?php echo $term_id ?>" 
                                                    name="state[]" 
                                                    <?php echo ( $saved_meta ) ? "checked" : "" ?>
                                                >
                                                <span class="bg-label padding-l-5" style="padding:5px 10px !important;">
                                                    <?php echo $term_name; ?>
                                                    
                                                </span>
                                            </label>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="text-center padding-t-10">
                                        <input type="submit" value="Request Accredition" class="btn btn-blue txt-xs txt-medium no-m-b">
                                    </div>
                                </div>
                            </div>
                            
                       </form>
                    <?php } ?>
               </div>
                <!--<div class="side-bar-card">
                    <form class="">
                         Switch 
                        <div class="row row-10 align-items-center margin-b-10">
                            <div class="col-8 txt-normal-s txt-medium padding-lr-10">
                                Make Profile Visible
                            </div>
                            <div class="col-4 text-right padding-lr-10">
                                <span class="switch switch-sm">
                                    <input type="checkbox" class="switch" id="1" checked>
                                    <label for="1" class="no-m-b"></label>
                                </span>
                            </div>
                        </div>
                        <input type="submit" value="Save" class="btn btn-blue txt-sm no-m-b">
                    </form>
                </div>-->
            </div>
        </section>
    </main>

<!--Load Scripts-->
<?php wp_footer('user-dashboard'); ?>

<!-- iCheck -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/_all.css" integrity="sha256-808LC4rdK/cu4RspCXPGrLKH7mgCcuNspF46UfBSbNQ=" crossorigin="anonymous" />

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js" integrity="sha256-LnexpAH6Dc12CjL5nVvF6kwco3N4Cs+Ahtm0fmnyGhg=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
    });
</script>