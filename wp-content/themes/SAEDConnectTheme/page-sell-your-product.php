    <?php /*Template Name: Sell Products*/ ?>
    
    <?php get_header() ?>
    
    <?php
        /* Filter Request */
        $model = $_GET['model'];
    ?>
   
    <main class="main-content">
        <header class="course-directory-banner image margin-b-40" style="background-image:url('<?php bloginfo('stylesheet_directory'); ?>/images/heroes/table.jpg');">
            <div class="content container-wrapper">
                <div class="row">
                    <div class="col-md-6 mx-auto text-center">
                        <h1 class="title">
                            Find industry experts, connect with a global network of experience.
                        </h1>
                    </div>
                </div>
            </div>
        </header>
        
        <section class="container-wrapper padding-t-40 padding-b-80">
            <header class="text-center margin-b-40">
                <h2 class="txt-xxlg txt-medium margin-b-15">
                    Browse by Category
                </h2> 
                <p>
                    Browse through some of our most popular categories.
                </p>  
            </header>
            <div class="">
                <ul class="row row-5 market-category-list">
                <?php
                    //Get Terms
                    $terms = get_terms( 'product-category', array('hide_empty' => false));
                    
                    foreach( $terms as $term ){
                        if ( $term->parent == 0 ){
                            $parent_category = $term->term_id;
                            $parent_name = $term->name;
                ?>
                   
                    <li class="col-sm-6 col-lg-3 padding-o-5 category-card">
                        <div class="wrapper">
                            <h3 class="title text-center" style="padding:20px">
                                <a 
                                   href="#termModal-<?php echo $parent_category ?>" 
                                   data-toggle="modal" 
                                   id="termModalTitle-<?php echo $parent_category ?>"
                                >
                                    <?php echo $term->name; ?>
                                </a>
                            </h3>
                        </div>   
                    </li>
                    
                    <!-- Term Modal -->
                    <div 
                       class="modal fade font-main product-category-modal" 
                       id="termModal-<?php echo $parent_category ?>" 
                       tabindex="-1" 
                       role="dialog" 
                       aria-labelledby="termModalTitle-<?php echo $parent_category ?>" 
                       aria-hidden="true"
                    >
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <button type="button" class="close txt-light txt-3em" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="container-wrapper">
                                    <div class="padding-tb-80">
                                        <div class="margin-b-40">
                                            <h2 class="txt-xlg txt-bold txt-height-1-2">
                                                <a href="<?php echo get_term_link($parent_category) ?>">
                                                    <?php echo $parent_name ?>
                                                </a>
                                            </h2>
                                        </div>
                                        <ul class="row row-20 shc-list txt-normal-s txt-medium">
                                        <?php
                                            foreach( $terms as $term ){ 
                                                if( $term->parent == $parent_category ){
                                        ?>

                                            <div class="col-md-4 padding-lr-20">
                                                <li>
                                                    <a href="<?php echo get_term_link($term) ?>">
                                                        <i class="fa fa-chevron-right txt-color-dark"></i>
                                                        <span class="txt-color-dark">
                                                            <?php echo $term->name; ?>
                                                        </span>
                                                    </a>
                                                </li>
                                            </div>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                <?php
                        } 
                    }
                ?>
                </ul>
            </div>
        </section>
    </main>
    
    <?php get_footer() ?>