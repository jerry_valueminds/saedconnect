<?php /*Template Name: Venture Responses*/ ?>

<?php get_header() ?>

<main class="main-content txt-color-light" style="margin-top: 100px"> 

<?php
    /* Reset Global Queried Object  */
    wp_reset_postdata();
    wp_reset_query();
   
    while ( have_posts() ) : the_post();

        $page_title = get_the_title();
        $gf_id = get_the_content();
   
    endwhile; // end of the loop.
?>
    <div class="container-wrapper bg-white padding-t-80">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="header text-center margin-b-40">
                    <h2 class="txt-xxlg txt-height-1-2 txt-light txt-color-dark section-wrapper-title">
                        <?php echo $page_title; ?>          
                    </h2>
                </div>
                <div class="">
                <?php
                    $request_type_array = array(
                        array(
                            'gf_id' => 20,
                            'parent_post_field' => 15,
                        ),
                        array(
                            'gf_id' => 21,
                            'parent_post_field' => 12,
                            'gv_id' => 99,
                            'title' => 'Land Request',
                        ),
                        array(
                            'gf_id' => 22,
                            'parent_post_field' => 13,
                            'gv_id' => 20,
                            'title' => 'Request for Workspace/Office Space/Building/Factory Space',
                        ),
                        array(
                            'gf_id' => 23,
                            'parent_post_field' => 13,
                            'gv_id' => 20,
                            'title' => 'Request for Collaborator/Volunteer',
                        ),
                        array(
                            'gf_id' => 24,
                            'parent_post_field' => 9,
                            'gv_id' => 20,
                            'title' => 'Request for Funding',
                        ),
                        array(
                            'gf_id' => 25,
                            'parent_post_field' => 11,
                            'gv_id' => 20,
                            'title' => 'Request for Marketing/Publicity',
                        ),
                        array(
                            'gf_id' => 27,
                            'parent_post_field' => 2,
                            'gv_id' => 20,
                            'title' => 'Request for a Mentor',
                        ),
                    );
                    
                    /* GF Search Criteria */
                    $search_criteria = array();

                    /* Get GF Entry Count */
                    $entry_count = GFAPI::count_entries( $gf_id, $search_criteria );
                    $entries = GFAPI::get_entries( $gf_id, $search_criteria );
                    $parent_post_id;

                    foreach( $entries as $entry ){
                        /* Get Parent Post ID */
                        foreach( $request_type_array as $request_item ){
                            /*echo $request_item['gf_id'];
                            echo '<br>';
                            echo $gf_id;
                            echo '<br><br><br>';*/
                            if($request_item['gf_id'] == $gf_id){
                                $parent_post_id = $request_item['parent_post_field'];
                            }
                        }
                          
                        $parent_post_id = rgar( $entry, $parent_post_id );
                        $entry_post_id = $entry['post_id'];
                        
                        $parent_post = get_post($parent_post_id);
                        $entry_post = get_post($entry_post_id);
                ?>
                    <div class="row border-b-1 border-color-darkgrey padding-b-20 margin-b-20">
                        <div class="col-md-8">
                            <div class="txt-lg margin-b-10">
                                <?php echo $entry_post->post_title; ?>
                            </div>
                            <div class="txt-sm">
                                for
                                <a href="<?php the_permalink($parent_post); ?>" class="txt-color-blue">
                                    <?php echo $parent_post->post_title; ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 text-right txt-sm">
                            <a href="<?php the_permalink($entry_post); ?>" class="txt-underline txt-bold">
                                View Request
                            </a>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
    
</main>

<?php wp_footer(); ?>
