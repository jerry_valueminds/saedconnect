<?php /*Template Name: Account Actived*/ ?>

<?php get_header() ?>
    
    <main class="main-content">
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="container-wrapper padding-o-80 text-center">
            <p class="txt-xlg">
                Your account has been successfully activated!
            </p>
            <div class="margin-t-20">
                <a class="btn btn-blue txt-sm no-m-b" href="https://www.saedconnect.org/login">
                    Click here to login
                </a>
            </div>
        </div>
        <?php endwhile; // end of the loop. ?>
    </main>

<?php get_footer() ?>