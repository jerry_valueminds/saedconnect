<?php
function mini_site_general_item_register_post_type() {

	$args = array (
		'label' => esc_html__( 'Mini-site General Items', 'text-domain' ),
		'labels' => array(
			'menu_name' => esc_html__( 'Mini-site General Items', 'text-domain' ),
			'name_admin_bar' => esc_html__( 'Mini-site General Item', 'text-domain' ),
			'add_new' => esc_html__( 'Add new', 'text-domain' ),
			'add_new_item' => esc_html__( 'Add new Mini-site General Item', 'text-domain' ),
			'new_item' => esc_html__( 'New Mini-site General Item', 'text-domain' ),
			'edit_item' => esc_html__( 'Edit Mini-site General Item', 'text-domain' ),
			'view_item' => esc_html__( 'View Mini-site General Item', 'text-domain' ),
			'update_item' => esc_html__( 'Update Mini-site General Item', 'text-domain' ),
			'all_items' => esc_html__( 'All Mini-site General Items', 'text-domain' ),
			'search_items' => esc_html__( 'Search Mini-site General Items', 'text-domain' ),
			'parent_item_colon' => esc_html__( 'Parent Mini-site General Item', 'text-domain' ),
			'not_found' => esc_html__( 'No Mini-site General Items found', 'text-domain' ),
			'not_found_in_trash' => esc_html__( 'No Mini-site General Items found in Trash', 'text-domain' ),
			'name' => esc_html__( 'Mini-site General Items', 'text-domain' ),
			'singular_name' => esc_html__( 'Mini-site General Item', 'text-domain' ),
		),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => false,
		'show_in_rest' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite_no_front' => false,
		'supports' => array(
			'title',
			'editor',
		),
		'menu_icon' => 'dashicons-admin-tools',
		'rewrite' => true,
	);

	register_post_type( 'mini-site-general-item', $args );
}
add_action( 'init', 'mini_site_general_item_register_post_type' );