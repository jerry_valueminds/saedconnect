<?php

add_action( 'mb_relationships_init', function() {
    MB_Relationships_API::register( array(
        'id'   => 'mini_sites_to_mini_site_general_items',
        
        
        'from' => array(
            'object_type' => 'post',
            'post_type'   => 'mini-site',
            'meta_box'    => array(
                'title'         => 'Items to show in Main Content area',
                'context'       => 'side',
            ),
        ),
        
        
        'to'   => array(
            'object_type' => 'post',
            'post_type'   => 'ms-general-item',
            'meta_box'    => array(
                'title'         => 'Shows on',
                'context'       => 'side',
                'empty_message' => 'No Mini Site created',
            ),
        ),
    ) );
} );